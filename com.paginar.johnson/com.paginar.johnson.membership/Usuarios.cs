using System.Web;
using System.Web.Security;
using System.Web.UI;
using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;


namespace com.paginar.johnson.membership
{
    public class Usuarios : MembershipProvider
    {
        MembershipDSTableAdapters.LoginTableAdapter LoginAdapter
        {
            get
            {
                return new MembershipDSTableAdapters.LoginTableAdapter();
            }
        }
        MembershipDSTableAdapters.UsuarioTableAdapter UsuarioAdapter
        {
            get
            {
                return new MembershipDSTableAdapters.UsuarioTableAdapter();
            }
        }   
        //MembershipDS.LoginDataTable LoginTable;
        //MembershipDS.UsuarioDataTable UsuarioTable;
        ManagerConnections MConnect = null;
        #region MembershipProvider Implementados
        public Usuarios()
        {
            //LoginAdapter = new MembershipDSTableAdapters.LoginTableAdapter();
          //  LoginTable = new MembershipDS.LoginDataTable();
            //UsuarioAdapter = new MembershipDSTableAdapters.UsuarioTableAdapter();
            //UsuarioTable = new MembershipDS.UsuarioDataTable();
            this.MConnect = new ManagerConnections();
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }


        /// <summary>
        /// Inicializa la clase
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);            
        }
        /// <summary>
        /// Validar Usuario
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        /// 
        private object LockObjectUser="";
        private object LockObjectLogin = "";
        public User GetUserByName(string nombre)
        {

            try
            {
                User UsuarioName = new User();
                
                MembershipDS.UsuarioDataTable UsuarioTable=null;

                lock(LockObjectUser){

                UsuarioTable = new MembershipDS.UsuarioDataTable();
                UsuarioTable.Clear();
                UsuarioAdapter.FillByUserName(UsuarioTable, nombre);
                }

                foreach (MembershipDS.UsuarioRow fila in UsuarioTable.Rows)
                {
                    UsuarioName.SetUsuarioId(fila.UsuarioID);
                    UsuarioName.SetApellido(fila.Apellido);
                    UsuarioName.SetEmail(fila.Email);
                    UsuarioName.SetLoginId(fila.LoginID);
                    UsuarioName.SetNombre(fila.Nombre);
                    UsuarioName.SetLegajo(fila.Legajo);
                }
                return UsuarioName;
            }
            catch (Exception)
            {
                if (UsuarioAdapter != null)
                    if (UsuarioAdapter.Connection != null)
                        this.MConnect.CerrarConexion(UsuarioAdapter.Connection);

                throw;
            }
            finally
            {
                if (UsuarioAdapter != null)
                    if (UsuarioAdapter.Connection != null)
                        this.MConnect.CerrarConexion (UsuarioAdapter.Connection);
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            //string global = username.Substring(0, 7);
            //Int32 longitud = username.Length - 7;
            //if (global.ToUpper() == "GLOBAL\\")
            //{
            //    string usernameB = username.Substring(7, longitud);
            //    username = usernameB;
            //}

            bool userExists = false;

            try
            {
                MembershipDS.LoginDataTable LoginTable=null;

                lock (this.LockObjectLogin)
                {
                    LoginTable = new MembershipDS.LoginDataTable();
                    LoginTable.Clear();
                    LoginAdapter.FillByUserNamePassword(LoginTable, username, password);

                    if (LoginTable.Rows.Count > 0)
                        userExists = true;
                }

            }
            catch
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                return false;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
            
            return userExists;

        }
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new System.Exception("The method or operation is not implemented.");
        }

        #endregion MembershipProvider Implementados

        #region MembershipProvider NO_Implementados

        public override string ApplicationName
        {
            get { return "Usuarios"; }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (this.ValidateUser(@username, @oldPassword))
            {
                LoginAdapter.UpdatePass(@newPassword, @username);
                return true;
            }
            else
            {
                return false;
                //throw new Exception("The method or operation is not implemented.");
                
            }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool EnablePasswordReset
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection retVal = null;
            //DSJohnson.LoginDataTable login_dt = FachadaDA.Singleton.Login.GetDataByEmail(emailToMatch);
            try
            {
                MembershipDS.LoginDataTable LoginTable=null;

                lock (this.LockObjectLogin)
                {
                    LoginTable = new MembershipDS.LoginDataTable();
                    LoginTable.Clear();
                    LoginAdapter.FillByEmail(LoginTable, emailToMatch);
                    totalRecords = LoginTable.Rows.Count;
                }
                 retVal = new MembershipUserCollection();

                for (int row = pageIndex * pageSize; row < (pageIndex + 1) * pageSize; row++)
                {
                    if (row >= (totalRecords - 1)) break;
                    MembershipDS.LoginRow user = LoginTable.Rows[row] as MembershipDS.LoginRow;
                    retVal.Add(new MembershipUser("Usuarios", user.Identif, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue));
                }
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }

            return retVal;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection retVal = null;
            try
            {
                MembershipDS.LoginDataTable LoginTable=null;


                lock (this.LockObjectLogin)
                {
                LoginTable = new MembershipDS.LoginDataTable();
                LoginTable.Clear();
                LoginAdapter.FillByName(LoginTable, usernameToMatch);
                totalRecords = LoginTable.Rows.Count;
                }


                retVal = new MembershipUserCollection();

                for (int row = pageIndex * pageSize; row < (pageIndex + 1) * pageSize; row++)
                {
                    if (row >= (totalRecords - 1)) break;
                    MembershipDS.LoginRow user = LoginTable.Rows[row] as MembershipDS.LoginRow;
                    retVal.Add(new MembershipUser("Usuarios", user.Identif, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue));
                }
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }

            return retVal;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection retVal = null;

            try
            {
                MembershipDS.LoginDataTable LoginTable=null;

                lock (this.LockObjectLogin)
                {
                    LoginTable = new MembershipDS.LoginDataTable();
                    LoginTable.Clear();
                    LoginAdapter.Fill(LoginTable);
                    totalRecords = LoginTable.Rows.Count;
                }

                retVal = new MembershipUserCollection();

                for (int row = pageIndex * pageSize; row < (pageIndex + 1) * pageSize; row++)
                {
                    if (row >= (totalRecords - 1)) break;
                    MembershipDS.LoginRow user = LoginTable.Rows[row] as MembershipDS.LoginRow;
                    retVal.Add(new MembershipUser("Usuarios", user.Identif, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue));
                }
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
            return retVal;
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string GetPassword(string username, string answer)
        {
            throw new Exception("The method or operation is not implemented.");

        }

        public MembershipUser GetUser(int LoginID)
        {

            try
            {
                MembershipDS.LoginDataTable LoginTable=null;

                lock (this.LockObjectLogin)
                {
                    LoginTable = new MembershipDS.LoginDataTable();
                    LoginTable.Clear();
                    LoginAdapter.FillByUserId(LoginTable, LoginID);
                }

                if ((LoginTable == null) || (LoginTable.Count == 0)) return null;

                MembershipDS.LoginRow user = LoginTable.Rows[0] as MembershipDS.LoginRow;
                MembershipUser retVal = new MembershipUser("Usuarios", user.Identif, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                return retVal;
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion (LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
        }

        //private void CerrarConexion(SqlConnection Connection)
        //{
        //    if (Connection != null)
        //        if (Connection.State != ConnectionState.Closed)
        //            Connection.Close();

        //}

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            try
            {
                // DSJohnson.LoginDataTable login_dt = FachadaDA.Singleton.Login.GetDataByLoginName(username);
                MembershipDS.LoginDataTable LoginTable=null;
                lock (this.LockObjectLogin)
                {
                    LoginTable = new MembershipDS.LoginDataTable();
                    LoginTable.Clear();
                    LoginAdapter.FillByName(LoginTable, username);
                }
                if ((LoginTable == null) || (LoginTable.Count == 0)) return null;

                MembershipDS.LoginRow user = LoginTable.Rows[0] as MembershipDS.LoginRow;
                MembershipUser retVal = new MembershipUser("Usuarios", username, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                return retVal;
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {

            try
            {
                if (!(providerUserKey is int))
                    throw new ApplicationException("OMLMembershipProvider::GetUs er() -- supplied providerUserKey is not an integer");

                MembershipDS.LoginDataTable LoginTable=null;

                lock (this.LockObjectLogin)
                {

                    LoginTable = new MembershipDS.LoginDataTable();
                LoginTable.Clear();
                LoginAdapter.FillByUserId(LoginTable, (int)providerUserKey);
                //DSJohnson.LoginDataTable login_dt = FachadaDA.Singleton.Login.GetDataByLoginID((int)providerUserKey);
                }
                if ((LoginTable == null) || (LoginTable.Count == 0)) return null;

                MembershipDS.LoginRow user = LoginTable.Rows[0] as MembershipDS.LoginRow;
                MembershipUser retVal = new MembershipUser("Usuarios", user.Identif, user.LoginID,
                    user.Email, String.Empty, "Comments!", true, false, DateTime.MinValue,
                    DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                return retVal;
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
        }

        public override string GetUserNameByEmail(string email)
        {

            try
            {
                  MembershipDS.LoginDataTable LoginTable=null;

                  lock (this.LockObjectLogin)
                  {

                      LoginTable = new MembershipDS.LoginDataTable();
                      LoginTable.Clear();
                      LoginAdapter.FillByEmail(LoginTable, email);
                  }
                if ((LoginTable == null) || (LoginTable.Count == 0)) return null;
                MembershipDS.LoginRow user = LoginTable.Rows[0] as MembershipDS.LoginRow;
                return user.Identif;
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.MConnect.CerrarConexion(LoginAdapter.Connection);
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return Int32.MaxValue; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            //get { throw new Exception("The method or operation is not implemented."); }
            get { return 0; }
        }

        public override int MinRequiredPasswordLength
        {
            //get { throw new Exception("The method or operation is not implemented."); }
            get { return 4; }
        }


        public override int PasswordAttemptWindow
        {
            get { return 10; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }



        public override bool RequiresUniqueEmail
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool UnlockUser(string userName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion MembershipProvider NO_Implementados
    }
}
