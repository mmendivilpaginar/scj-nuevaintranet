using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Configuration.Provider;
using System.Web;
using System.Web.Security;
using System.Data.OleDb;
using System.Data.SqlClient;



namespace com.paginar.johnson.membership
{
    public sealed class Roles : RoleProvider
    {
        
        MembershipDSTableAdapters.GrupoTableAdapter GrupoAdapter
        {
            get
            {
                return new MembershipDSTableAdapters.GrupoTableAdapter();
            }
        }

        MembershipDSTableAdapters.LoginTableAdapter LoginAdapter
        {
            get
            {
                return  new MembershipDSTableAdapters.LoginTableAdapter();
            }
        }

        //MembershipDS.GrupoDataTable GrupoTable;
        ////{
        ////    get
        ////    {
        ////        return new MembershipDS.GrupoDataTable();
        ////    }
        ////}

        //MembershipDS.LoginDataTable LoginTable;
        ////{
        //    get
        //    {
        //        return  new MembershipDS.LoginDataTable();
        //    }
        //}

        ManagerConnections MConnect = null;

        public Roles()
        {
            
            
            //GrupoAdapter = new MembershipDSTableAdapters.GrupoTableAdapter();
            //LoginAdapter = new MembershipDSTableAdapters.LoginTableAdapter();
            //GrupoTable = new MembershipDS.GrupoDataTable();
            //LoginTable = new MembershipDS.LoginDataTable();

            this.MConnect = new ManagerConnections();

            this.LockObjetc = DateTime.Now;
            this.LockObjetcLogin = DateTime.Now;
        }

        private RoleFlags TextToFlag(string flagName)
        {
            RoleFlags retVal = RoleFlags.None;

            try
            {
                retVal = (RoleFlags)Enum.Parse(typeof(RoleFlags), flagName, true);
            }
            catch { }

            return retVal;
        }

        #region RoleProvider Implementados

        public override string[] GetUsersInRole(string roleName)
        {
            try
            {
                List<string> resultado = new List<string>();
                MembershipDS.LoginDataTable LoginTable = null;
                lock (this.LockObjetcLogin)
                {
                LoginTable= new MembershipDS.LoginDataTable();
                LoginTable.Clear();

               
                    LoginAdapter.FillByRolName(LoginTable, roleName); 
                }

                foreach (MembershipDS.LoginRow usuario in LoginTable.Rows)
                {
                    resultado.Add(usuario.Identif);
                }

                return resultado.ToArray();
            }
            catch (Exception)
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.CerrarConexion(LoginAdapter.Connection);

                throw;
            }
            finally
            {
                if (LoginAdapter != null)
                    if (LoginAdapter.Connection != null)
                        this.CerrarConexion(LoginAdapter.Connection);
            }


        }
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            #region Validación de los Roles
            foreach (string rolename in roleNames)
            {
                if (!RoleExists(rolename))
                {
                    throw new ProviderException("El Rol no Existe.");
                }
            }
            #endregion Validación de los Roles

            #region validación de los Usuarios
            foreach (string username in usernames)
            {
                if (username.Contains(","))
                {
                    throw new ArgumentException("Los Nombres de usuario no pueden contener comas.");
                }

                foreach (string rolename in roleNames)
                {
                    if (IsUserInRole(username, rolename))
                    {
                        throw new ProviderException("El usuario ya pertenece al Rol.");
                    }
                }
            }
            #endregion validación de los Usuario

            try
            {

                foreach (string username in usernames)
                {
                    foreach (string rolename in roleNames)
                    {
                        //int nro_grupo = (int)ta_grupos.GetGroupNroByName(rolename);
                        //dt_Login = ta_login.GetDataByLoginName(username);
                        //foreach (DSJohnson.LoginRow fila_login in dt_Login.Rows)
                        //{
                        //    DSJohnson.RelLoginGrupoDataTable dt_RelLoginGrupo = new DSJohnson.RelLoginGrupoDataTable();
                        //    DSJohnson.RelLoginGrupoRow row_RelLoginGrupo = dt_RelLoginGrupo.NewRelLoginGrupoRow();
                        //    row_RelLoginGrupo.FHAlta = DateTime.Now;
                        //    row_RelLoginGrupo.LoginID = fila_login.LoginID;
                        //    row_RelLoginGrupo.GrupoNro = nro_grupo;
                        //    row_RelLoginGrupo.UsrAlta = 10096;
                        //    dt_RelLoginGrupo.AddRelLoginGrupoRow(row_RelLoginGrupo);
                        //    ta_RelLoginGrupo.Update(dt_RelLoginGrupo);

                        //}





                    }
                }

            }
            catch( Exception e ) {
            
            }
        }
        public override string[] GetAllRoles()
        {           
            List<string> resultado = new List<string>();
            try
            {
                MembershipDS.GrupoDataTable GrupoTable = null;
                lock (this.LockObjetc)
                {
                  GrupoTable = new MembershipDS.GrupoDataTable();
                     GrupoTable.Clear();
                    GrupoAdapter.Fill(GrupoTable);
                }

                foreach (MembershipDS.GrupoRow grupo in GrupoTable.Rows)
                {
                    resultado.Add(grupo.Descrip);
                }

                return resultado.ToArray();
            }
            catch
            {
                if (GrupoAdapter != null)
                    if (GrupoAdapter.Connection != null)
                        this.CerrarConexion(GrupoAdapter.Connection);
                return null;
            }
            finally
            {
                if (GrupoAdapter != null)
                    if (GrupoAdapter.Connection != null)
                        this.CerrarConexion(GrupoAdapter.Connection);
            }
        }
        private object LockObjetc = null;
        private object LockObjetcLogin = null;
        public override string[] GetRolesForUser(string username)
        {            
            List<string> resultado = new List<string>();
            try
            {
                //MembershipDSTableAdapters.GrupoTableAdapter GrupoAdapter = new MembershipDSTableAdapters.GrupoTableAdapter();
                //lock (LockObjetc)
                //{
                MembershipDS.GrupoDataTable GrupoTable = null;
                lock (this.LockObjetc)
                {
                    GrupoTable = new MembershipDS.GrupoDataTable();
                    
                    GrupoTable.Clear();
                    GrupoAdapter.FillRolesByUserName(GrupoTable, username);
                }
                foreach (MembershipDS.GrupoRow grupo in GrupoTable.Rows)
                {
                    resultado.Add(grupo.Descrip);
                }

                return resultado.ToArray();
            }
            catch (Exception exc)
            {
                if (GrupoAdapter != null)
                    if (GrupoAdapter.Connection != null)
                        this.CerrarConexion(GrupoAdapter.Connection);
                throw new Exception(string.Format("Error al consultar los roles por usuario. Detalle: {0}", exc.Message));

            }
            finally
            {
                if (GrupoAdapter != null)
                    if (GrupoAdapter.Connection != null)
                        this.CerrarConexion(GrupoAdapter.Connection);
            }
            
        }

        private void CerrarConexion(SqlConnection Connection)
        {
           
            this.MConnect.CerrarConexion(Connection);

        }
        public override bool IsUserInRole(string username, string roleName)
        {
            bool userIsInRole = false;            
            try
            {        
                lock(this.LockObjetcLogin){
                int numRoles = int.Parse( LoginAdapter.IsUserInRole(username,roleName).ToString() );
                
                if (numRoles > 0)
                {
                    userIsInRole = true;
                }
            }
            }
            catch (Exception e)
            {
                throw new Exception("Error al acceder Roles del usuario: "+e.Message);
            }
            
            return userIsInRole;

        }
        public override bool RoleExists(string roleName)
        {
            bool existe = false;
            try
            {
                lock(this.LockObjetc){
                    int numRols = GrupoAdapter.GetDataByRoleName(roleName).Rows.Count;

                    if (numRols > 0)
                    {
                        existe = true;
                    }
              }
            }
            catch (Exception e)
            {
                throw new Exception("Error al acceder a los Roles");
            }
            
            return existe;

        }

        #endregion RoleProvider Implementados

        #region RoleProvider NO_Implementados

        public override string ApplicationName
        { 
            get { return "Roles"; }
            
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        public override void CreateRole(string roleName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion RoleProvider NO_Implementados

    }
}
