using System;
using System.Collections.Generic;
using System.Text;

namespace com.paginar.johnson.membership
{
    public class User
    {
        int UsuarioId;
        int LoginId;
        string Nombre;
        string Apellido;
        string Email;
        int Legajo;
        int DiasDisponibles;

        public void SetUsuarioId(int UsuarioId)
        {
            this.UsuarioId = UsuarioId;
        }

        public void SetDiasDisponibles(int DiasDisponibles)
        {
            this.DiasDisponibles = DiasDisponibles;
        }

        public void SetLoginId(int LoginId)
        {
            this.LoginId = LoginId;
        }

        public void SetNombre(string Nombre)
        {
            this.Nombre = Nombre;
        }

        public void SetApellido(string Apellido)
        {
            this.Apellido = Apellido;
        }

        public void SetEmail(string Email)
        {
            this.Email = Email;
        }

        public void SetLegajo(int Legajo)
        {
            this.Legajo = Legajo;
        }

        public int GetUsuarioId()
        {
            return this.UsuarioId;
        }

        public int GetDiasDisponibles()
        {
            return this.DiasDisponibles;
        }

        public int GetLoginId()
        {
            return this.LoginId;
        }

        public string GetNombre()
        {
            return this.Nombre;
        }

        public string GetApellido()
        {
            return this.Apellido;
        }

        public string GetEmail()
        {
            return this.Email;
        }

        public int GetLegajo()
        {
            return this.Legajo;
        }
    }
}
