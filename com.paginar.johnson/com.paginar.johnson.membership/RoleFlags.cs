using System;
using System.Collections.Generic;
using System.Text;

namespace com.paginar.johnson.membership
{
    [Flags]
    public enum RoleFlags : int
    {
        None = 0,
        SysAdmin = 1,
        RegisteredUser = 2,
    }
}
