﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace com.paginar.johnson.membership
{
    internal class ManagerConnections
    {
        public void CerrarConexion(SqlConnection Connection)
        {
            if (Connection != null)
                if (Connection.State != ConnectionState.Closed)
                    Connection.Close();

        }
    }
}
