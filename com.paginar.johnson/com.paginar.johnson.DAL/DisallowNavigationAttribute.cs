﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    //[AttributeUsage(AttributeTargets.Property)]
    //public class AllowNavigationAttribute : Attribute
    //{
    //    public Boolean Show { get; private set; }

    //    public AllowNavigationAttribute(Boolean show)
    //    {
    //        Show = show;
    //    }
    //    // this will allow us to have a default set to false
    //    public static AllowNavigationAttribute Default = new
    //        AllowNavigationAttribute(true);
    //}
    [AttributeUsage(AttributeTargets.Property)]
    public class DisallowNavigationAttribute : Attribute
    {
        public Boolean Hide { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisallowNavigationAttribute"/> class.
        /// </summary>
        public DisallowNavigationAttribute()
        {
            Hide = false;
        }

        public DisallowNavigationAttribute(Boolean show)
        {
            Hide = show;
        }
    }
}
