﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSExperiencia
    {
        private DSExperienciaTableAdapters.exp_PublicacionTableAdapter _exp_Publicacion;
        public DSExperienciaTableAdapters.exp_PublicacionTableAdapter exp_Publicacion
        {
            get 
            {
                if (_exp_Publicacion == null)                 
                    _exp_Publicacion = new DSExperienciaTableAdapters.exp_PublicacionTableAdapter();
                return _exp_Publicacion;
                
            }
        }

        private DSExperienciaTableAdapters.exp_LinkTableAdapter _exp_LinkTable;
        public DSExperienciaTableAdapters.exp_LinkTableAdapter exp_LinkTable
        {
            get 
            {
                if(_exp_LinkTable == null)
                    _exp_LinkTable = new DSExperienciaTableAdapters.exp_LinkTableAdapter();
                return _exp_LinkTable;

            }
        }

        private DSExperienciaTableAdapters.exp_LikesTableAdapter _exp_LikesTable;
        public DSExperienciaTableAdapters.exp_LikesTableAdapter exp_LikesTable
        {
            get
            {
                if (_exp_LikesTable == null)
                    _exp_LikesTable = new DSExperienciaTableAdapters.exp_LikesTableAdapter();
                return _exp_LikesTable;
            }
        }

        private DSExperienciaTableAdapters.exp_ComentariosTableAdapter _exp_ComentariosTable;
        public DSExperienciaTableAdapters.exp_ComentariosTableAdapter exp_ComentariosTable
        {
            get
            {
                if (_exp_ComentariosTable == null)
                    _exp_ComentariosTable = new DSExperienciaTableAdapters.exp_ComentariosTableAdapter();
                return _exp_ComentariosTable;
            }
        }

        private DSExperienciaTableAdapters.exp_BuscadorTableAdapter _exp_BuscadorTable;
        public DSExperienciaTableAdapters.exp_BuscadorTableAdapter exp_BuscadorTable
        {
            get
            {
                if (_exp_BuscadorTable == null)
                    _exp_BuscadorTable = new DSExperienciaTableAdapters.exp_BuscadorTableAdapter();
                return _exp_BuscadorTable;
            }
        }
 
    }
}
