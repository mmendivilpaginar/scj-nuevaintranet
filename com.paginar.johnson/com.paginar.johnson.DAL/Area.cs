﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.Linq;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_Area))]
    [DisplayName("Areas")]
    public partial class Area
    {
        partial void OnValidate(ChangeAction action)
        {
            if (action == ChangeAction.Delete)
            {
                var DC = new SCJBODataContext();

                if (DC.UsuarioBOs.Where(c => c.AreaID == this.AreaID).ToList().Count > 0)
                {
                    throw new ValidationException("No podrá eliminar el area " + this.AreaDESC + ", debido a que posee un usuario relacionado. Por Favor, reasigne el/los usuarios al Area que corresponda.");
                }
            }
        }


        public class MetaData_Area
        {
            
           
            [DisplayName("Descripción")]
            [FilterUIHint("Search")]
            public object AreaDESC { get; set; }

            [ScaffoldColumnAttribute(false)]
            public EntitySet<UsuarioBO> UsuarioBOs { get; set; }

        }
    }
}
