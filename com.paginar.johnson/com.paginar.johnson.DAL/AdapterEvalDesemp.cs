﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterEvalDesemp
    {
        private DSUsuariosTableAdapters.UsuarioTableAdapter _Usuario;
        private DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter _frmObjetivosProxTableAdapter;


        public DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter frmObjetivosProxTableAdapter
        {
            get
            {
                if (_frmObjetivosProxTableAdapter == null) _frmObjetivosProxTableAdapter = new DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter();
                return _frmObjetivosProxTableAdapter;

            }
        }       


        private DSEvalDesempTableAdapters.frmFormularioTableAdapter _frmFormularioTableAdapter;


        public DSEvalDesempTableAdapters.frmFormularioTableAdapter frmFormularioTableAdapter
        {
            get
            {
                if (_frmFormularioTableAdapter == null) _frmFormularioTableAdapter = new DSEvalDesempTableAdapters.frmFormularioTableAdapter();
                return _frmFormularioTableAdapter;

            }
        }

        private DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter _GetReporteSegumientoTableAdapter;
        public DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter GetReporteSegumientoTableAdapter
        {
            get
            {
                if (_GetReporteSegumientoTableAdapter == null) _GetReporteSegumientoTableAdapter = new DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter();
                return _GetReporteSegumientoTableAdapter;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter _GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter
        {
            get
            {
                if (_GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter == null) _GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter();
                return _GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoTotalesByEvaluadorTableAdapter _GetReporteSeguimientoTotalesByEvaluadorTableAdapter;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoTotalesByEvaluadorTableAdapter GetReporteSeguimientoTotalesByEvaluadorTableAdapter
        {
            get
            {
                if (_GetReporteSeguimientoTotalesByEvaluadorTableAdapter == null) _GetReporteSeguimientoTotalesByEvaluadorTableAdapter = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoTotalesByEvaluadorTableAdapter();
                return _GetReporteSeguimientoTotalesByEvaluadorTableAdapter;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoConSubtotalesPorAreaTableAdapter _GetReporteSeguimientoConSubtotalesPorAreaTableAdapter;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoConSubtotalesPorAreaTableAdapter GetReporteSeguimientoConSubtotalesPorAreaTableAdapter
        {
            get
            {
                if (_GetReporteSeguimientoConSubtotalesPorAreaTableAdapter == null) _GetReporteSeguimientoConSubtotalesPorAreaTableAdapter = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoConSubtotalesPorAreaTableAdapter();
                return _GetReporteSeguimientoConSubtotalesPorAreaTableAdapter;

            }
        }

        private DSEvalDesempReportesTableAdapters.frmGetReportePMPNecesidadesPorcTableAdapter _GetReportePMPNecesidadesPorc;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPNecesidadesPorcTableAdapter GetReportePMPNecesidadesPorc
        {
            get
            {
                if (_GetReportePMPNecesidadesPorc == null) _GetReportePMPNecesidadesPorc = new DSEvalDesempReportesTableAdapters.frmGetReportePMPNecesidadesPorcTableAdapter();
                return _GetReportePMPNecesidadesPorc;

            }
        }
        private DSEvalDesempReportesTableAdapters.frmGetReportePMPFortalezasPorcTableAdapter _GetReportePMPFortalezasPorc;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPFortalezasPorcTableAdapter GetReportePMPFortalezasPorc
        {
            get
            {
                if (_GetReportePMPFortalezasPorc == null) _GetReportePMPFortalezasPorc = new DSEvalDesempReportesTableAdapters.frmGetReportePMPFortalezasPorcTableAdapter();
                return _GetReportePMPFortalezasPorc;

            }
        }
        private DSEvalDesempReportesTableAdapters.frmGetReportePMPCursosTableAdapter _GetReportePMPCursosTableAdapter;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPCursosTableAdapter GetReportePMPCursosTableAdapter
        {
            get
            {
                if (_GetReportePMPCursosTableAdapter == null) _GetReportePMPCursosTableAdapter = new DSEvalDesempReportesTableAdapters.frmGetReportePMPCursosTableAdapter();
                return _GetReportePMPCursosTableAdapter;

            }
        }

        private DSEvalDesempReportesTableAdapters.frmGetReporteCompetenciaComoFortalezasTableAdapter _GetReporteCompetenciaComoFortalezasTableAdapter;
        public DSEvalDesempReportesTableAdapters.frmGetReporteCompetenciaComoFortalezasTableAdapter GetReporteCompetenciaComoFortalezasTableAdapter
        {
            get
            {
                if (_GetReporteCompetenciaComoFortalezasTableAdapter == null) _GetReporteCompetenciaComoFortalezasTableAdapter = new DSEvalDesempReportesTableAdapters.frmGetReporteCompetenciaComoFortalezasTableAdapter();
                return _GetReporteCompetenciaComoFortalezasTableAdapter;

            }
        }


        private DSEvalDesempReportesTableAdapters.frmGetReportePMPCalificacionesTableAdapter _GetReportePMPCalificacionesTableAdapter;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPCalificacionesTableAdapter GetReportePMPCalificacionesTableAdapter
        {
            get
            {
                if (_GetReportePMPCalificacionesTableAdapter == null) _GetReportePMPCalificacionesTableAdapter = new DSEvalDesempReportesTableAdapters.frmGetReportePMPCalificacionesTableAdapter();
                return _GetReportePMPCalificacionesTableAdapter;

            }
        }


        private DSEvalDesempReportesTableAdapters.frmGetReportePMPPlanDeAccionTableAdapter _GetReportePMPPlanDeAccionTableAdapter;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPPlanDeAccionTableAdapter GetReportePMPPlanDeAccionTableAdapter
        {
            get
            {
                if (_GetReportePMPPlanDeAccionTableAdapter == null) _GetReportePMPPlanDeAccionTableAdapter = new DSEvalDesempReportesTableAdapters.frmGetReportePMPPlanDeAccionTableAdapter();
                return _GetReportePMPPlanDeAccionTableAdapter;

            }
        }

        private DSEvalDesempReportesTableAdapters.frmGetReportePMPCarrerPlanTableAdapter _GetReportePMPCarrerPlanTableAdapter;
        public DSEvalDesempReportesTableAdapters.frmGetReportePMPCarrerPlanTableAdapter GetReportePMPCarrerPlanTableAdapter
        {
            get
            {
                if (_GetReportePMPCarrerPlanTableAdapter == null) _GetReportePMPCarrerPlanTableAdapter = new DSEvalDesempReportesTableAdapters.frmGetReportePMPCarrerPlanTableAdapter();
                return _GetReportePMPCarrerPlanTableAdapter;

            }
        }


        private DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter _GetReporteSegumiento;
        public DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter GetReporteSegumiento
        {
            get
            {
                if (_GetReporteSegumiento == null) _GetReporteSegumiento = new DSEvalDesempTableAdapters.GetReporteSegumientoTableAdapter();
                return _GetReporteSegumiento;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTableAdapter _Eval_GetReporteSeguimientoAdministrativosPorArea;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTableAdapter Eval_GetReporteSeguimientoAdministrativosPorArea
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAdministrativosPorArea == null) _Eval_GetReporteSeguimientoAdministrativosPorArea = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTableAdapter();
                return _Eval_GetReporteSeguimientoAdministrativosPorArea;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativos1TableAdapter _Eval_GetReporteSeguimientoAministrativosPorDireccion;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativos1TableAdapter Eval_GetReporteSeguimientoAministrativosPorDireccion
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAministrativosPorDireccion == null) _Eval_GetReporteSeguimientoAministrativosPorDireccion = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativos1TableAdapter();
                return _Eval_GetReporteSeguimientoAministrativosPorDireccion;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosOrigTableAdapter _Eval_GetReporteSeguimientoAministrativosPorEvaluador;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosOrigTableAdapter Eval_GetReporteSeguimientoAministrativosPorEvaluador
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAministrativosPorEvaluador == null) _Eval_GetReporteSeguimientoAministrativosPorEvaluador = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosOrigTableAdapter();
                return _Eval_GetReporteSeguimientoAministrativosPorEvaluador;

            }
        }


        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTotalesTableAdapter _Eval_GetReporteSeguimientoAdministrativosPorAreaTotales;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTotalesTableAdapter Eval_GetReporteSeguimientoAdministrativosPorAreaTotales
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAdministrativosPorAreaTotales == null) _Eval_GetReporteSeguimientoAdministrativosPorAreaTotales = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAdministrativosPorAreaTotalesTableAdapter();
                return _Eval_GetReporteSeguimientoAdministrativosPorAreaTotales;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesOrigTableAdapter _Eval_GetReporteSeguimientoAministrativosTotalesPorEvaluador;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesOrigTableAdapter Eval_GetReporteSeguimientoAministrativosTotalesPorEvaluador
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAministrativosTotalesPorEvaluador == null) _Eval_GetReporteSeguimientoAministrativosTotalesPorEvaluador = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesOrigTableAdapter();
                return _Eval_GetReporteSeguimientoAministrativosTotalesPorEvaluador;

            }
        }

        private DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesTableAdapter _Eval_GetReporteSeguimientoAministrativosTotalesPorDireccion;
        public DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesTableAdapter Eval_GetReporteSeguimientoAministrativosTotalesPorDireccion
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAministrativosTotalesPorDireccion == null) _Eval_GetReporteSeguimientoAministrativosTotalesPorDireccion = new DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesTableAdapter();
                return _Eval_GetReporteSeguimientoAministrativosTotalesPorDireccion;

            }
        }

    }
}
