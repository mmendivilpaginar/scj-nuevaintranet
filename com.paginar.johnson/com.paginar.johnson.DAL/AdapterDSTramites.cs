﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSTramites
    {
        private DSTramitesTableAdapters.frmNuevoHistoricoTableAdapter _NuevoHistorico;
        public DSTramitesTableAdapters.frmNuevoHistoricoTableAdapter NuevoHistorico
        {
            get
            {
                if (_NuevoHistorico == null) _NuevoHistorico = new DSTramitesTableAdapters.frmNuevoHistoricoTableAdapter();
                return _NuevoHistorico;
            }
        }

        private DSTramitesTableAdapters.eval_EvaluadosTableAdapter _Evaluados;
        public DSTramitesTableAdapters.eval_EvaluadosTableAdapter Evaluados
        {
            get
            {
                if (_Evaluados == null) _Evaluados = new DSTramitesTableAdapters.eval_EvaluadosTableAdapter();
                return _Evaluados;
            }
        }


        private DSTramitesTableAdapters.DSTramites_frmTramite_GetTramitesVacacionesTableAdapter _frmTramiteVacaciones;
        public DSTramitesTableAdapters.DSTramites_frmTramite_GetTramitesVacacionesTableAdapter frmTramiteVacaciones
        {
            get
            {
                if (_frmTramiteVacaciones == null) _frmTramiteVacaciones = new DSTramitesTableAdapters.DSTramites_frmTramite_GetTramitesVacacionesTableAdapter();
                return _frmTramiteVacaciones;
            }
        }


    }
}
