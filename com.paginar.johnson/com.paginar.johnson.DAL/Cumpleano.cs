﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_Cumpleano))]
    [DisplayName("Cumpleaños")]
    public partial class Cumpleano
    {
        public class MetaData_Cumpleano
        {
             [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
             [DisplayName("Fecha de Nacimiento")]
            public System.DateTime FechaNacimiento { get; set; }
             [DisplayName("Cluster")]
             [DisallowNavigationAttribute(true)]
             public object Cluster1 { get; set; }
            
             [DisallowNavigationAttribute(true)]
             public object Ubicacion { get; set; }
        }


        
        
    }
}
