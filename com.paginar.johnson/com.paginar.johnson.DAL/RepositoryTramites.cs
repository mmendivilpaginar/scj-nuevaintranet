﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryTramites
    {
        private AdapterDSTramites _AdapterDSTramites;
        public AdapterDSTramites AdapterDSTramites
        {
            get
            {
                if (_AdapterDSTramites == null) _AdapterDSTramites = new AdapterDSTramites();
                return _AdapterDSTramites;
            }
        }

        public void InsertarPasoNuevoHistorico(int his_TramiteID, int his_UsrAlta, int his_UsrDestino, string his_Accion, int his_Paso, string his_Observaciones, int his_secuencia)
        {
            AdapterDSTramites.NuevoHistorico.frmNuevoHistoricoPasoInsertar(his_TramiteID, his_UsrAlta, his_UsrDestino, his_Accion, his_Paso, his_Observaciones, his_secuencia);

        }

        public DSTramites.frmNuevoHistoricoDataTable frmNuevoHistoricoUltimoByTramiteId(int his_tramiteID)
        {
            return AdapterDSTramites.NuevoHistorico.frmNuevoHistoricoUltimoByTramiteId(his_tramiteID);
        }

        public DSTramites.eval_EvaluadosDataTable getAreas()
        {
            return AdapterDSTramites.Evaluados.GetDataAreas();
        }

        public DSTramites.DSTramites_frmTramite_GetTramitesVacacionesDataTable getVacaciones(int? Area, int? Direccion, DateTime? fechadesde, DateTime? fechahasta, int? estado, int? legajo, string apellido, string nombre)
        {
            return AdapterDSTramites.frmTramiteVacaciones.GetVacaciones(fechadesde, fechahasta, Direccion, Area, estado, legajo, apellido, nombre);
        }
    }
}