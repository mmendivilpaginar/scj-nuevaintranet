﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSUsuarios
    {
        private DSUsuariosTableAdapters.UsuarioTableAdapter _Usuario;
        public DSUsuariosTableAdapters.UsuarioTableAdapter Usuario
        {
            get
            {
                if (_Usuario == null) _Usuario = new DSUsuariosTableAdapters.UsuarioTableAdapter();
                return _Usuario;

            }
        }

        private DSUsuariosTableAdapters.LoginTableAdapter _Login;
        public DSUsuariosTableAdapters.LoginTableAdapter Login
        {
            get
            {
                if (_Login == null) _Login = new DSUsuariosTableAdapters.LoginTableAdapter();
                return _Login;

            }
        }

        private DSUsuariosTableAdapters.GrupoTableAdapter _Grupo;
        public DSUsuariosTableAdapters.GrupoTableAdapter Grupo
        {
            get
            {
                if (_Grupo == null) _Grupo = new DSUsuariosTableAdapters.GrupoTableAdapter();
                return _Grupo;

            }
        }


        private DSUsuariosTableAdapters.frmFormularioTableAdapter _frmFormulario;
        public DSUsuariosTableAdapters.frmFormularioTableAdapter frmFormulario
        {
            get
            {
                if (_frmFormulario == null) _frmFormulario = new DSUsuariosTableAdapters.frmFormularioTableAdapter();
                return _frmFormulario;

            }
        }

        private DSUsuariosTableAdapters.UsuariosActivosTableAdapter _UsuariosActivos;
        public DSUsuariosTableAdapters.UsuariosActivosTableAdapter UsuariosActivos
        {
            get
            {
                if (_UsuariosActivos == null) _UsuariosActivos = new DSUsuariosTableAdapters.UsuariosActivosTableAdapter();
                return _UsuariosActivos;
            }
        }

        private DSUsuariosTableAdapters.frmRelUsuarioFormularioTableAdapter _frmRelUsuarioFormulario;
        public DSUsuariosTableAdapters.frmRelUsuarioFormularioTableAdapter frmRelUsuarioFormulario
        {
            get
            {
                if (_frmRelUsuarioFormulario == null) _frmRelUsuarioFormulario = new DSUsuariosTableAdapters.frmRelUsuarioFormularioTableAdapter();
                return _frmRelUsuarioFormulario;

            }
        }

        private DSUsuariosTableAdapters.RelLoginGrupoTableAdapter _RelLoginGrupo;
        public DSUsuariosTableAdapters.RelLoginGrupoTableAdapter RelLoginGrupo
        {
            get
            {
                if (_RelLoginGrupo == null) _RelLoginGrupo = new DSUsuariosTableAdapters.RelLoginGrupoTableAdapter();
                return _RelLoginGrupo;

            }
        }

        private DSUsuariosTableAdapters.RelUsuarioAccesoClusterTableAdapter _RelUsuarioAccesoCluster;
        public DSUsuariosTableAdapters.RelUsuarioAccesoClusterTableAdapter RelUsuarioAccesoCluster
        {
            get
            {
                if (_RelUsuarioAccesoCluster == null) _RelUsuarioAccesoCluster = new DSUsuariosTableAdapters.RelUsuarioAccesoClusterTableAdapter();
                return _RelUsuarioAccesoCluster;

            }
        }

        private DSUsuariosTableAdapters.PerfilTableAdapter _Perfil;
        public DSUsuariosTableAdapters.PerfilTableAdapter Perfil
        {
            get
            {
                if (_Perfil == null) _Perfil = new DSUsuariosTableAdapters.PerfilTableAdapter();
                return _Perfil;
            }
        }

        private DSUsuariosTableAdapters.HijosDataTableTableAdapter _Hijos;
        public DSUsuariosTableAdapters.HijosDataTableTableAdapter Hijos
        {
            get
            {
                if (_Hijos == null) _Hijos = new DSUsuariosTableAdapters.HijosDataTableTableAdapter();
                return _Hijos;
            }
        }

        private DSUsuariosTableAdapters.EscalaSalarialTableAdapter _EscalaSalarial;
        public DSUsuariosTableAdapters.EscalaSalarialTableAdapter EscalaSalarial
        {
            get
            {
                if (_EscalaSalarial == null) _EscalaSalarial = new DSUsuariosTableAdapters.EscalaSalarialTableAdapter();
                return _EscalaSalarial;
            }
        }

        private DSUsuariosTableAdapters.CargoTableAdapter _Cargos;
        public DSUsuariosTableAdapters.CargoTableAdapter Cargos
        {
            get 
            {
                if (_Cargos == null) _Cargos = new DSUsuariosTableAdapters.CargoTableAdapter();
                return _Cargos;
            }
        }

        private DSUsuariosTableAdapters.UsuarioConCompensatoriosTableAdapter _UsuariosCompensatorio;
        public DSUsuariosTableAdapters.UsuarioConCompensatoriosTableAdapter UsuariosCompensatorio
        {
            get
            {
                if (_UsuariosCompensatorio == null) _UsuariosCompensatorio = new DSUsuariosTableAdapters.UsuarioConCompensatoriosTableAdapter();
                return _UsuariosCompensatorio;
            }
        }

        private DSUsuariosTableAdapters.SectorTableAdapter _Sector;
        public DSUsuariosTableAdapters.SectorTableAdapter Sector
        {
            get
            {
                if (_Sector == null) _Sector = new DSUsuariosTableAdapters.SectorTableAdapter();
                return _Sector;
            }
        }

    }
}
