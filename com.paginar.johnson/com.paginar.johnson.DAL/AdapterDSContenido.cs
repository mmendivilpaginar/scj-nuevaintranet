﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
   public class AdapterDSContenido:IDisposable
    {

        
        private DsContenidoTableAdapters.Content_ItemsTableAdapter _ItemsTB;

        public DsContenidoTableAdapters.Content_ItemsTableAdapter ItemContenido
        {
            get {
                if (_ItemsTB == null) _ItemsTB = new DsContenidoTableAdapters.Content_ItemsTableAdapter();
                return _ItemsTB; 
            }
           
        }

        private DsContenidoTableAdapters.Content_TipoTableAdapter _TipoContenido;

        public DsContenidoTableAdapters.Content_TipoTableAdapter TipoContenido
        {
            get {
                if (_TipoContenido == null) _TipoContenido = new DsContenidoTableAdapters.Content_TipoTableAdapter();
                return _TipoContenido; }
           
        }

        private DsContenidoTableAdapters.Content_ItemsByClusterTableAdapter _ContenidoInCluster;

        public DsContenidoTableAdapters.Content_ItemsByClusterTableAdapter ContenidoInCluster
        {
            get {
                if (_ContenidoInCluster == null) _ContenidoInCluster = new DsContenidoTableAdapters.Content_ItemsByClusterTableAdapter();
                return _ContenidoInCluster; }
           
        }


        private DsContenidoTableAdapters.ClusterTableAdapter _Cluster;

        public DsContenidoTableAdapters.ClusterTableAdapter Cluster
        {
            get {
                if (_Cluster == null) _Cluster = new DsContenidoTableAdapters.ClusterTableAdapter();
                return _Cluster; }
           
        }


        private DsContenidoTableAdapters.Content_ItemsMenuUrlToolTipTableAdapter _Content_ItemsMenuUrlToolTip;
        public DsContenidoTableAdapters.Content_ItemsMenuUrlToolTipTableAdapter Content_ItemsMenuUrlToolTip
        {
            get
            {
                if (_Content_ItemsMenuUrlToolTip == null) _Content_ItemsMenuUrlToolTip = new DsContenidoTableAdapters.Content_ItemsMenuUrlToolTipTableAdapter();
                return _Content_ItemsMenuUrlToolTip;
            }
  
        }


        public void Dispose()
        {
            this._Cluster = null;
            this._ContenidoInCluster = null;
            this._TipoContenido = null;
            this._ItemsTB = null;
        }
    }
}
