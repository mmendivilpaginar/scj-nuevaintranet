﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryWorkShop
    {
        private AdapterDSWorkShop _AdapterDSWorkShop;
        public AdapterDSWorkShop AdapterDSWorkShop
        {
            get {
                if (_AdapterDSWorkShop == null) _AdapterDSWorkShop = new AdapterDSWorkShop();
                return _AdapterDSWorkShop;
            }
        }



        public void WorkShopInsetInscripcion(int IdWorkshop, int Legajo, int Cluster, string Comentarios, DateTime FechaInscripcion, bool Notificada)
        {
            AdapterDSWorkShop.WSInscripcion.ws_insertInscripcion(IdWorkshop, Legajo, Cluster, Comentarios, FechaInscripcion, Notificada);
        }

        public DSWorkShop.ws_WorkshopDataTable WorkshopSelectAll()
        {
            return AdapterDSWorkShop.Workshop.GetAllWorkshop();
        }

        public DSWorkShop.ws_WorkshopDataTable WorkshopGetSpecific(int IdWorkshop)
        {
            return AdapterDSWorkShop.Workshop.WorkshopGetSpecific(IdWorkshop);
        }

        public void WorkshopInsert(string Nombre, string Objetivo, string Metodologia, string Requisitos, string Duracion, string FechaInicioWorkshop, int IdEstado, DateTime? PeriodoInscripcionInicio, DateTime? PeriodoInscripcionFin)
        {
            AdapterDSWorkShop.Workshop.WorkshopInsert(Nombre, Objetivo, Metodologia, Requisitos, Duracion, FechaInicioWorkshop, IdEstado, PeriodoInscripcionInicio, PeriodoInscripcionFin);
        }

        public void WorkshopUpdate(int IdWorkshop, string Nombre, string Objetivo, string Metodologia, string Requisitos, string Duracion, string FechaInicioWorkshop, int IdEstado, DateTime? PeriodoInscripcionInicio, DateTime? PeriodoInscripcionFin)
        {
            AdapterDSWorkShop.Workshop.WorkshopUpdate(IdWorkshop, Nombre, Objetivo, Metodologia, Requisitos, Duracion, FechaInicioWorkshop, IdEstado, PeriodoInscripcionInicio, PeriodoInscripcionFin);
        }

        public void WorkshopDelete(int IdWorkshop)
        {
            AdapterDSWorkShop.Workshop.WorkshopDelete(IdWorkshop);
        }

        public DSWorkShop.ws_WorkshopDataTable WorkshopDisponiblesPorUsuario(int Legajo)
        {
            return AdapterDSWorkShop.Workshop.WorkshopDisponiblesPorUsuario(Legajo);

        }

        public DSWorkShop.ws_WorkshopDataTable WorshopDisponiblesActivos()
        {
            return AdapterDSWorkShop.Workshop.WorkshopCursosDisponiblesActivos();
        }

        public DSWorkShop.ws_Inscripcion1DataTable GetSuscriptos(int IdWorkshop)
        {
            return AdapterDSWorkShop.WSSuscriptos.GetInscriptosWorkshop(IdWorkshop);
        }

        public DSWorkShop.ws_selectInscriptosByAnioDataTable GetSuscriptosByAnio(int? anio)
        {
            return AdapterDSWorkShop.WSInscriptosByAnio.getInscriptosByAnio(anio);
        }

        
        //public void WSDocumentInsert(int IdWorkshop, string Ubicacion)
        //{
        //    //Ubicacion == Documento
        //    AdapterDSWorkShop.WSDocumentos.ws_DocumentInsert(IdWorkshop, Ubicacion);
        //}

        //public void WSDocumentDelete(int IdWorkshop)
        //{
        //    AdapterDSWorkShop.WSDocumentos.ws_DocumentDelete(IdWorkshop);
        //}

        //public void WSDOcumentUpdate(int IdDocumento, int IdWorkshop, string Ubicacion)
        //{
        //    AdapterDSWorkShop.WSDocumentos.ws_DocumentUpdate(IdWorkshop, Ubicacion);
        //}

        //public DSWorkShop.ws_Documentos_WorkshopDataTable WSDOcumentSelect(int IdWorkshop)
        //{
        //    return AdapterDSWorkShop.WSDocumentos.ws_DocumentSelect(IdWorkshop);
        //}

    }
}
