﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.Linq;
using System;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]   
    [MetadataType(typeof(MetaData_clasiRubro))]
    [DisplayName("Rubros Clasificados")]
    partial class clasiRubro
    {
        public class MetaData_clasiRubro
        {

            [ScaffoldColumn(false)]
            public object clasiAvisos { get; set; }


            [DisplayName("Descripción")]
            [FilterUIHint("Search")]
            public object Descrip { get; set; }

            [ScaffoldColumn(false)]
            public object UsrAlta { get; set; }

            [ScaffoldColumn(false)]
            public object FHAlta { get; set; }

            [ScaffoldColumn(false)]
            public object FHMod { get; set; }

            [ScaffoldColumn(false)]
            public object UsrMod { get; set; }

        }



        partial void OnValidate(ChangeAction action)
        {
            if (action == ChangeAction.Delete)
            {
                var DC = new SCJBODataContext();

              //  if (DC.UsuarioBOs.Where(c => c.AreaID == this.AreaID).ToList().Count > 0)
                if (DC.clasiAvisos.Where(c => c.RubroID == this.RubroID).ToList().Count > 0)
                {
                    throw new ValidationException("No podrá eliminar el Rubro: \"" + this.Descrip + "\", debido a que posee uno o mas articulos relacionado.");
                }
            }
        }

        partial void OnDescripChanged()
        {
            this.SetValueIniciales();

        }

        partial void OnUsrModChanging(System.Nullable<int> value)
        {
           
        }



     //   private System.Data.Linq.ChangeAction CurrentOperacion;
      

        private void SetValueIniciales()
        {

          //  this.UsrAlta = DAL.ValueCommon.CurrentUserId;

            switch (DAL.ValueCommon.LastOperation)
            {
                //case ChangeAction.Delete:
                //case ChangeAction.None:
                case ChangeAction.Update:
                    this.UsrMod = DAL.ValueCommon.CurrentUserId;
                    FHMod = System.DateTime.Now;
                break;
                case ChangeAction.Insert:
                    this.UsrAlta = DAL.ValueCommon.CurrentUserId;
                    FHAlta = System.DateTime.Now;
                    break;
                default:
                    break;
            }
                

        }

    }

    public class ValueCommon
    {
        
        public static int CurrentUserId;
        public static System.Data.Linq.ChangeAction LastOperation;
    }

}
