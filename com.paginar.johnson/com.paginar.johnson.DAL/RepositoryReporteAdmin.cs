﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryReporteAdmin
    {
        private AdapterDSReporteAdmin _AdapterDSReporteAdmin;
        public AdapterDSReporteAdmin AdapterDSReporteAdmin
        {
            get 
            {
                if (_AdapterDSReporteAdmin == null) _AdapterDSReporteAdmin = new AdapterDSReporteAdmin();
                return _AdapterDSReporteAdmin;
            }
        }

        public DSReporteAdmin.CalificacionesPMPDataTable GetCalificacionesLegajoPeriodoID(int? Legajo, int PeriodoID, string Cluster, int? TipoFormularioID)
        {
            
            
            if (Legajo == 0)
                return AdapterDSReporteAdmin.Calificaiones.GetDataCalificacionesByLegajoPeriodo(null, PeriodoID, Cluster, TipoFormularioID);
            else
                return AdapterDSReporteAdmin.Calificaiones.GetDataCalificacionesByLegajoPeriodo(Legajo, PeriodoID, Cluster, TipoFormularioID);

            
        }

        public DSReporteAdmin.CalificacionesPMPDataTable GetSummaryLegajoPeriodoID(int? Legajo, int PeriodoID)
        {
            if (Legajo == 0)
                return AdapterDSReporteAdmin.Calificaiones.GetDataSummaryByLegajoPeriodo(null, PeriodoID);
            else
                return AdapterDSReporteAdmin.Calificaiones.GetDataSummaryByLegajoPeriodo(Legajo, PeriodoID);
        }

        public DSReporteAdmin.UsuarioDataTable GetDataUsuarios()
        {
            return AdapterDSReporteAdmin.Usuarios.GetData();
        }

        public DSReporteAdmin.PlanAccionDataTable GetPlanByLegajoPeriodo(int? Legajo, int PeriodoID, string Cluster, int? TipoFormularioID)
        {
            return AdapterDSReporteAdmin.Plan.GetPlanByLegajoPeriodo(Legajo, PeriodoID, Cluster, TipoFormularioID);
        }

        public DSReporteAdmin.FortalezasNecesidadesGralDataTable GetFortalezasNecesidadesGral(int PeriodoID, int? Legajo, int SeccionID, int FortalezaID, int DireccionID, string Cluster, int? TipoFormularioID)
        {
            return AdapterDSReporteAdmin.FortalezasNecesidadesGralTableAdapter.GetData(PeriodoID, SeccionID, Legajo, FortalezaID, DireccionID, Cluster, TipoFormularioID);
        }

        public DSReporteAdmin.CarrerPlanDataTable GetCareerPlan(int? Legajo, int PeriodoID, string Cluster, int? TipoFormualrioId)
        {
            return AdapterDSReporteAdmin.CareerPlan.GetCareerPlanByLegajoPeriodoID(Legajo, PeriodoID, Cluster, TipoFormualrioId);
        }

        public DSReporteAdmin.CompetenciasComoFortalezasDataTable GetCompetenciasComoFortalezas(int PeriodoID, int? Legajo, int FortalezaID, int DireccionID, string Cluster, int? TipoFormularioID)
        {
            return AdapterDSReporteAdmin.CompetenciasComoFortalezas.GetData(PeriodoID, Legajo, FortalezaID, DireccionID, Cluster, TipoFormularioID);
        }

        public DSReporteAdmin.CursosDataTable GetCursosByLegajoPeriodoIdDireccionID(int? Legajo, int PeriodoId, int? DireccionID, string Cluster, int? TipoFormularioID)
        {
            return AdapterDSReporteAdmin.Cursos.GetDataCursosByLegajoPeriodoIdDireccionId(Legajo, PeriodoId, DireccionID, Cluster, TipoFormularioID);
        }

        public DSReporteAdmin.Eval_GetReporteObjetivosDataTable GetObjetivosDireccion(int PeriodoID,int? DireccionID,int? AreaID,int? ClusterID)
        {
            return AdapterDSReporteAdmin.Objetivos.GetData(PeriodoID, DireccionID, AreaID, ClusterID);
        }


    }
}
