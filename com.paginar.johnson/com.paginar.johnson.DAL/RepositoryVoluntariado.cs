﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryVoluntariado
    {
        private AdapterDSVoluntariado _AdapterDSVoluntariado;
        public AdapterDSVoluntariado AdapterDSVoluntariado
        {
            get 
            {
                if (_AdapterDSVoluntariado == null) _AdapterDSVoluntariado = new AdapterDSVoluntariado();
                return _AdapterDSVoluntariado;
            }
        }

        #region Evento Galeria

        public int EventoInsert(string Nombre, string Descripcion, DateTime Fecha)
        {
            string id = AdapterDSVoluntariado.VolEvento.vol_eventoInsert(Nombre, Descripcion, Fecha).ToString();
            return int.Parse(id);
        }

        public void EventoUpdate(int IdEvento, string Nombre, string Descripcion, DateTime Fecha)
        {
            AdapterDSVoluntariado.VolEvento.vol_eventoUpdate(IdEvento, Nombre, Descripcion, Fecha);
        }

        public void EventoDelete(int IdEvento)
        {
            AdapterDSVoluntariado.VolEvento.vol_eventoDelete(IdEvento);
        }

        public DSVoluntariado.vol_eventoDataTable EventoSelect(int IdEvento)
        {
            return AdapterDSVoluntariado.VolEvento.vol_eventoSelect(IdEvento);
        }

        public DSVoluntariado.vol_eventoDataTable EventoSelectAll()
        {

            return AdapterDSVoluntariado.VolEvento.GetData();

        }

        public DSVoluntariado.vol_eventoDataTable EventoSelectWImagen()
        {
            return AdapterDSVoluntariado.VolEvento.vol_eventoSelectWImagen();
        }

        #endregion

        #region Imagen Galeria

        public int ImagenInsert(int IdEvento, string Imagen, string Descripcion)
        {
            string IdImagen = AdapterDSVoluntariado.VolImage.vol_eventoImgInsert(IdEvento, Imagen, Descripcion).ToString();
            return int.Parse(IdImagen);

        }

        public void ImagenDelete(int IdImagen)
        {
            AdapterDSVoluntariado.VolImage.vol_eventoImgDelete(IdImagen);
        }

        public void ImagenUpdate(int IdImagen, int IdEvento, string Imagen, string Descripcion)
        {
            AdapterDSVoluntariado.VolImage.vol_eventoImgUpdate(IdImagen, IdEvento, Imagen, Descripcion);
        }

        public DSVoluntariado.vol_eventoImgDataTable ImagenSelect(int IdImagen)
        {
            return AdapterDSVoluntariado.VolImage.vol_eventoImgSelect(IdImagen);
        }

        public DSVoluntariado.vol_eventoImgDataTable ImagenSelectByEvento(int IdEvento)
        {
            return AdapterDSVoluntariado.VolImage.vol_eventoImgSelectByEvento(IdEvento);
        }

        #endregion

        #region Actividad

        public DSVoluntariado.vol_actividadDataTable ActividadSelect(int IdActividad)
        {
            return AdapterDSVoluntariado.VolActividad.ActividadSelect(IdActividad);
        }

        public DSVoluntariado.vol_actividadDataTable ActividadSelectActivas()
        {
            return AdapterDSVoluntariado.VolActividad.ActividadSelectActiva();
        }

        public int ActividadInsert(string Actividad, string Descripcion, string Que, string Cuando, string AQuienes, string Imagen, bool Estado)
        {
            return int.Parse(AdapterDSVoluntariado.VolActividad.ActividadInsert(Actividad, Descripcion, Que, Cuando, AQuienes, Imagen, Estado).ToString());
        }

        public void ActividadUpdate(int IdActividad, string Actividad, string Descripcion, string Que, string Cuando, string AQuienes, string Imagen, bool Estado)
        {
            AdapterDSVoluntariado.VolActividad.ActividadUpdate(IdActividad, Actividad, Descripcion, Que, Cuando, AQuienes, Imagen, Estado);
        }

        public void ActividadDelete(int IdActividad)
        {
            AdapterDSVoluntariado.VolActividad.ActividadDelete(IdActividad);
        }

        public DSVoluntariado.vol_actividadDataTable ActividadSelectAll()
        {
            return AdapterDSVoluntariado.VolActividad.GetData();
        }

        public DSVoluntariado.vol_actividadDataTable ActividadSeleccionarDisponibles(int Legajo)
        {
            return AdapterDSVoluntariado.VolActividad.ActividadSeleccionarDisponibles(Legajo);
        }

        #endregion

        #region Actividad Inscripcion

        public void ActividadInscripcion(int IdActividad, int Legajo, int Cluster, string Comentarios, bool Notificado)
        {
            //public void ActividadInscripcion(int IdActividad, int Legajo, int Cluster, string Comentarios, DateTime FechaInscripcion, bool Notificado)
            AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscribir(IdActividad, Legajo, Cluster, Comentarios, DateTime.Now, Notificado);
        }

        //vol_ActividadInscriptosSelectDos
        //public DSVoluntariado.vol_actividadInscriptosDataTable ActividadInscriptos(int IdActividad)
        //{
        //    return AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscriptosSelect(IdActividad);
        //}
        public DSVoluntariado.vol_actividadInscriptosDataTable ActividadInscriptosDos(int IdActividad)
        {
            return AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscriptosSelectDos(IdActividad);
        }

        //public DSVoluntariado.vol_actividadInscriptosDataTable ActividadInscriptos(int IdActividad)
        //{
        //    return AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscriptosSelectDos(IdActividad);
        //}

        public void ActividadInscriptosDelete(int IdActividad)
        {
            AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscriptosDelete(IdActividad);
        }

        public DSVoluntariado.vol_actividadInscriptosDataTable InscriptosVoluntariados(int IdVol)
        {
            return AdapterDSVoluntariado.VolActividadInscriptos.vol_ActividadInscriptosSelectDos(IdVol);
        }
        #endregion

        #region Cronograma

        public DSVoluntariado.vol_CronogramaDataTable CronogramaSelectAll()
        {
            return AdapterDSVoluntariado.VolCronograma.GetData();
        }

        public DSVoluntariado.vol_CronogramaDataTable CronocramaSelect(int IdCronograma)
        {
            return AdapterDSVoluntariado.VolCronograma.CronogramaSelect(IdCronograma);
        }

        public DSVoluntariado.vol_CronogramaDataTable CronogramaSelectActivos()
        {
            return AdapterDSVoluntariado.VolCronograma.CronogramaSelectActivos();
        }

        public void CronogramaInsert(string Actividad, bool Estado)
        {
            AdapterDSVoluntariado.VolCronograma.CronogramaInsert(Actividad, Estado);
        }

        public void CronogramaUpdate(int IdCronograma, string Actividad, bool Estado)
        {
            AdapterDSVoluntariado.VolCronograma.CronogramaUpdate(IdCronograma, Actividad, Estado);                
        }

        public void CronogramaDelete(int IdCronograma)
        {
            AdapterDSVoluntariado.VolCronograma.CronogramaDelete(IdCronograma);
        }

        #endregion 

        #region Cronograma Fecha

        public DSVoluntariado.vol_CronogramaFechaDataTable CronogramaFechaByCronSelect(int IdCronograma)
        {
            return AdapterDSVoluntariado.VolCronogramaFecha.CronogramaFechaByCronSelect(IdCronograma);
        }

        public DSVoluntariado.vol_CronogramaFechaDataTable CronogramaFechaSelect(int IdCronogramaFecha)
        {
            return AdapterDSVoluntariado.VolCronogramaFecha.CronogramaFechaSelect(IdCronogramaFecha);
        }

        public void CronogramaFechaInsert(int IdCronograma, string Fecha, bool TBD)
        {
            AdapterDSVoluntariado.VolCronogramaFecha.CronogramaFechaInsert(IdCronograma, Fecha, TBD);
        }

        public void CronogramaFechaUpdate(int IdCronogramaFecha, int IdCronograma, string Fecha, bool TBD)
        {
            AdapterDSVoluntariado.VolCronogramaFecha.CronogramaFechaUpdate(IdCronogramaFecha, IdCronograma, Fecha, TBD);
        }

        public void CronogramaFechaDelete(int IdCronogramaFecha)
        {
            AdapterDSVoluntariado.VolCronogramaFecha.CronogramaFechaDelete(IdCronogramaFecha);
        }

        #endregion

        #region reportes
        public DSVoluntariado.vol_ActividadSelectReporteDataTable vol_ActividadSelectReporte(bool todos)
        {
            return AdapterDSVoluntariado.VolReporte.vol_ActividadSelectReporte(todos);
        }
        #endregion
    }
}
