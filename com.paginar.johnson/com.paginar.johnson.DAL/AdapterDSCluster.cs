﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSCluster 
    {


        private DCClusterTableAdapters.ClusterTableAdapter _Cluster;

        public DCClusterTableAdapters.ClusterTableAdapter Cluster
        {
            get
            {
                if (_Cluster == null) _Cluster = new DCClusterTableAdapters.ClusterTableAdapter();
                return _Cluster;

            }
        }


        private DCClusterTableAdapters.RegionPaisTableAdapter _RegionPais;
        public DCClusterTableAdapters.RegionPaisTableAdapter RegionPais
        {
            get
            {
                if (_RegionPais == null) _RegionPais = new DCClusterTableAdapters.RegionPaisTableAdapter();
                return _RegionPais;

            }
        }
    }
}
