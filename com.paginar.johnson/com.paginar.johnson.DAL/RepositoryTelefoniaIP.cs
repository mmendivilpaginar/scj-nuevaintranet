﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryTelefoniaIP
    {
        private AdapterDSTelefoniaIP _AdapterDSTelefoniaIP;
        public AdapterDSTelefoniaIP AdapterDSTelefoniaIP
        { 
            get
            {
                if (_AdapterDSTelefoniaIP == null) _AdapterDSTelefoniaIP = new AdapterDSTelefoniaIP();
                return _AdapterDSTelefoniaIP;
            }
        }

        public DSTelefoniaIP.telefoniaIPDataTable telip_selectAll()
        {
            return AdapterDSTelefoniaIP.telefoniaIP.telip_SelectAll();
        }

        public DSTelefoniaIP.telefoniaIPDataTable telip_SelectByID(int id)
        {
            return AdapterDSTelefoniaIP.telefoniaIP.telip_SelectByID(id);
        }

        public bool teleip_Delete(int id)
        {
            try 
	        {	        
		        AdapterDSTelefoniaIP.telefoniaIP.telip_Delete(id);
                return true;
	        }
	        catch (Exception)
	        {
		
		        return false;
	        }
        }

        public bool teleip_Update(int id, string Ubicacion, string Salida, string Prefijo)
        {
            try 
	        {	        
		        AdapterDSTelefoniaIP.telefoniaIP.telip_Update(id, Ubicacion, Salida, Prefijo);
                return true;
	        }
	        catch (Exception)
	        {
		
		        return false;
	        }
            
        }

        public bool teleip_Insert(string Ubicacion, string Salida, string Prefijo)
        {
            try
            {
                AdapterDSTelefoniaIP.telefoniaIP.telip_Insert(Ubicacion, Salida, Prefijo);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
