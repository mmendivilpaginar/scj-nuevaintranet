﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryIndumentariaReporte
    {
        private AdapterDSIndumentariaReporte _AdapterDSIndumentariaReporte;
        public AdapterDSIndumentariaReporte AdapterDSIndumentariaReporte
        {
            get
            {
                if (_AdapterDSIndumentariaReporte == null) _AdapterDSIndumentariaReporte = new AdapterDSIndumentariaReporte();
                return _AdapterDSIndumentariaReporte;
            }
        }

        public DSIndumentariaReporte.IndumentariaReporteDetalleDataTable getIndumentariaReporteDetalleSelect(int? PeriodoID, string Area, bool? esBrigadista)
        {
            return AdapterDSIndumentariaReporte.IndumentariaReporte.IndumentariaReporteDetalleSelect(PeriodoID, Area, esBrigadista);
        }

        public DSIndumentariaReporte.IndumentariaReporteAreaSinDetalleDataTable getIndumentariaReporteAreaSinDetallesSelect(int? PeriodoID, string Area, bool? esBrigadista)
        {
            return AdapterDSIndumentariaReporte.IndumentariaReporteArea.GetDataIndumentariaReporteAreaSinDetalle(PeriodoID, Area, esBrigadista);
        }

        public DSIndumentariaReporte.IndumentariaReporteSinDetalleDataTable getIndumentariaReporteSinDetalle(int? PeriodoID, bool? esBrigadista)
        {
            return AdapterDSIndumentariaReporte.IndumentariaReporteSinDetalle.GetDataIndumentariaReporteSinDetalle(PeriodoID, esBrigadista);
        }
    }
}
