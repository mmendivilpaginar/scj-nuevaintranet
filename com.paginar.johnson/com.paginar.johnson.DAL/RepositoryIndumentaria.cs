﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryIndumentaria
    {

        private AdapterDSIndumentaria _Indumentaria;
        public AdapterDSIndumentaria Indumentariea
        {
            get
            {
                if (_Indumentaria == null) _Indumentaria = new AdapterDSIndumentaria();
                return _Indumentaria;
            }
        }

        #region Indumentaria

        public DSIndumentaria.IndumentariaMigracionDataTable getPeriodoMigrar(int periodo)
        {
            return Indumentariea.Migracion.GetDataByCodigoMigracion(periodo);
        }

        public DSIndumentaria.IndumentariaDataTable SeleccionarIndumentariaTodas()
        {
            return Indumentariea.Indumentaria.GetData();
        }
        #endregion

        #region Tramite

        public int? tramite_alta(int UsrNro, string Modo, int UsuarioId, string Apellido, string Nombre, int Legajo, string Sector, string Area)
        {
            int? traid = null;
            Indumentariea.Tramite.tramite_alta(UsrNro, 1, Modo, ref traid, 26, UsuarioId, DateTime.Now, 1, DateTime.Now, Apellido, Nombre, Legajo, Sector, Area, null);
            return traid;
        }

        public DSIndumentaria.TramitesNoIniciadosDataTable getUsuariosNoIniciaronTramite(int periodoid)
        {
            return Indumentariea.NoIniciados.GetUsuariosQueNoIniciaronTramite(periodoid);
        }

        public int IndumentariaActualizarPasoTramite(int tra_ID, int PasoActual)
        {
            return Indumentariea.Tramite.tramite_ActualizarPaso(PasoActual, tra_ID);
        }

        public DSIndumentaria.frmTramiteDataTable IndumentariaTramiteActualSeleccionar(int UsuarioId, int PeriodoId)
        {
            return Indumentariea.Tramite.IndumentariaTramiteActualSeleccionar(UsuarioId, PeriodoId);
        }

        public DSIndumentaria.frmTramiteDataTable IndumentariaTramiteSeleccionarPeriodoId(int PeriodoId)
        {
            return Indumentariea.Tramite.IndumentariaTramiteSeleccionarPeriodoId(PeriodoId);
        }
        #endregion

        #region Pedido
        public void IndumentariaPedidoInsertar(int tra_ID, int PeriodoId, int IndumentariaId, string Talle, bool esBrigadista)
        {
            Indumentariea.Pedido.IndumentariaPedidoInsertar(tra_ID, PeriodoId, IndumentariaId, Talle, esBrigadista);
        }

        public void IndumentariaPedidoActualizar(int tra_ID, int IndumentariaId, string Talle, bool esBrigadista)
        {
            Indumentariea.Pedido.IndumentariaPedidoActualizar(tra_ID, IndumentariaId, Talle, esBrigadista);
        }

        public DSIndumentaria.IndumentariaPedidoDataTable IndumentariaPedidoSeleccionar(int tra_ID)
        {
            return Indumentariea.Pedido.IndumentariaPedidoSeleccionar(tra_ID);
        }

        public void IndumentariaPedidoEliminar(int tra_ID)
        {
            Indumentariea.Pedido.IndumentariaPedidoEliminar(tra_ID);
        }
        #endregion

        #region Periodo
        
        public int IndumentariaPeriodoInsertar(DateTime Desde, DateTime Hasta, string Descripcion) 
        {
            return int.Parse(Indumentariea.Periodo.IndumentariaPeriodoInsertar(Desde, Hasta, true, Descripcion).ToString());
        }

        public void IndumentariaPeriodoEditar(DateTime Desde, DateTime Hasta, string Descripcion, int PeriodoId)
        {
            Indumentariea.Periodo.IndumentariaPeriodoEditar(Desde, Hasta, true, Descripcion, PeriodoId);
        }
        public DSIndumentaria.Indumentaria_PeriodoDataTable IndumentariaPeriodoSeleccionarPorId(int PeriodoId)
        {
            return Indumentariea.Periodo.IndumentariaPeriodoSeleccionarPorId(PeriodoId);
        }

        public DSIndumentaria.Indumentaria_PeriodoDataTable SeleccionarPeriodoActual()
        {
            return Indumentariea.Periodo.SeleccionarPeriodoActual(DateTime.Now);
        }

        public DSIndumentaria.Indumentaria_PeriodoDataTable SeleccionarPeriodosTodos()
        {
            return Indumentariea.Periodo.GetData();
        }

        public string GetIdLastPeriodo()
        {
            return Indumentariea.Periodo.GetIdLastPeriodo().ToString();
        }
        #endregion
    }
}
