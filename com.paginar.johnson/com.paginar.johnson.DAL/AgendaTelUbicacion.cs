﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.Web.DynamicData;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_AgendaTelUbicacion))]
    [DisplayName("Agenda Telefónica Ubicación")]
    public partial class AgendaTelUbicacion
    {

        public class MetaData_AgendaTelUbicacion
        {

            //            Descripcion AgendaTelCategorias Cluster 
            [DisplayName("Descripción")]
            public object Descripcion { get; set; }

            [DisplayName("Categorías")]
            public object AgendaTelCategorias { get; set; }


        }
    }
}
