﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryClasificados
    {
        private AdapterDSClasificados _AdapterDSClasificados;

        public AdapterDSClasificados AdapterDSClasificados
        {
            get
            {
                if (_AdapterDSClasificados == null) _AdapterDSClasificados = new AdapterDSClasificados();
                return _AdapterDSClasificados;
            }
        }
    }
}
