﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.Linq;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_Efemerides))]
    public partial class Efemerides
    {
        
        public class MetaData_Efemerides
        {
            //[DataTypeAttribute(DataType.Date)]
            [UIHint("DateTimeAnioMes")]
            [DisplayName("Día")]
            [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM}")]
            public object Dia{ get; set; }

            [DisplayName("Cluster")]
            [DisallowNavigationAttribute(true)]
            public object Cluster1 { get; set; }

            [DisplayName("Descripción")]
            [DisallowNavigationAttribute(true)]
            public object Descripcion { get; set; }
        }
    }
}
