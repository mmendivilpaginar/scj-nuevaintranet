﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSVitrina
    {
        private DSVitrinaTableAdapters.vitra_LinkTableAdapter _vitra_LinkTableAdapter;
        public DSVitrinaTableAdapters.vitra_LinkTableAdapter vitra_LinkTableAdapter
        {
            get
            {
                if (_vitra_LinkTableAdapter == null)
                    _vitra_LinkTableAdapter = new DSVitrinaTableAdapters.vitra_LinkTableAdapter();
                return _vitra_LinkTableAdapter;
            }
        }



        private DSVitrinaTableAdapters.vitra_PublicacionTableAdapter _vitra_PublicacionTableAdapter;
        public DSVitrinaTableAdapters.vitra_PublicacionTableAdapter vitra_PublicacionTableAdapter
        {
            get
            {
                if (_vitra_PublicacionTableAdapter == null)
                    _vitra_PublicacionTableAdapter = new DSVitrinaTableAdapters.vitra_PublicacionTableAdapter();
                return _vitra_PublicacionTableAdapter;
            }
        }



        private DSVitrinaTableAdapters.vitra_TipoTableAdapter _vitra_TipoTableAdapter;
        public DSVitrinaTableAdapters.vitra_TipoTableAdapter vitra_TipoTableAdapter
        {
            get
            {
                if (_vitra_TipoTableAdapter == null)
                    _vitra_TipoTableAdapter = new DSVitrinaTableAdapters.vitra_TipoTableAdapter();
                return _vitra_TipoTableAdapter;
            }
        }

        private DSVitrinaTableAdapters.vitra_BuscadorTableAdapter _vitra_Buscador;
        public DSVitrinaTableAdapters.vitra_BuscadorTableAdapter vitra_Buscador
        {
           get
            {
                if (_vitra_Buscador == null)
                    _vitra_Buscador = new DSVitrinaTableAdapters.vitra_BuscadorTableAdapter();
                return _vitra_Buscador;
            }
        }





    }
}
