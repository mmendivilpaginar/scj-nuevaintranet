﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSVoluntariado
    {
        private DSVoluntariadoTableAdapters.vol_eventoTableAdapter _VolEvento;
        public DSVoluntariadoTableAdapters.vol_eventoTableAdapter VolEvento
        {
            get
            {
                if (_VolEvento == null) _VolEvento = new DSVoluntariadoTableAdapters.vol_eventoTableAdapter();
                return _VolEvento;
            }
        }

        private DSVoluntariadoTableAdapters.vol_eventoImgTableAdapter _VolImage;
        public DSVoluntariadoTableAdapters.vol_eventoImgTableAdapter VolImage
        {
            get 
            {
                if (_VolImage == null) _VolImage = new DSVoluntariadoTableAdapters.vol_eventoImgTableAdapter();
                return _VolImage;
            }
        }

        private DSVoluntariadoTableAdapters.vol_actividadTableAdapter _VolActividad;
        public DSVoluntariadoTableAdapters.vol_actividadTableAdapter VolActividad
        {
            get 
            { 
                if(_VolActividad == null) _VolActividad = new DSVoluntariadoTableAdapters.vol_actividadTableAdapter();
                return _VolActividad;
            }
        }

        private DSVoluntariadoTableAdapters.vol_actividadInscriptosTableAdapter _VolActividadInscriptos;
        public DSVoluntariadoTableAdapters.vol_actividadInscriptosTableAdapter VolActividadInscriptos
        {
            get
            {
                if (_VolActividadInscriptos == null) _VolActividadInscriptos = new DSVoluntariadoTableAdapters.vol_actividadInscriptosTableAdapter();
                return _VolActividadInscriptos;
            }
        }

        private DSVoluntariadoTableAdapters.vol_CronogramaTableAdapter _VolCronograma;
        public DSVoluntariadoTableAdapters.vol_CronogramaTableAdapter VolCronograma
        {
            get
            {
                if (_VolCronograma == null) _VolCronograma = new DSVoluntariadoTableAdapters.vol_CronogramaTableAdapter();
                return _VolCronograma;
            }
        }

        private DSVoluntariadoTableAdapters.vol_CronogramaFechaTableAdapter _VolCronogramaFecha;
        public DSVoluntariadoTableAdapters.vol_CronogramaFechaTableAdapter VolCronogramaFecha
        {
            get
            {
                if (_VolCronogramaFecha == null) _VolCronogramaFecha = new DSVoluntariadoTableAdapters.vol_CronogramaFechaTableAdapter();
                return _VolCronogramaFecha;
            }
        }

        private DSVoluntariadoTableAdapters.vol_ActividadSelectReporteTableAdapter _VolReporte;
        public DSVoluntariadoTableAdapters.vol_ActividadSelectReporteTableAdapter VolReporte
        {
            get
            {
                if (_VolReporte == null) _VolReporte = new DSVoluntariadoTableAdapters.vol_ActividadSelectReporteTableAdapter();
                return _VolReporte;
            }
        }
    }
}
