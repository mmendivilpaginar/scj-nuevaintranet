﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    
    public class RepositoryContenido
    {
        private AdapterDSContenido _AdaptersDSContenido;

        public AdapterDSContenido AdaptersDSContenido
        {
            get
            {
                if (_AdaptersDSContenido == null) _AdaptersDSContenido = new AdapterDSContenido();
                return _AdaptersDSContenido;
            }
          
        }

        public DsContenido.Content_ItemsDataTable GetEstructuraById(int? IdItem)
        {
            return AdaptersDSContenido.ItemContenido.GetDataByEstructura(IdItem);
        }

        public DsContenido.Content_ItemsDataTable GetContentById(int? IdItem)
        {
            return AdaptersDSContenido.ItemContenido.Content_ItemsGetContent(IdItem);
        }

        public void fillData(ref DsContenido Tablas)
        {
            AdaptersDSContenido.ItemContenido.Fill(Tablas.Content_Items);
            
        }

        public void UpdateContent_Items(int Content_ItemId,string Titulo,string Contenido,int? parentId,int? Content_TipoID)
        {
            AdaptersDSContenido.ItemContenido.Content_ItemsUpdateNode(Content_ItemId, Titulo, Contenido, parentId, Content_TipoID);

        }

        public int InsertContent_Items(string Titulo, string Contenido, int? parentId, int? Content_TipoID)
        {
            return int.Parse(AdaptersDSContenido.ItemContenido.Content_ItemsAddNode(Titulo, Contenido, parentId, Content_TipoID).ToString());
           
        }

        public void DeleteContent_Items(int? Content_ItemId)
        {
            AdaptersDSContenido.ItemContenido.Content_ItemsDeleteNode(Content_ItemId);

        }

        public void Content_ItemsByClusterDelete(int? content_itemid, int? clusterid)
        {
            AdaptersDSContenido.ContenidoInCluster.Content_ItemsByClusterDelete(content_itemid, clusterid);
        }

        public void Content_ItemsByClusterAdd(int? content_itemid, int? clusterid)
        {
            AdaptersDSContenido.ContenidoInCluster.Content_ItemsByClusterAdd(content_itemid, clusterid);
        }

        public void Content_ItemsMenuUrlToolTipAdd(int Content_ItemId, string navigateurl, string @tooltip)
        {
            AdaptersDSContenido.Content_ItemsMenuUrlToolTip.Add(Content_ItemId, navigateurl, tooltip);
        }

        public void Content_ItemsUrlToolTipDelete(int Content_ItemId)
        {
            AdaptersDSContenido.Content_ItemsMenuUrlToolTip.DeleteByID(Content_ItemId);
        }


        public  DsContenido.Content_ItemsMenuUrlToolTipDataTable  Content_ItemsMenuUrlToolTipGet(int Content_ItemId)
        {
            return AdaptersDSContenido.Content_ItemsMenuUrlToolTip.GetDataByID(Content_ItemId);
        }

        public void Content_ItemsMenuUrlToolTipUpdate(int Content_ItemId, string navigateurl, string @tooltip)
        {
            AdaptersDSContenido.Content_ItemsMenuUrlToolTip.Update1(Content_ItemId, navigateurl, tooltip);
        }


        public DsContenido.Content_ItemsDataTable Content_ItemsGetVisible(string titulo, int clusterid)
        {
            return AdaptersDSContenido.ItemContenido.Content_ItemsGetVisible(titulo, clusterid);
        }

        public DsContenido.Content_ItemsDataTable Content_ItemsGetMenu(int @Content_ItemId, int clusterid)
        {
            return AdaptersDSContenido.ItemContenido.Content_ItemsGetMenu(Content_ItemId, clusterid);
        }

        public DsContenido.Content_TipoDataTable Content_TipoGetIdrootByClusterId(string nombre,int clusterid)
        {
            return AdaptersDSContenido.TipoContenido.GetIdrootByClusterId(nombre, clusterid);
        }

        public DsContenido.Content_ItemsByClusterDataTable Content_ItemsGetClustersByID(int ItemId)
        {
            return AdaptersDSContenido.ContenidoInCluster.GetDataByContent_ItemsGetClustersByID(ItemId);           
        }
        public void Content_ItemsByItemsDelete(int ItemId)
        {
            AdaptersDSContenido.ContenidoInCluster.Content_ItemsByItemsDelete(ItemId);
        }
 
    }
}
