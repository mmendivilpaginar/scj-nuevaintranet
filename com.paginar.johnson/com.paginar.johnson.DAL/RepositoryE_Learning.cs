﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
   public class RepositoryE_Learning
    {
        private AdapterDSE_Learning _AdapterDSE_Learning;
        public AdapterDSE_Learning AdapterDSE_Learning
        {
            get
            {
                if (_AdapterDSE_Learning == null) _AdapterDSE_Learning = new AdapterDSE_Learning();
                return _AdapterDSE_Learning;
            }
        }

        public  DSE_Learning.EL_CursosDataTable GetAllCursos()
        {
            return AdapterDSE_Learning.EL_Cursos.GetAllCursos();
        }

        public DSE_Learning.EL_CursosDataTable GetCursosOperarios()
        {
            return AdapterDSE_Learning.EL_Cursos.GetCursosOperarios();
        }

        public DSE_Learning.EL_CursosDataTable GetCursosAdministrativos()
        {
            return AdapterDSE_Learning.EL_Cursos.GetCursosAdministrativos();
        }
        public DSE_Learning.EL_CursosDataTable GetCursosByDescripcionCodigo( string Descripcion, string Codigo)
        {
            return AdapterDSE_Learning.EL_Cursos.GetDataByDescripcionCodigo(Descripcion,Codigo);
        }


        public DSE_Learning.EL_CursosDataTable GetCursoByID(int CursoID)
        {
           return AdapterDSE_Learning.EL_Cursos.GetDataByID(CursoID);

        }

        public string ExisteCodigo(string Codigo, int? CursoID)
        {
            return (string)AdapterDSE_Learning.EL_Cursos.ExisteCodigo(Codigo, CursoID);

        }      


    }
}
