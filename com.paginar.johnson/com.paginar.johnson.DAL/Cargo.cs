﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Data.Linq;

namespace com.paginar.johnson.DAL
{

    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_Cargo))]
    [DisplayName("Cargos")]
    partial class Cargo
    {
        partial void OnValidate(ChangeAction action)
        {
            if (action == ChangeAction.Delete)
            {
                var DC = new SCJBODataContext();

                if (DC.UsuarioBOs.Where(c => c.CargoID == this.CargoID).ToList().Count > 0)
                {
                    throw new ValidationException("No podrá eliminar el cargo " + this.CargoDET + ", debido a que posee un usuario relacionado. Por Favor, reasigne el/los usuarios al Cargo que corresponda.");
                }
            }
        }

        public class MetaData_Cargo
        {
           

            [DisplayName("Descripción")]
            [FilterUIHint("Search")]
            public object CargoDET { get; set; }

            [ScaffoldColumnAttribute(false)]
            public EntitySet<UsuarioBO> UsuarioBOs { get; set; }
        }

    }
}
