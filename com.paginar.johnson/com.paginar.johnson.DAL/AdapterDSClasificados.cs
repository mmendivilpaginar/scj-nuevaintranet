﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSClasificados
    {
        private DSClasificadosTableAdapters.clasiAvisoTableAdapter _clasiAviso;

        public DSClasificadosTableAdapters.clasiAvisoTableAdapter clasiAviso
        {
            get
            {
                if (_clasiAviso == null) _clasiAviso = new DSClasificadosTableAdapters.clasiAvisoTableAdapter();
                return _clasiAviso;

            }
        }


        private DSClasificadosTableAdapters.clasiRubroTableAdapter _clasiRubro;

        public DSClasificadosTableAdapters.clasiRubroTableAdapter clasiRubro
        {
            get
            {
                if (_clasiRubro == null) _clasiRubro = new DSClasificadosTableAdapters.clasiRubroTableAdapter();
                return _clasiRubro;

            }
        }


        private DSClasificadosTableAdapters.claAvisoComentariosTableAdapter _claAvisoComentarios;

        public DSClasificadosTableAdapters.claAvisoComentariosTableAdapter claAvisoComentarios
        {
            get
            {
                if (_claAvisoComentarios == null) _claAvisoComentarios = new DSClasificadosTableAdapters.claAvisoComentariosTableAdapter();
                return _claAvisoComentarios;

            }
        }


        private DSClasificadosTableAdapters.UsuarioTableAdapter _Usuario;

        public DSClasificadosTableAdapters.UsuarioTableAdapter Usuario
        {
            get
            {
                if (_Usuario == null) _Usuario = new DSClasificadosTableAdapters.UsuarioTableAdapter();
                return _Usuario;

            }
        }
    }
}
