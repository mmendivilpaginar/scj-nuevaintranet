﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryVisor4
    {
        private AdapterDSVisor4 _AdapterVisor4;

        public AdapterDSVisor4 AdapterDSVisor4
        {
            get
            {
                if (_AdapterVisor4 == null) _AdapterVisor4 = new AdapterDSVisor4();
                return _AdapterVisor4;
            }
        }


        public Visor4_DataSet.Visor4_GetRetencionesDataTable GetRetenciones(int Legajo, int Mes, int Anio)
        {
            return AdapterDSVisor4.Resultado.GetData(Legajo, Mes, Anio);
        }

        public Visor4_DataSet.Visor4_GetAniosLiquidadosDataTable GetAniosLiquidados(int Legajo)
        {
            return AdapterDSVisor4.Anios.GetData(Legajo);
        }

        public Visor4_DataSet.Visor4_GetMesUltimaLiquidacionDataTable GetMesUltimaLiquidacion(int Legajo)
        {
            return AdapterDSVisor4.Mes.GetData(Legajo);
        }
    }
}
