﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryPda
    {
        private AdapterDSPda _AdapterDSPda;
        public AdapterDSPda AdapterDSPda
        {
            get
            {
                if (_AdapterDSPda == null) _AdapterDSPda = new AdapterDSPda();
                return _AdapterDSPda;
            }
        }


        #region Curso

        public DSPda.pda_cursoDataTable pda_cursoSelectByIdReporte(string idCurso)
        {
            return AdapterDSPda.Curso.pda_cursoSelectByIdReporte(idCurso);
        }

        public DSPda.pda_cursoDataTable pda_cursoSelectAll()
        {
            return AdapterDSPda.Curso.pda_cursoSelectAll();
        }

        public DSPda.pda_cursoDataTable pda_cursoSelectByID(int CursoId)
        {
            return AdapterDSPda.Curso.pda_cursoSelectById(CursoId);
        }

        public void pda_cursoInsert(string Nombre, int idEstado, string Copete, string Descripcion, string Duracion, string PeriodoTentativo)
        {
            AdapterDSPda.Curso.pda_cursoInsert(Nombre, idEstado, Copete, Descripcion, Duracion, PeriodoTentativo);
        }

        public void pda_cursoUpdate(int idCurso, string Nombre, int idEstado, string Copete, string Descripcion, string Duracion, string PeriodoTentativo)
        {
            AdapterDSPda.Curso.pda_cursoUpdate(idCurso, Nombre, idEstado, Copete, Descripcion, Duracion, PeriodoTentativo);
        }

        public bool pda_cursoDelete(int idCurso)
        {
            bool valorRetorno = false;
            if (AdapterDSPda.Inscripto.pda_InscriptoSelectByCurso(idCurso).Count == 0)
            {
                AdapterDSPda.Curso.pda_cursoDelete(idCurso);
                valorRetorno = true;
            }
            return valorRetorno;
        }

        public DSPda.pda_cursoDataTable pda_cursoSelectDisponibles(int idUsuario)
        {
            return AdapterDSPda.Curso.pda_cursoSelectDisponibles(idUsuario);
        }


        public DSPda.pda_cursoDataTable pda_cursoSelectInscriptoByUsuario(int idUsuario)
        {
            return AdapterDSPda.Curso.pda_cursoSelectInscriptoByUsuario(idUsuario);
        }
        #endregion

        #region Inscripto
        public DSPda.pda_inscriptoDataTable pda_InscriptoSelectAll()
        {
            return AdapterDSPda.Inscripto.pda_InscriptoSelectAll();
        }

        public DSPda.pda_inscriptoDataTable pda_InscriptoByCurso(int idCurso)
        {
            return AdapterDSPda.Inscripto.pda_InscriptoSelectByCurso(idCurso);
        }

        public DSPda.pda_inscriptoDataTable pda_InscriptoByUsuario(int idUsuario)
        {
            return AdapterDSPda.Inscripto.pda_InscriptoSelectByUsuario(idUsuario);
        }

        public void pda_InscriptoInsert(int idCurso, int idUsuario, int Legajo, int Cluster, string Comentarios, DateTime FechaInscripcion, bool Notificado)
        {
            AdapterDSPda.Inscripto.pda_InscriptoInsert(idCurso, idUsuario, Legajo, Cluster, Comentarios, FechaInscripcion, Notificado);
        }

        public void pda_InscriptoDelete(int idInscripcion)
        {
            AdapterDSPda.Inscripto.pda_InscriptoDelete(idInscripcion);
        }
        #endregion

        #region Estado
        public DSPda.pda_estadoDataTable pda_estados()
        {
            return AdapterDSPda.Estado.GetData();
        }
        #endregion

        #region Reportes

        public DSPda.pda_ReporteInscripcionPorCursoDataTable getReporteInscripcionPorCurso(int? idCurso, int? idEstado, int? anio)
        {
            return AdapterDSPda.ReporteInscripcionPorCurso.pda_ReporteInscriptosByCursoId(idCurso, idEstado, anio);
        }

        public DSPda.pda_DetalleReporteAreaDireccionDataTable getDetalleReportePorAreaDireccionCurso(int AreaID, int DireccionID, int idCurso)
        {
            return AdapterDSPda.DetalleReporteAreaDireccion.pda_DetalleReporteAreaDireccion(AreaID, DireccionID, idCurso);
        }

        public DSPda.pda_ReporteAreaDireccionDataTable getReporteAreaDireccion(int AreaID, int DireccionID, int idCurso, int anio)
        {
            return AdapterDSPda.ReporteAreaDireccion.pda_ReporteAreaDireccion(AreaID, DireccionID, idCurso, anio);
        }

        public DSPda.pda_ReportePorUsuarioDataTable getReporteUsuarios(int? Legajo, string Apellido, int anio)
        {
            return AdapterDSPda.ReporteporUsuarios.pda_ReportePorUsuario(Legajo, Apellido, anio);
        }

        #endregion

        #region Filtros

        public DSPda.DireccionDataTable getDirecciones()
        {
            return AdapterDSPda.Direcciones.pda_getDirecciones();
                 
        }

        public DSPda.AreasDataTable getAreasByDireccionId(int? Dirid)
        {
            return AdapterDSPda.Areas.pda_getAreaByDireccionId(Dirid);
        }

        #endregion

    }
}
