﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositorySeguridad
    {
        private AdapterDSSeguridad _AdapterDSSeguridad;
        public AdapterDSSeguridad AdapterDSSeguridad
        {
            get
            {
                if (_AdapterDSSeguridad == null) _AdapterDSSeguridad = new AdapterDSSeguridad();
                return _AdapterDSSeguridad;
            }

        }

        public DSSeguridad.LoginDataTable getByUserAD(string UserAD)
        {
            return AdapterDSSeguridad.Login.GetByUserAD(UserAD);
        }

        //getLoginByUserName
        public DSSeguridad.LoginDataTable getByUserName(string UserName)
        {
             return AdapterDSSeguridad.Login.GetByUserName(UserName);
        }

    }
}
