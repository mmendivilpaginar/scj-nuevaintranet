﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSSeguridad: IDisposable
    {
        DSSeguridadTableAdapters.LoginTableAdapter _Login;
        public DSSeguridadTableAdapters.LoginTableAdapter Login
        {
            get
            {
                if (_Login == null) _Login = new DSSeguridadTableAdapters.LoginTableAdapter();
                return _Login;
            }
        }


        DSSeguridadTableAdapters.UsuarioTableAdapter _Usuario;
        DSSeguridadTableAdapters.UsuarioTableAdapter Usuario
        {
            get
            {
                if (_Usuario == null) _Usuario = new DSSeguridadTableAdapters.UsuarioTableAdapter();
                return _Usuario;
            }
        }


        DSSeguridadTableAdapters.frmRelUsuarioFormularioTableAdapter _frmRelUsuarioFormulario;

        DSSeguridadTableAdapters.frmRelUsuarioFormularioTableAdapter frmRelUsuarioFormulario
        {
            get
            {
                if (_frmRelUsuarioFormulario == null) _frmRelUsuarioFormulario = new DSSeguridadTableAdapters.frmRelUsuarioFormularioTableAdapter();
                return _frmRelUsuarioFormulario;
            }
        }


        DSSeguridadTableAdapters.RelLoginGrupoTableAdapter _RelLoginGrupoTableAdapter;

        DSSeguridadTableAdapters.RelLoginGrupoTableAdapter RelLoginGrupoTableAdapter
        {
            get
            {
                if (_RelLoginGrupoTableAdapter == null) _RelLoginGrupoTableAdapter = new DSSeguridadTableAdapters.RelLoginGrupoTableAdapter();
                return _RelLoginGrupoTableAdapter;
            }
        }


        DSSeguridadTableAdapters.LogTableAdapter _LogTableAdapter;
        public DSSeguridadTableAdapters.LogTableAdapter LogTableAdapter
        {
            get{
                if (_LogTableAdapter == null) _LogTableAdapter = new DSSeguridadTableAdapters.LogTableAdapter();
                return _LogTableAdapter;
            }
        }



        public void Dispose()
        {
            this._Login = null;
           
        }
    }
}
