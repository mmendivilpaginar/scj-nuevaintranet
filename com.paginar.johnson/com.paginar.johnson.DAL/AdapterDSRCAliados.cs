﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSRCAliados
    {
        private DSRCAliadosTableAdapters.com_AliadoTableAdapter _Aliados;
        public DSRCAliadosTableAdapters.com_AliadoTableAdapter Aliados
        {
            get
            {
                if (_Aliados == null) _Aliados = new DSRCAliadosTableAdapters.com_AliadoTableAdapter();
                return _Aliados;
            }
        }
    }
}
