﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryDSComunidad
    {
        private AdapterDSComunidad _AdapterDSComunidad;
        public AdapterDSComunidad AdapterDSComunidad
        {
            get
            {
                if (_AdapterDSComunidad == null) _AdapterDSComunidad = new AdapterDSComunidad();
                return _AdapterDSComunidad;
            }
        }

        public DSComunidad.com_sumarcomunidadDataTable com_selectVoluntariadosByMes(string pMes,int tipo)
        {
            return AdapterDSComunidad.Comunidad.GetDataBy(pMes, tipo);
        }

        public DSComunidad.com_sumarcomunidadDataTable com_selectVoluntariadosById(int Id)
        {
            return AdapterDSComunidad.Comunidad.GetData(Id);
        }

    }
}
