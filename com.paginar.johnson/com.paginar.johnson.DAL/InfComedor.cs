﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.Web.DynamicData;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_infComedor))]
    [DisplayName("Comedor")]
 
    public partial class infComedor
    {
        public class MetaData_infComedor
        {
            //[ScaffoldColumn(false)]
            //public object FHMod { get; set; }
            //[ScaffoldColumn(false)]
            [Display(Order = 1)]
            [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
            public object Fecha { get; set; }
           
            
            [DisallowNavigationAttribute(true)]
            [DependeeColumn("Cluster1")]
            //[UIHint("Ubicacion")]
            [DisplayName("Ubicación")]
            [ColumnOrder(2)]
            public object Ubicacion { get; set; }

            [DisplayName("Cluster")]
            //[UIHint("Cluster")]
            [DisallowNavigationAttribute(true)]
            [ColumnOrder(1)]
            public object Cluster1 { get; set; }

            [DataTypeAttribute(DataType.MultilineText)]
            public object Entrada { get; set; }

            [DataTypeAttribute(DataType.MultilineText)]
            [DisplayName("Plato Principal")]
            public object PPrincipal { get; set; }             
            [DataTypeAttribute(DataType.MultilineText)]
            [ScaffoldColumn(false)]
            public object Salsas { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            [DisplayName("Guarnición")]
            [ScaffoldColumn(false)]
            public object Guarnicion { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Cena { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]          
            public object Postre { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Express { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Sandwiches { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Vegetariano { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Dieta { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Oficina { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            [Required(AllowEmptyStrings= false)         ]

            public object Parrillas { get; set; }
            [DataTypeAttribute(DataType.MultilineText)]
            public object Pastas { get; set; }
          
        }
    }
}
