﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryUsuarios
    {
        private AdapterDSUsuarios _AdapterDSUsuarios;
        public AdapterDSUsuarios AdapterDSUsuarios
        {
            get
            {
                if (_AdapterDSUsuarios == null) _AdapterDSUsuarios = new AdapterDSUsuarios();
                return _AdapterDSUsuarios;
            }

        }
    }
}
