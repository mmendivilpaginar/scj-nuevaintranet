﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSNoticias : IDisposable
    {
        DSNoticiasTableAdapters.infInfoTableAdapter _infInfo;
        public DSNoticiasTableAdapters.infInfoTableAdapter infInfo
        {
            get
            {
                if (_infInfo == null) _infInfo = new DSNoticiasTableAdapters.infInfoTableAdapter();
                return _infInfo;
            }
        }

        DSNoticiasTableAdapters.infCategoriaTableAdapter _infCategoria;
        public DSNoticiasTableAdapters.infCategoriaTableAdapter infCategoria
        {
            get
            {
                if (_infCategoria == null) _infCategoria = new DSNoticiasTableAdapters.infCategoriaTableAdapter();
                return _infCategoria;
            }
        }

        DSNoticiasTableAdapters.infImagenTableAdapter _infImagen;
        public DSNoticiasTableAdapters.infImagenTableAdapter infImagen
        {
            get
            {
                if (_infImagen == null) _infImagen = new DSNoticiasTableAdapters.infImagenTableAdapter();
                return _infImagen;
            }
        }


        DSNoticiasTableAdapters.infLinkTableAdapter _infLink;
        public DSNoticiasTableAdapters.infLinkTableAdapter infLink
        {
            get
            {
                if (_infLink == null) _infLink = new DSNoticiasTableAdapters.infLinkTableAdapter();
                return _infLink;
            }
        }



        DSNoticiasTableAdapters.InfoUsuarioVotacionTableAdapter _infoUsuarioVotacion;

        public DSNoticiasTableAdapters.InfoUsuarioVotacionTableAdapter infoUsuarioVotacion
        {
            get
            {
                if (_infoUsuarioVotacion == null) _infoUsuarioVotacion = new DSNoticiasTableAdapters.InfoUsuarioVotacionTableAdapter();
                return _infoUsuarioVotacion;
            }
        }

        DSNoticiasTableAdapters.RelInfoClusterTableAdapter _RelInfoCluster;

        public DSNoticiasTableAdapters.RelInfoClusterTableAdapter RelInfoCluster
        {
            get
            {
                if (_RelInfoCluster == null) _RelInfoCluster = new DSNoticiasTableAdapters.RelInfoClusterTableAdapter();
                return _RelInfoCluster;
            }
        }

        DSNoticiasTableAdapters.InfoUsuarioComentariosTableAdapter _infoUsuarioComentarios;

        public DSNoticiasTableAdapters.InfoUsuarioComentariosTableAdapter infoUsuarioComentarios
        {
            get
            {
                if (_infoUsuarioComentarios == null) _infoUsuarioComentarios = new DSNoticiasTableAdapters.InfoUsuarioComentariosTableAdapter();
                return _infoUsuarioComentarios;
            }
        }

        private DSNoticiasTableAdapters.infoBannerTableAdapter _infoBanner;

        public DSNoticiasTableAdapters.infoBannerTableAdapter infoBanner
        {
            get
            {
                if (_infoBanner == null) _infoBanner = new DSNoticiasTableAdapters.infoBannerTableAdapter();
                return _infoBanner;
            }   
        }

        private DSNoticiasTableAdapters.infoBannerClusterTableAdapter _InfoBannerCluster;

        public DSNoticiasTableAdapters.infoBannerClusterTableAdapter InfoBannerCluster
        {
            get
            {
                if (_InfoBannerCluster == null) _InfoBannerCluster = new DSNoticiasTableAdapters.infoBannerClusterTableAdapter();
                return _InfoBannerCluster;
            }   
        }

        public DSNoticiasTableAdapters.infRecordatoriosTableAdapter _infRecordatorio;

        public DSNoticiasTableAdapters.infRecordatoriosTableAdapter infRecordatorio
        {
            get
            {
                if (_infRecordatorio == null) _infRecordatorio = new DSNoticiasTableAdapters.infRecordatoriosTableAdapter();
                return _infRecordatorio;
            }

        }
        DSNoticiasTableAdapters.GetInfoClusterPagTableAdapter _GetInfoClusterPag;
        public DSNoticiasTableAdapters.GetInfoClusterPagTableAdapter GetInfoClusterPag
        {
            get
            {
                if (_GetInfoClusterPag == null) _GetInfoClusterPag = new DSNoticiasTableAdapters.GetInfoClusterPagTableAdapter();
                return _GetInfoClusterPag;
            }

        }


        DSNoticiasTableAdapters.infTagsTableAdapter _infTags;

        public DSNoticiasTableAdapters.infTagsTableAdapter infTags
        {
            get
            {
                if (_infTags == null) _infTags = new DSNoticiasTableAdapters.infTagsTableAdapter();
                return _infTags;
            }
        }

        DSNoticiasTableAdapters.infRelInfoTagsTableAdapter _infRelInfoTags;

        public DSNoticiasTableAdapters.infRelInfoTagsTableAdapter infRelInfoTags
        {
            get
            {
                if (_infRelInfoTags == null) _infRelInfoTags = new DSNoticiasTableAdapters.infRelInfoTagsTableAdapter();
                return _infRelInfoTags;
            }
        }

        public void Dispose()
        {
            this._infInfo = null;
            this._infImagen = null;
            this._infLink = null;
            this._infoUsuarioVotacion = null;
            this._infoUsuarioComentarios = null;
            this._infoBanner = null;
            this._RelInfoCluster = null;
            this._infRecordatorio = null;
        }
    }
}
