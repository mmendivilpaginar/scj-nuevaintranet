﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryNewsLetter
    {

        private AdapterDSNewsletter _AdapterDSNewsletter;

        public AdapterDSNewsletter AdapterDSNewsletter
        {
            get
            {
                if (_AdapterDSNewsletter == null)
                    _AdapterDSNewsletter = new AdapterDSNewsletter();
                return _AdapterDSNewsletter;
            }
        }


    }
}
