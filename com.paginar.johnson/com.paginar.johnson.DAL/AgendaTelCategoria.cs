﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Microsoft.Web.DynamicData;

namespace com.paginar.johnson.DAL
{

    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_AgendaTelCategoria))]
    [DisplayName("Agenda Telefónica Categorías")]

    public partial class AgendaTelCategoria
    {



        public class MetaData_AgendaTelCategoria
        {
            [DisplayName("Descripción")]
            public object Descripcion { get; set; }

            [DisplayName("Agenda")]
            public object AgendaTels { get; set; }

            [DisplayName("Ubicación")]
            public object AgendaTelUbicacion { get; set; }
        }
        



    }
}


