﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSComunidad
    {
        private DSComunidadTableAdapters.com_sumarcomunidadTableAdapter _Comunidad;
        public DSComunidadTableAdapters.com_sumarcomunidadTableAdapter Comunidad
        {
            get 
            {
                if (_Comunidad == null) _Comunidad = new DSComunidadTableAdapters.com_sumarcomunidadTableAdapter();
                return _Comunidad;
            }
        }
    }
}
