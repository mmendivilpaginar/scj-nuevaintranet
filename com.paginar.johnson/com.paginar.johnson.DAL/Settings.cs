﻿using System.Configuration;

namespace com.paginar.johnson.DAL.Properties {
    
    
    // Esta clase le permite controlar eventos específicos en la clase de configuración:
    //  El evento SettingChanging se desencadena antes de cambiar un valor de configuración.
    //  El evento PropertyChanged se desencadena después de cambiar el valor de configuración.
    //  El evento SettingsLoaded se desencadena después de cargar los valores de configuración.
    //  El evento SettingsSaving se desencadena antes de guardar los valores de configuración.
    internal sealed partial class Settings {
        
        public Settings() {
            // // Para agregar los controladores de eventos para guardar y cambiar la configuración, quite la marca de comentario de las líneas:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
            this.SettingsLoaded += new System.Configuration.SettingsLoadedEventHandler(Settings_SettingsLoaded);
        }

        void Settings_SettingsLoaded(object sender, System.Configuration.SettingsLoadedEventArgs e)
        {
            //throw new System.Exception("The method or operation is not implemented.");
            string cadenaconexion = string.Empty;
            switch (System.Web.HttpContext.Current.Request.Url.Host)
            {
                case "190.221.11.154":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringQA_PNET"];
                    break;
                case "hrdev.scj.com":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringDev"];
                    break;
                case "intranetconosur-qa.scj.com":
                    cadenaconexion =  ConfigurationSettings.AppSettings["connectionStringQA"];
                    break;
                case "hr.scj.com":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringPro"];
                    break;
                case "intranetconosur.scj.com":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringPro"];
                    break;
                case "localhost":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringLocal"];
                    break;
                case "24935-scj-nuevaintranet.desa3.paginar.org":
                case "24935-scj-nuevaintranet.desa-scj.paginar.org":
                case "24935-scj-nuevaintranet-qa.aprodez0.paginar.org":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringLocal"];
                    break;
                default:
                    cadenaconexion = ConfigurationSettings.AppSettings["FormulariosconnectionStringDev"];
                    break;
            }
            try
            {
                this["FormulariosConnectionString"] = cadenaconexion;
            }
            catch (System.Exception err)
            {

            }
            //                this["HR_SCJConoSurConnectionString"] = ConfigurationManager.ConnectionStrings["IFLOW_Release"].ConnectionString;

        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Agregar código para administrar aquí el evento SettingChangingEvent.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Agregar código para administrar aquí el evento SettingsSaving.
        }
    }
}
