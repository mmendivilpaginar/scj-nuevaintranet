﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSVisor4
    {

        private Visor4_DataSetTableAdapters.Visor4_GetRetencionesTableAdapter _Resultado;

        public Visor4_DataSetTableAdapters.Visor4_GetRetencionesTableAdapter Resultado
        {
            get
            {
                if (_Resultado == null) _Resultado = new Visor4_DataSetTableAdapters.Visor4_GetRetencionesTableAdapter();
                return _Resultado;
            }
        }


        private Visor4_DataSetTableAdapters.Visor4_GetAniosLiquidadosTableAdapter _Anios;
        
        public Visor4_DataSetTableAdapters.Visor4_GetAniosLiquidadosTableAdapter Anios
        {
            get
            {
                if (_Anios == null) _Anios = new Visor4_DataSetTableAdapters.Visor4_GetAniosLiquidadosTableAdapter();
                return _Anios;
            }
        }


        private Visor4_DataSetTableAdapters.Visor4_GetMesUltimaLiquidacionTableAdapter _Mes;

        public Visor4_DataSetTableAdapters.Visor4_GetMesUltimaLiquidacionTableAdapter Mes
        {
            get
            {
                if (_Mes == null) _Mes = new Visor4_DataSetTableAdapters.Visor4_GetMesUltimaLiquidacionTableAdapter();
                return _Mes;
            }
        }
        
    }
}
