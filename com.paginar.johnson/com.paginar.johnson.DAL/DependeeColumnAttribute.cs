﻿using System;
using System.Linq;
using System.Web.DynamicData;
namespace com.paginar.johnson.DAL
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class DependeeColumnAttribute : Attribute
    {
        public static DependeeColumnAttribute Default = new DependeeColumnAttribute();

        public DependeeColumnAttribute() : this("") { }

        public DependeeColumnAttribute(String columnName)
        {
            ColumnName = columnName;
        }

        public String ColumnName { get; set; }
    }
}