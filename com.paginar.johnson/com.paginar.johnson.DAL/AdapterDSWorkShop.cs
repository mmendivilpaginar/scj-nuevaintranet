﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSWorkShop
    {
        private DSWorkShopTableAdapters.ws_WorkshopTableAdapter _Workshop;
        public DSWorkShopTableAdapters.ws_WorkshopTableAdapter Workshop
        {
            get {

                if (_Workshop == null) _Workshop = new DSWorkShopTableAdapters.ws_WorkshopTableAdapter();
                return _Workshop;
            
            }
        }

        private DSWorkShopTableAdapters.ws_EstadoTableAdapter _WSEstado;
        public DSWorkShopTableAdapters.ws_EstadoTableAdapter WSEstado 
        {
            get {

                if (_WSEstado == null) _WSEstado = new DSWorkShopTableAdapters.ws_EstadoTableAdapter();
                return _WSEstado;

            }
        }

        private DSWorkShopTableAdapters.ws_InscripcionTableAdapter _WSInscripcion;
        public DSWorkShopTableAdapters.ws_InscripcionTableAdapter WSInscripcion
        {
            get
            {
                if (_WSInscripcion == null) _WSInscripcion = new DSWorkShopTableAdapters.ws_InscripcionTableAdapter();
                return _WSInscripcion;
            }
        }

        private DSWorkShopTableAdapters.ws_Documentos_WorkshopTableAdapter _WSDocumentos;
        public DSWorkShopTableAdapters.ws_Documentos_WorkshopTableAdapter WSDocumentos
        {
            get 
            {
                if (_WSDocumentos == null) _WSDocumentos = new DSWorkShopTableAdapters.ws_Documentos_WorkshopTableAdapter();
                return _WSDocumentos;
            }
        }

        private DSWorkShopTableAdapters.ws_Inscripcion1TableAdapter _WSSuscriptos;
        public DSWorkShopTableAdapters.ws_Inscripcion1TableAdapter WSSuscriptos
        {
            get
            {
                if (_WSSuscriptos == null) _WSSuscriptos = new DSWorkShopTableAdapters.ws_Inscripcion1TableAdapter();
                return _WSSuscriptos;
            }
        }

        private DSWorkShopTableAdapters.ws_selectInscriptosByAnioTableAdapter _WSInscriptosByAnio;
        public DSWorkShopTableAdapters.ws_selectInscriptosByAnioTableAdapter WSInscriptosByAnio
        {
            get
            {
                if (_WSInscriptosByAnio == null) _WSInscriptosByAnio = new DSWorkShopTableAdapters.ws_selectInscriptosByAnioTableAdapter();
                return _WSInscriptosByAnio;
            }
        }
    }
}
