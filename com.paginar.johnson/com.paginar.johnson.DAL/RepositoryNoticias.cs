﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryNoticias
    {
        private AdapterDSNoticias _AdapterDSNoticias;
        public AdapterDSNoticias AdapterDSNoticias
        {
            get
            {
                if (_AdapterDSNoticias == null) _AdapterDSNoticias = new AdapterDSNoticias();
                return _AdapterDSNoticias;
            }
        }

        public DSNoticias.infInfoDataTable GetBusquedaABM(int? Estado,int? Destacado,int? CategoriaID,string TipoBusq,DateTime? Desde, DateTime? Hasta)
        {
            return AdapterDSNoticias.infInfo.GetDataByGetBusquedaABM(Estado, Destacado, CategoriaID, TipoBusq, Desde, Hasta);
        }

        public void GetInfoByID(DSNoticias DSN, int InfoID)
        {
            AdapterDSNoticias.infInfo.FillByID(DSN.infInfo, InfoID);
        }

        public void GetImagenByInfoID(DSNoticias DSN, int InfoID)
        {
            AdapterDSNoticias.infImagen.FillByInfoID(DSN.infImagen, InfoID);
        }

        public void GetLinkByInfoID(DSNoticias DSN, int InfoID)
        {
            AdapterDSNoticias.infLink.FillByInfoID(DSN.infLink, InfoID);
        }

        public bool UsuarioVotoNoticia(int usuarioid, int infoid)
        {
             int? voto= (int?)AdapterDSNoticias.infoUsuarioVotacion.UsuarioVotoNoticia(usuarioid, infoid);
             if (voto == 1) return true;
             else return false;
        }

        public int? getVotos(int infoid,int voto)
        {
            return AdapterDSNoticias.infoUsuarioVotacion.getVotos(infoid, voto);
        }

        public DSNoticias.InfoUsuarioVotacionDataTable getUserByVoto(int infoID)
        {
            return AdapterDSNoticias.infoUsuarioVotacion.GetUserByVoto(infoID);
        }

        public DSNoticias.InfoUsuarioComentariosDataTable getComments(int infoID)
        {
            return AdapterDSNoticias.infoUsuarioComentarios.getComents(infoID);
        }

        public int? getCantCommets(int infoid)
        {
            return AdapterDSNoticias.infoUsuarioComentarios.getCantComents(infoid);
        }


        public DSNoticias.infoBannerDataTable getBanners(int clusterid)
        {
            return AdapterDSNoticias.infoBanner.getBanners(clusterid);
        }

        public DSNoticias.infoBannerDataTable getLastBanner(int clusterid)
        {
            return AdapterDSNoticias.infoBanner.getLastBanner(clusterid);
        }

        public DSNoticias.infoBannerDataTable getAllBanners()
        {
           return AdapterDSNoticias.infoBanner.getAllBanners();
        }

        public DSNoticias.infoBannerDataTable getBannerByID(int bannerid)
        {
            return AdapterDSNoticias.infoBanner.GetBannerByID(bannerid);
        }

        public DSNoticias.infoBannerClusterDataTable getClusterByBannerID(int bannerid)
        {
            return AdapterDSNoticias.InfoBannerCluster.GetDataByBannerID(bannerid);
        }

        public void DeleteClusterBanner(int bannerid)
        {
            AdapterDSNoticias.InfoBannerCluster.DeleteByBannerID(bannerid);
        }

        public DSNoticias.infRecordatoriosDataTable getRecordatoriosInfByID(int idinf)
        {
            return AdapterDSNoticias.infRecordatorio.GetDataByInfoID(idinf);
        }

        public DSNoticias.infInfoDataTable getInfoByReporte(DateTime? desde, DateTime? hasta, string modo)
        {
            return AdapterDSNoticias.infInfo.GetInfoByReporte(desde, hasta, modo);
        }

        public DSNoticias.infCategoriaDataTable getInfCategorias()
        {
            return AdapterDSNoticias.infCategoria.GetData();
        }

        public DSNoticias.infCategoriaDataTable getinfCategoria_ById(int CategoriaID)
        {
            return AdapterDSNoticias.infCategoria.infCategoria_ById(CategoriaID);
        }

        public DSNoticias.infCategoriaDataTable getInfCat_Search(string Search)
        {
            return AdapterDSNoticias.infCategoria.infCat_Search(Search);
        }

        public int infCat_Delete(int CategoriaID)
        {
            int retorno = int.Parse((AdapterDSNoticias.infCategoria.infCat_Delete(CategoriaID)).ToString());
            return retorno;
        }

        public void infCat_Insert(string Descrip, int UsrAlta)
        {
            AdapterDSNoticias.infCategoria.infCat_Insert(Descrip, UsrAlta, DateTime.Now);
        }

        public void infCat_Update(int CategoriaID, string Descrip)
        {
            AdapterDSNoticias.infCategoria.infCat_Update(CategoriaID, Descrip, DateTime.Now);
        }


    }
}
