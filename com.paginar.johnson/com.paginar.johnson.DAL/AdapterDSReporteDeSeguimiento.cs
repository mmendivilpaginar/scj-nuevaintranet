﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSReporteDeSeguimiento : IDisposable
    {

        DSReportesDeSeguimientoTableAdapters.AreasTableAdapter _Areas;
        public DSReportesDeSeguimientoTableAdapters.AreasTableAdapter Areas
        {
            get {
                if (_Areas == null) _Areas = new DSReportesDeSeguimientoTableAdapters.AreasTableAdapter();
                return _Areas;
            
            }
        
        }

        DSReportesDeSeguimientoTableAdapters.DireccionTableAdapter _Direcciones;
        public DSReportesDeSeguimientoTableAdapters.DireccionTableAdapter Direcciones
        {
            get
            {
                if (_Direcciones == null) _Direcciones = new DSReportesDeSeguimientoTableAdapters.DireccionTableAdapter();
                return _Direcciones;

            }

        }

        DSReportesDeSeguimientoTableAdapters.EvaluadoresAdministrativosTableAdapter _EvaluadoresAdministrativos;
        public DSReportesDeSeguimientoTableAdapters.EvaluadoresAdministrativosTableAdapter EvaluadoresAdministrativos
        {
            get
            {
                if (_EvaluadoresAdministrativos == null) _EvaluadoresAdministrativos = new DSReportesDeSeguimientoTableAdapters.EvaluadoresAdministrativosTableAdapter();
                return _EvaluadoresAdministrativos;

            }

        }

        DSReportesDeSeguimientoTableAdapters.EvaluadoresTableAdapter _Evaluadores;
        public DSReportesDeSeguimientoTableAdapters.EvaluadoresTableAdapter Evaluadores
        {
            get
            {
                if (_Evaluadores == null) _Evaluadores = new DSReportesDeSeguimientoTableAdapters.EvaluadoresTableAdapter();
                return _Evaluadores;

            }

        }

        DSReportesDeSeguimientoTableAdapters.Eval_PeriodoFiscalTableAdapter _Eval_PeriodoFiscal;
        public DSReportesDeSeguimientoTableAdapters.Eval_PeriodoFiscalTableAdapter Eval_PeriodoFiscal
        {
            get
            {
                if (_Eval_PeriodoFiscal == null) _Eval_PeriodoFiscal = new DSReportesDeSeguimientoTableAdapters.Eval_PeriodoFiscalTableAdapter();
                return _Eval_PeriodoFiscal;

            }

        }

        DSReportesDeSeguimientoTableAdapters.eval_PeriodoTableAdapter _Eval_Periodo;
        public DSReportesDeSeguimientoTableAdapters.eval_PeriodoTableAdapter Eval_Periodo
        {
            get
            {
                if (_Eval_Periodo == null) _Eval_Periodo = new DSReportesDeSeguimientoTableAdapters.eval_PeriodoTableAdapter();
                return _Eval_Periodo;

            }

        }

        DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoAreaDireccionTableAdapter _Eval_GetReporteSeguimientoAgrupadoAreaDireccion;
        public DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoAreaDireccionTableAdapter Eval_GetReporteSeguimientoAgrupadoAreaDireccion
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAgrupadoAreaDireccion == null) _Eval_GetReporteSeguimientoAgrupadoAreaDireccion = new DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoAreaDireccionTableAdapter();
                return _Eval_GetReporteSeguimientoAgrupadoAreaDireccion;

            }

        }

        DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoEvaluadosTableAdapter _Eval_GetReporteSeguimientoEvaluados;
        public DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoEvaluadosTableAdapter Eval_GetReporteSeguimientoEvaluados
        {
            get
            {
                if (_Eval_GetReporteSeguimientoEvaluados == null) _Eval_GetReporteSeguimientoEvaluados = new DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoEvaluadosTableAdapter();
                return _Eval_GetReporteSeguimientoEvaluados;

            }

        }

        DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionTableAdapter _Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion;
        public DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionTableAdapter Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion
        {
            get
            {
                if (_Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion == null) _Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion = new DSReportesDeSeguimientoTableAdapters.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionTableAdapter();
                return _Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion;

            }

        }

        DSReportesDeSeguimientoTableAdapters.eval_GetReporteSguimientoAgrupadoEvaluadorRatingTableAdapter _eval_GetReporteSguimientoAgrupadoEvaluadorRating;
        public DSReportesDeSeguimientoTableAdapters.eval_GetReporteSguimientoAgrupadoEvaluadorRatingTableAdapter eval_GetReporteSguimientoAgrupadoEvaluadorRating
        {
            get
            {
                if (_eval_GetReporteSguimientoAgrupadoEvaluadorRating == null) _eval_GetReporteSguimientoAgrupadoEvaluadorRating = new DSReportesDeSeguimientoTableAdapters.eval_GetReporteSguimientoAgrupadoEvaluadorRatingTableAdapter();
                return _eval_GetReporteSguimientoAgrupadoEvaluadorRating;

            }

        }

        public void Dispose()
        {
            _Areas = null;
            _Direcciones = null;
            _Evaluadores = null;
            _EvaluadoresAdministrativos = null;
        }


    }
}
