﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSPda
    {
        private DSPdaTableAdapters.pda_cursoTableAdapter _curso;
        private DSPdaTableAdapters.pda_inscriptoTableAdapter _inscripto;

        public DSPdaTableAdapters.pda_cursoTableAdapter Curso
        { 
            get
            {
                if (_curso == null) _curso = new DSPdaTableAdapters.pda_cursoTableAdapter();
                return _curso;
            }
        }

        public DSPdaTableAdapters.pda_inscriptoTableAdapter Inscripto
        {
            get
            {
                if (_inscripto == null) _inscripto = new DSPdaTableAdapters.pda_inscriptoTableAdapter();
                return _inscripto;
            }

        }

        private DSPdaTableAdapters.pda_estadoTableAdapter _estado;
        public DSPdaTableAdapters.pda_estadoTableAdapter Estado
        {
            get
            {
                if (_estado == null) _estado = new DSPdaTableAdapters.pda_estadoTableAdapter();
                return _estado;
            }
        }

        private DSPdaTableAdapters.pda_ReporteInscripcionPorCursoTableAdapter _ReporteInscripcionPorCurso;
        public DSPdaTableAdapters.pda_ReporteInscripcionPorCursoTableAdapter ReporteInscripcionPorCurso
        {
            get
            {
                if (_ReporteInscripcionPorCurso == null) _ReporteInscripcionPorCurso = new DSPdaTableAdapters.pda_ReporteInscripcionPorCursoTableAdapter();
                return _ReporteInscripcionPorCurso;
            }
        }

        private DSPdaTableAdapters.DireccionTableAdapter _Direcciones;
        public DSPdaTableAdapters.DireccionTableAdapter Direcciones
        {
            get
            {
                if (_Direcciones == null) _Direcciones = new DSPdaTableAdapters.DireccionTableAdapter();
                return _Direcciones;
            }
        }

        private DSPdaTableAdapters.AreasTableAdapter _Areas;
        public DSPdaTableAdapters.AreasTableAdapter Areas
        {
            get
            {
                if (_Areas == null) _Areas = new DSPdaTableAdapters.AreasTableAdapter();
                return _Areas;                     
            }
        }

        private DSPdaTableAdapters.pda_DetalleReporteAreaDireccionTableAdapter _DetalleReporteAreaDireccion;
        public DSPdaTableAdapters.pda_DetalleReporteAreaDireccionTableAdapter DetalleReporteAreaDireccion
        {
            get
            {
                if (_DetalleReporteAreaDireccion == null) _DetalleReporteAreaDireccion = new DSPdaTableAdapters.pda_DetalleReporteAreaDireccionTableAdapter();
                return _DetalleReporteAreaDireccion;
            }
        }

        private DSPdaTableAdapters.pda_ReporteAreaDireccionTableAdapter _ReporteAreaDireccion;
        public DSPdaTableAdapters.pda_ReporteAreaDireccionTableAdapter ReporteAreaDireccion
        {
            get
            {
                if (_ReporteAreaDireccion == null) _ReporteAreaDireccion = new DSPdaTableAdapters.pda_ReporteAreaDireccionTableAdapter();
                return _ReporteAreaDireccion;
            }
        }

        private DSPdaTableAdapters.pda_ReportePorUsuarioTableAdapter _ReporteporUsuarios;
        public DSPdaTableAdapters.pda_ReportePorUsuarioTableAdapter ReporteporUsuarios
        {
            get 
            {
                if (_ReporteporUsuarios == null) _ReporteporUsuarios = new DSPdaTableAdapters.pda_ReportePorUsuarioTableAdapter();
                return _ReporteporUsuarios;
            }
        }

    }
}
