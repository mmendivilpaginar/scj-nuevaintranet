﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_AgendaTel))]
    [DisplayName("Agenda")]
    

    public partial class AgendaTel
    {

        public class MetaData_AgendaTel
        {
  /*           
        [UIHint("Html")]
          public object Descripcion;
 */           
         [DisplayName("Descripción")]
          public object Descripcion { get; set; }

         [DisplayName("Interno")]
         
         public object interno { get; set; }

        }

        partial void OninternoChanged()
        {
            if (this.interno == null) {
                this.interno = string.Empty;
            }
        }
    }


}
