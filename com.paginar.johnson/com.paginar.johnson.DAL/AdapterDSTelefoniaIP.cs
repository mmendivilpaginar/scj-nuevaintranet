﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSTelefoniaIP
    {
        private DSTelefoniaIPTableAdapters.telefoniaIPTableAdapter _telefoniaIP;
        public DSTelefoniaIPTableAdapters.telefoniaIPTableAdapter telefoniaIP
        {
            get
            {
                if (_telefoniaIP == null) _telefoniaIP = new DSTelefoniaIPTableAdapters.telefoniaIPTableAdapter();
                return _telefoniaIP;
            }
        }
    }
}
