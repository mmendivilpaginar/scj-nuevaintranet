﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSIndumentaria
    {
        private DSIndumentariaTableAdapters.IndumentariaTableAdapter _Indumentaria;
        public DSIndumentariaTableAdapters.IndumentariaTableAdapter Indumentaria
        {
            get
            {
                if (_Indumentaria == null) _Indumentaria = new DSIndumentariaTableAdapters.IndumentariaTableAdapter();
                return _Indumentaria;
            }
        }

        private DSIndumentariaTableAdapters.IndumentariaPedidoTableAdapter _Pedido;
        public DSIndumentariaTableAdapters.IndumentariaPedidoTableAdapter Pedido
        {
            get
            {
                if (_Pedido == null) _Pedido = new DSIndumentariaTableAdapters.IndumentariaPedidoTableAdapter();
                return _Pedido;
            }
        }

        private DSIndumentariaTableAdapters.Indumentaria_PeriodoTableAdapter _Periodo;
        public DSIndumentariaTableAdapters.Indumentaria_PeriodoTableAdapter Periodo
        {
            get 
            {
                if (_Periodo == null) _Periodo = new DSIndumentariaTableAdapters.Indumentaria_PeriodoTableAdapter();
                return _Periodo;
            }
        }

        private DSIndumentariaTableAdapters.frmTramiteTableAdapter _Tramite;
        public DSIndumentariaTableAdapters.frmTramiteTableAdapter Tramite
        {
            get 
            {
                if(_Tramite == null) _Tramite = new DSIndumentariaTableAdapters.frmTramiteTableAdapter();
                return _Tramite;
            }
        }

        private DSIndumentariaTableAdapters.IndumentariaMigracionTableAdapter _Migracion;
        public DSIndumentariaTableAdapters.IndumentariaMigracionTableAdapter Migracion
        {
            get 
            {
                if (_Migracion == null) _Migracion = new DSIndumentariaTableAdapters.IndumentariaMigracionTableAdapter();
                return _Migracion;
            }
        }

        private DSIndumentariaTableAdapters.TramitesNoIniciadosTableAdapter _NoIniciados;
        public DSIndumentariaTableAdapters.TramitesNoIniciadosTableAdapter NoIniciados
        {
            get 
            {
                if (_NoIniciados == null) _NoIniciados = new DSIndumentariaTableAdapters.TramitesNoIniciadosTableAdapter();
                return _NoIniciados;
            }
        }
        
    }
}
