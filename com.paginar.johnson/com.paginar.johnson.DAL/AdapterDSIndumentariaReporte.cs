﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSIndumentariaReporte
    {
        private DSIndumentariaReporteTableAdapters.IndumentariaReporteDetalleTableAdapter _IndumentariaReporte;
        public DSIndumentariaReporteTableAdapters.IndumentariaReporteDetalleTableAdapter IndumentariaReporte
        {
            get 
            {
                if (_IndumentariaReporte == null) _IndumentariaReporte = new DSIndumentariaReporteTableAdapters.IndumentariaReporteDetalleTableAdapter();
                return _IndumentariaReporte;
            }
        }

        private DSIndumentariaReporteTableAdapters.IndumentariaReporteAreaSinDetalleTableAdapter _IndumentariaReporteArea;
        public DSIndumentariaReporteTableAdapters.IndumentariaReporteAreaSinDetalleTableAdapter IndumentariaReporteArea
        {
            get
            {
                if (_IndumentariaReporteArea == null) _IndumentariaReporteArea = new DSIndumentariaReporteTableAdapters.IndumentariaReporteAreaSinDetalleTableAdapter();
                return _IndumentariaReporteArea;
            }
        }

        private DSIndumentariaReporteTableAdapters.IndumentariaReporteSinDetalleTableAdapter _IndumentariaReporteSinDetalle;
        public DSIndumentariaReporteTableAdapters.IndumentariaReporteSinDetalleTableAdapter IndumentariaReporteSinDetalle
        {
            get
            {
                if (_IndumentariaReporteSinDetalle == null) _IndumentariaReporteSinDetalle = new DSIndumentariaReporteTableAdapters.IndumentariaReporteSinDetalleTableAdapter();
                return _IndumentariaReporteSinDetalle;
            }
        }
    }
}
