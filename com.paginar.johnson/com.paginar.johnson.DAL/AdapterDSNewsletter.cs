﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSNewsletter
    {


        private DSNewsLetterTableAdapters.news_getDestinatariosTableAdapter _news_getDestinatariosTableAdapter;
        public DSNewsLetterTableAdapters.news_getDestinatariosTableAdapter news_getDestinatariosTableAdapter
        {
            get
            {
                if (_news_getDestinatariosTableAdapter == null)
                    _news_getDestinatariosTableAdapter = new DSNewsLetterTableAdapters.news_getDestinatariosTableAdapter();
                return _news_getDestinatariosTableAdapter;
            }
        }



        private DSNewsLetterTableAdapters.news_DestinatariosTableAdapter _DestinatariosTableAdapter;

        public DSNewsLetterTableAdapters.news_DestinatariosTableAdapter DestinatariosTableAdapter
        {

            get
            {
                if (_DestinatariosTableAdapter == null)
                    _DestinatariosTableAdapter = new DSNewsLetterTableAdapters.news_DestinatariosTableAdapter();
                return _DestinatariosTableAdapter;
            }
        }

        private DSNewsLetterTableAdapters.news_FotosTableAdapter _news_FotosTableAdapter;

        public DSNewsLetterTableAdapters.news_FotosTableAdapter news_FotosTableAdapter
        {

            get
            {
                if (_news_FotosTableAdapter == null)
                    _news_FotosTableAdapter = new DSNewsLetterTableAdapters.news_FotosTableAdapter();
                return _news_FotosTableAdapter;
            }
        }


        private DSNewsLetterTableAdapters.news_NewsLetterTableAdapter _news_NewsLetterTableAdapter;

        public DSNewsLetterTableAdapters.news_NewsLetterTableAdapter news_NewsLetterTableAdapter
        {

            get
            {
                if (_news_NewsLetterTableAdapter == null)
                    _news_NewsLetterTableAdapter = new DSNewsLetterTableAdapters.news_NewsLetterTableAdapter();
                return _news_NewsLetterTableAdapter;
            }
        }


        private DSNewsLetterTableAdapters.news_RInfInfoNewsLetterTableAdapter _news_RInfInfoNewsLetterTableAdapter;

        public DSNewsLetterTableAdapters.news_RInfInfoNewsLetterTableAdapter news_RInfInfoNewsLetterTableAdapter
        {

            get
            {
                if (_news_RInfInfoNewsLetterTableAdapter == null)
                    _news_RInfInfoNewsLetterTableAdapter = new DSNewsLetterTableAdapters.news_RInfInfoNewsLetterTableAdapter();
                return _news_RInfInfoNewsLetterTableAdapter;
            }
        }

        private DSNewsLetterTableAdapters.news_getNoticiasTableAdapter _news_getNoticiasTableAdapter;

        public DSNewsLetterTableAdapters.news_getNoticiasTableAdapter news_getNoticiasTableAdapter
        {

            get
            {
                if (_news_getNoticiasTableAdapter == null)
                    _news_getNoticiasTableAdapter = new DSNewsLetterTableAdapters.news_getNoticiasTableAdapter();
                return _news_getNoticiasTableAdapter;
            }
        }



        private DSNewsLetterTableAdapters.news_BuscadorTableAdapter _news_BuscadorTableAdapter;
        public DSNewsLetterTableAdapters.news_BuscadorTableAdapter news_BuscadorTableAdapter
        {
            get
            {
                if (_news_BuscadorTableAdapter == null)
                    _news_BuscadorTableAdapter = new DSNewsLetterTableAdapters.news_BuscadorTableAdapter();
                return _news_BuscadorTableAdapter;
            }
        }


        private DSNewsLetterTableAdapters.news_getNoticiasFilesTableAdapter _newsgetNoticiasFilesTableAdapter;
        public DSNewsLetterTableAdapters.news_getNoticiasFilesTableAdapter newsgetNoticiasFilesTableAdapter
        {
            get
            {
                if (_newsgetNoticiasFilesTableAdapter == null)
                    _newsgetNoticiasFilesTableAdapter = new DSNewsLetterTableAdapters.news_getNoticiasFilesTableAdapter();
                return _newsgetNoticiasFilesTableAdapter;
            }
        }



    }
}
