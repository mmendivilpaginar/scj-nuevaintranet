﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryExperiencia
    {
        private AdapterDSExperiencia _AdapterDSExperiencia;
        public AdapterDSExperiencia AdapterDSExperiencia
        {
            get
            {
                if (_AdapterDSExperiencia == null)
                    _AdapterDSExperiencia = new AdapterDSExperiencia();
                return _AdapterDSExperiencia;
            }
        }
    }
}
