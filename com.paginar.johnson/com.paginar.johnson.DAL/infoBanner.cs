﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace com.paginar.johnson.DAL
{

    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_infoBanner))]
    [DisplayName("Banners")]
    public partial class infoBanner
    {
        
        public class MetaData_infoBanner
        {
             [UIHint("Html")]
             public object Titulo;

             [UIHint("Html")]
             public object Cuerpo;

             [UIHint("FileUpload")]
             [FileUpload(
                 FileUrl = "~/noticias/Imagenes/{0}",
                 FileTypes = new String[] { "gif","jpg" },
                 DisplayImageType = "png",
                 DisableHyperlink = false,
                 HyperlinkRoles = new String[] { },
                 DisplayImageUrl = "~/noticias/Imagenes/{0}")]    
          //   public object Imagen { get; set; };
            public object Imagen { get; set; }

           // [DefaultValue(typeof(DateTime), DateTime.Now.ToString("yyyy-MM-dd"))]
            // [ScaffoldColumn(false)]
            // string now = DateTime.Now.ToString();
            //[DefaultValue(typeof(DateTime), now)]
            //public object FHAlta { get; set; }

            //public DateTime FHAlta
            //{
            //    get
            //    {
            //        return (this.dateCreated == default(DateTime))
            //           ? DateTime.Now
            //           : this.dateCreated;
            //    }

            //    set { this.dateCreated = value; }
            //}
            //private DateTime dateCreated = default(DateTime);


        }
    }

   
}
