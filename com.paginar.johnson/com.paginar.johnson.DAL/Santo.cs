﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace com.paginar.johnson.DAL
{
    [ScaffoldTable(true)]
    [MetadataType(typeof(MetaData_Santo))]
    [DisplayName("Santos")]
    public partial class Santo
    {
        public class MetaData_Santo
        {
            [DisplayName("Día")]
            [DisplayFormatAttribute(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM}")]
            [UIHint("DateTimeAnioMes")]
            public object Dia { get; set; }
            [DisplayName("Descripcíon")]
            public object Descripcion { get; set; }
        }
    }
}
