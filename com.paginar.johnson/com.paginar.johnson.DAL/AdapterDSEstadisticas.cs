﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSEstadisticas
    {
        private DSEstadisticasTableAdapters.EstadisticasTableAdapter _Estadisticas;
        public DSEstadisticasTableAdapters.EstadisticasTableAdapter Estadisticas 
        { 
            get{
                if (_Estadisticas == null) _Estadisticas = new DSEstadisticasTableAdapters.EstadisticasTableAdapter();
                return _Estadisticas;
            }
        }


        private DSEstadisticasTableAdapters.SeccionConsultadaTableAdapter _Estadisticas1;
        public DSEstadisticasTableAdapters.SeccionConsultadaTableAdapter Estadisticas1
        {
            get
            {
                if (_Estadisticas1 == null) _Estadisticas1 = new DSEstadisticasTableAdapters.SeccionConsultadaTableAdapter();
                return _Estadisticas1;
            }
        }

        private DSEstadisticasTableAdapters.GruposEtariosTableAdapter _GruposEtarios;
        public DSEstadisticasTableAdapters.GruposEtariosTableAdapter GruposEtarios
        {
            get
            {
                if (_GruposEtarios == null) _GruposEtarios = new DSEstadisticasTableAdapters.GruposEtariosTableAdapter();
                return _GruposEtarios;
            }
        }

        private DSEstadisticasTableAdapters.LogTableAdapter _Log;
        public DSEstadisticasTableAdapters.LogTableAdapter Log
        {
            get
            {
                if (_Log == null) _Log = new DSEstadisticasTableAdapters.LogTableAdapter();
                return _Log;
            }
        }

       

        private DSEstadisticasTableAdapters.UsuarioTableAdapter _Usuarios;
        public DSEstadisticasTableAdapters.UsuarioTableAdapter Usuarios
        {
            get 
            {
                if (_Usuarios == null) _Usuarios = new DSEstadisticasTableAdapters.UsuarioTableAdapter();
                return _Usuarios;
            }
        }

        private DSEstadisticasTableAdapters.Hombres_MujeresTableAdapter _Hombres_Mujeres;
        public DSEstadisticasTableAdapters.Hombres_MujeresTableAdapter Hombres_Mujeres
        {
            get
            {
                if (_Hombres_Mujeres == null) _Hombres_Mujeres = new DSEstadisticasTableAdapters.Hombres_MujeresTableAdapter();
                return _Hombres_Mujeres;
            }
        }

        private DSEstadisticasTableAdapters.HorariosConcurrenciaTableAdapter _Horarios_Concurrencia;
        public DSEstadisticasTableAdapters.HorariosConcurrenciaTableAdapter Horarios_Concurrencia
        {
            get
            {
                if (_Horarios_Concurrencia == null) _Horarios_Concurrencia = new DSEstadisticasTableAdapters.HorariosConcurrenciaTableAdapter();
                return _Horarios_Concurrencia;
            }
        }

        private DSEstadisticasTableAdapters.TopNoticiasTableAdapter _Noticias;
        public DSEstadisticasTableAdapters.TopNoticiasTableAdapter Noticias
        {
            get 
            {
                if (_Noticias == null) _Noticias = new DSEstadisticasTableAdapters.TopNoticiasTableAdapter();
                return _Noticias;
            }
        }
    }
}
