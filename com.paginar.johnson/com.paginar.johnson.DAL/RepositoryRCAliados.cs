﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryRCAliados
    {
        private AdapterDSRCAliados _AdapterDSRCAliados;
        public AdapterDSRCAliados AdapterDSRCAliados
        {
            get 
            {
                if (_AdapterDSRCAliados == null) _AdapterDSRCAliados = new AdapterDSRCAliados();
                return _AdapterDSRCAliados;
            }
        }

        public DSRCAliados.com_AliadoDataTable com_aliadosSelectAll()
        {
            return AdapterDSRCAliados.Aliados.com_aliadosSelectAll();
        }

        public DSRCAliados.com_AliadoDataTable com_aliadosSelectById(int idAliado)
        {
            return AdapterDSRCAliados.Aliados.com_aliadosSelectById(idAliado);
        }

        public DSRCAliados.com_AliadoDataTable com_aliadoSelectActivos()
        {
            return AdapterDSRCAliados.Aliados.com_aliadosSelectActivos();
        }

        public void com_aliadosDelete(int idAliado)
        {
            AdapterDSRCAliados.Aliados.com_aliadosDelete(idAliado);
        }

        public void com_aliadosInsert(string Nombre, string QueHace, string QueHara, string web, bool Activo, string imgLogo, int tipo)
        {
            AdapterDSRCAliados.Aliados.com_aliadosInsert(Nombre, QueHace, QueHara, web, Activo, imgLogo, tipo);
        }

        public void com_aliadosUpdate(int idAliado, string Nombre, string QueHace, string QueHara, string web, bool Activo, string imgLogo, int tipo)
        {
            AdapterDSRCAliados.Aliados.com_aliadosUpdate(idAliado, Nombre, QueHace, QueHara, web, Activo, imgLogo, tipo);
        }

        public DSRCAliados.com_AliadoDataTable com_aliadosSelectActivosByTipo(int tipo)
        {
            return AdapterDSRCAliados.Aliados.com_aliadosSelectActivosByTipo(tipo);
        }
    }
}
