﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryReportesDeSeguimiento
    {
        private AdapterDSReporteDeSeguimiento _AdapterReportesDeSeguimiento;
        public AdapterDSReporteDeSeguimiento AdapterReportesDeSeguimiento
        {
            get
            {
                if (_AdapterReportesDeSeguimiento == null) _AdapterReportesDeSeguimiento = new AdapterDSReporteDeSeguimiento();
                return _AdapterReportesDeSeguimiento;
            }
        }


        public DSReportesDeSeguimiento.AreasDataTable GetAreasByDireccionID(int DireccionID)
        {
            return AdapterReportesDeSeguimiento.Areas.GetDataByDireccionID(DireccionID);
        }

        public DSReportesDeSeguimiento.DireccionDataTable GetDirecciones()
        {
            return AdapterReportesDeSeguimiento.Direcciones.GetDireccionesEvaluaciones();
        }

        public DSReportesDeSeguimiento.Eval_PeriodoFiscalDataTable GetPeriodosFiscales()
        {
            return AdapterReportesDeSeguimiento.Eval_PeriodoFiscal.GetDataPeriodosFiscales();
        }
        public DSReportesDeSeguimiento.eval_PeriodoDataTable GetPeriodoByPeridoFiscalTipoPeriodo(int PeriodoFiscal, int TipoPeriodo)
        {
            return AdapterReportesDeSeguimiento.Eval_Periodo.GetPeriodoByPeriodoFiscalID(PeriodoFiscal, TipoPeriodo);
        }

        public DSReportesDeSeguimiento.EvaluadoresDataTable GetEvaluadoresByPeriodoID(int PeriodoID)
        {
            return AdapterReportesDeSeguimiento.Evaluadores.GetDataByPeriodoID(PeriodoID, 1);
        }

        public DSReportesDeSeguimiento.EvaluadoresAdministrativosDataTable GetEvaluadoresAdministrativosByPeriodoID(int PeriodoID)
        {
            return AdapterReportesDeSeguimiento.EvaluadoresAdministrativos.GetDataEvaluadoresAdministrativos();
        
        }

        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable GetReporteSeguimientoAgrupadoAreaDireccion(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster, int MostrarAreas)
        {
            return AdapterReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccion.GetData( PeriodoID,LegajoEvaluador,Pasoid,TipoFormularioID,DireccionID,AreaID,Cluster,MostrarAreas);
         }

        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoEvaluadosDataTable Eval_GetReporteSeguimientoEvaluados(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Calificacion, string Cluster)
        {
            return AdapterReportesDeSeguimiento.Eval_GetReporteSeguimientoEvaluados.GetData(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Calificacion, Cluster);
        }

        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster)
        {
            return AdapterReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion.GetData(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Cluster);
        }

        public DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable eval_GetReporteSguimientoAgrupadoEvaluadorRating(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster)
        {
            return AdapterReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRating.GetData(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Cluster);
        }
    }
}
