﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.ComponentModel;

namespace com.paginar.johnson.DAL
{
    [MetadataType(typeof(MetaData_Direccion))]
    [DisplayName("Direcciones")]
    public partial class Direccion
    {
        partial void OnValidate(ChangeAction action)
        {
            if (action == ChangeAction.Delete)
            {
                var DC = new SCJBODataContext();

                if (DC.UsuarioBOs.Where(c => c.DireccionID == this.DireccionID).ToList().Count > 0)
                {
                    throw new ValidationException("No podrá eliminar la dirección " + this.DireccionDET + ", debido a que posee un usuario relacionado. Por Favor, reasigne el/los usuarios a la Dirección que corresponda.");
                }
            }
        }


        public class MetaData_Direccion
        {
            [DisplayName("Descripción")]
            [FilterUIHint("Search")]
            public object DireccionDET { get; set; }
            
            [ScaffoldColumnAttribute(false)]
            public EntitySet<UsuarioBO> UsuarioBOs { get; set; }
        }
    }
}
