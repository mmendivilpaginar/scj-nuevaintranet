﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.DynamicData;

namespace com.paginar.johnson.DAL
{
    // attribute to determin what mode the FileImage_Edit is in
    // the default is Select.
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class FileImageAttribute : Attribute
    {
        public FileImageAttribute(EditorType edit)
        {
            Edit = edit;
        }

        public EditorType Edit { get; set; }

        // Editor Type
        public enum EditorType
        {
            Select,
            Upload
        }
    }

    // a way of specifying which extension type to accept
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class FileImageTypesAttribute : Attribute
    {
        public FileImageTypesAttribute()
        {
            ImageTypes = DefaultExtensions;
        }

        public FileImageTypesAttribute(params String[] imageTypes)
        {
            if (imageTypes.Length > 0)
            {
                ImageTypes = imageTypes;
            }
            else
            {
                ImageTypes = DefaultExtensions;
            }
        }

        public String[] ImageTypes { get; set; }

        // default extensions
        public static String[] DefaultExtensions
        {
            get { return new String[] { "gif", "png", "jpeg", "jpg" }; }
        }

        public String ToString()
        {
            StringBuilder extensions = new StringBuilder();

            foreach (var ext in ImageTypes)
            {
                extensions.Append(ext + ", ");
            }
            return extensions.ToString().Substring(0, extensions.Length - 2);
        }
    }

    //[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    //public sealed class FileImageSizeAttribute : Attribute
    //{
    //    public FileImageSizeAttribute()
    //    {
    //        LockAspect = true;
    //    }

    //    public FileImageSizeAttribute(int width, int height)
    //    {
    //        Width = width;
    //        Height = height;
    //        LockAspect = true;
    //    }

    //    public FileImageSizeAttribute(int width, int height, Boolean lockAspect)
    //    {
    //        Width = width;
    //        Height = height;
    //        LockAspect = lockAspect;
    //    }

    //    public int Width { get; set; }
    //    public int Height { get; set; }
    //    public Boolean LockAspect { get; set; }
    //}

    public static class FileImageHelper
    {
        public static void UpdateHeightWidth(ref int scaleWidth, ref int scaleHeight, String fileName)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
            float originalHeight = image.Height;
            float originalWidth = image.Width;
            float height = 0;
            float width = 0;
            if (scaleWidth > 0 && scaleHeight == 0)
            {
                //scale to width only
                //calculate height based on width keeping correct proportions
                height = originalHeight * (scaleWidth / originalWidth);
                scaleHeight = (int)height;
            }
            else if (scaleHeight > 0 && scaleWidth == 0)
            {
                //scale to height only
                //calculate height based on width keeping correct proportions
                width = originalWidth * (scaleHeight / originalHeight);
                scaleWidth = (int)width;
            }
            //else scale to both dims sent
        }

        /// <summary>
        /// If the given table contains a column that has a UI Hint with the value "DbImage", finds the ScriptManager
        /// for the current page and disables partial rendering
        /// </summary>
        /// <param name="page"></param>
        /// <param name="table"></param>
        public static void DisablePartialRenderingForUpload(Page page, MetaTable table)
        {
            foreach (var column in table.Columns)
            {
                // TODO this depends on the name of the field template, need to fix
                if (String.Equals(column.UIHint, "DBImage", StringComparison.OrdinalIgnoreCase)
                    || String.Equals(column.UIHint, "FileImage", StringComparison.OrdinalIgnoreCase))
                {
                    //var sm = ScriptManager.GetCurrent(page);
                    //if (sm != null)
                    //{
                    //    sm.EnablePartialRendering = false;
                    //}
                    break;
                }
            }
        }
    }
}
