﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSAgendaTel
    {

        private DSAgendaTelTableAdapters.AgendaTelTableAdapter _AgendaTel;


        public DSAgendaTelTableAdapters.AgendaTelTableAdapter AgendaTel
        {
            get
            {
                if (_AgendaTel == null) _AgendaTel = new DSAgendaTelTableAdapters.AgendaTelTableAdapter();
                return _AgendaTel;

            }
        }

        private DSAgendaTelTableAdapters.AgendaTelCategoriaTableAdapter _AgendaTelCategoria;

        public DSAgendaTelTableAdapters.AgendaTelCategoriaTableAdapter AgendaTelCategoria
        {
            get
            {
                if (_AgendaTelCategoria == null) _AgendaTelCategoria = new DSAgendaTelTableAdapters.AgendaTelCategoriaTableAdapter();
                return _AgendaTelCategoria;

            }
        }


        private DSAgendaTelTableAdapters.AgendaTelUbicacionTableAdapter _AgendaTelUbicacionTableAdapter;

        public DSAgendaTelTableAdapters.AgendaTelUbicacionTableAdapter AgendaTelUbicacionTableAdapter
        {
            get
            {
                if (_AgendaTelUbicacionTableAdapter == null) _AgendaTelUbicacionTableAdapter = new DSAgendaTelTableAdapters.AgendaTelUbicacionTableAdapter();
                return _AgendaTelUbicacionTableAdapter;

            }
        }


    }
}
