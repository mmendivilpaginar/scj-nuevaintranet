﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryCluster
    {

        private AdapterDSCluster _AdapterDSCluster;

        public AdapterDSCluster AdapterDSCluster
        {
            get
            {
                if (_AdapterDSCluster == null) _AdapterDSCluster = new AdapterDSCluster();
                return _AdapterDSCluster;
            }
        }

        
        public DCCluster.ClusterDataTable getCluster()
        {
            return AdapterDSCluster.Cluster.getCluster();
        }

        public DCCluster.ClusterDataTable getClusterDefectoUsuario(int usuarioid)
        {
            return AdapterDSCluster.Cluster.getClusterDefectoUsuario(usuarioid);
        }

        public DCCluster.ClusterDataTable getClusterUsuario(int usuarioid)
        {
            return AdapterDSCluster.Cluster.getClustersUsuario(usuarioid);
        }



        public DCCluster.RegionPaisDataTable GetPaisesHorario()
        {
            return AdapterDSCluster.RegionPais.GetPaisesHorario();
        }



        public DCCluster.RegionPaisDataTable GetClusterGMT(int clusterid)
        {
            return AdapterDSCluster.RegionPais.GetClusterGMT(clusterid);
        }
    }
}
