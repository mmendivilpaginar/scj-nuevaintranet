﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSUCHome
    {

        private DsUCHomeTableAdapters.UsuarioTableAdapter _Usuario;

        public DsUCHomeTableAdapters.UsuarioTableAdapter Usuario
        {
            get
            {
                if (_Usuario == null) _Usuario = new DsUCHomeTableAdapters.UsuarioTableAdapter();
                return _Usuario;
                
            }
        }

        private DsUCHomeTableAdapters.EfemeridesTableAdapter _Efemerides;

        public DsUCHomeTableAdapters.EfemeridesTableAdapter Efemerides
        {
            get
            {
                if (_Efemerides == null) _Efemerides = new DsUCHomeTableAdapters.EfemeridesTableAdapter();
                return _Efemerides;
            }
        }

        private DsUCHomeTableAdapters.SantosTableAdapter _Santos;

        public DsUCHomeTableAdapters.SantosTableAdapter Santos
        {
            get
            {
                if (_Santos == null) _Santos = new DsUCHomeTableAdapters.SantosTableAdapter();
                return _Santos;
            }
        }


        private DsUCHomeTableAdapters.frmTramiteDevTableAdapter _frmTramitesDev;

        public DsUCHomeTableAdapters.frmTramiteDevTableAdapter frmTramitesDev
        {
            get
            {
                if (_frmTramitesDev == null) _frmTramitesDev = new DsUCHomeTableAdapters.frmTramiteDevTableAdapter();
                return _frmTramitesDev;
            }
        }

        private DsUCHomeTableAdapters.infComedorTableAdapter _Infocomedor;
        public DsUCHomeTableAdapters.infComedorTableAdapter InfoComedor
        {
            get
            {
                if (_Infocomedor == null) _Infocomedor = new DsUCHomeTableAdapters.infComedorTableAdapter();
                return _Infocomedor;
            }
        }

        private DsUCHomeTableAdapters.InfoComedorUsuarioComentariosTableAdapter _InfoComedorUsuarioComentarios;
        public DsUCHomeTableAdapters.InfoComedorUsuarioComentariosTableAdapter InfoComedorUsuarioComentarios
        {
            get
            {
                if (_InfoComedorUsuarioComentarios == null) _InfoComedorUsuarioComentarios = new DsUCHomeTableAdapters.InfoComedorUsuarioComentariosTableAdapter();
                return _InfoComedorUsuarioComentarios;
            }
        }


        private DsUCHomeTableAdapters.InfoComedorUsuarioVotacionTableAdapter _InfoComedorUsuarioVotacion;

        public DsUCHomeTableAdapters.InfoComedorUsuarioVotacionTableAdapter InfoComedorUsuarioVotacion
        {
            get
            {
                if (_InfoComedorUsuarioVotacion == null) _InfoComedorUsuarioVotacion = new DsUCHomeTableAdapters.InfoComedorUsuarioVotacionTableAdapter();
                return _InfoComedorUsuarioVotacion;
            }
        }


        public DsUCHomeTableAdapters.UbicacionTableAdapter _Ubicacion;

        public DsUCHomeTableAdapters.UbicacionTableAdapter Ubicacion
        {
            get
            {
                if (_Ubicacion == null) _Ubicacion = new DsUCHomeTableAdapters.UbicacionTableAdapter();
                return _Ubicacion;
            }
        }

        public void Dispose()
        {
            this._Usuario = null;
            this._Efemerides = null;
            this._Santos = null;
        }

    }
}
