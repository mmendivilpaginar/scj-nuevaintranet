﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class Repository_UCHome
    {
        private AdapterDSUCHome _AdapterDSUCHome;
        public AdapterDSUCHome AdapterDSUCHome
        {
            get
            {
                if (_AdapterDSUCHome == null) _AdapterDSUCHome = new AdapterDSUCHome();
                return _AdapterDSUCHome;
            }
        }


        public DsUCHome.UsuarioDataTable GetCumpleanioHoy()
        {
            return AdapterDSUCHome.Usuario.GetCumpleanioHoy();
        }

        public DsUCHome.UsuarioDataTable GetCumpleanioSemana()
        {
            return AdapterDSUCHome.Usuario.GetCumpleanioSemana();
        }

        public DsUCHome.UsuarioDataTable GetCumpleanioMes(int? mes,int? cluster)
        {
            return AdapterDSUCHome.Usuario.GetCumpleanioMes(mes, cluster);
        }

        public DsUCHome.EfemeridesDataTable GetEfemeridesHoy(int clusterid)
        {
            return AdapterDSUCHome.Efemerides.GetEfemeridesHoy(clusterid);
        }

        public DsUCHome.EfemeridesDataTable GetEfemeridesMes(int clusterid)
        {
            return AdapterDSUCHome.Efemerides.GetEfemeridesMes(clusterid);
        }


        public DsUCHome.SantosDataTable GetSantosHoy()
        {
            return AdapterDSUCHome.Santos.GetSantosHoy();
        }

        public DsUCHome.SantosDataTable GetSantosMes()
        {
            return AdapterDSUCHome.Santos.GetSantosMes();
        }


        public DsUCHome.frmTramiteDevDataTable frmTramiteDev(int usralta)
        {
            try
            {
                return AdapterDSUCHome.frmTramitesDev.frmTramitesDev(usralta);
            }
            catch (Exception)
            {
                return (new DsUCHome.frmTramiteDevDataTable());
                //return AdapterDSUCHome.frmTramitesDev.frmTramitesDev(usralta);
            }
                      
        }

        public DsUCHome.frmTramiteDevDataTable frmMisTramiteDev(int usralta)
        {
            try
            {
                return AdapterDSUCHome.frmTramitesDev.frmMisTramitesDev(usralta);
            }
            catch (Exception)
            {
                return (new DsUCHome.frmTramiteDevDataTable());
                //return AdapterDSUCHome.frmTramitesDev.frmTramitesDev(usralta);
            }

        }

        public DsUCHome.frmTramiteDevDataTable frmTramitesAsignadosDev(int usrActual)
        {
            try
            {
                return AdapterDSUCHome.frmTramitesDev.frmTramitesAsignadosDev(usrActual);
            }
            catch (Exception)
            {
                return (new DsUCHome.frmTramiteDevDataTable());
                //return AdapterDSUCHome.frmTramitesDev.frmTramitesDev(usralta);
            }

        }




        public DsUCHome.UsuarioDataTable getDataUsuario(int usuarioid)
        {
            return AdapterDSUCHome.Usuario.getDataUsuario(usuarioid);
        }

        public DsUCHome.UsuarioDataTable GetDataUsuarioUbicacion(int usuarioid)
        {
            return AdapterDSUCHome.Usuario.GetDataUsuarioUbicacion(usuarioid);
        }

        public DsUCHome.UsuarioDataTable getDataUsuarioRandom()
        {
            return AdapterDSUCHome.Usuario.getDataUsuarioRamdom();
        }


        public DsUCHome.infComedorDataTable getComedorByID(int id)
        {
            return AdapterDSUCHome.InfoComedor.GetDataByID(id);
        }

        public DsUCHome.infComedorDataTable getComedorHoy(int ubicacionid)
        {
            return AdapterDSUCHome.InfoComedor.GetComedorHoy(ubicacionid);
        }

        public DsUCHome.infComedorDataTable getComedorSemana(int dia,int clusterid)
        {
            return AdapterDSUCHome.InfoComedor.GetComedorMes(dia,clusterid);
        }

        public DsUCHome.UbicacionDataTable GetUbicacion(int clusterid)
        {
            return AdapterDSUCHome.Ubicacion.GetUbicacion(clusterid);
        }

        public DsUCHome.UbicacionDataTable GetUbicacionTodas(int clusterid)
        {
            return AdapterDSUCHome.Ubicacion.GetUbicacionTodas(clusterid);
        }

        public DsUCHome.UbicacionDataTable GetUbicacionByID(string ubicacionid)
        {
            return AdapterDSUCHome.Ubicacion.GetUbicacionByID(ubicacionid);
        }

        public DsUCHome.infComedorDataTable GetComedorBuscador(string ubicacionid, DateTime? fecha)
        {
            return AdapterDSUCHome.InfoComedor.GetComedorBuscador(ubicacionid, fecha);
        }


        public bool UsuarioComedorVotoNoticia(int usuarioid, int id)
        {
            int? voto = (int?) AdapterDSUCHome.InfoComedorUsuarioVotacion.UsuarioVotoComedor(usuarioid, id);            
            if (voto == 1) return true;
            else return false;
        }

        public int? getVotos(int id, int voto)
        {
            return AdapterDSUCHome.InfoComedorUsuarioVotacion.getVotos(id, voto);
        }

        public DsUCHome.InfoComedorUsuarioComentariosDataTable getComments(int id)
        {
            return AdapterDSUCHome.InfoComedorUsuarioComentarios.GetComents(id);
        }

        public int? getCantCommets(int id)
        {
            return AdapterDSUCHome.InfoComedorUsuarioComentarios.GetCantComents(id);
        }

        public DsUCHome.UsuarioDataTable getBusquedaUsuario(string busqueda)
        {
            return AdapterDSUCHome.Usuario.GetBusquedaUsuario(busqueda);
        }

        public DsUCHome.UsuarioDataTable getUsuariosVotantesByInfoID(int infoId)
        {
            return AdapterDSUCHome.Usuario.getUsuariosVotantesByInfoID(infoId);
        }

        public DsUCHome.UsuarioDataTable getBusquedaUsuarioUbicacion(string busqueda, string ubicacion)
        {
            return AdapterDSUCHome.Usuario.GetBusquedaUsuarioUbicacion(busqueda, ubicacion);
        }

        public DsUCHome.UsuarioDataTable getBusquedaUsuarios(string busqueda, int? clusterid, string ubicacion)
        {
            return AdapterDSUCHome.Usuario.GetBusquedaUsuarios(busqueda, clusterid, ubicacion);
        }

    }
}
