﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSE_Learning
    {

        private DSE_LearningTableAdapters.EL_CursosTableAdapter _EL_Cursos;


        public DSE_LearningTableAdapters.EL_CursosTableAdapter EL_Cursos
        {
            get
            {
                if (_EL_Cursos == null) _EL_Cursos = new DSE_LearningTableAdapters.EL_CursosTableAdapter();
                return _EL_Cursos;

            }
        }
    }
}
