﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class AdapterDSReporteAdmin
    {
        private DSReporteAdminTableAdapters.CalificacionesPMPTableAdapter _Calificaiones;
        public DSReporteAdminTableAdapters.CalificacionesPMPTableAdapter Calificaiones
        {
            get 
            {
                if (_Calificaiones == null) _Calificaiones = new DSReporteAdminTableAdapters.CalificacionesPMPTableAdapter();
                return _Calificaiones;
            }
        }

        private DSReporteAdminTableAdapters.UsuarioTableAdapter _Usuarios;
        public DSReporteAdminTableAdapters.UsuarioTableAdapter Usuarios
        {
            get 
            {
                if (_Usuarios == null) _Usuarios = new DSReporteAdminTableAdapters.UsuarioTableAdapter();
                return _Usuarios;
            }
        }

        private DSReporteAdminTableAdapters.PlanAccionTableAdapter _Plan;
        public DSReporteAdminTableAdapters.PlanAccionTableAdapter Plan
        {
            get
            {
                if (_Plan == null) _Plan = new DSReporteAdminTableAdapters.PlanAccionTableAdapter();
                return _Plan;

            }
        }


        private DSReporteAdminTableAdapters.FortalezasNecesidadesGralTableAdapter _FortalezasNecesidadesGralTableAdapter;
        public DSReporteAdminTableAdapters.FortalezasNecesidadesGralTableAdapter FortalezasNecesidadesGralTableAdapter
        {
            get
            {
                if (_FortalezasNecesidadesGralTableAdapter == null) _FortalezasNecesidadesGralTableAdapter = new DSReporteAdminTableAdapters.FortalezasNecesidadesGralTableAdapter();
                return _FortalezasNecesidadesGralTableAdapter;

            }
        }

        private DSReporteAdminTableAdapters.CarrerPlanTableAdapter _CareerPlan;
        public DSReporteAdminTableAdapters.CarrerPlanTableAdapter CareerPlan
        {
            get 
            {
                if (_CareerPlan == null) _CareerPlan = new DSReporteAdminTableAdapters.CarrerPlanTableAdapter();
                return _CareerPlan;
            }
        }

        private DSReporteAdminTableAdapters.CompetenciasComoFortalezasTableAdapter _CompetenciasComoFortalezas;
        public DSReporteAdminTableAdapters.CompetenciasComoFortalezasTableAdapter CompetenciasComoFortalezas
        {
            get
            {
                if (_CompetenciasComoFortalezas == null) _CompetenciasComoFortalezas = new DSReporteAdminTableAdapters.CompetenciasComoFortalezasTableAdapter();
                return _CompetenciasComoFortalezas;
            }
        }

        private DSReporteAdminTableAdapters.CursosTableAdapter _Cursos;
        public DSReporteAdminTableAdapters.CursosTableAdapter Cursos
        {
            get 
            {
                if (_Cursos == null) _Cursos = new DSReporteAdminTableAdapters.CursosTableAdapter();
                return _Cursos;
            }
        }


        private DSReporteAdminTableAdapters.Eval_GetReporteObjetivosTableAdapter _Objetivos;
        public DSReporteAdminTableAdapters.Eval_GetReporteObjetivosTableAdapter Objetivos
        {
            get
            {
                if (_Objetivos == null) _Objetivos = new DSReporteAdminTableAdapters.Eval_GetReporteObjetivosTableAdapter();
                return _Objetivos;
            }
        }
    }
}
