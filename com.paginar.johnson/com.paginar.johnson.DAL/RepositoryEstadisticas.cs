﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.DAL
{
    public class RepositoryEstadisticas
    {
        private AdapterDSEstadisticas _AdapterDSEstadisticas;
        public AdapterDSEstadisticas AdapterDSEstadisticas
        {
            get
            {
                if (_AdapterDSEstadisticas == null) _AdapterDSEstadisticas = new AdapterDSEstadisticas();
                return _AdapterDSEstadisticas;
            }
        }



        public DSEstadisticas.LogDataTable GetEstadisticas_user_access_site(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Log.Get_User_access_site(modo, desde, hasta);
        }

        public DSEstadisticas.SeccionConsultadaDataTable Get_seccion_mas_consultada(string modo, DateTime desde, DateTime hasta) 
        { 
            return AdapterDSEstadisticas.Estadisticas1.Get_seccion_mas_consultada(modo, desde,  hasta);
        }

        public DSEstadisticas.SeccionConsultadaDataTable Get_banner_mas_consultado(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Estadisticas1.Get_banner_mas_consultado(modo, desde, hasta);
        }

        public DSEstadisticas.SeccionConsultadaDataTable Get_seccion_mas_consultada_gu(string modo, DateTime desde, DateTime hasta ,string titulo)
        {
            return AdapterDSEstadisticas.Estadisticas1.Get_Estadisticas_secc_mas_consult_gu(modo, desde, hasta);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_user_access_site_gu(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_user_access_site_gu(modo, desde, hasta);
        }

        public DSEstadisticas.GruposEtariosDataTable Get_grupos_etarios(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.GruposEtarios.Get_grupos_etarios(modo, desde, hasta);
        }

        public DSEstadisticas.Hombres_MujeresDataTable Get_Hombres_Mujeres(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Hombres_Mujeres.Get_hombres_mujeres(modo, desde, hasta);
        }

        public DSEstadisticas.HorariosConcurrenciaDataTable Get_horarios_concurrencia(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Horarios_Concurrencia.Get_horarios_concurrencia(modo, desde, hasta);
        }

        public DSEstadisticas.TopNoticiasDataTable Get_top_noticias(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Noticias.Get_top_noticias(modo, desde, hasta);
        }

        public DSEstadisticas.TopNoticiasDataTable Get_Estadisticas_noticias_mas_visitadas_gu(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Noticias.Get_Estadisticas_noticias_mas_visitadas_gu(modo, desde, hasta);
        }

        public DSEstadisticas.GruposEtariosDataTable Get_Estadisticas_horario_concurrencia_gu(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.GruposEtarios.Get_Estadisticas_horario_concurrencia_gu(modo, desde, hasta);
        }

        public DSEstadisticas.GruposEtariosDataTable Get_Estadisticas_grupo_etario_gu(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.GruposEtarios.Get_Estadisticas_grupo_etario_gu(modo, desde, hasta);
        }

        public DSEstadisticas.Hombres_MujeresDataTable Get_Estadisticas_hombres_mujeres_gu(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Hombres_Mujeres.Get_Estadisticas_hombres_mujeres_gu(modo,desde,hasta);
           
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_grupo_etario_detalle(string modo, DateTime desde, DateTime hasta, string rango)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_grupo_etario_detalle(modo, desde, hasta, rango);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_hombres_mujeres_detalle(string modo, DateTime desde, DateTime hasta, string genero)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_hombres_mujeres_detalle(modo, desde, hasta, genero);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_horario_concurrencia_detalle(string modo, DateTime desde, DateTime hasta, string rango)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_horario_concurrencia_detalle(modo, desde, hasta, rango);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_noticias_mas_visitadas_detalle(string modo, DateTime desde, DateTime hasta, string notid)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_noticias_mas_visitadas_detalle(modo, desde, hasta, notid);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_secc_mas_consult_detalle(string modo, DateTime desde, DateTime hasta, string titulo)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_secc_mas_consult_detalle(modo, desde, hasta, titulo);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_user_access_site_detalle(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Usuarios.Get_Estadisticas_user_access_site_detalle(modo, desde, hasta);
        }

        public DSEstadisticas.Hombres_MujeresDataTable Get_hombre_mujeres_pai(string modo, DateTime desde, DateTime hasta)
        {
            return AdapterDSEstadisticas.Hombres_Mujeres.Get_hombre_mujeres_pai(modo, desde, hasta);
        }
    }
}
