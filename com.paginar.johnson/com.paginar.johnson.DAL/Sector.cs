﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace com.paginar.johnson.DAL
{
    [MetadataType(typeof(MetaData_Sector))]
    [DisplayName("Sectores")]
    partial class Sector
    {
        partial void OnValidate(ChangeAction action)
        {
            var DC = new SCJBODataContext();
            if (action == ChangeAction.Delete)
            {
                if (DC.UsuarioBOs.Where(c => c.SectorID == this.SectorID).ToList().Count > 0)
                {
                    throw new ValidationException("No podrá eliminar el sector " + this.Descripcion + ", debido a que posee un usuario relacionado. Por Favor, reasigne el/los usuarios al Sector que corresponda.");
                }
            }
            else if (action == ChangeAction.Update || action == ChangeAction.Insert)
            {

                if (DC.Sectors.Where(c => c.Descripcion.ToLower().Trim() == this.Descripcion.ToLower().Trim()).ToList().Count > 0)
                {
                    throw new ValidationException(string.Format("Ya existe un sector con el nombre {0}",this.Descripcion));
                }

            }
        }

        public class MetaData_Sector
        {
            [DisplayName("Descripción")]
            [FilterUIHint("Search")]
            public object Descripcion { get; set; }

            [ScaffoldColumnAttribute(false)]
            public EntitySet<UsuarioBO> UsuarioBOs { get; set; }
        }
    }
}
