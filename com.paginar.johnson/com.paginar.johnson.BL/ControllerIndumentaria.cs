﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
     [DataObject]
    public class ControllerIndumentaria
    {
         private RepositoryIndumentaria _Respositorio;
         public RepositoryIndumentaria Repositorio
         {
             get 
             {
                 if (_Respositorio == null) _Respositorio = new RepositoryIndumentaria();
                 return _Respositorio;
             }
         }

         #region Indumentaria
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public DSIndumentaria.IndumentariaDataTable SeleccionarIndumentariaTodas()
         {
             return Repositorio.SeleccionarIndumentariaTodas();
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public DSIndumentaria.IndumentariaMigracionDataTable getPeriodoMigrar(int periodo)
         {
             return Repositorio.getPeriodoMigrar(periodo);
         }
         #endregion

         #region Tramite

         public int intRetorno(int? entero)
         {
             if (entero != null)
                 return (int)entero;
             else
                 return 0;
         
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
         public int IndumentariaActualizarPasoTramite(int tra_ID, int PasoActual)
         {
             return Repositorio.IndumentariaActualizarPasoTramite(tra_ID, PasoActual);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
         public int tramite_alta(int UsrNro, string Modo, int UsuarioId, string Apellido, string Nombre, int Legajo, string Sector, string Area)
         {
             int? Periodo = int.Parse(((DSIndumentaria.Indumentaria_PeriodoDataTable)(this.SeleccionarPeriodoActual())).Rows[0][0].ToString());
             if(Periodo == null)
                return 0;

             if (((DSIndumentaria.frmTramiteDataTable)this.IndumentariaTramiteActualSeleccionar(UsuarioId, intRetorno(Periodo))).Rows.Count == 0)
                 return int.Parse(Repositorio.tramite_alta(UsrNro, Modo, UsuarioId, Apellido, Nombre, Legajo, Sector, Area).ToString());
             else 
                 return 0;
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
         public int tramite_alta_Clon(int UsrNro, string Modo, int UsuarioId, string Apellido, string Nombre, int Legajo, string Sector, string Area)
         {
             return int.Parse(Repositorio.tramite_alta(UsrNro, Modo, UsuarioId, Apellido, Nombre, Legajo, Sector, Area).ToString());
         }


         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
         public int tramite_iniciar(int UsrNro, string Modo, int UsuarioId, string Apellido, string Nombre, int Legajo, string Sector, string Area)
         {

             int idTramite = int.Parse(this.tramite_alta(UsrNro, Modo, UsuarioId, Apellido, Nombre, Legajo, Sector, Area).ToString());
             //int Periodo = int.Parse(((DSIndumentaria.Indumentaria_PeriodoDataTable)(this.SeleccionarPeriodoActual())).Rows[0][0].ToString());
             //foreach (var item in this.SeleccionarIndumentariaTodas())
             //{
             //    //this.IndumentariaPedidoInsertar(idTramite, Periodo, item.IndumentariaId, "",);
             //}

             return idTramite;
         }
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]

         public DSIndumentaria.frmTramiteDataTable IndumentariaTramiteActualSeleccionar(int UsuarioId, int PeriodoId)
         {
             return Repositorio.IndumentariaTramiteActualSeleccionar(UsuarioId, PeriodoId);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
         public DSIndumentaria.frmTramiteDataTable IndumentariaTramiteSeleccionarPeriodoId(int PeriodoId)
         {
             return Repositorio.IndumentariaTramiteSeleccionarPeriodoId(PeriodoId);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public DSIndumentaria.TramitesNoIniciadosDataTable getUsuariosNoIniciaronTramite(int periodoid)
         {
             return Repositorio.getUsuariosNoIniciaronTramite(periodoid);
         }

        #endregion

        #region Pedido
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
         public void IndumentariaPedidoInsertar(int tra_ID, int PeriodoId, int IndumentariaId, string Talle, bool esBrigadista)
         {
             Repositorio.IndumentariaPedidoInsertar(tra_ID, PeriodoId, IndumentariaId, Talle, esBrigadista);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
         public void IndumentariaPedidoActualizar(int tra_ID, int IndumentariaId, string Talle, bool esBrigadista)
         {
             Repositorio.IndumentariaPedidoActualizar(tra_ID, IndumentariaId, Talle, esBrigadista);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public DSIndumentaria.IndumentariaPedidoDataTable IndumentariaPedidoSeleccionar(int tra_ID)
         {
             return Repositorio.IndumentariaPedidoSeleccionar(tra_ID);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
         public void IndumentariaPedidoEliminar(int tra_ID)
         {
             Repositorio.IndumentariaPedidoEliminar(tra_ID);
         }
        #endregion

        #region Periodo
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
         public int IndumentariaPeriodoInsertar(DateTime Desde, DateTime Hasta, string Descripcion) 
         {
            return Repositorio.IndumentariaPeriodoInsertar(Desde, Hasta, Descripcion);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
         public void IndumentariaPeriodoEditar(DateTime Desde, DateTime Hasta, string Descripcion, int PeriodoId)
         {
             Repositorio.IndumentariaPeriodoEditar(Desde, Hasta, Descripcion, PeriodoId);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public DSIndumentaria.Indumentaria_PeriodoDataTable IndumentariaPeriodoSeleccionarPorId(int PeriodoId)
         {
             return Repositorio.IndumentariaPeriodoSeleccionarPorId(PeriodoId);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public DSIndumentaria.Indumentaria_PeriodoDataTable SeleccionarPeriodoActual()
         {
             return Repositorio.SeleccionarPeriodoActual();
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public DSIndumentaria.Indumentaria_PeriodoDataTable SeleccionarPeriodosTodos()
         {
             return Repositorio.SeleccionarPeriodosTodos();
         }

         public string GetIdLastPeriodo()
         {
             return Repositorio.GetIdLastPeriodo();
         }
        #endregion
    }
}
