﻿using System;
using System.Collections.Generic;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    public class Visor4_BL
    {

        private Visor4_DataSet Visor4DS = new Visor4_DataSet();


        private RepositoryVisor4 _Repositorio = new RepositoryVisor4();
        public RepositoryVisor4 Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryVisor4();
                return _Repositorio;
            }
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public Visor4_DataSet.Visor4_GetRetencionesDataTable GetRetenciones(int Legajo, int Mes, int Anio)
        {
            Visor4_DataSet.Visor4_GetRetencionesDataTable tableReturn = null;

            tableReturn= Repositorio.GetRetenciones(Legajo, Mes, Anio);

            this.TranformarData(tableReturn);

            return tableReturn;
        }


        public string GetRetencionesInText(Visor4_DataSet.Visor4_GetRetencionesDataTable table)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Visor4_DataSet.Visor4_GetRetencionesRow row in table)
            {
                //string cadena = row.SOB_LINEA.Replace("¾", "ó").Replace("±", "ñ").Replace("Ý", "í").Replace("Þ", "é");
                ////sb.AppendLine(row.SOB_LINEA);
                sb.AppendLine(row.SOB_LINEA.Replace("¾", "ó").Replace("±", "ñ").Replace("Ý", "í").Replace("Þ", "é"));
            }

            return sb.ToString();
        }

        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public Visor4_DataSet.Visor4_GetAniosLiquidadosDataTable GetAniosLiquidados(int Legajo)
        {
            Visor4_DataSet.Visor4_GetAniosLiquidadosDataTable tablereturn = null;
            
            tablereturn = Repositorio.GetAniosLiquidados(Legajo);
            
            return tablereturn;
        }

        
        private void TranformarData(Visor4_DataSet.Visor4_GetRetencionesDataTable tableReturn)
        {
            foreach (Visor4_DataSet.Visor4_GetRetencionesRow row in tableReturn)
            {
                
                
            }
        }

        //Comentario
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public Visor4_DataSet.Visor4_GetMesUltimaLiquidacionDataTable GetMesUltimaLiquidacion(int Legajo)
        {
            Visor4_DataSet.Visor4_GetMesUltimaLiquidacionDataTable tablereturn = null;

            tablereturn = Repositorio.GetMesUltimaLiquidacion(Legajo);

            return tablereturn;
        }
        

    }

}
