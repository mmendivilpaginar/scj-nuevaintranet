﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.BL
{
    public class ControllerSeguridad
    {
        private DSSeguridad DSSeguridad = new DSSeguridad();
        private RepositorySeguridad _Repositorio = new RepositorySeguridad();
        DSSeguridad.LoginDataTable LoginDt;
        DSSeguridad.LoginRow LoginActual;

        public ControllerSeguridad()
        {
            LoginDt = new DSSeguridad.LoginDataTable();
        }
        public RepositorySeguridad Repositiorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositorySeguridad();
                return _Repositorio;
            }
        }


        public DSSeguridad.LoginDataTable getLoginByUserName(string UserAD)
        {

            return Repositiorio.getByUserAD(UserAD);
        }

        public Usuario GetDatosUsuarioByUserName(string UserName)
        {
            LoginDt = Repositiorio.getByUserName(UserName);
            Usuario U = new Usuario();
            foreach (DSSeguridad.LoginRow item in LoginDt.Rows)
            {
                U.apellido = item.Apellido;
                U.nombre = item.Nombre;
                U.legajo = item.Legajo;
                U.email = item.Email;
                U.usuarioid = item.UsuarioID;
                U.clusterID = item.ClusterID;
                ControllerCluster cl= new ControllerCluster();                
                U.clusteriddefecto = cl.getClusterIDDefectoUsuario(item.UsuarioID);
                U.clusteridactual = U.clusteriddefecto;
                U.Identif = item.Identif;
                U.Clave = item.Clave;
            }
            return U;
        }
        public string GetUserNameByUserAD(string UserAD)
        {
            string UserName = null;
            LoginDt = this.getLoginByUserName(UserAD);
            foreach (DSSeguridad.LoginRow item in LoginDt.Rows)
            {
                return item.Identif;
            }
            return UserName;
        }


        public void GuardarLog(int usuarioid, DateTime fecha, string ipaddress)
        {
            DSSeguridad.LogRow row = DSSeguridad.Log.NewLogRow();
            row.usuarioID = usuarioid;
            row.Fecha = fecha;
            row.IPaddress = ipaddress;
            DSSeguridad.Log.AddLogRow(row);
            Repositiorio.AdapterDSSeguridad.LogTableAdapter.Update(DSSeguridad.Log);
        }
    }
}
