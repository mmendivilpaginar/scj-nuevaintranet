﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;


namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerExperiencia
    {
        private DSExperiencia DSExperiencia = new DSExperiencia();
        private RepositoryExperiencia _Repositorio;
        public RepositoryExperiencia Repositorio
        { 
            get
            {
                if (_Repositorio == null)
                    _Repositorio = new RepositoryExperiencia();
                return _Repositorio;
            }
        }

        public void AgregarLinks(string path, int idPublicacion)
        {
            DSExperiencia.exp_LinkRow row = DSExperiencia.exp_Link.Newexp_LinkRow();
            row.Path = path;
            row.PublicacionId = idPublicacion;
            DSExperiencia.exp_Link.Addexp_LinkRow(row);
            Repositorio.AdapterDSExperiencia.exp_LinkTable.Update(DSExperiencia.exp_Link);
        }

        public void BorrarLinks(int idPublicacion)
        {
            Repositorio.AdapterDSExperiencia.exp_LinkTable.DeleteLinkByPublicacionId(idPublicacion);
        }

        public int AgregarPublicacion(int usuarioid, DateTime fecha, string titulo, string descripcion, bool estado, string link, DataTable dtlinks)
        {
            DSExperiencia.exp_PublicacionRow row = DSExperiencia.exp_Publicacion.Newexp_PublicacionRow();
            row.UsuarioID = usuarioid;
            row.Fecha = fecha;
            row.Titulo = titulo;
            row.Descripcion = descripcion;
            row.Estado = estado;
            row.Link = link;
            DSExperiencia.exp_Publicacion.Addexp_PublicacionRow(row);
            Repositorio.AdapterDSExperiencia.exp_Publicacion.Update(DSExperiencia.exp_Publicacion);
            int publicacionid = row.PublicacionId;
            foreach (DataRow dr in dtlinks.Rows)
            {
                AgregarLinks(dr["Path"].ToString(),publicacionid);
            }

            return publicacionid;
        }
        
        public void ModificarPublicacion(int publicacionid, int usuarioid, DateTime fecha, string titulo, string descripcion, bool estado,string link, DataTable dtlinks)
        {
            Repositorio.AdapterDSExperiencia.exp_Publicacion.FillBy(DSExperiencia.exp_Publicacion, publicacionid);
            DSExperiencia.exp_PublicacionRow row = DSExperiencia.exp_Publicacion.FindByPublicacionId(publicacionid);
            row.Fecha = fecha;
            row.Titulo = titulo;
            row.Descripcion = descripcion;
            row.Estado = estado;
            row.Link = link;
            Repositorio.AdapterDSExperiencia.exp_Publicacion.Update(DSExperiencia.exp_Publicacion);
            BorrarLinks(publicacionid);
            foreach(DataRow dr in dtlinks.Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                    AgregarLinks(dr["Path"].ToString(), publicacionid);
            }
            
        }

        public void BorrarPulbicacion(int idPublicacion)
        {
            BorrarLinks(idPublicacion);
            Repositorio.AdapterDSExperiencia.exp_Publicacion.Delete1(idPublicacion);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_LinkDataTable getLinksByID(int publicacionid)
        {
            return Repositorio.AdapterDSExperiencia.exp_LinkTable.GetDataBy1(publicacionid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_PublicacionDataTable getPublicacion(int idPublicacion)
        {
            return Repositorio.AdapterDSExperiencia.exp_Publicacion.GetDataBy1(idPublicacion);
        }
    
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSExperiencia.exp_BuscadorDataTable getPublicaciones(int clusterid, int usuarioid, bool estado)
        {
            return Repositorio.AdapterDSExperiencia.exp_BuscadorTable.GetData(clusterid, usuarioid, estado);
                 
        }

        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public int setLike(int PulblicacionId, int UsuarioId, DateTime Date)
        {
            return Repositorio.AdapterDSExperiencia.exp_LikesTable.InsertLike(PulblicacionId, UsuarioId, Date, true);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_LikesDataTable getLikes(int PublicacionId)
        {
            return Repositorio.AdapterDSExperiencia.exp_LikesTable.GeLikes(PublicacionId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_LikesDataTable UserLikeThis(int publicacionid, int usuarioid)
        {
            return Repositorio.AdapterDSExperiencia.exp_LikesTable.userLikeThis(publicacionid, usuarioid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_ComentariosDataTable getComentariosByPublicacionId(int publicacionId)
        {
            return Repositorio.AdapterDSExperiencia.exp_ComentariosTable.GetDataByPublicacionId(publicacionId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void InsertCommentByPublicacionId(int UsuarioId,string Comentario,DateTime Fecha, int PublicacionId)
        {
            Repositorio.AdapterDSExperiencia.exp_ComentariosTable.InsertCommentByPublicacion(UsuarioId, Comentario, Fecha, PublicacionId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_BuscadorDataTable getPublicacionByID(int PublicacionId)
        {
            return Repositorio.AdapterDSExperiencia.exp_BuscadorTable.GetpublicaiconById(PublicacionId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSExperiencia.exp_ComentariosDataTable GetMessagesCount(int publicacionId)
        {
            return Repositorio.AdapterDSExperiencia.exp_ComentariosTable.GetMessagesCount(publicacionId);
        }

        

    
    }
}
