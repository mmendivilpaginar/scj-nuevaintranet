﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerAliadosComunidad
    {
        private RepositoryRCAliados _Repositorio;
        public RepositoryRCAliados Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryRCAliados();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSRCAliados.com_AliadoDataTable com_aliadosSelectAll()
        {
            return Repositorio.com_aliadosSelectAll();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSRCAliados.com_AliadoDataTable com_aliadosSelectById(int idAliado)
        {
            return Repositorio.com_aliadosSelectById(idAliado);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSRCAliados.com_AliadoDataTable com_aliadoSelectActivos()
        {
            return Repositorio.com_aliadoSelectActivos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void com_aliadosInsert(string Nombre, string QueHace, string QueHara, string web, bool? Activo, string imgLogo, int tipo)
        {
            bool iActivo;
            if (Activo == null)
                iActivo = false;
            else
                iActivo = (bool)Activo;
            Repositorio.com_aliadosInsert(Nombre, QueHace, QueHara, web, iActivo, imgLogo, tipo);
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void com_aliadosUpdate(int idAliado, string Nombre, string QueHace, string QueHara, string web, bool Activo, string imgLogo, int tipo)
        {
            bool iActivo;
            if (Activo == null)
                iActivo = false;
            else
                iActivo = (bool)Activo;
            Repositorio.com_aliadosUpdate(idAliado, Nombre, QueHace, QueHara, web, iActivo, imgLogo, tipo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSRCAliados.com_AliadoDataTable com_aliadosSelectActivosByTipo(int tipo)
        {
            return Repositorio.com_aliadosSelectActivosByTipo(tipo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void com_aliadosDelete(int idAliado)
        {
            Repositorio.com_aliadosDelete(idAliado);
        }





    }
}
