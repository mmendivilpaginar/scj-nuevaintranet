﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;


namespace com.paginar.johnson.BL
{

    [DataObject]
    public class ControllerCluster
    {
        private RepositoryCluster _Repositorio = new RepositoryCluster();

        public RepositoryCluster Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryCluster();
                return _Repositorio;
            }
        }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]        
       public DCCluster.ClusterDataTable getCluster()
       {
            return Repositorio.getCluster();
       }



       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]        
       public DCCluster.ClusterDataTable getClusterDefectoUsuario(int usuarioid)
       {
           return Repositorio.getClusterDefectoUsuario(usuarioid);
       }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]        
        public DCCluster.ClusterDataTable getClusterUsuario(int usuarioid)
       {
           return Repositorio.getClusterUsuario(usuarioid);
       }

       public int getClusterIDDefectoUsuario(int usuarioid)
       {
           int clusterid = 0;
           DCCluster.ClusterDataTable dt= getClusterDefectoUsuario(usuarioid);
           if (dt.Rows.Count > 0)
           {
               DCCluster.ClusterRow dr = (DCCluster.ClusterRow)dt.Rows[0];
               clusterid = dr.ID;
           }

           return clusterid;
       }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
       public DCCluster.RegionPaisDataTable GetPaisesHorario()
       {
           return Repositorio.GetPaisesHorario();
       }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
       public double GetClusterGMT(int clusterid)
       {
           DCCluster.RegionPaisDataTable dt = Repositorio.GetClusterGMT(clusterid);
           if (dt.Rows.Count > 0)
           {
               DCCluster.RegionPaisRow dr = (DCCluster.RegionPaisRow)dt.Rows[0];
               return dr.gmt;
           }
           else
               return 0;
       }

    }
}
