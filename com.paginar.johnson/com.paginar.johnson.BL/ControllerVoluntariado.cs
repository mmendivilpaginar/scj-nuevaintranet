﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerVoluntariado
    {
        private RepositoryVoluntariado _Repositorio;
        public RepositoryVoluntariado Repositorio
        {
            get 
            {
                if (_Repositorio == null) _Repositorio = new RepositoryVoluntariado();
                return _Repositorio;
            }
        }

        //[DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]

        #region Evento
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public int EventoInsert(string Nombre, string Descripcion, DateTime Fecha)
        {
            return int.Parse(Repositorio.EventoInsert(Nombre, Descripcion, Fecha).ToString());
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void EventoUpdate(int IdEvento, string Nombre, string Descripcion, DateTime Fecha)
        {
            Repositorio.EventoUpdate(IdEvento, Nombre, Descripcion, Fecha);
        
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void EventoDelete(int IdEvento)
        {
            Repositorio.EventoDelete(IdEvento);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_eventoDataTable EventoSelect(int IdEvento)
        {
            return Repositorio.EventoSelect(IdEvento);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_eventoDataTable EventoSelectAll()
        {
            return Repositorio.EventoSelectAll();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_eventoDataTable EventoSelectWImagen()
        {
            return Repositorio.EventoSelectWImagen();
        }

        #endregion

        #region Imagen

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public int ImagenInsert(int IdEvento, string Imagen, string Descripcion)
        {
            return int.Parse(Repositorio.ImagenInsert(IdEvento, Imagen, Descripcion).ToString());

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void ImagenDelete(int IdImagen)
        {
            Repositorio.ImagenDelete(IdImagen);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void ImagenUpdate(int IdImagen, int IdEvento, string Imagen, string Descripcion)
        {
            Repositorio.ImagenUpdate(IdImagen, IdEvento, Imagen, Descripcion);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_eventoImgDataTable ImagenSelect(int IdImagen)
        {
            return Repositorio.ImagenSelect(IdImagen);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_eventoImgDataTable ImagenSelectByEvento(int IdEvento)
        {
            return Repositorio.ImagenSelectByEvento(IdEvento);
        }


        #endregion

        #region Actividad

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_actividadDataTable ActividadSelect(int IdActividad)
        {
            return Repositorio.ActividadSelect(IdActividad);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_actividadDataTable ActividadSelectActivas()
        {
            return Repositorio.ActividadSelectActivas();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public int ActividadInsert(string Actividad, string Descripcion, string Que, string Cuando, string AQuienes, string Imagen, bool Estado)
        {
            return int.Parse(Repositorio.ActividadInsert(Actividad, Descripcion, Que, Cuando, AQuienes, Imagen, Estado).ToString());
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void ActividadUpdate(int IdActividad, string Actividad, string Descripcion, string Que, string Cuando, string AQuienes, string Imagen, bool Estado)
        {
            Repositorio.ActividadUpdate(IdActividad, Actividad, Descripcion, Que, Cuando, AQuienes, Imagen, Estado);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void ActividadDelete(int IdActividad)
        {
            Repositorio.ActividadDelete(IdActividad);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_actividadDataTable ActividadSelectAll()
        {
            return Repositorio.AdapterDSVoluntariado.VolActividad.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_actividadDataTable ActividadSeleccionarDisponibles(int Legajo)
        {
            return Repositorio.ActividadSeleccionarDisponibles(Legajo);
        }
        #endregion

        #region Inscripcion Actividad
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void ActividadInscripcion(int IdActividad, int Legajo, int Cluster, string Comentarios, bool Notificado)
        {
            Repositorio.ActividadInscripcion(IdActividad, Legajo, Cluster, Comentarios, Notificado);
        }

        //[DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        //public DSVoluntariado.vol_actividadInscriptosDataTable ActividadInscriptos(int IdInscriptos)
        //{
        //    return Repositorio.ActividadInscriptos(IdInscriptos);
        //}

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_actividadInscriptosDataTable InscriptosVoluntariados(int idVol)
        {
            return Repositorio.InscriptosVoluntariados(idVol);
        }

        //public DSVoluntariado.vol_actividadInscriptosDataTable ActividadInscriptosDos(int IdInscriptos)
        //{
        //    return Repositorio.ActividadInscriptosDos(IdInscriptos);
        //}

        public void ActividadInscriptosDelete(int IdActividad)
        {
            Repositorio.ActividadInscriptosDelete(IdActividad);
        }
        #endregion

        #region Cronograma

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_CronogramaDataTable CronogramaSelectAll()
        {
            return Repositorio.CronogramaSelectAll();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_CronogramaDataTable CronocramaSelect(int IdCronograma)
        {
            return Repositorio.CronocramaSelect(IdCronograma);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_CronogramaDataTable CronogramaSelectActivos()
        {
            return Repositorio.CronogramaSelectActivos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void CronogramaInsert(string Actividad, bool Estado)
        {
            Repositorio.CronogramaInsert(Actividad, Estado);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void CronogramaUpdate(int IdCronograma, string Actividad, bool Estado)
        {
            Repositorio.CronogramaUpdate(IdCronograma, Actividad, Estado);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void CronogramaDelete(int IdCronograma)
        {
            Repositorio.CronogramaDelete(IdCronograma);
        }


        #endregion

        #region Cronograma Fechas

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_CronogramaFechaDataTable CronogramaFechaByCronSelect(int IdCronograma)
        {
            return Repositorio.CronogramaFechaByCronSelect(IdCronograma);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSVoluntariado.vol_CronogramaFechaDataTable CronogramaFechaSelect(int IdCronogramaFecha)
        {
            return Repositorio.CronogramaFechaSelect(IdCronogramaFecha);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void CronogramaFechaInsert(int IdCronograma, string Fecha, bool TBD)
        {
            Repositorio.CronogramaFechaInsert(IdCronograma, Fecha, TBD);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void CronogramaFechaUpdate(int IdCronogramaFecha, int IdCronograma, string Fecha, bool TBD)
        {
            Repositorio.CronogramaFechaUpdate(IdCronogramaFecha, IdCronograma, Fecha, TBD);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void CronogramaFechaDelete(int IdCronogramaFecha)
        {
            Repositorio.CronogramaFechaDelete(IdCronogramaFecha);
        }

        #endregion

        #region reportes
        DSVoluntariado.vol_ActividadSelectReporteDataTable DT = new DSVoluntariado.vol_ActividadSelectReporteDataTable();
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVoluntariado.vol_ActividadSelectReporteDataTable vol_ActividadSelectReporte(bool todos)
        {
            string AuxActividad = string.Empty;
            
            
            foreach (DSVoluntariado.vol_ActividadSelectReporteRow item in  Repositorio.vol_ActividadSelectReporte(todos))
            {
                DSVoluntariado.vol_ActividadSelectReporteRow r = DT.Newvol_ActividadSelectReporteRow();
                if (AuxActividad == item.Actividad)
                {
                    r.legajo = item.legajo;
                    r.Apellido = item.Apellido;
                    r.Nombre = item.Nombre;
                    r.FechaInscripsion = item.FechaInscripsion;
                    //r.Actividad = item.Actividad;
                    r.AQuienes = item.AQuienes;
                    r.Cuando = item.Cuando;
                    r.Descripcion = item.Descripcion;
                    r.Estado = item.Estado;
                    r.IdActividad = item.IdActividad;
                    r.Imagen = item.Imagen;
                    r.Que = item.Que;

                }
                else
                {
                    r.legajo = item.legajo;
                    r.Apellido = item.Apellido;                    
                    r.Nombre = item.Nombre;
                    r.FechaInscripsion = item.FechaInscripsion;                    
                    r.Actividad = item.Actividad;                    
                    r.AQuienes = item.AQuienes;
                    r.Cuando = item.Cuando;
                    r.Descripcion = item.Descripcion;
                    r.Estado = item.Estado;
                    r.IdActividad = item.IdActividad;
                    r.Imagen = item.Imagen;
                    r.Que = item.Que;

                    AuxActividad = item.Actividad;
                    
                }

                DT.Addvol_ActividadSelectReporteRow(r);
            }
            return DT;
        }
        #endregion

    }
}
