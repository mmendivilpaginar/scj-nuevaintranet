﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerEstadisticas
    {
        private DSEstadisticas EstadisticasDS = new DSEstadisticas();

        private RepositoryEstadisticas _Repositorio;
        public RepositoryEstadisticas Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryEstadisticas();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.EstadisticasDataTable SelectEstadistica()
        {
            return Repositorio.AdapterDSEstadisticas.Estadisticas.GetData();

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertRegistroEstadistico(int UsuarioID, string paginaVisitada, string titulo, string Tipo)
        {
            Repositorio.AdapterDSEstadisticas.Estadisticas.SetNewRegisterEstadistic(UsuarioID, paginaVisitada, titulo, Tipo);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.LogDataTable SelectLog()
        {
            return Repositorio.AdapterDSEstadisticas.Log.GetData(); ;

        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.LogDataTable GetEstadisticas_user_access_site(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Log.Get_User_access_site(modo, desde, hasta);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_user_access_site_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_user_access_site_gu(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.SeccionConsultadaDataTable Get_Estadisticas_secc_mas_consult_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Estadisticas1.Get_Estadisticas_secc_mas_consult_gu(modo, desde, hasta);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.SeccionConsultadaDataTable Get_seccion_mas_consultada(string modo, DateTime desde, DateTime hasta)
        {
            return  Repositorio.AdapterDSEstadisticas.Estadisticas1.Get_seccion_mas_consultada(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSEstadisticas.SeccionConsultadaDataTable Get_banner_mas_consultado(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Estadisticas1.Get_banner_mas_consultado(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.GruposEtariosDataTable Get_grupos_etarios(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.GruposEtarios.Get_grupos_etarios(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.Hombres_MujeresDataTable Get_Hombres_Mujeres(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Hombres_Mujeres.Get_hombres_mujeres(modo, desde, hasta);
        }
        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.HorariosConcurrenciaDataTable Get_horarios_concurrencia(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Horarios_Concurrencia.Get_horarios_concurrencia(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.TopNoticiasDataTable Get_top_noticias(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Noticias.Get_top_noticias(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.TopNoticiasDataTable Get_Estadisticas_noticias_mas_visitadas_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Noticias.Get_Estadisticas_noticias_mas_visitadas_gu(modo, desde, hasta);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.GruposEtariosDataTable Get_Estadisticas_horario_concurrencia_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.GruposEtarios.Get_Estadisticas_horario_concurrencia_gu(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.GruposEtariosDataTable Get_Estadisticas_grupo_etario_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.GruposEtarios.Get_Estadisticas_grupo_etario_gu(modo, desde, hasta);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_grupo_etario_detalle(string modo, DateTime desde, DateTime hasta, string rango)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_grupo_etario_detalle(modo, desde, hasta, rango);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_hombres_mujeres_detalle(string modo, DateTime desde, DateTime hasta, string genero)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_hombres_mujeres_detalle(modo, desde, hasta, genero);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_horario_concurrencia_detalle(string modo, DateTime desde, DateTime hasta, string rango)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_horario_concurrencia_detalle(modo, desde, hasta, rango);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_noticias_mas_visitadas_detalle(string modo, DateTime desde, DateTime hasta, string notid)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_noticias_mas_visitadas_detalle(modo, desde, hasta, notid);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_secc_mas_consult_detalle(string modo, DateTime desde, DateTime hasta, string titulo)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_secc_mas_consult_detalle(modo, desde, hasta, titulo);
        }

        public DSEstadisticas.UsuarioDataTable Get_Estadisticas_user_access_site_detalle(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_user_access_site_detalle(modo, desde, hasta);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSEstadisticas.UsuarioDataTable Get_banner_mas_consultado_detalle(string modo, DateTime desde, DateTime hasta,string titulo)
        {
            return Repositorio.AdapterDSEstadisticas.Usuarios.Get_Estadisticas_Banner_mas_consult_detalle(modo, desde, hasta,titulo);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.Hombres_MujeresDataTable Get_Estadisticas_hombre_mujeres_pai(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Hombres_Mujeres.Get_hombre_mujeres_pai(modo, desde, hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSEstadisticas.Hombres_MujeresDataTable Get_Estadisticas_hombres_mujeres_gu(string modo, DateTime desde, DateTime hasta)
        {
            return Repositorio.AdapterDSEstadisticas.Hombres_Mujeres.Get_Estadisticas_hombres_mujeres_gu(modo, desde, hasta);
        }
    }
}
