﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using com.paginar.johnson.DAL;


namespace com.paginar.johnson.BL
{
   [DataObject]
    public class ControllerAgendaTel
    {
        
       private RespositoryAgendaTel _Repositorio = new RespositoryAgendaTel();
       public RespositoryAgendaTel Repositorio
       {
           get
           {
               if (_Repositorio == null) _Repositorio = new RespositoryAgendaTel();
               return _Repositorio;
           }
       }


       DSAgendaTel AgendaTelDS = new DSAgendaTel();

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
       public DSAgendaTel.AgendaTelUbicacionDataTable GetUbicacionbyCluster(int clusterid)
       {
           return _Repositorio.AdapterDSAgendaTel.AgendaTelUbicacionTableAdapter.GetDataByClusterID(clusterid);
       }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
       public DSAgendaTel.AgendaTelDataTable GetBusqueda(int? clusterid, int? ubicacionid, string busqeuda)
       {
           return Repositorio.AdapterDSAgendaTel.AgendaTel.GetBusqueda(clusterid, ubicacionid, busqeuda);
       }

       [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
       public DSAgendaTel.AgendaTelDataTable GetDataByID(int agendatelid)
       {
           return Repositorio.AdapterDSAgendaTel.AgendaTel.GetDataByID(agendatelid);
       }
    }
}
