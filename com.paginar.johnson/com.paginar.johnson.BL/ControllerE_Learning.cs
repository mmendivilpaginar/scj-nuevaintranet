﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
   [DataObject]
   public  class ControllerE_Learning
    {

        private DSE_Learning E_LearningDS = new DSE_Learning();

        private RepositoryE_Learning _Repositorio;
        public RepositoryE_Learning Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryE_Learning();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSE_Learning.EL_CursosDataTable getAllCursos()
        {
            return Repositorio.GetAllCursos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSE_Learning.EL_CursosDataTable getCursosAdministrativos()
        {
            return Repositorio.GetCursosAdministrativos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSE_Learning.EL_CursosDataTable getCursosOperarios()
        {
            return Repositorio.GetCursosOperarios();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSE_Learning.EL_CursosDataTable getCursosByDescripcionCodigo(string Descripcion, string Codigo)
        {
            return Repositorio.GetCursosByDescripcionCodigo(Descripcion,Codigo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSE_Learning.EL_CursosDataTable GetCursoByID(int CursoID)
        {
            return Repositorio.GetCursoByID(CursoID);
        }



        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void EL_CursoInsert(string Descripcion, string Link, string Codigo, string OfferingID, bool Operarios, bool Administrativos)
        {

            DSE_Learning.EL_CursosRow Curso = E_LearningDS.EL_Cursos.NewEL_CursosRow();
            Curso.Descripcion = Descripcion;
            Curso.Codigo = Codigo;
            Curso.Link = Link;
            Curso.OfferingID = OfferingID;
            Curso.Operarios = Operarios;
            Curso.Administrativos = Administrativos;
            E_LearningDS.EL_Cursos.AddEL_CursosRow(Curso);

            Repositorio.AdapterDSE_Learning.EL_Cursos.Update(E_LearningDS.EL_Cursos);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void EL_CursoModificar(string Descripcion, string Link, string Codigo, string OfferingID, int CursoID, bool Operarios, bool Administrativos)
        {
            Repositorio.AdapterDSE_Learning.EL_Cursos.FillByID(E_LearningDS.EL_Cursos, CursoID);
            DSE_Learning.EL_CursosRow RowCurso = E_LearningDS.EL_Cursos.FindByCursoID(CursoID);

            if (RowCurso != null)
            {
                RowCurso.Descripcion = Descripcion;
                RowCurso.Codigo = Codigo;
                RowCurso.Link = Link;
                RowCurso.OfferingID = OfferingID;
                RowCurso.Operarios = Operarios;
                RowCurso.Administrativos = Administrativos;
                
                
            }
            Repositorio.AdapterDSE_Learning.EL_Cursos.Update(E_LearningDS.EL_Cursos);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void EL_CursoDelete(int CursoID)
        {
            Repositorio.AdapterDSE_Learning.EL_Cursos.FillByID(E_LearningDS.EL_Cursos,CursoID);

            DSE_Learning.EL_CursosRow RowCurso = E_LearningDS.EL_Cursos.FindByCursoID(CursoID);

            if (RowCurso != null)
            {
                RowCurso.Delete();
            }

            Repositorio.AdapterDSE_Learning.EL_Cursos.Update(E_LearningDS.EL_Cursos);
        }

        public string ExisteCodigo(string Codigo, int? CursoID)
        {

            return Repositorio.ExisteCodigo(Codigo, CursoID);
        }






       


    }
}
