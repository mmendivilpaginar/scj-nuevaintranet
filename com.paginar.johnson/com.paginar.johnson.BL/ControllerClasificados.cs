﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using com.paginar.johnson.utiles;
using System.Globalization;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerClasificados
    {
        private RepositoryClasificados _Repositorio = new RepositoryClasificados();
        DSClasificados ClasificadosDS = new DSClasificados();
        DSClasificados.clasiAvisoDataTable MisAvisosDt = new DSClasificados.clasiAvisoDataTable();

        public RepositoryClasificados Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryClasificados();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DataTable getBusquedaFE(string Texto, int? ClusterID, int? UbicacionID, int? RubroID, int? TipoAvisoID, bool limit)
        {

            ClusterID = (ClusterID == -1) ? null : ClusterID;
            UbicacionID = (UbicacionID == -1) ? null : UbicacionID;
            RubroID = (RubroID == -1) ? null : RubroID;
            TipoAvisoID = (TipoAvisoID == -1) ? null : TipoAvisoID;

  
           
            Repositorio.AdapterDSClasificados.clasiAviso.FillDataByBusquedaFE(ClasificadosDS.clasiAviso, Texto, ClusterID, UbicacionID, RubroID, TipoAvisoID);


            if (limit)
            {
                if (ClasificadosDS.clasiAviso.Rows.Count <= 0)
                    return ClasificadosDS.clasiAviso;
                else
                    //return DistinctRows(ClasificadosDS.clasiAviso.Take(20).CopyToDataTable(), "RubroID");
                    return ClasificadosDS.clasiAviso.Take(20).CopyToDataTable();
            }
            else
                return ClasificadosDS.clasiAviso;
                //return DistinctRows(ClasificadosDS.clasiAviso, "RubroID");
        }

       

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DataTable GetByUsrAlta(int UsrAlta)
        {
            Repositorio.AdapterDSClasificados.clasiAviso.FillDataByUsrAlta(MisAvisosDt, UsrAlta);
             //return ClasificadosDS.clasiAviso;
            //return DistinctRows(MisAvisosDt, "RubroID");
            return MisAvisosDt;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DataTable getBusquedaFEByRubroID(int RubroID)
        {
            DataView DVAvisos = new DataView(ClasificadosDS.clasiAviso, "RubroID=" + RubroID.ToString(), "", DataViewRowState.CurrentRows);
            return DVAvisos.ToTable();
          
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DataTable getBusquedaFEByRubroIDMisAvisos(int RubroID)
        {
            DataView DVAvisos = new DataView(MisAvisosDt, "RubroID=" + RubroID.ToString(), "", DataViewRowState.CurrentRows);
            return DVAvisos.ToTable();

        }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSClasificados.claAvisoComentariosDataTable getComentariosByAvisoID(int AvisoID)
        {
            return Repositorio.AdapterDSClasificados.claAvisoComentarios.GetDataByAvisoID(AvisoID);            

        }
        protected DataTable DistinctRows(DataTable dt, string keyfield)
        {
            DataTable newTable = null;
            int keyval = 0;
            DataView dv;

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    newTable = dt.Clone();
                    dv = dt.DefaultView;
                    dv.Sort = keyfield;

                    foreach (DataRow dr1 in dt.Rows)
                    {
                        bool existe = false;
                        foreach (DataRow dr2 in newTable.Rows)
                        {
                            if (dr1[keyfield].ToString() == dr2[keyfield].ToString())
                                existe = true;
                        }

                        if (!existe)
                        {
                            newTable.ImportRow(dr1);
                        }
                    }
                }
                else
                    newTable = dt.Clone();
            }
            return newTable;
        }



        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertarClasificado(int UsrAlta, DateTime FHAlta, int TipoAvisoID, int RubroID, string Titulo,
            string Descrip, string ArchivoAdjunto, string Link, string Email, string Telefono, DateTime FechaVencimiento, Boolean Habilitado)
        {
            DSClasificados.clasiAvisoDataTable ClasificadosDt = new DSClasificados.clasiAvisoDataTable();
            DSClasificados.clasiAvisoRow ClasificadoRow = ClasificadosDt.NewclasiAvisoRow();
            ClasificadoRow.UsrAlta = UsrAlta;
            ClasificadoRow.FHAlta = FHAlta;
            ClasificadoRow.DiasVigencia = FechaVencimiento.Subtract(FHAlta).Days;
            ClasificadoRow.FHPublicacion = FHAlta;
            ClasificadoRow.TipoAvisoID = TipoAvisoID;
            ClasificadoRow.RubroID = RubroID;
            ClasificadoRow.Titulo = Titulo;
            ClasificadoRow.Descrip = Descrip;
            ClasificadoRow.ArchivoAdjunto = ArchivoAdjunto;
            ClasificadoRow.Link = Link;
            ClasificadoRow.Email = Email;
            ClasificadoRow.Telefono = Telefono;
            ClasificadoRow.Habilitado = Habilitado;
            ClasificadosDt.AddclasiAvisoRow(ClasificadoRow);
            Repositorio.AdapterDSClasificados.clasiAviso.Update(ClasificadosDt);
        }

        public void InsertUSuarioComentario(int usuarioid, int AvisoID, string comentario, string Page)
        {
            DSClasificados.claAvisoComentariosDataTable ComDt = new DSClasificados.claAvisoComentariosDataTable();
            DSClasificados.claAvisoComentariosRow info = ComDt.NewclaAvisoComentariosRow();
            info.usuarioID = usuarioid;
            info.AvisoID = AvisoID;
            info.comentarios = comentario;
            info.Fecha = DateTime.Now;
            ComDt.AddclaAvisoComentariosRow(info);
            Repositorio.AdapterDSClasificados.claAvisoComentarios.Update(ComDt);
           
            DSClasificados.clasiAvisoDataTable ClasificadosDt = new DSClasificados.clasiAvisoDataTable();
            Repositorio.AdapterDSClasificados.clasiAviso.FillByAvisoID(ClasificadosDt, AvisoID);
            DSClasificados.clasiAvisoRow ClasiRow = ClasificadosDt.Rows[0] as DSClasificados.clasiAvisoRow;

            DSClasificados.UsuarioDataTable UsuarioDT = new DSClasificados.UsuarioDataTable();
            Repositorio.AdapterDSClasificados.Usuario.FillByUsuarioID(UsuarioDT, ClasiRow.UsrAlta);
            DSClasificados.UsuarioRow URow = UsuarioDT.FindByUsuarioID(ClasiRow.UsrAlta);
            string email = string.Empty;
            if (URow != null)
                email = URow.Email;
            if (!ClasiRow.IsEmailNull())
                email += "," + ClasiRow.Email;
            if (!string.IsNullOrEmpty(email))
            {
                Message M = new Message(email, string.Format("Comentario sobre su clasificado {0} ({1:dd/MM/yyyy})", ClasiRow.Titulo, ClasiRow.FHAlta), string.Format("Recibi&oacute un comentario sobre su clasificado {0}, publicado el {1:dd/MM/yyyy}.<p>{2}</p> ", ClasiRow.Titulo, ClasiRow.FHAlta, comentario.Truncate(25, "...", false) + "..." + string.Format("<a href='{0}'>Ver mas</a>", Page)));
                M.Send();
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateClasificado(int AvisoID, int TipoAvisoID, int RubroID, string Titulo, string Descrip, string ArchivoAdjunto, string Link, string Email, string Telefono, DateTime FechaVencimiento, Boolean Habilitado)
        {
            DSClasificados.clasiAvisoDataTable ClasificadosDt = new DSClasificados.clasiAvisoDataTable();
            Repositorio.AdapterDSClasificados.clasiAviso.FillByAvisoID(ClasificadosDt, AvisoID);
            foreach (DSClasificados.clasiAvisoRow item in ClasificadosDt.Rows)
            {                
                item.TipoAvisoID = TipoAvisoID;
                item.RubroID = RubroID;
                item.Titulo = Titulo;
                item.Descrip = Descrip;
                item.ArchivoAdjunto = ArchivoAdjunto;
                item.Link = Link;
                item.Email = Email;
                item.Telefono = Telefono;
                item.Habilitado = Habilitado;
                item.DiasVigencia = FechaVencimiento.Subtract(item.FHAlta).Days;
            }
           
            Repositorio.AdapterDSClasificados.clasiAviso.Update(ClasificadosDt);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSClasificados.clasiAvisoDataTable GetClasificadoByID(int AvisoID)
        {

            //DSClasificados.clasiAvisoDataTable ClasificadosDt = new DSClasificados.clasiAvisoDataTable();
            return Repositorio.AdapterDSClasificados.clasiAviso.GetDataByAvisoID(AvisoID);
            //return ClasificadosDt;            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteAvisoByID(int AvisoID)
        {

            
             Repositorio.AdapterDSClasificados.clasiAviso.Delete(AvisoID);
           
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSClasificados.clasiAvisoDataTable getClasificadosConComentario(DateTime? FechaDesde, DateTime? FechaHasta, string modo)
        {
            if (FechaDesde.HasValue)
                FechaDesde = Convert.ToDateTime(FechaDesde);
            if (FechaHasta.HasValue)
                FechaHasta = Convert.ToDateTime(FechaHasta);

            return Repositorio.AdapterDSClasificados.clasiAviso.getClasificadosConComentarios(FechaDesde, FechaHasta, modo);
        }


        
    }
}
