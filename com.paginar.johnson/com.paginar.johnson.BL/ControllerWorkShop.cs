﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using com.paginar.johnson.utiles;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerWorkShop
    {
        private RepositoryWorkShop _Repositorio;
        public RepositoryWorkShop Repositorio
        {
            get 
            {
                if (_Repositorio == null) _Repositorio = new RepositoryWorkShop();
                return _Repositorio;
            }
        }

        public DSWorkShop.ws_WorkshopDataTable WSDataTable()
        {
            DSWorkShop.ws_WorkshopDataTable WS = new DSWorkShop.ws_WorkshopDataTable();
            return WS;
        }

        public void WorkShopInsetInscripcion(int IdWorkshop, int Legajo, int Cluster, string Comentarios, DateTime FechaInscripcion, bool Notificada)
        {
            Repositorio.AdapterDSWorkShop.WSInscripcion.ws_insertInscripcion(IdWorkshop, Legajo, Cluster, Comentarios, FechaInscripcion, Notificada);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSWorkShop.ws_WorkshopDataTable WorkshopSelectAll()
        {
            return Repositorio.WorkshopSelectAll();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSWorkShop.ws_WorkshopDataTable WorkshopGetSpecific(int IdWorkshop)
        {
            return Repositorio.WorkshopGetSpecific(IdWorkshop);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void WorkshopInsert(string Nombre, string Objetivo, string Metodologia, string Requisitos, string Duracion, string FechaInicioWorkshop, int IdEstado, DateTime? PeriodoInscripcionInicio, DateTime? PeriodoInscripcionFin)
        {
            Repositorio.WorkshopInsert(Nombre, Objetivo, Metodologia, Requisitos, Duracion, FechaInicioWorkshop, IdEstado, PeriodoInscripcionInicio, PeriodoInscripcionFin);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void WorkshopUpdate(int IdWorkshop, string Nombre, string Objetivo, string Metodologia, string Requisitos, string Duracion, string FechaInicioWorkshop, int IdEstado, DateTime? PeriodoInscripcionInicio, DateTime? PeriodoInscripcionFin)
        {
            Repositorio.WorkshopUpdate(IdWorkshop, Nombre, Objetivo, Metodologia, Requisitos, Duracion, FechaInicioWorkshop, IdEstado, PeriodoInscripcionInicio, PeriodoInscripcionFin);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void WorkshopDelete(int IdWorkshop)
        {
            Repositorio.WorkshopDelete(IdWorkshop);
        }




        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSWorkShop.ws_WorkshopDataTable WorkshopDisponiblesPorUsuario(int Legajo)
        {
            return Repositorio.WorkshopDisponiblesPorUsuario(Legajo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSWorkShop.ws_WorkshopDataTable WorkshopDisponiblesActivos()
        {
            return Repositorio.WorshopDisponiblesActivos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSWorkShop.ws_Inscripcion1DataTable WorkshopGetInscriptos(int IdWorkshop)
        {
            return Repositorio.GetSuscriptos(IdWorkshop);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSWorkShop.ws_selectInscriptosByAnioDataTable WorkshopGetSuscriptosByAnio(int? anio)
        {
            return Repositorio.GetSuscriptosByAnio(anio);
        }


    }
}
