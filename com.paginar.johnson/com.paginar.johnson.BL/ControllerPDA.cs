﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using com.paginar.johnson.utiles;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerPDA
    {
        private RepositoryPda _Repositorio;
        public RepositoryPda Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryPda();
                return _Repositorio;
            }
        }

        public DSPda.pda_cursoDataTable PDAcursoDT()
        {
            DSPda.pda_cursoDataTable dt = new DSPda.pda_cursoDataTable();
            return dt;
        }
        //[DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]

        #region Cursos

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_cursoDataTable pda_cursoSelectByIdReporte(string idCurso)
        {
            
            return Repositorio.pda_cursoSelectByIdReporte(idCurso);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_cursoDataTable pda_cursoSelectInscriptoByUsuario(int idUsuario)
        {
            return Repositorio.pda_cursoSelectInscriptoByUsuario(idUsuario);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_cursoDataTable pda_cursoSelectAll()
        {
            return Repositorio.pda_cursoSelectAll();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_cursoDataTable pda_cursoSelectByID(int CursoId)
        {
            return Repositorio.pda_cursoSelectByID(CursoId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void pda_cursoInsert(string Nombre, int idEstado, string Copete, string Descripcion, string Duracion, String PeriodoTentativo)
        {
            Repositorio.pda_cursoInsert(Nombre, idEstado, Copete, Descripcion, Duracion, PeriodoTentativo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void pda_cursoUpdate(int idCurso, string Nombre, int idEstado, string Copete, string Descripcion, string Duracion, string PeriodoTentativo)
        {
            Repositorio.pda_cursoUpdate(idCurso, Nombre, idEstado, Copete, Descripcion, Duracion, PeriodoTentativo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public bool pda_cursoDelete(int idCurso)
        {
            return Repositorio.pda_cursoDelete(idCurso);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_cursoDataTable pda_cursoSelectDisponibles(int idUsuario)
        {
            return Repositorio.pda_cursoSelectDisponibles(idUsuario);
        }

        #endregion

        #region Inscripto
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_inscriptoDataTable pda_InscriptoSelectAll()
        {
            return Repositorio.pda_InscriptoSelectAll();
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_inscriptoDataTable pda_InscriptoByCurso(int idCurso)
        {
            return Repositorio.pda_InscriptoByCurso(idCurso);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_inscriptoDataTable pda_InscriptoByUsuario(int idUsuario)
        {
            return Repositorio.pda_InscriptoByUsuario(idUsuario);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void pda_InscriptoInsert(int idCurso, int idUsuario, int Legajo, int Cluster, string Comentarios, DateTime FechaInscripcion, bool Notificado)
        { 
            Repositorio.pda_InscriptoInsert(idCurso, idUsuario, Legajo, Cluster, Comentarios, FechaInscripcion, Notificado);
        }
        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void pda_InscriptoDelete(int idInscripcion)
        {
            Repositorio.pda_InscriptoDelete(idInscripcion);
        }
        #endregion

        #region Estados
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_estadoDataTable pda_estados()
        {
            return Repositorio.pda_estados();
        }
        #endregion

        #region Reportes

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_ReporteInscripcionPorCursoDataTable getReporteInscripcionPorCurso(int? idCurso, int? idEstado, int? anio)
        {
            return Repositorio.getReporteInscripcionPorCurso(idCurso, idEstado, anio);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_DetalleReporteAreaDireccionDataTable getDetalleReportePorAreaDireccionCurso(int AreaID, int DireccionID, int idCurso)
        {
            string AuxApellido = string.Empty;
            string AuxNombre = string.Empty;

            DSPda.pda_DetalleReporteAreaDireccionDataTable DT = new DSPda.pda_DetalleReporteAreaDireccionDataTable();

            foreach (DSPda.pda_DetalleReporteAreaDireccionRow item in Repositorio.getDetalleReportePorAreaDireccionCurso(AreaID, DireccionID, idCurso).Rows)
            {

                DSPda.pda_DetalleReporteAreaDireccionRow r = DT.Newpda_DetalleReporteAreaDireccionRow();
                if (AuxApellido == item.Apellido && AuxNombre == item.Nombre)
                {
                    r.Usuarioid = item.Usuarioid;
                    
                    r.Apellido = "";
                    r.Nombre = "";
                    
                }
                else
                {
                    r.Usuarioid = item.Usuarioid;
                    r.Legajo = item.Legajo;
                    r.Apellido = item.Apellido;
                    r.Nombre = item.Nombre;
                    r.FechaInscripcion = item.FechaInscripcion;
                    AuxNombre = item.Nombre;
                    AuxApellido = item.Apellido;

                }
                r.curso = item.curso;
                if (AuxNombre == string.Empty)
                {
                    AuxNombre = item.Nombre;
                    AuxApellido = item.Apellido;
                }
                DT.Addpda_DetalleReporteAreaDireccionRow(r);    
            }
            return DT;//Repositorio.getDetalleReportePorAreaDireccionCurso(AreaID, DireccionID, idCurso);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_ReporteAreaDireccionDataTable getReporteAreaDireccion(int AreaID, int DireccionID, int idCurso, int anio)
        {
            return Repositorio.getReporteAreaDireccion(AreaID, DireccionID, idCurso, anio);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.pda_ReportePorUsuarioDataTable getReporteUsuarios(int? Legajo, string Apellido, int anio)
        {
            return Repositorio.getReporteUsuarios(Legajo, Apellido, anio);
        }

        #endregion

        #region Filtros

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.DireccionDataTable getDirecciones()
        {
            return Repositorio.getDirecciones();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSPda.AreasDataTable getAreasByDireccionId(int? Dirid)
        {
            return Repositorio.getAreasByDireccionId(Dirid);
        }

        #endregion
    }
}
