﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using com.paginar.johnson.utiles;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerReportesDeSeguimiento
    {
        private DSReportesDeSeguimiento DSReporteSeguimiento= new DSReportesDeSeguimiento();
        private RepositoryReportesDeSeguimiento _Repositorio = new RepositoryReportesDeSeguimiento();

        public RepositoryReportesDeSeguimiento Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryReportesDeSeguimiento();
                return _Repositorio;
            }

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.AreasDataTable GetAreasByDireccionID(int DireccionID)
        {
            return Repositorio.GetAreasByDireccionID(DireccionID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.DireccionDataTable GetDirecciones()
        {
            return Repositorio.GetDirecciones();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.Eval_PeriodoFiscalDataTable GetPeriodosFiscales()
        {
            return Repositorio.GetPeriodosFiscales();
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.eval_PeriodoDataTable GetPeriodoByPeridoFiscalTipoPeriodo(int PeriodoFiscal, int TipoPeriodo)
        {
            return Repositorio.GetPeriodoByPeridoFiscalTipoPeriodo(PeriodoFiscal, TipoPeriodo);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.EvaluadoresDataTable GetEvaluadoresByPeriodoID(int PeriodoID)
        {
            return Repositorio.GetEvaluadoresByPeriodoID(PeriodoID);
        
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.EvaluadoresAdministrativosDataTable GetEvaluadoresAdministrativosByPeriodoID(int PeriodoID)
        {
            return Repositorio.GetEvaluadoresAdministrativosByPeriodoID(PeriodoID);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable Eval_GetReporteSeguimientoAgrupadoAreaDireccion(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster, int MostrarAreas)
        {
            return Repositorio.GetReporteSeguimientoAgrupadoAreaDireccion(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Cluster, MostrarAreas);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoEvaluadosDataTable Eval_GetReporteSeguimientoEvaluados(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Calificacion, string Cluster)
        {
            return Repositorio.Eval_GetReporteSeguimientoEvaluados(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Calificacion, Cluster);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster)
        {
            return Repositorio.GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID,  Cluster);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select,false)]
        public DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable eval_GetReporteSguimientoAgrupadoEvaluadorRating(int PeriodoID,
             int LegajoEvaluador, int Pasoid, int TipoFormularioID, int DireccionID, int AreaID, string Cluster)
        {
            return Repositorio.eval_GetReporteSguimientoAgrupadoEvaluadorRating(PeriodoID, LegajoEvaluador, Pasoid, TipoFormularioID, DireccionID, AreaID, Cluster);
        }

    }
}
