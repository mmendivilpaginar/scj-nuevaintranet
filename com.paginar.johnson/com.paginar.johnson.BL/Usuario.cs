﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.BL
{
    public class Usuario
    {
        private int _usuarioid;
        public int usuarioid
        {
            get { return _usuarioid; }
            set { _usuarioid = value; }
        }

        private string _email;
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _nombre;
        public string nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        private string _apellido;
        public string apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }

        private int _legajo;
        public int legajo
        {
            get { return _legajo; }
            set { _legajo = value; }
        }

        private int _clusterID;
        public int clusterID
        {
            get { return _clusterID; }
            set { _clusterID = value; }
        }

        private int _clusteriddefecto;
        public int clusteriddefecto
        {
            get { return _clusteriddefecto; }
            set { _clusteriddefecto = value; }
        }

        private int _clusteridactual;
        public int clusteridactual
        {
            get{return _clusteridactual;}
            set{_clusteridactual=value;}
        }

        private string _Identif;
        public string Identif {
            get { return _Identif; }
            set { _Identif = value; }
        }

        private string _Clave;
        public string Clave
        {
            get { return _Clave; }
            set { _Clave = value; }
        }

        
        //------Nombre, Apellido
        public string GetFullNameWithSeparator(string separator=", ")
        {
            
            string value=String.Format("{0}{1}{2}", this.nombre.Trim(),separator, this.apellido.Trim());
            return value;
            
        }
    }
}
