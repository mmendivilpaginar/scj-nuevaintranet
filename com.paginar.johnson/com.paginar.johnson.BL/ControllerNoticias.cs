﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using com.paginar.johnson.utiles;
namespace com.paginar.johnson.BL
{
     [DataObject]
    public class ControllerNoticias
    {
        private DSNoticias NoticiasDS = new DSNoticias();

        private RepositoryNoticias _Repositorio = new RepositoryNoticias();
        public RepositoryNoticias Repositorio
        {
            get {
                if (_Repositorio == null) _Repositorio = new RepositoryNoticias();
                return _Repositorio; }
           
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infInfoDataTable GetBusquedaABM(int? Estado, int? Destacado, int? CategoriaID, string TipoBusq, DateTime? Desde, DateTime? Hasta)
        {
            return _Repositorio.GetBusquedaABM(Estado, Destacado, CategoriaID, TipoBusq, Desde, Hasta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infInfoDataTable GetBusquedaFE(string Texto, int? CategoriaID, DateTime? Desde, DateTime? Hasta,int? limit,int? clusterid,int? TagId)
        {
            return _Repositorio.AdapterDSNoticias.infInfo.GetDataByBusquedaFE(Texto, CategoriaID, Desde, Hasta,null,clusterid,"",TagId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infInfoDataTable GetBusquedaFE(string Texto, int? CategoriaID, DateTime? Desde, DateTime? Hasta, int limit)
        {
            return _Repositorio.AdapterDSNoticias.infInfo.GetDataByBusquedaFE(Texto, CategoriaID, Desde, Hasta, limit,null,"",null);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infInfoDataTable GetInfInfoByInfoID(int InfoID)
        {
            //_Repositorio.GetInfoByID(NoticiasDS, InfoID);
            Repositorio.AdapterDSNoticias.infInfo.FillByID(NoticiasDS.infInfo, InfoID);
            Repositorio.AdapterDSNoticias.infLink.FillByInfoID(NoticiasDS.infLink, InfoID);
            Repositorio.AdapterDSNoticias.infImagen.FillByInfoID(NoticiasDS.infImagen, InfoID);
            Repositorio.AdapterDSNoticias.RelInfoCluster.FillByInfoID(NoticiasDS.RelInfoCluster, InfoID);
            Repositorio.AdapterDSNoticias.infRecordatorio.FillByInfoID(NoticiasDS.infRecordatorios, InfoID);
            return NoticiasDS.infInfo;
        }

        #region Categorias
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infCategoriaDataTable getInfCategorias()
        {
            return Repositorio.getInfCategorias();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void infCat_Insert(string Descrip, int UsrAlta)
        {
            Repositorio.infCat_Insert(Descrip, UsrAlta);
        }

        public int infCat_Delete(int CategoriaID)
        {
            return Repositorio.infCat_Delete(CategoriaID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infCategoriaDataTable getInfCat_Search(string Search)
        {
            return Repositorio.getInfCat_Search(Search);
        }

        public bool boolSearchExistCategoty(string textCompare)
        {
            bool resp = false;

            foreach (DSNoticias.infCategoriaRow item in this.getInfCat_Search(textCompare))
            {
                if (item.Descrip.Trim() == textCompare.Trim())
                    resp = true;
            }

            return resp;
            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infCategoriaDataTable getinfCategoria_ById(int CategoriaID)
        {
            return Repositorio.getinfCategoria_ById(CategoriaID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, false)]
        public void infCat_Update(int CategoriaID, string Descrip)
        {
            Repositorio.infCat_Update(CategoriaID, Descrip);
        }



        #endregion

        #region Links Metodos
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infLinkDataTable GetLinkByInfoID()
        {
            
            return NoticiasDS.infLink;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infLinkRow GetLinkByLinkID(int LinkID)
        {            
            return NoticiasDS.infLink.FindByLinkID(LinkID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infLinkDataTable GetLinkByInfoID(int InfoID)
        {
            return Repositorio.AdapterDSNoticias.infLink.GetDataByInfoID(InfoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteLink(int LinkID)
        {
            NoticiasDS.infLink.FindByLinkID(LinkID).Delete();
        }       

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateLink(int LinkID, string Descrip, string URL)
        {
            DSNoticias.infLinkRow LinkR = NoticiasDS.infLink.FindByLinkID(LinkID);
            LinkR.Descrip = Descrip;
            LinkR.URL = URL;
            LinkR.Orden = 1;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertLink(int InfoID, string Descrip, string URL)
        {
            DSNoticias.infLinkRow LinkR = NoticiasDS.infLink.NewinfLinkRow();
            LinkR.InfoID = InfoID;
            LinkR.Descrip = Descrip;
            LinkR.URL = URL;
            LinkR.Orden = 0;
            NoticiasDS.infLink.AddinfLinkRow(LinkR);
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infRecordatoriosDataTable getRecordatoriosInfByID(int infid)
        {
        //retornamos recordatorios segun id
            return Repositorio.AdapterDSNoticias.infRecordatorio.GetDataByInfoID(infid);
        }
        
        #endregion 

        #region Imagens Metodos
         

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infImagenDataTable GetImagenByInfoID()
        {
            return NoticiasDS.infImagen;
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infImagenDataTable Get_ImagenesByInfoID()
        {
            DataView DVInfImagen = new DataView(NoticiasDS.infImagen, "Path Like '%jpg' or Path like '%png' or Path like '%.mp4%' ","", DataViewRowState.CurrentRows);
            DSNoticias.infImagenDataTable DtAux = new DSNoticias.infImagenDataTable();
            DtAux.Merge(DVInfImagen.ToTable());
            return DtAux;            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infImagenDataTable Get_ArchivosByInfoID()
        {
            DataView DVInfImagen = new DataView(NoticiasDS.infImagen, "Path Not Like '%jpg' and Path Not Like '%png' and Path Not like '%.mp4%' ", "", DataViewRowState.CurrentRows);
            DSNoticias.infImagenDataTable DtAux = new DSNoticias.infImagenDataTable();
            DtAux.Merge(DVInfImagen.ToTable());
            return DtAux;
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infImagenRow GetImagenByImagenID(int ImagenID)
        {
            return NoticiasDS.infImagen.FindByImagenID(ImagenID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteImagen(int ImagenID)
        {
            NoticiasDS.infImagen.FindByImagenID(ImagenID).Delete();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateImagen(int ImagenID, string Descrip, string Path)
        {
            DSNoticias.infImagenRow ImagenR = NoticiasDS.infImagen.FindByImagenID(ImagenID);
            ImagenR.Descrip = Descrip;
            ImagenR.Path = Path;
            ImagenR.Orden = 1;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertImagen(int InfoID, string Descrip, string Path,bool flyer)
        {
            DSNoticias.infImagenRow ImagenR = NoticiasDS.infImagen.NewinfImagenRow();
            ImagenR.Descrip = Descrip;
            ImagenR.InfoID = InfoID;
            ImagenR.Path = Path;
            ImagenR.Orden = 0;
            ImagenR.flyer = flyer;
            NoticiasDS.infImagen.AddinfImagenRow(ImagenR);
        }

        #endregion


        #region Cluster Metodos
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.RelInfoClusterDataTable GetClusterByInfoID()
        {

            return NoticiasDS.RelInfoCluster;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertCluster(int InfoID, int ClusterID)
        {
            DSNoticias.RelInfoClusterRow RICRow = NoticiasDS.RelInfoCluster.FindByInfoIDClusterID(InfoID, ClusterID);
            if (RICRow == null)
            {
                DSNoticias.RelInfoClusterRow ClusterR = NoticiasDS.RelInfoCluster.NewRelInfoClusterRow();
                ClusterR.ClusterID = ClusterID;
                ClusterR.InfoID = InfoID;
                NoticiasDS.RelInfoCluster.AddRelInfoClusterRow(ClusterR);
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteCluster( int InfoID,int ClusterID)
        {
            DSNoticias.RelInfoClusterRow RICRow = NoticiasDS.RelInfoCluster.FindByInfoIDClusterID(InfoID,ClusterID );
            if (RICRow != null)
                RICRow.Delete();
        }
        #endregion


        #region Recordatorios Metodos
        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSNoticias.infRecordatoriosDataTable GetRecordatoriosByInfoID()
        {
            return NoticiasDS.infRecordatorios;
        }

        public bool ValidarFechaPublicacion(DateTime FHPublicacion)
        {
            foreach (DSNoticias.infRecordatoriosRow recordatorios in NoticiasDS.infRecordatorios.Rows)
            {
                if( recordatorios.RowState!= DataRowState.Deleted)
                {
                  if( recordatorios.FHRecordatorio<= FHPublicacion )
                      return false;
                 }
            }
            return true;
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, false)]
        public void DeleteRecordatorio(int InfRecordatorioID)
        {
            DSNoticias.infRecordatoriosRow InfRecRow = NoticiasDS.infRecordatorios.FindByInfRecordatorioID(InfRecordatorioID);
            if (InfRecRow != null)
                InfRecRow.Delete();
        }
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, false)]
        public void InsertarRecordatorio(int InfoID, DateTime FHRecordatorio)
        {
            DSNoticias.infRecordatoriosRow InfRecRow = NoticiasDS.infRecordatorios.NewinfRecordatoriosRow();
            InfRecRow.InfoID = InfoID;
            InfRecRow.FHRecordatorio = FHRecordatorio;
            NoticiasDS.infRecordatorios.AddinfRecordatoriosRow(InfRecRow);
        }
        #endregion

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateInfo(int InfoID, string Titulo, string Copete, string Fuente, int Estado, int Destacada, string Texto, int CategoriaID, int TipoInfoID, int Privacidad, DateTime FHMod, int UsrMod, DateTime FechaAsoc)
        {
            foreach (DSNoticias.infInfoRow info in NoticiasDS.infInfo)
            {
                info.Titulo = Titulo;
                info.Copete = Copete;
                info.Fuente = Fuente;
                info.Estado = Estado;
                info.Destacada = Destacada;
                info.Texto = Texto;
                info.CategoriaID = CategoriaID;
                info.TipoInfoID = TipoInfoID;
                
                info.Privacidad = Privacidad;
                info.FHMod = FHMod;
                info.UsrMod = UsrMod;
                info.FechaAsoc = FechaAsoc;
            }
            Repositorio.AdapterDSNoticias.infInfo.Update(NoticiasDS.infInfo);
            ActualizarDetalle();
            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertInfo(string Titulo, string Copete, string Fuente, int Estado, int Destacada, string Texto, int CategoriaID, int TipoInfoID, int Privacidad, DateTime FHAlta, int UsrAlta,DateTime FechaAsoc)
        {

            DSNoticias.infInfoRow info = NoticiasDS.infInfo.NewinfInfoRow();
            info.Titulo = Titulo;
            info.Copete = Copete;
            info.Fuente = Fuente;
            info.Estado = Estado;
            info.Destacada = Destacada;
            info.Texto = Texto;
            info.CategoriaID = CategoriaID;
            info.TipoInfoID = TipoInfoID;
         
            info.Privacidad = Privacidad;
            info.FHAlta = FHAlta;
            info.UsrAlta = UsrAlta;
            info.FechaAsoc = FechaAsoc;
            NoticiasDS.infInfo.AddinfInfoRow(info);
            Repositorio.AdapterDSNoticias.infInfo.Update(NoticiasDS.infInfo);

            foreach (DSNoticias.infImagenRow imagen in NoticiasDS.infImagen.Rows)
            {
                imagen.InfoID = info.InfoID;
            }
            foreach (DSNoticias.infLinkRow link in NoticiasDS.infLink.Rows)
            {
                //link.SetParentRow(info);
                link.InfoID = info.InfoID;
            }
            foreach (DSNoticias.RelInfoClusterRow item in NoticiasDS.RelInfoCluster.Rows)
            {
                item.InfoID = info.InfoID;
            }
            foreach (DSNoticias.infRecordatoriosRow item in NoticiasDS.infRecordatorios.Rows)
            {
                item.InfoID = info.InfoID;
            }

            foreach (DSNoticias.infRelInfoTagsRow item in NoticiasDS.infRelInfoTags.Rows)
            {
                item.InfoId = info.InfoID;
            }

            Repositorio.AdapterDSNoticias.infInfo.Update(NoticiasDS.infInfo);
            ActualizarDetalle();
        }

        private void ActualizarDetalle()
        {
            Repositorio.AdapterDSNoticias.infImagen.Update(NoticiasDS.infImagen);
            Repositorio.AdapterDSNoticias.infLink.Update(NoticiasDS.infLink);
            Repositorio.AdapterDSNoticias.RelInfoCluster.Update(NoticiasDS.RelInfoCluster);
            Repositorio.AdapterDSNoticias.infRecordatorio.Update(NoticiasDS.infRecordatorios);
            Repositorio.AdapterDSNoticias.infRelInfoTags.Update(NoticiasDS.infRelInfoTags);
        }

        public bool noticiaGuardada()
        {
            if (int.Parse(NoticiasDS.infInfo.Count.ToString()) > 0)
            {
                NoticiasDS.Clear();
                return true;
            }
            return false;
        }

        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infInfoDataTable GetInfoCluster(int? Clusterid, int? CategoriaID)
        {
            return Repositorio.AdapterDSNoticias.infInfo.GetInfoCluster(Clusterid, CategoriaID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteInfo(int InfoID)
        {
            ControllerNewsletter cn = new ControllerNewsletter();

            if (!cn.TieneNewsletter(InfoID))
            {
                foreach (DSNoticias.infLinkRow link in NoticiasDS.infLink.Rows)
                {
                    link.Delete();
                }
                foreach (DSNoticias.infImagenRow imagen in NoticiasDS.infImagen.Rows)
                {
                    imagen.Delete();
                }
                foreach (DSNoticias.infInfoRow info in NoticiasDS.infInfo.Rows)
                {
                    info.Delete();
                }

                foreach (DSNoticias.RelInfoClusterRow RIC in NoticiasDS.RelInfoCluster.Rows)
                {
                    RIC.Delete();
                }

                foreach (DSNoticias.infRecordatoriosRow IR in NoticiasDS.infRecordatorios.Rows)
                {
                    IR.Delete();
                }


                foreach (DSNoticias.infRelInfoTagsRow Tag in NoticiasDS.infRelInfoTags.Rows)
                {
                    Tag.Delete();
                }

                Repositorio.AdapterDSNoticias.infImagen.Update(NoticiasDS.infImagen);
                Repositorio.AdapterDSNoticias.infLink.Update(NoticiasDS.infLink);
                Repositorio.AdapterDSNoticias.infRecordatorio.Update(NoticiasDS.infRecordatorios);
                Repositorio.AdapterDSNoticias.RelInfoCluster.Update(NoticiasDS.RelInfoCluster);
                Repositorio.AdapterDSNoticias.infInfo.Update(NoticiasDS.infInfo);
                Repositorio.AdapterDSNoticias.infRelInfoTags.Update(NoticiasDS.infRelInfoTags);
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infInfoDataTable getInfoByReporte(DateTime? desde, DateTime? hasta, string modo)
        {
            return Repositorio.AdapterDSNoticias.infInfo.GetInfoByReporte(desde, hasta, modo);
        }


        #region InfoUsuarioVotacion
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public bool UsuarioVotacion(int usuarioid, int infoid)
        {
            return Repositorio.UsuarioVotoNoticia(usuarioid, infoid);
        }

        public void InsertInfoUsuarioVotacion(int usuarioid,int infoid,int voto) 
        {
            DSNoticias.InfoUsuarioVotacionRow info = NoticiasDS.InfoUsuarioVotacion.NewInfoUsuarioVotacionRow();            
            info.UsuarioID = usuarioid;
            info.InfoID = infoid;
            info.FHAlta = DateTime.Now;
            info.voto = voto;
            NoticiasDS.InfoUsuarioVotacion.AddInfoUsuarioVotacionRow(info);
            Repositorio.AdapterDSNoticias.infoUsuarioVotacion.Update(NoticiasDS.InfoUsuarioVotacion);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.InfoUsuarioVotacionDataTable getUserByVoto(int InfoID)
        {
            return Repositorio.AdapterDSNoticias.infoUsuarioVotacion.GetUserByVoto(InfoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public int? getVotos(int infoid, int voto)
        {
            return Repositorio.getVotos(infoid, voto);
        }

        public void InsertUSuarioComentario(int usuarioid, int infoid, string comentario)
        {
            DSNoticias.InfoUsuarioComentariosRow info = NoticiasDS.InfoUsuarioComentarios.NewInfoUsuarioComentariosRow();
            info.usuarioID = usuarioid;
            info.InfoID = infoid;
            info.comentarios = comentario;
            info.Fecha = DateTime.Now;
            NoticiasDS.InfoUsuarioComentarios.AddInfoUsuarioComentariosRow(info);
            Repositorio.AdapterDSNoticias.infoUsuarioComentarios.Update(NoticiasDS.InfoUsuarioComentarios);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public int? getCantComments(int infoid)
        {
            return Repositorio.getCantCommets(infoid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.InfoUsuarioComentariosDataTable getComments(int infoid)
        {
            Repositorio.AdapterDSNoticias.infoUsuarioComentarios.FillComents(NoticiasDS.InfoUsuarioComentarios, infoid);
            return NoticiasDS.InfoUsuarioComentarios;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.InfoUsuarioComentariosRow getCommentsBycomentarioID(int comentarioID)
        {
            return NoticiasDS.InfoUsuarioComentarios.FindBycomentarioID(comentarioID); 
            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteComentario(int comentarioID)
        {
             NoticiasDS.InfoUsuarioComentarios.FindBycomentarioID(comentarioID).Delete();
             Repositorio.AdapterDSNoticias.infoUsuarioComentarios.Update(NoticiasDS.InfoUsuarioComentarios);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateComment(int comentarioID, string comentarios)
        {
            DSNoticias.InfoUsuarioComentariosRow IUCRow = NoticiasDS.InfoUsuarioComentarios.FindBycomentarioID(comentarioID);
            IUCRow.comentarios = comentarios;
            Repositorio.AdapterDSNoticias.infoUsuarioComentarios.Update(IUCRow);
        }

        #endregion


        #region Banners
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infoBannerDataTable getBanners(int clusterid)
        {
            return Repositorio.getBanners(clusterid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infoBannerDataTable getLastBanner(int clusterid)
        {
            return Repositorio.getLastBanner(clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infoBannerDataTable getAllBanners()
        {
            return Repositorio.getAllBanners();
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infoBannerDataTable getBannerByID(int bannerid)
        {
            return Repositorio.getBannerByID(bannerid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public int BannerAdd(string titulo,string cuerpo,string imagen,DateTime fhalta, bool habilitado)
        {
            DSNoticias.infoBannerRow banner = NoticiasDS.infoBanner.NewinfoBannerRow();
            banner.Titulo = titulo;
            banner.Cuerpo = cuerpo;
            banner.Imagen = imagen;
            banner.FHAlta = fhalta;
            banner.Habilitado = habilitado;
            NoticiasDS.infoBanner.AddinfoBannerRow(banner);
            Repositorio.AdapterDSNoticias.infoBanner.Update(NoticiasDS.infoBanner);
            return banner.BannerID;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infoBannerClusterDataTable getClusterByBannerID(int bannerid)
        {
            return Repositorio.AdapterDSNoticias.InfoBannerCluster.GetDataByBannerID(bannerid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void BannerClusterAdd(int bannerid, int clusterid)
        {
            Repositorio.AdapterDSNoticias.InfoBannerCluster.Fill(NoticiasDS.infoBannerCluster);

            DSNoticias.infoBannerClusterRow row = NoticiasDS.infoBannerCluster.FindByBannerIDClusterID(bannerid, clusterid);
            if (row == null)
            {
                DSNoticias.infoBannerClusterRow bannercluster = NoticiasDS.infoBannerCluster.NewinfoBannerClusterRow();
                bannercluster.BannerID = bannerid;
                bannercluster.ClusterID = clusterid;
                NoticiasDS.infoBannerCluster.AddinfoBannerClusterRow(bannercluster);
                Repositorio.AdapterDSNoticias.InfoBannerCluster.Update(NoticiasDS.infoBannerCluster);
            }
         
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void BannerClusterDelete(int bannerid, int clusterid)
        {
            Repositorio.AdapterDSNoticias.InfoBannerCluster.Fill(NoticiasDS.infoBannerCluster);
            DSNoticias.infoBannerClusterRow row = NoticiasDS.infoBannerCluster.FindByBannerIDClusterID(bannerid, clusterid);
            if (row != null)
            {
                NoticiasDS.infoBannerCluster.FindByBannerIDClusterID(bannerid, clusterid).Delete();
                Repositorio.AdapterDSNoticias.InfoBannerCluster.Update(NoticiasDS.infoBannerCluster);
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void BannerUpdate(int bannerid,string titulo, string cuerpo, string imagen, DateTime fhalta, bool habilitado,int original_bannerid)
        {
            Repositorio.AdapterDSNoticias.infoBanner.Fill(NoticiasDS.infoBanner);
            DSNoticias.infoBannerRow banner = NoticiasDS.infoBanner.FindByBannerID(bannerid);
            banner.Titulo = titulo;
            banner.Cuerpo = cuerpo;
            banner.Imagen = imagen;
            banner.FHAlta = fhalta;
            banner.Habilitado = habilitado;
            Repositorio.AdapterDSNoticias.infoBanner.Update(banner);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void BannerDelete(int bannerid)
        {
            Repositorio.AdapterDSNoticias.infoBanner.Fill(NoticiasDS.infoBanner);
            NoticiasDS.infoBanner.FindByBannerID(bannerid).Delete();
            Repositorio.AdapterDSNoticias.infoBanner.Update(NoticiasDS.infoBanner);
        }

        public void BannerClusterDelete(int bannerid)
        {
            Repositorio.AdapterDSNoticias.InfoBannerCluster.DeleteByBannerID(bannerid);
        }

        #endregion

        #region Tags

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infTagsDataTable getTags()
        {
            return Repositorio.AdapterDSNoticias.infTags.GetData();
        }

         
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infRelInfoTagsDataTable getTagsByInfoId(int InfoId)
        {
            return Repositorio.AdapterDSNoticias.infRelInfoTags.GetDataByInfoId(InfoId);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNoticias.infRelInfoTagsDataTable getTagsByInfoIDgrv(int InfoId)
        {
            return Repositorio.AdapterDSNoticias.infRelInfoTags.GetDataByInfoIDgrv(InfoId);
        }


        public void AddInfoTags(int infoid, int tagId)
        {

            DSNoticias.infRelInfoTagsRow row = NoticiasDS.infRelInfoTags.NewinfRelInfoTagsRow();

            row.InfoId = infoid;
            row.TagId = tagId;

            NoticiasDS.infRelInfoTags.AddinfRelInfoTagsRow(row);
      

        }

        public void DeleteInfoTags(int InfoID)
        {
            Repositorio.AdapterDSNoticias.infRelInfoTags.DeleteTags(InfoID);
        }


        #endregion  

    }
}
