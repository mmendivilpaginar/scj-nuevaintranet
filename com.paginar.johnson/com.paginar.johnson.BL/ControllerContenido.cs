﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;


namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerContenido
    {
        private RepositoryContenido _Repositorio = new RepositoryContenido();


        public RepositoryContenido Repositorio
        {
            get {
                if (_Repositorio == null) _Repositorio = new RepositoryContenido();
                return _Repositorio; }
           
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsContenido.Content_ItemsDataTable GetEstructuraById(int? IdItem)
        {
            return Repositorio.GetEstructuraById(IdItem);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsContenido.Content_ItemsDataTable GetContentById(int? IdItem)
        {
            return Repositorio.GetContentById(IdItem);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateContent_Items(int Content_ItemId, string Titulo, string Contenido, int? parentId,int? Content_TipoID)
        {
            Repositorio.UpdateContent_Items(Content_ItemId, Titulo, Contenido, parentId, Content_TipoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public  int InsertContent_Items(string Titulo, string Contenido, int? parentId, int? Content_TipoID)
        {
            return Repositorio.InsertContent_Items(Titulo, Contenido, parentId, Content_TipoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public void DeleteContent_Items(int? Content_ItemId)
        {
            Repositorio.DeleteContent_Items(Content_ItemId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public DsContenido.Content_ItemsDataTable Content_ItemsGetMenu(int Content_ItemId, int clusterid)
        {
           return  Repositorio.Content_ItemsGetMenu(Content_ItemId, clusterid);
        }

        public Boolean EsVisible(string titulo, int clusterid)
        {
            DsContenido.Content_ItemsDataTable dt =  Repositorio.Content_ItemsGetVisible(titulo, clusterid);
            if (dt.Rows.Count == 0) return false;
            else
            {
                DsContenido.Content_ItemsRow dr = (DsContenido.Content_ItemsRow)dt.Rows[0];
                return bool.Parse(dr.visible);
            }

        }

        public int Content_TipoGetIdrootByClusterId(string nombre, int clusterid)
        {
            DsContenido.Content_TipoDataTable dt = Repositorio.Content_TipoGetIdrootByClusterId(nombre, clusterid);
            if (nombre == "Calendario Fiscal")
            {
                if (dt.Count == 0 && clusterid != 1)
                {
                    DsContenido.Content_TipoDataTable dtt = Repositorio.Content_TipoGetIdrootByClusterId(nombre, 1);
                    DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dtt.Rows[0];
                    return dr.Content_itemIdroot;

                }
                else
                {
                    DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dt.Rows[0];
                    return dr.Content_itemIdroot;

                }
            }
            else
            {
                DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dt.Rows[0];
                return dr.Content_itemIdroot;
            }
        }


        public int Content_TipoGetContent_tipoIDByClusterId(string nombre, int clusterid)
        {
            DsContenido.Content_TipoDataTable dt = Repositorio.Content_TipoGetIdrootByClusterId(nombre, clusterid);
            if (nombre == "Calendario Fiscal")
            {
                if (dt.Count == 0 && clusterid != 1)
                {
                    DsContenido.Content_TipoDataTable dtt = Repositorio.Content_TipoGetIdrootByClusterId(nombre, 1);
                    DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dtt.Rows[0];
                    return dr.Content_TipoID;

                }
                else
                {
                    DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dt.Rows[0];
                    return dr.Content_TipoID;

                }

            }
            else
            {
                DsContenido.Content_TipoRow dr = (DsContenido.Content_TipoRow)dt.Rows[0];
                return dr.Content_TipoID;
            }

        }


        public void Content_ItemsByClusterDelete(int? content_itemid, int? clusterid)
        {
            Repositorio.AdaptersDSContenido.ContenidoInCluster.Content_ItemsByClusterDelete(content_itemid, clusterid);
        }

        public void Content_ItemsByClusterAdd(int? content_itemid, int? clusterid)
        {
            Repositorio.AdaptersDSContenido.ContenidoInCluster.Content_ItemsByClusterAdd(content_itemid, clusterid);
        }

        public void Content_ItemsMenuUrlToolTipAdd(int Content_ItemId, string navigateurl, string @tooltip)
        {

            Repositorio.Content_ItemsMenuUrlToolTipAdd(Content_ItemId, navigateurl, tooltip);
        }

        public void Content_ItemsMenuUrlToolTipDelete(int Content_ItemId)
        {
            Repositorio.Content_ItemsUrlToolTipDelete(Content_ItemId);
        }

        public void Content_ItemsMenuUrlToolTipUpdate(int Content_ItemId, string navigateurl, string @tooltip)
        {
            Repositorio.Content_ItemsMenuUrlToolTipUpdate(Content_ItemId, navigateurl, tooltip);
        }

        public DsContenido.Content_ItemsMenuUrlToolTipDataTable Content_ItemsMenuUrlToolTipGet(int Content_ItemId)
        {
            return Repositorio.Content_ItemsMenuUrlToolTipGet(Content_ItemId);
        }

        public DsContenido.ClusterDataTable GetDataByExcludeTodos()
        {
            return Repositorio.AdaptersDSContenido.Cluster.GetDataByExcludeTodos();

        }
       
        
        public DsContenido.Content_ItemsByClusterDataTable Content_ItemsGetClustersByID(int ItemId)
        { 
            //return Repositorio.AdaptersDSContenido
            return Repositorio.AdaptersDSContenido.ContenidoInCluster.GetDataByContent_ItemsGetClustersByID(ItemId);
        }

        public void Content_ItemsByItemsDelete(int ItemId)
        {
            Repositorio.AdaptersDSContenido.ContenidoInCluster.Content_ItemsByItemsDelete(ItemId);
        }

         
    }
}
