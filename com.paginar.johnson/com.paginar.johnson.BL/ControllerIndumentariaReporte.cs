﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerIndumentariaReporte
    {
        private RepositoryIndumentariaReporte _Repositorio;
        public RepositoryIndumentariaReporte Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryIndumentariaReporte();
                return _Repositorio;

            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSIndumentariaReporte.IndumentariaReporteDetalleDataTable getIndumentariaReporteDetalleSelect(int? PeriodoID, string Area, string esBrigadista)
        {
            bool? brigadista;
            if (esBrigadista == null)
                brigadista = null;
            else
            {
                if (esBrigadista != "Todos")
                    brigadista = bool.Parse(esBrigadista);
                else
                    brigadista = null;
            }
            return Repositorio.getIndumentariaReporteDetalleSelect(PeriodoID, Area, brigadista);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSIndumentariaReporte.IndumentariaReporteAreaSinDetalleDataTable getIndumentariaReporteAreaSinDetallesSelect(int? PeriodoID, string Area, string esBrigadista)
        {
            bool? brigadista;
            if (esBrigadista == null)
            {
                brigadista = null;
            }
            else
            {
                if (esBrigadista != "Todos")
                    brigadista = bool.Parse(esBrigadista);
                else
                    brigadista = null;
            }
            return Repositorio.getIndumentariaReporteAreaSinDetallesSelect(PeriodoID, Area, brigadista);
            
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSIndumentariaReporte.IndumentariaReporteSinDetalleDataTable getIndumentariaReporteSinDetalle(int? PeriodoID, string esBrigadista)
        {
            bool? brigadista;
            if (esBrigadista == null)
            {
                brigadista = null;
            }
            else
            {

                if (esBrigadista != "Todos")
                    brigadista = bool.Parse(esBrigadista);
                else
                    brigadista = null;
            }
            return Repositorio.getIndumentariaReporteSinDetalle(PeriodoID, brigadista);
            
        }

    }
}
