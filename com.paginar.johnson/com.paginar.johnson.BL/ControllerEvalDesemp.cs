﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Reflection;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerEvalDesemp
    {
        
        private DSEvalDesemp EvalDesemp = new DSEvalDesemp();
        public List<Objetivo> ObjetivoList = new List<Objetivo>();
        private RepositoryEvalDesemp _Repositorio = new RepositoryEvalDesemp();
        public RepositoryEvalDesemp Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryEvalDesemp();
                return _Repositorio;
            }
        }


        public void AddObjetivo(int Indice, string objetivos, string medidas, string resultados)
        {

            ObjetivoList.Add(new Objetivo(Indice, objetivos, medidas, resultados));         
        }

        public void AddObjetivo(string objetivos, string medidas, string resultados)
        {
            int Indice=1;
            foreach (Objetivo item in ObjetivoList)
                 {
                     Indice = item.Indice + 1;
                 }
            ObjetivoList.Add(new Objetivo(Indice, objetivos, medidas, resultados));
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public List<Objetivo> GetObjetivos()
        {
            return ObjetivoList;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public List<Objetivo> GetObjetivoByIndice(int Indice)
        {
            List<Objetivo> ObjetivoList1 = new List<Objetivo>();
            foreach (Objetivo item in ObjetivoList)
            {
                if (item.Indice == Indice)
                {
                    ObjetivoList1.Add(new Objetivo(Indice, item.Objetivos , item.Medidas , item.Resultados));

                }
            }
            return ObjetivoList1;
           
        }

        public void GetObjetivosActuales(int UsuarioID)
        {

            ObjetivoList.Clear();
            
            Repositorio.AdapterEvalDesemp.frmObjetivosProxTableAdapter.FillFrmObjetivosActualByUsuarioID(EvalDesemp.frmObjetivosProx, UsuarioID);
            foreach (DSEvalDesemp.frmObjetivosProxRow item in EvalDesemp.frmObjetivosProx.Rows)
            {

                //foreach (var property in item.GetType().GetProperties())
                //{
                //    if (property.CanRead)
                //    {

                //    }
                //}

                if ((!item.Isobjetivo1Null())&&(!string.IsNullOrEmpty(item.objetivo1)))
                {
                    AddObjetivo(1,item.objetivo1, item.Ismedidas1Null() ? string.Empty : item.medidas1, item.Isresultados1Null() ? string.Empty : item.resultados1);
                }
                if ((!item.Isobjetivo2Null()) && (!string.IsNullOrEmpty(item.objetivo2)))
                {
                    AddObjetivo(2,item.objetivo2, item.Ismedidas2Null() ? string.Empty : item.medidas2, item.Isresultados2Null() ? string.Empty : item.resultados2);
                }
                if ((!item.Isobjetivo3Null()) && (!string.IsNullOrEmpty(item.objetivo3)))
                {
                    AddObjetivo(3,item.objetivo3, item.Ismedidas3Null() ? string.Empty : item.medidas3, item.Isresultados3Null() ? string.Empty : item.resultados3);
                }
                if ((!item.Isobjetivo4Null()) && (!string.IsNullOrEmpty(item.objetivo4)))
                {
                    AddObjetivo(4,item.objetivo4, item.Ismedidas4Null() ? string.Empty : item.medidas4, item.Isresultados4Null() ? string.Empty : item.resultados4);
                }
                if ((!item.Isobjetivo5Null()) && (!string.IsNullOrEmpty(item.objetivo5)))
                {
                    AddObjetivo(5,item.objetivo5, item.Ismedidas5Null() ? string.Empty : item.medidas5, item.Isresultados5Null() ? string.Empty : item.resultados5);
                }
                return;
                
                
            }
        }

 
        public bool PuedeAgregar()
        {           
            return (ObjetivoList.Count < 5);
        }

        public void SaveEvalDesemp(int UsuarioID)
        {
            DSEvalDesemp.frmObjetivosProxRow frmObjetivosProxR=null;
            
            if (EvalDesemp.frmObjetivosProx.Rows.Count > 0)
                frmObjetivosProxR = EvalDesemp.frmObjetivosProx.Rows[0] as DSEvalDesemp.frmObjetivosProxRow;
            if (frmObjetivosProxR == null)
                frmObjetivosProxR = EvalDesemp.frmObjetivosProx.NewfrmObjetivosProxRow();
            frmObjetivosProxR.usr_alta = UsuarioID;
            frmObjetivosProxR.fecha = DateTime.Now;
            
            
            
            if (ObjetivoList.Count>0)
            {
                frmObjetivosProxR.objetivo1 = ObjetivoList[0].Objetivos;
                frmObjetivosProxR.medidas1 = ObjetivoList[0].Medidas;
                frmObjetivosProxR.resultados1 = ObjetivoList[0].Resultados;
            }
            else
            {
                frmObjetivosProxR.objetivo1 = "";
                frmObjetivosProxR.medidas1 = "";
                frmObjetivosProxR.resultados1 = "";
            }

            if (ObjetivoList.Count > 1)
            {
                frmObjetivosProxR.objetivo2 = ObjetivoList[1].Objetivos;
                frmObjetivosProxR.medidas2 = ObjetivoList[1].Medidas;
                frmObjetivosProxR.resultados2 = ObjetivoList[1].Resultados;
            }
            else
            {
                frmObjetivosProxR.objetivo2 = "";
                frmObjetivosProxR.medidas2 = "";
                frmObjetivosProxR.resultados2 = "";
            }

            if (ObjetivoList.Count>2)
            {
                frmObjetivosProxR.objetivo3 = ObjetivoList[2].Objetivos;
                frmObjetivosProxR.medidas3 = ObjetivoList[2].Medidas;
                frmObjetivosProxR.resultados3 = ObjetivoList[2].Resultados;
            }
            else
            {
                frmObjetivosProxR.objetivo3 = "";
                frmObjetivosProxR.medidas3 = "";
                frmObjetivosProxR.resultados3 = "";
            }
            if (ObjetivoList.Count>3)
            {
                frmObjetivosProxR.objetivo4 = ObjetivoList[3].Objetivos;
                frmObjetivosProxR.medidas4 = ObjetivoList[3].Medidas;
                frmObjetivosProxR.resultados4 = ObjetivoList[3].Resultados;
            }
            else
            {
                frmObjetivosProxR.objetivo4 = "";
                frmObjetivosProxR.medidas4 = "";
                frmObjetivosProxR.resultados4 = "";
            }
            if (ObjetivoList.Count>4)
            {
                frmObjetivosProxR.objetivo5 = ObjetivoList[4].Objetivos;
                frmObjetivosProxR.medidas5 = ObjetivoList[4].Medidas;
                frmObjetivosProxR.resultados5 = ObjetivoList[4].Resultados;
            }
            else
            {
                frmObjetivosProxR.objetivo5 = "";
                frmObjetivosProxR.medidas5 = "";
                frmObjetivosProxR.resultados5 = "";
            }

            //bool estaItem;
            //for (int i = 1; i <= 5; i++)
            //{
            //    estaItem = false;

               
            //    foreach (Objetivo item in ObjetivoList)
            //    {

                    
            //        if (item.Indice == i)
            //        {
            //            PropertyInfo PI = frmObjetivosProxR.GetType().GetProperty("objetivo" + item.Indice);
            //            PI.SetValue(frmObjetivosProxR, item.Objetivos, null);
            //            PI = frmObjetivosProxR.GetType().GetProperty("medidas" + item.Indice);
            //            PI.SetValue(frmObjetivosProxR, item.Medidas, null);
            //            PI = frmObjetivosProxR.GetType().GetProperty("resultados" + item.Indice);
            //            PI.SetValue(frmObjetivosProxR, item.Resultados, null);
            //            estaItem = true;
            //        }
            //    }
            //    if (!estaItem)
            //    {
            //        PropertyInfo PI = frmObjetivosProxR.GetType().GetProperty("objetivo" + i);
            //        PI.SetValue(frmObjetivosProxR, string.Empty, null);
            //        PI = frmObjetivosProxR.GetType().GetProperty("medidas" + i);
            //        PI.SetValue(frmObjetivosProxR, string.Empty, null);
            //        PI = frmObjetivosProxR.GetType().GetProperty("resultados" + i);
            //        PI.SetValue(frmObjetivosProxR, string.Empty, null);
                
            //    }
            //}
            if (EvalDesemp.frmObjetivosProx.Rows.Count == 0)
                EvalDesemp.frmObjetivosProx.AddfrmObjetivosProxRow(frmObjetivosProxR);

            Repositorio.AdapterEvalDesemp.frmObjetivosProxTableAdapter.Update(frmObjetivosProxR);

            GetObjetivosActuales(UsuarioID);
        }

        public void DeletebyIndice(int Indice)
        {

        }
        public void DelObjetivo(Int32 Indice, int UsuarioID)
        {
            Int32 Indicem = Indice - 1;

            foreach (Objetivo item in ObjetivoList)
            {
                if (item.Indice == Indice)
                {
                    ObjetivoList.Remove(item);
                    SaveEvalDesemp(UsuarioID);
                    return;
                    //item.Medidas = string.Empty;
                    //item.Objetivos = string.Empty;
                    //item.Resultados = string.Empty;
                }
            }

            
                     
        }

        public void UpdateAdjunto(int UsuarioID, string Path)
        {

            DSEvalDesemp.frmObjetivosProxRow frmObjetivosProxR = null;

            if (EvalDesemp.frmObjetivosProx.Rows.Count > 0)
                frmObjetivosProxR = EvalDesemp.frmObjetivosProx.Rows[0] as DSEvalDesemp.frmObjetivosProxRow;
            if (frmObjetivosProxR == null)
                frmObjetivosProxR = EvalDesemp.frmObjetivosProx.NewfrmObjetivosProxRow();

            frmObjetivosProxR.usr_alta = UsuarioID;
            frmObjetivosProxR.fecha = DateTime.Now;
            frmObjetivosProxR.AdjuntoPath = Path;

            if (EvalDesemp.frmObjetivosProx.Rows.Count == 0)
                EvalDesemp.frmObjetivosProx.AddfrmObjetivosProxRow(frmObjetivosProxR);

            Repositorio.AdapterEvalDesemp.frmObjetivosProxTableAdapter.Update(frmObjetivosProxR);
            GetObjetivosActuales(UsuarioID);
        
        }

        public void UpdateItem(Int32 Indice, string objetivos, string medidas, string resultados)
        {       
            foreach (Objetivo item in ObjetivoList)
            {
                if (item.Indice == Indice)

                {
                    item.Medidas = medidas;
                    item.Objetivos = objetivos;
                    item.Resultados = resultados;                   
                    return;
                   
                }
            }

        }


    }
}
