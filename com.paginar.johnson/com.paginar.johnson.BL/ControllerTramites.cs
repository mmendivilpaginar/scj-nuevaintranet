﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerTramites
    {
        private RepositoryTramites _Repositorio;
        public RepositoryTramites Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryTramites();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertarPasoNuevoHistorico(int his_TramiteID, int his_UsrAlta, int his_UsrDestino, string his_Accion, int his_Paso, string his_Observaciones, int his_secuencia)
        {
            Repositorio.InsertarPasoNuevoHistorico(his_TramiteID, his_UsrAlta, his_UsrDestino, his_Accion, his_Paso, his_Observaciones, his_secuencia);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSTramites.frmNuevoHistoricoDataTable frmNuevoHistoricoUltimoByTramiteId(int his_tramiteID)
        {
            return Repositorio.frmNuevoHistoricoUltimoByTramiteId(his_tramiteID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSTramites.eval_EvaluadosDataTable getAreas()
        {
            return Repositorio.getAreas();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSTramites.DSTramites_frmTramite_GetTramitesVacacionesDataTable getVacaciones(int? Area, int? Direccion, DateTime? fechadesde, DateTime? fechahasta, int? estado, int? legajo, string apellido, string nombre)
        {
            return Repositorio.getVacaciones(Area, Direccion, fechadesde, fechahasta, estado, legajo, apellido, nombre);
        }
        

    }
}
