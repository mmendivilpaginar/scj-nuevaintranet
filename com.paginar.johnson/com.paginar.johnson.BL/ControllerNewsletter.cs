﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Net.Mail;
using System.Collections;
using System.IO;

namespace com.paginar.johnson.BL
{ 
   [DataObject]
    public class ControllerNewsletter
    {
       private DSNewsLetter DSNewsLetter = new DSNewsLetter();


       private RepositoryNewsLetter _RepositoryNewsLetter;
        public RepositoryNewsLetter RepositoryNewsLetter
        {
            get{
                if(_RepositoryNewsLetter==null)
                    _RepositoryNewsLetter= new RepositoryNewsLetter();
                return _RepositoryNewsLetter;
            }
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNewsLetter.news_getNoticiasDataTable getNoticias(string top, int clusterid, string filter, string ids, int? newsletterid)
        {
            return RepositoryNewsLetter.AdapterDSNewsletter.news_getNoticiasTableAdapter.GetNoticias(top, clusterid, filter, ids, newsletterid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSNewsLetter.news_getDestinatariosDataTable getDestinatarios(string texto,string modo)
        {
            return RepositoryNewsLetter.AdapterDSNewsletter.news_getDestinatariosTableAdapter.GetDestinatarios(texto,modo);
        }

       
       public int getMaxId()
       {
           return int.Parse(RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.GetMaxId().ToString()) + 1;
       }

       public int AgregarNewsletter(int estado,bool estadoenvio,string nombre,DateTime fecha,DateTime fechaenvio,int usralta,   DataTable dtdestinatarios,string noticiasids)
       {
           DSNewsLetter.news_NewsLetterRow row= DSNewsLetter.news_NewsLetter.Newnews_NewsLetterRow();
           row.EstadoID = estado;
           row.EstadoEnvio = estadoenvio;
           row.Titulo = nombre;
           row.FechaEnvio = fechaenvio;
           row.FHAlta = fecha;
           row.UsrAlta = usralta;


           DSNewsLetter.news_NewsLetter.Addnews_NewsLetterRow(row);

           RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.Update(DSNewsLetter.news_NewsLetter);

           int newsletterid = row.NewsLetterID;
           string id = newsletterid.ToString("00000");

           string subnombre = nombre.Substring(5, nombre.Length - 5);

           string nombrefinal = id + subnombre;

           RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.updatetitulo(nombrefinal, newsletterid);

           foreach (DataRow  dr in dtdestinatarios.Rows)
           {
               AgregarDestinatarios(dr["email"].ToString(), newsletterid);
           }

           string[] noticiaids = noticiasids.Split(',');

           foreach (string noticiaid in noticiaids)
           {
               AgregarNoticias(int.Parse(noticiaid), newsletterid);
           }


           return row.NewsLetterID;
       }

       public void ModificarNewsletter(int newsletterid, int estado,bool estadoenvio,string nombre,DateTime fhmod,DateTime fechaenvio,int usrmod,   DataTable dtdestinatarios,string noticiasids)
       {

           RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.FillbyID(DSNewsLetter.news_NewsLetter, newsletterid);
           DSNewsLetter.news_NewsLetterRow row = DSNewsLetter.news_NewsLetter.FindByNewsLetterID(newsletterid);

             row.EstadoID = estado;
             row.EstadoEnvio = estadoenvio;
             row.Titulo = nombre;
             row.FechaEnvio = fechaenvio;
             row.FHMod = fhmod;
             row.UsrMod = usrmod;

             RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.Update(DSNewsLetter.news_NewsLetter);


             BorrarDestinatarios(newsletterid);

             foreach (DataRow dr in dtdestinatarios.Rows)
             {
                 if(dr.RowState != DataRowState.Deleted)
                     AgregarDestinatarios(dr["email"].ToString(), newsletterid);
             }

             BorrarNoticias(newsletterid);


             string[] noticiaids = noticiasids.Split(',');

             foreach (string noticiaid in noticiaids)
             {
                 AgregarNoticias(int.Parse(noticiaid), newsletterid);
             }
       }


       public void BorrarNewsletter(int newsletterid)
       {
           BorrarDestinatarios(newsletterid);
           BorrarNoticias(newsletterid);
           RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.Delete1(newsletterid);
       }

       public void AgregarDestinatarios(string email, int newsletterid)
       {
           DSNewsLetter.news_DestinatariosRow row = DSNewsLetter.news_Destinatarios.Newnews_DestinatariosRow();
           row.email = email;
           row.NewsLetterID = newsletterid;
           DSNewsLetter.news_Destinatarios.Addnews_DestinatariosRow(row);
           RepositoryNewsLetter.AdapterDSNewsletter.DestinatariosTableAdapter.Update(DSNewsLetter.news_Destinatarios);

       }

       public void BorrarDestinatario(int destinatarioid,string email, int newsletterid)
       {
           RepositoryNewsLetter.AdapterDSNewsletter.DestinatariosTableAdapter.FillbyID(DSNewsLetter.news_Destinatarios, newsletterid);

           DSNewsLetter.news_DestinatariosRow row = DSNewsLetter.news_Destinatarios.FindByDestinatarioID(destinatarioid);

           if (row != null)
           {
               row.Delete();
           }

           RepositoryNewsLetter.AdapterDSNewsletter.DestinatariosTableAdapter.Update(DSNewsLetter.news_Destinatarios);
       }

       public void BorrarDestinatarios(int newsletterid)
       {
           RepositoryNewsLetter.AdapterDSNewsletter.DestinatariosTableAdapter.DeletebyNewsletterID(newsletterid);
       }


       public void AgregarNoticias(int InfoId, int newsletterid)
       {
           DSNewsLetter.news_RInfInfoNewsLetterRow row = DSNewsLetter.news_RInfInfoNewsLetter.Newnews_RInfInfoNewsLetterRow();
           row.InfoId = InfoId;
           row.NewsLetterID = newsletterid;
           DSNewsLetter.news_RInfInfoNewsLetter.Addnews_RInfInfoNewsLetterRow(row);
           RepositoryNewsLetter.AdapterDSNewsletter.news_RInfInfoNewsLetterTableAdapter.Update(DSNewsLetter.news_RInfInfoNewsLetter);
       }


       public void BorrarNoticias(int newsletterid)
       {
           RepositoryNewsLetter.AdapterDSNewsletter.news_RInfInfoNewsLetterTableAdapter.DeleteByNewsletterID(newsletterid);
       }

       public DSNewsLetter.news_BuscadorDataTable Buscar(int? estado, DateTime? fhdesde, DateTime? fhhasta, string nombre)
       {
           return RepositoryNewsLetter.AdapterDSNewsletter.news_BuscadorTableAdapter.Buscar(estado, fhdesde, fhhasta, nombre);
       }


       public DSNewsLetter.news_NewsLetterDataTable getNewsletterByID(int newsletterID)
       {
           return RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.GetbyID(newsletterID);
       }


       public DSNewsLetter.news_DestinatariosDataTable getDestinatarioByID(int newsletterID)
       {
           return RepositoryNewsLetter.AdapterDSNewsletter.DestinatariosTableAdapter.GetaByNewsletterID(newsletterID);
       }


       public DSNewsLetter.news_RInfInfoNewsLetterDataTable getNoticiasByID(int newsletterID)
       {
           return RepositoryNewsLetter.AdapterDSNewsletter.news_RInfInfoNewsLetterTableAdapter.GetByNewsletterID(newsletterID);
       }

       public DAL.DSNewsLetter.news_getNoticiasFilesDataTable getNoticiasFiles(int InfoID)
       {
           return RepositoryNewsLetter.AdapterDSNewsletter.newsgetNoticiasFilesTableAdapter.GetData(InfoID);
       }


       public void ActualizaNewsletter(int newsletterID)
       {
           RepositoryNewsLetter.AdapterDSNewsletter.news_NewsLetterTableAdapter.ActualizaNewsLetter(newsletterID);
       }


       public bool TieneNewsletter(int infoid)
       {

           if (RepositoryNewsLetter.AdapterDSNewsletter.news_RInfInfoNewsLetterTableAdapter.TieneNewsletter(infoid) > 0)
               return true;
           else
               return false;
       }

       private string formatFecha(int dia, int mes, int anio)
       {
           string strmes = "";
           switch (mes)
           {
               case 1:
                   strmes = "enero";
                   break;
               case 2:
                   strmes = "febrero";
                   break;
               case 3:
                   strmes = "marzo";
                   break;
               case 4:
                   strmes = "abril";
                   break;
               case 5:
                   strmes = "mayo";
                   break;

               case 6:
                   strmes = "junio";
                   break;

               case 7:
                   strmes = "julio";
                   break;

               case 8:
                   strmes = "agosto";
                   break;

               case 9:
                   strmes = "septiembre";
                   break;

               case 10:
                   strmes = "octubre";
                   break;

               case 11:
                   strmes = "noviembre";
                   break;

               case 12:
                   strmes = "diciembre";
                   break;

           }



           return dia + " de " + strmes + " de " + anio;
       }


       protected string CodValidacion(int infoid)
       {
           int codigo;
           string retorno = string.Empty;
           codigo = infoid % 15;
           switch (codigo)
           {
               case 0:
                   retorno = "2A55S";
                   break;
               case 1:
                   retorno = "2EW85";
                   break;
               case 2:
                   retorno = "DKE55";
                   break;
               case 3:
                   retorno = "SAP2Z";
                   break;
               case 4:
                   retorno = "LS5S2";
                   break;
               case 5:
                   retorno = "6SDRS";
                   break;
               case 6:
                   retorno = "ASE3Q";
                   break;
               case 7:
                   retorno = "WY7T4";
                   break;
               case 8:
                   retorno = "8ZW8S";
                   break;
               case 9:
                   retorno = "8ERSA";
                   break;
               case 10:
                   retorno = "3B843";
                   break;
               case 11:
                   retorno = "YYW5S";
                   break;
               case 12:
                   retorno = "P8SD9";
                   break;
               case 13:
                   retorno = "W2S3E";
                   break;
               case 14:
                   retorno = "Q31RT";
                   break;

           }
           return retorno;
       }


       private void AjustaTexto(string Categoria, string titulotxt, string copetetxt,string imagen,string flyer, ref int titulolargo, ref int copetelargo,ref int textolargo, int cantidad)
       {

           titulolargo = 95;
           copetelargo = 95;
           textolargo = 0;

           if (!(imagen.Contains(".jpg") || imagen.Contains(".jpeg") || imagen.Contains(".png")))
               imagen = "";

           if((Categoria=="Novedades") ||(Categoria=="Vivi SCJ"))
           {

               //si tiene copete e imagen
               if ((imagen != "") && (copetetxt != ""))
               {
                   titulolargo = 90;
                   copetelargo = 155;
                   //textolargo = 100;// sin texto cuando tiene imagen
               }

               // sin copete sin imagen
               if( (copetetxt=="") && (imagen==""))
               {
                   titulolargo = 245;
                   textolargo = 100;
                   //no modficio copetelargo
               }

               //tiene copete y no tiene imagen
               if ((copetetxt != "") && (imagen == ""))
               {
                   titulolargo = 90;
                   copetelargo = 155;
                   textolargo = 100;
               }



               if (flyer == "1" || cantidad==1)
               {
                   textolargo = 100;
               }

           }


           if ((Categoria == "Equipo SCJ") || (Categoria == "Avisos"))
           {
               // sin lmites pero pongo 100
               titulolargo = 100;
               copetelargo = 100;
           }


           if ((Categoria == "A family Company") || (Categoria == "Acordate"))
           {
               // sin copete sin imagen
               if ((copetetxt == "") && (imagen == ""))
               {
                   titulolargo = 85;
                   if (Categoria == "Acordate") textolargo = 100; // solo para acordate
               }

               //tiene copete y no tiene imagen
               if ((copetetxt != "") && (imagen == ""))
               {
                   titulolargo = 55;
                   copetelargo = 40;
                   if(Categoria == "Acordate") textolargo=100; // solo para acordate
               }

               //si tiene copete e imagen
               if ((imagen != "") && (copetetxt != ""))
               {
                   titulolargo = 55; //20
                   copetelargo = 40;//4
                   if (Categoria == "Acordate")//SCJINTMANT-614
                   {
                       titulolargo = 90;
                       copetelargo = 155;
                   }
               }

               //sin imagen y no tiene copete 
               if ((imagen != "") && (copetetxt == ""))
               {
                   titulolargo = 60; //20
                   if (Categoria == "A family Company" && (cantidad==1 || cantidad==2 )) textolargo = 70; // solo A family Company texto en 1 y 2 
               }

               if (Categoria == "Acordate" && (flyer == "1" || cantidad == 1))
               {
                   textolargo = 100;
               }
           }


       }

       public static string WordWrap(string text, int width)
       {
           int pos, next;
           StringBuilder sb = new StringBuilder();

           // Lucidity check
           if (width < 1)
               return text;

           // Parse each line of text
           for (pos = 0; pos < text.Length; pos = next)
           {
               // Find end of line
               int eol = text.IndexOf(Environment.NewLine, pos);
               if (eol == -1)
                   next = eol = text.Length;
               else
                   next = eol + Environment.NewLine.Length;

               // Copy this line of text, breaking into smaller lines as needed
               if (eol > pos)
               {
                   do
                   {
                       int len = eol - pos;
                       if (len > width)
                           len = BreakLine(text, pos, width);
                       sb.Append(text, pos, len);
                       sb.Append("<br>");//sb.Append(Environment.NewLine);

                       // Trim whitespace following break
                       pos += len;
                       while (pos < eol && Char.IsWhiteSpace(text[pos]))
                           pos++;
                   } while (eol > pos);
               }
               else sb.Append(Environment.NewLine); // Empty line
           }
           return sb.ToString();
       }
       private static int BreakLine(string text, int pos, int max)
       {
           // Find last whitespace in line
           int i = max;
           while (i >= 0 && !Char.IsWhiteSpace(text[pos + i]))
               i--;

           // If no whitespace found, break at maximum length
           if (i < 0)
               return max;

           // Find start of whitespace
           while (i >= 0 && Char.IsWhiteSpace(text[pos + i]))
               i--;

           // Return length of text before whitespace
           return i + 1;
       }


       protected string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
       protected string TruncarString(string Texto, int Longitud)
        {
            return TruncateString(HttpUtility.HtmlDecode(Texto), Longitud, TruncateOptions.IncludeEllipsis);
        }
       public enum TruncateOptions
        {
            None = 0x0,
            [Description("Make sure that the string is not truncated in the middle of a word")]
            FinishWord = 0x1,
            [Description("If FinishWord is set, this allows the string to be longer than the maximum length if there is a word started and not finished before the maximum length")]
            AllowLastWordToGoOverMaxLength = 0x2,
            [Description("Include an ellipsis HTML character at the end of the truncated string.  This counts as one of the characters for the maximum length")]
            IncludeEllipsis = 0x4
        }
       public string TruncateString(string valueToTruncate, int maxLength, TruncateOptions options)
        {
            if (valueToTruncate == null || maxLength <= 0)
            {
                return "";
            }

            if (valueToTruncate.Length <= maxLength)
            {
                return valueToTruncate;
            }

            bool includeEllipsis = (options & TruncateOptions.IncludeEllipsis) == TruncateOptions.IncludeEllipsis;
            bool finishWord = (options & TruncateOptions.FinishWord) == TruncateOptions.FinishWord;
            bool allowLastWordOverflow = (options & TruncateOptions.AllowLastWordToGoOverMaxLength) == TruncateOptions.AllowLastWordToGoOverMaxLength;

            string retValue = valueToTruncate;

            if (includeEllipsis)
            {
                maxLength -= 1;
            }

            int lastSpaceIndex = retValue.LastIndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);

            if (!finishWord)
            {
                retValue = retValue.Remove(maxLength);
            }
            else if (allowLastWordOverflow)
            {
                int spaceIndex = retValue.IndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);
                if (spaceIndex != -1)
                {
                    retValue = retValue.Remove(spaceIndex);
                }
            }
            else if (lastSpaceIndex > -1)
            {
                retValue = retValue.Remove(lastSpaceIndex);
            }

            if (includeEllipsis && retValue.Length < valueToTruncate.Length)
            {
                retValue += " [&hellip;]";
            }
            return retValue;
        }




       public StringBuilder getBodyNewsletter(string url, DSNewsLetter.news_getNoticiasDataTable dt, string fecha, string numero, string path, bool onlypreview, ref AlternateView htmlView)
       {
                    
           
           StringBuilder Body = new StringBuilder();
           Body.Append("");
           ControllerNewsletter cn = new ControllerNewsletter();


           if (fecha != "")
           {
               DateTime f = DateTime.Parse(fecha);
               fecha = formatFecha(f.Day, f.Month, f.Year);
           }


           Hashtable imagenes = new Hashtable();

            #region Estilos
           //Estilos

           Body.Append("<style type='text/css'>");
           Body.Append("* {font-family: Verdana !important} ");           
           Body.Append("#outlook a {padding:0;} ");           
           Body.Append("body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; font-family: Verdana; font-size: 12px;}");
           Body.Append("html,body{height: 100%;}");
           Body.Append(".ExternalClass {width:100%;} ");
           Body.Append(".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} ");
           Body.Append("#backgroundTable {margin:0; padding:0; width:800px !important; line-height: 100% !important;}");
           Body.Append("img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}");
           Body.Append("a img {border:none;}");
           Body.Append(".image_fix {display:block;}");
           Body.Append("p {margin: 0.5em 0; }");
           Body.Append("a { font: 13px Verdana; color: #555555; }");
           Body.Append("span { color: #009bd9; font: 13px Verdana; }");
           //Body.Append("h1, h2, h3, h4, h5, h6 {color: black !important;}");
           //Body.Append("h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}");
           //Body.Append("h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {");
           //Body.Append("color: red !important;");
           //Body.Append("}");

           Body.Append("h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {");
           Body.Append("	color: purple !important;");
           Body.Append("}");
           Body.Append("table td {border-collapse: collapse;}");
           Body.Append("table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }");
           //Body.Append("a {color: #009bd9;}");
           Body.Append("</style>");
            #endregion

            #region Header
            //Header
           Body.Append("<table align='center' cellpadding='0' cellspacing='0' border='0' id='backgroundTable' style='font-family: Verdana; margin: 0 auto; border-left:1px solid #EAEAEA; border-right:1px solid #EAEAEA'>");
            Body.Append("<tr bgcolor='#019BD9'>");
            Body.Append("<td>");

            Body.Append("<table cellpadding='0' cellspacing='0' border='0' id='backgroundTable' width='800'>");
            Body.Append("<tr>");

            Body.Append("<td width='20'> </td>");
            Body.Append("<td width='760'>");
            Body.Append("<table>");
            Body.Append("<tr>");
            Body.Append("<td>");

            if (onlypreview)
                Body.Append("<img src='" + url + "/noticias/newsletter/logo.gif' alt='SCJohnson'/>");
            else
            {
                Body.Append("<img src='cid:logo' />");
                imagenes.Add("logo", path + "/noticias/newsletter/logo.gif");
            }

            Body.Append("</td>");
            Body.Append("<td width='94%' align='right'>");
            Body.Append("<h2 style='font-size:19px;font-family:Verdana;font-weight:bold;color:#ffffff;'>SCJ News </h2>"); ///numero
            Body.Append("<h3 style='font-size:14px;font-family:Verdana;color:#ffffff;'>" + fecha + "</h3>");
            Body.Append("</td>");
            Body.Append("<td width='2%'>");
            Body.Append("</td>");
            Body.Append("<td>");
            if (onlypreview)
                Body.Append("<img src='" + url + "/noticias/newsletter/inspired.gif' alt='SCJohnson'/>");
            else
            {
                Body.Append("<img src='cid:inspired'  />");
                imagenes.Add("inspired", path + "/noticias/newsletter/inspired.gif");
            }
            Body.Append("</td>");
            Body.Append("</tr>");
            Body.Append("</table>");
            Body.Append("</td>");
            Body.Append("<td width='20'> </td>");
            Body.Append("</tr>");
            Body.Append("</table>");
            Body.Append("</td>");
            Body.Append("</tr>");




            //Header
            #endregion

            int titulolargo=95;
            int copetelargo=95;
            int textolargo = 95;

            string tableWidth_a = "";


            #region EquipoSCJ
           //Equipo SCJ 
            if (dt.Select("CategoriaDescripcion='Equipo SCJ'").Count() > 0 )
            {

                Body.Append("<tr class='block'>");
                Body.Append("<td>");
                Body.Append("<table width='800' cellspacing='0' cellpading='0' border='0'>");

                Body.Append("<tr>");//tr1

                Body.Append("<td  width='20'>");
                Body.Append("</td>");
                Body.Append("<td  width='760'>");



                Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");
                //Logo tr solo para el logo colspan=3
                Body.Append("<tr>");
                if (onlypreview)
                Body.Append("<td> <br><br> <img src='" + url + "/noticias/newsletter/equipoSCJIcon.gif' alt='Equipo SCJ' /><br><br></td>");
                else                                
                {
                    imagenes.Add("equipoSCJIcon", path + "/noticias/newsletter/equipoSCJIcon.gif");
                    Body.Append("<td colspan='3'> <br><br>  <img src='cid:equipoSCJIcon'/> <br><br></td>");

                }

                Body.Append("</tr>");

                //Contenido
                Body.Append("<tr>");



                int cantidad = dt.Select("CategoriaDescripcion='Equipo SCJ'").Count();
                //Una sola noticia
                if (dt.Select("CategoriaDescripcion='Equipo SCJ'").Count() == 1)
                {
                    Body.Append("<td width='100%'>");
                    DataRow[] dr = dt.Select("CategoriaDescripcion='Equipo SCJ'");
                       Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr[0],true,onlypreview,path,ref imagenes);
                    
                    Body.Append("</td>");
                }

                //Mas de una noticia
                if (dt.Select("CategoriaDescripcion='Equipo SCJ'").Count() > 1)
                {
                    Body.Append("<td  style='border:0px solid #ffffff;padding:10px'>");//borde
                     Body.Append("<table>");//
                     Body.Append("<tr>");//

                  


                    Body.Append("<td width='370' valign='top'>");//Noticias impares

                    Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table1                    
                    Body.Append("<tr>");
                    Body.Append("<td>");
                    Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table2


                    int filaimpar = 1;
                    foreach (DSNewsLetter.news_getNoticiasRow dr in dt.Select("CategoriaDescripcion='Equipo SCJ'"))
                    {
                        if (filaimpar % 2 != 0)
                        {
                            //impares
                            AjustaTexto("Equipo SCJ", dr["titulo"].ToString(), dr["copete"].ToString(), dr["path"].ToString(), dr["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);
                            Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr,false,onlypreview,path,ref imagenes);
                        }
                        filaimpar++;
                    }

                    Body.Append("</table>");//table2
                    
                    Body.Append("</table>");//table1

                    Body.Append("</td>");//Noticias impares



                    Body.Append("<td width='20'>");
                    Body.Append("</td>");


                    Body.Append("<td width='370' valign='top'>");//td1
                    //Noticias pares
                    Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table1
                    Body.Append("<tr>");
                    Body.Append("<td>");
                    Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table2
   

                    int filapar = 1;
                    foreach (DSNewsLetter.news_getNoticiasRow dr in dt.Select("CategoriaDescripcion='Equipo SCJ'"))
                    {

                        if (filapar % 2 == 0)
                        {
                            //pares
                            AjustaTexto("Equipo SCJ", dr["titulo"].ToString(), dr["copete"].ToString(), dr["path"].ToString(), dr["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);
                            Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr, false, onlypreview, path, ref imagenes);
                        }
                        filapar++;
                    }

                    Body.Append("</table>");//table2

                    Body.Append("</table>");//table1

                    Body.Append("</td>");//td1


                    Body.Append("</tr>");//cierra borde
                    Body.Append("</table>");//
                    Body.Append("</td>");//


                }//Mas de una noticia





                //fin    Contenido
                Body.Append("</tr>");

              
                Body.Append("</table>");
                Body.Append("</td>");//760
                Body.Append("<td width='20'>");
                Body.Append("</td>");

                Body.Append("</tr>");//tr1

                //separador
                Body.Append("<tr>");
                Body.Append("<td colspan='3' height='15' >");
                Body.Append("</td>");
                Body.Append("</tr>");
                //separador

                Body.Append("</table>");
                

            }
            ////Equipo SCJ y Avisos
            #endregion


            #region Novedades        
            //Novedades
            if (dt.Select("CategoriaDescripcion='Novedades'").Count() > 0)
            {


                Body.Append("<tr bgcolor='#c3d941'>");
                Body.Append("<td>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='800'>");
                //
                Body.Append("<tr>");
                Body.Append("<td  colspan='3'>");
                if (onlypreview)
                    Body.Append("<img src='" + url + "/noticias/newsletter/franja-horizontal-verde-limon.jpg' alt='Novedades' />");
                else
                {
                    Body.Append("<img src='cid:novedadesFranja'/>");
                    imagenes.Add("novedadesFranja", path + "/noticias/newsletter/franja-horizontal-verde-limon.jpg");
                }

                Body.Append("</td>");
                Body.Append("</tr>");
                //
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td></td>");
                if (onlypreview)
                    Body.Append("<td><img src='" + url + "/noticias/newsletter/novedadesIcon.gif' alt='Novedades' /></td>");
                else
                {
                    Body.Append("<td><img src='cid:novedadesIcon'/> </td>");
                    imagenes.Add("novedadesIcon", path + "/noticias/newsletter/novedadesIcon.gif");
                }

                Body.Append("<td> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td width='20'> </td>");
                Body.Append("<td width='760'>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
                Body.Append("<tr>");


                
                int cantidadxfila = 4;
                int cantidad = dt.Select("CategoriaDescripcion='Novedades'").Count();
                int cantidadfilas = (int)Math.Ceiling((Double)cantidad / (Double)cantidadxfila);
                int cantidadmaxima = cantidadxfila * cantidadfilas;
                DataRow[] dr = dt.Select("CategoriaDescripcion='Novedades'");
                int proxima = 0;
                int filanro = 1;


                if (cantidad >= 4) tableWidth_a = "175";
                if (cantidad == 3) tableWidth_a = "240";
                if (cantidad == 2) tableWidth_a = "370";
                if (cantidad == 1) tableWidth_a = "760";
                
                for (int i = 0; i < cantidad; i++)
                {


                    if (i > cantidadxfila -1) //o sea mas de 4 noticias corto
                    {
                        proxima = i;
                        filanro++;
                        break;
                    }


                    AjustaTexto("Novedades", dr[i]["titulo"].ToString(), dr[i]["copete"].ToString(), dr[i]["path"].ToString(), dr[i]["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);

                    switch (cantidad)
                    {
                        case 4:
                            Block_A(url, Body, titulolargo, copetelargo, textolargo, tableWidth_a, dr, i, cantidad,onlypreview,ref imagenes,path);
                            break;
                        case 3:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_a, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                        case 2:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_a, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                        case 1:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_a, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                    }

                }


                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("<td width='20'> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("</tr>");


               


            }
            //Novedades

            #endregion

           //Avisos no debe ser la primera categoria
            bool avisos = false;
            bool writeavisos = false;
            if ((dt.Select("CategoriaDescripcion='Equipo SCJ'").Count() > 0) ||
                 (dt.Select("CategoriaDescripcion='Novedades'").Count() > 0) )
           {
               avisos = true;
           }


            writeAvisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo, ref avisos, ref writeavisos);


            #region Afamilycompany
            string tableWidth_b = "";
            // A Family Company - Máximo 3 notas
            if (dt.Select("CategoriaDescripcion='A family Company'").Count() > 0)
            {
                Body.Append("<tr bgcolor='#FEC938'>");
                Body.Append("<td>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='800'>");
                //
                Body.Append("<tr>");
                Body.Append("<td  colspan='3'>");
                if (onlypreview)
                    Body.Append("<img src='" + url + "/noticias/newsletter/franja-horizontal-amarilla.jpg' alt='A Family Company' />");
                else
                {
                    Body.Append("<img src='cid:familycompanysFranja'/>");
                    imagenes.Add("familycompanysFranja", path + "/noticias/newsletter/franja-horizontal-amarilla.jpg");
                }

                Body.Append("</td>");
                Body.Append("</tr>");
                //
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td> </td>");
                if (onlypreview)
                    Body.Append("<td><img src='" + url + "/noticias/newsletter/familyIcon.gif' alt='A Family Company'/></td>");
                else
                {
                    Body.Append("<td><img src='cid:familyIcon'  /></td>");
                    imagenes.Add("familyIcon", path + "/noticias/newsletter/familyIcon.gif");
                }
                Body.Append("<td> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");


                Body.Append("<tr>");
                Body.Append("<td width='20'> </td>");
                Body.Append("<td width='760'>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
                Body.Append("<tr>");



                int cantidadxfila = 3;
                int cantidad = dt.Select("CategoriaDescripcion='A family Company'").Count();
                int cantidadfilas = (int)Math.Ceiling((Double)cantidad / (Double)cantidadxfila);
                int cantidadmaxima = cantidadxfila * cantidadfilas;
                DataRow[] dr = dt.Select("CategoriaDescripcion='A family Company'");
                int proxima = 0;
                int filanro = 1;


                if (cantidad >= 4) tableWidth_b = "175";
                if (cantidad == 3) tableWidth_b = "240";
                if (cantidad == 2) tableWidth_b = "370";
                if (cantidad == 1) tableWidth_b = "760";



                for (int i = 0; i < cantidad; i++)
                {


                    if (i > cantidadxfila - 1) //o sea mas de 4 noticias corto
                    {
                        proxima = i;
                        filanro++;
                        break;
                    }

                    AjustaTexto("A family Company", dr[i]["titulo"].ToString(), dr[i]["copete"].ToString(), dr[i]["path"].ToString(), dr[i]["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);

                    switch (cantidad)
                    {
                        case 3:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad,true,onlypreview,ref imagenes,path);
                            break;
                        case 2:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, true, onlypreview, ref imagenes, path);
                            break;
                        case 1:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, true, onlypreview, ref imagenes, path);
                            break;
                    }
                }


                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("<td width='20'> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("</tr>");


              

            }// A Family Company

            #endregion


            if (dt.Select("CategoriaDescripcion='A family Company'").Count() > 0)
            {
                avisos = true;
            }


            writeAvisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo, ref avisos, ref writeavisos);



            #region ViviSCJ
            //Vivi SCJ

            if (dt.Select("CategoriaDescripcion='Vivi SCJ'").Count() > 0)
            {

                Body.Append("<tr bgcolor='#b81137'>");
                Body.Append("<td>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='800'>");
                //
                Body.Append("<tr>");
                Body.Append("<td  colspan='3'>");
                if (onlypreview)
                    Body.Append("<img src='" + url + "/noticias/newsletter/franja-horizontal-roja.jpg' alt='Vivi SCJ' />");
                else
                {
                    Body.Append("<img src='cid:viviscjFranja'/>");
                    imagenes.Add("viviscjFranja", path + "/noticias/newsletter/franja-horizontal-roja.jpg");
                }

                Body.Append("</td>");
                Body.Append("</tr>");
                //

                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td> </td>");
                if (onlypreview)
                    Body.Append("<td><img src='" + url + "/noticias/newsletter/viviSCJIcon.gif' alt='Vivi SCJ'/></td>");
                else
                {
                    Body.Append("<td><img src='cid:viviSCJIcon'  /></td>");
                    imagenes.Add("viviSCJIcon", path + "/noticias/newsletter/viviSCJIcon.gif");
                }
                Body.Append("<td> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");

                Body.Append("<tr>");
                Body.Append("<td width='20'> </td>");
                Body.Append("<td width='760'>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
                Body.Append("<tr>");



                int cantidadxfila = 2;
                int cantidad = dt.Select("CategoriaDescripcion='Vivi SCJ'").Count();
                int cantidadfilas = (int)Math.Ceiling((Double)cantidad / (Double)cantidadxfila);
                int cantidadmaxima = cantidadxfila * cantidadfilas;
                DataRow[] dr = dt.Select("CategoriaDescripcion='Vivi SCJ'");
                int proxima = 0;
                int filanro = 1;


                if (cantidad >= 4) tableWidth_b = "175";
                if (cantidad == 3) tableWidth_b = "240";
                if (cantidad == 2) tableWidth_b = "370";
                if (cantidad == 1) tableWidth_b = "760";


                for (int i = 0; i < cantidad; i++)
                {

                    if (i > cantidadxfila - 1) //o sea mas de 4 noticias corto
                    {
                        proxima = i;
                        filanro++;
                        break;
                    }

                    AjustaTexto("Vivi SCJ", dr[i]["titulo"].ToString(), dr[i]["copete"].ToString(), dr[i]["path"].ToString(), dr[i]["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);


                    switch (cantidad)
                    {
                        case 4:
                            Block_A(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad,onlypreview,ref imagenes,path);
                            break;
                        case 3:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad,false,onlypreview,ref imagenes,path);
                            break;
                        case 2:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                        case 1:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                    }

                }



                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("<td width='20'> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("</table>");
                Body.Append("</td>");
                Body.Append("</tr>");



           

            }

            //Vivi SCJ
            #endregion


            if (dt.Select("CategoriaDescripcion='Vivi SCJ'").Count() > 0)
            {
                avisos = true;
            }

            writeAvisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo, ref avisos, ref writeavisos);

    

            #region Acordate

            //Acordate
            if (dt.Select("CategoriaDescripcion='Acordate'").Count() > 0)
            {
                Body.Append("<tr bgcolor='#297535'>");
                Body.Append("<td>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='800'>");
                //
                Body.Append("<tr>");
                Body.Append("<td  colspan='3'>");
                if (onlypreview)
                    Body.Append("<img src='" + url + "/noticias/newsletter/franja-horizontal-verde.jpg' alt='Vivi SCJ' />");
                else
                {
                    Body.Append("<img src='cid:acordateFranja'/>");
                    imagenes.Add("acordateFranja", path + "/noticias/newsletter/franja-horizontal-verde.jpg");
                }

                Body.Append("</td>");
                Body.Append("</tr>");
                //
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td> </td>");
                if(onlypreview)
                Body.Append("<td><img src='" + url + "/noticias/newsletter/acordateIcon.gif' alt='Acordate'/></td>");
                else
                {
                    Body.Append("<td><img src='cid:acordateIcon'  /></td>");
                    imagenes.Add("acordateIcon", path + "/noticias/newsletter/acordateIcon.gif");
                }
                Body.Append("<td> </td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td height='13' colspan='3'></td>");
                Body.Append("</tr>");
                Body.Append("<tr>");
                Body.Append("<td width='20'> </td>");
                Body.Append("<td width='760'>");
                Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
                Body.Append("<tr>");


                int cantidadxfila = 4;
                int cantidad = dt.Select("CategoriaDescripcion='Acordate'").Count();
                int cantidadfilas = (int)Math.Ceiling((Double)cantidad / (Double)cantidadxfila);
                int cantidadmaxima = cantidadxfila * cantidadfilas;
                DataRow[] dr = dt.Select("CategoriaDescripcion='Acordate'");
                int proxima = 0;
                int filanro = 1;


                if (cantidad >= 4) tableWidth_b = "175";
                if (cantidad == 3 ) tableWidth_b = "240";
                if (cantidad == 2) tableWidth_b = "370";
                if (cantidad == 1) tableWidth_b = "760";

                

                // Desde acá se repite
                for (int i = 0; i < cantidad ; i++)
                {

                    if (i > cantidadxfila - 1) //o sea mas de 4 noticias corto
                    {
                        proxima = i;
                        filanro++;
                        break;
                    }

                    AjustaTexto("Acordate", dr[i]["titulo"].ToString(), dr[i]["copete"].ToString(), dr[i]["path"].ToString(), dr[i]["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo,cantidad);

                    switch (cantidad)
                    {
                        case 4:
                            Block_A(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, onlypreview, ref imagenes, path);
                            break;
                        case 3:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, false,onlypreview, ref imagenes, path);
                            break;
                        case 2:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                        case 1:
                            Block_B(url, Body, titulolargo, copetelargo, textolargo, tableWidth_b, dr, i, cantidad, false, onlypreview, ref imagenes, path);
                            break;
                    }

                } 

                    Body.Append("</tr>");
                    Body.Append("</table>");
                    Body.Append("</td>");
                    Body.Append("<td width='20'> </td>");
                    Body.Append("</tr>");
                    Body.Append("<tr>");
                    Body.Append("<td height='13' colspan='3'></td>");
                    Body.Append("</tr>");
                    Body.Append("</table>");
                    Body.Append("</td>");
                    Body.Append("</tr>");


            }
            //Acordate

            #endregion

            if (dt.Select("CategoriaDescripcion='Acordate'").Count() > 0)
            {
                avisos = true;
            }


            writeAvisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo, ref avisos, ref writeavisos);

           //En caso de ser la unica categoria
           if(!writeavisos)
            Avisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo);


            //Footer
           Body.Append("<tr bgColor='#402B84' align='right'>");
           if (onlypreview)
               Body.Append("<td><img src='" + url + "/noticias/newsletter/footer.jpg' alt='SCJohnson'/></td>");
           else
           {
               Body.Append("<td><img src='cid:footer'  /></td>");
               imagenes.Add("footer", path + "/noticias/newsletter/footer.jpg");
           }
           Body.Append("</tr>");


           Body.Append("</table>");


           if (!onlypreview) //adjuntos imagnees
           {
               htmlView = AlternateView.CreateAlternateViewFromString(Body.ToString(), null, "text/html");

               foreach (DictionaryEntry item in imagenes)
               {
                   LinkedResource img = new LinkedResource(item.Value.ToString());
                   img.ContentId = item.Key.ToString();
                   htmlView.LinkedResources.Add(img);
               }
           }

           return Body;

           

       }

       private void writeAvisos(string url, DSNewsLetter.news_getNoticiasDataTable dt, string path, bool onlypreview, StringBuilder Body, ref Hashtable imagenes, ref int titulolargo, ref int copetelargo, ref int textolargo, ref bool avisos, ref bool writeavisos)
       {
           if (avisos && !writeavisos)
           {
               Avisos(url, dt, path, onlypreview, Body, ref imagenes, ref titulolargo, ref copetelargo, ref textolargo);
               avisos = true;
               writeavisos = true;
           }
       }

       private void Avisos(string url, DSNewsLetter.news_getNoticiasDataTable dt, string path, bool onlypreview, StringBuilder Body, ref Hashtable imagenes, ref int titulolargo, ref int copetelargo, ref int textolargo)
       {
           #region Avisos
           if (dt.Select("CategoriaDescripcion='Avisos'").Count() > 0)
           {
               Body.Append("<tr class='block'>");
               Body.Append("<td>");
               Body.Append("<table width='800' cellspacing='0' cellpading='0' border='0'>");
               //
               Body.Append("<tr>");
               Body.Append("<td  colspan='3'>");
               if (onlypreview)
                   Body.Append("<img src='" + url + "/noticias/newsletter/franja-horizontal-blanca.jpg' alt='Avisos' />");
               else
               {
                   Body.Append("<img src='cid:avisosFranja'/>");
                   imagenes.Add("avisosFranja", path + "/noticias/newsletter/franja-horizontal-blanca.jpg");
               }

               Body.Append("</td>");
               Body.Append("</tr>");
               //


               Body.Append("<tr>");//tr1

               Body.Append("<td  width='20'>");
               Body.Append("</td>");
               Body.Append("<td  width='760'>");


               int cantidad = dt.Select("CategoriaDescripcion='Avisos'").Count();

               Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>"); //una sola noticia


               


               //Logo tr solo para el logo colspan=3
               Body.Append("<tr>");
               if (onlypreview)
                   Body.Append("<td> <br> <img src='" + url + "/noticias/newsletter/avisosIcon.gif' alt='Avisos' /><br><br></td>");
               else
               {
                   Body.Append("<td colspan='3'> <br> <img src='cid:avisosIcon'  /> <br><br></td>");
                   imagenes.Add("avisosIcon", path + "/noticias/newsletter/avisosIcon.gif");
               }
               Body.Append("</tr>");

               //Contenido
               Body.Append("<tr>");


               //Una sola noticia
               if (dt.Select("CategoriaDescripcion='Avisos'").Count() == 1)
               {
                   Body.Append("<td width='100%'>");
                   DataRow[] dr = dt.Select("CategoriaDescripcion='Avisos'");
                   Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr[0], true, onlypreview, path, ref imagenes);

                   Body.Append("</td>");
               }


               //Mas de una noticia
               if (dt.Select("CategoriaDescripcion='Avisos'").Count() > 1)
               {
                   Body.Append("<td  style='border:0px solid #ffffff;padding:10px'>");//borde
                   Body.Append("<table>");//
                   Body.Append("<tr>");//



                   Body.Append("<td width='370' valign='top'>");//Noticias impares

                   Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table1
                   Body.Append("<tr>");
                   Body.Append("<td>");
                   Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table2


                   int filaimpar = 1;
                   foreach (DSNewsLetter.news_getNoticiasRow dr in dt.Select("CategoriaDescripcion='Avisos'"))
                   {
                       if (filaimpar % 2 != 0)
                       {
                           //impares
                           AjustaTexto("Avisos", dr["titulo"].ToString(), dr["copete"].ToString(), dr["path"].ToString(), dr["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo, cantidad);
                           Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr, false, onlypreview, path, ref imagenes);
                       }
                       filaimpar++;
                   }

                   Body.Append("</table>");//table2

                   Body.Append("</table>");//table1

                   Body.Append("</td>");//Noticias impares



                   Body.Append("<td width='20'>");
                   Body.Append("</td>");


                   Body.Append("<td width='370' valign='top'>");//td1
                   //Noticias pares
                   Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table1
                   Body.Append("<tr>");
                   Body.Append("<td>");
                   Body.Append("<table width='100%' cellpadding='0' cellspacing='0' border='0'>");//table2


                   int filapar = 1;
                   foreach (DSNewsLetter.news_getNoticiasRow dr in dt.Select("CategoriaDescripcion='Avisos'"))
                   {

                       if (filapar % 2 == 0)
                       {
                           //pares
                           AjustaTexto("Avisos", dr["titulo"].ToString(), dr["copete"].ToString(), dr["path"].ToString(), dr["flyer"].ToString(), ref titulolargo, ref copetelargo, ref textolargo, cantidad);
                           Block_EquipoSCJAvisos(url, Body, titulolargo, copetelargo, dr, false, onlypreview, path, ref imagenes);
                       }
                       filapar++;
                   }

                   Body.Append("</table>");//table2

                   Body.Append("</table>");//table1

                   Body.Append("</td>");//td1

                   Body.Append("</tr>");//cierra borde
                   Body.Append("</table>");//
                   Body.Append("</td>");//


               }//Mas de una noticia



               //fin 
               Body.Append("</tr>");//contenido



               Body.Append("</table>");
               Body.Append("</td>");//760
               Body.Append("<td width='20'>");
               Body.Append("</td>");

               Body.Append("</tr>");//tr1

               //separador
               Body.Append("<tr>");
               Body.Append("<td colspan='3' height='15' >");
               Body.Append("</td>");
               Body.Append("</tr>");
               //separador
               Body.Append("</table>");


           }
           #endregion
       }


       private void Block_A(string url, StringBuilder Body, int titulolargo, int copetelargo, int textolargo, string tableWidth_b, DataRow[] dr, int i, int cantidad, bool onlypreview, ref Hashtable imagenes,string path)
       {
           Body.Append("<td width='" + tableWidth_b + "'  bgcolor='#FFFFFF' valign='top' height='100%'>");

           Body.Append("<table height='100%'  width='100%'>");//table 1
           Body.Append("<tr>");
           Body.Append("<td style='padding:10px'>");

           Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%' height='100%'>");
           Body.Append("<tr>");

           // Asi es cuando tiene imagen
           if ((dr[i]["path"].ToString() != "") && (dr[i]["path"].ToString().Contains(".jpg") || dr[i]["path"].ToString().Contains(".png") ||  dr[i]["path"].ToString().Contains(".jpeg") ) )
           {
               Body.Append("<td valign='top'>");
               Body.Append("<div style='text-align:center'>");

               if (onlypreview)
               {
                   if (dr[i]["flyer"].ToString() != "1")
                   {
                       if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                           Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='155' height='65' /></a>");
                       else
                           Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["path"].ToString() + " 'width='155' height='65' /></a>");
                   }
                   else
                       Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='155' height='65' /></a>");

               }
               else
               {
                   Body.Append("<a href='#'><img src='cid:" + dr[i]["infoid"].ToString() + "' width='155' height='65' /></a>");

                   if (dr[i]["flyer"].ToString() != "1")
                   {
                       if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                           imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
                       else
                       imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["path"].ToString());

                   }
                   else
                       imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
               }

               Body.Append("<br><br></div>");
           }
           // Asi es cuando no
           else
           {
               Body.Append("<td align='left' valign='top'>");
           }


           Body.Append("<span>");
           Body.Append("<a>");
           Body.Append("<strong style='color:#009bd9;font-family:Verdana;font-size:13px;'>");


           //titulo
           if (dr[i]["titulo"].ToString().Length > titulolargo)
               Body.Append(WordWrap(dr[i]["titulo"].ToString(), 16).Substring(0, titulolargo - 4) + "...");
           else
               Body.Append(dr[i]["titulo"].ToString());
           //titulo
           Body.Append("</strong>");
           Body.Append("</a>");
           //copete
           Body.Append("<p style='font-family:Verdana;font-size:11px;color:#009bd9;'>");
           if (dr[i]["copete"].ToString() != "")
           {
               if (dr[i]["copete"].ToString().Length > copetelargo)
                   Body.Append(dr[i]["copete"].ToString().Substring(0, copetelargo - 4) + "...");
               else
                   Body.Append(dr[i]["copete"].ToString());
           }
           Body.Append("</p>");
           //copete
           //titulo
           //texto
           
           if ((dr[i]["texto"].ToString() != "" && textolargo > 0) || (dr[i]["flyer"].ToString()=="1")) // y tine flyer muestra texto
           {
               Body.Append("<p style='font-family:Verdana;font-size:11px;color:#7d7b7c'>");
               string texto = TruncarString(StripTagsCharArray(dr[i]["texto"].ToString()), 100);
               Body.Append(texto.ToString());
               Body.Append("</p>");
           }
           
           //texto


           Body.Append("</span>");

           
           NoticiasFiles(url, Body, dr, i, onlypreview,ref imagenes,path);

           Body.Append("</td>");
           Body.Append("</tr>");
           Body.Append("<tr>");
           Body.Append("<td width='100%' colspan='3' valign='bottom' align='right'>");

           //link
           Body.Append(HtmlLink(dr[i]["Privacidad"].ToString(), dr[i]["InfoID"].ToString(), url,onlypreview,path,ref imagenes));
           //link

           Body.Append("</td>");
           Body.Append("</tr>");
           Body.Append("</table>");

           Body.Append("</td>");
           Body.Append("</tr>");
           Body.Append("</table>");//table 1

           Body.Append("</td>");
           if (i < cantidad - 1)
           {
               Body.Append("<td width='20'></td>");
           }

           // Hasta acá se repite
       }

       private void Block_B(string url, StringBuilder Body, int titulolargo, int copetelargo, int textolargo, string tableWidth_b, DataRow[] dr, int i, int cantidad,bool esfamilycompany,bool onlypreview,ref Hashtable imagenes,string path)
       {
           Body.Append("<td width='" + tableWidth_b + "'  bgcolor='#FFFFFF' valign='top' height='100%' class='nivel_1'>");

           Body.Append("<table height='100%' width='100%'>");//table 1
           Body.Append("<tr>");
           Body.Append("<td style='padding:10px' height='100%' class='nivel_2'>");

           Body.Append("<table cellpadding='0' cellspacing='0' border='0' width='100%' height='100%'>");//table 2
           

           // Asi es cuando tiene imagen
           if ((dr[i]["path"].ToString() != "") && (dr[i]["path"].ToString().Contains(".jpg") || dr[i]["path"].ToString().Contains(".png") || dr[i]["path"].ToString().Contains(".jpeg")))
           {

               if (cantidad == 3 && !esfamilycompany)
               {
                   Body.Append("<tr>");
                   Body.Append("<td valign='top'>");
                   Body.Append("<div style='text-align:center'>");
                   if (onlypreview)
                   {
                       if (dr[i]["flyer"].ToString() != "1")
                       {
                              if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                                 Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='220' height='92' /></a>");
                              else
                                  Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["path"].ToString() + " 'width='220' height='92' /></a>");
                       }
                       else
                           Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='220' height='92' /></a>");

                   }
                   else
                   {
                       Body.Append("<a href='#'><img src='cid:" + dr[i]["infoid"].ToString() + "' width='220' height='92' /></a>");

                       if (dr[i]["flyer"].ToString() != "1")
                       {
                           if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                               imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
                           else
                              imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["path"].ToString());
                       }
                       else
                           imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
                   }
                   Body.Append("<br><br></div>");
                   Body.Append("</td>");
                   Body.Append("</tr>");

                   Body.Append("<tr>");
               }

               if ((cantidad == 2 || cantidad == 1) || esfamilycompany)
               {
                   Body.Append("<tr>");

                   if (esfamilycompany)
                       Body.Append("<td valign='top' width='87' >");
                   else
                       Body.Append("<td valign='top' width='165' >");

                   Body.Append("<div>");

                   if (esfamilycompany)
                   {
                       if (onlypreview)
                           Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["path"].ToString() + " 'width='77' height='70' /></a>");
                       else
                       {
                           Body.Append("<a href='#'><img src='cid:" + dr[i]["infoid"].ToString() + " 'width='77' height='70' /></a>");
                           imagenes.Add( dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["path"].ToString());
                       }
                   }
                   else
                   {
                       if (onlypreview)
                       {
                           if (dr[i]["flyer"].ToString() != "1")
                           {
                               if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                                   Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='155' height='65' /></a>");
                               else
                                   Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["path"].ToString() + " 'width='155' height='65' /></a>");
                           }
                           else
                               Body.Append("<a href='#'><img src='" + url + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg' width='155' height='65' /></a>");
                       }
                       else
                       {
                          Body.Append("<a href='#'><img src='cid:" + dr[i]["infoid"].ToString() + "' width='155' height='65' /></a>");
                          if (dr[i]["flyer"].ToString() != "1")
                          {
                              if (File.Exists(path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg"))
                                  imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
                              else
                                  imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["path"].ToString());
                          }
                          else
                              imagenes.Add(dr[i]["infoid"].ToString(), path + "/noticias/Imagenes/" + dr[i]["infoid"].ToString() + "_flyercrop.jpg  ");
                       }
                   }

                   Body.Append("</div>");
                   Body.Append("</td>");
               }

              
           }

           //celda de texto
           Body.Append("<td valign='top' height='100%' class='nivel_3'>");

           Body.Append("<table height='100%' width='100%'>");//table 3
           Body.Append("<tr>");
           Body.Append("<td align='left' valign='top'>");

           Body.Append("<span>");
           Body.Append("<a>");
           Body.Append("<strong style='color:#009bd9;font-family:Verdana;font-size:13px;'>");


           //titulo
           if (dr[i]["titulo"].ToString().Length > titulolargo)
               Body.Append(WordWrap(dr[i]["titulo"].ToString(), 16).Substring(0, titulolargo - 4) + "...");
           else
               Body.Append(dr[i]["titulo"].ToString());
           //titulo
           Body.Append("</strong>");
           Body.Append("</a>");
           //copete
           
           if (dr[i]["copete"].ToString() != "")
           {
               Body.Append("<p style='font-family:Verdana;font-size:11px;color:#009bd9;'>");
               if (dr[i]["copete"].ToString().Length > copetelargo)
                   Body.Append(dr[i]["copete"].ToString().Substring(0, copetelargo - 4) + "...");
               else
                   Body.Append(dr[i]["copete"].ToString());
               Body.Append("</p>");
           }
           
           //copete
           //titulo
           //texto
           
           if (dr[i]["texto"].ToString() != "" && textolargo > 0)
           {
               Body.Append("<p style='font-family:Verdana;font-size:11px;color:#7d7b7c'>");
               string texto = TruncarString(StripTagsCharArray(dr[i]["texto"].ToString()), 100);
               Body.Append(texto.ToString());             
               Body.Append("</p>");
           }
           

           //texto

           Body.Append("</span>");


           NoticiasFiles(url, Body, dr, i, onlypreview,ref imagenes,path);
           
           Body.Append("</td>");
           Body.Append("</tr>");

           Body.Append("<tr>");
           Body.Append("<td align='right' valign='bottom'>");

           //link
           Body.Append(HtmlLink(dr[i]["Privacidad"].ToString(), dr[i]["InfoID"].ToString(), url, onlypreview, path, ref imagenes));
           //link

           Body.Append("</td>");
           Body.Append("</tr>");
           Body.Append("</table>");//table 3

           Body.Append("</td>"); ////fin celda de texto
           Body.Append("</tr>");


           Body.Append("</table>");//table 2
           Body.Append("</td>");

           Body.Append("</td>");
           Body.Append("</tr>");
           Body.Append("</table>");//table 1

           if (i < cantidad - 1)
           {
               Body.Append("<td width='20'></td>");
           }

           // Hasta acá se repite
       }


       private static void NoticiasFiles(string url, StringBuilder Body, DataRow[] dr, int i, bool onlypreview, ref Hashtable imagenes, string path)
       {
           ///Iconos de archivos
           ControllerNewsletter cnew = new ControllerNewsletter();
           DAL.DSNewsLetter.news_getNoticiasFilesDataTable dsFiles = cnew.getNoticiasFiles(int.Parse(dr[i]["infoid"].ToString()));
           int adjunto = 1;
           foreach (DAL.DSNewsLetter.news_getNoticiasFilesRow drFile in dsFiles.Rows)
           {
               string[] name  = drFile["path"].ToString().Split('.');
               string infoid= dr[i]["infoid"].ToString();
               string fileicon = "Generic-icon.png";
               string extension = name[1];

               if (extension != "jpg" && extension != "png" && extension != "gif")
               {
                   switch (extension)
                   {
                       case "pdf": fileicon = "Pdf-icon.png"; break;
                       case "xls": fileicon = "Excel-icon.png"; break;
                       case "xlsx": fileicon = "Excel-icon.png"; break;
                       case "ppt": fileicon = "PowerPoint-icon.png"; break;
                       case "pptx": fileicon = "PowerPoint-icon.png"; break;
                       case "doc": fileicon = "Word-icon.png"; break;
                       case "docx": fileicon = "Word-icon.png"; break;
                       case "zip": fileicon = "Zip-icon.png"; break;
                       case "rar": fileicon = "Zip-icon.png"; break;
                       case "txt": fileicon = "Txt-icon.png"; break;

                   }

                  // if (adjunto == 1) Body.Append("<br>");

                   if (onlypreview)
                   {
                       
                       Body.Append("<a TARGET='_blank' href='" + url + "/noticias/Imagenes/" + drFile["path"].ToString() + "'><img src='" + url + "/noticias/newsletter/" + fileicon + "'  alt='" + drFile["path"].ToString() + "' /></a>");
                       Body.Append("&nbsp");
                   }
                   else
                   {
                       Body.Append("<a TARGET='_blank' href='" + url + "/noticias/Imagenes/" + drFile["path"].ToString() + "'>");
                       Body.Append("<img src='cid:" + infoid + extension + adjunto + "'  alt='" + drFile["path"].ToString() + "'/></a>");
                       Body.Append("&nbsp");

                       imagenes.Add(infoid +extension + adjunto, path + "/noticias/Newsletter/" + fileicon);
                   }
                   adjunto++;
               }
           }
           ///Iconos archivos
       }


       private void Block_EquipoSCJAvisos(string url, StringBuilder Body, int titulolargo, int copetelargo,DataRow dr,bool UnaNoticia,bool onlypreview,string path,ref Hashtable imagenes )
       {
           // Acá empieza la repetición de los elementos
           Body.Append("<tr>");
           Body.Append("<td colspan='3' >");

           if (UnaNoticia)
           {
               Body.Append("<table width='100%'>");
               Body.Append("<tr>");
               Body.Append("<td colspan='2'>");

               //titulo
               Body.Append("<strong style='font-family:Verdana;font-size:13px;line-height:1.4;color:#737478'>");
               if (dr["titulo"].ToString().Length > titulolargo)
                   Body.Append(dr["titulo"].ToString().Substring(0, titulolargo - 4) + "...");
               else
                   Body.Append(dr["titulo"].ToString());
               Body.Append("</strong>");
               //titulo

               Body.Append("</td>");
               Body.Append("</tr>");

               //texto de la noticia
               Body.Append("<tr>");
               Body.Append("<td>");
               Body.Append("<p style='font-family:Verdana;font-size:11px;color:#7d7b7c'>");
               if (dr["texto"].ToString() != "")
               {
                   string texto = TruncarString(StripTagsCharArray(dr["texto"].ToString()), 100);
                   Body.Append(texto.ToString());
               }
               Body.Append("</p>");
               Body.Append("</td>");
    
               //link
               Body.Append("<td align='right' valing='bottom'>");
               Body.Append(HtmlLink(dr["Privacidad"].ToString(), dr["InfoID"].ToString(), url, onlypreview, path, ref imagenes));
               Body.Append("</td>");
               //link             
               Body.Append("</td>");
               Body.Append("</tr>");


               Body.Append("<tr>");
               Body.Append("<td colspan='2' style='padding-bottom:15px'>");
               if (onlypreview)
                   Body.Append("<img src='" + url + "/noticias/newsletter/separador2.gif'/>");//cambiar imagen
               else
               {
                   Body.Append("<img src='cid:" + "separador2_" + dr["infoid"].ToString() + "' />");
                   imagenes.Add("separador2_" + dr["infoid"].ToString(), path + "/noticias/newsletter/separador2.gif");
               }

               Body.Append("</td>");
               Body.Append("</tr>");

               Body.Append("</table>");
           }
           else
           {
               Body.Append("<table width='100%'>");

               Body.Append("<tr>");
               Body.Append("<td align='left'>");

               //titulo
               Body.Append("<strong style='font-family:Verdana;font-size:13px;line-height:1.4;color:#737478' >");
               if (dr["titulo"].ToString().Length > titulolargo)
                   Body.Append(dr["titulo"].ToString().Substring(0, titulolargo - 4) + "...");
               else
                   Body.Append(dr["titulo"].ToString());
               Body.Append("</strong>");
               //titulo

               Body.Append("</td>");


               //link
               Body.Append("<td align='right' valing='bottom'>");
               Body.Append(HtmlLink(dr["Privacidad"].ToString(), dr["InfoID"].ToString(), url, onlypreview, path, ref imagenes));
               Body.Append("</td>");
               //link

               Body.Append("</tr>");

               Body.Append("<tr>");
               Body.Append("<td height='10' colspan='2'></td>");
               Body.Append("</tr>");

               

               Body.Append("<tr>");
               Body.Append("<td colspan='2' style='padding-bottom:15px'>");

               if (onlypreview)
                   Body.Append("<img src='" + url + "/noticias/newsletter/separador.gif'/>");
               else
               {
                   
                   Body.Append("<img src='cid:" + "separador2_" + dr["infoid"].ToString() + "' />");
                   imagenes.Add("separador2_" + dr["infoid"].ToString(), path + "/noticias/newsletter/separador.gif");
               }

               Body.Append("</td>");
               Body.Append("</tr>");

               Body.Append("</table>");

           }

          // Body.Append("</td>");
           //Body.Append("</tr>");
          
       }

 
       public string HtmlLink(string privacidad,string InfoID,string url,bool onlypreview,string path,ref Hashtable imagenes)
       {
           //string link="<div align='right'> ";
           string link = "";

           if (privacidad == "0")
               if (onlypreview)
                   link += "<a TARGET='_blank' href='" + url + "?InfoID=" + InfoID + "'><img src='" + url + "/noticias/newsletter/imgVerMas.gif' alt='Ver más' /></a>";
               else
               {
                   link += "<a TARGET='_blank' href='" + url + "?InfoID=" + InfoID + "'><img src='cid:Info_" + InfoID + "'/> </a>";
                   imagenes.Add("Info_" + InfoID, path + "/noticias/newsletter/imgVerMas.gif");
               }
           else
           {
               if (onlypreview)
                   link += ("<a TARGET='_blank' href='" + url + "?privInfoID=" + CodValidacion(int.Parse(InfoID)) + "&InfoID=" + InfoID + "'> <img src='" + url + "/noticias/newsletter/imgVerMas.gif' alt='Ver más' /></a>");
               else
               {
                   link += ("<a TARGET='_blank' href='" + url + "?privInfoID=" + CodValidacion(int.Parse(InfoID)) + "&InfoID=" + InfoID + "'> <img src='cid:Info_" + InfoID + "'/> </a>");
                   imagenes.Add("Info_" + InfoID, path + "/noticias/newsletter/imgVerMas.gif");
               }
           }

           //link+="</div>";

           return link;
       }


       private string FillTexto(string texto, int fill,int width)
       {
           if (texto.Length <= fill)
           {
               int cantidad= fill - texto.Length ;

               for(int i=0;i<cantidad;i++)
               {
                   texto += "&nbsp;";
               }
           }

           return WordWrap(texto, width);
       }



       
    }
}

