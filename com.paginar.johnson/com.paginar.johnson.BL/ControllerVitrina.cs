﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;

namespace com.paginar.johnson.BL
{
   [DataObject]
    public class ControllerVitrina
    {
        private DSVitrina DSVitrina = new DSVitrina();

        private RespositoryVitrina _Repositorio;
        public RespositoryVitrina Repositorio
        {
            get
            {
                if (_Repositorio == null)
                    _Repositorio = new RespositoryVitrina();
                return _Repositorio;
            }

        }


        public int AgregarPublicacion(int usuarioid, int tipoid, DateTime fecha, DateTime fechalogo, string titulo, string descripcion, bool estado,string link ,DataTable dtlinks)
        {
            DSVitrina.vitra_PublicacionRow row = DSVitrina.vitra_Publicacion.Newvitra_PublicacionRow();
            row.UsuarioID = usuarioid;
            row.TipoId = tipoid;
            row.Fecha = fecha;
            row.FechaLogro = fechalogo;
            row.Titulo = titulo;
            row.Descripcion = descripcion;
            row.link = link;
            row.Estado = estado;

            DSVitrina.vitra_Publicacion.Addvitra_PublicacionRow(row);

            Repositorio.AdapterDSVitrina.vitra_PublicacionTableAdapter.Update(DSVitrina.vitra_Publicacion);

            int publicacionid = row.PublicacionId;

            foreach (DataRow dr in dtlinks.Rows)
            {
                AgregarLinks(dr["path"].ToString(), publicacionid);
            }


            return publicacionid;
        }


        public void ModificarPublicacion(int publicacionid, int usuarioid, int tipoid, DateTime fecha, DateTime fechalogro, string titulo, string descripcion, bool estado,string link, DataTable dtlinks)
        {
            Repositorio.AdapterDSVitrina.vitra_PublicacionTableAdapter.FillBy(DSVitrina.vitra_Publicacion, publicacionid);
            DSVitrina.vitra_PublicacionRow row = DSVitrina.vitra_Publicacion.FindByPublicacionId(publicacionid);
            row.TipoId = tipoid;
            row.Fecha = fecha;
            row.FechaLogro = fechalogro;
            row.Titulo = titulo;
            row.Descripcion = descripcion;
            row.link = link;
            row.Estado = estado;

            Repositorio.AdapterDSVitrina.vitra_PublicacionTableAdapter.Update(DSVitrina.vitra_Publicacion);

            BorrarLinks(publicacionid);

            foreach (DataRow dr in dtlinks.Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                    AgregarLinks(dr["path"].ToString(), publicacionid);
            }

        }


        public void BorrarPublicacion(int publicacionid)
        {
            BorrarLinks(publicacionid);
            Repositorio.AdapterDSVitrina.vitra_PublicacionTableAdapter.DeletePublicacion(publicacionid);
        }



        public void AgregarLinks(string path,int publicacionid)
        {
            DSVitrina.vitra_LinkRow row = DSVitrina.vitra_Link.Newvitra_LinkRow();
            row.Path = path;
            row.PublicacionId = publicacionid;

            DSVitrina.vitra_Link.Addvitra_LinkRow(row);
            Repositorio.AdapterDSVitrina.vitra_LinkTableAdapter.Update(DSVitrina.vitra_Link);

        }


        public void BorrarLinks(int publicacionid)
        {
            Repositorio.AdapterDSVitrina.vitra_LinkTableAdapter.DeleteByPublicacionID(publicacionid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVitrina.vitra_LinkDataTable getLinksByID(int publicacionid)
        {
            return Repositorio.AdapterDSVitrina.vitra_LinkTableAdapter.GetDataBy1(publicacionid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVitrina.vitra_PublicacionDataTable getPublicacion(int publicacionid)
        {
            return Repositorio.AdapterDSVitrina.vitra_PublicacionTableAdapter.GetDataBy1(publicacionid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVitrina.vitra_BuscadorDataTable getPublicaciones(int tipoid, int clusterid,int usuarioid,bool estado)
        {
            return Repositorio.AdapterDSVitrina.vitra_Buscador.GetData(tipoid, clusterid,usuarioid,estado);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSVitrina.vitra_TipoDataTable getTipo()
        {
            return Repositorio.AdapterDSVitrina.vitra_TipoTableAdapter.GetData();
        }
    }
}
