﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.BL
{
    public class Objetivo
    {
        public int Indice { get; set; }
        public string Objetivos { get; set; }
        public string Medidas { get; set; }
        public string Resultados { get; set; }

        public Objetivo(int indice, string objetivos, string medidas, string resultados)
        {
            this.Indice = indice;
            this.Objetivos = objetivos;
            this.Medidas = medidas;
            this.Resultados = resultados;
        }
    }
}
