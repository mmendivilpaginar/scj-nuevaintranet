﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;


namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerTelefoniaIP
    {
        private RepositoryTelefoniaIP _Repositorio;
        public RepositoryTelefoniaIP Repositorio
        {
            get 
            {
                if (_Repositorio == null) _Repositorio = new RepositoryTelefoniaIP();
                return _Repositorio;
            }
        }

    
    }
}
