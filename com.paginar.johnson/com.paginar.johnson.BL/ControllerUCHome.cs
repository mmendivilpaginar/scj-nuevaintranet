﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerUCHome
    {
        private DsUCHome UCHomeDS = new DsUCHome();


        private Repository_UCHome _Repositorio = new Repository_UCHome();

        public Repository_UCHome Repositiorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new Repository_UCHome();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getCumpleanioHoy()
        {
            return Repositiorio.GetCumpleanioHoy();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getCumpleanioSemana()
        {
            return Repositiorio.GetCumpleanioSemana();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getCumpleanioMes(int? mes, int? cluster)
        {
            return Repositiorio.GetCumpleanioMes(mes, cluster);
        }
        

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getUsuariosVotantesByInfoID(int InfoID)
        {
            return Repositiorio.getUsuariosVotantesByInfoID(InfoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.EfemeridesDataTable getEfemeridesHoy(int clusterid)
        {
            return Repositiorio.GetEfemeridesHoy(clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.EfemeridesDataTable getEfemeridesMes(int clusterid)
        {
            return Repositiorio.GetEfemeridesMes(clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.SantosDataTable getSantosHoy()
        {
            return Repositiorio.GetSantosHoy();
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.SantosDataTable getSantosMes()
        {
            return Repositiorio.GetSantosMes();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.frmTramiteDevDataTable frmTramiteDev(int usralta)
        {
            return Repositiorio.frmTramiteDev(usralta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.frmTramiteDevDataTable frmMisTramiteDev(int usralta)
        {
            return Repositiorio.frmMisTramiteDev(usralta);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.frmTramiteDevDataTable frmTramitesAsignadosDev(int usrActual)
        {
            return Repositiorio.frmTramitesAsignadosDev(usrActual);
        }



        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getDataUsuario(int usuarioid)
        {
            return Repositiorio.getDataUsuario(usuarioid);
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable GetDataUsuarioUbicacion(int usuarioid)
        {
            return Repositiorio.GetDataUsuarioUbicacion(usuarioid);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable getDataUsuarioRandom()
        {
            return Repositiorio.getDataUsuarioRandom();
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.infComedorDataTable getComedorByID(int id)
        {
            return Repositiorio.getComedorByID(id);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.infComedorDataTable getComedorHoy(int ubicacionid)
        {
            return Repositiorio.getComedorHoy(ubicacionid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.infComedorDataTable getComedorSemana(int dia,int clusterid)
        {
            return Repositiorio.getComedorSemana(dia,clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.infComedorDataTable getComedorBuscador(string ubicacionid, DateTime? fecha)
        {
            return Repositiorio.GetComedorBuscador(ubicacionid, fecha);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UbicacionDataTable getUbicacion(int clusterid)
        {
            return Repositiorio.GetUbicacion(clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UbicacionDataTable getUbicacionTodas(int clusterid)
        {
            return Repositiorio.GetUbicacionTodas(clusterid);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UbicacionDataTable getUbicacionByID(string ubicacionid)
        {
            return Repositiorio.GetUbicacionByID(ubicacionid);
        }

        public void InsertInfoComedorUsuarioVotacion(int usuarioid, int id, int voto)
        {
            DsUCHome.InfoComedorUsuarioVotacionRow info = UCHomeDS.InfoComedorUsuarioVotacion.NewInfoComedorUsuarioVotacionRow();            
            info.UsuarioID = usuarioid;
            info.InfoComedorID = id;
            info.FHAlta = DateTime.Now;
            info.voto = voto;
            UCHomeDS.InfoComedorUsuarioVotacion.AddInfoComedorUsuarioVotacionRow(info);
            Repositiorio.AdapterDSUCHome.InfoComedorUsuarioVotacion.Update(UCHomeDS.InfoComedorUsuarioVotacion);
        }

        public void InsertInfoComedorComentarios(int usuarioid, int id, string comentario)
        {
            DsUCHome.InfoComedorUsuarioComentariosRow info = UCHomeDS.InfoComedorUsuarioComentarios.NewInfoComedorUsuarioComentariosRow();
            info.usuarioID = usuarioid;
            info.InfoComedorID = id;
            info.comentarios = comentario;
            info.Fecha = DateTime.Now;           
            UCHomeDS.InfoComedorUsuarioComentarios.AddInfoComedorUsuarioComentariosRow(info);
            Repositiorio.AdapterDSUCHome.InfoComedorUsuarioComentarios.Update(UCHomeDS.InfoComedorUsuarioComentarios);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public int? getVotos(int id, int voto)
        {
            return Repositiorio.getVotos(id, voto);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public int? getCantComments(int id)
        {
            return Repositiorio.getCantCommets(id);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.InfoComedorUsuarioComentariosDataTable getComments(int id)
        {
            Repositiorio.AdapterDSUCHome.InfoComedorUsuarioComentarios.FillBy(UCHomeDS.InfoComedorUsuarioComentarios, id);
            return UCHomeDS.InfoComedorUsuarioComentarios;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public bool UsuarioVotacion(int usuarioid, int id)
        {
            return Repositiorio.UsuarioComedorVotoNoticia(usuarioid,id);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable GetBusquedaUsuario(string busqueda)
        {
            return Repositiorio.getBusquedaUsuario(busqueda);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable GetBusquedaUsuarioUbicacion(string busqueda, string ubicacion)
        {
            return Repositiorio.getBusquedaUsuarioUbicacion(busqueda,ubicacion);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DsUCHome.UsuarioDataTable GetBusquedaUsuarios(string busqueda,int? clusterid,string ubicacionid)
        {
            return Repositiorio.getBusquedaUsuarios(busqueda,clusterid,ubicacionid);
        }
    }
}
