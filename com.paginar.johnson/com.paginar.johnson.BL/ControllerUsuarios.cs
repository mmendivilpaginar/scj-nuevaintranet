﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Collections;
using System.IO;
using System.Web;
using System.Configuration;

namespace com.paginar.johnson.BL
{
     [DataObject]
    public class ControllerUsuarios
    {
        private DSUsuarios UsuariosDS = new DSUsuarios();
        private RepositoryUsuarios _Repositorio = new RepositoryUsuarios();
        public RepositoryUsuarios Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryUsuarios();
                return _Repositorio;
            }
        }        

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuarioDataTable GetByBusqueda(string Apellido, int? GrupoID)
        {
           return  Repositorio.AdapterDSUsuarios.Usuario.GetUsuariosByBusqueda(Apellido.Trim(), GrupoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuarioConCompensatoriosDataTable GetDataByUsuariosConCompensatoriosPorArea(string Area)
        {
            return Repositorio.AdapterDSUsuarios.UsuariosCompensatorio.GetDataByUsuariosConCompensatoriosPorArea(Area);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuarioConCompensatoriosDataTable GetCompensatorios()
        {
            return Repositorio.AdapterDSUsuarios.UsuariosCompensatorio.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuariosActivosDataTable GetUsuariosActivosByBusqueda(string Apellido, int? GrupoID)
        {
            return Repositorio.AdapterDSUsuarios.UsuariosActivos.GetUsuariosActivosByBusqueda(Apellido, GrupoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]  
        public DSUsuarios.UsuarioDataTable GetUsuarioByUsuarioID(int UsuarioID)
        {            
            Repositorio.AdapterDSUsuarios.Usuario.FillByUsuarioID(UsuariosDS.Usuario, UsuarioID);
            Repositorio.AdapterDSUsuarios.Login.FillByLoginID(UsuariosDS.Login, UsuarioID);
            Repositorio.AdapterDSUsuarios.frmRelUsuarioFormulario.FillByUsuarioID(UsuariosDS.frmRelUsuarioFormulario, UsuarioID);
            Repositorio.AdapterDSUsuarios.RelLoginGrupo.FillByUsuarioID(UsuariosDS.RelLoginGrupo, UsuarioID);
            Repositorio.AdapterDSUsuarios.RelUsuarioAccesoCluster.FillByUsuarioID(UsuariosDS.RelUsuarioAccesoCluster, UsuarioID);
            return UsuariosDS.Usuario;
        }

        public string DameEscalaSalarial(int UsuarioID)
        { 
           int CodEscalaSalarial;
            string EscalaSalarial= string.Empty;
            DSUsuarios.UsuarioDataTable DTU = new DSUsuarios.UsuarioDataTable();
            DSUsuarios.EscalaSalarialDataTable DTES = new DSUsuarios.EscalaSalarialDataTable();
            DTU = this.GetUsuarioByUsuarioID(UsuarioID);

            if (DTU.Rows.Count > 0)
            {
                int.TryParse(DTU.Rows[0]["EscalaSalarial"].ToString(), out CodEscalaSalarial);
                DTES = Repositorio.AdapterDSUsuarios.EscalaSalarial.GetDataByCodigoID(CodEscalaSalarial);
                if (DTES.Rows.Count > 0)
                    EscalaSalarial = DTES[0]["Codigo"].ToString();

            }
            return EscalaSalarial;
        }

        public string DameTipoEvaluado(int? Legajo, int? UsuarioID)
        {            
            return  Repositorio.AdapterDSUsuarios.Usuario.DameTipoEvaluado(Legajo, UsuarioID);             
        
        }

        public bool PuedeVerReporteSeguimiento(int UsuarioID)
        {
            int CodEscalaSalarial;
            string EscalaSalarial= string.Empty;
            DSUsuarios.UsuarioDataTable DTU = new DSUsuarios.UsuarioDataTable();
            DSUsuarios.EscalaSalarialDataTable DTES = new DSUsuarios.EscalaSalarialDataTable();
            DTU = this.GetUsuarioByUsuarioID(UsuarioID);

            if (DTU.Rows.Count > 0)
            {
                int.TryParse(DTU.Rows[0]["EscalaSalarial"].ToString(), out CodEscalaSalarial);
                //CodEscalaSalarial= int.Parse(DTU.Rows[0]["EscalaSalarial"].ToString());
                DTES = Repositorio.AdapterDSUsuarios.EscalaSalarial.GetDataByCodigoID(CodEscalaSalarial);
                if (DTES.Rows.Count > 0)
                    EscalaSalarial = DTES[0]["Codigo"].ToString();

                if (EscalaSalarial == "Q" || EscalaSalarial == "N" || EscalaSalarial == "O" || EscalaSalarial == "Om" || EscalaSalarial == "I" || EscalaSalarial == "J" || EscalaSalarial == "K"
                    || EscalaSalarial == "L" || EscalaSalarial == "M")

                    return true;
                else
                    return false;


            
            }
            return false;
        }
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]  
        public DSUsuarios.LoginDataTable GetDataByUsuarioChekIdentif(string UserLogin)
        {
            Repositorio.AdapterDSUsuarios.Login.GetDataByUsuarioChekIdentif(UserLogin);
            
           
            return UsuariosDS.Login;
        }
        
        public void UsuarioClear()
        {
            UsuariosDS.Usuario.Clear();
            UsuariosDS.Login.Clear();
            UsuariosDS.frmRelUsuarioFormulario.Clear();
            UsuariosDS.RelLoginGrupo.Clear();
            UsuariosDS.RelUsuarioAccesoCluster.Clear();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.UsuarioDataTable GetUsuario()
        {
            return UsuariosDS.Usuario;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.RelLoginGrupoDataTable GetGrupos()
        {
            return UsuariosDS.RelLoginGrupo;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.frmRelUsuarioFormularioDataTable GetFormularios()
        {
            return UsuariosDS.frmRelUsuarioFormulario;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.RelUsuarioAccesoClusterDataTable GetClustersAcceso()
        {
            return UsuariosDS.RelUsuarioAccesoCluster;
        }

        public void AddCluster(int UsuarioID,int ClusterID, bool Agrega)
        {
            DSUsuarios.RelUsuarioAccesoClusterRow RelUsuarioAccesoClusterR = UsuariosDS.RelUsuarioAccesoCluster.FindByUsuarioIDClusterID(UsuarioID, ClusterID);
            if (Agrega)
            {
                
                if (RelUsuarioAccesoClusterR == null)
                {
                    RelUsuarioAccesoClusterR = UsuariosDS.RelUsuarioAccesoCluster.NewRelUsuarioAccesoClusterRow();
                    RelUsuarioAccesoClusterR.UsuarioID = UsuarioID;
                    RelUsuarioAccesoClusterR.ClusterID = ClusterID;
                    RelUsuarioAccesoClusterR.Defecto = true;
                    UsuariosDS.RelUsuarioAccesoCluster.AddRelUsuarioAccesoClusterRow(RelUsuarioAccesoClusterR);
                }
            }
            else
            {                
                if (RelUsuarioAccesoClusterR != null)
                {
                    RelUsuarioAccesoClusterR.Delete();
                }
            }

        }

        public void AddFormularios(int UsuarioID, int FormularioID, bool Agrega)
        {
            DSUsuarios.frmRelUsuarioFormularioRow frmRelUsuarioFormularioR = UsuariosDS.frmRelUsuarioFormulario.FindByUsuarioIDFormularioID(UsuarioID, FormularioID);
            if (Agrega)
            {
                if (frmRelUsuarioFormularioR == null)
                {
                    UsuariosDS.frmRelUsuarioFormulario.AddfrmRelUsuarioFormularioRow(UsuarioID, FormularioID, string.Empty, string.Empty);
                }

            }
            else
            {
                if (frmRelUsuarioFormularioR != null)
                {
                    frmRelUsuarioFormularioR.Delete();
                }
            }
        }


        public void AddGrupos(int UsuarioID, int GrupoID, bool Agrega)
        {
            DSUsuarios.RelLoginGrupoRow RelLoginGrupoR = UsuariosDS.RelLoginGrupo.FindByGrupoNroLoginID(GrupoID, UsuarioID);
            if (Agrega)
            {
                if (RelLoginGrupoR == null)
                {
                    RelLoginGrupoR = UsuariosDS.RelLoginGrupo.NewRelLoginGrupoRow();
                    RelLoginGrupoR.GrupoNro=GrupoID;
                    RelLoginGrupoR.LoginID=UsuarioID;
                    RelLoginGrupoR.FHAlta = DateTime.Now;
                    UsuariosDS.RelLoginGrupo.AddRelLoginGrupoRow(RelLoginGrupoR);
                }

            }
            else
            {
                if (RelLoginGrupoR != null)
                {
                    RelLoginGrupoR.Delete();
                }
            }
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.UsuarioDataTable GetUsuarios()
        {
            return  Repositorio.AdapterDSUsuarios.Usuario.GetData();            
        }
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSUsuarios.UsuarioDataTable GetUsuariosByUbicacion(string ubicacion)
        {
            return Repositorio.AdapterDSUsuarios.Usuario.GetUsuariosByUbicacion(ubicacion);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public void UpdateUsuario(int UsuarioID, int LoginID, string Apellido, string Nombre, string Email, int Estado, DateTime FHNacimiento, int Legajo, string Interno,string Internoip, string UbicacionID, int SectorID, int EsPayRoll, int Cluster, int EscalaSalarial, string UsuarioFoto, int AreaID, int DireccionID, int CargoID, int ACargoDe, int EstaEnQeQ, DateTime FHMod, int UsrMod, string Identif, string Clave,int LegajoWD)
        {
            DSUsuarios.UsuarioRow user = UsuariosDS.Usuario.FindByUsuarioID(UsuarioID);
            user.Apellido = Apellido;
            user.Nombre = Nombre;
            user.Email = Email;
            user.Estado = Estado;
            user.FHNacimiento = FHNacimiento;
            //user.legajo; // user.Legajo 
            user.LegajoWD = LegajoWD;
            user.Interno = Interno;
            user.Internoip = Internoip;
            user.UbicacionID = UbicacionID;
            user.SectorID = SectorID;
            user.EsPayRoll = EsPayRoll;
            user.Cluster = Cluster;
            user.EscalaSalarial = EscalaSalarial;
            user.UsuarioFoto = UsuarioFoto;
            user.AreaID = AreaID;
            user.CargoID = CargoID;
            user.DireccionID = DireccionID;
            user.ACargoDe = ACargoDe;
            user.EstaEnQeQ = EstaEnQeQ;
            user.FHMod = FHMod;
            user.UsrMod = UsrMod;
            Repositorio.AdapterDSUsuarios.Usuario.Update(UsuariosDS.Usuario);
            // usuarioID y login ID siempre son iguales!!!!
            DSUsuarios.LoginRow login = UsuariosDS.Login.FindByLoginID(UsuarioID);
            login.Identif = Identif;
            login.Clave = Clave;
            
            Repositorio.AdapterDSUsuarios.Login.Update(UsuariosDS.Login);
            Repositorio.AdapterDSUsuarios.RelUsuarioAccesoCluster.Update(UsuariosDS.RelUsuarioAccesoCluster);
            Repositorio.AdapterDSUsuarios.RelLoginGrupo.Update(UsuariosDS.RelLoginGrupo);
            Repositorio.AdapterDSUsuarios.frmRelUsuarioFormulario.Update(UsuariosDS.frmRelUsuarioFormulario);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public void InsertUsuario(string Apellido, string Nombre, string Email, int Estado, DateTime FHNacimiento, int Legajo, string Interno,string Internoip, string UbicacionID, int SectorID, int EsPayRoll, int Cluster, int EscalaSalarial, string UsuarioFoto, int AreaID, int DireccionID, int CargoID, int? ACargoDe, int EstaEnQeQ, DateTime FHAlta, int UsrAlta, string Identif, string Clave,int LegajoWD)
        {
            DSUsuarios.UsuarioRow user = UsuariosDS.Usuario.NewUsuarioRow();
            user.Apellido = Apellido;
            user.Nombre = Nombre;
            user.Email = Email;
            user.Estado = Estado;
            user.FHNacimiento = FHNacimiento;
            user.Legajo= LegajoWD; //user.Legajo
            user.LegajoWD = LegajoWD;
            user.Interno = Interno;
            user.Internoip = Internoip;
            user.UbicacionID = UbicacionID;
            user.SectorID = SectorID;
            user.EsPayRoll = EsPayRoll;
            user.Cluster = Cluster;
            user.EscalaSalarial = EscalaSalarial;
            user.UsuarioFoto = UsuarioFoto;
            user.AreaID = AreaID;
            user.CargoID = CargoID;
            user.DireccionID = DireccionID;
            if (ACargoDe.HasValue)
            {
                user.ACargoDe = ACargoDe.Value;
            }
            user.EstaEnQeQ = EstaEnQeQ;
            user.FHAlta = FHAlta;
            user.UsrAlta = UsrAlta;
            user.LoginID = -1;
            user.ApruebaForms = 1;
            UsuariosDS.Usuario.AddUsuarioRow(user);
            Repositorio.AdapterDSUsuarios.Usuario.Update(UsuariosDS.Usuario);
            // usuarioID y login ID siempre son iguales!!!!
            DSUsuarios.LoginRow login = UsuariosDS.Login.NewLoginRow();
            login.LoginID = user.UsuarioID;
            login.Identif = Identif;
            login.Clave = Clave;            
            login.Intentos = 0;
            login.FHAlta = DateTime.Now;
            login.UsrAlta = UsrAlta;
            UsuariosDS.Login.AddLoginRow(login);
            Repositorio.AdapterDSUsuarios.Login.Update(UsuariosDS.Login);
            user.LoginID = user.UsuarioID;
            Repositorio.AdapterDSUsuarios.Usuario.Update(UsuariosDS.Usuario);



            foreach (DSUsuarios.RelUsuarioAccesoClusterRow item in UsuariosDS.RelUsuarioAccesoCluster.Rows)
            {
                if (item.UsuarioID == -1)
                {
                    item.UsuarioID = user.UsuarioID;
                }
            }

            foreach (DSUsuarios.frmRelUsuarioFormularioRow item in UsuariosDS.frmRelUsuarioFormulario.Rows)
            {
                if (item.UsuarioID == -1)
                {
                    item.UsuarioID = user.UsuarioID;
                }
            }

            foreach (DSUsuarios.RelLoginGrupoRow item in UsuariosDS.RelLoginGrupo.Rows)
            {
                if (item.LoginID == -1)
                {
                    item.LoginID = user.UsuarioID;
                }
            }
            
            Repositorio.AdapterDSUsuarios.RelUsuarioAccesoCluster.Update(UsuariosDS.RelUsuarioAccesoCluster);
            Repositorio.AdapterDSUsuarios.RelLoginGrupo.Update(UsuariosDS.RelLoginGrupo);
            Repositorio.AdapterDSUsuarios.frmRelUsuarioFormulario.Update(UsuariosDS.frmRelUsuarioFormulario);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]  
        public DSUsuarios.UsuarioDataTable GetDependientesByUsuarioID(int UsuarioID)
        {
            Repositorio.AdapterDSUsuarios.Usuario.GetDependientesByUsuarioID(UsuarioID);
            return UsuariosDS.Usuario;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuarioDataTable GetUsuariosByLegajo(int Legajo)
        {

           return Repositorio.AdapterDSUsuarios.Usuario.GetUsuariosByLegajo(Legajo);           
            //return UsuariosDS.Usuario;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.UsuarioDataTable GetUsuariosByLegajoyCluster(int Legajo, int Cluster)
        {

            return Repositorio.AdapterDSUsuarios.Usuario.GetUsuarioByLegajoyClust(Legajo, Cluster);
            //return UsuariosDS.Usuario;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.PerfilDataTable getPerfilById(int usrId)
        {
            return Repositorio.AdapterDSUsuarios.Perfil.getPerfilByID(usrId);
        }
        
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public DSUsuarios.HijosDataTableDataTable getHijosByUsuarioID(int usuarioID)
        {
            return Repositorio.AdapterDSUsuarios.Hijos.GetDataHijosUsuarioID(usuarioID);
        }
         
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public DSUsuarios.UsuarioDataTable GetUsuariosByUsuarioID(int UsuarioID)
         {
             return Repositorio.AdapterDSUsuarios.Usuario.GetDataByUsuarioID(UsuarioID);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public DSUsuarios.CargoDataTable getCargosByCargoID(int cargoId)
         {
             return Repositorio.AdapterDSUsuarios.Cargos.GetCargoByCargoID(cargoId);
         }


         public DSUsuarios.SectorDataTable getSector(int sectorid)
         {
             return Repositorio.AdapterDSUsuarios.Sector.GetDataById(sectorid);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public DSUsuarios.UsuarioDataTable getUsuariosmails(string search)
         {
             return Repositorio.AdapterDSUsuarios.Usuario.getUsermails(search);

         }
    }
}
