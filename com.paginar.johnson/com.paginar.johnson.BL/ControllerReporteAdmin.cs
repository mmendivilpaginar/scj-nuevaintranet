﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerReporteAdmin
    {
        DSReporteAdmin DSReporte = new DSReporteAdmin();

        private RepositoryReporteAdmin _Repositorio;
        public RepositoryReporteAdmin Repositorio
        {
            get 
            {
                if (_Repositorio == null) _Repositorio = new RepositoryReporteAdmin();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.CalificacionesPMPDataTable GetCalificacionesLegajoPeriodoID(int Legajo, int PeriodoID, string Cluster, int? TipoFormularioID)
        {

            return Repositorio.GetCalificacionesLegajoPeriodoID(Legajo, PeriodoID, Cluster, TipoFormularioID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.UsuarioDataTable GetDataUsuarios()
        {
            return Repositorio.GetDataUsuarios();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.CalificacionesPMPDataTable GetSummaryLegajoPeriodoID(int Legajo, int PeriodoID)
        {
            return Repositorio.GetSummaryLegajoPeriodoID(Legajo, PeriodoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.PlanAccionDataTable GetPlanByLegajoPeriodo(int? Legajo, int PeriodoID, string Cluster, int? TipoFormularioID)
        {
            return Repositorio.GetPlanByLegajoPeriodo(Legajo, PeriodoID, Cluster, TipoFormularioID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.CarrerPlanDataTable GetCareerPlan(int? Legajo, int PeriodoID, string Cluster, int? TipoFormualrioId)
        {
            return Repositorio.GetCareerPlan(Legajo, PeriodoID, Cluster, TipoFormualrioId);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.CursosDataTable GetCursosByLegajoPeriodoIdDireccionID(int? Legajo, int PeriodoId, int? DireccionID, string Cluster, int? TipoFormularioID)
        {
            return Repositorio.GetCursosByLegajoPeriodoIdDireccionID(Legajo, PeriodoId, DireccionID, Cluster, TipoFormularioID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSReporteAdmin.CompetenciasComoFortalezasDataTable GetCompetenciaComoFortaleza(int PeriodoID, int? Legajo, int FortalezaID, int DireccionID, string Cluster, int? TipoFormularioID)
        {
            return Repositorio.GetCompetenciasComoFortalezas(PeriodoID, Legajo, FortalezaID, DireccionID, Cluster, TipoFormularioID);
        }

    }
}
