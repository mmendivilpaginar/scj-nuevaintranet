/*
 * Copyright (c) 2004, Developer Fusion Ltd
 * Author: James Crowley
 * Home Page: http://www.developerfusion.com/
 */
using System;
using System.Collections;

namespace dfTreeDemo.Tree
{
	/// <summary>
	/// Represents a node in the tree
	/// </summary>
[Serializable]
public class TreeNode
{
	private int _uniqueID;
	private string _name;
	private int _parentID;
	private int _depth;
	private ArrayList _children;

	public TreeNode() { }

	public TreeNode(string name, int parentID) : this(0,name,parentID,-1)
	{
	}
	public TreeNode(int uniqueID, string name, int parentID, int depth)
	{
		_uniqueID = uniqueID;
		_name = name;
		_parentID = parentID;
		_depth = depth;
	}

	/// <summary>
	/// Gets or sets the unique ID associated with this category
	/// </summary>
	/// <remarks>Once a non-zero ID has been set, it may not be modified.</remarks>
	public int UniqueID
	{
		get { return _uniqueID; }
		set 
		{ 
			if (_uniqueID == 0)
				_uniqueID = value;
			else
				throw new Exception("The UniqueID property cannot be modified once it has a non-zero value");
		}
	}

	public int Depth
	{
		get { return _depth; }
	}

	/// <summary>
	/// Gets or sets the label for this node
	/// </summary>
	public string Name 
	{
		get { return _name; }
		set { _name = value; }
	}

	/// <summary>
	/// The ID of the parent node
	/// </summary>
	public int ParentID
	{
		get { return _parentID; }
		set { _parentID = value; }
	}

	/// <summary>
	/// Gets the children TreeNode objects for this category
	/// </summary>
	/// <remarks>In .NET 2.0, this can be modified to use generics, and have type ArrayList&lt;TreeNode></remarks>
	public ArrayList Children 
	{
		get { return _children; }
		set { _children = value; }
	}
} 
}
