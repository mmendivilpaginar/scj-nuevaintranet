using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.HierarchicalDataSource
{
   /// <summary>
   /// Summary description for FileSystemNodeCollection
   /// </summary>
   public class RelationalSystemNodeCollection : List<RelationalSystemNode>, IHierarchicalEnumerable
   {
      #region IHierarchicalEnumerable Members
      public RelationalSystemNode GetHierarchyData( object enumeratedItem )
      {
         if ( !( enumeratedItem is RelationalSystemNode ) )
         {
            throw new InvalidOperationException( "Unexpected object of type " + enumeratedItem.GetType().FullName );
         }
         return enumeratedItem as RelationalSystemNode;
      }

      IHierarchyData IHierarchicalEnumerable.GetHierarchyData( object enumeratedItem )
      {
         if ( !(enumeratedItem is IHierarchyData) )
         {
            throw new InvalidOperationException( "Unexpected object of type " + enumeratedItem.GetType().FullName ); 
         }
         return enumeratedItem as IHierarchyData;
      }

      #endregion
   }
}
