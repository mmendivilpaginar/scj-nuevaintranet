﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections;
namespace com.paginar.johnson.BL
{
    public class JS
    {

        public JS()
        { }

        public static void ShowAlert(string message, ControlCollection Controls)
        {
            string str = "<script language='JavaScript'>alert('" + message + "');</script>";
            Controls.Add(new LiteralControl(str));
        }

    }
}