using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace com.paginar.HierarchicalDataSource
{
   public class RelationalSystemDataSourceView : HierarchicalDataSourceView
   {
      public RelationalSystemDataSourceView( IHierarchicalEnumerable nodes )
      {
         this.nodes = nodes;
      }

      /// <summary>
      /// Recurse into hierarchy for each call.
      /// </summary>
      /// <returns></returns>
      public override IHierarchicalEnumerable Select()
      {
         return nodes;
      }

      IHierarchicalEnumerable nodes = null;
   }
}