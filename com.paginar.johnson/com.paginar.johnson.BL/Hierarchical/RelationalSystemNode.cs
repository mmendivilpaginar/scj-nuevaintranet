using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Text;
using System.ComponentModel;
using com.paginar.johnson.DAL;
using System.Collections;


namespace com.paginar.HierarchicalDataSource
{
   /// <summary>
   /// A node in the virtual Relational system. Can be a folder or a Relational.
   /// </summary>
   /// <remarks>
   /// Physical paths can be disjointed from logical paths and need not be within the sites virtual folder.
   /// Folders may contain Relationals and folders.
   /// Relationals should not have children, though it's entirely possible.
   /// </remarks>
   public class RelationalSystemNode : IHierarchyData
   {
       public RelationalSystemNode(DsContenido.Content_ItemsRow info)
      {
         this.info = info;
      }

      /// <summary>
      /// The name of the node.
      /// </summary>
      public string Name
      {
         get
         {
            return info.Titulo.Trim();
         }
      }

      /// <summary>
      /// Used by HierarchicalDataSources to navigate the node structure.
      /// </summary>
      public string DataPath
      {
         get 
         {
            // Tip: If your using a database, the datapath could simply be the database 
            // ID of the record
            //return info.FullName;
             return info.Content_ItemId.ToString();
         }
      }

      
      /// <summary>
      /// Gets or sets the parent node.
      /// </summary>
      /// <remarks>
      /// Root nodes have no parent (Parent is null).
      /// </remarks>
      public RelationalSystemNode Parent
      {
         get
         {
            if ( null == parentNode )
            {
               //if ( info is FileInfo )
               //{
               //   parentNode = FindByDataPath( ( info as FileInfo ).DirectoryName );
               //}
                parentNode = FindByDataPath(info.parentId.ToString());
            }
            return parentNode;
         }
      }

      public static RelationalSystemNode FindByDataPath( string datapath )
      {
         //Tip: Retrieve a node from cache or database here
         RelationalSystemNode node = null;
         //HierarchicalSchemaTableAdapters.CategoryTableAdapter CategoryAdapter;

         int Id=0;
        
         //if (HierarchicalDs == null)
         //{
         //    HierarchicalDs=new HierarchicalSchema();
         //    CategoryAdapter=new HierarchicalSchemaTableAdapters.CategoryTableAdapter();
         //    CategoryAdapter.Fill(HierarchicalDs.Category);
         //}
         RepositoryContenido RC = new RepositoryContenido();
         //RC.fillData(ref HierarchicalDs);
         RC.AdaptersDSContenido.ItemContenido.FillByEstructura(HierarchicalDs.Content_Items, int.Parse(datapath));
         if (datapath=="")
         {
             //CategoryAdapter = new HierarchicalSchemaTableAdapters.CategoryTableAdapter();
             //CategoryAdapter.Fill(HierarchicalDs.Category);
             
             
             //HierarchicalDs.Content_Items = RC.GetData();
             foreach (DsContenido.Content_ItemsRow Row in HierarchicalDs.Content_Items)
             {
                 if (Row.IsparentIdNull()  )
                 {
                     Id = Row.Content_ItemId;
                     break;
                 }
             }
         }
         else
         {
             Id = Convert.ToInt32(datapath);
         }
 
         

         //if (  File.Exists( datapath ) )
         if (HierarchicalDs.Content_Items.FindByContent_ItemId(Id) !=null )
         {
             node = new RelationalSystemNode(HierarchicalDs.Content_Items.FindByContent_ItemId(Id));
         }
         else
         {
            throw new ApplicationException( string.Format( "The path '{0}' does not exist", datapath ) );
         }

         return node;
      }

      public RelationalSystemNodeCollection Children
      {
         get
         {
            //HierarchicalSchema.CategoryRow[] ChildRows;
            DsContenido.Content_ItemsRow[] ChildRows;
            if (( null == children ))
            {
//               ChildRows=info.GetChildRows(
                //try
                //{
               //  ChildRows = info.GetContent_ItemsRows();
                 //DsContenido.Content_ItemsRow[] ChildRows01= this.GetChildrensItems(info.Content_ItemId);
                 ChildRows = this.GetChildrensItems(info.Content_ItemId);

               // Boolean val01= this.IsEqualsArrary_tmp(ChildRows, ChildRows01);
                
                if (ChildRows.Length > 0)
                    {
                        children = new RelationalSystemNodeCollection();
                        //foreach (FileSystemInfo child in (info as DirectoryInfo).GetFileSystemInfos())
                        foreach (DsContenido.Content_ItemsRow Row in ChildRows)
                        {
                            children.Add(new RelationalSystemNode(Row));
                        }
                    }
                    else
                    {
                        children = new RelationalSystemNodeCollection(); // empty collection; no children
                    }
                //}
                //catch (RowNotInTableException e)
                //{

                    
                //}
                //finally {
                //    children = new RelationalSystemNodeCollection();
                //}
                
            }
            return children;
         }
      }

      private bool IsEqualsArrary_tmp(DsContenido.Content_ItemsRow[] ChildRows, DsContenido.Content_ItemsRow[] ChildRows01)
      {
          Boolean res = true;
          int countEquals = ChildRows.Length;
          int numeroHallado = 0;
          if (ChildRows.Length == ChildRows01.Length)
          {
              foreach (var item in ChildRows)
              {
                  foreach (var item01 in ChildRows01)
                  {
                      if (item.Content_ItemId == item01.Content_ItemId)
                      {
                          numeroHallado++;
                          break;
                      }
                  }
              }

              res = numeroHallado == countEquals;
          }
          else
              res = false;

          return res;
      }

      private DsContenido.Content_ItemsRow[] GetChildrensItems(int id)
      {
          AdapterDSContenido tb = new AdapterDSContenido();
          DsContenido.Content_ItemsDataTable table = tb.ItemContenido.GetDataByChildren(id);
          
          List<DsContenido.Content_ItemsRow> lst = new List<DsContenido.Content_ItemsRow>();


          if (id == 44 || id == 45 || id == 42 || id == 47)
          {

              if (id == 42)
              {
                  var table1 = from r in table orderby r.orden.ToString(), r.Titulo select r;
                  foreach (var item in table1)
                  {
                      lst.Add(item);
                  }
              }
              else
              {
                  var table1 = from r in table orderby r.Titulo select r;
                  foreach (var item in table1)
                  {
                      lst.Add(item);
                  } 
              }


          }

          else
          {
              foreach (var item in table)
              {
                  lst.Add(item);

              }

          }
             

          return lst.ToArray();
      }

      public bool HasChildren
      {
         get
         {
            return Children.Count > 0;
         }
      }

      #region IHierarchyData Members

      IHierarchicalEnumerable IHierarchyData.GetChildren()
      {
         return Children;
      }

      IHierarchyData IHierarchyData.GetParent()
      {
         return Parent;
      }

      object IHierarchyData.Item
      {
         get { return this; }
      }

      string IHierarchyData.Path
      {
         get { return DataPath; }
      }

      string IHierarchyData.Type
      {
         get
         {
            //// You can use the Type to declaritively select the icon to use for
            //// an item in a tree view.
            //if ( info is FileInfo )
            //{
            //   return ( info as FileInfo ).Extension;
            //}
            //else
            //{
               return "\\";
           //}
         }
      }
      #endregion

      // Cache
      RelationalSystemNodeCollection children;
      RelationalSystemNode parentNode;    
      DsContenido.Content_ItemsRow info;      
     static  DsContenido _HierarchicalDs = new DsContenido();
     static Object myLock = new Object();
     static DsContenido HierarchicalDs
     {
         get
         {
             lock (myLock)
             {
                 return _HierarchicalDs;
             }
         }
     }

     

      //DsContenido HierarchicalDs = new DsContenido();
      
      
      
   }
}
