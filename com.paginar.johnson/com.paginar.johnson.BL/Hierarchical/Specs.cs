﻿using System;

public class Specs
{
    private string _code;
    private string _name;
    private string _nameTechnique;
    private decimal _price;
    
	public Specs(string code, string name, string nameTechnique, decimal price)
	{
        _nameTechnique = nameTechnique;
        _name = name;
        _code = code;
        _price = price;
	}

    public string Code
    {
        get { return _code; }
        set { _code = value; }
    }

    public string NameTechnique
    {
        get { return _nameTechnique; }
        set { _nameTechnique = value; }
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public decimal Price
    {
        get { return _price; }
        set { _price = value; }
    }

}
