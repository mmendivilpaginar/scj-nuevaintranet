using System;
using System.Collections;
using com.paginar.johnson.DAL;

namespace dfTreeDemo.Tree
{
	/// <summary>
	/// Provides an implementation of the TreeProvider for SQL Server
	/// </summary>
	public class SqlServerTreeProvider /* : TreeProvider */
	{
        RepositoryContenido RC = new RepositoryContenido();
        //private CatalogueSchemaTableAdapters.CategoryTableAdapter CTa;
        //private CatalogueSchemaTableAdapters.ManufacturerTableAdapter MTa;

        public SqlServerTreeProvider()
        {
        //    CTa = new CatalogueSchemaTableAdapters.CategoryTableAdapter();
        //    MTa = new CatalogueSchemaTableAdapters.ManufacturerTableAdapter();
        }

		public ArrayList GetTreeList(int catId, int Content_TipoID)
		{
			return ProcessList(catId);
		}

        protected ArrayList ProcessList(int IdItem)
        {
            ArrayList nodes = new ArrayList();

            DsContenido.Content_ItemsDataTable CDt = new DsContenido.Content_ItemsDataTable();
//            CatalogueSchema.CategoryDataTable CDt = new CatalogueSchema.CategoryDataTable();
            CDt= RC.GetEstructuraById(IdItem);
            
            

            nodes.Add(TreeNodeFromDataReader(-1,"Home"));

            
            foreach (DsContenido.Content_ItemsRow Cr in CDt.Rows)
                nodes.Add(TreeNodeFromDataReader(Cr));

            return nodes;
        }

        protected virtual TreeNode TreeNodeFromDataReader(DsContenido.Content_ItemsRow Ir)
		{
			TreeNode node = new TreeNode(
                Ir.Content_ItemId,
                Ir.Titulo,
                Ir.IsparentIdNull() ? 0 : Ir.parentId,
                Ir.depth);

			return node;
		}

        protected virtual TreeNode TreeNodeFromDataReader(int id, string name)
        {
            TreeNode node = new TreeNode(
                id,
                name,
                -1,
                -1);

            return node;
        }

	}
}
