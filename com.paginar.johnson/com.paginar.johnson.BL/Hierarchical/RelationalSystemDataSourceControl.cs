using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Security.Permissions;

namespace com.paginar.HierarchicalDataSource
{
   /// <summary>
   /// An Hierarchical datasource for a virtual file system represented by a <see cref="FileSystemNode"/> structure.
   /// </summary>
   [AspNetHostingPermission( SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal ),
   ToolboxData("<{0}:RelationalSystemDataSource runat=\"server\" />")]
   public class RelationalSystemDataSource : HierarchicalDataSourceControl, IHierarchicalDataSource
   {
      /// <summary>
      /// Create a new <see cref="RelationalSystemDataSource"/>.
      /// </summary>
      public RelationalSystemDataSource() : base() 
      { 
      }

      /// <summary>
      /// Called to retrieve hierarchical data.
      /// </summary>
      /// <remarks>
      /// This method is called repeatedly when databinding to controls
      /// like the TreeView. Each node returned from this method will
      /// cause a new call to get it's children. 
      /// </remarks>
      /// <param name="viewPath">Empty string to get the top level of the hierarchy or
      /// a DataPath to get all child nodes of a node in the hierarchy.</param>
      /// <returns></returns>
      protected override HierarchicalDataSourceView GetHierarchicalView( string viewPath )
      {
         if ( string.IsNullOrEmpty( Path ) )
         {
            throw new ArgumentException( "No Path specified." );
         }

         return GetTreeView(Path);
      }

      private RelationalSystemDataSourceView GetTreeView( string viewPath )
      {
         RelationalSystemNodeCollection nodes = new RelationalSystemNodeCollection();
         
         
         //if ( string.IsNullOrEmpty( viewPath ) )
         //{
            
             RelationalSystemNode root = RelationalSystemNode.FindByDataPath(Path );
            
            if ( IncludeRoot )
            {
               nodes.Add( root );
            }
            else
            {
               nodes = root.Children;
            }
         //}
         //else
         //{
         //   RelationalSystemNode node = RelationalSystemNode.FindByDataPath( viewPath );
         //   if ( null != node )
         //   {
         //      nodes = node.Children;
         //   }

         //   if (IncludeRoot)
         //   {
         //       nodes.Add(node);
         //   }
         //   else
         //   {
         //       nodes = node.Children;
         //   }
         //}
         return new RelationalSystemDataSourceView( nodes );
      }

      // The RelationalSystemDataSource can be used declaratively. To enable
      // declarative use, override the default implementation of
      // CreateControlCollection to return a ControlCollection that
      // you can add to.
      protected override ControlCollection CreateControlCollection()
      {
         return new ControlCollection( this );
      }

      /// <summary>
      /// The virtual path to the root node.
      /// </summary>
      [Category( "Appearance" ),
          DefaultValue( "" ),
          Bindable( true )]
      public string Path
      {
         get
         {
            object viewStateObj = ViewState[ "Path" ];
            if ( viewStateObj != null && viewStateObj is string )
            {
               return ( string )viewStateObj;
            }
            return "";
         }
         set { ViewState[ "Path" ] = value; }
      }
	
      /// <summary>
      /// If true the root node is included in the returned data.
      /// </summary>
      [DefaultValue(true)]
      public bool IncludeRoot
      {
         get
         {
            object includeRoot = ViewState[ "includeRoot" ];
            if ( includeRoot != null && includeRoot is bool)
            {
               return ( bool )includeRoot;
            }
            return true;
         }
         set
         {
            ViewState[ "includeRoot" ] = value;
         }
      }
   }
}