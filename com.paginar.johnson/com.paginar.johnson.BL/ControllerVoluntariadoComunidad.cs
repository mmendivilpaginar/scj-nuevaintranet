﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.paginar.johnson.DAL;
using System.ComponentModel;
using System.Data;

namespace com.paginar.johnson.BL
{
    [DataObject]
    public class ControllerVoluntariadoComunidad
    {
        private RepositoryDSComunidad _Repositorio;
        public RepositoryDSComunidad Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryDSComunidad();
                return _Repositorio;
            }
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSComunidad.com_sumarcomunidadDataTable getActividadesByMes(string pMes,int tipo)
        {
            return Repositorio.com_selectVoluntariadosByMes(pMes,tipo);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DSComunidad.com_sumarcomunidadDataTable getActividadesByID(int id)
        {
            return Repositorio.com_selectVoluntariadosById(id);
        }


    }
}
