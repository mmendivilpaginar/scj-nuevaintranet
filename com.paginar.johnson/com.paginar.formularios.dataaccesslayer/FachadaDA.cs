﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.paginar.formularios.dataaccesslayer
{
    public class FachadaDA
    {
        private static FachadaDA _Singleton;
        public static FachadaDA Singleton
        {
            get
            {
                //if (_Singleton == null)
                //{
                    _Singleton = new FachadaDA();
                //}
                return _Singleton;
            }

        }
        FormulariosDSTableAdapters.CabeceraFormularioTableAdapter _CabeceraFormulario;
        public FormulariosDSTableAdapters.CabeceraFormularioTableAdapter CabeceraFormulario
        {
            get
            {
                if (_CabeceraFormulario == null)
                {
                    _CabeceraFormulario = new FormulariosDSTableAdapters.CabeceraFormularioTableAdapter();
                }
                return _CabeceraFormulario;
            }
        }
        FormulariosDSTableAdapters.ItemEvaluacionTableAdapter _ItemEvaluacion;
        public FormulariosDSTableAdapters.ItemEvaluacionTableAdapter ItemEvaluacion
        {
            get
            {
                if (_ItemEvaluacion == null)
                {
                    _ItemEvaluacion = new FormulariosDSTableAdapters.ItemEvaluacionTableAdapter();
                }
                return _ItemEvaluacion;
            }
        }

        FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter _AreasDeOperaciones;
        public FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter AreasDeOperaciones
        {
            get
            {
                if (_AreasDeOperaciones == null)
                {
                    _AreasDeOperaciones = new FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter();
                }
                return _AreasDeOperaciones;
            }
        }

        FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter _FormularioPmpOperarios;
        public FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter FormularioPmpOperarios
        {
            get
            {
                if (_FormularioPmpOperarios == null)
                {
                    _FormularioPmpOperarios = new FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter();
                }
                return _FormularioPmpOperarios;
            }
        }

        FormulariosDSTableAdapters.RelFormularioPmpOperariosItemsEvaluacionTableAdapter _RelFormularioPmpOperariosItemsEvaluacion;
        public FormulariosDSTableAdapters.RelFormularioPmpOperariosItemsEvaluacionTableAdapter RelFormularioPmpOperariosItemsEvaluacion
        {
            get
            {
                if (_RelFormularioPmpOperariosItemsEvaluacion == null)
                {
                    _RelFormularioPmpOperariosItemsEvaluacion = new FormulariosDSTableAdapters.RelFormularioPmpOperariosItemsEvaluacionTableAdapter();
                }
                return _RelFormularioPmpOperariosItemsEvaluacion;
            }
        }

        FormulariosDSTableAdapters.RelFormularioPmpOperariosAreasDeOperacionesTableAdapter _RelFormularioPmpOperariosAreasDeOperaciones;
        public FormulariosDSTableAdapters.RelFormularioPmpOperariosAreasDeOperacionesTableAdapter RelFormularioPmpOperariosAreasDeOperaciones
        {
            get
            {
                if (_RelFormularioPmpOperariosAreasDeOperaciones == null)
                {
                    _RelFormularioPmpOperariosAreasDeOperaciones = new FormulariosDSTableAdapters.RelFormularioPmpOperariosAreasDeOperacionesTableAdapter();
                }
                return _RelFormularioPmpOperariosAreasDeOperaciones;
            }
        }

        FormulariosDSTableAdapters.PeriodoTableAdapter _Periodo;
        public FormulariosDSTableAdapters.PeriodoTableAdapter Periodo
        {
            get
            {
                if (_Periodo == null)
                {
                    _Periodo = new FormulariosDSTableAdapters.PeriodoTableAdapter();
                }
                return _Periodo;
            }
        }
        FormulariosDSTableAdapters.HistorialTableAdapter _Historial;
        public FormulariosDSTableAdapters.HistorialTableAdapter Historial
        {
            get
            {
                if (_Historial == null)
                {
                    _Historial = new FormulariosDSTableAdapters.HistorialTableAdapter();
                }
                return _Historial;
            }
        }

        FormulariosDSTableAdapters.vwUsuariosTableAdapter _vwUsuarios;
        public FormulariosDSTableAdapters.vwUsuariosTableAdapter vwUsuarios
        {
            get
            {
                if (_vwUsuarios == null)
                {
                    _vwUsuarios = new FormulariosDSTableAdapters.vwUsuariosTableAdapter();
                }
                return _vwUsuarios;
            }
        }

        FormulariosDSTableAdapters.RelEvaluadosEvaluadoresTableAdapter _RelEvaluadosEvaluadores;
        public FormulariosDSTableAdapters.RelEvaluadosEvaluadoresTableAdapter RelEvaluadosEvaluadores
        {
            get
            {
                if (_RelEvaluadosEvaluadores == null)
                {
                    _RelEvaluadosEvaluadores = new FormulariosDSTableAdapters.RelEvaluadosEvaluadoresTableAdapter();
                }
                return _RelEvaluadosEvaluadores;
            }
        }

        FormulariosDSTableAdapters.PasosTableAdapter _Pasos;
        public FormulariosDSTableAdapters.PasosTableAdapter Pasos
        {
            get
            {
                if (_Pasos == null)
                {
                    _Pasos = new FormulariosDSTableAdapters.PasosTableAdapter();
                }
                return _Pasos;
            }
        }

        FormulariosDSTableAdapters.EvaluadosTableAdapter _Evaluados;
        public FormulariosDSTableAdapters.EvaluadosTableAdapter Evaluados
        {
            get
            {
                if (_Evaluados == null)
                {
                    _Evaluados = new FormulariosDSTableAdapters.EvaluadosTableAdapter();
                }
                return _Evaluados;
            }
        }

        FormulariosDSTableAdapters.TipoFormularioTableAdapter _TipoFormulario;
        public FormulariosDSTableAdapters.TipoFormularioTableAdapter TipoFormulario
        {
            get
            {
                if (_TipoFormulario == null)
                {
                    _TipoFormulario = new FormulariosDSTableAdapters.TipoFormularioTableAdapter();
                }
                return _TipoFormulario;
            }
        }

        FormulariosDSTableAdapters.UsuariosModuloImportacionTableAdapter _UsuariosModuloImportacion;
        public FormulariosDSTableAdapters.UsuariosModuloImportacionTableAdapter UsuariosModuloImportacion
        {
            get
            {
                if (_UsuariosModuloImportacion == null)
                {
                    _UsuariosModuloImportacion = new FormulariosDSTableAdapters.UsuariosModuloImportacionTableAdapter();
                }
                return _UsuariosModuloImportacion;
            }
        }

        FormulariosDSTableAdapters.AreasTableAdapter _Areas;
        public FormulariosDSTableAdapters.AreasTableAdapter Areas
        {
            get
            {
                if (_Areas == null)
                {
                    _Areas = new FormulariosDSTableAdapters.AreasTableAdapter();
                }
                return _Areas;
            }
        }

        FormulariosDSTableAdapters.CargoTableAdapter _Cargo;
        public FormulariosDSTableAdapters.CargoTableAdapter Cargo
        {
            get
            {
                if (_Cargo == null)
                {
                    _Cargo = new FormulariosDSTableAdapters.CargoTableAdapter();
                }
                return _Cargo;
            }
        }


        FormulariosDSTableAdapters.eval_PeriodoTableAdapter _eval_PeriodoTableAdapter;
        public FormulariosDSTableAdapters.eval_PeriodoTableAdapter eval_PeriodoTableAdapter
        {
            get
            {
                if (_eval_PeriodoTableAdapter == null)
                {
                    _eval_PeriodoTableAdapter = new FormulariosDSTableAdapters.eval_PeriodoTableAdapter();
                }
                return _eval_PeriodoTableAdapter;
            }
        }


        FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter _Eval_evaluadores;
        public FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter Eval_evaluadores
        {
            get
            {
                if (_Eval_evaluadores == null)
                {
                    _Eval_evaluadores = new FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter();
                }
                return _Eval_evaluadores;
            }
        }

        FormulariosDSTableAdapters.RelPasosTipoFormulariosTableAdapter _RelPasosTipoFormularios;
        public FormulariosDSTableAdapters.RelPasosTipoFormulariosTableAdapter RelPasosTipoFormularios
        {
            get
            {
                if (_RelPasosTipoFormularios == null)
                {
                    _RelPasosTipoFormularios = new FormulariosDSTableAdapters.RelPasosTipoFormulariosTableAdapter();
                }
                return _RelPasosTipoFormularios;
            }
        }

        FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteGeneralTableAdapter _GetReporteGeneral;
        public FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteGeneralTableAdapter GetReporteGeneral
        {
            get
            {
                if (_GetReporteGeneral == null)
                {
                    _GetReporteGeneral = new FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteGeneralTableAdapter();
                }
                return _GetReporteGeneral;
            }
        }


        FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorCalificacionesTableAdapter _GetReportePorCalificaciones;
        public FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorCalificacionesTableAdapter GetReportePorCalificaciones
        {
            get
            {
                if (_GetReportePorCalificaciones == null)
                {
                    _GetReportePorCalificaciones = new FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorCalificacionesTableAdapter();
                }
                return _GetReportePorCalificaciones;
            }
        }

        FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorComentariosTableAdapter _GetReportePorComentarios;
        public FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorComentariosTableAdapter GetReportePorComentarios
        {
            get
            {
                if (_GetReportePorComentarios == null)
                {
                    _GetReportePorComentarios = new FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorComentariosTableAdapter();
                }
                return _GetReportePorComentarios;
            }
        }

        FormulariosReporteDSTableAdapters.GetReporteSegumientoEstadoIDTableAdapter  _GetReporteSegumientoEstadoID;
        public FormulariosReporteDSTableAdapters.GetReporteSegumientoEstadoIDTableAdapter GetReporteSegumientoEstadoID
        {
            get
            {
                if (_GetReporteSegumientoEstadoID == null)
                {
                    _GetReporteSegumientoEstadoID = new FormulariosReporteDSTableAdapters.GetReporteSegumientoEstadoIDTableAdapter();
                }
                return _GetReporteSegumientoEstadoID;
            }
        }

        FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionTableAdapter _GetReporteParaCalibracion;
        public FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionTableAdapter GetReporteParaCalibracion
        {
            get
            {
                if (_GetReporteParaCalibracion == null)
                {
                    _GetReporteParaCalibracion = new FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionTableAdapter();
                }
                return _GetReporteParaCalibracion;
            }
        }

        FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionResumTableAdapter _GetReporteParaCalibracionResum;
        public FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionResumTableAdapter GetReporteParaCalibracionResum
        {
            get
            {
                if (_GetReporteParaCalibracionResum == null)
                {
                    _GetReporteParaCalibracionResum = new FormulariosReporteGeneralDSTableAdapters.eval_GetReporteParaCalibracionResumTableAdapter();
                }
                return _GetReporteParaCalibracionResum;
            }
        }

        FormulariosReporteDSTableAdapters.Eval_GetReporteSeguimientoByEvaluadorAreaTableAdapter _GetReporteSeguimientoByEvaluadorArea;
        public FormulariosReporteDSTableAdapters.Eval_GetReporteSeguimientoByEvaluadorAreaTableAdapter GetReporteSeguimientoByEvaluadorArea
        {
            get {

                if (_GetReporteSeguimientoByEvaluadorArea == null)
                {
                    _GetReporteSeguimientoByEvaluadorArea = new FormulariosReporteDSTableAdapters.Eval_GetReporteSeguimientoByEvaluadorAreaTableAdapter();
                }
                return _GetReporteSeguimientoByEvaluadorArea;
            }
        }

        FormularioLogExportDSTableAdapters.Eval_log_DownloadDevTableAdapter _log_DownloadDevTableAdapter;
        public FormularioLogExportDSTableAdapters.Eval_log_DownloadDevTableAdapter log_DownloadDevTableAdapter
        {
            get {
                if (_log_DownloadDevTableAdapter == null)
                {
                    _log_DownloadDevTableAdapter = new FormularioLogExportDSTableAdapters.Eval_log_DownloadDevTableAdapter();
                }
                return _log_DownloadDevTableAdapter;
            }
        }

        FormulariosDSTableAdapters.UbicacionEvaluadoTableAdapter _UbicacionEvaluadoTableAdapter;
        public FormulariosDSTableAdapters.UbicacionEvaluadoTableAdapter UbicacionEvaluadoTableAdapter
        {
            get 
            {
                if (_UbicacionEvaluadoTableAdapter == null)
                {
                    _UbicacionEvaluadoTableAdapter = new FormulariosDSTableAdapters.UbicacionEvaluadoTableAdapter();
                }
                return _UbicacionEvaluadoTableAdapter;
            }
        }


        FormulariosDSTableAdapters.CursosTableAdapter _CursosTableAdapter;
        public FormulariosDSTableAdapters.CursosTableAdapter CursosTableAdapter
        {
            get
            {
                if (_CursosTableAdapter == null)
                {
                    _CursosTableAdapter = new FormulariosDSTableAdapters.CursosTableAdapter();
                }
                return _CursosTableAdapter;
            }
        }


        FormulariosDSTableAdapters.eval_RelFormularioPmpOperariosCursosTableAdapter _eval_RelFormularioPmpOperariosCursosTableAdapter;
        public FormulariosDSTableAdapters.eval_RelFormularioPmpOperariosCursosTableAdapter eval_RelFormularioPmpOperariosCursosTableAdapter
        {
            get
            {
                if (_eval_RelFormularioPmpOperariosCursosTableAdapter == null)
                {
                    _eval_RelFormularioPmpOperariosCursosTableAdapter = new FormulariosDSTableAdapters.eval_RelFormularioPmpOperariosCursosTableAdapter();
                }
                return _eval_RelFormularioPmpOperariosCursosTableAdapter;
            }
        }


        FormulariosPMPFYDSTableAdapters.frmPMPCalificacionTableAdapter _frmPMPCalificacionTableAdapter;
        public FormulariosPMPFYDSTableAdapters.frmPMPCalificacionTableAdapter frmPMPCalificacionTableAdapter
        {
            get
            {
                if (_frmPMPCalificacionTableAdapter == null)
                {
                    _frmPMPCalificacionTableAdapter = new FormulariosPMPFYDSTableAdapters.frmPMPCalificacionTableAdapter();
                }
                return _frmPMPCalificacionTableAdapter;
            }
        
        }

        FormulariosPMPFYDSTableAdapters.frmPMPFortalezasTempTableAdapter _frmPMPFortalezasTempTableAdapter;
        public FormulariosPMPFYDSTableAdapters.frmPMPFortalezasTempTableAdapter frmPMPFortalezasTempTableAdapter
        {
            get
            {
                if (_frmPMPFortalezasTempTableAdapter == null)
                {
                    _frmPMPFortalezasTempTableAdapter = new FormulariosPMPFYDSTableAdapters.frmPMPFortalezasTempTableAdapter();
                }
                return _frmPMPFortalezasTempTableAdapter;
            }

        }

        FormulariosPMPFYDSTableAdapters.frmPMPAreasTableAdapter _frmPMPAreasTableAdapter;
        public FormulariosPMPFYDSTableAdapters.frmPMPAreasTableAdapter frmPMPAreasTableAdapter
        {
            get
            {
                if (_frmPMPAreasTableAdapter == null)
                {
                    _frmPMPAreasTableAdapter = new FormulariosPMPFYDSTableAdapters.frmPMPAreasTableAdapter();
                }
                return _frmPMPAreasTableAdapter;
            }

        }

        FormulariosPMPFYDSTableAdapters.frmPMPCareerTableAdapter _frmPMPCareerTableAdapter;
        public FormulariosPMPFYDSTableAdapters.frmPMPCareerTableAdapter frmPMPCareerTableAdapter
        {
            get
            {
                if (_frmPMPCareerTableAdapter == null)
                {
                    _frmPMPCareerTableAdapter = new FormulariosPMPFYDSTableAdapters.frmPMPCareerTableAdapter();
                }
                return _frmPMPCareerTableAdapter;
            }

        }


        FormulariosDSTableAdapters.fyCVExperienceTableAdapter  _fyCVExperienceTableAdapter;
        public FormulariosDSTableAdapters.fyCVExperienceTableAdapter fyCVExperienceTableAdapter
        {
            get
            {
                if (_fyCVExperienceTableAdapter == null)
                {
                    _fyCVExperienceTableAdapter = new FormulariosDSTableAdapters.fyCVExperienceTableAdapter();
                }
                return _fyCVExperienceTableAdapter;
            }

        }


        FormulariosDSTableAdapters.fyCVEducationTableAdapter _fyCVEducationTableAdapter;
        public FormulariosDSTableAdapters.fyCVEducationTableAdapter fyCVEducationTableAdapter
        {
            get
            {
                if (_fyCVEducationTableAdapter == null)
                {
                    _fyCVEducationTableAdapter = new FormulariosDSTableAdapters.fyCVEducationTableAdapter();
                }
                return _fyCVEducationTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVEducationGetByTramiteIDTableAdapter _fyCVEducationGetByTramiteIDTableAdapter;
        public FormulariosDSTableAdapters.fyCVEducationGetByTramiteIDTableAdapter fyCVEducationGetByTramiteIDTableAdapter
        {
            get
            {
                if (_fyCVEducationGetByTramiteIDTableAdapter == null)
                {
                    _fyCVEducationGetByTramiteIDTableAdapter = new FormulariosDSTableAdapters.fyCVEducationGetByTramiteIDTableAdapter();
                }
                return _fyCVEducationGetByTramiteIDTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVExperienceGetByTramiteIDTableAdapter _fyCVExperienceGetByTramiteIDTableAdapter;
        public FormulariosDSTableAdapters.fyCVExperienceGetByTramiteIDTableAdapter fyCVExperienceGetByTramiteIDTableAdapter
        {
            get
            {
                if (_fyCVExperienceGetByTramiteIDTableAdapter == null)
                {
                    _fyCVExperienceGetByTramiteIDTableAdapter = new FormulariosDSTableAdapters.fyCVExperienceGetByTramiteIDTableAdapter();
                }
                return _fyCVExperienceGetByTramiteIDTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVHeaderTableAdapter _fyCVHeaderTableAdapter;
        public FormulariosDSTableAdapters.fyCVHeaderTableAdapter fyCVHeaderTableAdapter
        {
            get
            {
                if (_fyCVHeaderTableAdapter == null)
                {
                    _fyCVHeaderTableAdapter = new FormulariosDSTableAdapters.fyCVHeaderTableAdapter();
                }
                return _fyCVHeaderTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVLanguageTableAdapter _fyCVLanguageTableAdapter;
        public FormulariosDSTableAdapters.fyCVLanguageTableAdapter fyCVLanguageTableAdapter
        {
            get
            {
                if (_fyCVLanguageTableAdapter == null)
                {
                    _fyCVLanguageTableAdapter = new FormulariosDSTableAdapters.fyCVLanguageTableAdapter();
                }
                return _fyCVLanguageTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVLanguageGetByTramiteIDTableAdapter _fyCVLanguageGetByTramiteIDTableAdapter;
        public FormulariosDSTableAdapters.fyCVLanguageGetByTramiteIDTableAdapter fyCVLanguageGetByTramiteIDTableAdapter
        {
            get
            {
                if (_fyCVLanguageGetByTramiteIDTableAdapter == null)
                {
                    _fyCVLanguageGetByTramiteIDTableAdapter = new FormulariosDSTableAdapters.fyCVLanguageGetByTramiteIDTableAdapter();
                }
                return _fyCVLanguageGetByTramiteIDTableAdapter;


            }
        }

        FormulariosDSTableAdapters.fyCVTrackingTableAdapter _fyCVTrackingTableAdapter;
        public FormulariosDSTableAdapters.fyCVTrackingTableAdapter fyCVTrackingTableAdapter
        {
            get
            {
                if (_fyCVTrackingTableAdapter == null)
                {
                    _fyCVTrackingTableAdapter = new FormulariosDSTableAdapters.fyCVTrackingTableAdapter();
                }
                return _fyCVTrackingTableAdapter;


            }
        }


        FormulariosPMPFYDSTableAdapters.frmPMPCursosTableAdapter _frmPMPCursosTableAdapter;
        public FormulariosPMPFYDSTableAdapters.frmPMPCursosTableAdapter frmPMPCursosTableAdapter
        {
            get
            {
                if (_frmPMPCursosTableAdapter == null)
                {
                    _frmPMPCursosTableAdapter = new FormulariosPMPFYDSTableAdapters.frmPMPCursosTableAdapter();
                }
                return _frmPMPCursosTableAdapter;


            }
        }


        FormulariosPMPFYDSTableAdapters.LastRatingsTableAdapter _LastRatingsTableAdapter;
        public FormulariosPMPFYDSTableAdapters.LastRatingsTableAdapter LastRatingsTableAdapter
        {
            get
            {
                if (_LastRatingsTableAdapter == null)
                {
                    _LastRatingsTableAdapter = new FormulariosPMPFYDSTableAdapters.LastRatingsTableAdapter();
                }
                return _LastRatingsTableAdapter;


            }
        }


        
    }
}
