﻿using System.Configuration;

namespace com.paginar.formularios.dataaccesslayer.Properties {
    
    
    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    internal sealed partial class Settings {
        
        public Settings() {
            // // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
            this.SettingsLoaded += new System.Configuration.SettingsLoadedEventHandler(Settings_SettingsLoaded);                                         
        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Add code to handle the SettingChangingEvent event here.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Add code to handle the SettingsSaving event here.
        }

        void Settings_SettingsLoaded(object sender, System.Configuration.SettingsLoadedEventArgs e)
        {
            //throw new System.Exception("The method or operation is not implemented.");
            string cadenaconexion = string.Empty;
            switch (System.Web.HttpContext.Current.Request.Url.Host)
            {
                case "hrdev.scj.com":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringDev"];
                    break;
                case "190.220.133.194":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringQA"];
                    break;
                case "hr.scj.com":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringPro"];
                    break;
                case "intranetconosur.scj.com":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringPro"];
                    break;
                case "localhost":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringLocal"];
                    break;
                case "24935-scj-nuevaintranet.desa-scj.paginar.org":
                case "24935-scj-nuevaintranet.desa3.paginar.org":
                case "24935-scj-nuevaintranet-qa.aprodez0.paginar.org":
                case "qa.arsanida000.paginar.org":
                    cadenaconexion = ConfigurationManager.AppSettings["connectionStringLocal"];
                    break;
                case "190.221.11.154":
                case "24935-scj-nuevaintranet-qa.desa3.paginar.org":
                case "24935-scj-nuevaintranet-qa.desa-scj.paginar.org":
                    cadenaconexion = ConfigurationSettings.AppSettings["connectionStringQA_PNET"];
                    break;                     
                default:
                    cadenaconexion = ConfigurationManager.AppSettings["FormulariosconnectionStringDev"];
                    break;
            }            
            try
            {
                this["FormulariosConnectionString"] = cadenaconexion;
            }
            catch (System.Exception err)
            {

            }
            //                this["HR_SCJConoSurConnectionString"] = ConfigurationManager.ConnectionStrings["IFLOW_Release"].ConnectionString;

        }
    }
}
