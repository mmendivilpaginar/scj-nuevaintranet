﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web
{
    public partial class ASPPages : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = "SessionTransfer.aspx?dir=2asp&url=" + Server.UrlEncode(Request.QueryString["url"]);
            IAsp.Attributes.Add("src", url);
        }
    }
}