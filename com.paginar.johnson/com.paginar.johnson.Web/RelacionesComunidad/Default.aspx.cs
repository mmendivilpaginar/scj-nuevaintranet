﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.RelacionesComunidad
{
    public partial class Default1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                ControllerVoluntariadoComunidad cv = new ControllerVoluntariadoComunidad();

              //  getActGrupales(cv);
              //  getActIndividuales(cv);
             }

             

        }

        private void getActGrupales(ControllerVoluntariadoComunidad cv)
        {
          //  rptGrpABR.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("ABRIL", 1);
          //  rptGrpABR.DataBind();


          //  rptGrpMAY.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("MAYO", 1);
          //  rptGrpMAY.DataBind();

          //  rptGrpJUN.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("JUNIO", 1);
          //  rptGrpJUN.DataBind();


          //  rptGrpJUL.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("JULIO", 1);
          //  rptGrpJUL.DataBind();


          //  rptGrpAGO.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("AGOSTO", 1);
          //  rptGrpAGO.DataBind();

          // // rptGrpSEP.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("SEPTIEMBRE", 1);
          // // rptGrpSEP.DataBind();

          //  rptGrpOCT.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("OCTUBRE", 1);
          //  rptGrpOCT.DataBind();


          //  rptGrpNOV.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("NOVIEMBRE", 1);
          //  rptGrpNOV.DataBind();

          ////  rptGrpDIC.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("DICIEMBRE", 1);
          ////  rptGrpDIC.DataBind();
        }


        private void getActIndividuales(ControllerVoluntariadoComunidad cv)
        {
           // rptIndABR.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("ABRIL", 2);
           // rptIndABR.DataBind();


            //rptIndMAY.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("MAYO", 2);
            //rptIndMAY.DataBind();

            //rptIndJUN.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("JUNIO", 2);
            //rptIndJUN.DataBind();


            ////rptIndJUL.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("JULIO", 2);
            ////rptIndJUL.DataBind();


            //rptIndAGO.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("AGOSTO", 2);
            //rptIndAGO.DataBind();

            //rptIndSEP.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("SEPTIEMBRE", 2);
            //rptIndSEP.DataBind();

            //rptIndOCT.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("OCTUBRE", 2);
            //rptIndOCT.DataBind();


            //rptIndNOV.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("NOVIEMBRE", 2);
            //rptIndNOV.DataBind();

            //rptIndDIC.DataSource = cv.Repositorio.com_selectVoluntariadosByMes("DICIEMBRE", 2);
            //rptIndDIC.DataBind();
        }

        protected string getinformation(object Id, object img)
        {
            return string.Format("<a href='javascript:getinformation({0});' onclick='modal()'><img src='{1}' width='100px' height='{2}'></a>", Id.ToString(), img.ToString(), getHeight(img.ToString()));
        }


        public string RutaImagen(object Imagen)
        {
            string[] i = Imagen.ToString().Split(',');

            if (i != null)
                return "/RelacionesComunidad/Logos/" + i[0].ToString();
            else
                return "/RelacionesComunidad/Logos/noimage.jpg";
        }

        public string RutaImagenes(object Imagen)
        {
            string imgs="";
            string[] i = Imagen.ToString().Split(',');

            if (i.Count() > 1)
            {
               imgs += "<table> <tr> <td>";
                imgs += " <img src='/RelacionesComunidad/Logos/" + i[0].ToString()+"' />";
                imgs += "</td>";
                imgs += "<td>" + " &nbsp &nbsp + &nbsp &nbsp " + "</td>";
                 imgs += "<td>";
                imgs += " <img src='/RelacionesComunidad/Logos/" + i[1].ToString() + "' />";
                imgs += "</td>";
                 imgs += "</tr>  </table>";
            }
            else
            {
                if (i[0] != null)
                    imgs = "<img src='/RelacionesComunidad/Logos/" + i[0].ToString() + "' />";
                else
                    imgs = "<img src='/RelacionesComunidad/Logos/noimage.jpg' />";
            }

            return imgs;
        }

        public string getWeb(object web)
        {
            string link = "";
            string[] l = web.ToString().Split(',');
             if (l.Count() > 1)
             {
                 link += "<strong>Web: </strong>  &nbsp <a href='" + l[0].ToString() + "'>" + l[0].ToString() + "</a>";
              //   link += "<br> <br>";
                 link += " &nbsp <a href='" + l[1].ToString() + "'>" + l[1].ToString() + "</a>";
             }
             else
             {
                link = "<strong>Web: </strong> &nbsp <a href='"+web.ToString()+"'>"+web.ToString()+"</a>";
             }


             return link;
        }

        protected string getHeight(string imgPath)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imgPath));
            int ActualWidth = image.Width;
            int ActualHeight = image.Height;
            image.Dispose();
            string altoImagen = string.Empty;
            return altoImagen = Math.Round(double.Parse(((ActualHeight * 100) / ActualWidth).ToString()), 0).ToString();

        }
    }
}