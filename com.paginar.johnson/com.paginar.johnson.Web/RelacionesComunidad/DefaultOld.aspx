﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="DefaultOld.aspx.cs" Inherits="com.paginar.johnson.Web.RelacionesComunidad.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderhead" runat="server">
<script language="javascript" type="text/javascript">
    $overlay = $('<div id="contentx"></div>');
    function getinformation(identificador) {
        var btn = document.getElementById('<%= btnTrigger.ClientID %>');
        var id = parseInt(identificador);
        $("input[id*=hfAliadoSelect]").val(identificador);
        btn.click();
    }
    function pageScroll() {
        document.getElementById('detalle').focus();
    }
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
<div id="contentx" style="display:none"></div>
    <asp:HiddenField ID="hfAliadoSelect" runat="server" />
<h2>Relaciones con la Comunidad</h2>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel runat="server" HeaderText="MISIÓN" ID="TabPanel1">
        <ContentTemplate>
        <h3>Contribuir al desarrollo sustentable de nuestras comunidades, a través de iniciativas que promuevan su bienestar.</h3>
            <div align="center"><asp:Image ID="imgMision" runat="server" ImageUrl="~/RelacionesComunidad/Images/misionrelacioncomunidad.jpg" /></div>
        </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" HeaderText="ÁREA" ID="TabPanel2">
        <ContentTemplate>
            <div align="center"><asp:Image ID="Image1" runat="server" ImageUrl="~/RelacionesComunidad/Images/arearelacioncomunidad.jpg" /></div>
        </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" HeaderText="NUESTROS ALIADOS" ID="TabPanel3">
        <ContentTemplate>
            <div align="center"><asp:Image ID="Image2" runat="server" 
                    ImageUrl="~/RelacionesComunidad/Images/nuestrosaliadosrealcionescomunidad.jpg" 
                    Width="680px" /></div>
                    <div>
<!--Tipo 1-->
            <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ODSAliadosActxTipo">
                    <HeaderTemplate><div class="wrapper-grilla tipo1"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSAliadosActxTipo" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="com_aliadosSelectActivosByTipo" 
                TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                <SelectParameters>
                    <asp:Parameter DefaultValue="1" Name="tipo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<!--Tipo 2-->
            <asp:Repeater ID="Repeater2" runat="server" DataSourceID="ODSAliadosActxTipo2">
                    <HeaderTemplate><div class="wrapper-grilla tipo2"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSAliadosActxTipo2" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="com_aliadosSelectActivosByTipo" 
                TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                <SelectParameters>
                    <asp:Parameter DefaultValue="2" Name="tipo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<!--Tipo 3-->
            <asp:Repeater ID="Repeater3" runat="server" DataSourceID="ODSAliadosActxTipo3">
                    <HeaderTemplate><div class="wrapper-grilla tipo3"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSAliadosActxTipo3" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="com_aliadosSelectActivosByTipo" 
                TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                <SelectParameters>
                    <asp:Parameter DefaultValue="3" Name="tipo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<!--Tipo 4-->
            <asp:Repeater ID="Repeater4" runat="server" DataSourceID="ODSAliadosActxTipo4">
                    <HeaderTemplate><div class="wrapper-grilla tipo4"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSAliadosActxTipo4" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="com_aliadosSelectActivosByTipo" 
                TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                <SelectParameters>
                    <asp:Parameter DefaultValue="4" Name="tipo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<!--Tipo 5-->
            <asp:Repeater ID="Repeater5" runat="server" DataSourceID="ODSAliadosActxTipo5">
                    <HeaderTemplate><div class="wrapper-grilla tipo4"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSAliadosActxTipo5" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="com_aliadosSelectActivosByTipo" 
                TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                <SelectParameters>
                    <asp:Parameter DefaultValue="5" Name="tipo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

                <%--<asp:Repeater ID="rptAliadosActivos" runat="server" DataSourceID="ODSAliadosActivos">
                    <HeaderTemplate><div class="wrapper-grilla"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("idAliado"),RutaImagen(Eval("imgLogo"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
                </asp:Repeater>--%>

                <%--<asp:ObjectDataSource ID="ODSAliadosActivos" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="com_aliadoSelectActivos" 
                    TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                </asp:ObjectDataSource>--%>
                <asp:Button ID="btnTrigger" runat="server" style="display:none" Text="x" />
            
                <div class="pnet-modal-overlay"></div>
                <div class="pnet-wrapper-modal">
                    <a href="javascript:void(0)" class="pnet-modal-cerrar" onclick="cerrarModal()">Cerrar</a>

                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                        <asp:FormView ID="FormView1" runat="server" DataKeyNames="idAliado" 
                            DataSourceID="ODSDetalleAliado">
                            <EditItemTemplate>
                                idAliado:
                                <asp:Label ID="idAliadoLabel1" runat="server" Text='<%# Eval("idAliado") %>' />
                                <br />
                                Nombre:
                                <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
                                <br />
                                QueHace:
                                <asp:TextBox ID="QueHaceTextBox" runat="server" Text='<%# Bind("QueHace") %>' />
                                <br />
                                QueHara:
                                <asp:TextBox ID="QueHaraTextBox" runat="server" Text='<%# Bind("QueHara") %>' />
                                <br />
                                web:
                                <asp:TextBox ID="webTextBox" runat="server" Text='<%# Bind("web") %>' />
                                <br />
                                Activo:
                                <asp:CheckBox ID="ActivoCheckBox" runat="server" 
                                    Checked='<%# Bind("Activo") %>' />
                                <br />
                                imgLogo:
                                <asp:TextBox ID="imgLogoTextBox" runat="server" Text='<%# Bind("imgLogo") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                                    CommandName="Update" Text="Update" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                Nombre:
                                <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
                                <br />
                                QueHace:
                                <asp:TextBox ID="QueHaceTextBox" runat="server" Text='<%# Bind("QueHace") %>' />
                                <br />
                                QueHara:
                                <asp:TextBox ID="QueHaraTextBox" runat="server" Text='<%# Bind("QueHara") %>' />
                                <br />
                                web:
                                <asp:TextBox ID="webTextBox" runat="server" Text='<%# Bind("web") %>' />
                                <br />
                                Activo:
                                <asp:CheckBox ID="ActivoCheckBox" runat="server" 
                                    Checked='<%# Bind("Activo") %>' />
                                <br />
                                imgLogo:
                                <asp:TextBox ID="imgLogoTextBox" runat="server" Text='<%# Bind("imgLogo") %>' />
                                <br />
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Insert" />
                                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                            </InsertItemTemplate>
                            <ItemTemplate>
                            
                            <div class="pnet-inner-modal">
                                <div class="pic">
                                    <div class="item">
                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%# RutaImagen(Eval("imgLogo")) %>' Width="100px" />
                                    </div>
                                </div>
                                <div class="info">
                                    <div class="item">
                                        <strong>Nombre:</strong><br />
                                        <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
                                    </div>
                                    <div class="item">
                                        <strong>Que hace:</strong><br />
                                        <asp:Label ID="QueHaceLabel" runat="server" Text='<%# Bind("QueHace") %>' />
                                    </div>
                                    <div class="item">
                                        <strong>Que vamos a hacer:</strong><br />
                                        <asp:Label ID="QueHaraLabel" runat="server" Text='<%# Bind("QueHara") %>' />
                                    </div>
                                    <div class="item">
                                        <strong>web:</strong><br />
                                        <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("web") %>' Text='<%# Bind("web") %>'> </asp:HyperLink>
                                    </div>
                                </div>
                            </div>

                            </ItemTemplate>
                        </asp:FormView>
                        <asp:ObjectDataSource ID="ODSDetalleAliado" runat="server" 
                            OldValuesParameterFormatString="original_{0}" 
                            SelectMethod="com_aliadosSelectById" 
                            TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfAliadoSelect" Name="idAliado" 
                                    PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnTrigger" EventName="Click" />
                    </Triggers>
                    </asp:UpdatePanel>
                </div>

            </div>
        </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
    <div id="detalle"></div>
</asp:Content>
