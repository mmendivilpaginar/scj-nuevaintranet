﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.RelacionesComunidad
{
    public partial class Default : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Page.SetFocus(FormView1);
            if (!Page.IsPostBack) 
            {

                if (this.ObjectUsuario.clusteridactual == 1)
                {
                    TabPanel3.Visible = true;
                }
                else
                {
                    TabPanel3.Visible = false;
                }
            }
        }

        public string RutaImagen(object Imagen)
        {
            if (Imagen != null)
                return "/RelacionesComunidad/Logos/" + Imagen.ToString();
            else
                return "/RelacionesComunidad/Logos/noimage.jpg";
        }

        protected string getinformation(object Id)
        {
            return string.Format("<a href='javascript:getinformation({0});'>Ver más...</a>", Id.ToString());
        }

        protected string getinformationV2(object Id, object img)
        {
            return string.Format("<a href='javascript:getinformation({0});' onclick='modal()'><img src='{1}' width='100px' height='{2}'></a>", Id.ToString(), img.ToString(), getHeight(img.ToString()));            
        }

        protected string getHeight(string imgPath)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imgPath));
            int ActualWidth = image.Width;
            int ActualHeight = image.Height;
            image.Dispose();
            string altoImagen = string.Empty;
            return altoImagen = Math.Round(double.Parse(((ActualHeight * 100) / ActualWidth).ToString()), 0).ToString();
        
        }
    }

    
}