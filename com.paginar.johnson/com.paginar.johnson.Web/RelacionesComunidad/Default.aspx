﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="com.paginar.johnson.Web.RelacionesComunidad.Default1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" tagname="ucCategoriaNoticia" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderhead" runat="server">
    <style>
        .ajax__tab_panel{
            padding-top:25px;
        }
        .ajax__tab_body {
            padding-top:40px;
        }


        .ajax__tab_xp .ajax__tab_tab
        {
            height :100% !important;
          }

        .ajax__tab_tab
        {
            padding-top:0px !important;
        }

        .msg-mes {
	color: #d20000;
	background-color: #fdeadb;
    font-weight: bold;
    font-size:14px;
    text-align: center;
      }

        
        .msg-mes2 {
	color: #d20000;
	background-color: #fcd5b4;
    font-weight: bold;
    font-size:14px;
        text-align: center;
      }

        
    </style>

    <script language="javascript" type="text/javascript">
        $overlay = $('<div id="contentx"></div>');
        function getinformation(identificador) {
            var btn = document.getElementById('<%= btnTrigger.ClientID %>');
        var id = parseInt(identificador);
        $("input[id*=hfActividad]").val(identificador);
     
        btn.click();

        
    }
    function pageScroll() {
        document.getElementById('detalle').focus();
    }

    function Hide() {
    
        $('#imgReaload').fadeIn(2000);
        

    
    }


    $(document).ready(function () {
      //  $('#imgReaload').fadeIn(500);

    });
    
                              
                             
</script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <br /><br />
    <h2>Relaciones con la Comunidad</h2>
    <br />
     <asp:HiddenField ID="hfActividad" runat="server" />
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel runat="server" HeaderText="Sumar Comunidad" ID="TabPanel1">
                   <HeaderTemplate>                  
                       
                       Sumar</br>
                     
                        Comunidad
                          
                   </HeaderTemplate>
            <ContentTemplate>
                <div align="center">
                <img src="Images/sumarcomunidad2015.jpg" />
                    </div>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server"  ID="TabPanel2">
             <HeaderTemplate>                      
                  
                   Actividades
                       <br />
                        
                      Grupales
                   </HeaderTemplate>
               
            <ContentTemplate>

                <img src="Images/Actividadesgrupales.jpg" />
                <br /><br />

           <%--     <table>
                    <tr>
                        <td>
                            <div class="msg-mes">ABR</div>
                        </td>
                        <td>
                            <div class="msg-mes2">MAY</div>
                        </td>
                        <td>
                            <div class="msg-mes">JUN</div>
                        </td>
                        <td>
                            <div class="msg-mes2">JUL</div>
                        </td>
                    </tr>


                    <tr>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                    </tr>

                    <tr>
                        <td valign="TOP">
                            <asp:Repeater ID="rptGrpABR" runat="server">

                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>



                            </asp:Repeater>
                        </td>
                        <td valign="TOP">
                            <asp:Repeater ID="rptGrpMAY" runat="server">

                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate></ul> </FooterTemplate>


                            </asp:Repeater>

                        </td>
                        <td valign="TOP">
                            <asp:Repeater ID="rptGrpJUN" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>


                            </asp:Repeater>

                        </td>
                        <td>
                            <asp:Repeater ID="rptGrpJUL" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>


                            </asp:Repeater>

                        </td>
                    </tr>
                </table>--%>


      
                <br /><br /><br />
                
      <%--          <table>
                    <tr>
                        <td>
                            <div class="msg-mes">AGO</div>
                        </td>
                        <td>
                            <div class="msg-mes2">SEP</div>
                        </td>
                        <td>
                            <div class="msg-mes">OCT</div>
                        </td>
                        <td>
                            <div class="msg-mes2">NOV</div>
                        </td>
                          <td>
                            <div class="msg-mes">DIC</div>
                        </td>
                    </tr>


                    <tr>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td valign="TOP">
                            <asp:Repeater ID="rptGrpAGO" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>



                            </asp:Repeater>
                        </td>
                        <td valign="TOP">
                            <%--SEP--%>
                <%--           <div  style=" width: 115px;">
                        </td>
                        <td valign="TOP">
                        <asp:Repeater ID="rptGrpOCT" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>



                            </asp:Repeater>
                        </td>
                        <td valign="TOP">

                                <asp:Repeater ID="rptGrpNOV" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>


                            </asp:Repeater>
                        </td>

                          <td valign="TOP">
                              <%--DIC--%>
                        <%--   <div  style=" width: 115px;">
                        </td>
                    </tr>

                </table>--%>


              
            
               
               <%-- <br />
           
                <asp:Repeater ID="rptGrpSEP" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>--%>
            
              
               <%-- <br />
             
                <asp:Repeater ID="rptGrpDIC" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>--%>

            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel3">
               <HeaderTemplate>
                       Actividades  <br /> Individuales
                        
                   </HeaderTemplate>
            <ContentTemplate>

                   <img src="Images/Actividadesindividuales.jpg" />
                <br /><br />
  <%--              <table>
                    <tr>
                        <td>
                            <div class="msg-mes">ABR</div>
                        </td>
                        <td>
                            <div class="msg-mes2">MAY</div>
                        </td>
                        <td>
                            <div class="msg-mes">JUN</div>
                        </td>
                        <td>
                            <div class="msg-mes2">JUL</div>
                        </td>
                    </tr>


                    <tr>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                    </tr>

                    <tr>
                        <td valign="TOP">
                            <div style="width: 115px;">
                        </td>

                        <td valign="TOP">
                            <asp:Repeater ID="rptIndMAY" runat="server">

                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate></ul> </FooterTemplate>


                            </asp:Repeater>
                        </td>

                        <td valign="TOP">
                            <asp:Repeater ID="rptIndJUN" runat="server">


                                <HeaderTemplate>
                                    <ul class="pnet-action-list">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>


                            </asp:Repeater>
                        </td>

                        <td valign="TOP">
                            <div style="width: 115px;">
                        </td>
                    </tr>
                </table>--%>


         <%--                 <table>
                    <tr>
                        <td>
                            <div class="msg-mes">AGO</div>
                        </td>
                        <td>
                            <div class="msg-mes2">SEP</div>
                        </td>
                        <td>
                            <div class="msg-mes">OCT</div>
                        </td>
                        <td>
                            <div class="msg-mes2">NOV</div>
                        </td>
                          <td>
                            <div class="msg-mes">DIC</div>
                        </td>
                    </tr>


                    <tr>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                        <td>&nbsp</td>
                    </tr>
                    <tr>
                        <td valign="TOP">
                           <asp:Repeater ID="rptIndAGO" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>



                </asp:Repeater>
                        </td>
                        <td valign="TOP">
                            <asp:Repeater ID="rptIndSEP" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>
                        </td>
                        <td valign="TOP">
                           <asp:Repeater ID="rptIndOCT" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>



                </asp:Repeater>
                        </td>
                        <td valign="TOP">
                              <div style="width: 115px;">
                        </td>
                        <td valign="TOP">
                              <div style="width: 115px;">
                        </td>
                        </tr>
                              </table>--%>

           <%--                <br />
                <br />
                <div class="msg-mes">ABRIL</div>
                <br />
                <asp:Repeater ID="rptIndABR" runat="server">

                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>



                </asp:Repeater>--%>
              
             
           <%--     <br />
                <br />
                <div class="msg-mes2">JULIO</div>
                <br />
                <asp:Repeater ID="rptIndJUL" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>--%>
             
                
             <%--   <br />
                <br />
                <div class="msg-mes2">NOVIEMBRE</div>
                <br />
                <asp:Repeater ID="rptIndNOV" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>
                <br />
                <br />
                <div class="msg-mes">DICIEMBRE</div>
                <br />
                <asp:Repeater ID="rptIndDIC" runat="server">


                    <HeaderTemplate>
                        <ul class="pnet-action-list">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# getinformation(Eval("id"),RutaImagen(Eval("Logos"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></ul></FooterTemplate>


                </asp:Repeater>--%>

            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server"  ID="TabPanel4">
             <HeaderTemplate>
                      SCJ  <br />Recicla
                     
                   </HeaderTemplate>
            <ContentTemplate>
                <img src="Images/SCJRecicla2015.jpg" />
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server"  ID="TabPanel5">
            <HeaderTemplate>
                     Noticias
                     <br />
                &nbsp&nbsp&nbsp&nbsp
                   </HeaderTemplate>
            <ContentTemplate>
                <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="58" TagID="4" runat="server" />
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>


    <asp:Button ID="btnTrigger" runat="server" Style="display: none" Text="x"  OnClientClick="Hide();"/>
    <div class="pnet-modal-overlay"></div>
    <div class="pnet-wrapper-modal">
        <a href="javascript:void(0)" class="pnet-modal-cerrar" onclick="cerrarModal()">Cerrar</a>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:FormView ID="frmDetalle" runat="server"
                    DataSourceID="ODSDetalle">
                    <ItemTemplate>

                        <div class="pnet-inner-modal">
                       
                            <div class="info">
                                <div id="divReload"   align="center">
                                 <img id="imgReaload" src="../images/activity.gif" style="display:none;"  />
                                </div>
                                
                              

                                <div class="item">
                                         <div>
                                       <asp:Literal ID="ImgLogos" runat="server" Text='<%# RutaImagenes(Eval("Logos")) %>' />
                                             </div> 
                                 
                                    <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Info") %>' />
                                </div>

                                <table cellspacing="15">
                                    <tr>
                                        <td  colspan="3" >  
                              
                                              <asp:Literal ID="Literal2" runat="server" Text='<%# getWeb(Eval("web")) %>' />
                                        </td>
                                       
                                    </tr>
                                       <tr>
                                        <td colspan="3">   &nbsp   &nbsp  &nbsp </td>
                                     
                                       </td>
                                    </tr>
                                      <tr>
                                        <td colspan="3">  <img src="Images/linea.jpg" /> </td>
                                     
                                       </td>
                                    </tr>
                                    <tr>
                                        <td>   <img src="Images/que.jpg" alt="" /></td>
                                        <td>   &nbsp  &nbsp</td>
                                        <td>    <asp:Label ID="QueHaceLabel" runat="server" Text='<%# Bind("Que") %>' Font-Bold="true"  ForeColor="Gray" /></td>
                                    </tr>
                                       <tr>
                                        <td colspan="3">  <img src="Images/linea2.jpg" /> </td>
                                     
                                       </td>
                                    </tr>
                                    <tr>
                                        <td>   <img src="Images/cuando.jpg" alt="" /> </td>
                                            <td>  &nbsp   &nbsp  &nbsp</td>
                                        <td>    <asp:Label ID="QueHaraLabel" runat="server" Text='<%# Bind("Cuando") %>' Font-Bold="true" ForeColor="Gray"  /> </td>
                                    </tr>
                                       <tr>
                                        <td colspan="3">  <img src="Images/linea2.jpg" /> </td>
                                     
                                       </td>
                                    </tr>
                                    <tr>
                                        <td>   <img src="Images/cuanto.jpg" alt="" /></td>
                                            <td>   &nbsp  &nbsp  &nbsp</td> 
                                        <td>    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Cuanto") %>' Font-Bold="true" ForeColor="Gray" /> </td>
                                    </tr>
                                       <tr>
                                        <td colspan="3">  <img src="Images/linea2.jpg" /> </td>
                                     
                                       </td>
                                    </tr>
                                       <tr>
                                        <td>
                                              <img src="Images/quienes.jpg" alt="" />
                                        </td>
                                               <td>  &nbsp   &nbsp  &nbsp</td>
                                        <td>   <asp:Label ID="Label2" runat="server" Text='<%# Bind("Quienes") %>' Font-Bold="true"  ForeColor="Gray" /></td>
                                    </tr>
                                       <tr>
                                        <td colspan="3">  <img src="Images/linea2.jpg" /> </td>
                                     
                                       </td>
                                    </tr>

                                     </tr>
                                       <tr>
                                        <td colspan="3">  <img src="Images/pie.jpg" /> </td>
                                     
                                       </td>
                                    </tr>
                                </table>

                               
                            

                            </div>
                        </div>

                    </ItemTemplate>

                </asp:FormView>

                <asp:ObjectDataSource ID="ODSDetalle" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="getActividadesByID" TypeName="com.paginar.johnson.BL.ControllerVoluntariadoComunidad">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hfActividad" DefaultValue="0" Name="id" PropertyName="Value" Type="Int32" />
                    </SelectParameters>

                </asp:ObjectDataSource>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnTrigger" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>



    <DIV style="text-align: center; FONT-SIZE: 11px; BORDER-TOP: 1px solid; FONT-FAMILY: Arial; BORDER-RIGHT: 1px solid; BACKGROUND-IMAGE: url(images/alerta.png); BACKGROUND-REPEAT: no-repeat; BORDER-BOTTOM: 1px solid; BACKGROUND-POSITION: 10px center; COLOR: #9f6000; PADDING-BOTTOM: 15px; PADDING-TOP: 15px; PADDING-LEFT: 50px; BORDER-LEFT: 1px solid; MARGIN-TOP: 50px; DISPLAY: block; PADDING-RIGHT: 50px; BACKGROUND-COLOR: #feefb3">
    Si estas interesado en participar en alguna de estas actividades por favor contacta al capitán de tu área o a: 
    <br><br>
    <B>María del Carmen Aranguren <a href="mailto:marangur@scj.com">marangur@scj.com</a></B> 
    <br>o<br>
    <B>Alexandra Hoppe <a href="mailto:ahoppe@ziglaconsultores.com">ahoppe@ziglaconsultores.com</B>
    <br><br>
    <a href="Cronograma2015.pptx">Desgargar cronograma 2015</a>

</DIV>
</asp:Content>
