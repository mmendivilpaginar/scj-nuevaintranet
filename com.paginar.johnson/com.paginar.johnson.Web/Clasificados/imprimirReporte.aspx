﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imprimirReporte.aspx.cs" Inherits="com.paginar.johnson.Web.Clasificados.imprimirReporte" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Comentarios Avisos Clasificados</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
<style type="text/css">

table.ClasificadosComentarios{
    border:0;
}
table.ClasificadosComentarios th{
    background-color:#CCC;
    text-transform:uppercase;
    font-weight:bold;
}
table.ClasificadosComentarios td {
    border:1px solid #aaa;
}
table.ClasificadosComentarios td.vacio {
    border:0;
}
table.ClasificadosComentarios tr.separadorClasificados td {
    height:35px;
    border:0;
}

table.ClasificadosComentarios tr.separadorComentarios td{
    border:0;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
      </asp:ScriptManager>
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Reporte Clasificados</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
        <div id="noprint">
            
            
                <div class="box filtros" style="display:none;">
                    <asp:RadioButtonList ID="RBLFiltroClasificados" runat="server" 
                        RepeatLayout="Flow" AutoPostBack="True">
                        <asp:ListItem Selected="True">TODOS</asp:ListItem>
                        <asp:ListItem Value="FECHA">HOY</asp:ListItem>
                        <asp:ListItem Value="DESDEHASTA">RANGO</asp:ListItem>
                    </asp:RadioButtonList>
                <asp:TextBox ID="TextBox1" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CalendarExtender TargetControlID="TextBox1" ID="CalendarExtender1" 
                        runat="server" PopupButtonID="btnc" Enabled="True" Format="dd/MM/yyyy" 
                        DaysModeTitleFormat="dd/MM/yyyy" TodaysDateFormat="dd/MM/yyyy"></asp:CalendarExtender><asp:ImageButton ID="btnc" runat="server" ImageUrl="~/images/Calendar.png">
                    </asp:ImageButton>
                    -
                <asp:TextBox ID="TextBox2" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CalendarExtender TargetControlID="TextBox2" PopupButtonID="btnhasta" 
                        ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy" 
                        DaysModeTitleFormat="dd/MM/yyyy" TodaysDateFormat="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:ImageButton ID="btnhasta" runat="server" ImageUrl="~/images/Calendar.png" />

                    <asp:Button ID="btnFlitroClasificado" runat="server" Text="Filtrar" />
                </div>
           <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
        </div>
        <br />
        <br />
        <asp:Panel ID="PanelClasificados" runat="server">
                <div class="body">
                    <asp:Label ID="lblContenido" runat="server"></asp:Label>
                </div>

        </asp:Panel>

    
    </div>
    </form>
</body>
</html>
