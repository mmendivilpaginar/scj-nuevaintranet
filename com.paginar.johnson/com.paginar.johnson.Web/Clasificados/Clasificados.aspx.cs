﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.Clasificados
{
    public partial class Clasificados : PageBase
    {
        private ControllerClasificados _CC;
        public ControllerClasificados CC
        {
            get
            {
                _CC = (ControllerClasificados)this.Session["CC"];
                if (_CC == null)
                {
                    _CC = new ControllerClasificados();
                    this.Session["CC"] = _CC;
                }
                return _CC;
            }
            set
            {
                this.Session["CC"] = value;
            }
        }

        public static Control GetPostBackControl(Page page)
        {

            Control postbackControlInstance = null;
            string postbackControlName = page.Request.Params.Get("__EVENTTARGET");
            if (postbackControlName != null && postbackControlName != string.Empty)
            {
                postbackControlInstance = page.FindControl(postbackControlName);
            }
            else
            {
                // handle the Button control postbacks
                for (int i = 0; i < page.Request.Form.Keys.Count; i++)
                {

                    postbackControlInstance = page.FindControl(page.Request.Form.Keys[i]);

                    if (postbackControlInstance is System.Web.UI.WebControls.Button)
                    {

                        return postbackControlInstance;
                    }
                }
            }

            // handle the ImageButton postbacks

            if (postbackControlInstance == null)
            {
                for (int i = 0; i < page.Request.Form.Count; i++)
                {
                    if ((page.Request.Form.Keys[i].EndsWith(".x")) || (page.Request.Form.Keys[i].EndsWith(".y")))
                    {
                        postbackControlInstance = page.FindControl(page.Request.Form.Keys[i].Substring(0, page.Request.Form.Keys[i].Length - 2));
                        return postbackControlInstance;
                    }
                }
            }
            return postbackControlInstance;

        }   

        protected void Page_Load(object sender, EventArgs e)
        {
            RadioButtonList RBL = (RadioButtonList)RBLFiltroClasificados;
            if (RBL.SelectedIndex == 0 || RBL.SelectedIndex == 1)
            {
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox1.Enabled = false;
                TextBox2.Enabled = false;
            }
            else
            {
                TextBox1.Enabled = true;
                TextBox2.Enabled = true;
            }

            TextBox1.Text = TextBox1.Text.Trim();
            TextBox2.Text = TextBox2.Text.Trim();
            
            com.paginar.johnson.membership.Roles RLS = new membership.Roles();
            TabPanel3.Visible = (RLS.IsUserInRole(Page.User.Identity.Name, "Administradores generales"));
            if (!Page.IsPostBack)
            {


                UsrAltaHiddenField.Value = this.ObjectUsuario.usuarioid.ToString();
                MVCABM.SetActiveView(VABM);
                CC = new ControllerClasificados();
                MVBusqueda.SetActiveView(VResultado);
                //if ( TextBoxBuscar.Text.ToString() != string.Empty)
                //{
                //    MVBusqueda.SetActiveView(VResultado);
                //    LimitHiddenField.Value = "false";
                //    ClasificadosDL.DataBind();
                //}


            }
            else 
            {


                RepeaterClasificado.DataBind();
            }

            if (!string.IsNullOrEmpty(Request.QueryString["AvisoID"]))
            {
                AvisosTabContainer.TabIndex = 0;
                AvisoIDDetalleHiddenField.Value = Request.QueryString["AvisoID"];
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = Request.QueryString["AvisoID"];
                MVBusqueda.SetActiveView(VDetalleAviso);
            }

                Page.MaintainScrollPositionOnPostBack = false;
        }

        protected void ButtonBuscar_Click(object sender, EventArgs e)
        {
            MVBusqueda.SetActiveView(VResultado);
            LimitHiddenField.Value = "false";
            ClasificadosDL.DataBind();
        }

        protected void ODSClasificados_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //Control cause = GetPostBackControl(Page);
            //if (cause == null)
            //{
            //    e.Cancel = true;
            //    return;
            //}

            //if (cause.ID != ButtonBuscar.ID)
               // e.Cancel = true;    
            
            
        }

        protected void ClasificadosDL_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ///
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.SelectedItem || e.Item.ItemType == ListItemType.EditItem)
            {
                Image AvisoImage = e.Item.FindControl("AvisoImage") as Image;
                HyperLink HyperLinkArchivoAdjunto = e.Item.FindControl("HyperLinkArchivoAdjunto") as HyperLink;
                // si es imagen, oculto icono adjunto y mutro imagen
                if (File.Exists(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl)))
                {
                    string extension = Path.GetExtension(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl));
                    if (extension.ToUpper() == ".JPG")
                    {
                        AvisoImage.Visible = true;
                        HyperLinkArchivoAdjunto.Visible = false;
                        AvisoImage.ImageUrl = "/ResizeImage.aspx?Image=" + HyperLinkArchivoAdjunto.NavigateUrl + "&Size=132";

                    }
                    else
                    {
                        AvisoImage.Visible = false;
                    }

                }


                //ImageButton EditarLinkButton = (ImageButton)e.Item.FindControl("EditarLinkButton");
                LinkButton EditarLinkButton = (LinkButton)e.Item.FindControl("EditarLinkButton");
                HiddenField UsrAltaHiddenField = (HiddenField)e.Item.FindControl("UsrAltaHiddenField");
                com.paginar.johnson.membership.Roles R = new membership.Roles();
                //EditarLinkButton.Visible = R.IsUserInRole(Page.User.Identity.Name, "Administradores generales");
                EditarLinkButton.Visible = (R.IsUserInRole(Page.User.Identity.Name, "Administradores generales") || (int.Parse(UsrAltaHiddenField.Value) == this.ObjectUsuario.usuarioid));

                


                Label lblnombreapellido = (Label)e.Item.FindControl("LabelNombre");

                HiddenField AvisoIDHiddenField = (HiddenField)e.Item.FindControl("AvisoIDHiddenField");
                Label lblConComents = (Label)e.Item.FindControl("lblConComents");
                lblConComents.Text = CC.getComentariosByAvisoID(int.Parse(AvisoIDHiddenField.Value)).Rows.Count.ToString();
            }
        }

        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

        protected void FormView1_DataBound(object sender, EventArgs e)
        {
            FormView FVClasificado = ((FormView)sender);
            if (FVClasificado.CurrentMode == FormViewMode.Insert)
            {

                TextBox NombreTextBox = FVClasificado.FindControl("NombreTextBox") as TextBox;
                NombreTextBox.Text = this.ObjectUsuario.nombre + " " + this.ObjectUsuario.apellido;
                TextBox FHAltaTextBox = FVClasificado.FindControl("FHAltaTextBox") as TextBox;
                FHAltaTextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                HiddenField UsrAltaHiddenField = FVClasificado.FindControl("UsrAltaHiddenField") as HiddenField;
                UsrAltaHiddenField.Value = this.ObjectUsuario.usuarioid.ToString();

            }
        }

        protected void FVClasificado_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            FileUpload ArchivoAdjuntoFileUpload = FVClasificado.FindControl("ArchivoAdjuntoFileUpload") as FileUpload;

            if (ArchivoAdjuntoFileUpload.HasFile)
            {
                string FileName = Path.GetFileNameWithoutExtension(ArchivoAdjuntoFileUpload.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(ArchivoAdjuntoFileUpload.FileName);
                string strPath = MapPath("~/Clasificados/archivos/") + FileName;
                ArchivoAdjuntoFileUpload.SaveAs(strPath);
                e.Values["ArchivoAdjunto"] = FileName;
            }
            TextBox vencimiento_TextBox = FVClasificado.FindControl("vencimiento_TextBox") as TextBox;
            e.Values["FechaVencimiento"] = vencimiento_TextBox.Text;
            RadioButtonList EstadoRadioButtonList = FVClasificado.FindControl("EstadoRadioButtonList") as RadioButtonList;
            e.Values["Habilitado"] = EstadoRadioButtonList.Items[0].Selected;
            TextBox tb = FVClasificado.FindControl("LinkTextBox") as TextBox;
            // http://
            if (tb.Text.Length > 7)
            {
                if (tb.Text.ToString().Substring(0, 7) != "http://")
                {

                    tb.Text = "http://" + tb.Text;

                }
            }
            e.Values["Link"] = tb.Text.ToString();
        }

        protected void FVClasificado_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            MisClasificadosDL.DataBind();
            FVClasificado.DataBind();
            Timer1.Enabled = true;
            MVCABM.SetActiveView(VResul);
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            MVCABM.SetActiveView(VABM);
            
        }

        protected void ODSClasificados_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = CC;
        }

        protected void ODSClasificados_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSAvisos_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = CC;
        }

        protected void ODSAvisos_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
            
        }

        protected global::AjaxControlToolkit.TabContainer TabContainer1;

        protected void DLAvisosRubro_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Detalle")
            {
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
            }
            if (e.CommandName == "EditarAviso")
            {
                FVDetalleClasificado.ChangeMode(FormViewMode.Edit);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
            }
            if (e.CommandName == "VERCOMENTARIOS")
            {
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
                TextBox tbcomentario = (TextBox)FVDetalleClasificado.FindControl("txtComentario");
                tbcomentario.Focus();
                SetFocus("CPHMain_main_AvisosTabContainer_Panel1_FVDetalleClasificado_txtComentario");
            }
            
            }

        protected void FVDetalleClasificado_DataBound(object sender, EventArgs e)
        {
            ControllerUCHome CU = new ControllerUCHome();
            DsUCHome.UsuarioDataTable du = CU.getDataUsuario(this.ObjectUsuario.usuarioid);
            DsUCHome.UsuarioRow ru = (DsUCHome.UsuarioRow)du.Rows[0];
            if (FVDetalleClasificado.CurrentMode == FormViewMode.ReadOnly)
            {

                Image AvisoImage = FVDetalleClasificado.FindControl("AvisoImage") as Image;
                HyperLink HyperLinkArchivoAdjunto = FVDetalleClasificado.FindControl("HyperLinkArchivoAdjunto") as HyperLink;
                // si es imagen, oculto icono adjunto y mutro imagen
                if ((HyperLinkArchivoAdjunto!=null)&&(File.Exists(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl))))
                {
                    string extension = Path.GetExtension(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl));
                    if (extension.ToUpper() == ".JPG")
                    {
                        HyperLinkArchivoAdjunto.Visible = false;
                        AvisoImage.Visible = true;
                        AvisoImage.ImageUrl = "/ResizeImage.aspx?Image=" + HyperLinkArchivoAdjunto.NavigateUrl + "&Size=132";

                    }
                    else
                    {
                        AvisoImage.Visible = false;
                    }

                }

                Label lblnombreapellido = (Label)FVDetalleClasificado.FindControl("LabelNombre");
                //Panel pnl = (Panel)FVDetalleClasificado.FindControl("pnlUsuario");
                //pnl.Attributes.Add("style", "display:none");
                //Literal ltjs = (Literal)FVDetalleClasificado.FindControl("ltjs");
                //ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
                HiddenField AvisoIDHiddenField = (HiddenField)FVDetalleClasificado.FindControl("AvisoIDHiddenField");
                


                Image imgUsuario = FVDetalleClasificado.FindControl("imgUsuario") as Image;
                if(imgUsuario!=null)
                    imgUsuario.ImageUrl = "~/images/personas/" + ru.UsuarioFoto;
            }
            if (FVDetalleClasificado.CurrentMode == FormViewMode.Edit)
            {
                TextBox NombreTextBox = FVDetalleClasificado.FindControl("NombreTextBox") as TextBox;
                NombreTextBox.Text = this.ObjectUsuario.nombre + " " + this.ObjectUsuario.apellido;
                HiddenField DiasVigenciaHiddenField = (HiddenField)FVDetalleClasificado.FindControl("DiasVigenciaHiddenField");
                HiddenField FHPublicacionHiddenField = (HiddenField)FVDetalleClasificado.FindControl("FHPublicacionHiddenField");

                if (!string.IsNullOrEmpty(DiasVigenciaHiddenField.Value))
                {
                    double DiasVigencia = double.Parse(DiasVigenciaHiddenField.Value);
                    TextBox vencimiento_TextBox = (TextBox)FVDetalleClasificado.FindControl("vencimiento_TextBox");
                    DateTime FHPublicacion = DateTime.Parse(FHPublicacionHiddenField.Value);
                    vencimiento_TextBox.Text = FHPublicacion.AddDays(DiasVigencia).ToString("dd/MM/yyy");
                }

                
                string seleccionado = string.Empty;
                
                HiddenField HabilitadoHiddenField = (HiddenField)FVDetalleClasificado.FindControl("HabilitadoHiddenField");
                RadioButtonList EstadoRadioButtonList = (RadioButtonList)FVDetalleClasificado.FindControl("EstadoRadioButtonList");
               // ListItem LI = EstadoRadioButtonList.Items.FindByValue(HabilitadoHiddenField.Value.ToUpper()=="TRUE"? "1":"0");
               // if(LI!=null)
               //     LI.Selected = true;
                RadioButtonList RBLHabilitado = (RadioButtonList)FVDetalleClasificado.FindControl("EstadoRadioButtonList");
                HiddenField HFHabilitado = (HiddenField)FVDetalleClasificado.FindControl("HabilitadoHiddenField");
                if (HFHabilitado.Value.ToUpper() == "TRUE")
                {
                    seleccionado = "1";
                }
                else
                {
                    seleccionado = "0";
                }
                RBLHabilitado.SelectedValue = seleccionado;//HFHabilitado.Value.ToString();                

            }
        }

        protected void FVDetalleClasificado_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            TextBox txtComentario = FVDetalleClasificado.FindControl("txtComentario") as TextBox;
            DataList dtComments = FVDetalleClasificado.FindControl("dtComments") as DataList;
            if (e.CommandName == "Comentar")
            {
                string Page = Request.Url.ToString() + string.Format("?AvisoID={0}#comentarios-abajo", e.CommandArgument.ToString());
                CC.InsertUSuarioComentario(this.ObjectUsuario.usuarioid, int.Parse(e.CommandArgument.ToString()), txtComentario.Text, Page);

                txtComentario.Text = "";
                dtComments.DataBind();
            }
        }

        protected void FVDetalleClasificado_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            FileUpload ArchivoAdjuntoFileUpload = FVClasificado.FindControl("ArchivoAdjuntoFileUpload") as FileUpload;

            if (ArchivoAdjuntoFileUpload.HasFile)
            {
                string FileName = Path.GetFileNameWithoutExtension(ArchivoAdjuntoFileUpload.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(ArchivoAdjuntoFileUpload.FileName);
                string strPath = MapPath("~/Clasificados/archivos/") + FileName;
                ArchivoAdjuntoFileUpload.SaveAs(strPath);
                e.Values["ArchivoAdjunto"] = FileName;
            }

        }

        protected void TipoAvisoIDRadioButtonList_DataBound(object sender, EventArgs e)
        {
            RadioButtonList RBLTipoAviso = sender as RadioButtonList;
            ListItem LI = RBLTipoAviso.Items.FindByValue("2");
            if (LI != null)
                LI.Selected = true;
            
        }

        protected void DropDownListTipoAviso_DataBound(object sender, EventArgs e)
        {
            DropDownList DropDownListTipoAviso = sender as DropDownList;
            ListItem LI = DropDownListTipoAviso.Items.FindByValue("2");
            if (LI != null)
                LI.Selected = true;
        }

        protected void DropDownListPlantas_DataBound(object sender, EventArgs e)
        {
            DropDownListPlantas.Items.Insert(0,new ListItem("Todos", "-1"));
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime FecVenc = DateTime.Parse(args.Value);
            args.IsValid = (FecVenc.Date.Subtract(DateTime.Now.Date).Days > 0);
             
        }

        protected void CustomValidator1_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            DateTime FecVenc = DateTime.Parse(args.Value);
            args.IsValid = (FecVenc.Date.Subtract(DateTime.Now.Date).Days > 0);
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FileUpload ArchivoAdjuntoFileUpload = FVClasificado.FindControl("ArchivoAdjuntoFileUpload") as FileUpload;
            args.IsValid = (ArchivoAdjuntoFileUpload.FileBytes.Length <= 5242880);
           
        }

        protected void FVDetalleClasificado_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            FileUpload ArchivoAdjuntoFileUpload = FVDetalleClasificado.FindControl("ArchivoAdjuntoEditFileUpload") as FileUpload;

            if (ArchivoAdjuntoFileUpload.HasFile)
            {
                string FileName = Path.GetFileNameWithoutExtension(ArchivoAdjuntoFileUpload.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(ArchivoAdjuntoFileUpload.FileName);
                string strPath = MapPath("~/Clasificados/archivos/") + FileName;
                ArchivoAdjuntoFileUpload.SaveAs(strPath);
                e.NewValues["ArchivoAdjunto"] = FileName;
            }
            else
            {
                HiddenField ArchivoAdjuntoHiddenField = FVDetalleClasificado.FindControl("ArchivoAdjuntoHiddenField") as HiddenField;
                e.NewValues["ArchivoAdjunto"] = ArchivoAdjuntoHiddenField.Value;
            }

            TextBox vencimiento_TextBox = FVDetalleClasificado.FindControl("vencimiento_TextBox") as TextBox;
            e.NewValues["FechaVencimiento"] = vencimiento_TextBox.Text;
            RadioButtonList EstadoRadioButtonList = FVDetalleClasificado.FindControl("EstadoRadioButtonList") as RadioButtonList;
            e.NewValues["Habilitado"] = EstadoRadioButtonList.Items[0].Selected;

            TextBox tb = FVDetalleClasificado.FindControl("LinkTextBox") as TextBox;
            // http://
            if (tb.Text.Length > 7)
            {

                if (tb.Text.ToString().Substring(0, 7) != "http://")
                {

                    tb.Text = "http://" + tb.Text;

                }
            }
            e.NewValues["Link"] = tb.Text.ToString();
        }

        protected void ODSMisAvisos_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = CC;
        }

        protected void ODSMisAvisos_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSAvisos0_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = CC;
        }

        protected void ODSAvisos0_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void DLAvisosRubro0_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "EditarAviso")
            {
                AvisosTabContainer.ActiveTabIndex = 0;
                FVDetalleClasificado.ChangeMode(FormViewMode.Edit);
                
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                //FVDetalleClasificado.DataBind();
                MVBusqueda.SetActiveView(VDetalleAviso);
                TextBox TituloTextBox = FVDetalleClasificado.FindControl("TituloTextBox") as TextBox;
                //SetFocus(TituloTextBox.ClientID);
                //SetFocus("DetalleAviso-abajo");
                //Page.SetFocus(TituloTextBox);
            }

            if (e.CommandName == "VERCOMENTARIOS")
            {
                AvisosTabContainer.ActiveTabIndex = 0;
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
                TextBox tbcomentario = (TextBox)FVDetalleClasificado.FindControl("txtComentario");
                tbcomentario.Focus();
                //txtComentario
                //SetFocus("comentarios-abajo");
            }

            if (e.CommandName == "Detalle")
            {
                AvisosTabContainer.ActiveTabIndex = 0;
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
                //SetFocus("DetalleAviso-abajo");
                TextBox tbcomentario = (TextBox)FVDetalleClasificado.FindControl("txtComentario");
                tbcomentario.Focus();
            }
        }

        protected void FVDetalleClasificado_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            //AvisosTabContainer.ActiveTabIndex = 2;
            
            FVDetalleClasificado.DataBind();
            MisClasificadosDL.DataBind();
            ClasificadosDL.DataBind();
            FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
        }

        protected void DLAvisosRubro0_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            LinkButton EditarLinkButton = (LinkButton)e.Item.FindControl("EditarLinkButton");
            HiddenField UsrAltaHiddenField = (HiddenField)e.Item.FindControl("UsrAltaHiddenField");
            EditarLinkButton.Visible = (int.Parse(UsrAltaHiddenField.Value) == this.ObjectUsuario.usuarioid);


            Label lblnombreapellido = (Label)e.Item.FindControl("LabelNombre");
            //Panel pnl = (Panel)e.Item.FindControl("pnlUsuario");
            //pnl.Attributes.Add("style", "display:none");
            //Literal ltjs = (Literal)e.Item.FindControl("ltjs");
            //ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
            HiddenField AvisoIDHiddenField = (HiddenField)e.Item.FindControl("AvisoIDHiddenField");
            Label lblConComents = (Label)e.Item.FindControl("lblConComents");
            lblConComents.Text = CC.getComentariosByAvisoID(int.Parse(AvisoIDHiddenField.Value)).Rows.Count.ToString();

            Image AvisoImage = e.Item.FindControl("AvisoImage") as Image;
            HyperLink HyperLinkArchivoAdjunto = e.Item.FindControl("HyperLinkArchivoAdjunto") as HyperLink;
            // si es imagen, oculto icono adjunto y mutro imagen
            if (File.Exists(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl)))
            {
                string extension = Path.GetExtension(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl));
                if (extension.ToUpper() == ".JPG")
                {
                    HyperLinkArchivoAdjunto.Visible = false;
                    AvisoImage.Visible = true;
                    AvisoImage.ImageUrl = "/ResizeImage.aspx?Image=" + HyperLinkArchivoAdjunto.NavigateUrl + "&Size=132";

                }
                else
                {
                    AvisoImage.Visible = false;
                }

            }
        }

        private void SetFocus(String controlID)
        {
            string script = "<SCRIPT language='javascript'>location.href='#" + controlID + "';</SCRIPT>";
            // Register the script code with the page.
            ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100",  script, false);
        }

        protected void FVDetalleClasificado_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {            
            FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
            FVDetalleClasificado.DataBind();
            MisClasificadosDL.DataBind();
            AvisosTabContainer.ActiveTabIndex = 1;
            Timer1.Enabled = true;
            MVCABM.SetActiveView(VResul);
        }

        protected string truncarTexto(string Cadena, int AvisoID)
        {
            string CadenaHTML;
            CadenaHTML = Server.HtmlEncode(Cadena);
            string strAux = "";
            if (CadenaHTML.Trim().Length < 250)
                strAux = Cadena;
            else
            {
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htm = new HtmlTextWriter(sw);

                strAux = CadenaHTML.Substring(0, 250) + " [...] ";
            }
            LinkButton LnkBtnAviso = new LinkButton();
            //string Page = Request.Url.ToString() + string.Format("?AvisoID={0}", AvisoID.ToString());
            string Page = "/Clasificados/avisoClasificado.aspx" + string.Format("?AvisoID={0}", AvisoID.ToString());
            
            return strAux + string.Format("<br><strong><a href='{0}'>AMPLIAR</a></strong>", Page);

        }

        protected void MisClasificadosDL_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "EditarAviso")
            {
                AvisosTabContainer.ActiveTabIndex = 0;
                FVDetalleClasificado.ChangeMode(FormViewMode.Edit);

                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                //FVDetalleClasificado.DataBind();
                MVBusqueda.SetActiveView(VDetalleAviso);
                TextBox TituloTextBox = FVDetalleClasificado.FindControl("TituloTextBox") as TextBox;
                //SetFocus(TituloTextBox.ClientID);
                //SetFocus("DetalleAviso-abajo");
                //Page.SetFocus(TituloTextBox);
            }

            if (e.CommandName == "Detalle")
            {
                AvisosTabContainer.ActiveTabIndex = 0;
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = e.CommandArgument.ToString();
                MVBusqueda.SetActiveView(VDetalleAviso);
                //SetFocus("DetalleAviso-abajo");
                TextBox tbcomentario = (TextBox)FVDetalleClasificado.FindControl("txtComentario");
                tbcomentario.Focus();
            }
        }

        protected void btnFlitroClasificado_Click(object sender, EventArgs e)
        {
            TextBox1.Text = TextBox1.Text.Trim();
            TextBox2.Text = TextBox2.Text.Trim();
            RadioButtonList RBL = (RadioButtonList)RBLFiltroClasificados;
            if (RBL.SelectedIndex == 0 || RBL.SelectedIndex == 1)
            {
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            RepeaterClasificado.DataBind();

        }

        protected void Export_Click(object sender, EventArgs e)
        {

            StringWriter sw = new StringWriter();

            string attachment = "attachment; filename=CLasificado-" + DateTime.Now.ToShortDateString() + ".xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            DSClasificados.clasiAvisoDataTable CADT = new DSClasificados.clasiAvisoDataTable();
            DSClasificados.claAvisoComentariosDataTable CACDT = new DSClasificados.claAvisoComentariosDataTable();

            DateTime? Desde = new DateTime();
            DateTime? Hasta = new DateTime();
            if(TextBox1.Text.Trim() != "" )
                Desde =  Convert.ToDateTime(TextBox1.Text);
            else
                Desde = null;
            if(TextBox2.Text.Trim() != "" )
                Hasta = Convert.ToDateTime(TextBox2.Text);
            else
                Hasta = null;

            CADT = CC.getClasificadosConComentario(Desde, Hasta, RBLFiltroClasificados.SelectedValue);
            sw.WriteLine("<table VALIGN=\"top\" ALING=\"left\" BORDER=\"5\"><thead><tr><th bgcolor=\"#CCCCCC\">Rubro</th><th bgcolor=\"#CCCCCC\">Fecha</th><th bgcolor=\"#CCCCCC\">Autor</th><th bgcolor=\"#CCCCCC\">Titulo</th><th bgcolor=\"#CCCCCC\">Cuerpo</th></tr></thead>");

                foreach (DSClasificados.clasiAvisoRow AvisoRow in CADT)
                {
                    sw.WriteLine("<tr>");
                    sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\"><strong> " + AvisoRow.RubroDesc + " - " + AvisoRow.TipoAvisoDet + "</strong></td>");
                    sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + AvisoRow.FHAlta.ToShortDateString() + "</td>");
                    sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + AvisoRow.NombreApellido + "</td>");
                    sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + AvisoRow.Titulo.ToString().ToUpper() + "</td>");
                    sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + AvisoRow.Descrip + "</td>");
                    sw.WriteLine("</tr>");
                    CACDT = CC.getComentariosByAvisoID(AvisoRow.AvisoID);
                    sw.WriteLine("<tr>");
                        sw.WriteLine("<td colspan=\"5\">");
                        sw.WriteLine("<table BORDER=\"2\" BORDERCOLOR=\"#558EC6\">");
                        sw.WriteLine("<tr>");
                            sw.WriteLine("<td>");
                            sw.WriteLine("</td>");

                            sw.WriteLine("<td bgcolor=\"#558EC6\">");
                            sw.WriteLine("<strong>Autor</strong>"); 
                            sw.WriteLine("</td>");

                            sw.WriteLine("<td bgcolor=\"#558EC6\">");
                            sw.WriteLine("<strong>Fecha</strong>"); 
                            sw.WriteLine("</td>");

                            sw.WriteLine("<td bgcolor=\"#558EC6\">");
                            sw.WriteLine("<strong>Comentario</strong>"); 
                            sw.WriteLine("</td>");
                            sw.WriteLine("</tr>");
                        foreach(DSClasificados.claAvisoComentariosRow ComentarioRow in CACDT)
                        {
                            sw.WriteLine("<tr>");
                            sw.WriteLine("<td>");
                            sw.WriteLine("</td>");
                            sw.WriteLine("<td>");
                                sw.WriteLine(ComentarioRow.nombrecompleto);
                            sw.WriteLine("</td>");
                            sw.WriteLine("<td>");
                                sw.WriteLine(ComentarioRow.Fecha);
                            sw.WriteLine("</td>");
                            sw.WriteLine("<td>");
                                sw.WriteLine(ComentarioRow.comentarios);
                            sw.WriteLine("</td>");
                            sw.WriteLine("</tr>");

                        }

                        sw.WriteLine("</table>");
                        sw.WriteLine("</td>");
                    sw.WriteLine("</tr>");

                }
            sw.WriteLine("</table>");

//            RepeaterClasificado.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();


        }

        protected void RBLFiltroClasificados_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList RBLTipoBusqueda = (RadioButtonList)sender;
            if (RBLTipoBusqueda.SelectedIndex != 2)
            {
                TextBox1.Enabled = false;
                TextBox2.Enabled = false;
            }
            else
            {
                TextBox1.Enabled = true;
                TextBox2.Enabled = true;
            }
        }

        protected void DropDownListClusters_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList DDLC = (DropDownList)sender;
            if (DDLC.SelectedValue != "0")
            {
                DropDownListPlantas.Enabled = true;
            }
            else 
            {
                DropDownListPlantas.Enabled = false;
            }
        }




    }
}
