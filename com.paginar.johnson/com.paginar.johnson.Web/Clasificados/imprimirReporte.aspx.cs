﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;


namespace com.paginar.johnson.Web.Clasificados
{
    public partial class imprimirReporte : System.Web.UI.Page
    {

        private ControllerClasificados _CC;
        public ControllerClasificados CC
        {
            get
            {
                _CC = (ControllerClasificados)this.Session["CC"];
                if (_CC == null)
                {
                    _CC = new ControllerClasificados();
                    this.Session["CC"] = _CC;
                }
                return _CC;
            }
            set
            {
                this.Session["CC"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
                RadioButtonList RBL = (RadioButtonList)RBLFiltroClasificados;
                if (RBL.SelectedIndex == 0 || RBL.SelectedIndex == 1)
                {
                    TextBox1.Text = "";
                    TextBox2.Text = "";
                }


                StringWriter sw = new StringWriter();

                string attachment = "attachment; filename=CLasificado-" + DateTime.Now.ToShortDateString() + ".xls";
                Response.ClearContent();
                Response.ContentType = "text/HTML";
                DSClasificados.clasiAvisoDataTable CADT = new DSClasificados.clasiAvisoDataTable();
                DSClasificados.claAvisoComentariosDataTable CACDT = new DSClasificados.claAvisoComentariosDataTable();

                DateTime? Desde = new DateTime();
                DateTime? Hasta = new DateTime();

                if (TextBox1.Text.Trim() != "")
                    Desde = DateTime.Parse(TextBox1.Text);
                    //Desde = DateTime.ParseExact(TextBox1.Text, "yyyyMMdd","Es-Ar");
                else
                    Desde = null;

                if (TextBox2.Text.Trim() != "")
                    Hasta = DateTime.Parse(TextBox2.Text);
                else
                    Hasta = null;

                CADT = CC.getClasificadosConComentario(Desde, Hasta, RBLFiltroClasificados.SelectedValue);
                sw.WriteLine("<table class=\"ClasificadosComentarios\"><thead><tr><th width=\"100\">Rubro</th><th width=\"85\">Fecha</th><th width=\"85\">Autor</th><th width=\"150\">Titulo</th><th width=\"330\">Cuerpo</th></tr></thead>");

                foreach (DSClasificados.clasiAvisoRow AvisoRow in CADT)
                {
                    sw.WriteLine("<tr class=\"clasificado\">");
                    sw.WriteLine("<td valign=\"top\"><strong> " + AvisoRow.RubroDesc + " - " + AvisoRow.TipoAvisoDet + "</strong></td>");
                    sw.WriteLine("<td valign=\"top\">" + AvisoRow.FHAlta.ToShortDateString() + "</td>");
                    sw.WriteLine("<td valign=\"top\">" + AvisoRow.NombreApellido + "</td>");
                    sw.WriteLine("<td valign=\"top\">" + AvisoRow.Titulo.ToString().ToUpper() + "</td>");
                    sw.WriteLine("<td valign=\"top\">" + AvisoRow.Descrip + "</td>");
                    sw.WriteLine("</tr>");
                    sw.WriteLine("<tr class=\"separadorComentarios\"><td colspan=\"5\"></td></tr>");


                    CACDT = CC.getComentariosByAvisoID(AvisoRow.AvisoID);
                    sw.WriteLine("<tr class=\"cabeceraCometarios\">");
                    sw.WriteLine("<td class=\"vacio\"></td>");
                    sw.WriteLine("<td><strong>Autor</strong></td>");
                    sw.WriteLine("<td><strong>Fecha</strong></td>");
                    sw.WriteLine("<td colspan=\"2\"><strong>Comentario</strong></td>");
                    sw.WriteLine("</tr>");
                    foreach (DSClasificados.claAvisoComentariosRow ComentarioRow in CACDT)
                    {
                        sw.WriteLine("<tr class=\"comentario\">");
                        sw.WriteLine("<td class=\"vacio\"></td>");
                        sw.WriteLine("<td valign=\"top\">");
                        sw.WriteLine(ComentarioRow.nombrecompleto);
                        sw.WriteLine("</td>");
                        sw.WriteLine("<td valign=\"top\">");
                        sw.WriteLine(ComentarioRow.Fecha);
                        sw.WriteLine("</td>");
                        sw.WriteLine("<td colspan=\"2\" valign=\"top\">");
                        sw.WriteLine(ComentarioRow.comentarios);
                        sw.WriteLine("</td>");
                        sw.WriteLine("</tr>");
                    }

                    sw.WriteLine("<tr class=\"separadorClasificados\"><td colspan=\"5\"></td></tr>");
                }
                
                sw.WriteLine("</table>");

                //RepeaterClasificado.RenderControl(htw);
                //Response.Write(sw.ToString());
                //Response.Flush();
                //Response.End();
                lblContenido.Text = sw.ToString();
            

        }

        protected void Export_Click(object sender, EventArgs e)
        {

        }
    }
}