﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.Clasificados
{
    public partial class avisoClasificado : PageBase
    {
        private ControllerClasificados _CC;
        public ControllerClasificados CC
        {
            get
            {
                _CC = (ControllerClasificados)this.Session["CC"];
                if (_CC == null)
                {
                    _CC = new ControllerClasificados();
                    this.Session["CC"] = _CC;
                }
                return _CC;
            }
            set
            {
                this.Session["CC"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            com.paginar.johnson.membership.Roles RLS = new membership.Roles();
            
            if (!Page.IsPostBack)
            {


                UsrAltaHiddenField.Value = this.ObjectUsuario.usuarioid.ToString();
                
                


            }

            if (!string.IsNullOrEmpty(Request.QueryString["AvisoID"]))
            {
                
                AvisoIDDetalleHiddenField.Value = Request.QueryString["AvisoID"];
                FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
                AvisoIDDetalleHiddenField.Value = Request.QueryString["AvisoID"];
                
            }           

        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime FecVenc = DateTime.Parse(args.Value);
            args.IsValid = (FecVenc.Date.Subtract(DateTime.Now.Date).Days > 0);

        }



        protected void CustomValidator1_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            DateTime FecVenc = DateTime.Parse(args.Value);
            args.IsValid = (FecVenc.Date.Subtract(DateTime.Now.Date).Days > 0);
        }
        protected void FVDetalleClasificado_DataBound(object sender, EventArgs e)
        {
            ControllerUCHome CU = new ControllerUCHome();
            DsUCHome.UsuarioDataTable du = CU.getDataUsuario(this.ObjectUsuario.usuarioid);
            DsUCHome.UsuarioRow ru = (DsUCHome.UsuarioRow)du.Rows[0];
            if (FVDetalleClasificado.CurrentMode == FormViewMode.ReadOnly)
            {

                Image AvisoImage = FVDetalleClasificado.FindControl("AvisoImage") as Image;
                HyperLink HyperLinkArchivoAdjunto = FVDetalleClasificado.FindControl("HyperLinkArchivoAdjunto") as HyperLink;
                // si es imagen, oculto icono adjunto y mutro imagen
                if ((HyperLinkArchivoAdjunto != null) && (File.Exists(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl))))
                {
                    string extension = Path.GetExtension(Server.MapPath(HyperLinkArchivoAdjunto.NavigateUrl));
                    if (extension.ToUpper() == ".JPG")
                    {
                        HyperLinkArchivoAdjunto.Visible = false;
                        AvisoImage.Visible = true;
                        AvisoImage.ImageUrl = "/ResizeImage.aspx?Image=" + HyperLinkArchivoAdjunto.NavigateUrl + "&Size=132";

                    }
                    else
                    {
                        AvisoImage.Visible = false;
                    }

                }

                Label lblnombreapellido = (Label)FVDetalleClasificado.FindControl("LabelNombre");
                //Panel pnl = (Panel)FVDetalleClasificado.FindControl("pnlUsuario");
                //pnl.Attributes.Add("style", "display:none");
                //Literal ltjs = (Literal)FVDetalleClasificado.FindControl("ltjs");
                //ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
                HiddenField AvisoIDHiddenField = (HiddenField)FVDetalleClasificado.FindControl("AvisoIDHiddenField");



                Image imgUsuario = FVDetalleClasificado.FindControl("imgUsuario") as Image;
                if (imgUsuario != null)
                    imgUsuario.ImageUrl = "~/images/personas/" + ru.UsuarioFoto;
            }
            if (FVDetalleClasificado.CurrentMode == FormViewMode.Edit)
            {
                TextBox NombreTextBox = FVDetalleClasificado.FindControl("NombreTextBox") as TextBox;
                NombreTextBox.Text = this.ObjectUsuario.nombre + " " + this.ObjectUsuario.apellido;
                HiddenField DiasVigenciaHiddenField = (HiddenField)FVDetalleClasificado.FindControl("DiasVigenciaHiddenField");
                HiddenField FHPublicacionHiddenField = (HiddenField)FVDetalleClasificado.FindControl("FHPublicacionHiddenField");

                if (!string.IsNullOrEmpty(DiasVigenciaHiddenField.Value))
                {
                    double DiasVigencia = double.Parse(DiasVigenciaHiddenField.Value);
                    TextBox vencimiento_TextBox = (TextBox)FVDetalleClasificado.FindControl("vencimiento_TextBox");
                    DateTime FHPublicacion = DateTime.Parse(FHPublicacionHiddenField.Value);
                    vencimiento_TextBox.Text = FHPublicacion.AddDays(DiasVigencia).ToString("dd/MM/yyy");
                }


                string seleccionado = string.Empty;

                HiddenField HabilitadoHiddenField = (HiddenField)FVDetalleClasificado.FindControl("HabilitadoHiddenField");
                RadioButtonList EstadoRadioButtonList = (RadioButtonList)FVDetalleClasificado.FindControl("EstadoRadioButtonList");
                // ListItem LI = EstadoRadioButtonList.Items.FindByValue(HabilitadoHiddenField.Value.ToUpper()=="TRUE"? "1":"0");
                // if(LI!=null)
                //     LI.Selected = true;
                RadioButtonList RBLHabilitado = (RadioButtonList)FVDetalleClasificado.FindControl("EstadoRadioButtonList");
                HiddenField HFHabilitado = (HiddenField)FVDetalleClasificado.FindControl("HabilitadoHiddenField");
                if (HFHabilitado.Value.ToUpper() == "TRUE")
                {
                    seleccionado = "1";
                }
                else
                {
                    seleccionado = "0";
                }
                RBLHabilitado.SelectedValue = seleccionado;//HFHabilitado.Value.ToString();                

            }
        }

        protected void FVDetalleClasificado_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            TextBox txtComentario = FVDetalleClasificado.FindControl("txtComentario") as TextBox;
            DataList dtComments = FVDetalleClasificado.FindControl("dtComments") as DataList;
            if (e.CommandName == "Comentar")
            {
                string Page = Request.Url.ToString() + string.Format("?AvisoID={0}#comentarios-abajo", e.CommandArgument.ToString());
                CC.InsertUSuarioComentario(this.ObjectUsuario.usuarioid, int.Parse(e.CommandArgument.ToString()), txtComentario.Text, Page);

                txtComentario.Text = "";
                dtComments.DataBind();
            }
        }

        protected void FVDetalleClasificado_ItemInserting(object sender, FormViewInsertEventArgs e)
        {


        }
        protected void FVDetalleClasificado_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            FileUpload ArchivoAdjuntoFileUpload = FVDetalleClasificado.FindControl("ArchivoAdjuntoEditFileUpload") as FileUpload;

            if (ArchivoAdjuntoFileUpload.HasFile)
            {
                string FileName = Path.GetFileNameWithoutExtension(ArchivoAdjuntoFileUpload.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(ArchivoAdjuntoFileUpload.FileName);
                string strPath = MapPath("~/Clasificados/archivos/") + FileName;
                ArchivoAdjuntoFileUpload.SaveAs(strPath);
                e.NewValues["ArchivoAdjunto"] = FileName;
            }
            else
            {
                HiddenField ArchivoAdjuntoHiddenField = FVDetalleClasificado.FindControl("ArchivoAdjuntoHiddenField") as HiddenField;
                e.NewValues["ArchivoAdjunto"] = ArchivoAdjuntoHiddenField.Value;
            }

            TextBox vencimiento_TextBox = FVDetalleClasificado.FindControl("vencimiento_TextBox") as TextBox;
            e.NewValues["FechaVencimiento"] = vencimiento_TextBox.Text;
            RadioButtonList EstadoRadioButtonList = FVDetalleClasificado.FindControl("EstadoRadioButtonList") as RadioButtonList;
            e.NewValues["Habilitado"] = EstadoRadioButtonList.Items[0].Selected;

            TextBox tb = FVDetalleClasificado.FindControl("LinkTextBox") as TextBox;
            // http://
            if (tb.Text.Length > 7)
            {

                if (tb.Text.ToString().Substring(0, 7) != "http://")
                {

                    tb.Text = "http://" + tb.Text;

                }
            }
            e.NewValues["Link"] = tb.Text.ToString();
        }

        protected void FVDetalleClasificado_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            //AvisosTabContainer.ActiveTabIndex = 2;

            FVDetalleClasificado.DataBind();
            FVDetalleClasificado.ChangeMode(FormViewMode.ReadOnly);
        }



    }
}