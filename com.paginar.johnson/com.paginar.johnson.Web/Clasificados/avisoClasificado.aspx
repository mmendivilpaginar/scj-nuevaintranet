﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="avisoClasificado.aspx.cs" Inherits="com.paginar.johnson.Web.Clasificados.avisoClasificado" %>
 
<%@ Register Assembly="com.paginar.johnson.Web" Namespace="com.paginar.johnson.Web"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">


                        <div id="FormDetalleAviso">
                            <asp:HiddenField ID="AvisoIDDetalleHiddenField" runat="server" />
                            <asp:HiddenField ID="UsrAltaHiddenField" runat="server" />
                            <h3 class="hide">
                                Detalle de búsqueda</h3>
                            <div class="box resultados">
                                <asp:FormView ID="FVDetalleClasificado" runat="server" DataKeyNames="AvisoID" DataSourceID="ODSAvisoDetalle"
                                    OnDataBound="FVDetalleClasificado_DataBound" OnItemCommand="FVDetalleClasificado_ItemCommand"
                                    OnItemInserting="FVDetalleClasificado_ItemInserting" OnItemUpdating="FVDetalleClasificado_ItemUpdating"
                                    OnItemUpdated="FVDetalleClasificado_ItemUpdated">
                                    <EditItemTemplate>
                                        <asp:HiddenField runat="server" ID="AvisoID" Value='<%# Bind("AvisoID") %>' />
                                        <asp:HiddenField ID="DiasVigenciaHiddenField" runat="server" Value='<%# Eval("DiasVigencia") %>' />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Actualizar" />
                                        <div class="form-item leftHalf">
                                            <asp:HiddenField ID="FHPublicacionHiddenField" runat="server" Value='<%# Eval("FHPublicacion") %>' />
                                            <label>
                                                Usuario:</label>
                                            <asp:HiddenField ID="ArchivoAdjuntoHiddenField" runat="server" Value='<%# Eval("ArchivoAdjunto") %>' />
                                            <asp:TextBox ID="NombreTextBox" Enabled="false" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                F. Publicación:</label>
                                            <asp:TextBox ID="FHAltaTextBox" Enabled="false" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />
                                        </div>
                                        <div class="form-item leftHalf">
                                            <label>
                                                Tipo Aviso:</label>
                                            <asp:RadioButtonList ID="TipoAvisoIDRadioButtonList" runat="server" DataSourceID="ODSTiposAvisos"
                                                DataTextField="TipoAvisoDet" DataValueField="TipoAvisoID" SelectedValue='<%# Bind("TipoAvisoID") %>'
                                                RepeatDirection="Horizontal">
                                            </asp:RadioButtonList>
                                            <asp:ObjectDataSource ID="ODSTiposAvisos" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.claTipoAvisoTableAdapter"
                                                UpdateMethod="Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                                </InsertParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ControlToValidate="TipoAvisoIDRadioButtonList"
                                                runat="server" Text="*" ErrorMessage="Seleccionar un valor para el campo Tipo de Aviso"
                                                ValidationGroup="Insertar"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                Rubro:</label>
                                            <asp:DropDownList ID="RubroIDDropDownList" runat="server" DataSourceID="ODSRubros"
                                                DataTextField="Descrip" DataValueField="RubroID" SelectedValue='<%# Bind("RubroID") %>'
                                                AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Text="Seleccionar"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:ObjectDataSource ID="ODSRubros" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.clasiRubroTableAdapter"
                                                UpdateMethod="Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                                </InsertParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="reqddlcnt" runat="server" Display="Dynamic" ControlToValidate="RubroIDDropDownList"
                                                Text="*" ErrorMessage="Seleccionar un valor en el campo Rubro" InitialValue="-1"
                                                ValidationGroup="Actualizar"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item full">
                                            <label>
                                                Referencia:</label>
                                            <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                ControlToValidate="TituloTextBox" ErrorMessage="Ingrese un valor en el campo Referencia"
                                                ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Descripción:</label>
                                            <asp:TextBox TextMode="MultiLine" ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Adjuntar Archivo:</label>
                                            <asp:FileUpload ID="ArchivoAdjuntoEditFileUpload" runat="server" />
                                            <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" ImageUrl="~/css/images/attach.png"
                                                NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>' Target="_blank"
                                                Text='<%# Eval("ArchivoAdjunto") %>' Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                ControlToValidate="DescripTextBox" ErrorMessage="Ingrese un valor en el campo Descripción "
                                                ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                            <br />
                                            <asp:Label ID="Label6" runat="server" Font-Size="Smaller" Text="(Hasta 5 MB)"></asp:Label>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Link:</label>
                                            <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' />
<%--                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"
                                                ControlToValidate="LinkTextBox" ErrorMessage="El formato del campo Link es incorrecto"
                                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>--%>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Fecha de Vencimiento:</label>
                                            <asp:TextBox ID="vencimiento_TextBox" runat="server" SkinID="form-date"></asp:TextBox>
                                            <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                                            <asp:CalendarExtender ID="vencimiento_CalendarExtender" runat="server" Enabled="True"
                                                PopupButtonID="IMGCalen1" TargetControlID="vencimiento_TextBox" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="El formato de la fecha ingresada en Fecha de vencimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                                Operator="DataTypeCheck" Type="Date" ValidationGroup="Actualizar">*</asp:CompareValidator>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="La fecha de vencimiento debe ser mayor a la fecha actual" ValidationGroup="Actualizar"
                                                OnServerValidate="CustomValidator1_ServerValidate1">*</asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="Ingrese un valor en el campo Vencimiento" ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item leftHalf">
                                            <label>
                                                Email Alternativo:</label>
                                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                                ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                Tel. Alternativo:</label>
                                            <asp:TextBox ID="TelefonoTextBox" runat="server" Text='<%# Bind("Telefono") %>' />
                                        </div>
                                        <div class="form-item full">
                                            <label>
                                                Estado:</label>
                                            <asp:HiddenField ID="HabilitadoHiddenField" runat="server" 
                                                Value='<%# Eval("Habilitado") %>' />
                                            <asp:RadioButtonList ID="EstadoRadioButtonList" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="1">Habilitado</asp:ListItem>
                                                <asp:ListItem Value="0">Deshabilitado</asp:ListItem>
                                            </asp:RadioButtonList>
                                            
                                        </div>
                                        <p>
                                            Si no encuentra el rubro, por favor contactarse con Recursos Humanos</p>
                                        <div class="controls">
                                            <asp:Button ID="EditButton" runat="server" CausesValidation="True" CommandName="Update"
                                                Text="Actualizar" ValidationGroup="Actualizar" />&nbsp;<asp:Button ID="Button1" runat="server"
                                                    CommandArgument='<%# Eval("AvisoID") %>' CommandName="Delete" OnClientClick="return confirm('¿Está seguro que desea eliminar el registro?')"
                                                    Text="Eliminar" />
                                            <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancelar" />
                                        </div>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <div class="bloque">
                                            <asp:HiddenField ID="RubroIDHiddenField" runat="server" Value='<%# Eval("RubroID") %>' />
                                            <h4>
                                                <asp:Label ID="RubroDescLabel" runat="server" Text='<%# Eval("RubroDesc") %>' />
                                                -
                                                <asp:Label ID="TipoAvisoDetLabel" runat="server" Text='<%# Eval("TipoAvisoDet") %>' /></h4>
                                            <div class="container">
                                                <div class="content">
                                                    <div class="userCard">
                                                        <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                        <div class="foto">
                                                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>' />
                                                        </div>
                                                        <div class="info">
                                                            <strong>PUBLICADO POR</strong><br />
                                                            <strong>
                                                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("NombreApellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                int:
                                                                <%# Eval("interno")%></strong>
                                                            <br />
                                                            <%# Eval("cargodet")%>,
                                                            <%# Eval("areadesc")%>,<br />
                                                            <%# Eval("DireccionDET")%>,
                                                           <%# Eval("UbicacionDET")%>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                                                        </div>
                                                    </div>
                                                    <div class="detailAviso">
                                                        <ul>
                                                            <li class="first"></li>
                                                            <li>
                                                                <h3>
                                                                    <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                        CommandName="Detalle" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLinkLink" runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                                    Text='<%# Eval("Link") %>' Target="_blank"></asp:HyperLink>
                                                            </li>
                                                            <li class="last">
                                                                <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>'
                                                                    Target="_blank" Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'> <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                                            </li>
                                                        </ul>
                                                        <asp:Image ID="AvisoImage" Visible="false" runat="server" CssClass="floatRight" /><br />
                                                        <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("Descrip").ToString().Replace("\r\n","<br>") %>' />
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <div class="comments">
                                                        <%--<asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                                            ToolTip="Ver Comentarios" CommandArgument='<%# Eval("AvisoID") %>' CommandName="VERCOMENTARIOS" />--%>
                                                        <asp:Label ID="lblConComents" runat="server" Visible="false"></asp:Label>
                                                        <%--comentarios<br />
                                                        <br />--%>
                                                    </div>
                                                    <asp:HiddenField ID="AvisoHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="AvisoIDHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                    <%--PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' --%>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/bloque-->
                                        <div class="box comentarios">
                                            <h4>
                                                Comentarios</h4>
                                            <a name="comentarios-abajo"></a>
                                            <div class="usuario-comentar">
                                                <asp:Image ID="imgUsuario" runat="server" CssClass="hide" />
                                                <div class="form-item full">
                                                    <label>
                                                        Descripción:
                                                    </label>
                                                    <asp:TextBox ID="txtComentario" runat="server" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComentario"
                                                        Text="*" ValidationGroup="Comentar"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="controls">
                                                    <asp:Button ID="btnComentar" runat="server" Text="Comentar" ValidationGroup="Comentar"
                                                        CommandArgument='<%# Eval("AvisoID") %>' CommandName="Comentar" />
                                                </div>
                                            </div>
                                            <!--/usuario-comentar-->
                                            <div class="lista-comentarios">
                                                <asp:DataList ID="dtComments" runat="server" DataKeyField="comentarioID" DataSourceID="ODSComentarios"
                                                    RepeatLayout="Flow">
                                                    <ItemTemplate>
                                                        <div class="comentario">
                                                            <div class="pic">
                                                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif" %>'
                                                                    ToolTip='<%# Eval("nombrecompleto") %>' />
                                                            </div>
                                                            <div class="content">
                                                                <p>
                                                                    <asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                                                <p>
                                                                    Fecha:
                                                                    <asp:Label ID="FechaLabel" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy  - hh:MM tt}") %>' /></p>
                                                                <p>
                                                                    Comentario:
                                                                    <asp:Label ID="comentariosLabel" runat="server" Text='<%# Eval("comentarios").ToString().Replace("\r\n","<br>") %>' /></p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                            <!--/lista-comentarios-->
                                            <asp:ObjectDataSource ID="ODSComentarios" runat="server" InsertMethod="InsertarClasificado"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="getComentariosByAvisoID"
                                                TypeName="com.paginar.johnson.BL.ControllerClasificados" UpdateMethod="UpdateClasificado">
                                                <InsertParameters>
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Titulo" Type="String" />
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                                    <asp:Parameter Name="Link" Type="String" />
                                                    <asp:Parameter Name="Email" Type="String" />
                                                    <asp:Parameter Name="Telefono" Type="String" />
                                                </InsertParameters>
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="AvisoHiddenField" Name="AvisoID" PropertyName="Value"
                                                        Type="Int32" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                            <asp:ObjectDataSource ID="ODSAvisoDetalle" runat="server" InsertMethod="InsertarClasificado"
                                SelectMethod="GetClasificadoByID" TypeName="com.paginar.johnson.BL.ControllerClasificados"
                                UpdateMethod="UpdateClasificado" DeleteMethod="DeleteAvisoByID">
                                <DeleteParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="AvisoIDDetalleHiddenField" Name="AvisoID" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                    <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                                    <asp:Parameter Name="Habilitado" Type="Boolean" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>

                  
</asp:Content>
