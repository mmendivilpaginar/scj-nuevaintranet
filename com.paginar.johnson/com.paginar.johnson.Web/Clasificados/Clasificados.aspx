﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" ResponseEncoding="iso-8859-1"
    AutoEventWireup="true" CodeBehind="Clasificados.aspx.cs" Inherits="com.paginar.johnson.Web.Clasificados.Clasificados"  %>
   
    
<%@ Register Assembly="com.paginar.johnson.Web" Namespace="com.paginar.johnson.Web"
    TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Clasificados</h2>
    <asp:HiddenField ID="UsrAltaHiddenField" runat="server" />
    <asp:TabContainer ID="AvisosTabContainer" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="BUSCADOR" ID="Panel1" runat="server" TabIndex="0">
            <HeaderTemplate>
                BUSCADOR
            </HeaderTemplate>
            <ContentTemplate>
                <a name="DetalleAviso-abajo"></a>
                <div class="box formulario">
                    <asp:Panel ID="pnlBuscador" runat="server" DefaultButton="ButtonBuscar">
                        <div class="form-item leftHalf">
                            <label>
                                País</label>
                            <asp:DropDownList ID="DropDownListClusters" runat="server" DataSourceID="ODSClusters"
                                DataTextField="Descripcion" DataValueField="ID" AutoPostBack="True" 
                                AppendDataBoundItems="True" 
                                onselectedindexchanged="DropDownListClusters_SelectedIndexChanged">
                                <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ODSClusters" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.ClusterTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="ID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Planta</label>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:DropDownList ID="DropDownListPlantas" runat="server" DataSourceID="ODSPlantas"
                                        DataTextField="Descripcion" DataValueField="UbicacionID" 
                                        OnDataBound="DropDownListPlantas_DataBound" Enabled="False">
                                        <asp:ListItem Text="Todos" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:ObjectDataSource ID="ODSPlantas" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByClusterID"
                                        TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.UbicacionTableAdapter"
                                        UpdateMethod="Update">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Original_UbicacionID" Type="String" />
                                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                                            <asp:Parameter Name="Original_Tramites" Type="Boolean" />
                                            <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                            <asp:Parameter Name="Original_clusterID" Type="Int32" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="UbicacionID" Type="String" />
                                            <asp:Parameter Name="Descripcion" Type="String" />
                                            <asp:Parameter Name="Tramites" Type="Boolean" />
                                            <asp:Parameter Name="DireccionID" Type="Int32" />
                                            <asp:Parameter Name="clusterID" Type="Int32" />
                                        </InsertParameters>
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="DropDownListClusters" Name="ClusterID" PropertyName="SelectedValue"
                                                Type="Int32" />
                                        </SelectParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="Descripcion" Type="String" />
                                            <asp:Parameter Name="Tramites" Type="Boolean" />
                                            <asp:Parameter Name="DireccionID" Type="Int32" />
                                            <asp:Parameter Name="clusterID" Type="Int32" />
                                            <asp:Parameter Name="Original_UbicacionID" Type="String" />
                                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                                            <asp:Parameter Name="Original_Tramites" Type="Boolean" />
                                            <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                            <asp:Parameter Name="Original_clusterID" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="DropDownListClusters" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Rubro</label>
                            <asp:DropDownList ID="DropDownListRubros" runat="server" DataSourceID="ODSRubros"
                                DataTextField="Descrip" DataValueField="RubroID" AppendDataBoundItems="True">
                                <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ODSRubros" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.clasiRubroTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Tipo de Aviso:</label>
                            <asp:DropDownList ID="DropDownListTipoAviso" runat="server" DataSourceID="ODSTipoAviso"
                                DataTextField="TipoAvisoDet" DataValueField="TipoAvisoID" OnDataBound="DropDownListTipoAviso_DataBound">
                                <asp:ListItem Value="-1" Text="Todos"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ODSTipoAviso" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.claTipoAvisoTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item full">
                            <label>
                                Buscar:</label>
                            <asp:TextBox ID="TextBoxBuscar" runat="server" ></asp:TextBox>
                        </div>
                        <div class="controls">
                            <asp:Button ID="ButtonBuscar" runat="server" Text="Buscar" OnClick="ButtonBuscar_Click" />
                        </div>
                    </asp:Panel>
                </div>
                <!--/box-->
                <asp:MultiView ID="MVBusqueda" runat="server">
                    <asp:View ID="VResultado" runat="server">
                        <div class="box resultados">
                            <h3 class="hide">
                                Resultados de búsqueda</h3>
                            <asp:HiddenField ID="LimitHiddenField" runat="server" Value="true" />                           
                            <asp:DataList ID="ClasificadosDL" runat="server" DataKeyField="AvisoID" DataSourceID="ODSClasificados"
                                            OnItemDataBound="ClasificadosDL_ItemDataBound" OnItemCommand="DLAvisosRubro_ItemCommand"
                                            RepeatLayout="Flow">
                                            <FooterTemplate>
                                                <asp:Label ID="Label7" runat="server" CssClass="messages msg-info" 
                                                    Text="La búsqueda no produjo resultados para los parámetros indicados."
                                                    Visible="<%#bool.Parse(((ClasificadosDL.Items.Count==0)&&(GetPostBackControl(Page)!=null)&&(GetPostBackControl(Page).ID == ButtonBuscar.ID)).ToString())%>"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <div class="container">
                                                    <div class="content">
                                                        <div class="userCard">
                                                            <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                            <div class="foto">
                                                                <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>' />
                                                            </div>
                                                            <div class="info">
                                                                <strong>PUBLICADO POR:</strong><br />
                                                                <strong>
                                                                    <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("NombreApellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                    Interno:
                                                                    <%# Eval("interno")%></strong>
                                                                <br />
                                                                <%# Eval("cargodet")%>,
                                                                <%# Eval("areadesc")%><br />
                                                                <%# Eval("DireccionDET")%>,
                                                                <%# Eval("UbicacionDET")%>
                                                                <asp:Image ID="imgBandera" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                                                            </div>
                                                        </div>
                                                        <div class="detailAviso">
                                                            <ul>
                                                                <li class="first"></li>
                                                                <li>
                                                                    <h3>
                                                                        <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                            CommandName="Detalle" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLinkLink" runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                                        Text='<%# Eval("Link") %>' Target="_blank"></asp:HyperLink>
                                                                </li>
                                                                <li class="last">
                                                                    <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>'
                                                                        Target="_blank" Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'> <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                            <asp:Image ID="AvisoImage" Visible="false" runat="server" CssClass="floatRight" /><br />
                                                            <asp:Label ID="DescripLabel" runat="server" Text='<%# truncarTexto(Eval("Descrip").ToString().Replace("\r\n",""),(int)Eval("AvisoID")) %>' />
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <div class="comments">
                                                            <asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                                                ToolTip="Ver Comentarios" CommandArgument='<%# Eval("AvisoID") %>' CommandName="VERCOMENTARIOS" />
                                                            <asp:Label ID="lblConComents" runat="server"></asp:Label>
                                                            comentarios<br />
                                                            <br />
                                                            <asp:LinkButton ID="EditarLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                CommandName="EditarAviso">
                                                                <asp:Image runat="server" ID="imgEditar" ImageUrl="~/images/page_edit.png" AlternateText="Editar" />
                                                                Editar</asp:LinkButton>
                                                        </div>
                                                        <asp:HiddenField ID="AvisoIDHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                        <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                        <%--PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' --%>
                                                    </div>
                                                </div>
                                                <!--/container-->   
                                            </ItemTemplate>
                                        </asp:DataList>
                            <asp:ObjectDataSource ID="ODSClasificados" runat="server" OldValuesParameterFormatString="original_{0}"
                                OnSelecting="ODSClasificados_Selecting" SelectMethod="getBusquedaFE" TypeName="com.paginar.johnson.BL.ControllerClasificados"
                                OnObjectCreating="ODSClasificados_ObjectCreating" OnObjectDisposing="ODSClasificados_ObjectDisposing"
                                DeleteMethod="DeleteAvisoByID" InsertMethod="InsertarClasificado" UpdateMethod="UpdateClasificado">
                                <DeleteParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                    <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                                    <asp:Parameter Name="Habilitado" Type="Boolean" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="TextBoxBuscar" Name="Texto" PropertyName="Text"
                                        Type="String" />
                                    <asp:ControlParameter ControlID="DropDownListClusters" Name="ClusterID" PropertyName="SelectedValue"
                                        Type="Int32" />
                                    <asp:ControlParameter ControlID="DropDownListPlantas" Name="UbicacionID" PropertyName="SelectedValue"
                                        Type="Int32" />
                                    <asp:ControlParameter ControlID="DropDownListRubros" Name="RubroID" PropertyName="SelectedValue"
                                        Type="Int32" />
                                    <asp:ControlParameter ControlID="DropDownListTipoAviso" Name="TipoAvisoID" PropertyName="SelectedValue"
                                        Type="Int32" />
                                    <asp:ControlParameter ControlID="LimitHiddenField" Name="limit" PropertyName="Value"
                                        Type="Boolean" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                    <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                                    <asp:Parameter Name="Habilitado" Type="Boolean" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <!--/box-->
                    </asp:View>
                    <asp:View ID="VDetalleAviso" runat="server">
                        <div id="FormDetalleAviso">
                            <asp:HiddenField ID="AvisoIDDetalleHiddenField" runat="server" />
                            <h3 class="hide">
                                Detalle de búsqueda</h3>
                            <div class="box resultados">
                                <asp:FormView ID="FVDetalleClasificado" runat="server" DataKeyNames="AvisoID" DataSourceID="ODSAvisoDetalle"
                                    OnDataBound="FVDetalleClasificado_DataBound" OnItemCommand="FVDetalleClasificado_ItemCommand"
                                    OnItemInserting="FVDetalleClasificado_ItemInserting" OnItemUpdating="FVDetalleClasificado_ItemUpdating"
                                    OnItemUpdated="FVDetalleClasificado_ItemUpdated" OnItemDeleted="FVDetalleClasificado_ItemDeleted">
                                    <EditItemTemplate>
                                        <asp:HiddenField runat="server" ID="AvisoID" Value='<%# Bind("AvisoID") %>' />
                                        <asp:HiddenField ID="DiasVigenciaHiddenField" runat="server" Value='<%# Eval("DiasVigencia") %>' />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Actualizar" />
                                        <div class="form-item leftHalf">
                                            <asp:HiddenField ID="FHPublicacionHiddenField" runat="server" Value='<%# Eval("FHPublicacion") %>' />
                                            <label>
                                                Usuario:</label>
                                            <asp:HiddenField ID="ArchivoAdjuntoHiddenField" runat="server" Value='<%# Eval("ArchivoAdjunto") %>' />
                                            <asp:TextBox ID="NombreTextBox" Enabled="false" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                F. Publicación:</label>
                                            <asp:TextBox ID="FHAltaTextBox" Enabled="false" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />
                                        </div>
                                        <div class="form-item leftHalf">
                                            <label>
                                                Tipo Aviso:</label>
                                            <asp:RadioButtonList ID="TipoAvisoIDRadioButtonList" runat="server" DataSourceID="ODSTiposAvisos"
                                                DataTextField="TipoAvisoDet" DataValueField="TipoAvisoID" SelectedValue='<%# Bind("TipoAvisoID") %>'
                                                RepeatDirection="Horizontal">
                                            </asp:RadioButtonList>
                                            <asp:ObjectDataSource ID="ODSTiposAvisos" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.claTipoAvisoTableAdapter"
                                                UpdateMethod="Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                                </InsertParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                                    <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ControlToValidate="TipoAvisoIDRadioButtonList"
                                                runat="server" Text="*" ErrorMessage="Seleccionar un valor para el campo Tipo de Aviso"
                                                ValidationGroup="Insertar"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                Rubro:</label>
                                            <asp:DropDownList ID="RubroIDDropDownList" runat="server" DataSourceID="ODSRubros"
                                                DataTextField="Descrip" DataValueField="RubroID" SelectedValue='<%# Bind("RubroID") %>'
                                                AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Text="Seleccionar"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:ObjectDataSource ID="ODSRubros" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.clasiRubroTableAdapter"
                                                UpdateMethod="Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                                </InsertParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="FHMod" Type="DateTime" />
                                                    <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Original_Descrip" Type="String" />
                                                    <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                                    <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="reqddlcnt" runat="server" Display="Dynamic" ControlToValidate="RubroIDDropDownList"
                                                Text="*" ErrorMessage="Seleccionar un valor en el campo Rubro" InitialValue="-1"
                                                ValidationGroup="Actualizar"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item full">
                                            <label>
                                                Referencia:</label>
                                            <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                ControlToValidate="TituloTextBox" ErrorMessage="Ingrese un valor en el campo Referencia"
                                                ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Descripción:</label>
                                            <asp:TextBox TextMode="MultiLine" ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Adjuntar Archivo:</label>
                                            <asp:FileUpload ID="ArchivoAdjuntoEditFileUpload" runat="server" />
                                            <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" ImageUrl="~/css/images/attach.png"
                                                NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>' Target="_blank"
                                                Text='<%# Eval("ArchivoAdjunto") %>' Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                ControlToValidate="DescripTextBox" ErrorMessage="Ingrese un valor en el campo Descripción "
                                                ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                            <br />
                                            <asp:Label ID="Label6" runat="server" Font-Size="Smaller" Text="(Hasta 5 MB)"></asp:Label>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Link:</label>
                                            <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' />
<%--                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"
                                                ControlToValidate="LinkTextBox" ErrorMessage="El formato del campo Link es incorrecto"
                                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>--%>
                                        </div>
                                        <div class="form-item">
                                            <label>
                                                Fecha de Vencimiento:</label>
                                            <asp:TextBox ID="vencimiento_TextBox" runat="server" SkinID="form-date"></asp:TextBox>
                                            <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                                            <asp:CalendarExtender ID="vencimiento_CalendarExtender" runat="server" Enabled="True"
                                                PopupButtonID="IMGCalen1" TargetControlID="vencimiento_TextBox" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="El formato de la fecha ingresada en Fecha de vencimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                                Operator="DataTypeCheck" Type="Date" ValidationGroup="Actualizar">*</asp:CompareValidator>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="La fecha de vencimiento debe ser mayor a la fecha actual" ValidationGroup="Actualizar"
                                                OnServerValidate="CustomValidator1_ServerValidate1">*</asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="vencimiento_TextBox"
                                                ErrorMessage="Ingrese un valor en el campo Vencimiento" ValidationGroup="Actualizar">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-item leftHalf">
                                            <label>
                                                Email Alternativo:</label>
                                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                                ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>
                                        </div>
                                        <div class="form-item rightHalf">
                                            <label>
                                                Tel. Alternativo:</label>
                                            <asp:TextBox ID="TelefonoTextBox" runat="server" Text='<%# Bind("Telefono") %>' />
                                        </div>
                                        <div class="form-item full">
                                            <label>
                                                Estado:</label>
                                            <asp:HiddenField ID="HabilitadoHiddenField" runat="server" 
                                                Value='<%# Eval("Habilitado") %>' />
                                            <asp:RadioButtonList ID="EstadoRadioButtonList" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Value="1">Habilitado</asp:ListItem>
                                                <asp:ListItem Value="0">Deshabilitado</asp:ListItem>
                                            </asp:RadioButtonList>
                                            
                                        </div>
                                        <p>
                                            Si no encuentra el rubro, por favor contactarse con Recursos Humanos</p>
                                        <div class="controls">
                                            <asp:Button ID="EditButton" runat="server" CausesValidation="True" CommandName="Update"
                                                Text="Actualizar" ValidationGroup="Actualizar" />&nbsp;<asp:Button ID="Button1" runat="server"
                                                    CommandArgument='<%# Eval("AvisoID") %>' CommandName="Delete" OnClientClick="return confirm('¿Está seguro que desea eliminar el registro?')"
                                                    Text="Eliminar" />
                                            <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancelar" />
                                        </div>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <div class="bloque">
                                            <asp:HiddenField ID="RubroIDHiddenField" runat="server" Value='<%# Eval("RubroID") %>' />
                                            <h4>
                                                <asp:Label ID="RubroDescLabel" runat="server" Text='<%# Eval("RubroDesc") %>' />
                                                -
                                                <asp:Label ID="TipoAvisoDetLabel" runat="server" Text='<%# Eval("TipoAvisoDet") %>' /></h4>
                                            <div class="container">
                                                <div class="content">
                                                    <div class="userCard">
                                                        <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                        <div class="foto">
                                                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>' />
                                                        </div>
                                                        <div class="info">
                                                            <strong>PUBLICADO POR</strong><br />
                                                            <strong>
                                                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("NombreApellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                int:
                                                                <%# Eval("interno")%></strong>
                                                            <br />
                                                            <%# Eval("cargodet")%>,
                                                            <%# Eval("areadesc")%>,<br />
                                                            <%# Eval("DireccionDET")%>,
                                                           <%# Eval("UbicacionDET")%>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                                                        </div>
                                                    </div>
                                                    <div class="detailAviso">
                                                        <ul>
                                                            <li class="first"></li>
                                                            <li>
                                                                <h3>
                                                                    <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                        CommandName="Detalle" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLinkLink" runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                                    Text='<%# Eval("Link") %>' Target="_blank"></asp:HyperLink>
                                                            </li>
                                                            <li class="last">
                                                                <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>'
                                                                    Target="_blank" Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'> <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                                            </li>
                                                        </ul>
                                                        <asp:Image ID="AvisoImage" Visible="false" runat="server" CssClass="floatRight" /><br />
                                                        <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("Descrip").ToString().Replace("\r\n","<br>") %>' />
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <div class="comments">
                                                        <%--<asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                                            ToolTip="Ver Comentarios" CommandArgument='<%# Eval("AvisoID") %>' CommandName="VERCOMENTARIOS" />--%>
                                                        <asp:Label ID="lblConComents" runat="server" Visible="false"></asp:Label>
                                                        <%--comentarios<br />
                                                        <br />--%>
                                                    </div>
                                                    <asp:HiddenField ID="AvisoHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="AvisoIDHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                    <%--PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' --%>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/bloque-->
                                        <div class="box comentarios">
                                            <h4>
                                                Comentarios</h4>
                                            <a name="comentarios-abajo"></a>
                                            <div class="usuario-comentar">
                                                <asp:Image ID="imgUsuario" runat="server" CssClass="hide" />
                                                <div class="form-item full">
                                                    <label>
                                                        Descripción:
                                                    </label>
                                                    <asp:TextBox ID="txtComentario" runat="server" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComentario"
                                                        Text="*" ValidationGroup="Comentar"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="controls">
                                                    <asp:Button ID="btnComentar" runat="server" Text="Comentar" ValidationGroup="Comentar"
                                                        CommandArgument='<%# Eval("AvisoID") %>' CommandName="Comentar" />
                                                </div>
                                            </div>
                                            <!--/usuario-comentar-->
                                            <div class="lista-comentarios">
                                                <asp:DataList ID="dtComments" runat="server" DataKeyField="comentarioID" DataSourceID="ODSComentarios"
                                                    RepeatLayout="Flow">
                                                    <ItemTemplate>
                                                        <div class="comentario">
                                                            <div class="pic">
                                                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif" %>'
                                                                    ToolTip='<%# Eval("nombrecompleto") %>' />
                                                            </div>
                                                            <div class="content">
                                                                <p>
                                                                    <asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                                                <p>
                                                                    Fecha:
                                                                    <asp:Label ID="FechaLabel" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy  - hh:MM tt}") %>' /></p>
                                                                <p>
                                                                    Comentario:
                                                                    <asp:Label ID="comentariosLabel" runat="server" Text='<%# Eval("comentarios").ToString().Replace("\r\n","<br>") %>' /></p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                            <!--/lista-comentarios-->
                                            <asp:ObjectDataSource ID="ODSComentarios" runat="server" InsertMethod="InsertarClasificado"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="getComentariosByAvisoID"
                                                TypeName="com.paginar.johnson.BL.ControllerClasificados" UpdateMethod="UpdateClasificado">
                                                <InsertParameters>
                                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                                    <asp:Parameter Name="Titulo" Type="String" />
                                                    <asp:Parameter Name="Descrip" Type="String" />
                                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                                    <asp:Parameter Name="Link" Type="String" />
                                                    <asp:Parameter Name="Email" Type="String" />
                                                    <asp:Parameter Name="Telefono" Type="String" />
                                                </InsertParameters>
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="AvisoHiddenField" Name="AvisoID" PropertyName="Value"
                                                        Type="Int32" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                                </UpdateParameters>
                                            </asp:ObjectDataSource>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                            <asp:ObjectDataSource ID="ODSAvisoDetalle" runat="server" InsertMethod="InsertarClasificado"
                                SelectMethod="GetClasificadoByID" TypeName="com.paginar.johnson.BL.ControllerClasificados"
                                UpdateMethod="UpdateClasificado" DeleteMethod="DeleteAvisoByID">
                                <DeleteParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="AvisoIDDetalleHiddenField" Name="AvisoID" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AvisoID" Type="Int32" />
                                    <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                    <asp:Parameter Name="RubroID" Type="Int32" />
                                    <asp:Parameter Name="Titulo" Type="String" />
                                    <asp:Parameter Name="Descrip" Type="String" />
                                    <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                    <asp:Parameter Name="Link" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Telefono" Type="String" />
                                    <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                                    <asp:Parameter Name="Habilitado" Type="Boolean" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Publicar Aviso" ID="TabPanel1" runat="server" TabIndex="1">
            <ContentTemplate>
                <asp:HiddenField ID="AvisoIDHiddenField" Value="1" runat="server" />
                <asp:MultiView ID="MVCABM" runat="server">
                    <asp:View ID="VABM" runat="server">
                        <asp:FormView ID="FVClasificado" runat="server" DataKeyNames="AvisoID" DataSourceID="ODSClasificado"
                            DefaultMode="Insert" OnDataBound="FormView1_DataBound" OnItemInserting="FVClasificado_ItemInserting"
                            OnItemInserted="FVClasificado_ItemInserted">
                            <InsertItemTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Insertar" />
                                <div class="form-item leftHalf">
                                    <label>
                                        Usuario:</label>
                                    <asp:TextBox ID="NombreTextBox" Enabled="false" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-item rightHalf">
                                    <label>
                                        F. Publicación:</label>
                                    <asp:TextBox ID="FHAltaTextBox" Enabled="false" runat="server" Text='<%# Bind("FHAlta") %>' />
                                </div>
                                <div class="form-item leftHalf">
                                    <label>
                                        Tipo Aviso:</label>
                                    <asp:RadioButtonList ID="TipoAvisoIDRadioButtonList" runat="server" DataSourceID="ODSTiposAvisos"
                                        DataTextField="TipoAvisoDet" DataValueField="TipoAvisoID" SelectedValue='<%# Bind("TipoAvisoID") %>'
                                        RepeatDirection="Horizontal" OnDataBound="TipoAvisoIDRadioButtonList_DataBound">
                                    </asp:RadioButtonList>
                                    <asp:ObjectDataSource ID="ODSTiposAvisos" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.claTipoAvisoTableAdapter"
                                        UpdateMethod="Update">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                            <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="TipoAvisoDet" Type="String" />
                                            <asp:Parameter Name="Original_TipoAvisoID" Type="Int32" />
                                            <asp:Parameter Name="Original_TipoAvisoDet" Type="String" />
                                        </UpdateParameters>
                                    </asp:ObjectDataSource>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TipoAvisoIDRadioButtonList"
                                        runat="server" Text="*" Display="Dynamic" ErrorMessage="Seleccionar un valor para el campo Tipo de Aviso"
                                        ValidationGroup="Insertar"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-item rightHalf">
                                    <label>
                                        Rubro:</label>
                                    <asp:DropDownList ID="RubroIDDropDownList" runat="server" DataSourceID="ODSRubros"
                                        DataTextField="Descrip" DataValueField="RubroID" SelectedValue='<%# Bind("RubroID") %>'
                                        AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1" Text="Seleccionar"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:ObjectDataSource ID="ODSRubros" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSClasificadosTableAdapters.clasiRubroTableAdapter"
                                        UpdateMethod="Update">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                            <asp:Parameter Name="Original_Descrip" Type="String" />
                                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="Descrip" Type="String" />
                                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                                            <asp:Parameter Name="UsrMod" Type="Int32" />
                                            <asp:Parameter Name="FHMod" Type="DateTime" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="Descrip" Type="String" />
                                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                                            <asp:Parameter Name="UsrMod" Type="Int32" />
                                            <asp:Parameter Name="FHMod" Type="DateTime" />
                                            <asp:Parameter Name="Original_RubroID" Type="Int32" />
                                            <asp:Parameter Name="Original_Descrip" Type="String" />
                                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                        </UpdateParameters>
                                    </asp:ObjectDataSource>
                                    <asp:RequiredFieldValidator ID="reqddlcnt" runat="server" Display="Dynamic" ControlToValidate="RubroIDDropDownList"
                                        Text="*" ErrorMessage="Seleccionar un valor en el campo Rubro" InitialValue="-1"
                                        ValidationGroup="Insertar"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-item full">
                                    <label>
                                        Referencia:</label>
                                    <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TituloTextBox"
                                        ErrorMessage="Ingrese un valor en el campo Referencia" ValidationGroup="Insertar">*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-item full">
                                    <label>
                                        Descripción:</label>
                                    <asp:TextBox TextMode="MultiLine" ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                                </div>
                                <div class="form-item">
                                    <label>
                                        Adjuntar archivo:</label>
                                    <asp:FileUpload ID="ArchivoAdjuntoFileUpload" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DescripTextBox"
                                        ErrorMessage="Ingrese un valor en el campo Descripción " ValidationGroup="Insertar">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="El tamaño del archivo a adjuntar no debe superar los 5MB"
                                        OnServerValidate="CustomValidator2_ServerValidate" ValidationGroup="Insertar">*</asp:CustomValidator>
                                    <br />
                                    <asp:Label ID="Label6" runat="server" Font-Size="Smaller" Text="(Hasta 5 MB)"></asp:Label>
                                </div>
                                <div class="form-item full">
                                    <label>
                                        Link:</label>
                                    <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' />
                                </div>
                                <div class="form-item">
                                    <label>
                                        Fecha de Vencimiento:</label>
                                    <asp:TextBox ID="vencimiento_TextBox" runat="server" SkinID="form-date"></asp:TextBox>
                                    <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                                    <asp:CalendarExtender ID="vencimiento_CalendarExtender" runat="server" Enabled="True"
                                        PopupButtonID="IMGCalen1" TargetControlID="vencimiento_TextBox" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                        ErrorMessage="El formato de la fecha ingresada en Fecha de vencimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                        Operator="DataTypeCheck" Type="Date" ValidationGroup="Insertar">*</asp:CompareValidator>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="vencimiento_TextBox"
                                        ErrorMessage="La fecha de vencimiento debe ser mayor a la fecha actual" OnServerValidate="CustomValidator1_ServerValidate"
                                        ValidationGroup="Insertar">*</asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="vencimiento_TextBox"
                                        ErrorMessage="Ingrese un valor en el campo Vencimiento" ValidationGroup="Insertar">*</asp:RequiredFieldValidator>
                                </div>
                                <div class="form-item leftHalf">
                                    <label>
                                        Email Alternativo:</label>
                                    <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                        ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                        ValidationGroup="Insertar" Display="Dynamic">*</asp:RegularExpressionValidator>
                                </div>
                                <div class="form-item rightHalf">
                                    <label>
                                        Tel. Alternativo:</label>
                                    <asp:TextBox ID="TelefonoTextBox" runat="server" Text='<%# Bind("Telefono") %>' />
                                    <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Bind("UsrAlta") %>' />
                                </div>
                                <div class="form-item full">
                                    <label>
                                        Estado:</label>
                                    <asp:RadioButtonList ID="EstadoRadioButtonList" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="1">Habilitado</asp:ListItem>
                                        <asp:ListItem Value="0">Deshabilitado</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <p>
                                    Si no encuentra el rubro, por favor contactarse con Recursos Humanos</p>
                                <div class="controls">
                                    <asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                        Text="Guardar" ValidationGroup="Insertar" />
                                    <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="Cancelar" />
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                        <asp:ObjectDataSource ID="ODSClasificado" runat="server" OldValuesParameterFormatString="original_{0}"
                            OnSelecting="ODSClasificados_Selecting" SelectMethod="GetClasificadoByID" TypeName="com.paginar.johnson.BL.ControllerClasificados"
                            InsertMethod="InsertarClasificado" UpdateMethod="UpdateClasificado">
                            <InsertParameters>
                                <asp:Parameter Name="UsrAlta" Type="Int32" />
                                <asp:Parameter Name="FHAlta" Type="DateTime" />
                                <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                                <asp:Parameter Name="Habilitado" Type="Boolean" />
                                <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                <asp:Parameter Name="RubroID" Type="Int32" />
                                <asp:Parameter Name="Titulo" Type="String" />
                                <asp:Parameter Name="Descrip" Type="String" />
                                <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                <asp:Parameter Name="Link" Type="String" />
                                <asp:Parameter Name="Email" Type="String" />
                                <asp:Parameter Name="Telefono" Type="String" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="AvisoIDHiddenField" Name="AvisoID" PropertyName="Value"
                                    Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="AvisoID" Type="Int32" />
                                <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                                <asp:Parameter Name="RubroID" Type="Int32" />
                                <asp:Parameter Name="Titulo" Type="String" />
                                <asp:Parameter Name="Descrip" Type="String" />
                                <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                                <asp:Parameter Name="Link" Type="String" />
                                <asp:Parameter Name="Email" Type="String" />
                                <asp:Parameter Name="Telefono" Type="String" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                        <div class="messages msg-alerta">
                            Para consultas sobre esta sección o su contenido, y para agregar tu foto, por favor
                            contactar a <b>María del Carmen Aranguren</b> 
                        </div>
                    </asp:View>
                    <asp:View ID="VResul" runat="server">
                        <asp:Label ID="Label1" runat="server" Text="La operación fue realizada exitosamente."></asp:Label>
                        <asp:Timer ID="Timer1" runat="server" Interval="5000" OnTick="Timer1_Tick">
                        </asp:Timer>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Mis avisos" Width="100%" ID="TabPanel2" runat="server" TabIndex="2">
            <ContentTemplate>
                <div class="box resultados">
                    <asp:DataList ID="MisClasificadosDL" runat="server" DataKeyField="AvisoID" DataSourceID="ODSMisAvisos"
                        RepeatLayout="Flow" OnItemCommand="MisClasificadosDL_ItemCommand">
                        <FooterTemplate>
                            <%--<asp:Label ID="lblEmpty0" runat="server" CssClass="messages msg-info" Text="La búsqueda no produjo resultados para los parámetros indicados."
                                Visible="<%#bool.Parse(((ClasificadosDL.Items.Count==0)&&(GetPostBackControl(Page)!=null)&&(GetPostBackControl(Page).ID == ButtonBuscar.ID)).ToString())%>">
                            </asp:Label>--%>
                        </FooterTemplate>
                        <ItemTemplate>
                                                <div class="container">
                                                    <div class="content">
                                                        <div class="userCard">
                                                            <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                            <div class="foto">
                                                                <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>' />
                                                            </div>
                                                            <div class="info">
                                                                <strong>PUBLICADO POR:</strong><br />
                                                                <strong>
                                                                    <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("NombreApellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                    Interno:
                                                                    <%# Eval("interno")%></strong>
                                                                <br />
                                                                <%# Eval("cargodet")%>,
                                                                <%# Eval("areadesc")%><br />
                                                                <%# Eval("DireccionDET")%>,
                                                                <%# Eval("UbicacionDET")%>
                                                                <asp:Image ID="imgBandera" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                                                            </div>
                                                        </div>
                                                        <div class="detailAviso">
                                                            <ul>
                                                                <li class="first"></li>
                                                                <li>
                                                                    <h3>
                                                                        <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                            CommandName="Detalle" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                                                </li>
                                                                <li>
                                                                    <asp:HyperLink ID="HyperLinkLink" runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                                        Text='<%# Eval("Link") %>' Target="_blank"></asp:HyperLink>
                                                                </li>
                                                                <li class="last">
                                                                    <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>'
                                                                        Target="_blank" Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'> <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                                                </li>
                                                            </ul>
                                                            <asp:Image ID="AvisoImage" Visible="false" runat="server" CssClass="floatRight" /><br />
                                                            <asp:Label ID="DescripLabel" runat="server" Text='<%# truncarTexto(Eval("Descrip").ToString().Replace("\r\n","<br>"),(int)Eval("AvisoID")) %>' />
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <div class="comments">
                                                            <asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                                                ToolTip="Ver Comentarios" CommandArgument='<%# Eval("AvisoID") %>' CommandName="VERCOMENTARIOS" />
                                                            <asp:Label ID="lblConComents" runat="server"></asp:Label>
                                                            comentarios<br />
                                                            <br />
                                                            <asp:LinkButton ID="EditarLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                CommandName="EditarAviso">
                                                                <asp:Image runat="server" ID="imgEditar" ImageUrl="~/images/page_edit.png" AlternateText="Editar" />
                                                                Editar</asp:LinkButton>
                                                        </div>
                                                        <asp:HiddenField ID="AvisoIDHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                        <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                        <%--PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' --%>
                                                    </div>
                                                </div>
                                                <!--/container-->
                                            </ItemTemplate>
                    </asp:DataList>
                    <asp:ObjectDataSource ID="ODSMisAvisos" runat="server" DeleteMethod="DeleteAvisoByID"
                        InsertMethod="InsertarClasificado" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetByUsrAlta" TypeName="com.paginar.johnson.BL.ControllerClasificados"
                        UpdateMethod="UpdateClasificado" OnObjectCreating="ODSMisAvisos_ObjectCreating"
                        OnObjectDisposing="ODSMisAvisos_ObjectDisposing">
                        <DeleteParameters>
                            <asp:Parameter Name="AvisoID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                            <asp:Parameter Name="RubroID" Type="Int32" />
                            <asp:Parameter Name="Titulo" Type="String" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                            <asp:Parameter Name="Link" Type="String" />
                            <asp:Parameter Name="Email" Type="String" />
                            <asp:Parameter Name="Telefono" Type="String" />
                            <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                            <asp:Parameter Name="Habilitado" Type="Boolean" />
                        </InsertParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="UsrAltaHiddenField" Name="UsrAlta" PropertyName="Value"
                                Type="Int32" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="AvisoID" Type="Int32" />
                            <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                            <asp:Parameter Name="RubroID" Type="Int32" />
                            <asp:Parameter Name="Titulo" Type="String" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                            <asp:Parameter Name="Link" Type="String" />
                            <asp:Parameter Name="Email" Type="String" />
                            <asp:Parameter Name="Telefono" Type="String" />
                            <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                            <asp:Parameter Name="Habilitado" Type="Boolean" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                </div>
                <!--/resultados-->
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Reporte de Comentarios" Width="100%" ID="TabPanel3" runat="server" TabIndex="3">
            <ContentTemplate>
                <asp:Panel ID="PanelReporte" runat="server" DefaultButton="btnFlitroClasificado">
                
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Buscar" />
            <div class="box resultados">
                <div class="box filtros">
                    <asp:RadioButtonList ID="RBLFiltroClasificados" runat="server" 
                        RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" 
                        onselectedindexchanged="RBLFiltroClasificados_SelectedIndexChanged">
                        <asp:ListItem Selected="True">TODOS</asp:ListItem>
                        <asp:ListItem Value="FECHA">HOY</asp:ListItem>
                        <asp:ListItem Value="DESDEHASTA">RANGO</asp:ListItem>
                    </asp:RadioButtonList>
                <asp:TextBox ID="TextBox1" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBox1"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="TextBox1" SetFocusOnError="True">*</asp:RangeValidator>
                <asp:CalendarExtender TargetControlID="TextBox1" ID="CalendarExtender1" 
                        runat="server" PopupButtonID="btnc" Enabled="True" Format="dd/MM/yyyy">
                </asp:CalendarExtender><asp:ImageButton ID="btnc" runat="server" ImageUrl="~/images/Calendar.png">
                    </asp:ImageButton>
                    -
                <asp:TextBox ID="TextBox2" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBox2"
                    ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="TextBox2"
                    ControlToValidate="TextBox1" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                    Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="TextBox2" SetFocusOnError="True">*</asp:RangeValidator>

                <asp:CalendarExtender TargetControlID="TextBox2" PopupButtonID="btnhasta" 
                        ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:ImageButton ID="btnhasta" runat="server" ImageUrl="~/images/Calendar.png" />

                    <asp:Button ID="btnFlitroClasificado" runat="server" 
                        onclick="btnFlitroClasificado_Click" Text="Buscar" 
                        ValidationGroup="Buscar" />
                    <asp:Button ID="Export" runat="server" Text="Exportar" onclick="Export_Click" /><br /><br /><asp:Image ID="imgPrinter" runat="server" ImageUrl="~/images/printer.png" />&nbsp;&nbsp;<asp:HyperLink
                        ID="HLimprimirReporte" runat="server" Text="Imprimir Reporte" NavigateUrl="~/Clasificados/imprimirReporte.aspx" Target="_blank"></asp:HyperLink><br /><br />
                </div>
                <asp:Repeater ID="RepeaterClasificado" runat="server" 
                    DataSourceID="ODSClasificadosConComentarios">
                                    <ItemTemplate>
                                        <div class="bloque">
                                            <asp:HiddenField ID="RubroIDHiddenField" runat="server" Value='<%# Eval("RubroID") %>' />
                                            <h4>
                                                <asp:Label ID="RubroDescLabel" runat="server" Text='<%# Eval("RubroDesc") %>' />
                                                -
                                                <asp:Label ID="TipoAvisoDetLabel" runat="server" Text='<%# Eval("TipoAvisoDet") %>' /></h4>
                                            <div class="container">
                                                <div class="content">
                                                    <div class="userCard">
                                                        <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                        <div class="foto">
                                                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>' />
                                                        </div>
                                                        <div class="info">
                                                            <strong>PUBLICADO POR</strong><br />
                                                            <strong>
                                                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("FHAlta","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("NombreApellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                int:
                                                                <%# Eval("interno")%></strong>
                                                            <br />
                                                            <%# Eval("cargodet")%>,
                                                            <%# Eval("areadesc")%>,<br />
                                                            <%# Eval("DireccionDET")%>,
                                                           <%# Eval("UbicacionDET")%>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                                                        </div>
                                                    </div>
                                                    <div class="detailAviso">
                                                        <ul>
                                                            <li class="first"></li>
                                                            <li>
                                                                <h3>
                                                                    <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("AvisoID") %>'
                                                                        CommandName="Detalle" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                                            </li>
                                                            <li>
                                                                <asp:HyperLink ID="HyperLinkLink" runat="server" NavigateUrl='<%# Eval("Link") %>'
                                                                    Text='<%# Eval("Link") %>' Target="_blank"></asp:HyperLink>
                                                            </li>
                                                            <li class="last">
                                                                <asp:HyperLink ID="HyperLinkArchivoAdjunto" runat="server" NavigateUrl='<%# "~/Clasificados/archivos/"+Eval("ArchivoAdjunto") %>'
                                                                    Target="_blank" Visible='<%# Eval("ArchivoAdjunto").ToString()!="" %>'> <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                                            </li>
                                                        </ul>
                                                        <asp:Image ID="AvisoImage" Visible="false" runat="server" CssClass="floatRight" /><br />
                                                        <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("Descrip").ToString().Replace("\r\n","<br>") %>' />
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <div class="comments">
                                                        <%--<asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                                            ToolTip="Ver Comentarios" CommandArgument='<%# Eval("AvisoID") %>' CommandName="VERCOMENTARIOS" />--%>
                                                        <asp:Label ID="lblConComents" runat="server" Visible="false"></asp:Label>
                                                        <%--comentarios<br />
                                                        <br />--%>
                                                    </div>
                                                    <asp:HiddenField ID="AvisoHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="AvisoIDHiddenField" runat="server" Value='<%# Eval("AvisoID") %>' />
                                                    <asp:HiddenField ID="UsrAltaHiddenField" runat="server" Value='<%# Eval("UsrAlta") %>' />
                                                    <%--PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' --%>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/bloque-->
                                        <div class="box comentarios">
                                            <h4>
                                                Comentarios</h4>
                                            <a name="comentarios-abajo"></a>

                                            <!--/usuario-comentar-->
                                            <div class="lista-comentarios">
                                                <asp:DataList ID="dtComments" runat="server" DataKeyField="comentarioID" DataSourceID="ODSComentarios"
                                                    RepeatLayout="Flow">
                                                    <ItemTemplate>
                                                        <div class="comentario">
                                                            <div class="pic">
                                                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif" %>'
                                                                    ToolTip='<%# Eval("nombrecompleto") %>' />
                                                            </div>
                                                            <div class="content">
                                                                <p>
                                                                    <asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                                                <p>
                                                                    Fecha:
                                                                    <asp:Label ID="FechaLabel" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy  - hh:MM tt}") %>' /></p>
                                                                <p>
                                                                    Comentario:
                                                                    <asp:Label ID="comentariosLabel" runat="server" Text='<%# Eval("comentarios").ToString().Replace("\r\n","<br>") %>' /></p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                            <!--/lista-comentarios-->
                                            <asp:ObjectDataSource ID="ODSComentarios" runat="server" InsertMethod="InsertarClasificado"
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="getComentariosByAvisoID"
                                                TypeName="com.paginar.johnson.BL.ControllerClasificados" UpdateMethod="UpdateClasificado">

                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="AvisoHiddenField" Name="AvisoID" PropertyName="Value"
                                                        Type="Int32" />
                                                </SelectParameters>

                                            </asp:ObjectDataSource>
                                        </div>
                                    </ItemTemplate>
                </asp:Repeater>
                <asp:ObjectDataSource ID="ODSClasificadosConComentarios" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="getClasificadosConComentario" 
                    TypeName="com.paginar.johnson.BL.ControllerClasificados" 
                    DeleteMethod="DeleteAvisoByID" InsertMethod="InsertarClasificado" 
                    UpdateMethod="UpdateClasificado">

                    <DeleteParameters>
                        <asp:Parameter Name="AvisoID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="UsrAlta" Type="Int32" />
                        <asp:Parameter Name="FHAlta" Type="DateTime" />
                        <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                        <asp:Parameter Name="RubroID" Type="Int32" />
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                        <asp:Parameter Name="Link" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Telefono" Type="String" />
                        <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                        <asp:Parameter Name="Habilitado" Type="Boolean" />
                    </InsertParameters>

                    <SelectParameters>
                        <asp:ControlParameter ControlID="TextBox1" DefaultValue="" Name="FechaDesde" 
                            PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="TextBox2" DefaultValue="" Name="FechaHasta" 
                            PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="RBLFiltroClasificados" DefaultValue="TODOS" 
                            Name="modo" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>

                    <UpdateParameters>
                        <asp:Parameter Name="AvisoID" Type="Int32" />
                        <asp:Parameter Name="TipoAvisoID" Type="Int32" />
                        <asp:Parameter Name="RubroID" Type="Int32" />
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="ArchivoAdjunto" Type="String" />
                        <asp:Parameter Name="Link" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Telefono" Type="String" />
                        <asp:Parameter Name="FechaVencimiento" Type="DateTime" />
                        <asp:Parameter Name="Habilitado" Type="Boolean" />
                    </UpdateParameters>

                </asp:ObjectDataSource>
            </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
</asp:Content>
