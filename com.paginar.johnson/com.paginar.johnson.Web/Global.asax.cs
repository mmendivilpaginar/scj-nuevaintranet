﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.Caching;
using System.Web.Security;
using System.Security.Principal;
using com.paginar.johnson.DAL;
using com.paginar.johnson.Web.Monitoring;
using System.Net.Mail;
using System.IO;

namespace com.paginar.johnson.Web
{
    public class Global : System.Web.HttpApplication
    {
        private static MetaModel s_defaultModel = new MetaModel();
        public static MetaModel DefaultModel
        {
            get
            {
                return s_defaultModel;
            }
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            //                    IMPORTANTE: REGISTRO DEL MODELO DE DATOS 
            // Quite la marca de comentario de esta línea para registrar un modelo de LINQ to SQL para datos dinámicos de ASP.NET.
            // Establezca ScaffoldAllTables = true solo si está seguro de que desea que todas las tablas del
            // modelo de datos admitan una vista con scaffold (es decir, plantillas). Para controlar la técnica scaffolding para
            // tablas individuales, cree una clase parcial para la tabla y aplique el
            // atributo [ScaffoldTable(true)] a la clase parcial.
            // Nota: asegúrese de que cambia "YourDataContextType" al nombre de la clase del contexto de datos
            // en la aplicación.

            DefaultModel.RegisterContext(typeof(SCJBODataContext), new ContextConfiguration() { ScaffoldAllTables = true });

            // La siguiente declaración admite el modo de páginas independientes, donde las tareas List, Detail, Insert y 
            // Update se realizan usando páginas independientes. Para habilitar este modo, quite las marcas de comentario de la siguiente 
            // definición del objeto route y marque como comentario las definiciones de route en la sección del modo combined-page siguiente.
            routes.Add(new DynamicDataRoute("BO/{table}/{action}.aspx")
            {
                Constraints = new RouteValueDictionary(new { action = "List|Details|Edit|Insert" }),
                Model = DefaultModel
            });

            // Las siguientes declaraciones admiten el modo combined-page, donde las tareas List, Detail, Insert y
            // Update se realizan usando la misma página. Para habilitar este modo, quite las marcas de comentario de los siguientes objetos
            // routes y marque como comentario la definición del objeto route en la sección del modo de páginas independientes anterior.
            //routes.Add(new DynamicDataRoute("{table}/ListDetails.aspx") {
            //    Action = PageAction.List,
            //    ViewName = "ListDetails",
            //    Model = DefaultModel
            //});

            //routes.Add(new DynamicDataRoute("{table}/ListDetails.aspx") {
            //    Action = PageAction.Details,
            //    ViewName = "ListDetails",
            //    Model = DefaultModel
            //});

        }

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            Application["UsuariosConectados"] = 0;
        }

        void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            Application["UsuariosConectados"] = (int)Application["UsuariosConectados"] + 1;
            Application.UnLock();
        }

        void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["UsuariosConectados"] = (int)Application["UsuariosConectados"] - 1;
            Application.UnLock();
        }  

        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    HttpContext.Current.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
        //}


      

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            //com.paginar.johnson.Web.Compress.CompressByPreRequest objectStream = new Compress.CompressByPreRequest();
            //objectStream.SetCompresionStream(sender);
           
        }


        //By Claudinio
        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                 NotificarPorError();
            }
            catch (Exception)
            {

            }

        }

        private void NotificarPorError()
        {

            string PATH_FILE_CONFIG = Server.MapPath("~/WebNotify.xml");
            HttpException lastErrorWrapper = Server.GetLastError() as HttpException;
            Exception lastError = lastErrorWrapper;
            if (lastErrorWrapper.InnerException != null)
                lastError = lastErrorWrapper.InnerException;

            string lastErrorTypeName = lastError.GetType().ToString();
            string lastErrorMessage = lastError.Message;
            string lastErrorStackTrace = lastError.StackTrace;
            string numberUser= Application["UsuariosConectados"] .ToString();
            string Server_Name = this.GetNameLocalHost();
            IContentsMail ic = new ContentsHTML(Request.RawUrl,
           User.Identity.Name, lastErrorTypeName, lastErrorMessage, lastErrorStackTrace.Replace(Environment.NewLine, "<br />"),numberUser,Server_Name);
            
            IDataRetriever idr = new ConfigurationDataRetriever(PATH_FILE_CONFIG);
            IFilter myFilter = new FilterByMail(idr);
            Mail mailSend = new Mail(idr, ic) { SourceFilter = myFilter };
            

            // Adjuntar html de error.
            string strHtmlError = lastErrorWrapper.GetHtmlErrorMessage();
            if (!string.IsNullOrEmpty(strHtmlError))
            {
                Attachment YSOD = Attachment.CreateAttachmentFromString(strHtmlError, "ScrError_Original.htm");
                ic.Adjuntos.Add(YSOD);
            }

            EscribirEnLog(ic.TextToSend);
            EscribirEnLog(strHtmlError);

            


            mailSend.EnviarNotificacion();
        }

        private void EscribirEnLog(string strHtmlError)
        {
            try
            {
                string filename = DateTime.Now.Ticks.ToString() + ".txt";
                string nameFolder = DateTime.Now.ToShortDateString().Replace("/", "");

                string strPathFolder = Server.MapPath("~/Log/" + nameFolder);
                string strPathFile = Server.MapPath("~/Log/" + nameFolder + "/" + filename);

                if (!Directory.Exists(strPathFolder))
                {
                    Directory.CreateDirectory(strPathFolder);
                }

                StreamWriter swrtLog = System.IO.File.CreateText(strPathFile);
                Log LogErrores = new Log(strHtmlError, swrtLog);
                LogErrores.Run();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetNameLocalHost()
        {
            string val="No se pudo detectar el nombre del SERVER";
            try
            {
                val=Environment.MachineName;
            }
            catch(Exception){
             
            }

            return val;

        }
    }
}
