﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/MasterPages/TwoSidebars.master"   AutoEventWireup="true" CodeBehind="DefaultDynData.aspx.cs" Inherits="com.paginar.johnson.Web.DefaultDynData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />

    <h2 class="DDSubHeader">Mis tablas</h2>

    <br /><br />

    <asp:GridView ID="Menu1" runat="server" AutoGenerateColumns="false"
        CssClass="DDGridView" RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6">
        <Columns>
            <asp:TemplateField HeaderText="Nombre de la tabla" SortExpression="TableName">
                <ItemTemplate>
                    <asp:DynamicHyperLink ID="HyperLink1" runat="server"><%# Eval("DisplayName") %></asp:DynamicHyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

