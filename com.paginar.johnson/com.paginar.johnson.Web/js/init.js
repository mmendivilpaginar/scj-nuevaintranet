$(document).ready(function () {
    estrategias();
    actCron();
    $('#CPHMain_main_TabContainerVoluntariado_TPVolutariadoEventos_tab').click(function () {
       // galeria_elemHeight();
    });
});


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

function pathIqualTo(pathString) {
    if (document.location.pathname.toLowerCase() == pathString.toLowerCase())
        return true;
    else
        return false;
}

function pathStartsWith(pathStartString) {
    if (document.location.pathname.toLowerCase().indexOf(pathStartString.toLowerCase()) == 0)
        return true;
    else
        return false;
}

function pathContains(pathStartString) {
    if (document.location.pathname.toLowerCase().indexOf(pathStartString.toLowerCase()) > -1)
        return true;
    else
        return false;
}

function estrategias() {
    if (pathContains('estrategias2016.aspx')) {
        $('#noticias').addClass('estrategiaMargen');

    }
}



function navGlobal() {
    $('#navglobal ul.level1 > li').each(function (index) {
        //si dentro de un 'li' hay un '.selected', entonces agregar clase 'active' a ese 'li'
        if ($(this).find('a.selected').length != 0) {
            $(this).addClass('active');
        }
    });
    $('#navglobal ul.level1 > li').hover(
        function () {
            $(this).addClass('over');
        },
        function () {
            $(this).removeClass('over');
        }
    );
    $('#navglobal ul.level2').corner('bl br 10px');//tl tr bl br

    itemMenuEspecial();
} 


function itemMenuEspecial() {
    $('#navglobal ul.level1 > li').each(function (index) {
        //si dentro de un 'li' hay un texto 'Links', entonces agregar clase 'item-especial' a ese 'li'
        if ($(this).find('a.level1:contains("Links")').length != 0) {
            $(this).addClass('specialitem');
        }
    });

    //personalizando el tooltip
    $('#navglobal ul.level1 > li.specialitem a.level2').cluetip({
        splitTitle: '|',
        width: 220,
        //positionBy: 'bottomTop',
        
        positionBy: 'fixed',
        topOffset: 35,
        leftOffset:25,

        arrows: false,
        clickThrough: true
    });

}

function buscadorGeneral() {
    $('#buscador-general #BusquedaTextBox').wrap('<div class="wrapper-input"></div>');
}


function guardarTamFuente(size) {
    //alert(size);
    //createCookie("fontsize", size, 30);
}
function recuperarTamFuente() { 
    
}

function aumentarReducirFuente() {
    var tamActual = $('#content').css('font-size'); //tama�o por defecto es 62.5% = 10px
    var size = parseFloat(tamActual, 10);
    var BASE = size;
    var TOPE = 14;

    //Aumentar el tama�o de la fuente
    $('#FuenteMASHyperLink').click(function () {
        if (size < TOPE && size >= BASE) {
            size++;
            $('#content').animate({ fontSize: size });
            guardarTamFuente(size);
        }
        return false;
    });

    //Reducir el tama�o de la fuente
    $('#FuenteMENOSHyperLink').click(function () {
        if (size > BASE && size <= TOPE) {
            size--;
            $('#content').animate({ fontSize: size });
            guardarTamFuente(size);
        }
        return false;
    });
}


function customPages() {
    if ($('#home').length > 0) {
        $('body').addClass("home");
    }
    if ($('#nobg').length > 0) {
        $('body').addClass("nobg");
    }
}

function initWidgets() {
    $('.accordion').accordion({
        autoHeight: false,
        collapsible: true
    });

    $('.tabs').tabs();

}

function customChangePass() {
    $('.AspNet-ChangePassword-SubmitPanel input').addClass('form-submit');
    $('.AspNet-ChangePassword-PasswordPanel input, .AspNet-ChangePassword-NewPasswordPanel input,.AspNet-ChangePassword-ConfirmNewPasswordPanel input').addClass('form-text');
}



$(document).ready(function () {
    customChangePass();
    customPages();
    navGlobal();
    buscadorGeneral();
    aumentarReducirFuente();
    initWidgets();
    //$('.ajax__tab_outer').corner('tl tr 10px'); //tl tr bl br
    //$('.ui-tabs-nav li').corner('tl tr 10px'); //tl tr bl br

    $('.noticia .tags li').corner('10px');


    $(".TreeView a").click(function () {  // bullets
        $(".TreeView a").removeClass("activo");
        $(this).addClass('activo');
    });



});











//Reloj
var timerID = null;
var timerRunning = false;
function stopclock() {
    if (timerRunning)
        clearTimeout(timerID);
    timerRunning = false;
}
function showtime(hours, minutes ,seconds) {
    var timeValue = "" + hours;
    if (timeValue == "0") timeValue = 12;
    timeValue += ((minutes < 10) ? ".0" : ".") + minutes
    //timeValue += ((seconds < 10) ? ":0" : ":") + seconds

    document.getElementById('lblHora').innerText = timeValue;

    
    if (seconds > 59) {
        seconds = 0;
        minutes++;
    }

    if (minutes > 59) {
        minutes = 0;
        hours++;
    }

    if (hours > 23) {
        seconds = 0;
        minutes = 0;
        hours = 0;
    }

    seconds++;

    timerID = setTimeout("showtime("+hours+","+minutes+","+seconds+")", 1000);
    timerRunning = true;
}
function startclock() {
    stopclock();
    showtime();
}
//Fin Reloj


//Inicio - cookie functions http://www.quirksmode.org/js/cookies.html
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
function eraseCookie(name) {
    createCookie(name, "", -1);
}
//Fin - cookie functions



//Funcion PopUp
function popup(url) {    
    void (window.open(url));
}

//Funcion Scroll Contenido
function pageScroll() {
    if (window.divContenido.children[0].innerHTML != "" &&
     window.divContenido.children[0].innerHTML != "<DIV class=AspNet-FormView-Data>&nbsp;<BR></DIV>")
       window.divContenido.focus();
}

//Funcion Scroll Contenido en politicas empresas
function pageScrollPoliticasDeEmpresa(event) {

    var obj = event.srcElement || event.target;
    var seltreeNode = obj;
    var href = event.srcElement.href;
    if (href) {
        var funcion = href.substr(0, 30);
        var extension = href.substr(href.length - 3);
        if (extension != 'doc' && extension != 'xls' && extension != 'ppt' && extension != 'pps' && funcion != 'javascript:TreeView_ToggleNode')
            window.divContenido.focus();
        else {
            window.divContenido.children[0].innerHTML = "<DIV class=AspNet-FormView-Data>&nbsp;<BR></DIV>"
        }
    }
   
}

/* Esta variable sirve para representar la instancia del popup que se abre cuando iniciamos el backOffice*/
var scj_PopupBackOffice;
/* ******************************************************************************************* */
function ClosePopupBO_IfExist() {

    if (scj_PopupBackOffice != null) {

        scj_PopupBackOffice.close();
    }
}

function confirmClose() {
    if (confirm("�Se encuentra seguro de salir de la Intranet?")) {

        ClosePopupBO_IfExist();

        return true;
      
    }
    else {
        alert("Operacion Cancelada.");
        return false;
        }
}

/**/
function checkTextAreaMaxLength(textBox, e, length) {

    var mLen = textBox["MaxLength"];
    if (null == mLen)
        mLen = length;

    var maxLength = parseInt(mLen);
    if (!checkSpecialKeys(e)) {
        if (textBox.value.length > maxLength - 1) {
            if (window.event)//IE
                e.returnValue = false;
            else//Firefox
                e.preventDefault();

            alert('No puede superar los ' + maxLength + ' caracteres');
        }
    }
}
function checkSpecialKeys(e) {
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
        return false;
    else
        return true;
}

/* Helper para estilar la tabla del cronograma de actividades */
function actCron() {
    var tabla = $('table.Cronograma');
    if (pathContains('/Voluntariado/')) {
        tabla.find('th').each(function (e) {
            if (e > 0) {
                var innerTxt = $(this).html();
                $(this).html(innerTxt.substring(0, 3) + '.');
            }
        });

        tabla.find('tr').each(function (e) {
            if (e % 2 == 0) {
                $(this).addClass('odd');
            } else {
                $(this).addClass('even');
            }
        });
    }
}


function CallServiceGuardarEstadistica(Pagina, Titulo) {

    var UsuarioID = $('input[id$=HiddenFieldUsuarioID]').val();


    $.ajax({
        type: "POST",
        url: "../UserControl_Home/wsEstadisticas.asmx/GuardarEstadistica",
        data: "{'UsuarioID': '" + UsuarioID + "', 'Pagina': '" + Pagina + "', 'Titulo': '" + Titulo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        error: OnError
    });

}

function OnSuccess(data, status) {
    // alert("OnSuccess");
}

function OnError(request, status, error) {
    //alert(error);
}
/*function galeria_elemHeight() {
    var h = 0;
    $('#CPHMain_main_TabContainerVoluntariado_TPVolutariadoEventos li').each(function () {
        if ($(this).height() > 0) {
            h = $(this).height();
        }
    });
    $('#CPHMain_main_TabContainerVoluntariado_TPVolutariadoEventos li').height(h)
}*/





/************Relaciones con la Comunidad*****************/
$.fn.center = function () {
    this.css({
        'position': 'fixed',
        'left': '50%',
        'top': '50%'
    });
    this.css({
        'margin-left': -this.outerWidth() / 2 + 'px',
        'margin-top': -this.outerHeight() / 2 + 'px'
    });

    return this;
}
$.fn.vAlign = function () {
    return this.each(function (i) {
        var ah = $(this).height();
        var ph = $(this).parent().parent().height();
        var mh = Math.ceil((ph - ah) / 2); //var mh = (ph - ah) / 2;
        $(this).css('margin-top', mh);
    });
};

function modal() {
    $('.pnet-modal-overlay,.pnet-wrapper-modal').show();
    $('.pnet-wrapper-modal').center();
}

function cerrarModal() {
    $('.pnet-modal-overlay,.pnet-wrapper-modal').hide();
}


$(window).load(function () {
    $('.pnet-action-list li img').vAlign();

});

