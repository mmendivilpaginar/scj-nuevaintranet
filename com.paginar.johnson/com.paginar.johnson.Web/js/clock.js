var id = "";

function Clock(hour,min,sec){

    var aClock = $('#analog-clock');

    if (aClock != null) {

        

        var clockWidthHeight = aClock.width(); //width and height of the clock

        aClock.css({ "height": clockWidthHeight + "px" }); //sets the height if .js is enabled. If not, height = 0;
        aClock.fadeIn(); //fade it in

        var date = new Date();

        var now = new Date(date.getYear, date.getMonth, date.getDay, hour, min, sec);

        var secondAngle = 360 / 60 * sec; //turn the time into angle

        if (aClock != null) {
            $('#secondHand').rotate(secondAngle, 'abs'); //set the hand angle
            $('#secondHand').css({ "left": (clockWidthHeight - $('#secondHand').width()) / 2 + "px", "top": (clockWidthHeight - $('#secondHand').height()) / 2 + "px" }); //set x and y pos
        }


        //set the minute hand
        if (aClock != null) {
            var minuteAngle = 360 / 60 * min;  //turn the time into angle
            $('#minuteHand').rotate(minuteAngle, 'abs'); //set the hand angle
            $('#minuteHand').css({ "left": (clockWidthHeight - $('#minuteHand').width()) / 2 + "px", "top": (clockWidthHeight - $('#minuteHand').height()) / 2 + "px" }); //set x and y pos
        }

        //set the hour hand
        if (aClock != null) {
            var hourAngle = 360 / 12 * hour; //turn the time into angle
            $('#hourHand').rotate((hourAngle + minuteAngle / 12) % 360, 'abs'); //set the hand angle
            $('#hourHand').css({ "left": (clockWidthHeight - $('#hourHand').width()) / 2 + "px", "top": (clockWidthHeight - $('#hourHand').height()) / 2 + "px" }); //set x and y pos
        }

        sec++;

        if (sec > 59) {
            sec = 0;
            min++;
        }

        if (min > 59) {
            min = 0;
            hour++;
        }

        id=setTimeout("Clock(" + hour + "," + min + "," + sec + ");", 1000);
    } //if aclock != null

}


function StopClock() {
    
    clearInterval(id);
}





