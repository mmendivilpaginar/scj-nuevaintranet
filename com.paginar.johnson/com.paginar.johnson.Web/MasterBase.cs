﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.paginar.johnson.BL;
using com.paginar.johnson.Web.UserControl_Home;

namespace com.paginar.johnson.Web
{
    public class MasterBase : System.Web.UI.MasterPage
    {

        public Usuario ObjectUsuario
        {
            get
            {
                if (Session["Usuario"] == null)
                {
                    if (Page.User.Identity.IsAuthenticated)
                    {
                        Usuario u = new Usuario();
                        ControllerSeguridad CS = new ControllerSeguridad();
                        u = CS.GetDatosUsuarioByUserName(Page.User.Identity.Name);
                        Session["Usuario"] = u;
                    }
                }
                return Session["Usuario"] as Usuario;
            }
        }

        public bool EditaClave
        {
            get
            {
                if (Session["EditaClave"] == null)
                    return false;
                else
                    return ((bool)Session["EditaClave"]);
            }
            set { Session["EditaClave"] = value; }
        }

    }

}