﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="CumplimientoLegal.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.CumplimientoLegal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<h2>Línea Directa de Cumplimiento Legal</h2>
<p>
SC Johnson se compromete a sostener una cultura de transparencia, honestidad e integridad de acuerdo con los principios definidos en En esto creemos.
</p>
<p>
A fin de mantener alineación y la congruencia a nivel global, hemos extendido una <strong>línea directa gratuita confidencial, Línea Directa de Cumplimiento Legal</strong> a la Argentina, que <strong>puede ser usada por los empleados para informar acerca de posibles infracciones a la ley</strong>, incluyendo prácticas comerciales o financieras no éticas e irregularidades contables. La Línea Directa está disponible llamando al <strong>0800-444-2715.</strong> 
</p>
<p>
Pueden usar la Línea Directa en cualquier momento, de día y de noche, y hablar con un entrevistador de una organización externa que grabará los detalles del informe. Todos los informes serán re-enviados al Departamento Legal Corporativo de SCJ, ubicado en Racine, para el seguimiento.
</p>
<p>
<strong>Es vital que los empleados cuenten con una alternativa más para compartir sus inquietudes, porque la integridad de SCJ en su totalidad depende de la integridad de cada individuo.</strong> Permitir que la integridad decaiga nos afecta a todos y compromete la cultura de respeto y confianza en la compañía.
</p>
<p>
En el archivo adjunto encontrarán mayor información sobre el tema. Si tienen alguna pregunta, por favor, hablen con su gerente, con Recursos Humanos o contacten al Departamento Legal Corporativo de SCJ, en Racine.
</p>
<p>
    <asp:HyperLink ID="Descargappt" runat="server" NavigateUrl="~/servicios/documents/Linea_Directa_de_Cumplimiento_Etico_Legal.pptx" >Descargar</asp:HyperLink>
</p>
</asp:Content>
