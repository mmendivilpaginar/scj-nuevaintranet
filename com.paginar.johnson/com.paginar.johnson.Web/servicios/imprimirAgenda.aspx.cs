﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;
using com.paginar.johnson.DAL;
using System.Text;


namespace com.paginar.johnson.Web.servicios
{
    public partial class imprimirAgenda : System.Web.UI.Page
    {

        bool primeraHoja = true;
        bool cambioUbicacion = false;
        int acumulativo = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                primeraHoja = true;
                lblPodesta.Text = listadoAgenda("000", "OFICINAS SAN ISIDRO 4549 + N° INT", "ARGENTINA");
                lblpacheco.Text = listadoAgenda("003", "PLAZA LOGÍSTICA - PACHECO (00 54 03327) 449-700", "ARGENTINA");
                lblpilar.Text = listadoAgenda("600", "PLANTA PILAR 0230 - 4538-300", "ARGENTINA");
                lblchile.Text = listadoAgenda("201", "SANTIAGO (00 56 2) 370-5100 | 1+730+ 3 últ. dígitos ext.", "CHILE");
                lblparaguay.Text = listadoAgenda("400", "(595 21) 221751 / 213759 / 228321", "PARAGUAY");
                lbluruguay.Text = listadoAgenda("300", "(00 59 82) 622-4444 | Modo económ: 1760+0014 / 1760+0015", "URUGUAY");
            }
        }

        protected void DDLUbicacion_SelectedIndexChanged(object sender, EventArgs e)
        {

            /*
000	Podestá
003	Pacheco
007	Externos a Arg: Viña
009	Externos a Arg: Asunción
010	Externos a Arg: Santiago
011	Vendedores Interior
201	Santiago
202	Chile: Regionales
300	Uruguay
400	Paraguay
600	Pilar             
             */

            DropDownList valor = sender as DropDownList;
            string valorselect = valor.SelectedValue.ToString();
            if(valorselect == "999"){
                primeraHoja = true;
                cambioUbicacion = false;
                lblPodesta.Text = listadoAgenda("000", "PLANTA PODESTÁ 4841 + N° INT ó CENTRO DE SERVICIOS COMPARTIDOS 5554-3800 ó 554 + N° INT", "ARGENTINA");
                lblpacheco.Text = listadoAgenda("003", "PLAZA LOGÍSTICA - PACHECO (00 54 03327) 449-700", "ARGENTINA");
                lblpilar.Text = listadoAgenda("600", "PLANTA PILAR (00 54 0230) 4538-300", "ARGENTINA");
                lblchile.Text   = listadoAgenda("201", "SANTIAGO (00 56 2) 370-5100 | 1+730+ 3 últ. dígitos ext.", "CHILE");
                lblparaguay.Text= listadoAgenda("400", "(595 21) 221751 / 213759 / 228321", "PARAGUAY");
                PanelArgentinaPilar.Visible = true;
                pnlArgentinaPacheco.Visible = true;
                pnlArgentinaPodesta.Visible = true;
                pnlChile.Visible = true;
                pnlParaguay.Visible = true;
                pnlUruguay.Visible = true;
                pnlGenerales.Visible = true;
            }
            if (valorselect == "000")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lblPodesta.Text = listadoAgenda("000", "PLANTA PODESTÁ 4841 + N° INT ó CENTRO DE SERVICIOS COMPARTIDOS 5554-3800 ó 554 + N° INT", "ARGENTINA");
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = true;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
                pnlGenerales.Visible = false;
            }
            if (valorselect == "003")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lblpacheco.Text = listadoAgenda("003", "PLAZA LOGÍSTICA - PACHECO (00 54 03327) 449-700", "ARGENTINA");
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = true;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
                pnlGenerales.Visible = false;
            }
            if (valorselect == "600")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lblpilar.Text = listadoAgenda("600", "PLANTA PILAR (00 54 0230) 4538-300", "ARGENTINA");
                PanelArgentinaPilar.Visible = true;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
                pnlGenerales.Visible = false;
            }

            if (valorselect == "201")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lblchile.Text = listadoAgenda("201", "SANTIAGO (00 56 2) 370-5100 | 1+730+ 3 últ. dígitos ext.", "CHILE");
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = true;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
                pnlGenerales.Visible = false;
            }
            if (valorselect == "400")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lblparaguay.Text = listadoAgenda("400", "(595 21) 221751 / 213759 / 228321", "PARAGUAY");
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = true;
                pnlUruguay.Visible = false;
                pnlGenerales.Visible = false;
            }

            if (valorselect == "300")
            {
                primeraHoja = true;
                cambioUbicacion = false;
                lbluruguay.Text = listadoAgenda("300", "(00 59 82) 622-4444 | Modo económ: 1760+0014 / 1760+0015", "URUGUAY");
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = true;
                pnlGenerales.Visible = false;
            }

            if (valorselect == "888")
            {
                //Generales
                primeraHoja = true;
                cambioUbicacion = false;
                pnlGenerales.Visible = true;
                PanelArgentinaPilar.Visible = false;
                pnlArgentinaPacheco.Visible = false;
                pnlArgentinaPodesta.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
            }
          
            /*
            ControllerAgendaTel ca = new ControllerAgendaTel();
            int? clusterid = null;
            int? ubicacionid = null;
            string busqueda = null;

            gvGeneral.DataSource = ca.GetBusqueda(clusterid, ubicacionid, busqueda);
            gvGeneral.DataBind();
             
             */


        }

        string cabeceraActual = string.Empty;
        bool primerCabecera = true;
        public string cabeceraGeneral(string cabecera)
        {
            string cabeceraRetorno = string.Empty;
            if (cabeceraActual != cabecera )
            {
                cabeceraActual = cabecera ;
                if (primerCabecera)
                {
                    cabeceraRetorno = "<table><thead><tr><th class='AspNet-DataList-Header '><h3><span>" + cabeceraActual + "</span></h3></th></tr></thead><tbody>";
                }
                else
                {
                    cabeceraRetorno = "</tbody></table><table><thead><tr><th class='AspNet-DataList-Header '><h3><span>" + cabeceraActual + "</span></h3></th></tr></thead><tbody>";
                    primerCabecera = false;
                }
                //return "<tr><th><strong>" + cabeceraActual + "</strong><br /></th></tr>";
                return cabeceraRetorno;
            }


            return "";
        }

        public string printInterno(string interno)
        {
            if (interno != "0")
            {
                return  " - " + interno;
            }
            return null;

        }

        public string printDescripcion(string descripcion)
        {
            string original = descripcion.Replace("Para ver el instructivo, <a shape=\"rect\" href=\"PWP.pps\" target=\"_blank\">click aquí</a>","");
            descripcion = descripcion.Replace("&nbsp;", "");
            descripcion = descripcion.Trim();
            if (descripcion.Length < 80)
            {
                descripcion = descripcion.Replace("<strong>", "");
                descripcion = descripcion.Replace("</strong>", "");
                descripcion = descripcion.Replace("<br>", "");
                descripcion = descripcion.Replace("<br/>", "");
                descripcion = descripcion.Replace("<br />", "");
                descripcion = descripcion.Replace("<BR>", "");
                descripcion = descripcion.Replace("<BR/>", "");
                descripcion = descripcion.Replace("<BR />", "");
                original = descripcion;
            }

            //&nbsp;
            
            if (descripcion != "")
            {
                return " <br /> " + original;
            }
            return null;

        }

        protected string listadoAgenda(string ubicacion, string titulo, string pais)
        {
            //Declaramos las variables a usar (en este caso el valor en la primera pagina)
            int inicadorA;
            int inicadorB;
            int inicadorC;
            int inicadorD;
            int registroColumna = 0;
            int columna = 1;

            if (primeraHoja)
            {
                inicadorA = 0;
                inicadorB = 33;
                inicadorC = 66;
                inicadorD = 99;
            }
            else
            {
                inicadorA = 0;
                inicadorB = 40;
                inicadorC = 80;
                inicadorD = 120;
            }


            StringBuilder CadenaRetorno = new StringBuilder();
            CadenaRetorno.Append(string.Empty);

            ControllerUsuarios COU = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable UDT = new DSUsuarios.UsuarioDataTable();
            UDT = COU.GetUsuariosByUbicacion(ubicacion);
            int registroActual = 0;
            int registrosTTotales = 0;
            int cantidadRegistros = UDT.Rows.Count;
            registrosTTotales = cantidadRegistros + acumulativo;
            if ( registrosTTotales >= 40 && !primeraHoja)
                CadenaRetorno.Append("<div style='height: 0; page-break-after:always;'>&nbsp;</div>");

            foreach (DSUsuarios.UsuarioRow registro in UDT.Rows)
            {
                if (registroColumna == inicadorA) 
                {
                    //open tabla a
                    CadenaRetorno.Append("<table>");
                        CadenaRetorno.Append("<thead><tr><th class='AspNet-DataList-Header'><h2>" + pais + "<span class='bg'></span></h2><h3><span>" + titulo + "</span></h3></th></tr></thead>");

                        CadenaRetorno.Append("<tbody><tr><td>");
                            //open tabla 100%
                            CadenaRetorno.Append("<table  width='100%'><tr>");
                }

                if (registroColumna == inicadorB || registroColumna == inicadorC || registroColumna == inicadorD)
                {
                                    //cierre 33%
                                    CadenaRetorno.Append("</table></td>");
                    columna++;
                }

                if (registroColumna == inicadorA || registroColumna == inicadorB || registroColumna == inicadorC)
                {
                    CadenaRetorno.Append("<td width='33%' valign='top'>");
                    //abre 33%
                    CadenaRetorno.Append("<table>");
                }

                                        CadenaRetorno.Append("<tr><td class='textominiatura'>" + registro.Apellido + ", " + registro.Nombre + " - " + registro.Interno + "</td></tr>");


                registroColumna++;
                acumulativo++;
                registroActual++;

                if (registroColumna == inicadorD || (registroActual == cantidadRegistros && acumulativo > inicadorC))
                {
                            //tabla de 100%
                            CadenaRetorno.Append("</table>");
                            CadenaRetorno.Append("</td></tr></table></tbody>");
                    CadenaRetorno.Append("</table>");
                    
                    //if (registroColumna > inicadorB)
                    CadenaRetorno.Append("<div style='height: 0; page-break-after:always;'>&nbsp;</div>");
                    acumulativo = 0;

                    registroColumna = inicadorA;
                    //Modificamos valor para las paginas siguientes a la primera
                    inicadorA = 0;
                    inicadorB = 40;
                    inicadorC = 80;
                    inicadorD = 120;
                    columna = 1;
                    primeraHoja = false;

                }


            }

            if (cantidadRegistros < inicadorD || cantidadRegistros < inicadorC || cantidadRegistros < inicadorB)
            {

                //tabla de 100%
                CadenaRetorno.Append("</table></td>");
                CadenaRetorno.Append("</table>");
                CadenaRetorno.Append("</td></tr></table></tbody>");
                CadenaRetorno.Append("</table>");


                registroColumna = inicadorA;


                primeraHoja = false;

            }
            if (registroColumna > inicadorB || (acumulativo >= 30 && cambioUbicacion))
            {
                CadenaRetorno.Append("<div style='height: 0; page-break-after:always;'>&nbsp;</div>");
                acumulativo = 0;
            }

            cambioUbicacion = true;
            return CadenaRetorno.ToString();
            

        }


    }
}
