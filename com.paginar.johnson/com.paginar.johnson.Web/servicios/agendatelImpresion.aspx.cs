﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Data;
//using Root.Reports;


namespace com.paginar.johnson.Web.servicios
{
    public partial class agendatelImpresion : PageBase
    {

        protected enum Cluster
        {
            Argentina = 1,
            Chile = 2,
            Uruguay = 3,
            Paraguay = 4
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request["Tipo"] == "Impresion")
                {
                    FillPersonas();
                    FillGeneral();
                }


                if (Request["Tipo"] == "Impresion")
                {
                    FillPersonas();
                    FillGeneral();
                    Impresion();
                }


                if (Request["Tipo"] == "PDF")
                {
                    FillPersonas();
                    FillGeneral();
                    
                }
            }

        }



        private void FillPersonas()
        {
            GetDatosUsuarios();
        }


        private void FillPersonasPDF()
        {
            GetDatosUsuarios();

            //Response.AddHeader("Content-Disposition", "attachment; filename=PressRelease.pdf");
            //Response.ContentType = "application/pdf";
            //Response.Write("I am here");
            ////Response.End();

        }

        private void GetDatosUsuarios()
        {
         
            ControllerUCHome cc = new ControllerUCHome();
            DAL.DsUCHome.UsuarioDataTable dtargentina = cc.GetBusquedaUsuarios("", (int)(Cluster.Argentina), "");

            DataView dvpodesta = new DataView(dtargentina, "ubicacionid='000'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvPodesta.DataSource = dvpodesta;
            gvPodesta.DataBind();

            DataView dvpaacheco = new DataView(dtargentina, "ubicacionid='003'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvPacheco.DataSource = dvpaacheco;
            gvPacheco.DataBind();

            DataView dvpilar = new DataView(dtargentina, "ubicacionid='600'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvPilar.DataSource = dvpilar;
            gvPilar.DataBind();

            if (dtargentina.Rows.Count > 0) pnlArgentina.Visible = true;
            else pnlArgentina.Visible = false;



            DAL.DsUCHome.UsuarioDataTable dtchile = cc.GetBusquedaUsuarios("", (int)(Cluster.Chile), "");

            DataView dvsantiago = new DataView(dtchile, "ubicacionid='201'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvSantiago.DataSource = dvsantiago;
            gvSantiago.DataBind();
            
            if (dtchile.Rows.Count > 0) pnlChile.Visible = true;
            else pnlChile.Visible = false;


            DAL.DsUCHome.UsuarioDataTable dturuguay = cc.GetBusquedaUsuarios("", (int)(Cluster.Uruguay), "");

            DataView dvuruguay = new DataView(dturuguay, "ubicacionid='300'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvUruguay.DataSource = dvuruguay;
            gvUruguay.DataBind();

            if (dturuguay.Rows.Count > 0) pnlUruguay.Visible = true;
            else pnlUruguay.Visible = false;


            DAL.DsUCHome.UsuarioDataTable dtparaguay = cc.GetBusquedaUsuarios("", (int)(Cluster.Paraguay), "");

            DataView dvparaguay = new DataView(dtparaguay, "ubicacionid='400'", "apellido asc,ubicaciondescripcion asc,direcciondescripcion asc", DataViewRowState.CurrentRows);
            gvParaguay.DataSource = dvparaguay;
            gvParaguay.DataBind();


            if (dtparaguay.Rows.Count > 0) pnlParaguay.Visible = true;
            else pnlParaguay.Visible = false;
        }


        private void GetDatosGeneral()
        {
            ControllerAgendaTel ca = new ControllerAgendaTel();
            gvGeneral.DataSource = ca.GetBusqueda(null, null, "");
            gvGeneral.DataBind();

        }


        private void FillGeneral()
        {
            GetDatosGeneral();
            
        }

        private void Impresion()
        {

            Response.Write("<script language='javascript'>");
            Response.Write("window.print()");
            Response.Write("</script>");
        }


        private void PDF()
        {
            //Report report= new Report(new PdfFormatter());          
            //FontDef fd= new FontDef(report,"Helvetica");
            //FontProp fp = new FontPropMM(fd, 25, System.Drawing.Color.Red);
            //Root.Reports.Page page = new Root.Reports.Page(report); 
            //page.AddCB_MM(80, new RepString(fp, this.Page.ToString()));
            //RT.ViewPDF(report, "Agenda.pdf");
            

        }



    }
}