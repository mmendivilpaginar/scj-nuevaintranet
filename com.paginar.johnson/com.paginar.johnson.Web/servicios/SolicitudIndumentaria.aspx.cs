﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Data;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class SolicitudIndumentaria : PageBase
    {
        int UsuarioId;
        DSIndumentaria.Indumentaria_PeriodoDataTable PeriodoActual;
        int PeriodoId;
        ControllerIndumentaria CI = new ControllerIndumentaria();
        ControllerTramites CT = new ControllerTramites();
        ControllerUCHome UCH = new ControllerUCHome();
        ControllerUsuarios CU = new ControllerUsuarios();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["TramiteID"] != null)
            {
                DSIndumentaria.IndumentariaPedidoDataTable di = CI.IndumentariaPedidoSeleccionar(int.Parse(Request["TramiteID"].ToString()));
                PeriodoActual = CI.IndumentariaPeriodoSeleccionarPorId(int.Parse(di[0]["PeriodoID"].ToString()));
                if(PeriodoActual.Rows[0]["Estado"].ToString().ToLower()=="false")
                    Disable();
            }
            else
            {
                PeriodoActual = CI.SeleccionarPeriodoActual();
            }


                if (PeriodoActual.Rows.Count == 0)
                    MVIndumentaria.SetActiveView(VSinPeriodo);
                else
                {
                    lblPeriodo.Text = "Período: " + procesarPeriodo(DateTime.Parse(PeriodoActual[0][1].ToString()), DateTime.Parse(PeriodoActual[0][2].ToString()));

                    CU.GetUsuarioByUsuarioID(ObjectUsuario.usuarioid);

                    DSUsuarios.SectorDataTable DSSector = CU.getSector(int.Parse(CU.GetUsuarioByUsuarioID(ObjectUsuario.usuarioid).Rows[0]["sectorid"].ToString()));

                    if (DSSector.Rows.Count > 0)
                        ltSector.Text = "Sector: " + DSSector.Rows[0]["descripcion"].ToString();
                       

                               


                    ltrlArea.Text = "Área: " + ((DsUCHome.UsuarioDataTable)UCH.getDataUsuario(ObjectUsuario.usuarioid)).Rows[0][4].ToString();
                    MVIndumentaria.SetActiveView(VTramite);
                    PeriodoId = int.Parse(PeriodoActual.Rows[0][0].ToString());
                    UsuarioId = int.Parse(ObjectUsuario.usuarioid.ToString());


                    if (!Page.IsPostBack)
                    {
                        DSIndumentaria.IndumentariaDataTable Indumentaria = CI.SeleccionarIndumentariaTodas();
                        CBItem1.Text = Indumentaria.Rows[0]["Descripcion"].ToString();
                        CBItem2.Text = Indumentaria.Rows[1]["Descripcion"].ToString();
                        CBItem3.Text = Indumentaria.Rows[2]["Descripcion"].ToString();
                        CBItem4.Text = Indumentaria.Rows[3]["Descripcion"].ToString();
                        CBItem1.Attributes.Add("onclick", "statusItem(" + CBItem1.ClientID + ".id," + tbItem1.ClientID + ".id)");
                        CBItem2.Attributes.Add("onclick", "statusItem(" + CBItem2.ClientID + ".id," + DDLItem2.ClientID + ".id)");
                        CBItem3.Attributes.Add("onclick", "statusItem(" + CBItem3.ClientID + ".id," + DDLItem3.ClientID + ".id)");
                        CBItem4.Attributes.Add("onclick", "statusItem(" + CBItem4.ClientID + ".id," + DDLItem4.ClientID + ".id)");
                        ProcesarTramitePeriodo(PeriodoActual);

                    }


                }
            
        }

        private string procesarPeriodo(DateTime Desde, DateTime Hasta)
        {
            string fecha = String.Format("{0:dd}/{0:MM}/{0:yyyy}", Desde, Desde, Desde);
            fecha = "Desde " + fecha + " hasta " + String.Format("{0:dd}/{0:MM}/{0:yyyy}", Hasta, Hasta, Hasta);
            return fecha;

        }


        private void ProcesarTramitePeriodo(DSIndumentaria.Indumentaria_PeriodoDataTable PeriodoActual)
        {
            if (PeriodoActual.Rows.Count > 0)
            {
                DSIndumentaria.frmTramiteDataTable Traminte2 = CI.IndumentariaTramiteActualSeleccionar(int.Parse(ObjectUsuario.usuarioid.ToString()), int.Parse(PeriodoActual.Rows[0][0].ToString()));
                
                if (Traminte2.Rows.Count > 0)
                {
                    

                    int tramiteID = int.Parse(Traminte2.Rows[0][0].ToString());
                    DSTramites.frmNuevoHistoricoDataTable Historico = CT.frmNuevoHistoricoUltimoByTramiteId(tramiteID);
                    if (Historico.Rows.Count > 0)
                    {
                        ltrlFechas.Text = "Trámite iniciado: " + ((DateTime)Traminte2.Rows[0][3]).ToShortDateString() + "<BR />" + "Última actualización: " + ((DateTime)Historico.Rows[0][2]).ToShortDateString();
                        tbObservaciones.Text = Historico.Rows[0][7].ToString();
                    }
                    else
                    {
                        ltrlFechas.Text = "Trámite iniciado: " + ((DateTime)Traminte2.Rows[0][3]).ToShortDateString();
                        tbObservaciones.Text = "";
                    }

                    
                    DSIndumentaria.IndumentariaPedidoDataTable PedidoActual = CI.IndumentariaPedidoSeleccionar(tramiteID);

                    foreach (DSIndumentaria.IndumentariaPedidoRow item in PedidoActual.Rows)
                    {
                        procesarProducto(item.IndumentariaId, item.Talle, item.esBrigadista);
                    }


                }
            }        
        }

        private void procesarProducto(int p, string valor, bool esBrigadista)
        {
            switch (p)
            {
                case 1:
                    CBItem1.Checked = true;
                    tbItem1.Attributes.Remove("disabled");
                    tbItem1.Text = valor;
                    
                    break;
                case 2:
                    CBItem2.Checked = true;
                    DDLItem2.Enabled = true;
                    DDLItem2.SelectedValue = valor;
                    
                    break;
                case 3:
                    CBItem3.Checked = true;
                    DDLItem3.Enabled = true;
                    DDLItem3.SelectedValue = valor;
                    
                    break;

                case 4:
                    CBItem4.Checked = true;
                    DDLItem4.Enabled = true;
                    DDLItem4.SelectedValue = valor;
                    break;
            }


            rblBrigadista.Items.FindByValue(esBrigadista.ToString().ToLower()).Selected = true;
            
            
            
            
        }

      
        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                PeriodoActual = CI.SeleccionarPeriodoActual();
                DSIndumentaria.frmTramiteDataTable Traminte = CI.IndumentariaTramiteActualSeleccionar(int.Parse(ObjectUsuario.usuarioid.ToString()), int.Parse(PeriodoActual.Rows[0][0].ToString()));
                int tramiteID;
                int his_secuencia;
                if (Traminte.Rows.Count == 0)
                {
                    tramiteID = int.Parse(CI.tramite_alta(ObjectUsuario.usuarioid, "ALTA", ObjectUsuario.usuarioid, ObjectUsuario.apellido, ObjectUsuario.nombre, ObjectUsuario.legajo, "", "").ToString());
                    his_secuencia = 1;
                    CI.IndumentariaActualizarPasoTramite(tramiteID, 1);
                }
                else
                {

                    tramiteID = int.Parse(Traminte.Rows[0][0].ToString());
                    his_secuencia = int.Parse(((DSTramites.frmNuevoHistoricoDataTable)CT.frmNuevoHistoricoUltimoByTramiteId(tramiteID)).Rows[0][1].ToString());
                    his_secuencia = his_secuencia + 1;
                }


                CT.InsertarPasoNuevoHistorico(tramiteID, ObjectUsuario.usuarioid, ObjectUsuario.usuarioid, "E", 1, tbObservaciones.Text, his_secuencia);


                CI.IndumentariaPedidoEliminar(tramiteID);

                if (CBItem1.Checked)
                    CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 1, tbItem1.Text.ToString(), bool.Parse(rblBrigadista.SelectedValue));
                if (CBItem2.Checked)
                    CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 2, DDLItem2.SelectedValue.ToString(), bool.Parse(rblBrigadista.SelectedValue));
                if (CBItem3.Checked)
                    CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 3, DDLItem3.SelectedValue.ToString(), bool.Parse(rblBrigadista.SelectedValue));
                if (CBItem4.Checked)
                    CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 4, DDLItem4.SelectedValue.ToString(), bool.Parse(rblBrigadista.SelectedValue));


                MVIndumentaria.SetActiveView(VExito);
                //ProcesarTramitePeriodo(PeriodoActual);
                Timer1.Enabled = true;
            }
            else
            { 
            
            }

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Response.Redirect("~/Default.aspx");
            
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }


        protected void Disable()
        {
            Panel1.Enabled = false;
            rblBrigadista.Enabled = false;
            btnCancelar.Visible = false;
            btnEnviar.Visible = false;
        }

    }

}