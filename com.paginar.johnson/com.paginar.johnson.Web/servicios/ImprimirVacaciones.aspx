﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirVacaciones.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.ImprimirVacaciones" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Listado de Vacaciones</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
</head>
<body>
    <form id="formNoticiasABM" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Reporte de solicitudes de vacaciones</h1>
            <img id="logo" src="../images/logoprint.gif" alt="Listado de Vacaciones"/>
        </div>
        
           <div id="noprint">
             <div class="controls">               
                <asp:Button ID="Button3" runat="server" OnClientClick="window.print(); return false;"
                    Text="Imprimir"  />
            </div>
           </div>


                    <asp:GridView ID="VacacionesGV"  runat="server" AutoGenerateColumns="False" >
                        <Columns>

                            <asp:BoundField DataField="Legajo" HeaderText="Legajo"  />
                            <asp:BoundField DataField="apellido" HeaderText="Apellido" />
                            <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                            <asp:BoundField DataField="area" HeaderText="Area" />
                            <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha"   DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="fechadesde" HeaderText="Fecha Desde"   DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="fechahasta" HeaderText="Fecha Hasta"   DataFormatString="{0:dd/MM/yyyy}" />
                            <asp:BoundField DataField="estado" HeaderText="Estado" />

                        </Columns>
                        <EmptyDataTemplate>
                            <div class="messages msg-info">
                                No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>


       </div>
    
    </form>
</body>
</html>
