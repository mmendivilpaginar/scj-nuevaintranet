﻿<%@ Page Title=""  validateRequest="false"  enableEventValidation="false" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master"
    AutoEventWireup="true" CodeBehind="agendatel.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.agendatel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        $(function () {
            $('a.load-local').cluetip({
                local: true,
                hideLocal: true,
                sticky: true,
                arrows: true,
                cursor: 'pointer',
                width: 300,
                mouseOutClose: true

            });

        });


        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(startRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function startRequest(sender, e) {
            //disable button during the AJAX call
            document.getElementById('<%=btnBuscar.ClientID%>').disabled = true;            
        }
        function endRequest(sender, e) {
            //re-enable button once the AJAX call has completed
            document.getElementById('<%=btnBuscar.ClientID%>').disabled = false;            
        }

    </script>
    <style type="text/css">
        #telefonos
        {
            height: 300px;
            overflow: auto;
        }
        #telefonos table
        {
            border: 1px solid #2A3C6D;
            border-collapse: collapse;
        }
        #telefonos table th, .normal td
        {
            border: 1px solid #2A3C6D;
            text-align: center;
        }
        #telefonos table th
        {
            text-transform: uppercase;
            padding-bottom: 10px;
            padding-left: 10px;
            padding-right: 10px;
            text-align: center;
        }
        
        
        #centrales
        {
            height: 120px;
            overflow: auto;
        }
        #centrales table
        {
            border: 1px solid #2A3C6D;
            border-collapse: collapse;
        }
        #centrales table th, .normal td
        {
            border: 1px solid #2A3C6D;
            text-align: center;
            padding-left: 10px;
        }
        #centrales table th
        {
            text-transform: uppercase;
            padding-bottom: 10px;
            padding-left: 10px;
            padding-right: 10px;
            text-align: center;
        }
    </style>
    <h2>
        Agenda Telef&oacute;nica</h2>
    <div class="box formulario">
        <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="btnBuscar">
            <div class="form-item leftHalf">
                <label>
                    <asp:Label ID="lblTipo" runat="server" Text="Tipo"></asp:Label></label>
                <asp:DropDownList ID="drpTipo" runat="server" OnSelectedIndexChanged="drpTipo_SelectedIndexChanged"
                    AutoPostBack="True">
                    <asp:ListItem>Personas</asp:ListItem>
                    <asp:ListItem>General</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item rightHalf">
                <label>
                    <asp:Label ID="lblPais" runat="server" Text="País"></asp:Label></label>
                <asp:UpdatePanel ID="updPais" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpPais" runat="server" AutoPostBack="True" DataSourceID="odsPais"
                            DataTextField="Descripcion" DataValueField="ID" AppendDataBoundItems="True" OnSelectedIndexChanged="drpPais_SelectedIndexChanged">
                            <asp:ListItem Value="0">Todos</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpTipo" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsPais" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getCluster" TypeName="com.paginar.johnson.BL.ControllerCluster">
                </asp:ObjectDataSource>
            </div>
            <div class="form-item full">
                
                <asp:UpdatePanel ID="updUbicacion" runat="server">
                    <ContentTemplate>
                    <label>
                    <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label></label>
                        <asp:DropDownList ID="drpUbicacion" runat="server" DataTextField="Descripcion" DataValueField="UbicacionID"
                            AppendDataBoundItems="True" DataSourceID="odsUbicacion" 
                            AutoPostBack="True" onselectedindexchanged="drpUbicacion_SelectedIndexChanged">
                            <asp:ListItem Value="0">Todas</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="drpUbicacionAgenda" runat="server" DataTextField="Descripcion"
                            DataValueField="AgendaUbicacionID" AppendDataBoundItems="True" DataSourceID="odsUbicacionAgenda">
                            <asp:ListItem Value="0">Todas</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpPais" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsUbicacionAgenda" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetUbicacionbyCluster" TypeName="com.paginar.johnson.BL.ControllerAgendaTel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="drpPais" Name="clusterid" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsUbicacion" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getUbicacionTodas" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="drpPais" Name="clusterid" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <div class="form-item full">
                <label>
                    <asp:Label ID="lblBuscar" runat="server" Text="Buscar"></asp:Label></label>
                <asp:TextBox ID="txtBuscar" runat="server"></asp:TextBox>
            </div>
            <div class="controls">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
            </div>
        </asp:Panel>
    </div>
    <!--/box-->
    <p>
        <asp:Image ID="imgTooltip" runat="server" ImageUrl="~/images/telephone.png" />
        <a class="load-local" href="#telefonos" rel="#telefonos">Telefonía IP</a> <span style="padding-left: 20px">
            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/server.png" />
            <a class="load-local" href="#centrales" rel="#centrales">Conmutadores</a> </span>
        <span style="padding-left: 320px">
            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/printer.png" />
            <%# Eval("UBICACIONDET")%>
                <a href="imprimirAgenda.aspx"
                title="Impresion" target="_blank">Imprimir Agenda Completa 
        </a>

                </span>
    </p>
        <asp:UpdatePanel ID="UPConmutadores" runat="server">
        <ContentTemplate>
            <%--<asp:Label ID="lblConmutadorUP" runat="server"></asp:Label>--%>
            <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ODSUbicacionData">
            <ItemTemplate>
                <asp:Label ID="Label9" Visible='<%# !string.IsNullOrEmpty(Eval("SubfijoTel").ToString()) %>' Text='<%# "Conmutador "+  Eval("Descripcion").ToString() +" " + Eval("SubfijoTel").ToString()%>' runat="server" ></asp:Label>
            </ItemTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ODSUbicacionData" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataByUbicacionID" 
                TypeName="com.paginar.johnson.DAL.DSAgendaTelTableAdapters.UbicacionTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_UbicacionID" Type="String" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Tramites" Type="Boolean" />
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_clusterID" Type="Int32" />
                    <asp:Parameter Name="Original_SubfijoTel" Type="String" />
                    <asp:Parameter Name="Original_escomedor" Type="Boolean" />
                    <asp:Parameter Name="Original_prefixip" Type="String" />
                    <asp:Parameter Name="Original_visible" Type="Boolean" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="UbicacionID" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Tramites" Type="Boolean" />
                    <asp:Parameter Name="DireccionID" Type="Int32" />
                    <asp:Parameter Name="clusterID" Type="Int32" />
                    <asp:Parameter Name="SubfijoTel" Type="String" />
                    <asp:Parameter Name="escomedor" Type="Boolean" />
                    <asp:Parameter Name="prefixip" Type="String" />
                    <asp:Parameter Name="visible" Type="Boolean" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="drpUbicacion" Name="UbicacionID" 
                        PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Tramites" Type="Boolean" />
                    <asp:Parameter Name="DireccionID" Type="Int32" />
                    <asp:Parameter Name="clusterID" Type="Int32" />
                    <asp:Parameter Name="SubfijoTel" Type="String" />
                    <asp:Parameter Name="escomedor" Type="Boolean" />
                    <asp:Parameter Name="prefixip" Type="String" />
                    <asp:Parameter Name="visible" Type="Boolean" />
                    <asp:Parameter Name="Original_UbicacionID" Type="String" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Tramites" Type="Boolean" />
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_clusterID" Type="Int32" />
                    <asp:Parameter Name="Original_SubfijoTel" Type="String" />
                    <asp:Parameter Name="Original_escomedor" Type="Boolean" />
                    <asp:Parameter Name="Original_prefixip" Type="String" />
                    <asp:Parameter Name="Original_visible" Type="Boolean" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="drpUbicacion" 
                EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="centrales">
        <table border="1">
            <tr>
                <th>
                    Ubicación
                </th>
                <th>
                    Teléfono
                </th>
            </tr>
            <tr>
                <td>
                    Argentina -  San Isidro
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 54 011) 4549 5600
                </td>
            </tr>
<%--            <tr>
                <td>
                    Argentina - Pacheco
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 54 03327) 449-700
                </td>
            </tr>--%>
            <tr>
                <td>
                    Argentina - Pilar
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 54 0230) 453-8300
                </td>
            </tr>
            <tr>
                <td>
                    Chile - Santiago
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 56 2) 370-5100
                </td>
            </tr>
            <tr>
                <td>
                    Uruguay
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 59 82) 622-4444
                </td>
            </tr>
            <tr>
                <td>
                    Paraguay
                </td>
                <td align="center" style="padding-left: 10px">
                    (00 59 521) 221-751
                </td>
            </tr>
            <tr>
                <td>
                    Centro de Distribución</td>
                <td align="center" >
                    (00 54 0230) 453- 8402</td>
            </tr>
        </table>
    </div>
    <div id="telefonos">
        <%# Eval("DireccionDET")%><%# Eval("areadesc")%>
        <table border="1">
            <tr>
                <th>
                    Ubicación
                </th>
                <th align="center">
                    Salida
                </th>
                <th align="center">
                    Prefijo
                </th>
            </tr>
       
            <tr>
                <td>
                    Australia
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    420
                </td>
            </tr>
            <tr>
                <td>
                    Belgium
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    660
                </td>
            </tr>
            <tr>
                <td>
                    Brazil (Rio)
                </td>
                <td align="center">
                    8
                </td>
                <td align="center">
                    725
                </td>
            </tr>
            <tr>
                <td>
                    Brazil (Sao Paulo)
                </td>
                <td align="center">
                    8
                </td>
                <td align="center">
                    726
                </td>
            </tr>
            <tr>
                <td>
                    Canada
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    340
                </td>
            </tr>
            <tr>
                <td>
                    Chile (Santiago)
                </td>
                <td align="center">
                    8
                </td>
                <td align="center">
                    730
                </td>
            </tr>
            <tr>
                <td>
                    China (Pudong)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    423
                </td>
            </tr>
            <tr>
                <td>
                    Colombia (Bogota)
                </td>
                <td align="center">
                    5
                </td>
                <td align="center">
                    735
                </td>
            </tr>
            <tr>
                <td>
                    Costa Rica
                </td>
                <td align="center">
                    66
                </td>
                <td align="center">
                    740
                </td>
            </tr>
            <tr>
                <td>
                    Cyprus
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    520
                </td>
            </tr>
            <tr>
                <td>
                    Czech Republic (Praag)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    663
                </td>
            </tr>
            <tr>
                <td>
                    Ecuador
                </td>
                <td align="center">
                    Directo
                </td>
                <td align="center">
                    745
                </td>
            </tr>
            <tr>
                <td>
                    Egypt
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    525
                </td>
            </tr>
            <tr>
                <td>
                    France
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    630
                </td>
            </tr>
            <tr>
                <td>
                    Germany
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    680
                </td>
            </tr>
            <tr>
                <td>
                    Ghana
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    535
                </td>
            </tr>
            <tr>
                <td>
                    Greece (Athens)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    655
                </td>
            </tr>
            <tr>
                <td>
                    Greece (Attica)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    656
                </td>
            </tr>
            <tr>
                <td>
                    Hong Kong (Wanchai)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    430
                </td>
            </tr>
            <tr>
                <td>
                    Hungary
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    662
                </td>
            </tr>
            <tr>
                <td>
                    Indonesia
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    434
                </td>
            </tr>
            <tr>
                <td>
                    Italy
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    640
                </td>
            </tr>
            <tr>
                <td>
                    Japan (YoKohama)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    440
                </td>
            </tr>
            <tr>
                <td>
                    Kenya
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    530
                </td>
            </tr>
            <tr>
                <td>
                    Korea
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    445
                </td>
            </tr>
            <tr>
                <td>
                    Malaysia
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    450
                </td>
            </tr>
            <tr>
                <td>
                    Mexico (Polanco)
                </td>
                <td align="center">
                    66
                </td>
                <td align="center">
                    350
                </td>
            </tr>
            <tr>
                <td>
                    Mexico (Toluca)
                </td>
                <td align="center">
                    66
                </td>
                <td align="center">
                    351
                </td>
            </tr>
            <tr>
                <td>
                    Netherlands (Europlant)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    650
                </td>
            </tr>
            <tr>
                <td>
                    New Zealand
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    453
                </td>
            </tr>
            <tr>
                <td>
                    Nigeria
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    540
                </td>
            </tr>
            <tr>
                <td>
                    Norway (Oslo)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    675
                </td>
            </tr>
            <tr>
                <td>
                    Paraguay
                </td>
                <td align="center">
                    Directo
                </td>
                <td align="center">
                    748
                </td>
            </tr>
            <tr>
                <td>
                    Phillipines
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    455
                </td>
            </tr>
            <tr>
                <td>
                    Poland
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    688
                </td>
            </tr>
            <tr>
                <td>
                    Peru
                </td>
                <td align="center">
                    5
                </td>
                <td align="center">
                    742
                </td>
            </tr>
            <tr>
                <td>
                    Portugal
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    673
                </td>
            </tr>
            <tr>
                <td>
                    Puerto Rico
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    355
                </td>
            </tr>
            <tr>
                <td>
                    Russia Federation
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    667
                </td>
            </tr>
            <tr>
                <td>
                    Singapore
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    458
                </td>
            </tr>
            <tr>
                <td>
                    South Africa (Fairland)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    550
                </td>
            </tr>
            <tr>
                <td>
                    South Africa (Rossyln)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    551
                </td>
            </tr>
            <tr>
                <td>
                    Spain
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    670
                </td>
            </tr>
            <tr>
                <td>
                    Sweden
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    685
                </td>
            </tr>
            <tr>
                <td>
                    Switzerland
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    690
                </td>
            </tr>
            <tr>
                <td>
                    Taiwan (Taipei)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    475
                </td>
            </tr>
            <tr>
                <td>
                    Taiwan (Taoyuan)
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    478
                </td>
            </tr>
            <tr>
                <td>
                    Thailand
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    480
                </td>
            </tr>
            <tr>
                <td>
                    Turkey
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    545
                </td>
            </tr>
            <tr>
                <td>
                    U.S. Minneapolis Sales Office
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    338
                </td>
            </tr>
            <tr>
                <td>
                    U.S. Racine
                </td>
                <td align="center">
                    8
                </td>
                <td align="center">
                    320
                </td>
            </tr>
            <tr>
                <td>
                    U.S. Waxdale
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    330
                </td>
            </tr>
            <tr>
                <td>
                    U.S. Rogers Arkansas
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    337
                </td>
            </tr>
            <tr>
                <td>
                    U.S. Washington D.C.
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    336
                </td>
            </tr>
            <tr>
                <td>
                    UK-Frimley
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    620
                </td>
            </tr>
            <tr>
                <td>
                    UK-Milton Park
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    622
                </td>
            </tr>
            <tr>
                <td>
                    Ukraine
                </td>
                <td align="center">
                    &nbsp;
                </td>
                <td align="center">
                    665
                </td>
            </tr>
            <tr>
                <td>
                    Uruguay
                </td>
                <td align="center">
                    Directo
                </td>
                <td align="center">
                    760
                </td>
            </tr>
            <tr>
                <td>
                    Venezuela(Caracas)
                </td>
                <td align="center">
                    8
                </td>
                <td align="center">
                    750
                </td>
            </tr>
            <tr>
                <td>
                    Venezuela (Maracay)
                </td>
                <td align="center">
                    2
                </td>
                <td align="center">
                    751
                </td>
            </tr>
        </table>
        <%# Eval("cargodet")%>
    </div>
    <asp:MultiView ID="mtResultados" runat="server">
        <asp:View ID="vUsuarios" runat="server">
            <asp:Panel ID="pnlArgentina" runat="server" Visible="False">
                <h3>
                    Argentina</h3>
                <asp:GridView ID="gvArgentina" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "agendatel.aspx?usuarioid=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("UBICACIONDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("DireccionDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>
                                                        <li>Interno:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicación" DataField="ubicaciondescripcion" />
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />
                        <%--<asp:BoundField HeaderText="Telefono" DataField="telefono" />--%>
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlChile" runat="server" Visible="False">
                <h3>
                    Chile</h3>
                <asp:GridView ID="gvChile" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "agendatel.aspx?usuarioid=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("UBICACIONDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("DireccionDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>
                                                        <li>Interno:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <%--<asp:BoundField HeaderText="Telefono" DataField="telefono" />--%>
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlParaguay" runat="server" Visible="False">
                <h3>
                    Paraguay</h3>
                <asp:GridView ID="gvParaguay" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "agendatel.aspx?usuarioid=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("UBICACIONDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("DireccionDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>
                                                        <li>Interno:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <%--<asp:BoundField HeaderText="Telefono" DataField="telefono" />--%>
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlUruguay" runat="server" Visible="False">
                Uruguay
                <asp:GridView ID="gvUruguay" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "agendatel.aspx?usuarioid=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("UBICACIONDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("DireccionDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>
                                                        <li>Interno:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <%--<asp:BoundField HeaderText="Telefono" DataField="telefono" />--%>
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Label ID="lblEmptyUsuarios" CssClass="messages msg-info" Text="No se encontraron Resultados"
                runat="server" Visible="false"></asp:Label>
        </asp:View>
        <asp:View ID="vGeneral" runat="server">
            <asp:GridView ID="gvGeneral" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvGeneral_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="detalle">
                        <ItemTemplate>
                            <asp:HyperLink ID="lblDetalle" runat="server" NavigateUrl='<%# "agendatel.aspx?agendatelid=" +Eval("agendatelid") %>'> <%# Eval("detalle") %></asp:HyperLink>
                            <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                            <asp:Panel ID="pnlAgenda" runat="server">
                                <div class="userCard">
                                    <asp:HiddenField ID="hdAgendaTelID" runat="server" Value='<%# Bind("agendatelid") %>' />
                                    <asp:FormView ID="frmAgenda" runat="server" DataSourceID="odsAgenda" Width="18px">
                                        <ItemTemplate>
                                            <div class="info">
                                                <ul>
                                                    <li>
                                                        <%# Eval("detalle")%>
                                                    </li>
                                                    <li>
                                                        <%# Eval("descripcion")%>
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsAgenda" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetDataByID" TypeName="com.paginar.johnson.BL.ControllerAgendaTel">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdAgendaTelID" Name="agendatelid" PropertyName="Value"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="categoriadescripcion" HeaderText="Categoría" />
                    <asp:BoundField DataField="ubicaciondescripcion" HeaderText="Ubicación" />
                    <%--<asp:BoundField DataField="telefono" HeaderText="Teléfono" />--%>
                    <asp:BoundField DataField="interno" HeaderText="Interno" />
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblEmptyGeneral" CssClass="messages msg-info" Text="No se encontraron Resultados"
                runat="server" Visible="false"></asp:Label>
        </asp:View>
        <asp:View ID="vUsuarioDetalle" runat="server">
            <div class="box resultados">
                <asp:FormView Width="100%" ID="FormViewUsuario" runat="server" DataKeyNames="UsuarioID"
                    DataSourceID="odsUsuario">
                    <ItemTemplate>
                        <div class="container">
                            <div class="header">
                                <h4>
                                    <asp:Label ID="Label1" runat="server" CssClass="QEQ_Nombre" Text='<%# Eval("NombreCompleto") %>'></asp:Label></h4>
                            </div>
                            <div class="content">
                                <div class="pic">
                                    <asp:Image ID="Image5" ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "/ResizeImage.aspx?Image=~/images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif"   %>'
                                        runat="server" Width="120" Height="120" />
                                </div>
                                <div class="info">
                                    <ul>
                                        <li runat="server" id="CtrlUbicacion" visible='<%# !(IsEmpty(Eval("UBICACIONDET"))) %>'>
                                            <strong>Ubicación: </strong>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("UBICACIONDET") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlDireccion" visible='<%# !(IsEmpty(Eval("DireccionDET"))) %>'>
                                            <strong>Dirección: </strong>
                                            <asp:Label ID="SectorLabel" runat="server" Text='<%# Bind("DireccionDET") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlArea" visible='<%# !(IsEmpty(Eval("AreaDESC"))) %>'>
                                            <strong>Área: </strong>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("AreaDESC") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlCargo" visible='<%# !(IsEmpty(Eval("CargoDET"))) %>'>
                                            <strong>Cargo: </strong>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("CargoDET") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlCumpleanos" visible='<%# (IsArgentina(Eval("Cluster"))) %>'>
                                            <strong>Cumpleaños: </strong>
                                            <asp:Label ID="Label6" runat="server" Text='<%# FormatearFecha(Eval("FHNacimiento")) %>'></asp:Label></li>
                                        <li runat="server" id="CtrlInterno" visible='<%# (!(IsEmpty(Eval("Interno")))) %>'>
                                            <strong>Interno: </strong>
                                            <asp:Label ID="LabelInterno" runat="server" Text='<%# Bind("Interno") %>'></asp:Label></li>
                                        <li runat="server" id="Li1" visible='<%# (!(IsEmpty(Eval("InternoIP")))) %>'>
                                            <strong>Interno IP: </strong>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>'></asp:Label></li>
                                        <li class="last" runat="server" id="CtrlEmail" visible='<%# (!(IsEmpty(Eval("Email")))) %>'>
                                            <strong>Email: </strong>
                                            <asp:HyperLink NavigateUrl='<%# Bind("Email","mailto:{0}") %>' Text='<%# Bind("Email") %>'
                                                ID="HyperLink1" runat="server"></asp:HyperLink>
                                            <asp:HiddenField ID="HiddenFieldUbicacionID" runat="server" Value='<%# Eval("UbicacionID") %>' />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                        
                    </ItemTemplate>
                </asp:FormView>
                <div class="messages msg-alerta">
                            Para consultas sobre esta sección o su contenido, por favor
                            contactar a <b>María del Carmen Aranguren</b>
                        </div>
                <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetUsuarioByUsuarioID" TypeName="com.paginar.johnson.BL.ControllerUsuarios"
                    OnSelecting="ObjectDataSourceUsuarioDet_Selecting">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="UsuarioID" QueryStringField="UsuarioID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <!--/resultados-->
        </asp:View>
        <asp:View ID="vGeneralDetalle" runat="server">
            <asp:FormView ID="frmAgenda" runat="server" DataKeyNames="AgendaTelID" DataSourceID="odsAgenda">
                <ItemTemplate>
                    Ubicación:
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("ubicaciondescripcion") %>' />
                    <br />
                    Categoría:
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("categoriadescripcion") %>' />
                    <br />
                    Detalle:
                    <asp:Label ID="DetalleLabel" runat="server" Text='<%# Bind("Detalle") %>' />
                    <br />
                    Descripcion:
                    <asp:Label ID="DescripcionLabel" runat="server" Text='<%# Bind("Descripcion") %>' />
                    <br />
                    <asp:Label ID="telefonoLabel" runat="server" Text='<%# Bind("telefono") %>' />
                    <br />
                    interno:
                    <asp:Label ID="internoLabel" runat="server" Text='<%# Bind("interno") %>' />
                    <br />
                </ItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="odsAgenda" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetDataByID" TypeName="com.paginar.johnson.BL.ControllerAgendaTel">
                <SelectParameters>
                    <asp:QueryStringParameter Name="agendatelid" QueryStringField="AgendaTelID" Type="Int32"
                        DefaultValue="0" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
    </asp:MultiView>
</asp:Content>
