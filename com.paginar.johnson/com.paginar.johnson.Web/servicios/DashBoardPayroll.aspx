﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="DashBoardPayroll.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.DashBoardPayroll" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="noticias" class="box">
    <h2>
        Administración Payroll</h2>

</div>

<iframe name="IAsp" runat="server" src="/SessionTransfer.aspx?dir=2aspx&url=Compensatorios/DashBoardPayroll.aspx" ClientIDMode="Static" id="IAsp"  width="100%"  marginheight="0" marginwidth="0" scrolling="auto" frameborder="0" allowTransparency="true">
    
  <p>Your browser does not support iframes.</p>
</iframe>

<asp:Label CssClass="destacado" ID="LabelAccesodenegado" runat="server" Text="Ud. no cuenta con los permisos necesarios para acceder a esta sección
"></asp:Label>
<script language='javascript'>

    function getDocHeight(doc) {
        var docHt = 0, sh, oh;
        if (doc.height) {
            docHt = doc.height;
        }
        else if (doc.body) {
            if (doc.body.scrollHeight) docHt = sh = doc.body.scrollHeight;
            if (doc.body.offsetHeight) docHt = oh = doc.body.offsetHeight;
            if (sh && oh) docHt = Math.max(sh, oh);
        }
        return docHt;
    }
    function getReSize() {
        var iframeWin = window.frames['IAsp'];
        var iframeEl = window.document.getElementById ? window.document.getElementById('IAsp') : document.all ? document.all['IAsp'] : null;
        if (iframeEl && iframeWin) {
            var docHt = getDocHeight(iframeWin.document);
            if (docHt != iframeEl.style.height) iframeEl.style.height = docHt + 20 + 'px';
        }
        else { // FireFox
            var docHt = window.document.getElementById('IAsp').contentDocument.height;
            window.document.getElementById('IAsp').style.height = docHt + 20 + 'px';
        }
    }
    function getRetry() {
        try
        {
            getReSize();
        }
        catch (err) {
            //Handle errors here
        }
        setTimeout('getRetry()', 500);
    }
    getRetry();
</script>
</asp:Content>
