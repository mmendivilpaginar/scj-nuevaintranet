﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imprimirVisor4.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.imprimirVisor4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Johnson Intranet</title>
    <!--<link href="../css/styles.css" rel="stylesheet" type="text/css" />-->
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>

</head>
<body>
<form id="form1" runat="server">
	<div id="wrapper">
        <div id="header" style="background-image:none;">
	        <div class="bg"></div>
	        <h1>Listado Detallado Ganancias</h1>
	        <img id="logo" src="../images/logoprint.gif" />
        </div>
					
        <div id="noprint" class="form-item leftHalf noPrintVisor">
	        <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
        </div>

			
		<div class="form-item rightHalf siPrintVisor">
            <h3 class="siPrintMes"><asp:Label ID="lblMesLiquidado" runat="server" Text=""></asp:Label></h3>

			<h3 class="siPrintGenerado"><asp:Label ID="lblGenerado" runat="server" Text=""></asp:Label></h3>
		</div>
		<br /><br />
		
        <div class="textoPlano">	
		<pre runat="server" class="noFormatoImpreso" ID="TextoFormateado" style="font-size: 10px"></pre>
        </div>
		<br /><br />

	</div>
			
</form>

</body>
</html>
