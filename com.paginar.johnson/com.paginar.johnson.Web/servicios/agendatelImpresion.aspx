﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agendatelImpresion.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.agendatelImpresion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Johnson Intranet</title>    
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
   

        <h3>Agenda Telefónica</h3>


            <asp:Panel ID="pnlArgentina" runat="server" >
            <h3>ARGENTINA</h3>    

                    
            <h3><asp:Label ID="lblPodesta" runat="server" Text="PLANTA PODESTÁ 4841 + N° INT ó CENTRO DE SERVICIOS COMPARTIDOS 5554-3800 ó 554 + N° INT"></asp:Label></h3>
            <asp:GridView ID="gvPodesta" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+", "+Eval("Nombre") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>

            <h3><asp:Label ID="lblPacheco" runat="server" Text="PLAZA LOGÍSTICA - PACHECO (00 54 03327) 449-700" ></asp:Label></h3>
            <asp:GridView ID="gvPacheco" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+","+Eval("Nombre") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>

            <h3><asp:Label ID="lblPîlar" runat="server" Text="PLANTA PILAR 0230 - 453 8300"></asp:Label></h3>
            <asp:GridView ID="gvPilar" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                    
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+","+Eval("Nombre") %>' ></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>



            </asp:Panel>
            <br />

            <asp:Panel ID="pnlChile" runat="server" >
            <h3>CHILE</h3>
            <h3><asp:Label ID="lblSantiago" runat="server" Text="SANTIAGO"></asp:Label></h3>
            <asp:GridView ID="gvSantiago" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+","+Eval("Nombre") %>' ></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>


            </asp:Panel>
            <br />
            <asp:Panel ID="pnlParaguay" runat="server" >
            <h3>PARAGUAY</h3>
            
            <asp:GridView ID="gvParaguay" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+","+Eval("Nombre") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>


            </asp:Panel>
            
            <asp:Panel ID="pnlUruguay" runat="server" >            
             <h3>URUGUAY</h3>

            <asp:GridView ID="gvUruguay" runat="server" AutoGenerateColumns="False" >
            <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:Label ID="lblnombreapellido" runat="server"  Text='<%# Eval("Apellido").ToString().ToUpper()+","+Eval("Nombre") %>' ></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />                        
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:BoundField HeaderText="Email" DataField="Email" />
            </Columns>
            </asp:GridView>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            </asp:Panel>

            <h3>GENERAL</h3>

            <asp:GridView ID="gvGeneral" runat="server" AutoGenerateColumns="False" >
                <Columns>
                    <asp:TemplateField HeaderText="detalle">
                        <ItemTemplate>
                            <asp:HyperLink ID="lblDetalle" runat="server" NavigateUrl='<%# "agendatel.aspx?agendatelid=" +Eval("agendatelid") %>'> <%# Eval("detalle") %></asp:HyperLink>
                            <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                            <asp:Panel ID="pnlAgenda" runat="server">
                                <div class="userCard">
                                    <asp:HiddenField ID="hdAgendaTelID" runat="server" Value='<%# Bind("agendatelid") %>' />
                                    <asp:FormView ID="frmAgenda" runat="server" DataSourceID="odsAgenda" Width="18px">
                                        <ItemTemplate>
                                            <div class="info">
                                                <ul>
                                                    <li>
                                                        <%# Eval("detalle")%>
                                                    </li>
                                                    <li>
                                                        <%# Eval("descripcion")%>
                                                    </li>
                                                </ul>
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsAgenda" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetDataByID" TypeName="com.paginar.johnson.BL.ControllerAgendaTel">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdAgendaTelID" Name="agendatelid" PropertyName="Value"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="categoriadescripcion" HeaderText="Categoría" />
                    <asp:BoundField DataField="ubicaciondescripcion" HeaderText="Ubicación" />
                    <asp:BoundField DataField="telefono" HeaderText="Teléfono" />
                    <asp:BoundField DataField="interno" HeaderText="Interno" />
                </Columns>
            </asp:GridView>

    </div>
    </form>
</body>
</html>
