﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="SolicitudIndumentaria.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.SolicitudIndumentaria" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        enableTextBoxIfChek();
    });

    function enableTextBoxIfChek() { 
        if($('#<%= CBItem1.ClientID %>').is(':checked'))
        {
            $('#<%= tbItem1.ClientID %>').removeAttr("disabled");
        }
    }

    function statusItem(id, changeStatusId) {
        //alert(id + "," + changeStatusId);
        if ($("#" + id).is(':checked')) {
        
            $("#" + changeStatusId).removeAttr("disabled");
        }
        else {
            $("#" + changeStatusId).val('');
            $("#" + changeStatusId).attr("disabled", "disabled");
            
        }
    }


    function CheckIfOk() {
        var IsValid = true;


        if ($('#<%= CBItem1.ClientID %>').is(':checked') && $('#<%= tbItem1.ClientID %>').val() == "") {
            IsValid = false;

        }

        if ($('#<%= CBItem2.ClientID %>').is(':checked') && $('#<%= DDLItem2.ClientID %>').val() == "") {
                IsValid = false;
        }

        if ($('#<%= CBItem3.ClientID %>').is(':checked') && $('#<%= DDLItem3.ClientID %>').val() == "") {
        IsValid = false;
    }

    if ($('#<%= CBItem4.ClientID %>').is(':checked') && $('#<%= DDLItem4.ClientID %>').val() == "") {
        IsValid = false;
    }


    if (!$('#<%= CBItem1.ClientID %>').is(':checked') && !$('#<%= CBItem2.ClientID %>').is(':checked') && !$('#<%= CBItem3.ClientID %>').is(':checked') && !$('#<%= CBItem4.ClientID %>').is(':checked'))
        IsValid = false;

        if (!IsValid)
            $('#MensajeError').show();

        return IsValid;

    }


    function chekAndLimit(elemento, max) {
        txt = elemento;
        MaxLength = max;
        if (txt.value.length > (MaxLength - 1)) {
            document.getElementById(elemento.id).value = document.getElementById(elemento.id).value.substring(0, MaxLength - 1);
        }
        else return true;

    }


</script>
<div id="wrapperIndumentaria">
<h2>Solicitud de Ropa de Trabajo</h2>

    <asp:MultiView ID="MVIndumentaria" runat="server">
        <asp:View ID="VExito" runat="server">
        <div><span class="messages msg-exito" id="msngadvice">Su tramite fue procesado satisfactoriamente. <br /> 
            Recuerde que Ud. podrá modificarlo mientras dure el período de carga.</span></div>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="10000" 
                ontick="Timer1_Tick">
            </asp:Timer>
        </asp:View>
        <asp:View ID="VSinPeriodo" runat="server">
        <div><span class="messages msg-info" id="Span1">No hay periodo de carga de solicitudes hablitado.</span></div>
        
        </asp:View>
        <asp:View ID="VTramite" runat="server">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnEnviar">

                    <div id="MensajeError" style="display:none"><span class="messages msg-error" id="Span2">Debe seleccionar al menos una prenda.<br /> En las prendas seleccionadas el talle es un campo obligatorio.</span></div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                        CssClass="messages msg-error" ValidationGroup="enviar" />      
<div class="form_mensaje">
                <asp:Label ID="lblPeriodo" runat="server" Text=""></asp:Label>
            <table>
            <tr>
                <td>
                    <asp:Literal ID="ltrlFechas" runat="server"></asp:Literal>
                    <br />
                    <asp:Literal ID="ltrlArea" runat="server"></asp:Literal>
                    <br />
                    <asp:Literal ID="ltSector" runat="server"></asp:Literal>
                </td>
            </tr>
            </table>
</div>


            

            <table class="form_talles">
            <tr>
                <td>
                    <asp:CheckBox ID="CBItem1" runat="server" ViewStateMode="Enabled" Visible="false" />
                </td>

                <td>
                    <asp:TextBox ID="tbItem1" runat="server" disabled="disabled" Visible="false"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="tbItem1_FilteredTextBoxExtender" 
                        runat="server" Enabled="True" TargetControlID="tbItem1" ValidChars="0123456789">
                    </asp:FilteredTextBoxExtender>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ErrorMessage="Deberá ingresar un talle de zapato entre 34 a 47." ControlToValidate="tbItem1" 
                        ValidationGroup="enviar" MaximumValue="47" MinimumValue="34">*</asp:RangeValidator>   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="CBItem2" runat="server" ViewStateMode="Enabled" />  
                </td>
    
                <td>
                    <asp:DropDownList ID="DDLItem2" runat="server" Enabled="False">
                        <asp:ListItem Selected="True" Value="">Seleccionar</asp:ListItem>
                        <asp:ListItem>38</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>42</asp:ListItem>
                        <asp:ListItem>44</asp:ListItem>
                        <asp:ListItem>46</asp:ListItem>
                        <asp:ListItem>48</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>52</asp:ListItem>
                        <asp:ListItem>54</asp:ListItem>
                        <asp:ListItem>56</asp:ListItem>
                        <asp:ListItem>58</asp:ListItem>
                        <asp:ListItem>60</asp:ListItem>
                        <asp:ListItem>62</asp:ListItem>
                        <asp:ListItem>64</asp:ListItem>
                        <asp:ListItem>66</asp:ListItem>
                    </asp:DropDownList>
                        
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="CBItem3" runat="server" ViewStateMode="Enabled" />
                </td>
              <%--  <td>
                    &nbsp;</td>--%>
                <td>
                    <asp:DropDownList ID="DDLItem3" runat="server" Enabled="False">
                        <asp:ListItem Selected="True" Value="">Seleccionar</asp:ListItem>
                        <asp:ListItem>XXXL</asp:ListItem>
                        <asp:ListItem>XXL</asp:ListItem>
                        <asp:ListItem>XL</asp:ListItem>
                        <asp:ListItem>L</asp:ListItem>
                        <asp:ListItem>M</asp:ListItem>
                        <asp:ListItem>S</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td >    <asp:CheckBox ID="CBItem4" runat="server" ViewStateMode="Enabled"/></td>
            <td >

                    <asp:DropDownList ID="DDLItem4" runat="server" Enabled="False">
                        <asp:ListItem Selected="True" Value="">Seleccionar</asp:ListItem>
                        <asp:ListItem>Masuculino</asp:ListItem>
                        <asp:ListItem>Femenino</asp:ListItem>
                    </asp:DropDownList>

                     
            </td>
            </tr>


            <tr class="pedidosEspeciales">
                <td colspan="2">
                    <label>Pedido Especial</label><br /><span>(*Si requiere indumentaria especial, infórmelo en este campo. Como ser Electricista, Personal de Laboratorio, etc.)</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="tbObservaciones" runat="server" MaxLength="199" Rows="6" 
                        TextMode="MultiLine"  onKeyDown="chekAndLimit(this,200)" ></asp:TextBox>
                </td>
            </tr>
            </table>
            <div class="brigadista">
                <label class="mv-10 block">Brigadista</label>
                    <asp:RadioButtonList ID="rblBrigadista" runat="server" ValidationGroup="enviar">
                        <asp:ListItem Value="true">si</asp:ListItem>
                        <asp:ListItem Value="false">no</asp:ListItem>
                    </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="rblBrigadista" ErrorMessage="Indique si es Brigadista" ForeColor="Red" 
                        ValidationGroup="enviar">*</asp:RequiredFieldValidator>     
            </div>
            
          


            <table class="form-end">
            <tr>
            <td style="width: 220px;">
            </td>
            <td>
                <asp:Button ID="btnEnviar" runat="server" Text="Enviar" 
                    onclick="btnEnviar_Click" OnClientClick="return CheckIfOk();" ValidationGroup="enviar" />
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                    onclick="btnCancelar_Click" />
            </td>
            </tr>
            </table>

            </asp:Panel>    
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
