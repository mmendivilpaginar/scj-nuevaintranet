﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="obrasocial.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.obrasocial" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" tagname="ucCategoriaNoticia" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>Obra social</h2>
    
    <h3>Auditor de la Obra Social Ceras Johnson</h3>
    <p>
        <img src="../images/obra-social.jpg" class="floatLeft" />
        <strong>Proveedor: Servicio para el Sistema de Salud, del Dr. Ángel R. D&#8217;Amico</strong>
    </p>
    <p>
        Dr. Rub&eacute;n A. D'Amico<br />
        Lunes, martes y jueves de 14 a 17 hs. en oficina de Obra Social<br>Teléfono: 4841-8000 interno 506.
    </p>
    <br />
    <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="50" runat="server" />    
</asp:Content>
