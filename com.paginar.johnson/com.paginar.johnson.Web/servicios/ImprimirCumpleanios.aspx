﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirCumpleanios.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.ImprimirCumpleanios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cumpleaños</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
    <style type="text/css" media="print">
    div.pagina {
    writing-mode: tb-rl;
    }
    </style> 
</head>
<body>
    <form id="form1" runat="server">
   <div id="wrapper">
        
        <div id="header">
            <div class="bgcumple"></div>
            <h1 class="Hcumple">Cumpleaños del Mes</h1>
            <!--img id="logo" src="../images/logoprint.gif" /-->
            <img id="ImgCumple" src="../images/logo.png" />
            
        </div>
        <div id="noprint">
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
            <br /><br />
        </div>
<%--        <div class="pagina">--%>
            <asp:Calendar ID="Calendar1" runat="server" BorderColor="#D9D9D9" 
                CaptionAlign="Left" ondayrender="Calendar1_DayRender" 
                onvisiblemonthchanged="Calendar1_VisibleMonthChanged" SelectionMode="None" 
                ShowNextPrevMonth="False" DayNameFormat="Full">
                <DayHeaderStyle CssClass="CalendarioDias" />
                <DayStyle BorderColor="#D9D9D9" BorderWidth="2px" HorizontalAlign="Left" 
                    VerticalAlign="Top" />
                <SelectedDayStyle Wrap="True" />
                <TitleStyle CssClass="titCalendario" />
                <TodayDayStyle BackColor="White" BorderColor="#D9D9D9" Wrap="True" />
            </asp:Calendar>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getCluster" 
                TypeName="com.paginar.johnson.BL.ControllerCluster"></asp:ObjectDataSource>
<%--        </div>  --%>  
    </div>
    </form>
</body>
</html>
