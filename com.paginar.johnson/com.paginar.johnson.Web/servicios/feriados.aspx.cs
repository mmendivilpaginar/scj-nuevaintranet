﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.servicios
{
    public partial class feriados : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //seteo el texto fijo según el país del cluster
            int anio;
            if (Request.QueryString["anio"] != null)
                anio = int.Parse(Request.QueryString["anio"]);
            else
                anio = 2012;//DateTime.Today.Year;                      

              mvClusterStaticText.ActiveViewIndex = this.ObjectUsuario.clusteridactual - 1;
              mvCluster2012.ActiveViewIndex = this.ObjectUsuario.clusteridactual - 1;

              if (anio == 2011)
                  {
                  mvClusterStaticText.Visible = true;
                  mvCluster2012.Visible = false;
              }

              else {
                  mvClusterStaticText.Visible = false;
                  mvCluster2012.Visible = true;
              }

        }
    }
}