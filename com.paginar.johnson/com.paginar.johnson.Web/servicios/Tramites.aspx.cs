﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.membership;
using System.IO;
using System.Text;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.servicios
{
    public partial class Tramites : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExportarButton.Enabled = false;
            FHSolicitudTextBox.Text = FHSolicitudTextBox.Text.Trim();
            UsuarioIDHiddenField.Value = this.ObjectUsuario.usuarioid.ToString();
            LegajoHiddenField.Value = this.ObjectUsuario.legajo.ToString();
            com.paginar.johnson.membership.Roles R = new Roles();
            TabPanelAdmUsuarios.Visible = (R.IsUserInRole(Page.User.Identity.Name, "Administradores generales") ||
                R.IsUserInRole(Page.User.Identity.Name, "RRHH") ||
                R.IsUserInRole(Page.User.Identity.Name, "Administrador Formularios"));



           
        }

        protected void ExportarButton_Click(object sender, EventArgs e)
        {
            
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page page = new Page();
            HtmlForm form = new HtmlForm();

            TramitesGV.EnableViewState = false;
            TramitesGV.AllowPaging = false;
            TramitesGV.DataBind();
            TramitesGV.Rows[0].Visible = false;
            //Le damos un poco de estilo al GV
            TramitesGV.HeaderRow.Style.Add("background-color", "#FFFFFF");
            TramitesGV.HeaderRow.Cells[0].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[1].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[2].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[3].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[4].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[5].Style.Add("background-color", "#ACADAD");
            TramitesGV.HeaderRow.Cells[6].Style.Add("background-color", "#ACADAD");
            for (int i = 0; i < TramitesGV.Rows.Count; i++)
            {

                GridViewRow row = TramitesGV.Rows[i];

                row.BackColor = System.Drawing.Color.White;

                row.Attributes.Add("class", "textmode");


                if (i % 2 != 0)
                {

                    row.Cells[0].Style.Add("background-color", "#DBDBDB");

                    row.Cells[1].Style.Add("background-color", "#DBDBDB");

                    row.Cells[2].Style.Add("background-color", "#DBDBDB");

                    row.Cells[3].Style.Add("background-color", "#DBDBDB");

                    row.Cells[4].Style.Add("background-color", "#DBDBDB");

                    row.Cells[5].Style.Add("background-color", "#DBDBDB");

                    row.Cells[6].Style.Add("background-color", "#DBDBDB");

                } 
                
            }

            TramitesGV.Columns[7].Visible = false;
            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(TramitesGV);

            page.RenderControl(htw);
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";

            DateTime Hoy = new DateTime();
            Hoy = DateTime.Now;

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + "Administracion_de_tramites_" + Hoy.ToShortDateString() + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(style); 
            Response.Write(sb.ToString());
            Response.End();
        
        }

        protected void BuscarButton_Click(object sender, EventArgs e)
        {
            LiteralResultados.Visible = true;
            TramitesGV.Visible = true;
            TramitesGV.DataBind();
            int rowscantidad = TramitesGV.Rows.Count;
            if (rowscantidad > 0)
            {
                ExportarButton.Enabled = true;
            }
            else 
            {
                ExportarButton.Enabled = false;
            }
        }

        protected void TramitesGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //HiddenField frmIDHiddenField = e.Row.FindControl("frmIDHiddenField") as HiddenField;
                //HiddenField frmaspHiddenField = e.Row.FindControl("frmaspHiddenField") as HiddenField;
                //HiddenField traidHiddenField = e.Row.FindControl("traidHiddenField") as HiddenField;
                //HyperLink IDLinkButton = e.Row.FindControl("IDLinkButton") as HyperLink;
                ////IDLinkButton.OnClientClick = "javascript:  popup(window.open('/SessionTransfer.aspx?dir=2asp&url=http://3721-johnson-intranet.desa2.paginar.org/scjconosur/loginautomatico.asp?Referer=" + "/servicios/" + frmaspHiddenField.Value + "?FormularioID=" + frmIDHiddenField.Value + "%26ASP=" + frmaspHiddenField.Value + "%26TramiteID =" + traidHiddenField.Value + "'));";
                //IDLinkButton.NavigateUrl = "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + frmaspHiddenField.Value + "?FormularioID=" + frmIDHiddenField.Value + "&ASP=" + frmaspHiddenField.Value + "&TramiteID=" + traidHiddenField.Value + "&FormEstado=NUEVO_PASO");
            }
        }

        protected void ODSTramites_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.Cancel = !this.IsPostBack;
        }

        public string VerificarEstadoTramite(string estado)
        {
            if (estado == "Enviado")
                    return "Cambiar";

                return " ";
        }

        public string VerificarEstadoUrl(string estado, string url)
        {
            if (estado == "Enviado")
                return url;
            return "#";
        }


        protected void BuscarVacaciones_Click(object sender, EventArgs e)
        {
            DateTime? fhdesde = null;
            DateTime? fhhasta = null;
            int? direccion = null;
            int? area = null;
            int? estado = null;
            int? legajo = null;
            string apellido = null;
            string nombre = null;

            if (ApellidoVactb.Text != "")
                apellido = ApellidoVactb.Text;

            if (NombreVactb.Text != "")
                nombre = NombreVactb.Text;


            if (LegajoVactb.Text != "")
                legajo = int.Parse(LegajoVactb.Text);

            if (EstadoDropDownListVac.SelectedValue != "")
                estado = int.Parse(EstadoDropDownListVac.SelectedValue);

            if (DireccionDropDownList.SelectedValue != "")
                direccion = int.Parse(DireccionDropDownList.SelectedValue);

            if (AreaDropDownList.SelectedValue != "")
                area = int.Parse(AreaDropDownList.SelectedValue);


            if (FHFechaDesde.Text.Trim() != "")
                fhdesde = DateTime.Parse(FHFechaDesde.Text);

            if (FHFechaHasta.Text.Trim() != "")
                fhhasta = DateTime.Parse(FHFechaHasta.Text);

            com.paginar.johnson.BL.ControllerTramites cn = new BL.ControllerTramites();


            VacacionesGV.DataSource = cn.getVacaciones(area, direccion, fhdesde, fhhasta, estado, legajo, apellido, nombre);
            VacacionesGV.DataBind();

            if (VacacionesGV.Rows.Count > 0)
            {
                ExportarVacaciones.Visible = true;
                ImprimirVacaciones.Visible = true;
                ImprimirVacaciones.Attributes.Add("onclick", "javascript:window.open('ImprimirVacaciones.aspx?FHDesde=" + FHFechaDesde.Text + "&FHHasta=" + FHFechaHasta.Text + "&Area=" + AreaDropDownList.SelectedValue + "&Direccion=" + DireccionDropDownList.SelectedValue + "&estado=" + EstadoDropDownListVac.SelectedValue + "&legajo=" + legajo + "&apellido=" + ApellidoVactb.Text + "&nombre=" + NombreVactb.Text + "');");

            }

            else
            {
                ExportarVacaciones.Visible = false;
                ImprimirVacaciones.Visible = false;

            }

        }

        protected void ExportarVacaciones_Click(object sender, EventArgs e)
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page page = new Page();
            HtmlForm form = new HtmlForm();

            GridView gvaux = new GridView();
            gvaux = VacacionesGV;


            DateTime? fhdesde = null;
            DateTime? fhhasta = null;
            int? direccion = null;
            int? area = null;
            int? estado = null;
            int? legajo = null;
            string apellido = null;
            string nombre = null;

            if (ApellidoVactb.Text != "")
                apellido = ApellidoVactb.Text;

            if (NombreVactb.Text != "")
                nombre = NombreVactb.Text;

            if (LegajoVactb.Text != "")
                legajo = int.Parse(LegajoVactb.Text);

            if (EstadoDropDownListVac.SelectedValue != "")
                estado = int.Parse(EstadoDropDownListVac.SelectedValue);

            if (DireccionDropDownList.SelectedValue != "")
                direccion = int.Parse(DireccionDropDownList.SelectedValue);

            if (AreaDropDownList.SelectedValue != "")
                area = int.Parse(AreaDropDownList.SelectedValue);


            if (FHFechaDesde.Text.Trim() != "")
                fhdesde = DateTime.Parse(FHFechaDesde.Text);

            if (FHFechaHasta.Text.Trim() != "")
                fhhasta = DateTime.Parse(FHFechaHasta.Text);

            com.paginar.johnson.BL.ControllerTramites cn = new BL.ControllerTramites();

            gvaux.DataSource = cn.getVacaciones(area, direccion, fhdesde, fhhasta, estado, legajo, apellido, nombre);
            gvaux.DataBind();



            gvaux.EnableViewState = false;
            gvaux.AllowPaging = false;
            gvaux.DataBind();
            //gvaux.Rows[0].Visible = false;
            //Le damos un poco de estilo al GV
            gvaux.HeaderRow.Style.Add("background-color", "#FFFFFF");
            gvaux.HeaderRow.Cells[0].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[1].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[2].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[3].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[4].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[5].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[6].Style.Add("background-color", "#ACADAD");
            gvaux.HeaderRow.Cells[7].Style.Add("background-color", "#ACADAD");
            for (int i = 0; i < gvaux.Rows.Count; i++)
            {

                GridViewRow row = gvaux.Rows[i];

                row.BackColor = System.Drawing.Color.White;

                row.Attributes.Add("class", "textmode");


                if (i % 2 != 0)
                {

                    row.Cells[0].Style.Add("background-color", "#DBDBDB");

                    row.Cells[1].Style.Add("background-color", "#DBDBDB");

                    row.Cells[2].Style.Add("background-color", "#DBDBDB");

                    row.Cells[3].Style.Add("background-color", "#DBDBDB");

                    row.Cells[4].Style.Add("background-color", "#DBDBDB");

                    row.Cells[5].Style.Add("background-color", "#DBDBDB");

                    row.Cells[6].Style.Add("background-color", "#DBDBDB");

                    row.Cells[7].Style.Add("background-color", "#DBDBDB");

                }

            }


            // Deshabilitar la validación de eventos, sólo asp.net 2
            page.EnableEventValidation = false;

            // Realiza las inicializaciones de la instancia de la clase Page que requieran los diseñadores RAD.
            page.DesignerInitialize();

            page.Controls.Add(form);
            Literal li = new Literal();
            li.Text = "<strong> Reporte de solicitudes de vacaciones </strong>  <BR><BR>";
            form.Controls.Add(li);
            form.Controls.Add(gvaux);

            page.RenderControl(htw);
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";

            DateTime Hoy = new DateTime();
            Hoy = DateTime.Now;

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + "Administracion_de_tramites_" + Hoy.ToShortDateString() + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(style);
            Response.Write(sb.ToString());
            Response.End();
        }

        
    }
}