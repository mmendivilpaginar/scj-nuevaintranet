﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.servicios
{
    public partial class agendatel : PageBase
    {
        protected enum Tipo
        {
            Personas=1,
            General=2

        }


        protected enum Cluster
        {
            Argentina = 1,
            Chile = 2,
            Uruguay = 3,
            Paraguay = 4
        }

        protected enum Tabs
        {
            Usuarios=0,
            General=1,
            UsuarioDetalle=2,
            GeneralDetalle=3
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                SeteaTipo(Tipo.Personas);
                
                if(Request["agendatelid"]!=null)
                {
                    SeteaTipo(Tipo.General);
                    mtResultados.ActiveViewIndex = (int)Tabs.GeneralDetalle;
                }

                if (Request["usuarioid"] != null)
                {
                    SeteaTipo(Tipo.Personas);
                    mtResultados.ActiveViewIndex = (int)Tabs.UsuarioDetalle;                    
                }

                if (Request["busqueda"] != null)
                {
                    txtBuscar.Text = Request["busqueda"].ToString();
                    BuscarAgenda();
                }
            }

        }


        protected string ProceseMail(object mail)
        {
            string mailto = string.Empty;
            if (mail.ToString().Length > 0)
                mailto = "<a href='mailto:" + mail.ToString().Trim() + "'>" + mail.ToString().Trim() + "</a>";
            return mailto;
        }

        protected void SeteaTipo(Tipo tipo)
        {
            if ( tipo == Tipo.Personas)
            {
                drpTipo.SelectedIndex = 0;
                drpUbicacion.Visible = true;
                drpUbicacionAgenda.Visible = false;
                mtResultados.ActiveViewIndex = (int)Tabs.Usuarios;

            }

            if (tipo == Tipo.General)
            {
                drpTipo.SelectedIndex = 1;
                drpUbicacion.Visible = false;
                drpUbicacionAgenda.Visible = true;
                mtResultados.ActiveViewIndex = (int)Tabs.General;
            }

                
        }


        protected void drpTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpTipo.SelectedIndex == 0) 
            { 
                SeteaTipo(Tipo.Personas);
                lblUbicacion.Text = "Ubicación";
            }
            if (drpTipo.SelectedIndex == 1)
            {
                SeteaTipo(Tipo.General);
                lblUbicacion.Text = "Categoría";
                //lblConmutadorUP.Text = "";
            }
        }

        protected void drpPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpUbicacion.Items.Clear();
            drpUbicacionAgenda.Items.Clear();

            ListItem li1 = new ListItem();
            li1.Value = "0";
            li1.Text = "Todas";

            ListItem li2 = new ListItem();
            li2.Value = "0";
            li2.Text = "Todas";

            drpUbicacion.Items.Add(li1);
            drpUbicacionAgenda.Items.Add(li2);

            drpUbicacion.DataBind();
            drpUbicacionAgenda.DataBind();
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (drpTipo.SelectedIndex == 0)
                {
                    BuscarUsuario();
                }

                if (drpTipo.SelectedIndex == 1)
                {
                    BuscarAgenda();
                }
            }

        }

        private void BuscarAgenda()
        {
            SeteaTipo(Tipo.General);

            ControllerAgendaTel ca = new ControllerAgendaTel();
            int? clusterid = null;
            int? ubicacionid = null;
            string busqueda = null;

            if (drpPais.SelectedIndex > 0) clusterid = int.Parse(drpPais.SelectedValue);

            if (drpUbicacionAgenda.SelectedIndex > 0) ubicacionid = int.Parse(drpUbicacionAgenda.SelectedValue);

            if (txtBuscar.Text != "") busqueda = txtBuscar.Text;

            gvGeneral.DataSource = ca.GetBusqueda(clusterid, ubicacionid, busqueda);
            gvGeneral.DataBind();


            if (gvGeneral.Rows.Count == 0 )
                lblEmptyGeneral.Visible = true;
            else
                lblEmptyGeneral.Visible = false;

        }


        protected void BuscarUsuario()
        {
            SeteaTipo(Tipo.Personas);

            if (Page.IsValid)
            {
                string ubicacionid = "";
                string busqueda = "";

                if (txtBuscar.Text != "") busqueda = txtBuscar.Text;
                if (drpUbicacion.SelectedIndex > 0) ubicacionid = drpUbicacion.SelectedValue;

                ControllerUCHome cc = new ControllerUCHome();

                pnlArgentina.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;

                gvArgentina.DataSource = null;
                gvArgentina.DataBind();
                gvChile.DataSource = null;
                gvChile.DataBind();
                gvParaguay.DataSource = null;
                gvParaguay.DataBind();
                gvUruguay.DataSource = null;
                gvUruguay.DataBind();

                if (drpPais.SelectedValue == "1" || drpPais.SelectedValue=="0")
                {
                    gvArgentina.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Argentina), ubicacionid);
                    gvArgentina.DataBind();
                    if (gvArgentina.Rows.Count > 0) pnlArgentina.Visible = true;
                }

                if (drpPais.SelectedValue == "2" || drpPais.SelectedValue == "0")
                {
                    gvChile.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Chile), ubicacionid);
                    gvChile.DataBind();
                    if (gvChile.Rows.Count > 0) pnlChile.Visible = true;
                }

                if (drpPais.SelectedValue == "3" || drpPais.SelectedValue == "0")
                {
                    gvUruguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Uruguay), ubicacionid);
                    gvUruguay.DataBind();
                    if (gvUruguay.Rows.Count > 0) pnlUruguay.Visible = true;
                }

                if (drpPais.SelectedValue == "4" || drpPais.SelectedValue == "0")
                {
                    gvParaguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Paraguay), ubicacionid);
                    gvParaguay.DataBind();
                    if (gvParaguay.Rows.Count > 0) pnlParaguay.Visible = true;
                }

                if (gvArgentina.Rows.Count == 0 && gvChile.Rows.Count == 0 && gvUruguay.Rows.Count == 0 && gvParaguay.Rows.Count == 0)
                    lblEmptyUsuarios.Visible = true;
                else
                    lblEmptyUsuarios.Visible = false;
              
            }
        }


        #region grillaUsuarios
        protected void gvGeneral_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HyperLink lblDetalle = (HyperLink)e.Row.FindControl("lblDetalle");
                Panel pnl = (Panel)e.Row.FindControl("pnlAgenda");
                pnl.Attributes.Add("style", "display:none");
                Literal ltjs = (Literal)e.Row.FindControl("ltjs");
                ltjs.Text = GetToolTipFunction(lblDetalle.ClientID, pnl.ClientID);
            }
        }


        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HyperLink lblnombreapellido = (HyperLink)e.Row.FindControl("lblnombreapellido");
                Panel pnl = (Panel)e.Row.FindControl("pnlUsuario");
                pnl.Attributes.Add("style", "display:none");
                Literal ltjs = (Literal)e.Row.FindControl("ltjs");
                ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
            }
        }

        protected bool IsEmpty(object cadena)
        {
            if (cadena != null)
            {

                return string.IsNullOrEmpty(cadena.ToString().Trim());
            }
            return true;
        }

        protected bool IsArgentina(object Cluster)
        {
            int cluster = int.Parse(Convert.ToString(Cluster));
            return (cluster == 1);
        }

        public string FormatearFecha(object fecha)
        {

            if (fecha != null)
            {
                DateTime FechaAux = DateTime.MinValue;
                if (DateTime.TryParse(Convert.ToString(fecha), out FechaAux))
                {
                    return string.Format("{0:dd} de {1:MMMM}", FechaAux, FechaAux);
                }
            }
            return string.Empty;


        }

        protected bool IsEqual(object cadena, string otracadena)
        {
            if (cadena != null)
            {
                return (Convert.ToString(cadena) == otracadena);
            }
            return false;
        }

        protected void ObjectDataSourceUsuarioDet_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.Cancel = string.IsNullOrEmpty(Request.QueryString["UsuarioID"]);
        }


        #endregion

        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

        protected void drpUbicacion_SelectedIndexChanged(object sender, EventArgs e)
        {


            //lblConmutadorUP.Text = "";


            //if (drpUbicacion.SelectedIndex == 5)
            //{
            //    lblConmutadorUP.Text = "Conmutador Podestá (00 54 011) 4841-8000";
            //}
            //if (drpUbicacion.SelectedIndex == 2)
            //{
            //    lblConmutadorUP.Text = "Conmutador Pacheco (00 54 03327) 449-700";
            //}
            //if (drpUbicacion.SelectedIndex == 4)
            //{
            //    lblConmutadorUP.Text = "Conmutador Pilar (00 54 0230) 538-300";
            //}
            //if (drpUbicacion.SelectedIndex == 6)
            //{
            //    lblConmutadorUP.Text = "Conmutador Santiago (00 56 2) 370-5100";
            //}
            //if (drpUbicacion.SelectedIndex == 7)
            //{
            //    lblConmutadorUP.Text = "Conmutador Uruguay (00 59 82) 900-5875";
            //}
            //if (drpUbicacion.SelectedIndex == 3)
            //{
            //    lblConmutadorUP.Text = "Conmutador Paraguay (00 59 521) 221-751";
            //}
            
        }

    }
}