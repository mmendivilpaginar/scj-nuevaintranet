﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="calendariofiscal.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.calendariofiscal" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Calendario Fiscal</h2>

                Para ver los calendarios fiscales de SCJ,
        <a href="https://collab.scj.com/Services/FormsLibrary/Service%20Documents/Forms/Fiscal%20Calendars.aspx" target="_blank" >
          click aquí.
        </a>

        <div style='display:none'>
    <asp:TreeView ID="TreeViewCalendario" runat="server" DataSourceID="RelationalSystemDataSource1"
        OnTreeNodeDataBound="TreeViewCalendario_TreeNodeDataBound" CssClass="adjuntos">
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    </div>
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
    <br />
    <%--       <a name="contenido-abajo"></a>
    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />            
          
             
             <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
               <ItemTemplate>    
                   <asp:Literal ID="ltContenido" runat="server"  Text='<%# Bind("Contenido") %>'  ></asp:Literal>
               </ItemTemplate>
             </asp:FormView>

            <asp:ObjectDataSource ID="odsContenido" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetContentById" 
                    TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" 
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>

    
    <br />



        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewCalendario" 
                EventName="SelectedNodeChanged" />
        </Triggers>
    </asp:UpdatePanel>

    --%>
</asp:Content>
