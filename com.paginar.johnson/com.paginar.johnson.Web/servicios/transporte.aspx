﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="transporte.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.transporte" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Transporte</h2>
    <asp:TreeView ID="TreeViewTransporte" runat="server" DataSourceID="RelationalSystemDataSource1"
        OnSelectedNodeChanged="TreeViewTransporte_SelectedNodeChanged">
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
    <br />
    <a name="contenido-abajo"></a>
    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
            <div id="divContenido" >
                <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                    <ItemTemplate>
                        <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewTransporte" EventName="SelectedNodeChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
