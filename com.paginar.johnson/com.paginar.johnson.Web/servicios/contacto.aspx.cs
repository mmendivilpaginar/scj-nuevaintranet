﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.utiles;


namespace com.paginar.johnson.Web.servicios
{
    public partial class contacto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string persona = string.Empty;
                persona = Master.ObjectUsuario.apellido + ", " + Master.ObjectUsuario.nombre;
                lblRemitente.Text = persona;
                lbllimitacion.Text = "Máximo de Caracteres 350";
                MVContacto.SetActiveView(VContacto);

            }
        }

        public void redirecciona()
        {
            string redirectionScript = "<script language='javascript'>" +  
            "function Delayer(){"+  
            "setTimeout('Redirection()', 3000);"+  
            "}"+  
            "function Redirection(){" +  
            "window.location = '/Default.aspx';" +  
            "}" +  
            "Delayer()" +  
            "</script>";  
            Page.RegisterStartupScript("Startup", redirectionScript);          
        }
        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool enviado = false;
                string cuerpo = tbMEnsaje.Text.ToString();
                string to = "rflbaron@paginar.net";//"test.G057040@scj.com_test";


                Message M = new Message(to, "Intranet Johnson: Área de Comunicaciones ", cuerpo);
                enviado = M.Send();
                tbMEnsaje.Visible = false;
                btnEnviar.Visible = false;
                btnCancelar.Visible = false;
                if (enviado)
                {
                    MVContacto.SetActiveView(VOk);
                    redirecciona();

                }
                else
                {
                    MVContacto.SetActiveView(Verror);
                    redirecciona();

                }
            }

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            MVContacto.SetActiveView(VCancel);
            redirecciona();
        }

    }
}