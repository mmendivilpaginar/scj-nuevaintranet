﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master"
    AutoEventWireup="true" CodeBehind="Tramites.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.Tramites" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">

    function abrirPMP(gstrASP, FormularioID, gstrgrabadolink) {
        //alert(gstrASP + ' ' + FormularioID + ' ' + gstrgrabadolink);
        ///SessionTransfer.aspx?dir=2asp%26url=servicios/form_PMPContributors.asp?FormularioID=19%26ASP=form_PMPContributors.asp&TramiteID=15548&FormEstado=NUEVO_PASO
        var url = "/SessionTransfer.aspx?dir=2asp&url=" + ('servicios/' + gstrASP + '?FormularioID=' + FormularioID + '%26ASP=' + gstrASP + "%26TramiteID=" + gstrgrabadolink + "%26FormEstado=NUEVO_PASO");
        url = url.replace(/&/, "%26")
        //alert(url);
        var win = window.open(url, "mywindow", "location=0,status=1,scrollbars=0,width=950,height=700,menubar=0,top=10,left=10");
        win.focus();
    }


    $(document).ready(function () {




        var id = $('#<%= FormulariosDropDownList.ClientID %>').val();

        seteaDivs(id);




            $('#<%= FormulariosDropDownList.ClientID %>').change(function () {
                seteaDivs($(this).val());
            });
    });


    function seteaDivs(id) {

        if (id== "8") {
            $("#divFormularios").hide();
            $("#divVacaciones").show();

            $("#divGrilla").hide();
            $("#divGrillaVacaciones").show();

        }
        else {

            $("#divFormularios").show();
            $("#divVacaciones").hide();

            $("#divGrilla").show();
            $("#divGrillaVacaciones").hide();

        }
    }


    </script> 
    <h2>
        Mis trámites</h2>
    <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" />
    <asp:HiddenField ID="LegajoHiddenField" runat="server" />
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" 
        Width="100%">
        <asp:TabPanel runat="server" HeaderText="Mis Trámites" ID="TabPanel1">
            <HeaderTemplate>
                Mis Trámites
            </HeaderTemplate>
            <ContentTemplate>
                <asp:TabContainer Width="100%" ID="TabContainer2" runat="server" ActiveTabIndex="0">
                    <asp:TabPanel ID="TabPanel21" runat="server" HeaderText="Iniciados">
                        <ContentTemplate>
                            <asp:TabContainer ID="TabContainer3" runat="server" ActiveTabIndex="0" Width="100%">
                                <asp:TabPanel runat="server" HeaderText="Abiertos" ID="TabPanel5">
                                    <ContentTemplate>
                                        <h4>
                                            Trámites Generales Abiertos</h4>
                                        <asp:GridView AllowPaging="true" PageSize="20" ID="IniciadosAbiertosGV" runat="server"
                                            DataSourceID="ODSTramitesIniciadosAbiertos" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tra_FHSolicitud" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio Trámite"
                                                    SortExpression="tra_FHSolicitud" />
                                                <asp:BoundField DataField="frm_Titulo" HeaderText="Formulario " SortExpression="frm_Titulo" />
                                                <%--<asp:BoundField DataField="Tra_Estado" HeaderText="Estado" SortExpression="Tra_Estado" />--%>
                                                <asp:BoundField DataField="UsuarioActual" HeaderText="Usuario Actual " SortExpression="UsuarioActual"
                                                    ReadOnly="True" />
                                                <asp:BoundField DataField="acc_Descripcion" HeaderText="Última Acción" SortExpression="acc_Descripcion" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites iniciados por Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSTramitesIniciadosAbiertos" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="INICIADOS_X_MI_ABIERTOS" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" HeaderText="Histórico" ID="TabPanel6">
                                    <ContentTemplate>
                                        <h4>
                                            Trámites Generales Cerrados</h4>
                                        <asp:GridView AllowPaging="true" PageSize="20" ID="IniciadosHistoricoGV" runat="server"
                                            DataSourceID="ODSTramitesIniciadosHistorico" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tra_FHSolicitud" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio Trámite"
                                                    SortExpression="tra_FHSolicitud" />
                                                <asp:BoundField DataField="frm_Titulo" HeaderText="Formulario " SortExpression="frm_Titulo" />
                                                <%--<asp:BoundField DataField="Tra_Estado" HeaderText="Estado" SortExpression="Tra_Estado" />--%>
                                                <asp:BoundField DataField="UsuarioActual" HeaderText="Usuario Actual " SortExpression="UsuarioActual"
                                                    ReadOnly="True" />
                                                <asp:BoundField DataField="acc_Descripcion" HeaderText="Última Acción" SortExpression="acc_Descripcion" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites iniciados por Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSTramitesIniciadosHistorico" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="INICIADOS_X_MI_CERRADOS" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                 <asp:TabPanel runat="server" HeaderText="Vacaciones y Dia SCJ" ID="TabPanel8">
                                    <ContentTemplate>
                                        <h4> Vacaciones</h4>

                                        <asp:GridView AllowPaging="true" PageSize="20" ID="TramitesVacacionesGV" runat="server"
                                            DataSourceID="ODSTramitesVacaciones" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha de Tramite" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField DataField="fechadesde" HeaderText="Fecha Desde" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField DataField="fechahasta" HeaderText="Fecha Hasta" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField DataField="estado" HeaderText="Estado" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites iniciados por Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                        <asp:ObjectDataSource ID="ODSTramitesVacaciones" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="TRAMITESVACACIONES" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>

                                        <h4> Dia SCJ</h4>

                                       <asp:GridView AllowPaging="true" PageSize="20" ID="DiaSCJ" runat="server"
                                            DataSourceID="ODSDiaSCJ" AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha de Tramite" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField DataField="FechaSolicitada" HeaderText="Fecha Solicitada" DataFormatString="{0:dd/MM/yyyy}" />
                                                <asp:BoundField DataField="estado" HeaderText="Estado" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites iniciados por Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                      <asp:ObjectDataSource ID="ODSDiaSCJ" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="TRAMITESDIASCJ" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>


                                      </ContentTemplate>
                                 </asp:TabPanel>            

                            </asp:TabContainer>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanel22" runat="server" HeaderText="Asignados">
                        <ContentTemplate>
                            <asp:TabContainer ID="TabContainer4" runat="server" ActiveTabIndex="0" Width="100%">
                                <asp:TabPanel runat="server" HeaderText="Abiertos" ID="TabPanel7">
                                    <ContentTemplate>
                                        <h4>
                                            Trámites Generales Asignados</h4>
                                        <asp:GridView AllowPaging="true" PageSize="20" ID="AsignadosGV" runat="server" DataSourceID="ODSTramitesAsignados"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tra_FHSolicitud" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio Trámite"
                                                    SortExpression="tra_FHSolicitud" />
                                                <asp:BoundField DataField="frm_Titulo" HeaderText="Formulario " SortExpression="frm_Titulo" />
                                                <asp:BoundField DataField="Tra_Estado" HeaderText="Estado" SortExpression="Tra_Estado" />
                                                <asp:BoundField DataField="UsuarioSolicitante" HeaderText="Solicitante" SortExpression="UsuarioSolicitante"
                                                    ReadOnly="True" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites asignados a Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSTramitesAsignados" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="ASIGNADOS_A_MI" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel ID="TabPanel23" runat="server" HeaderText="Histórico">
                                    <ContentTemplate>
                                        <h4>
                                            Trámites en los que he participado</h4>
                                        <asp:GridView AllowPaging="true" PageSize="20" ID="HistoricoGV" runat="server" DataSourceID="ODSTramitesHistorico"
                                            AutoGenerateColumns="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="tra_FHSolicitud" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio Trámite"
                                                    SortExpression="tra_FHSolicitud" />
                                                <asp:BoundField DataField="frm_Titulo" HeaderText="Formulario " SortExpression="frm_Titulo" />
                                                <asp:BoundField DataField="Tra_Estado" HeaderText="Estado" SortExpression="Tra_Estado" />
                                                <asp:BoundField DataField="UsuarioSolicitante" HeaderText="Solicitante" SortExpression="UsuarioSolicitante"
                                                    ReadOnly="True" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No tiene trámites asignados a Ud.
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSTramitesHistorico" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetTramitesByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="FRM_INVOLUCRADOS" Name="MODO" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer></ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPanel3" HeaderText="Formularios" runat="server">
            <ContentTemplate>
                <ul>
                    <asp:DataList ID="FormulariosDt" runat="server" DataSourceID="ODSFormularios" RepeatLayout="Flow">
                        <ItemTemplate>
                            <li>
                                <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Eval("frm_Titulo") %>'
                                  
                                    NavigateUrl='<%# ((Eval("FormularioID").ToString()!="17")&&(Eval("FormularioID").ToString()!="18")&&(Eval("FormularioID").ToString()!="19"))? "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("FormularioID") + "&ASP=" + Eval("frm_asp")):"ss"  %>'>                                
                                </asp:HyperLink></li>
                        </ItemTemplate>
                    </asp:DataList>
                </ul>

                <ul>
                 <BR />
                 <BR />
                  <div class="messages msg-info">
                  Para cualquier otro trámite por favor ingresar a Workday. 

                  <a HREF="https://fed.scj.com:9031/idp/startSSO.ping?PartnerSpId=http://www.workday.com" target="_blank"> 
                   Clic aquí
                  </a>
                  </div>
                </ul>
                <ul>
                  <a HREF="https://fed.scj.com:9031/idp/startSSO.ping?PartnerSpId=http://www.workday.com" target="_blank">
                   <div  align="center" >
                                        <asp:Image ID="Image1"  runat="server"   ImageUrl="~/images/Workday_logo_r.jpg" />
                
                   </div>
                  </a>
       
                </ul>

                <asp:ObjectDataSource ID="ODSFormularios" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetDataByUsuarioID" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmRelUsuarioFormularioTableAdapter">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPanel4" HeaderText="Mis Licencias" runat="server" Enabled="false">
            <ContentTemplate>
                <asp:TabContainer Width="100%" ID="TabContainer31" runat="server" ActiveTabIndex="0">
                    <asp:TabPanel runat="server" HeaderText="Vacaciones " ID="TabPanelLicencias1">
                        <ContentTemplate>
                            <h4>
                                Días pendientes por período</h4>
                            <asp:GridView ID="PendientesPorPeriodoGV" runat="server" AutoGenerateColumns="False"
                              >
                                <Columns>
                                    <asp:BoundField DataField="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio "
                                        ReadOnly="True" SortExpression="FechaInicio" />
                                    <asp:BoundField DataField="FechaFin" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fin"
                                        ReadOnly="True" SortExpression="FechaFin" />
                                    <asp:BoundField DataField="Saldo" HeaderText="Saldo" ReadOnly="True" SortExpression="Saldo" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="messages msg-info">
                                        No tiene licencias tomadas
                                    </div>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            
                            <asp:DataList ID="CorralitoGV" RepeatLayout="Flow" runat="server" AutoGenerateColumns="False"
                                >
                                <ItemTemplate>
                                    <h4>
                                        D&iacute;as de Corralito:
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Saldo") %>'></asp:Label></h4>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblEmpty" Visible='<%# bool.Parse((CorralitoGV.Items.Count==0).ToString())%>'
                                        Text="No tiene días de Corralito pendientes." runat="server" CssClass="messages msg-info"></asp:Label>
                                </FooterTemplate>
                            </asp:DataList>
                            
                            <br />
                            <p>
                                Recordamos que, según la política de la compañía, sólo puede transferirse hasta
                                1/3 del saldo de vacaciones al siguiente período. Te recomendamos que -de no haberlo
                                hecho aún- puedas completar el goce de las 2/3 partes de las vacaciones del corriente
                                período, a partir de un adecuado plan consensuado con tu jefe, para disfrutar de
                                tus vacaciones en el plazo establecido, y lograr un mejor balance entre tu vida
                                laboral y personal. Ante cualquier duda, comunicáte con Anahí Miraglia (Int. 8232
                                de Pablo Podestá). También podés consultar la Política de Vacaciones en la Intranet
                                (sección Nuestra Compañía - Políticas de Empresa - Capítulo III - Licencias).
                            </p>
                            <br />
                            <h4>
                                Días tomados</h4>
                            <asp:GridView ID="DiasTomadosGV" runat="server" AutoGenerateColumns="False" >
                                <Columns>
                                    <asp:BoundField DataField="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inicio "
                                        SortExpression="FechaInicio" />
                                    <asp:BoundField DataField="FechaFin" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fin"
                                        SortExpression="FechaFin" />
                                    <asp:BoundField DataField="Dias" HeaderText="Días " SortExpression="Dias" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="messages msg-info">
                                        No tiene licencias pedidas.
                                    </div>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSDiasTomados" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetDiasTomadosByLegajo" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.AR_PAGINAR3TableAdapter">
                                <InsertParameters>
                                    <asp:Parameter Name="Legajo" Type="Int32" />
                                    <asp:Parameter Name="FechaInicio" Type="DateTime" />
                                    <asp:Parameter Name="Dias" Type="Int32" />
                                    <asp:Parameter Name="FechaFin" Type="DateTime" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="LegajoHiddenField" Name="NroBusq1" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <br />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" HeaderText="Licencias Generales" ID="TabPanel2">
                        <ContentTemplate>
                            <h4>
                                Licencias Generales</h4>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" >
                                <Columns>
                                    <asp:BoundField DataField="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha"
                                        ReadOnly="True" SortExpression="Fecha" />
                                    <asp:BoundField DataField="Dias" HeaderText="Días" SortExpression="Dias" />
                                    <asp:BoundField DataField="Motivo" HeaderText="Motivo" SortExpression="Motivo" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="messages msg-info">
                                        No tiene licencias pedidas
                                    </div>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSLicenciasGenerales" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetLicenciaGeneralesByLegajo" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.AR_PAGINAR2TableAdapter">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPanelAdmUsuarios" runat="server" HeaderText="Administración de Trámites">
            <ContentTemplate>
                <h4>
                    Búsqueda</h4>
                <div class="box formulario">
                   <asp:Panel ID="BusquedaPanel" DefaultButton="BuscarButton" runat="server">
                    
                    <div class="form-item full">
                     
                        <label>
                            Formulario:</label>
                        <asp:DropDownList ID="FormulariosDropDownList" runat="server" DataSourceID="ODSForms"
                            DataTextField="frm_Titulo" AppendDataBoundItems="True" DataValueField="frm_ID">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ODSForms" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetFormulariosSinEvaluacion" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmFormularioTableAdapter"
                            UpdateMethod="Update">
                            <DeleteParameters>
                                <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                                <asp:Parameter Name="Original_frm_ASP" Type="String" />
                                <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                                <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                                <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                                <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                                <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                                <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="frm_ID" Type="Int32" />
                                <asp:Parameter Name="frm_ASP" Type="String" />
                                <asp:Parameter Name="frm_PrintASP" Type="String" />
                                <asp:Parameter Name="frm_Titulo" Type="String" />
                                <asp:Parameter Name="frm_Orden" Type="Int32" />
                                <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                                <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                                <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="frm_ASP" Type="String" />
                                <asp:Parameter Name="frm_PrintASP" Type="String" />
                                <asp:Parameter Name="frm_Titulo" Type="String" />
                                <asp:Parameter Name="frm_Orden" Type="Int32" />
                                <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                                <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                                <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                                <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                                <asp:Parameter Name="Original_frm_ASP" Type="String" />
                                <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                                <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                                <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                                <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                                <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                                <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                         
                    </div>
                   

                    <div  id="divFormularios" visible="true" >

                    <div class="form-item leftHalf">
                        <label>
                            Fecha de solicitud:</label>
                        <asp:TextBox ID="FHSolicitudTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                        <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                        <asp:CalendarExtender ID="FHSolicitudTextBox_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="FHSolicitudTextBox" DaysModeTitleFormat="dd/MM/yyyy" Format="dd/MM/yyyy"
                            PopupButtonID="IMGCalen1">
                        </asp:CalendarExtender>
                        <asp:CompareValidator ID="CompareValidator1" Text="*" runat="server" Operator="DataTypeCheck" Type="Date"  ControlToValidate="FHSolicitudTextBox"></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="FHSolicitudTextBox"
                            ErrorMessage="El formato de la fecha ingresada es incorrecto. El formato de fecha es dd/mm/yyyy."
                            Operator="DataTypeCheck" Type="Date" Display="Dynamic">*</asp:CompareValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                ErrorMessage="La fecha debe ser mayor a 01/01/1900" 
                                MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                                ControlToValidate="FHSolicitudTextBox" SetFocusOnError="True">*</asp:RangeValidator>


                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Estado:</label>
                        <asp:DropDownList ID="EstadoDropDownList" runat="server">
                            <asp:ListItem Value="">Todos</asp:ListItem>
                            <asp:ListItem Value="1">Abiertos</asp:ListItem>
                            <asp:ListItem Value="0">Cerrados</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Apellido del Solicitante:</label>
                        <asp:TextBox ID="ApellidoTextBox" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Nombre del Solicitante:</label>
                        <asp:TextBox ID="NombreTextBox" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Legajo del Solicitante:</label>
                        <asp:TextBox ID="LegajoTextBox" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="LegajoTextBox_FilteredTextBoxExtender" runat="server"
                            Enabled="True" FilterType="Numbers" TargetControlID="LegajoTextBox">
                        </asp:FilteredTextBoxExtender>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Jefe:</label>
                        <asp:DropDownList ID="JefeDropDownList" runat="server" DataSourceID="ODSJefes" 
                            DataTextField="ApellidoNombre" DataValueField="Legajo" AppendDataBoundItems="true">
                            <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ODSJefes" runat="server" DeleteMethod="Delete" 
                            InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" 
                            SelectMethod="GetData" 
                            TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.UsuarioTableAdapter" 
                            UpdateMethod="Update">
                            <DeleteParameters>
                                <asp:Parameter Name="Original_UsuarioID" Type="Int32" />
                                <asp:Parameter Name="Original_LoginID" Type="Int32" />
                                <asp:Parameter Name="Original_Apellido" Type="String" />
                                <asp:Parameter Name="Original_Nombre" Type="String" />
                                <asp:Parameter Name="Original_Email" Type="String" />
                                <asp:Parameter Name="Original_Estado" Type="Int32" />
                                <asp:Parameter Name="Original_FHNacimiento" Type="DateTime" />
                                <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioPadreID" Type="Int32" />
                                <asp:Parameter Name="Original_UbicacionID" Type="String" />
                                <asp:Parameter Name="Original_Legajo" Type="Int32" />
                                <asp:Parameter Name="Original_Interno" Type="String" />
                                <asp:Parameter Name="Original_ApruebaForms" Type="Int16" />
                                <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                <asp:Parameter Name="Original_EsPayRoll" Type="Int32" />
                                <asp:Parameter Name="Original_Cluster" Type="Int32" />
                                <asp:Parameter Name="Original_Foto" Type="String" />
                                <asp:Parameter Name="Original_EscalaSalarial" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioJefeID" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioFoto" Type="String" />
                                <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                <asp:Parameter Name="Original_ACargoDe" Type="Int32" />
                                <asp:Parameter Name="Original_EstaEnQeQ" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="LoginID" Type="Int32" />
                                <asp:Parameter Name="Apellido" Type="String" />
                                <asp:Parameter Name="Nombre" Type="String" />
                                <asp:Parameter Name="Email" Type="String" />
                                <asp:Parameter Name="Estado" Type="Int32" />
                                <asp:Parameter Name="FHNacimiento" Type="DateTime" />
                                <asp:Parameter Name="FHAlta" Type="DateTime" />
                                <asp:Parameter Name="UsrAlta" Type="Int32" />
                                <asp:Parameter Name="FHMod" Type="DateTime" />
                                <asp:Parameter Name="UsrMod" Type="Int32" />
                                <asp:Parameter Name="UsuarioPadreID" Type="Int32" />
                                <asp:Parameter Name="UbicacionID" Type="String" />
                                <asp:Parameter Name="Legajo" Type="Int32" />
                                <asp:Parameter Name="Interno" Type="String" />
                                <asp:Parameter Name="ApruebaForms" Type="Int16" />
                                <asp:Parameter Name="SectorID" Type="Int32" />
                                <asp:Parameter Name="EsPayRoll" Type="Int32" />
                                <asp:Parameter Name="Cluster" Type="Int32" />
                                <asp:Parameter Name="Foto" Type="String" />
                                <asp:Parameter Name="EscalaSalarial" Type="Int32" />
                                <asp:Parameter Name="UsuarioJefeID" Type="Int32" />
                                <asp:Parameter Name="UsuarioFoto" Type="String" />
                                <asp:Parameter Name="AreaID" Type="Int32" />
                                <asp:Parameter Name="DireccionID" Type="Int32" />
                                <asp:Parameter Name="CargoID" Type="Int32" />
                                <asp:Parameter Name="ACargoDe" Type="Int32" />
                                <asp:Parameter Name="EstaEnQeQ" Type="Int32" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="LoginID" Type="Int32" />
                                <asp:Parameter Name="Apellido" Type="String" />
                                <asp:Parameter Name="Nombre" Type="String" />
                                <asp:Parameter Name="Email" Type="String" />
                                <asp:Parameter Name="Estado" Type="Int32" />
                                <asp:Parameter Name="FHNacimiento" Type="DateTime" />
                                <asp:Parameter Name="FHAlta" Type="DateTime" />
                                <asp:Parameter Name="UsrAlta" Type="Int32" />
                                <asp:Parameter Name="FHMod" Type="DateTime" />
                                <asp:Parameter Name="UsrMod" Type="Int32" />
                                <asp:Parameter Name="UsuarioPadreID" Type="Int32" />
                                <asp:Parameter Name="UbicacionID" Type="String" />
                                <asp:Parameter Name="Legajo" Type="Int32" />
                                <asp:Parameter Name="Interno" Type="String" />
                                <asp:Parameter Name="ApruebaForms" Type="Int16" />
                                <asp:Parameter Name="SectorID" Type="Int32" />
                                <asp:Parameter Name="EsPayRoll" Type="Int32" />
                                <asp:Parameter Name="Cluster" Type="Int32" />
                                <asp:Parameter Name="Foto" Type="String" />
                                <asp:Parameter Name="EscalaSalarial" Type="Int32" />
                                <asp:Parameter Name="UsuarioJefeID" Type="Int32" />
                                <asp:Parameter Name="UsuarioFoto" Type="String" />
                                <asp:Parameter Name="AreaID" Type="Int32" />
                                <asp:Parameter Name="DireccionID" Type="Int32" />
                                <asp:Parameter Name="CargoID" Type="Int32" />
                                <asp:Parameter Name="ACargoDe" Type="Int32" />
                                <asp:Parameter Name="EstaEnQeQ" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioID" Type="Int32" />
                                <asp:Parameter Name="Original_LoginID" Type="Int32" />
                                <asp:Parameter Name="Original_Apellido" Type="String" />
                                <asp:Parameter Name="Original_Nombre" Type="String" />
                                <asp:Parameter Name="Original_Email" Type="String" />
                                <asp:Parameter Name="Original_Estado" Type="Int32" />
                                <asp:Parameter Name="Original_FHNacimiento" Type="DateTime" />
                                <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                                <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                                <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                                <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioPadreID" Type="Int32" />
                                <asp:Parameter Name="Original_UbicacionID" Type="String" />
                                <asp:Parameter Name="Original_Legajo" Type="Int32" />
                                <asp:Parameter Name="Original_Interno" Type="String" />
                                <asp:Parameter Name="Original_ApruebaForms" Type="Int16" />
                                <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                <asp:Parameter Name="Original_EsPayRoll" Type="Int32" />
                                <asp:Parameter Name="Original_Cluster" Type="Int32" />
                                <asp:Parameter Name="Original_Foto" Type="String" />
                                <asp:Parameter Name="Original_EscalaSalarial" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioJefeID" Type="Int32" />
                                <asp:Parameter Name="Original_UsuarioFoto" Type="String" />
                                <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                <asp:Parameter Name="Original_ACargoDe" Type="Int32" />
                                <asp:Parameter Name="Original_EstaEnQeQ" Type="Int32" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                       
                    </div>
                    <div class="controls">
                        <asp:Button ID="BuscarButton" runat="server" Text="Buscar" OnClick="BuscarButton_Click" />
                        <asp:Button ID="ExportarButton" runat="server" Text="Exportar" OnClick="ExportarButton_Click"/>
                    </div>

                    </div>



                      <%--Reporte Vacaciones--%>

                    <div  id="divVacaciones" visible="false">
                      <div class="form-item leftHalf">
                        <label>Fecha Desde:</label>
                        <asp:TextBox ID="FHFechaDesde" runat="server" SkinID="form-date"></asp:TextBox>
                        <asp:ImageButton ID="IMGCalen3" runat="server" ImageUrl="~/images/Calendar.png" />
                        <asp:CalendarExtender ID="FHFechaDesde_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="FHFechaDesde" DaysModeTitleFormat="dd/MM/yyyy" Format="dd/MM/yyyy"
                            PopupButtonID="IMGCalen3">
                        </asp:CalendarExtender>
                        <asp:CompareValidator ID="CompareValidator5" Text="*" runat="server" Operator="DataTypeCheck" Type="Date"  ControlToValidate="FHFechaDesde"></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="FHFechaDesde"
                            ErrorMessage="El formato de la fecha ingresada es incorrecto. El formato de fecha es dd/mm/yyyy."
                            Operator="DataTypeCheck" Type="Date" Display="Dynamic">*</asp:CompareValidator>
                            <asp:RangeValidator ID="RangeValidator3" runat="server" 
                                ErrorMessage="La fecha debe ser mayor a 01/01/1900" 
                                MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                                ControlToValidate="FHFechaDesde" SetFocusOnError="True">*</asp:RangeValidator>

                        <label>Fecha Hasta:</label>
                        <asp:TextBox ID="FHFechaHasta" runat="server" SkinID="form-date"></asp:TextBox>
                        <asp:ImageButton ID="IMGCalen2" runat="server" ImageUrl="~/images/Calendar.png" />
                        <asp:CalendarExtender ID="FHFechaHasta_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="FHFechaHasta" DaysModeTitleFormat="dd/MM/yyyy" Format="dd/MM/yyyy"
                            PopupButtonID="IMGCalen2">
                        </asp:CalendarExtender>
                        <asp:CompareValidator ID="CompareValidator2" Text="*" runat="server" Operator="DataTypeCheck" Type="Date"  ControlToValidate="FHFechaHasta"></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="FHFechaHasta"
                            ErrorMessage="El formato de la fecha ingresada es incorrecto. El formato de fecha es dd/mm/yyyy."
                            Operator="DataTypeCheck" Type="Date" Display="Dynamic">*</asp:CompareValidator>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" 
                                ErrorMessage="La fecha debe ser mayor a 01/01/1900" 
                                MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                                ControlToValidate="FHFechaHasta" SetFocusOnError="True">*</asp:RangeValidator>

                      </div>
                      
                    <div class="form-item rightHalf">
                        <label>
                            Estado:</label>
                        <asp:DropDownList ID="EstadoDropDownListVac" runat="server">
                            <asp:ListItem Value="">Todos</asp:ListItem>
                            <asp:ListItem Value="1">Abiertos</asp:ListItem>
                            <asp:ListItem Value="0">Cerrados</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                      
                       <div class="form-item leftHalf">
                      <label>Area:</label>
                          <asp:DropDownList ID="AreaDropDownList" runat="server" DataSourceID="ODSArea" DataTextField="AreaDESC"
                                DataValueField="AreaID"  AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ODSArea" runat="server"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.AreasTableAdapter">
                            </asp:ObjectDataSource>

                        </div>
                         <div class="form-item rightHalf">
                          <label>Dirección:</label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="DireccionDropDownList" runat="server"
                                DataSourceID="ODSDireccion" DataTextField="DireccionDET" DataValueField="DireccionID" >
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                         <asp:ObjectDataSource ID="ODSDireccion" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter">
                            </asp:ObjectDataSource>
                      </div>
                      <div class="form-item leftHalf">
                      <label>Apellido del Solicitante:</label>
                          <asp:TextBox ID="ApellidoVactb" runat="server"></asp:TextBox>

                      </div>
                      <div class="form-item rightHalf">
                      <label>Nombre del Solicitante:</label>
                          <asp:TextBox ID="NombreVactb" runat="server"></asp:TextBox>

                      </div>
                      <div class="form-item leftHalf">
                      <label>Legajo:</label>
                          <asp:TextBox ID="LegajoVactb" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="LegajoVactb_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="LegajoVactb" FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                      </div>
                       <div class="controls" >
                        <asp:Button ID="BuscarVacaciones" runat="server" Text="Buscar" OnClick="BuscarVacaciones_Click" />
                        <asp:Button ID="ExportarVacaciones" runat="server" Text="Exportar" OnClick="ExportarVacaciones_Click" Visible="false"/>
                        <asp:Button ID="ImprimirVacaciones" runat="server" Text="Imprimir"  Visible="false"/>
                      </div>
                    </div>


                     </asp:Panel>
                </div>
                <!--/formulario-->
                <div  id="divGrilla">
                <h4>
                    <asp:Literal ID="LiteralResultados" Visible="false" runat="server" Text="Resultados"></asp:Literal>
                    </h4>
                <asp:GridView ID="TramitesGV" Visible="false" runat="server" AutoGenerateColumns="False" DataSourceID="ODSTramites"
                    AllowPaging="True" PageSize="20">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                            <ItemTemplate>
                                <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# ((Eval("frm_ID").ToString()!="17")&&(Eval("frm_ID").ToString()!="18")&&(Eval("frm_ID").ToString()!="19"))? "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO"):"javascript:abrirPMP(\"" + Eval("frm_asp")+"\",\""+ Eval("frm_ID")+"\",\"" + Eval("tra_id") + "\");"  %>'>                                
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="tra_FHSolicitud" HeaderText="Fecha" SortExpression="tra_FHSolicitud"
                            DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="frm_Titulo" HeaderText="Formulario " SortExpression="frm_Titulo" />
                        <asp:BoundField DataField="Tra_Estado" HeaderText="Estado " ReadOnly="True" SortExpression="Tra_Estado" />
                        <asp:BoundField DataField="UsuarioSolicitante" HeaderText="Solicitante" SortExpression="UsuarioSolicitante" />
                        <asp:BoundField DataField="UsuarioActual" HeaderText="Usuario Actual" ReadOnly="True"
                            SortExpression="UsuarioActual" />
                        <asp:BoundField DataField="acc_descripcion" HeaderText="Última Acción" SortExpression="acc_descripcion" />
                        		<asp:TemplateField HeaderText="Cambiar Dest." SortExpression="CambiarDest">
			 <ItemTemplate>                                                                                                                         
				<asp:HyperLink ID="IDLinkButtonssCambiarExpr" runat="server" Text='<%# VerificarEstadoTramite(Eval("acc_descripcion").ToString())%>' NavigateUrl='<%# VerificarEstadoUrl(Eval("acc_descripcion").ToString(),"/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/form_ADMTramites.asp?FormEstado=CAMBIARDEST&TramiteID=" + Eval("tra_id")))  %>'>                                
				</asp:HyperLink>
			</ItemTemplate>
		</asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                    </div>
                </EmptyDataTemplate>
                </asp:GridView>
                <asp:ObjectDataSource ID="ODSTramites" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetTramitesByBusquedaADM" TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.frmTramiteTableAdapter"
                    OnSelecting="ODSTramites_Selecting">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="FormulariosDropDownList" Name="frm_ID" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="JefeDropDownList" Name="LegajoJefe" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="LegajoTextBox" Name="Legajo" PropertyName="Text"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="EstadoDropDownList" Name="tra_estado" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="FHSolicitudTextBox" Name="Fecha" PropertyName="Text"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="ApellidoTextBox" Name="Apellido" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="NombreTextBox" Name="Nombre" PropertyName="Text"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </div>


                <div  id="divGrillaVacaciones">
                    <asp:GridView ID="VacacionesGV"  runat="server" AutoGenerateColumns="False" >
                        <Columns>
                            <asp:TemplateField HeaderText="ID" SortExpression="tra_id">
                                <ItemTemplate>
                                    <asp:HyperLink ID="IDLinkButtonss" runat="server" Text='<%# Bind("tra_id") %>' NavigateUrl='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="apellido" HeaderText="Apellido" />
                            <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                            <asp:BoundField DataField="area" HeaderText="Area" />
                            <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha"   DataFormatString="{0:dd/MM/yyyy}" />
                           <%-- <asp:BoundField DataField="fechadesde" HeaderText="Fecha Desde"   DataFormatString="{0:dd/MM/yyyy}" /> --%>
                           <%-- <asp:BoundField DataField="fechahasta" HeaderText="Fecha Hasta"   DataFormatString="{0:dd/MM/yyyy}" /> --%>
                            <asp:BoundField DataField="UsuarioActual" HeaderText="Usuario Actual" />
                            <asp:BoundField DataField="ultimaAccion" HeaderText="Última Acción" />
                            <asp:BoundField DataField="estado" HeaderText="Estado" />

                        </Columns>
                        <EmptyDataTemplate>
                            <div class="messages msg-info">
                                No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                            </div>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>


            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
    <asp:LoginView ID="LoginView1" runat="server">
    </asp:LoginView>
</asp:Content>
