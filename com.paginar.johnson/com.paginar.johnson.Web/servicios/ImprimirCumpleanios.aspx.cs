﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Web.UI.WebControls.WebParts;

namespace com.paginar.johnson.Web.servicios
{
    public partial class ImprimirCumpleanios : PageBase
    {

        DateTime now = DateTime.Now;
        int? dia;
        int? mes;
        int? anio;
        int? cluster;

        protected void Page_Load(object sender, EventArgs e)
        {
            Calendar1.ShowNextPrevMonth = false;
            if (!Page.IsPostBack)
            {
                ControllerUCHome ch = new ControllerUCHome();
                

                
                dia = now.Day;
                anio = now.Year;
                
                if(Request.QueryString["mes"] != null)
                    mes = int.Parse(Request.QueryString["mes"]);
                else
                    mes = now.Month;

                if (Request.QueryString["cluster"] != null)
                    cluster = int.Parse(Request.QueryString["cluster"]);
                else
                    cluster = 0;
                string diaset = dia.ToString() + "/" + mes.ToString() + "/" + anio.ToString() +" 12:00:00 a.m.";
                DateTime fecha = DateTime.Parse(diaset);

                /*
                Calendar1.VisibleDate = fecha;
                if (now.Month.ToString() == mes.ToString())
                {
                    Calendar1.SelectedDate = fecha;
                }
                 */

                
            }
            else
            { 
            
            }

        }

        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {
           //Calendar calendar = (Calendar)sender;

            ControllerUCHome ch = new ControllerUCHome();
            DsUCHome.UsuarioDataTable USRT =  ch.getCumpleanioMes(mes, cluster);

            if (e.Day.IsOtherMonth != true)
            {
                foreach (DsUCHome.UsuarioRow item in USRT.Rows)
                {
                    if (item.dia == e.Day.DayNumberText)
                    {

                        string elliteral = "<strong>" + item.Apellido + ", " + item.Nombre + "</strong> - " + item.ubicaciondescripcion;
                        e.Cell.Controls.Add(new LiteralControl("<br />" + elliteral));

                    }
                }

            }
            else {
                e.Cell.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(0);
                e.Cell.Height = System.Web.UI.WebControls.Unit.Pixel(0);
                e.Cell.Text = "";
            }

        }

        protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
        {
            mes = e.NewDate.Month;

            dia = now.Day;

            anio = now.Year;
            string diaset = dia.ToString() + "/" + mes.ToString() + "/" + anio.ToString() + " 12:00:00 a.m.";
            DateTime fecha = DateTime.Parse(diaset);
            
            if (now.Month.ToString() == mes.ToString())
            {
                Calendar1.SelectedDate = fecha;
            }
        }
    }
}