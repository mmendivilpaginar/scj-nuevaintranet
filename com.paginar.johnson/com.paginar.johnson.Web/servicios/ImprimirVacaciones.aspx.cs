﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.servicios
{
    public partial class ImprimirVacaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                DateTime? fhdesde = null;
                DateTime? fhhasta = null;
                int? direccion = null;
                int? area = null;
                int? estado = null;
                int? legajo = null;
                string apellido = null;
                string nombre = null;


                if (Request["Area"].ToString()  != "")
                    area = int.Parse(Request["Area"].ToString());

                if (Request["Direccion"].ToString() != "")
                    direccion = int.Parse(Request["Direccion"].ToString());


                if (Request["FHdesde"].ToString() != "")
                    fhdesde = DateTime.Parse(Request["FHdesde"].ToString());

                if (Request["FHHasta"].ToString() != "")
                    fhhasta = DateTime.Parse(Request["FHHasta"].ToString());

                if (Request["estado"].ToString() != "")
                    estado = int.Parse(Request["estado"].ToString());

                if (Request["legajo"].ToString() != "")
                    legajo = int.Parse(Request["legajo"].ToString());

                if (Request["apellido"].ToString() != "")
                    apellido = Request["apellido"].ToString();

                if (Request["nombre"].ToString() != "")
                    nombre = Request["nombre"].ToString();               


                com.paginar.johnson.BL.ControllerTramites cn = new BL.ControllerTramites();


                VacacionesGV.DataSource = cn.getVacaciones(area, direccion, fhdesde, fhhasta, estado, legajo, apellido, nombre);
                VacacionesGV.DataBind();
            }

        }
    }
}