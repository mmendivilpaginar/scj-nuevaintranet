﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.servicios
{
    public partial class DashBoardPayroll : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            com.paginar.johnson.membership.Roles r = new com.paginar.johnson.membership.Roles();
            LabelAccesodenegado.Visible = !r.IsUserInRole(Page.User.Identity.Name, "Payroll Dias Compensatorios");
            IAsp.Visible = r.IsUserInRole(Page.User.Identity.Name, "Payroll Dias Compensatorios");

        }
    }
}