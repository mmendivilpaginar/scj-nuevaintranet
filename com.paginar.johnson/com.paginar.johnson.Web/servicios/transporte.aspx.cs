﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.servicios
{
    public partial class transporte : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TreeViewTransporte.Attributes.Add("onclick", "pageScroll()");
                ControllerContenido cc = new ControllerContenido();
                RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Transporte", this.ObjectUsuario.clusteridactual).ToString();
            }
        }

        protected void TreeViewTransporte_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewTransporte.SelectedNode.DataPath;
        }
    }
}