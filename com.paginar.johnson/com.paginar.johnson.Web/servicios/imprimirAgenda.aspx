﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imprimirAgenda.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.imprimirAgenda" %>

<%@ Register assembly="Microsoft.Web.DynamicData" namespace="Microsoft.Web.DynamicData" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agenda Telefónica</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Agenda Telefónica</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
     
        <div id="noprint">
            <asp:DropDownList ID="DDLUbicacion" runat="server" Width="200"
                onselectedindexchanged="DDLUbicacion_SelectedIndexChanged" 
                AutoPostBack="True">
                <asp:ListItem Value="999">Todos</asp:ListItem>
                <asp:ListItem Value="000">San Isidro</asp:ListItem>
                  <%-- <asp:ListItem Value="003">Pacheco</asp:ListItem>--%>
                <asp:ListItem Value="600">Pilar</asp:ListItem>
                <asp:ListItem Value="201">Santiago</asp:ListItem>
                <asp:ListItem Value="400">Paraguay</asp:ListItem>
                <asp:ListItem Value="300">Uruguay</asp:ListItem>
                <asp:ListItem Value="888">Generales</asp:ListItem>
            </asp:DropDownList>
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
            <%--<asp:Button ID="Button1" runat="server" Text="Imprimir" 
                onclick="Button1_Click"/>--%>
            <br /><br />
        </div>

        <%--<asp:Panel ID="PanelTituloArgentina" runat="server" >
            <h2>ARGENTINA<span class="bg"></span></h2>
        </asp:Panel>--%>

        <asp:Panel ID="pnlArgentinaPodesta" runat="server" >
            <asp:Label ID="lblPodesta" runat="server"></asp:Label>
        </asp:Panel>

        
        <asp:Panel ID="pnlArgentinaPacheco" runat="server" >
            <asp:Label ID="lblpacheco" runat="server"></asp:Label>       


        </asp:Panel>

        <asp:Panel ID="PanelArgentinaPilar" runat="server">
            <asp:Label ID="lblpilar" runat="server"></asp:Label>
        </asp:Panel>

        

        <asp:Panel ID="pnlChile" runat="server" >
            <asp:Label ID="lblchile" runat="server"></asp:Label>
        </asp:Panel>

        

        <asp:Panel ID="pnlParaguay" runat="server" >
            <asp:Label ID="lblparaguay" runat="server"></asp:Label>
        </asp:Panel>
            
        

        <asp:Panel ID="pnlUruguay" runat="server" >            
            <asp:Label ID="lbluruguay" runat="server"></asp:Label>
        </asp:Panel>
          <asp:Panel ID="pnlGenerales" runat="server">
            <div class="generales">
                <h2>GENERALES<span class="bg"></span></h2>
                        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ObjectDataSource1">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# cabeceraGeneral(DataBinder.Eval(Container.DataItem, "categoriadescripcion").ToString())%>
                                <tr>
                                    <td class="textominiatura"><%# DataBinder.Eval(Container.DataItem, "Detalle")%> <%# (DataBinder.Eval(Container.DataItem, "Telefono").ToString() != "0") ? ", " + DataBinder.Eval(Container.DataItem, "Telefono"): "" %><%# printInterno(DataBinder.Eval(Container.DataItem, "Interno").ToString())%>  <%# printDescripcion(DataBinder.Eval(Container.DataItem, "Descripcion").ToString())%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
             </tbody></table>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetBusqueda" 
                    TypeName="com.paginar.johnson.BL.ControllerAgendaTel">
                    <SelectParameters>
                        <asp:Parameter Name="clusterid" Type="Int32" />
                        <asp:Parameter Name="ubicacionid" Type="Int32" />
                        <asp:Parameter Name="busqeuda" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
          </asp:Panel>

      </div>
    </form>
</body>
</html>
