﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="feriados.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.feriados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <%--Multiview para los textos estáticos, debe estar en este orden para que el seteo desde el código funcione correctamente.--%>
    <asp:MultiView ID="mvClusterStaticText" runat="server">
        <asp:View ID="vArgentina" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Americas_Holidays_2011.xls">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2011</a> </li>
                <li><a target="_blank" href="documents/USA_Holidays.xls">Feriados
                    de Estados Unidos - 2011</a> </li>
            </ul>
            <br />
            <h3>Calendario de Argentina 2011:</h3>
            <div class="AspNet-GridView">
                <table>
                    <tr>
                        <th>
                            D&iacute;a Feriado
                        </th>
                        <th>
                            Se cumple el d&iacute;a
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <strong>1 de enero</strong> (A&ntilde;o Nuevo)
                        </td>
                        <td>
                            Sábado 1 de enero
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>7 y 8 de Marzo</strong> </span><span lang="ES-AR">(Carnaval)
                            </span>
                        </td>
                        <td>
                            Lunes y Martes 7 y 8 de Marzo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>24 de marzo</strong> </span><span lang="ES-AR">(Día Nacional
                                de la Memoria por la Verdad y la Justicia) </span>
                        </td>
                        <td>
                            Jueves 24 de marzo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>25 de Marzo</strong> </span><span lang="ES-AR">(Feriado Puente
                                Turístico) </span>
                        </td>
                        <td>
                            Viernes 25 de Marzo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>2 de abril</strong> </span><span lang="ES-AR">(D&iacute;a
                                del Veterano y los Ca&iacute;dos Guerra en Malvinas)</span>
                        </td>
                        <td>
                            Sábado 2 de abril
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>22 de abril</strong> </span><span lang="ES-AR">(Viernes Santo)
                            </span>
                        </td>
                        <td>
                            Viernes 22 de abril
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>1 de mayo</strong></span><span lang="ES-AR"> (D&iacute;a
                                del Trabajador) </span>
                        </td>
                        <td>
                            Domingo 1 de mayo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>25 de mayo</strong></span><span lang="ES-AR">&nbsp;&nbsp;(Primer
                                Gobierno Patrio) </span>
                        </td>
                        <td>
                            Miércoles 25 de mayo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>20 de junio</strong></span><span lang="ES-AR"> (D&iacute;a
                                de la Bandera) </span>
                        </td>
                        <td>
                            Lunes 20 de junio
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>9 de julio</strong></span><span lang="ES-AR"> (D&iacute;a
                                de la Independencia)</span>
                        </td>
                        <td>
                            Sábado 9 de julio
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>17 de agosto</strong></span><span lang="ES-AR"> (Movible)
                                (Paso a la Inmortalidad del Gral.San Mart&iacute;n) </span>
                        </td>
                        <td>
                            Lunes 22 de agosto
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>12 de octubre</strong></span><span lang="ES-AR"> (Movible)
                                (D&iacute;a del Respeto a la Diversidad Cultural) </span>
                        </td>
                        <td>
                            Lunes 10 de octubre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>20 de Noviembre</strong> </span><span lang="ES-AR">(Día de
                                la Soberanía Nacional) </span>
                        </td>
                        <td>
                            Lunes 28 de Noviembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>8 de diciembre</strong></span><span lang="ES-AR"> (Inmaculada
                                Concepci&oacute;n de Mar&iacute;a)</span>
                        </td>
                        <td>
                            Jueves 8 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>9 de diciembre</strong></span><span lang="ES-AR"> (Feriado
                                Puente Turístico)</span>
                        </td>
                        <td>
                            Viernes 9 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>25 de diciembre</strong> (Navidad)
                        </td>
                        <td>
                            Domingo 25 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td class="gris">
                            D&iacute;a no Laborable para SCJ
                        </td>
                        <td class="gris">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Jueves Santo</strong>
                        </td>
                        <td>
                            Jueves 21 de abril
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>24 de diciembre</strong>
                        </td>
                        <td>
                            Sábado 24 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>31 de diciembre</strong>
                        </td>
                        <td>
                            Sábado 31 de diciembre
                        </td>
                    </tr>
                </table>
            </div>
            <ul>
                <li><strong>FERIADOS FIJOS:</strong> que no se trasladan son los siguientes: 1 de enero,
                    24 de marzo, 2 de abril, Viernes Santo, 1 de mayo, 25 de mayo, 9 de julio, 8 de
                    diciembre y 25 de diciembre.</li>
                <li><strong>FERIADOS MOVIBLES:</strong> Se traslada al lunes anterior si cae en días
                    martes y miércoles. Se trasladan al lunes siguiente si caen en jueves y viernes.</li>
                <li><strong>DIA DE LA BANDERA Y DÍA DEL LIBERTADOR (ANIVERSARIO DE LA MUERTE DEL GRAL.
                    SAN MARTIN):</strong> Se trasladan siempre al tercer lunes del mes en que se celebran.</li>
                <li><strong>AÑO NUEVO JUDÍO: (ROSH HASHANA):</strong> dos (2) días y el DIA DEL PERDÓN
                    (IOM KIPUR) un (1) día. Son días no laborables para todos los habitantes de la Nación
                    Argentina que profesen la religión judía. (Ley 24.571) B.O. 30/10/95.</li>
                <li><strong>AÑO NUEVO MUSULMAN (HEGIRA):</strong> Un (1) día, el día posterior a la
                    culminación del ayuno (Id Al-Fitr) un (1) día y el día de la Fiesta del Sacrificio
                    (Id Al-Adha) un (1) día. Son días no laborables para todos los habitantes que profesen
                    la religión islámica. (Ley 24.757) B.O. 2/01/97.</li>
            </ul>
        </asp:View>
        <asp:View ID="vChile" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Americas_Holidays_2011.xls">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2011</a> </li>
                <li><a target="_blank" href="documents/USA_Holidays.xls">Feriados
                    de Estados Unidos - 2011</a> </li>
            </ul>
        </asp:View>
        <asp:View ID="vUruguay" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Americas_Holidays_2011.xls">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2011</a> </li>
                <li><a target="_blank" href="documents/USA_Holidays.xls">Feriados
                    de Estados Unidos - 2011</a> </li>
            </ul>
        </asp:View>
        <asp:View ID="vParaguay" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Americas_Holidays_2011.xls">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2011</a> </li>
                <li><a target="_blank" href="documents/USA_Holidays.xls">Feriados
                    de Estados Unidos - 2011</a> </li>
            </ul>
        </asp:View>

    </asp:MultiView>

    <asp:MultiView ID="mvCluster2012" runat="server">
        <asp:View ID="ViewArgentina" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Holidays2014Int.xls">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2014</a> </li>
              <%--  <li><a target="_blank" href="documents/USA_Holidays_2012.xls">Feriados
                    de Estados Unidos - 2012</a> </li>--%>
            </ul>
            <br />
            <h3>Calendario de Argentina 2014:</h3>
            <div class="AspNet-GridView">
                <table>
                    <tr>
                        <th>
                            D&iacute;a Feriado
                        </th>
                        <th>
                            Se cumple el d&iacute;a
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <strong>1 de enero</strong> (A&ntilde;o Nuevo)
                        </td>
                        <td>
                            Miercoles 1 de enero
                        </td>
                    </tr>
 
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>3 y 4 de Marzo</strong> </span>
                            <span lang="ES-AR">(Carnaval) </span>
                        </td>
                        <td>
                            Lunes y Martes 3 y 4 de Marzo
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>24 de marzo</strong> </span><span lang="ES-AR">(Día Nacional
                                de la Memoria por la Verdad y la Justicia) </span>
                        </td>
                        <td>
                            Lunes 24 de marzo
                        </td>
                    </tr>                   



                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>2 de abril</strong> (Día del Veterano y los Caídos 
                            Guerra en Malvinas)</span>&nbsp;</td>
                        <td>
                            Miercoles 2 de abril
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>17 de Abril</strong> (Jueves Santo) </span>
                        </td>
                        <td>
                           17 de Abril
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>18 de Abril</strong> (Viernes Santo) </span>
                        </td>
                        <td>
                            18 de Abril</td>
                    </tr>



                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>1 de mayo</strong></span><span lang="ES-AR"> (D&iacute;a
                                del Trabajador) </span>
                        </td>
                        <td>
                            Jueves 1 de mayo
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <span lang="ES-AR"><strong>2 de mayo</strong></span><span lang="ES-AR"> (Feriado Puente ) </span>
                        </td>
                        <td>
                            Viernes 2 de mayo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>25 de mayo</strong></span><span lang="ES-AR">&nbsp;&nbsp;(Primer
                                Gobierno Patrio) </span>
                        </td>
                        <td>
                            Domingo 25 de mayo
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>20 de junio</strong></span><span lang="ES-AR"> (D&iacute;a
                                de la Bandera) </span>
                        </td>
                        <td>
                            Viernes 20 de Junio
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>9 de julio</strong></span><span lang="ES-AR"> (D&iacute;a
                                de la Independencia)</span>
                        </td>
                        <td>
                            Miercoles 9 de julio
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>17 de agosto</strong> (Movible)</span><span lang="ES-AR"> 
                                (Paso a la Inmortalidad del Gral.San Mart&iacute;n) </span>
                        </td>
                        <td>
                            Lunes 18 de agosto
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>12 de octubre</strong> (Movible)</span><span lang="ES-AR"> 
                                (D&iacute;a del Respeto a la Diversidad Cultural) </span>
                        </td>
                        <td>
                            Lunes 13 de octubre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>20 de Noviembre</strong> (Movible) </span><span lang="ES-AR">(Día de
                                la Soberanía Nacional) </span>
                        </td>
                        <td>
                            Lunes 24 de Noviembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span lang="ES-AR"><strong>8 de diciembre</strong></span><span lang="ES-AR"> (Inmaculada
                                Concepci&oacute;n de Mar&iacute;a)</span>
                        </td>
                        <td>
                            Lunes 8 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>25 de diciembre</strong> (Navidad)
                        </td>
                        <td>
                            Jueves 25 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>26 de diciembre</strong> (Feriado Puente)
                        </td>
                        <td>
                            Viernes 26 de diciembre
                        </td>
                    </tr>
                    <!--tr>
                        <td class="gris">
                            D&iacute;a no Laborable para SCJ
                        </td>
                        <td class="gris">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Jueves Santo</strong>
                        </td>
                        <td>
                            Jueves 5 de abril
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>24 de diciembre</strong>
                        </td>
                        <td>
                            Lunes 24 de diciembre
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>31 de diciembre</strong>
                        </td>
                        <td>
                            Lunes 31 de diciembre
                        </td>
                    </tr-->
                </table>
            </div>
            <ul>
                <li><strong>FERIADOS FIJOS:</strong> que no se trasladan son los siguientes: 1 de enero,
                    24 de marzo, 2 de abril, Viernes Santo, 1 de mayo, 25 de mayo, 9 de julio, 8 de
                    diciembre y 25 de diciembre.</li>
                <li><strong>FERIADOS MOVIBLES:</strong> Se traslada al lunes anterior si cae en días
                    martes y miércoles. Se trasladan al lunes siguiente si caen en jueves y viernes.</li>
                <li><strong>DIA DE LA BANDERA Y DÍA DEL LIBERTADOR (ANIVERSARIO DE LA MUERTE DEL GRAL.
                    SAN MARTIN):</strong> Se trasladan siempre al tercer lunes del mes en que se celebran.</li>
                <li><strong>AÑO NUEVO JUDÍO: (ROSH HASHANA):</strong> dos (2) días y el DIA DEL PERDÓN
                    (IOM KIPUR) un (1) día. Son días no laborables para todos los habitantes de la Nación
                    Argentina que profesen la religión judía. (Ley 24.571) B.O. 30/10/95.</li>
                <li><strong>AÑO NUEVO MUSULMAN (HEGIRA):</strong> Un (1) día, el día posterior a la
                    culminación del ayuno (Id Al-Fitr) un (1) día y el día de la Fiesta del Sacrificio
                    (Id Al-Adha) un (1) día. Son días no laborables para todos los habitantes que profesen
                    la religión islámica. (Ley 24.757) B.O. 2/01/97.</li>
            </ul>
        </asp:View>
        <asp:View ID="ViewChile" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Holidays2014Int.xlsx">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2014</a> </li>
<%--                <li><a target="_blank" href="documents/USA_Holidays_2012.xls">Feriados
                    de Estados Unidos - 2012</a> </li>--%>
            </ul>
        </asp:View>
        <asp:View ID="ViewParaguay" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Holidays2014Int.xlsx">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2014</a> </li>
<%--                <li><a target="_blank" href="documents/USA_Holidays_2012.xls">Feriados
                    de Estados Unidos - 2012</a> </li>--%>
            </ul>
        </asp:View>
        <asp:View ID="ViewUruguay" runat="server">
            <h2>
                Feriados</h2>
            <ul>
                <li><a target="_blank" href="documents/Holidays2014Int.xlsx">Feriados
                    de los Cluster Andino, Cono Sur y Brasil - 2014</a> </li>
<%--                <li><a target="_blank" href="documents/USA_Holidays_2012.xls">Feriados
                    de Estados Unidos - 2013</a> </li>--%>
            </ul>
        </asp:View>

    </asp:MultiView>
</asp:Content>
