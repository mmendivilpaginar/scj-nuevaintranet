﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Horario.aspx.cs" MasterPageFile="~/MasterPages/TwoSidebars.master"
    Inherits="com.paginar.johnson.Web.servicios.Horario" Title="" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Horarios de Servicios</h2>
    
    <br />
    <h5>
        Planta Podestá</h5>
    <br />
    <h6>
        Servicios Generales</h6>
    <br />
    <ul>
        <li>Lunes a jueves de todo el año: de 8.30 a 12.30 y de 13.20 a 17.45.</li>
        <li>Viernes de invierno: de 8.30 a 12.30 y de 13.20 a 15.</li>
        <li>Viernes de verano: de 8.30 a 12.30.</li>
    </ul>
    <br />
    <h6>
        Mail Room</h6>
    <br />
    <ul>
        <li>Lunes a jueves de todo el año: de 8.30 a 12.30 y de 13.20 a 17.45.</li>
        <li>Viernes de invierno: de 8.30 a 12.30 y de 13.20 a 15.</li>
        <li>Viernes de verano: de 8.30 a 12.30.</li>
        <li style="margin-left: 30px; margin-top: 20px;">
            <h6>
                Recorrido diario de Mail Room por sectores:</h6>
            - Lunes a jueves de todo el año: de 10 a 11.00 y de 15 a 16.<br />
            - Viernes de todo el año: de 10 a 11.00. </li>
        <li style="margin-left: 30px; margin-top: 20px;">
            <h6>
                Bolsines inter-plantas (Pilar y Pacheco) e Interior del País:</h6>
            - Lunes a jueves de todo el año (desde Podestá): a Pilar y Pacheco: 12.00. Córdoba,
            Mendoza y Tucumán: 14.30. Bahía Blanca, Mar del Plata y Rosario. 17.00.<br />
            - Viernes de todo el año: salen todos a las 12.30. </li>
    </ul>
    <br />
    <h6>
        Centro de Copiado</h6>
    <br />
    <ul>
        <li>Lunes a jueves de todo el año: de 8.30 a 12.30 y de 13.15 a 18.</li>
        <li>Viernes de invierno: de 8.30 a 12.30 y de 13.15 a 17.</li>
        <li>Viernes de verano: de 8.30 a 14.00.</li>
    </ul>
    <br />
    <h6>
        Centro de Trámites</h6>
    <br />
    <ul>
        <li>Lunes a jueves de todo el año: de 8.30 a 12.30 y de 13.20 a 18.</li>
        <li>Viernes de invierno: de 8.30 a 12.30 y de 13.20 a 15.</li>
        <li>Viernes de verano: de 8.30 a 13.00.</li>
    </ul>
    <br />
    <h6>
        Archivo</h6>
    <br />
    <ul>
        <li>Lunes a jueves de todo el año: de 8.30 a 12.30 y de 13.20 a 18.</li>
        <li>Viernes de invierno: de 8.30 a 12.30 y de 13.20 a 15.</li>
        <li>Viernes de verano: de 8.30 a 12.30.</li>
    </ul>
    <br />
    <h6>
        Comedores</h6>
    <br />
    <ul>
        <li><b>Planta Podestá</b>: de 11.30 a 14.30 y de 19.30 a 21.</li>
        <li><b>Planta Pilar</b>: de 11.30 a 14.30 y de 19.30 a 21.</li>
        <li><b>Centro de Distribución</b>: de 12 a 13.30 y de 19.30 a 20.30.</li>
    </ul>
</asp:Content>
