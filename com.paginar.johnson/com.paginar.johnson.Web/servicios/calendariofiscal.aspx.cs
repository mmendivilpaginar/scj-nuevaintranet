﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.servicios
{
    public partial class calendariofiscal : PageBase
    {
        ControllerContenido c = new ControllerContenido();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {          
                ControllerContenido cc = new ControllerContenido();
                //Siempre se trae el de ARG
                RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Calendario Fiscal", 1).ToString(); 
            }
        }


        protected void TreeViewCalendario_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            //Siempre se trae el de ARG
            DAL.DsContenido.Content_ItemsDataTable dt = c.Content_ItemsGetMenu(int.Parse(e.Node.DataPath), 1);
            if (dt.Rows.Count > 0)
            {
                DAL.DsContenido.Content_ItemsRow dr = (DAL.DsContenido.Content_ItemsRow)dt.Rows[0];
                e.Node.NavigateUrl = dr.Contenido;
                e.Node.Target = "_blank";
            }
        }
    }
}