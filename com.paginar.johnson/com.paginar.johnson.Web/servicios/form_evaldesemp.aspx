﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master"  AutoEventWireup="true" CodeBehind="form_evaldesemp.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.form_evaldesemp"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <style type="text/css">

        .LockOff {
            display: none;
            visibility: hidden;
        }

        .LockOn {
            display: block; 
            visibility: visible;
            position: absolute;
            z-index: 999;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            background-color: #fff;
            text-align: center;
            padding-top: 20%;
            filter: alpha(opacity=75);
            opacity: 0.75;
        }

        .PrProgress{
            width:300px;
            margin:0 auto;
        }
        .PrContainer{
            border: solid 1px #808080;
            border-width: 1px 0px;
        }
        .PrHeader{
            background: url(../img/sprite.png) repeat-x 0px 0px;
            border-color: #808080 #808080 #ccc;
            border-style: solid;
            border-width: 0px 1px 1px;
            padding: 0px 10px;
            color: #000000;
            font-size: 9pt;
            font-weight: bold;
            line-height: 1.9;  
            white-space:nowrap;
            font-family: arial,helvetica,clean,sans-serif;
        }
        .PrBody{
            background-color: #f2f2f2;
            border-color: #808080;
            border-style: solid;
            border-width: 0px 1px;
            padding: 10px;
        }    
        
    </style>
    <script type="text/javascript" src="/PMP/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript">
        function skm_LockScreen(str) {
            var lock = document.getElementById('skm_LockPane');
            if (lock)
                lock.className = 'LockOn';
        }

        function Preloader() {
            var lock = document.getElementById('divPreloader');
            if (lock)
                lock.className = 'LockOn';
        }




        $(function () {
            

            $('.load-tools').cluetip({
                local: true,
                hideLocal: true,
                sticky: false,
                arrows: true,
                cursor: 'pointer',
                width: 300,
                mouseOutClose: true

            });

        });
    </script>    
    <script type="text/javascript">
     
    function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = textBox["MaxLength"];
            if (null == mLen)
                mLen = length;
            mLen = mLen - 1;
            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    alert("No puede superar los 500 caracteres en este campo");
                    if (window.event)//IE
                        e.returnValue = false;
                    else//Firefox
                        e.preventDefault();
                }
            }
        }
        function checkSpecialKeys(e) {
            if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                return false;
            else
                return true;
        }        


        function popupPmpO(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function popupPmpAdmRep(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpAdministrativos", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function popupPmpORep(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        $(document).ready(function () {
            //solucion: para contenido que supera el ancho del contenedor
            $('.AspNet-GridView > table').each(function (index) {
                //$(this).parent().css('width', ancho);
                //$(this).parent().css('margin-dright', '-500px');
                //$(this).parent().css('position', 'relative');
            });

        });

        function abrirPMP(gstrASP, FormularioID, gstrgrabadolink) {
            //alert(gstrASP + ' ' + FormularioID + ' ' + gstrgrabadolink);
            var url = "/SessionTransfer.aspx?dir=2asp&url=" + ('servicios/' + gstrASP + '?FormularioID=' + FormularioID + '%26ASP=' + gstrASP + gstrgrabadolink);
            //alert(url);
            var win = window.open(url, "mywindow", "location=0,status=1,scrollbars=0,width=950,height=700,menubar=0,top=10,left=10");
            win.focus();
        }

        function popupObjetivos(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PMPObjetivos", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function popupObjetivosAnt(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PMPObjetivosAnt", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        $(document).ready(function () {
            $(".solonumeros").keydown(function (event) {
                var charCode = (e.which) ? e.which : e.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }

            });
        });

       function TextAreaGrid() {

        var TextAreaGrid;
        var grid = document.getElementById('ObjetivosGV');
        if (!grid) return;
        if (grid.nodeName != "TABLE")
            grid = grid.children[0];


        if (grid.rows.length > 0) {
            for (i = 1; i < grid.rows.length; i++) {
                row = grid.rows[i];

                cell1 = grid.rows[i].cells[1];
                TextAreaGrid = cell1.childNodes[0];
                TextArea(TextAreaGrid.id, 499);
            
                cell1 = grid.rows[i].cells[2];
                TextAreaGrid = cell1.childNodes[0];
                TextArea(TextAreaGrid.id, 499);

                cell1 = grid.rows[i].cells[3];
                TextAreaGrid = cell1.childNodes[0];
                TextArea(TextAreaGrid.id, 499);
            } 
               
            }
        }

     function TextArea(txtArea,maxlength) {
      //tamano maximio de caracteres en textarea
         $('#' + txtArea).maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: maxlength, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "caracteres disponibles", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });
 
    //In-Field Label
    $('#' + txtArea).each(function () {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val($this.attr('title'));
        }
        $this.focus(function () {
            if ($this.val() == $this.attr('title')) {
                $this.val('');
            }
        });
        $this.blur(function () {
            if ($this.val() == '') {
                $this.val($this.attr('title'));
            }
        });
    });

}
$(document).ready(function () {
    $('#iniciados').removeAttr('display');
    $('#asignados').removeAttr('display');
    $('#historico').removeAttr('display');

});

function ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
    popup('/PMP/Impresion.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}



    </script> 

    
    
    <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" />
    <asp:HiddenField ID="LegajoHiddenField" runat="server" />
    <asp:HiddenField ID="HiddenFieldCluster" runat="server" />

    

    <h2>Evaluación de Desempeño</h2>
       <div id="skm_LockPane" class="LockOff">
            <div class="PrProgress">
                <div id="innerPopup" class="PrContainer">
                    <div class="PrHeader">
                        Subiendo archivo aguarde un instante.
                    </div>
                    <div class="PrBody">
                        <img src="../images/preloader.gif" alt="Subiendo..." />
                        <!--img width="220px" height="19px" src="Images/activity.gif" alt="Buscando..." /-->
                    </div>           
                </div>
            </div>
        </div>

       <div id="divPreloader" class="LockOff">
            <div class="PrProgress">
                <div id="innerPopup2" class="PrContainer">
                    <div class="PrHeader">
                        Cargando Datos...
                    </div>
                    <div class="PrBody">
                        <img src="../images/activity.gif" alt="Cargando.." />
                    </div>           
                </div>
            </div>
        </div>

    <asp:TabContainer ID="TabContainerprincipal" runat="server" ActiveTabIndex="0">
    <asp:TabPanel ID="TabPanelMisEvaluaciones" runat="server" Width="100%" HeaderText="Mis Evaluaciones">
    <ContentTemplate>
    <asp:TabContainer ID="TabContainerMisevaluaciones" runat="server" 
            ActiveTabIndex="1" CssClass=""><asp:TabPanel Width="100%" HeaderText="Mid Year" runat="server" ID="TabPanelMisEvaluacionesMidYear"><ContentTemplate>
    
    <asp:TabContainer ID="TabContainerMisEvaluacionesMY" runat="server" ActiveTabIndex="0" ssClass="">
                <asp:TabPanel runat="server" HeaderText="Iniciados" ID="TabPanelMYIniciados">
                    <HeaderTemplate>
                    <a id="iniciadoscontool" class="load-tools"  style="text-decoration: none;"  href="#iniciados" rel="#iniciados">A</a>
                    <asp:Label ID="lblIniciados" runat="server" Text="Iniciados"></asp:Label>
                    <%--<asp:Label ID="Label1" runat="server" ToolTip="Las evaluaciones que se encuentren en INICIADOS, Son aquellas que fueron iniciadas por el usuario logueado. Con dos estados posibles de la misma, ABIERTA y/o NO INICIADA." Text="Iniciados"></asp:Label>--%>
                        
                        <div id="iniciados" style="display:none">
                            <p>Las evaluaciones que se encuentren en INICIADOS,<br />
                            Son aquellas que fueron iniciadas por el usuario logueado.<br />
                            Con dos estados posibles de la misma, ABIERTA y/o NO INICIADA</p>
                        </div>

                    
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:GridView ID="GridViewIniciados" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                            CellSpacing="1" Width="420px" DataSourceID="ODSIniciadasAbiertas" 
                            OnRowDataBound="GridViewIniciados_RowDataBound">
                            <Columns>
                              <asp:TemplateField>
                                <ItemTemplate>
                                        <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="~/PMP/img/editar.jpg" />
                               </ItemTemplate>

                               <HeaderStyle CssClass="destacados" />
                           </asp:TemplateField>
                                <asp:TemplateField HeaderText="Período">
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="PeriodoDesc" Text="Período">
                                            </asp:LinkButton></strong>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3123" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal>
                                </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Legajo" Visible="False">
                                <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="Legajo" Text="Legajo">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Apellido y Nombre" Visible="False">
    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Apellido y Nombre">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Formulario">
    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Literal ID="Literal3l" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Sector" Visible="False">
    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoSector" Text="Sector">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Cargo" Visible="False">
    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta">
    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                            </asp:LinkButton></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Evaluador">
    <HeaderTemplate>
                                        <strong>
                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluador">
                                            </asp:Label></strong>
</HeaderTemplate>
<ItemTemplate>
                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Estado">
    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado1" runat="server" Text='<%# DameEstado(Eval("EstadoID")) %>'></asp:Label>
                                     
</ItemTemplate>

<HeaderStyle CssClass="destacados" />
</asp:TemplateField>
<asp:TemplateField HeaderText="ubicación">
    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# DameUbicacion(Eval("Estado")) %>'></asp:Label><asp:HiddenField
                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                    
</ItemTemplate>

<HeaderStyle CssClass="destacados" />
</asp:TemplateField>
</Columns>
<EmptyDataTemplate>
                                <div class="messages msg-info">
                                    No cuenta con ninguna evaluación en curso
                                </div>
                            
</EmptyDataTemplate>
</asp:GridView>

                        <asp:ObjectDataSource ID="ODSIniciadasAbiertas" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter"><SelectParameters>
<asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                    Type="Int32" />
<asp:Parameter DefaultValue="2" Name="TipoPeriodoID" Type="Int32" />
<asp:Parameter DefaultValue="INICIADOSABIERTOS" Name="Modo" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>

                                        
                    
</ContentTemplate>
</asp:TabPanel>
              <asp:TabPanel runat="server" HeaderText="Asignados" ID="TabPanelMYAsignados">
                  <HeaderTemplate>
                    <%--<asp:Label ID="ldlAsignadosID" Text="Asignados" runat="server" ToolTip="Las evaluaciones que se encuentren en ASIGNADOS, Son aquellas donde tuvo participación el usuario logueado y debe darle curso a la misma. Con los estados ABIERTA y/o NO INICIADA."></asp:Label>--%>
                    <asp:LinkButton ID="lnkAsignados" runat="server"  OnClientClick="Preloader();"   OnClick="lnkAsignados_Click"  Style="text-decoration: none;"> 
                      <asp:Label ID="ldlAsignadosID" Text="Asignados" runat="server"></asp:Label>
                    </asp:LinkButton>
                        <a id="asignadoscontool" class="load-tools" style="text-decoration: none;" href="#asignados" rel="#asignados">A</a>
                        <div id="asignados" style="display:none">
                            <p>Las evaluaciones que se encuentren en ASIGNADOS <br />
                            Son aquellas donde tuvo participación el usuario logueado y debe darle curso a la misma.<br />
                            Con los estados ABIERTA y/o NO INICIADA. </p>
                        </div>
                    
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:GridView ID="GridViewAsignados" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                            CellSpacing="1" Width="420px"  OnRowDataBound="GridViewAsignados_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="~/PMP/img/editar.jpg" /></ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Período">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3393" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="PeriodoDesc" Text="Período">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Legajo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="Legajo" Text="Legajo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apellido y Nombre" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Apellido y Nombre">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Formulario">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                        <asp:Literal ID="Literal3233" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sector" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoSector" Text="Sector">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cargo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Evaluado">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluado">
                                            </asp:Label></strong></HeaderTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado2" runat="server" Text='<%# DameEstado(Eval("EstadoID")) %>'></asp:Label>
                                     </ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Ubicación">
                                    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# DameUbicacion(Eval("Estado")) %>'></asp:Label><asp:HiddenField
                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <div class="messages msg-info">
                                    No cuenta con ninguna evaluación en curso
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ODSAsignadosAbiertos" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                    Type="Int32" />
                                <asp:Parameter DefaultValue="2" Name="TipoPeriodoID" Type="Int32" />
                                <asp:Parameter DefaultValue="ASIGNADOABIERTOS" Name="Modo" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    
                        </ContentTemplate>
                        </asp:TabPanel>
                 <asp:TabPanel runat="server" HeaderText="Histórico" ID="TabPanelMYHistorico">
                 <HeaderTemplate>
                    <%--<asp:Label ID="lblhistoricoId" Text="Histórico" runat="server" ToolTip="Las evaluaciones que se encuentren en HISTÓRICO, Son aquellas que fueron iniciadas o que tuvo participación el usuario logueado. Con dos estados posibles de la misma, NO CONCLUIDAS y/o APROBADO"></asp:Label>--%>

                   <asp:LinkButton ID="lnkHistorico" runat="server" OnClientClick="Preloader();"  OnClick="lnkHistorico_Click"  Style="text-decoration: none;"> 
                      <asp:Label ID="lblhistoricoId" Text="Histórico" runat="server"></asp:Label>
                   </asp:LinkButton>

                        <a id="historicocontool" class="load-tools" href="#historico" style="text-decoration: none;" rel="#historico">A</a>                     
                        <div id="historico" style="display:none">
                            <p>Las evaluaciones que se encuentren en HISTÓRICO, <br />
                            Son aquellas que fueron iniciadas o que tuvo participación el usuario logueado. <br />
                            Con dos estados posibles de la misma, NO CONCLUIDAS y/o APROBADO  </p>
                        </div>
                    
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:GridView ID="GridViewHistoricos" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                            CellSpacing="1" Width="420px"  OnRowDataBound="GridViewHistoricos_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                        <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="~/PMP/img/ver.jpg" PostBackUrl='<%# String.Format("javascript:popupPmpO(%22/PMP/Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}%22;,1000,600); return false;",Eval("Legajo"),Eval("PeriodoID"),Eval("TipoFormularioID")) %>' /></ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Período">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3353" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="PeriodoDesc" Text="Período">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Legajo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="Legajo" Text="Legajo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Evaluado" Visible="True">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Evaluado">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Formulario">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3373" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sector" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoSector" Text="Sector">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cargo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Evaluador" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluador">
                                            </asp:Label></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label><asp:HiddenField
                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <div class="messages msg-info">
                                    No cuenta con ninguna evaluación en curso
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ODSIniciadasHistoricos" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                    Type="Int32" />
                                <asp:Parameter DefaultValue="2" Name="TipoPeriodoID" Type="Int32" />
                                <asp:Parameter DefaultValue="HISTORICOS" Name="Modo" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                       
                    
                    </ContentTemplate>
                    </asp:TabPanel>
                    </asp:TabContainer>
     
     
                        </ContentTemplate>
                        </asp:TabPanel>
                <asp:TabPanel Width="100%" HeaderText="Full Year" runat="server" ID="TabPanelMisEvaluacionesFullYear">
                 <ContentTemplate>
    
                    <asp:TabContainer ID="TabContainer6" runat="server" ActiveTabIndex="1" 
                         CssClass="">
        
      
                    <asp:TabPanel Width="100%" HeaderText="Iniciados" runat="server" ID="TabPanelIniciados">
                       <HeaderTemplate>
                           <asp:Label ID="Label3" runat="server" Text="Iniciados"></asp:Label>
                       
                       <a id="iniciadoscontool" class="load-tools"  style="text-decoration: none;"  href="#iniciados" rel="#iniciados">?</a>
                      
                        
                        
                        
                        
                            <div id="Div1" style="display:none">
                                <p>Las evaluaciones que se encuentren en INICIADOS,<br />
                                Son aquellas que fueron iniciadas por el usuario logueado.<br />
                                Con dos estados posibles de la misma, ABIERTA y/o NO INICIADA</p>
                            </div>

                    
                    </HeaderTemplate>
                        <ContentTemplate>                          
                            <br><br>
                                        <asp:GridView ID="FULLIniciados" runat="server" AutoGenerateColumns="False"  DataSourceID="ODSFULLIniciadasAbiertas"
                                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                                            CellSpacing="1" Width="420px"  OnRowDataBound="GridViewIniciados_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="~/PMP/img/editar.jpg" /></ItemTemplate>
                                                    <HeaderStyle CssClass="destacados" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Período">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="PeriodoDesc" Text="Período">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal3123" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Legajo" Visible="False">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="Legajo" Text="Legajo">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Apellido y Nombre" Visible="False">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Apellido y Nombre">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Formulario">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal3l" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sector" Visible="False">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="EvaluadoSector" Text="Sector">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cargo" Visible="False">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Evaluador">
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluador">
                                                            </asp:Label></strong></HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="última acción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label><asp:HiddenField
                                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="destacados" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No cuenta con ninguna evaluación en curso
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSFULLIniciadasAbiertas" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                                                <asp:Parameter DefaultValue="INICIADOS" Name="Modo" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                          </ContentTemplate>
                     </asp:TabPanel>
                               
                    <asp:TabPanel Width="100%" HeaderText="Asignados" runat="server" ID="TabPanelAsignados">
                    <HeaderTemplate>
                    <asp:LinkButton ID="lnkAsignadosFY" runat="server"  OnClientClick="Preloader();"  OnClick="lnkAsignadosFY_Click" OnLoad="lnkAsignadosFY_Click"  Style="text-decoration: none;">
                     <asp:Label ID="Label6" Text="Asignados" runat="server"></asp:Label>
                    </asp:LinkButton>

                        <a id="iniciadoscontool" class="load-tools" style="text-decoration: none;" href="#asignados" rel="#asignados">?</a>
                        <div id="Div2" style="display:none">
                            <p>Las evaluaciones que se encuentren en ASIGNADOS <br />
                            Son aquellas donde tuvo participación el usuario logueado y debe darle curso a la misma.<br />
                            Con los estados ABIERTA y/o NO INICIADA. </p>
                        </div>
                    
                     </HeaderTemplate>
                       <ContentTemplate>
                       <br />
                       <br />
                        <%--<input id="OpenCal" type="button" runat="server" value="Calibración"/>--%>
                        <asp:ImageButton ID="btnCalibracion" runat="server" UseSubmitBehavior="false"
                               ImageUrl="~/images/calibracion/CalibracionCalibracion.png" />
                       <br />
                       <br />
                                        <asp:GridView ID="FULLAsignados" runat="server" AutoGenerateColumns="False"
                                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                                            CellSpacing="1" Width="420px"  OnRowDataBound="GridViewAsignados_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageCommand" runat="server" 
                                                            ImageUrl="~/PMP/img/editar.jpg" CssClass="FullAsignEdit" 
                                                            onclientclick="$('.FullAsignEdit').attr('disabled', 'disabled');" />
                                                        <asp:ImageButton ID="ImageVerPMPAsignada" runat="server" Visible="false" ImageUrl="~/PMP/img/ver.jpg" />
                                                        </ItemTemplate>
                                                    <HeaderStyle CssClass="destacados" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Período">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal3393" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="PeriodoDesc" Text="Período">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Legajo" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="Legajo" Text="Legajo">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Apellido y Nombre" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Apellido y Nombre">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Formulario">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                                        <asp:Literal ID="Literal3233" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sector" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="EvaluadoSector" Text="Sector">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cargo" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                                            </asp:LinkButton></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Evaluado">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Label></ItemTemplate>
                                                    <HeaderTemplate>
                                                        <strong>
                                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluado">
                                                            </asp:Label></strong></HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="última acción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label><asp:HiddenField
                                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="destacados" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    No cuenta con ninguna evaluación en curso
                                                </div>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ODSFULLAsignadosAbiertos" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                                                <asp:Parameter DefaultValue="ASIGNADOS" Name="Modo" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                          </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" HeaderText="Histórico" ID="TabPanelHistorico">
                    <HeaderTemplate>
                    <asp:LinkButton ID="lnkHistoricoFY" runat="server"  OnClientClick="Preloader();" OnClick="lnkHistoricoFY_Click"  Style="text-decoration: none;">
                      <asp:Label ID="Label8" Text="Histórico" runat="server"></asp:Label>
                    </asp:LinkButton>
                        <a id="iniciadoscontool" class="load-tools" href="#historico" style="text-decoration: none;" rel="#historico">?</a>                     
                        <div id="Div3" style="display:none">
                            <p>Las evaluaciones que se encuentren en HISTÓRICO, <br />
                            Son aquellas que fueron iniciadas o que tuvo participación el usuario logueado. <br />
                            Con dos estados posibles de la misma, NO CONCLUIDAS y/o APROBADO  </p>
                        </div>
                    
                         </HeaderTemplate>
                        <ContentTemplate>

                        <asp:GridView ID="GridViewHistoricosFY" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="Legajo,PeriodoID,TipoFormularioID" AllowSorting="True" CellPadding="3"
                            CellSpacing="1" Width="420px"  OnRowDataBound="GridViewHistoricos_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                        <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                        <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" Value='<%# Eval("TipoPeriodoID") %>' />
                                        <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="~/PMP/img/ver.jpg" PostBackUrl='<%# String.Format("javascript:popupPmpO(%22/PMP/Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}%22;,1000,600); return false;",Eval("Legajo"),Eval("PeriodoID"),Eval("TipoFormularioID")) %>' /></ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Período">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3353" runat="server" Text='<%# Eval("PeriodoDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoPeriodoDesc" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="PeriodoDesc" Text="Período">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Legajo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="Legajo" Text="Legajo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Evaluado" Visible="True">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Evaluado">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Formulario">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3373" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoTipoFormularioDesc" runat="server" CommandName="Sort"
                                                CssClass="destacados" CommandArgument="TipoFormularioDesc" Text="Formulario">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sector" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoSector" Text="Sector">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cargo" Visible="False">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="EvaluadoCargo" Text="Cargo">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                                                CommandArgument="FAlta" Text="Inicio <br /> Evaluación">
                                            </asp:LinkButton></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Evaluador" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label></ItemTemplate>
                                    <HeaderTemplate>
                                        <strong>
                                            <asp:Label ID="lnkEvaluador" runat="server" CssClass="destacados" Text="Evaluador">
                                            </asp:Label></strong></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label><asp:HiddenField
                                            ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <div class="messages msg-info">
                                    No cuenta con ninguna evaluación en curso
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                            <asp:ObjectDataSource ID="ODSFULLAsignadosHistoricos" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetFormulariosByLegajoTipoPeriodo" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="LegajoHiddenField" Name="Legajo" PropertyName="Value"
                                        Type="Int32" />
                                    <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                                    <asp:Parameter DefaultValue="HISTORICOS" Name="Modo" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>                            
                    
                    </asp:TabPanel>
                     
       
                       </asp:TabContainer>       
    
                </ContentTemplate>
                </asp:TabPanel>
                </asp:TabContainer>

    
              </ContentTemplate>
    
</asp:TabPanel>
    <asp:TabPanel Width="100%" HeaderText="Objetivos" runat="server" ID="TabObjectivosHabilidades">
            <ContentTemplate>
                <asp:TabContainer ID="TabContainer1_1" runat="server" ActiveTabIndex="0" 
                    CssClass="">
                    <asp:TabPanel Width="100%" HeaderText="Período Actual" runat="server" ID="TabPanelObjetivosPeriodoActual">
                        <ContentTemplate>
                         <div class="box formulario" >
                             <div class="form-item leftHalf">
                               <label>Adjunto:  </label><br />
                                      <asp:FileUpload ID="FileUploadAdjunto" runat="server" />
                                      <asp:Button ID="ButtonAgregarAdjunto" ValidationGroup="AgregarAdjunto" runat="server" Text="Agregar" OnClick="ButtonAgregarAdjunto_Click" ToolTip="Solo se puede adjuntar un (1) archivo. El tamaño máximo permitido para un archivo es de 4MB."    OnClientClick="skm_LockScreen('Búsqueda en proceso...'); return true;" />
                                <br />
                                 <asp:CustomValidator ID="CustomValidatorAdjunto" runat="server" ErrorMessage="*" ControlToValidate="FileUploadAdjunto"  ValidationGroup="AgregarAdjunto" OnServerValidate="CustomValidatorAdjunto_ServerValidate">Se permiten archivos .txt, .doc,.docx, .xls, .xlsx, .pdf, .jpg</asp:CustomValidator>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorAdjunto" runat="server" Display="Dynamic" ControlToValidate="FileUploadAdjunto" ErrorMessage="Ingrese ruta" ValidationGroup="AgregarAdjunto">Debe seleccionar un archivo, para ser adjunto</asp:RequiredFieldValidator>

                                <br />
                                <label style=" font-weight:normal"><asp:Label ID="lblLimitUpload" runat="server" Text="" CssClass="msg-error1"></asp:Label></label>
                             </div>
                             <div class="form-item rightHalf">
                               <label>Archivo Adjunto</label><br />
                                     <asp:FormView ID="FormViewAdjunto" runat="server" 
                                     DataSourceID="ObjectDataSourceAdjuntoPath" 
                                     onitemcommand="FormViewAdjunto_ItemCommand">
                                            <ItemTemplate>
                                             <h1> 
                                                 <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl='<%# String.Format("~/PMP/documents/{0}",Eval("AdjuntoPath")) %>' Target="_blank" Text='<%# (String.IsNullOrEmpty(Eval("AdjuntoPath").ToString()) ? "" : Eval("AdjuntoPath").ToString().Substring(19))  %>'>HyperLink</asp:HyperLink>
                                                  &nbsp;&nbsp;
                                                 
                                                 <asp:LinkButton ID="LinkButtonAdjunto" runat="server" CommandName="Adjunto" onclientclick="return confirm('Desea eliminar este Adjunto?');" Visible='<%# !string.IsNullOrEmpty(Eval("AdjuntoPath").ToString()) %>'> Eliminar </asp:LinkButton>
                                                 
                                              </h1>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        
                                        <asp:ObjectDataSource ID="ObjectDataSourceAdjuntoPath" runat="server" 
                                             OldValuesParameterFormatString="original_{0}" 
                                            SelectMethod="GetFrmObjetivosActualByUsuarioID" 
                                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" 
                                                    PropertyName="Value" Type="Int32" />
                                            </SelectParameters>
                                            
                                        </asp:ObjectDataSource>
                             </div>
                         </div>
                                    
                                        
                                        
                                       
                         <asp:UpdatePanel ID="UpdatePanelCargaObjetivos" runat="server">
                            <ContentTemplate>
                                 <div class="box">
                                <asp:Button ID="AgregarButton" runat="server"  Text="Agregar Objetivo" ValidationGroup="AgregarObjetivos"  onclick="AgregarButton_Click"/>
                                <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="AgregarObjetivos">  No es posible agregar más de 5 objetivos.</asp:CustomValidator>
                                
                                        
                                </div>   
                                <div class="box">   
                                
                                    <div class="GridObjetivos">                           
                                    <asp:Repeater ID="RepeaterObjetivosActual" runat="server" DataSourceID="ODSObjetivos" 
                                            onitemcommand="RepeaterObjetivosActual_ItemCommand" 
                                            onitemdatabound="RepeaterObjetivosActual_ItemDataBound">
                                    <HeaderTemplate>
                                    <table>
                                    <tr>
                                    <th style="width:200px">Objetivos</th>
                                    <th style="width:200px">Medidas</th>
                                    <th style="width:200px">Resultados</th>
                                    <th style="width:30px"></th>
                                    <th style="width:30px"></th>
                                    
                                    </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <tr>
                                    <td><asp:Label ID="LabelObjetivo" runat="server" Text='<%# Bind("Objetivos") %>'></asp:Label></td>
                                    <td><asp:Label ID="LabelMedida" runat="server" Text='<%# Bind("Medidas") %>'></asp:Label></td>
                                    <td> <asp:Label ID="LabelResultado" runat="server" Text='<%# Bind("Resultados") %>'></asp:Label></td>
                                    <td>
                                    <asp:ImageButton ID="ImageButtonEditar" runat="server" ImageUrl="../images/page_edit.png" 
                                                       Rows="8" PostBackUrl='<%# String.Format("javascript: popupObjetivos(%22/PMP/Objetivos.aspx?Indice={0}%22,500,500);",Eval("Indice") )%>' />
                                               </td>
                                               <td>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" CommandArgument='<%# Eval("Indice") %>'
                                                      CommandName="Delete" ImageUrl="~/images/delete.png"  onclientclick="return confirm('Desea eliminar este Objetivo?');"/> 
                                    </td>
                                    </tr>
                                    
                                    </ItemTemplate>
                                    <FooterTemplate>
                                       
                                    </table>
                                    </FooterTemplate>
                                    </asp:Repeater>
                                     <asp:Label ID="LabelEmptyData" runat="server" Text="Aún no cargó objetivos para el período actual" Visible="false"></asp:Label>
                                    </div>
                                    <%--<asp:GridView ID="ObjetivosGV" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        DataSourceID="ODSObjetivos" OnRowCommand="ObjetivosGV_RowCommand" 
                                        PageSize="20" DataKeyNames="Indice" >
                                        <Columns>
                                           <asp:TemplateField HeaderText="Objetivos" SortExpression="Objetivos">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelObjetivo" runat="server" Text='<%# Bind("Objetivos") %>'></asp:Label>                                                    
                                                </ItemTemplate>
                                                <HeaderStyle  Width="50px"/>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Medidas" SortExpression="Medidas">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelMedida" runat="server" Text='<%# Bind("Medidas") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle  Width="100px"/>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Resultados" SortExpression="Resultados">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelResultado" runat="server" Text='<%# Bind("Resultados") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle  Width="100px"/>
                                            </asp:TemplateField>                                            
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                   <asp:ImageButton ID="ImageButtonEditar" runat="server" ImageUrl="../images/page_edit.png" 
                                                       Rows="8" PostBackUrl='<%# String.Format("javascript: popupObjetivos(%22/PMP/Objetivos.aspx?Indice={0}%22,500,500);",Eval("Indice") )%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton2" runat="server" CommandArgument='<%# Eval("Indice") %>'
                                                      CommandName="Delete" ImageUrl="~/images/delete.png"  onclientclick="return confirm('Desea eliminar este Objetivo?');"/>                                                
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                       
                                        </Columns>
                                        <EmptyDataTemplate>
                                                <div class="messages msg-info">
                                                    Aún no ha cargado objetivos para este periodo
                                                </div>
                                            </EmptyDataTemplate>
                                    </asp:GridView>--%>
                                    <asp:ObjectDataSource ID="ODSObjetivos" runat="server" OldValuesParameterFormatString="{0}"
                                        OnObjectCreating="ODSObjetivos_ObjectCreating" OnObjectDisposing="ODSObjetivos_ObjectDisposing"
                                        SelectMethod="GetObjetivos" TypeName="com.paginar.johnson.BL.ControllerEvalDesemp" DeleteMethod="DelObjetivo">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Indice" Type="Int32" />
                                        </DeleteParameters>                                
                                    </asp:ObjectDataSource>
                                 <%--   <div class="controls">
                                        <asp:Button ID="GuardarButton" runat="server" OnClick="GuardarButton_Click" Text="Guardar"
                                            ValidationGroup="guardar" /></div>--%>
                                </div>
                             </ContentTemplate>
                            </asp:UpdatePanel> 
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel Width="100%" HeaderText="Histórico" runat="server" ID="TabPanelObjetivosHistorico">
                        <ContentTemplate>
                        <div class="GridObjetivos">
                            <asp:Repeater ID="RepeaterObjetivos" runat="server" DataSourceID="ODSFrmObetivosAnt" OnItemDataBound="RepeaterObjetivos_ItemDataBound">
                              <%-- <asp:DataList ID="ObjetivosAntDataList" runat="server" RepeatLayout="Flow" DataSourceID="ODSFrmObetivosAnt">--%>
                                <HeaderTemplate>
                                  <table id="TableObjetivosHistorico">
                                        <thead>
                                        <tr>
                                          <th style="width: 100px;">Período</th>
                                          <th style="width: 50px;"></th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    
                                    <tr>
                                    <td>

                                    <asp:HiddenField ID="HiddenFieldObjetivoID" runat="server" Value='<%# Eval("ObjetivoID")%>' />
                                    <asp:Label ID="Label17" runat="server" Text='<%# DamePeriodoByFecha(Eval("Fecha","{0:dd/MM/yyyy}")) %>' /></td>
                                    <td>
                                        <asp:HyperLink ID="HyperLinkVerObjetivos" runat="server" NavigateUrl='<%# String.Format("javascript: popupObjetivosAnt(%22/PMP/ObjetivosAnteriores.aspx?ObjetivoID={0}%22,700,600);",Eval("ObjetivoID") )%>'>Ver...</asp:HyperLink>
                                                                       
                                     </td>
                                    </tr>
                                   
                                </ItemTemplate>
                                <FooterTemplate>
                                   </tbody>
                                </table>

                                    <%--<asp:Label ID="lblEmpty" Visible='<%# bool.Parse((ObjetivosAntDataList.Items.Count==0).ToString())%>'
                                        Text="No agrego objetivos en este periodo." runat="server" CssClass="messages msg-info"></asp:Label>--%></FooterTemplate>
                            </asp:Repeater>

                            <asp:Label ID="LabelEmptyDataObjetivosAnteriores" runat="server" Text="No cargó objetivos en períodos anteriores" Visible="false"></asp:Label>
                            </div>
                            <asp:ObjectDataSource ID="ODSFrmObetivosAnt" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetFrmObjetivosAntByUsuarioID"
                                TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter">                                
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="UsuarioIDHiddenField" Name="UsuarioID" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:TabPanel>
                   
                </asp:TabContainer>
</ContentTemplate>
        
</asp:TabPanel>    
    <asp:TabPanel ID="TabPanelReportes" runat="server" Width="100%" HeaderText="Reportes">
    <ContentTemplate>
    <asp:TabContainer ID="TabContainerReportes" runat="server" ActiveTabIndex="0" Width="100%">
    <asp:TabPanel Width="100%" runat="server" ID="TabPanelReportesdeSeguimiento" HeaderText="Reporte de Seguimiento">
    <ContentTemplate>
            <div class="box formulario">
            <div class="form-item leftthird">
                 <label>Período de Evaluación</label>
                 <asp:DropDownList  ID="DropDownListPeriodoFiscalRS" runat="server" 
                       DataTextField="Descripcion" DataValueField="PeriodoFiscalID" AutoPostBack="True" 
                     onselectedindexchanged="DropDownListPeriodoFiscalRS_SelectedIndexChanged">
                 </asp:DropDownList>
                 <asp:ObjectDataSource ID="ObjectDataSourcePeriodoFiscalRS" 
                     runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetPeriodosFiscales" 
                     TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                 </asp:ObjectDataSource>                
                  </div>

            <div class="form-item middlethird">
                 <label>Tipo de Evaluado</label>
                 <asp:DropDownList ID="DropDownListTipoEvaluadoRS" runat="server" AutoPostBack="True" onselectedindexchanged="DropDownListTipoEvaluadoRS_SelectedIndexChanged">
                 <asp:ListItem Text="Administrativos" Value="Administrativos"></asp:ListItem>
                 <asp:ListItem Text="Operarios" Value="Operarios"></asp:ListItem>
                 </asp:DropDownList>
                
                 </div>
            <div class="form-item rightthird ">
                 <label>Tipo de Evaluación</label>
                 <asp:DropDownList ID="DropDownListTipoEvaluacionRS" runat="server" AutoPostBack="True" onselectedindexchanged="DropDownListTipoEvaluacionRS_SelectedIndexChanged">
                 <asp:ListItem Text="Full Year" Value="1"></asp:ListItem>
                 <asp:ListItem Text="Mid Year" Value="2"></asp:ListItem>
                 </asp:DropDownList>
                 </div>
            <div class="form-item leftthird">
                 <label>Dirección</label>
                 <asp:DropDownList ID="DropDownListDireccionRS" 
                     runat="server"  AppendDataBoundItems="True"
                     DataValueField="DireccionID" DataTextField="DireccionDET" 
                     AutoPostBack="True" 
                     onselectedindexchanged="DropDownListDireccionRS_SelectedIndexChanged" > 
                  <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                 </asp:DropDownList>
                 <asp:ObjectDataSource ID="ObjectDataSourceDireccionRS" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetDirecciones" 
                     TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                 </asp:ObjectDataSource>

                                 
                  </div>
            <div class="form-item middlethird">
                 <label>Área</label>
                 <asp:DropDownList ID="DropDownListAreaRS" runat="server" 
                     DataValueField="AreaID" DataTextField="AreaDesc"
                                     AppendDataBoundItems="True" >
                   <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                 
                 </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceAreaRS" runat="server" 
                     OldValuesParameterFormatString="original_{0}" 
                     SelectMethod="GetAreasByDireccionID" 
                     TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownListDireccionRS" Name="DireccionID" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                 </asp:ObjectDataSource>
                 </div>
            <div class="form-item rightthird">
                 <label>Evaluador</label>
                 <asp:DropDownList ID="DropDownListEvaluadorRS" runat="server" 
                     DataTextField="ApellidoNombre" DataValueField="Legajo"
                     AppendDataBoundItems="True"  Enabled="False">
                  <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                 </asp:DropDownList>
                 </div>
            <div class="form-item leftthird">
                 <label>Estado</label>
                 <asp:DropDownList ID="DropDownListEstadoRS" runat="server">
                  <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                  <asp:ListItem Text="No Iniciadas" Value="-1"></asp:ListItem>
                  <asp:ListItem Text="Abierta" Value="21"></asp:ListItem>
                  <asp:ListItem Text="Cerrada" Value="4"></asp:ListItem>
                  <asp:ListItem Text="No Concluidas" Value="6"></asp:ListItem>
                 </asp:DropDownList>                
                  </div>                  
            <div class="form-item middlethird">
                  <label>Agrupado Por</label>
                   <asp:DropDownList ID="DropDownListAgrupadoPorRS" runat="server" AutoPostBack="True"
                      onselectedindexchanged="DropDownListAgrupadoPorRS_SelectedIndexChanged">
                        <asp:ListItem Text="Área" Value="Direccion/Area"></asp:ListItem>
                        <asp:ListItem Text="Dirección" Value="Direccion" Selected="True"></asp:ListItem>                        
                        <asp:ListItem Text="Evaluador" Value="Evaluador"></asp:ListItem>                         
                     </asp:DropDownList>
                   
                 
                                
                 </div>
            <div class="form-item rightthird">
             <asp:Panel ID="PanelOpcionesMY" runat="server">
                 <label>Incluir Detalle</label>
                <asp:DropDownList ID="DropDownListIncluirDetalle" runat="server" ToolTip="Esta opción es visible en la exportación">
                <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                </asp:DropDownList>
                <label>Incluir No concluidas</label>
                <asp:DropDownList ID="DropDownListIncluirNoConluidas" runat="server">
                 <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                <asp:ListItem Text="SI" Value="SI"></asp:ListItem>
                </asp:DropDownList>

                </asp:Panel>
                 </div>
            <div class="form-item leftthird">
            <asp:Panel ID="PanelCluster" runat="server">
            <label>Cluster</label>

                <asp:ListBox ID="DropDownListCluster" runat="server" SelectionMode="Multiple" Rows="4" CssClass="form-select">
                     <%--  <asp:ListItem Text="Todos" Value="0" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="Argentina" Value="1"></asp:ListItem>
                      <asp:ListItem Text="Chile" Value="2"></asp:ListItem>
                      <asp:ListItem Text="Paraguay" Value="4"></asp:ListItem>
                      <asp:ListItem Text="Uruguay" Value="3"></asp:ListItem>--%>
                </asp:ListBox>
               
                 </asp:Panel>
            </div>
            </div>
            <div class="controls">
            <table><tr>
                    <td><asp:Button ID="ButtonBuscarReportesDeSeguimiento" runat="server" Text="Buscar" 
                    onclick="ButtonBuscarReportesDeSeguimiento_Click" /></td>
                    <td>&nbsp&nbsp</td>
                    <td>
                    <asp:Button ID="ButtonExportarReportesDeSeguimiento" runat="server" onclick="ButtonExportarReportesDeSeguimiento_Click" 
                        Text="Exportar/Imprimir" />
                    </td> 
                    <td></td>
                    </tr>
            </table>                                           
            </div>

        <asp:MultiView ID="MultiViewReporteSeguimiento" runat="server" ActiveViewIndex="0" >
            <asp:View ID="ViewVacia" runat="server">
            </asp:View>
            <asp:View ID="ViewSinResultados" runat="server">
                <label>No se encontrarón resultados para la búsqueda</label>
            </asp:View>
          <asp:View ID="ViewReporteSeguimientoAdministrativosPorEvaluador" runat="server">
                  <div class="AspNet-GridViewRepPMP">
                        <asp:Repeater ID="RepeaterEvaluacionesPorEvaluador" runat="server"  
                            OnItemDataBound="RepeaterEvaluacionesMYPorEvaluador_ItemDataBound" >
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <th align="center" rowspan="2">
                                            Evaluador
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            No Iniciadas
                                        </th>
                                        <th align="center" colspan="4">
                                            Iniciadas
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            Aprobadas
                                        </th>
                                            <th align="center" colspan="2"  id="COL_TO_HIDE_HEADER0" runat="server" rowspan="2">
                                                No concluidas
                                            </th>
                                            <th align="center" colspan="2" rowspan="2">
                                                TOTAL
                                            </th>
                                    </tr>
                                    <tr>
                                    <th align="center" colspan="2">
                                            En Evaluado
                                        </th>
                                        <th align="center" colspan="2">
                                         En Evaluador </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER1" runat="server">
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER2" runat="server">
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style='<%# DameEstilo(Eval("Evaluador"), Eval("Direccion") ) %>'>
                                    <td>
                                        <asp:HiddenField ID="HiddenFieldCantTotal" runat="server" Value='<%# Eval("CantTotalEvaluador") %>' />
                                        <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" Value='<%# Eval("LegajoEvaluador") %>' />                                        
                                        <asp:Label ID="Label7" runat="server" Text='<%# DameNombreColumna(Eval("AreaEvaluador"),Eval("DireccionEvaluador"),Eval("Evaluador")) %>'></asp:Label>
                                       
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE1" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE2" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantTotalEvaluador"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcTotal"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                             </table>
                            </FooterTemplate>
                        </asp:Repeater>
                  </div>

           </asp:View>
          <asp:View ID="ViewReporteSeguimientoAdministrativosPorDireccionArea" runat="server">
                  <div class="AspNet-GridViewRepPMP">
                        <asp:Repeater ID="RepeaterEvaluacionesPorDireccionArea" runat="server" OnItemDataBound="RepeaterEvaluacionesMYPorDireccionArea_ItemDataBound">
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <th align="center" rowspan="2">
                                            <asp:Label ID="LabelTitulo" runat="server" Text="Dirección/Área"></asp:Label>                                            
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            No Iniciadas
                                        </th>
                                        <th align="center" colspan="4">
                                            Iniciadas
                                        </th>
                                        
                                        <th align="center" colspan="2" rowspan="2">
                                            Aprobadas
                                        </th>
                                            <th align="center" colspan="2" id="COL_TO_HIDE_HEADER0" runat="server" rowspan="2">
                                                No concluidas
                                            </th>
                                            <th align="center" colspan="2" rowspan="2">
                                                TOTAL
                                            </th>
                                    </tr>

                                    <tr>
                                    <th align="center" colspan="2">
                                            En Evaluado
                                        </th>
                                        <th align="center" colspan="2">
                                            En Evaluador </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER1" runat="server">
                                            Cant
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER2" runat="server">
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style='<%# DameEstilo(Eval("Area"), Eval("Direccion") ) %>'>
                                    <td>
                                       <asp:Label ID="Label7" runat="server" Text='<%# DameNombreColumna(Eval("Area"), Eval("Direccion") ) %>'></asp:Label> 
                                       
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantNoIniciadas"), -1, Eval("AreaID"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcNoIniciadas"), -1, Eval("AreaID"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantEnEvaluado"), 2, Eval("AreaID"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcEnEvaluado"), 2, Eval("AreaID"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantEnEvaluador"), 1, Eval("AreaID"), Eval("CantEnEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcEnEvaluador"), 1, Eval("AreaID"), Eval("CantEnEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantAprobadas"), 4, Eval("AreaID"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcAprobadas"), 4, Eval("AreaID"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE1" runat="server">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantNoConcluidas"), 6, Eval("AreaID"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center"  id="COL_TO_HIDE2" runat="server">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcNoConcluidas"), 6, Eval("AreaID"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantTotal"), 0, Eval("AreaID"), Eval("CantTotal"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcTotal"), 0, Eval("AreaID"), Eval("CantTotal"))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                  </div>

           </asp:View>

          <asp:View ID="ViewReporteSeguimientoOperariosPorEvaluador" runat="server">
                  <div class="AspNet-GridViewRepPMP">
                        <asp:Repeater ID="RepeaterReporteSeguimientoOperariosPorEvaluador" runat="server"  
                            OnItemDataBound="RepeaterEvaluacionesMYPorEvaluador_ItemDataBound" >
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <th align="center" rowspan="2">
                                            Evaluador
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            No Iniciadas
                                        </th>
                                        <th align="center" colspan="6">
                                            Iniciadas
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            Aprobadas
                                        </th>
                                            <th align="center" colspan="2"  id="COL_TO_HIDE_HEADER0" runat="server" rowspan="2">
                                                No concluidas
                                            </th>
                                            <th align="center" colspan="2" rowspan="2">
                                                TOTAL
                                            </th>
                                    </tr>
                                    <tr>
                                    <th align="center" colspan="2">
                                            En Evaluador
                                        </th>
                                        <th align="center" colspan="2">
                                         En Evaluado </th>
                                        <th align="center" colspan="2">
                                         En Auditor </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER1" runat="server">
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER2" runat="server">
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style='<%# DameEstilo(Eval("Evaluador"), Eval("Direccion") ) %>'>
                                    <td>
                                        <asp:HiddenField ID="HiddenFieldCantTotal" runat="server" Value='<%# Eval("CantTotalEvaluador") %>' />
                                        <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" Value='<%# Eval("LegajoEvaluador") %>' />                                        
                                        <asp:Label ID="Label7" runat="server" Text='<%# DameNombreColumna(Eval("AreaEvaluador"),Eval("DireccionEvaluador"),Eval("Evaluador")) %>'></asp:Label>
                                       
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                     <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                   
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE1" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE2" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantTotalEvaluador"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcTotal"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                             </table>
                            </FooterTemplate>
                        </asp:Repeater>
                  </div>

           </asp:View>
          <asp:View ID="ViewReporteSeguimientoOperariosPorEvaluadorDetalle" runat="server">
                  <div class="AspNet-GridViewRepPMP">
                        <asp:Repeater ID="RepeaterReporteSeguimientoOperariosPorEvaluadorDetalle" runat="server"  
                            OnItemDataBound="RepeaterReporteSeguimientoOperariosPorEvaluadorDetalle_ItemDataBound" >
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <th align="center" rowspan="2">
                                            Evaluador
                                        </th>
                                        <th align="center" rowspan="2">
                                            Calificación Promedio
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            No Iniciadas
                                        </th>
                                        <th align="center" colspan="8">
                                            Iniciadas
                                        </th>
                                        <th align="center" colspan="3" rowspan="2">
                                            Aprobadas
                                        </th>
                                           <%-- <th align="center" colspan="2"  id="COL_TO_HIDE_HEADER0" runat="server" rowspan="2">
                                                No concluidas
                                            </th>--%>
                                            <th align="center" colspan="2" rowspan="2">
                                                TOTAL
                                            </th>
                                    </tr>
                                    <tr>
                                    <th align="center" colspan="2">
                                            En Evaluador
                                        </th>
                                        <th align="center" colspan="3">
                                         En Evaluado </th>
                                        <th align="center" colspan="3">
                                         En Auditor </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <!--No iniciadas -->
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--No iniciadas -->
                                        <!--En evaluador -->
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--En evaluador -->
                                        <!--En evaluado -->
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--En evaluado -->
                                        <!--En Auditor -->
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--En Auditor -->
                                        <!--Aprobadas -->
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--Aprobadas -->
                                        <%--<th id="COL_TO_HIDE_HEADER1" runat="server">
                                            Cant
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER2"  runat="server" >
                                            %
                                        </th>--%>

                                        <!--Total -->
                                        <th >
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <!--Total -->
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style='<%# DameEstilo(Eval("Evaluador"), Eval("Direccion") ) %>'>
                                    <td rowspan="4">
                                        <asp:HiddenField ID="HiddenFieldCantTotal" runat="server" Value='<%# Eval("CantTotalEvaluador") %>' />
                                        <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" Value='<%# Eval("LegajoEvaluador") %>' />                                        
                                        <asp:Label ID="Label7" runat="server" Text='<%# DameNombreColumna(Eval("AreaEvaluador"),Eval("DireccionEvaluador"),Eval("Evaluador")) %>'></asp:Label>
                                       
                                    </td>
                                    <td align="center"  rowspan="4"> 
                                     <asp:Label ID="Label9" runat="server" Text='<%#  Eval("PromedioDescripcion", "{0} (").ToString() + Eval("PromedioEvaluador", "{0})").ToString()   %>'></asp:Label>
                                    </td>
                                    <td align="center"  rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center" rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoIniciadas"), -1, Eval("direccionid"), Eval("CantNoIniciadas"))%>
                                    </td>
                                     <td align="center" rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <td align="center" rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnLT"), 1, Eval("direccionid"), Eval("CantEnLT"))%>
                                    </td>
                                    <%--<td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                   
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE1" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE2" runat="server">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcNoConcluidas"), 6, Eval("direccionid"), Eval("CantNoConcluidas"))%>
                                    </td>--%>
                        
                        <asp:Repeater ID="RepeaterAsignadoEvaluadosMB" 
                            runat="server">
                            <ItemTemplate>
                                <td align="center">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("CantidadEnEaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                  
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("PorcentajeEnEvaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("CantidadEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                           
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("PorcentajeEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("CantidadFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                           
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Muy Bueno", Eval("PorcentajeFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>


                            </ItemTemplate>
                        </asp:Repeater>

                                   <td align="center" rowspan="4" runat="server" visible='<%#string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString()) %>' colspan="2">
                                   <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center" rowspan="4" runat="server" visible='<%# string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString())%>'>
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnEvaluado"), 2, Eval("direccionid"), Eval("CantEnEvaluado"))%>
                                    </td>
                                   
                                    <td align="center" rowspan="4" runat="server" visible='<%# string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString()) %>' colspan="2">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center" rowspan="4" runat="server" visible='<%# string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString()) %>'>
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcEnJP"), 3, Eval("direccionid"), Eval("CantEnJP"))%>
                                    </td>
                                    <td align="center" rowspan="4" runat="server" visible='<%# string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString()) %>' colspan="2">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" rowspan="4" runat="server" visible='<%# string.IsNullOrEmpty(Eval("LegajoEvaluador").ToString()) %>'>
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcAprobadas"), 4, Eval("direccionid"), Eval("CantAprobadas"))%>
                                    </td>

                        <td align="center" rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("CantTotalEvaluador"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                           <td align="center" rowspan="4">
                                        <%# CrearLinkMYEvalaudorDireccion(Eval("LegajoEvaluador"), Eval("PorcTotal"), 0, Eval("direccionid"), Eval("CantTotalEvaluador"))%>
                                    </td>
                      
                        <asp:Repeater ID="RepeaterAsignadoEvaluadosB"  runat="server" >
                        <HeaderTemplate>
                        <tr>
                        </HeaderTemplate>
                            <ItemTemplate>
                                <td align="center">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("CantidadEnEaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("PorcentajeEnEvaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("CantidadEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("PorcentajeEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>

                                <td align="center">
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("CantidadFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Bueno", Eval("PorcentajeFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>

                            </ItemTemplate>
                            <FooterTemplate>
                            </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                                                   
                    
                        <asp:Repeater ID="RepeaterAsignadoEvaluadosR"  runat="server" >
                         <HeaderTemplate>
                        <tr>
                        </HeaderTemplate>
                            <ItemTemplate>
                                <td align="center">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("CantidadEnEaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("PorcentajeEnEvaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("CantidadEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("PorcentajeEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>

                                <td align="center">
                                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("CantidadFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "Regularmente cumple / Necesita desarrollarse", Eval("PorcentajeFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>

                            </ItemTemplate>

                          <FooterTemplate>
                            </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                         
                        <asp:Repeater ID="RepeaterAsignadoEvaluadosNS"  runat="server" >
                         <HeaderTemplate>
                        <tr>
                        </HeaderTemplate>
                            <ItemTemplate>
                                <td align="center">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("CantidadEnEaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("PorcentajeEnEvaluado"), 2, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("CantidadEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("PorcentajeEnAuditor"), 3, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("CalificacionResumen") %>'></asp:Label>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("CantidadFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>
                                <td align="center">
                                                            
                                    <%# CrearLinkOp(Eval("LegajoEvaluador"), int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), int.Parse(DropDownListEstadoRS.SelectedValue), 0, "No Satisfactorio", Eval("PorcentajeFinalizadas"), 4, int.Parse(Eval("AreaID").ToString()))%>
                                </td>

                            </ItemTemplate>

                       <FooterTemplate>
                            </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                                                   
                    
                                    
                               

                
                            </ItemTemplate>
                            <FooterTemplate>
                             </table>
                            </FooterTemplate>
                        </asp:Repeater>
                  </div>

           </asp:View>
          <asp:View ID="ViewReporteSeguimientoOperariosPorDireccionArea" runat="server">
                  <div class="AspNet-GridViewRepPMP">
                        <asp:Repeater ID="RepeaterReporteSeguimientoOperariosPorDireccionArea" runat="server" OnItemDataBound="RepeaterEvaluacionesMYPorDireccionArea_ItemDataBound">
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                        <th align="center" rowspan="2">
                                            <asp:Label ID="LabelTitulo" runat="server" Text="Dirección/Área"></asp:Label>                                            
                                        </th>
                                        <th align="center" colspan="2" rowspan="2">
                                            No Iniciadas
                                        </th>
                                        <th align="center" colspan="6">
                                            Iniciadas
                                        </th>
                                        
                                        <th align="center" colspan="2" rowspan="2">
                                            Aprobadas
                                        </th>
                                            <th align="center" colspan="2" id="COL_TO_HIDE_HEADER0" runat="server" rowspan="2">
                                                No concluidas
                                            </th>
                                            <th align="center" colspan="2" rowspan="2">
                                                TOTAL
                                            </th>
                                    </tr>

                                    <tr>
                                    <th align="center" colspan="2">
                                            En Evaluador </th>
                                    
                                    <th align="center" colspan="2">
                                            En Evaluado
                                        </th>
                                    <th align="center" colspan="2">
                                            En Auditor </th>
                                    </tr>
                                        
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER1" runat="server">
                                            Cant
                                        </th>
                                        <th id="COL_TO_HIDE_HEADER2" runat="server">
                                            %
                                        </th>
                                        <th>
                                            Cant
                                        </th>
                                        <th>
                                            %
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style='<%# DameEstilo(Eval("Area"), Eval("Direccion") ) %>'>
                                    <td>
                                       <asp:Label ID="Label7" runat="server" Text='<%# DameNombreColumna(Eval("Area"), Eval("Direccion") ) %>'></asp:Label> 
                                       
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantNoIniciadas"), -1, Eval("AreaID"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcNoIniciadas"), -1, Eval("AreaID"), Eval("CantNoIniciadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantEnEvaluador"), 1, Eval("AreaID"), Eval("CantEnEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcEnEvaluador"), 1, Eval("AreaID"), Eval("CantEnEvaluador"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantEnEvaluado"), 2, Eval("AreaID"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcEnEvaluado"), 2, Eval("AreaID"), Eval("CantEnEvaluado"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantEnAuditor"), 3, Eval("AreaID"), Eval("CantEnAuditor"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcEnAuditor"), 3, Eval("AreaID"), Eval("CantEnAuditor"))%>
                                    </td>

                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantAprobadas"), 4, Eval("AreaID"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcAprobadas"), 4, Eval("AreaID"), Eval("CantAprobadas"))%>
                                    </td>
                                    <td align="center" id="COL_TO_HIDE1" runat="server">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantNoConcluidas"), 6, Eval("AreaID"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center"  id="COL_TO_HIDE2" runat="server">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcNoConcluidas"), 6, Eval("AreaID"), Eval("CantNoConcluidas"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("CantTotal"), 0, Eval("AreaID"), Eval("CantTotal"))%>
                                    </td>
                                    <td align="center">
                                        <%# CrearLinkMYDireccionArea(Eval("direccionID"), Eval("PorcTotal"), 0, Eval("AreaID"), Eval("CantTotal"))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                  </div>

           </asp:View>
        </asp:MultiView>
    

        <asp:Panel ID="pnlCharts" runat="server" Visible="false">
            <br />
            <asp:Chart ID="ChartOperariosGral" runat="server" Height="400px" Width="670px" Visible="False"
                Palette="Pastel">
                <Series>
                    <asp:Series Name="Series1" XValueMember="Estado" YValueMembers="Cantidad" YValuesPerPoint="6"
                        ChartType="Pie">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                        <Area3DStyle Enable3D="True"></Area3DStyle>
                        <AxisX Interval="1" IsLabelAutoFit="True">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <BorderSkin SkinStyle="Emboss" />
                <Titles>
                    <asp:Title Text="General" Font="Times New Roman, 14pt, style=Bold" Docking="Top">
                    </asp:Title>
                </Titles>
            </asp:Chart>
            <asp:Chart ID="ChartDireccionNoIniciadas" runat="server" Visible="false" Palette="Bright"
                Height="400px" Width="670px">
                <Series>
                    <asp:Series Name="Total" XValueMember="direccionarea" LegendText="Total de Evaluaciones"
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Total">
                    </asp:Series>
                    <asp:Series Name="No Iniciadas" XValueMember="direccionarea" LegendText="No Iniciadas "
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend>
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Font="Times New Roman, 14pt, style=Bold" Text="No Iniciadas" Docking="Top">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
                <BorderSkin SkinStyle="Emboss" />
            </asp:Chart>
            <asp:Chart ID="ChartDireccionIniciadas" runat="server" Width="670px" Height="400px"
                Visible="false" Palette="EarthTones">
                <Series>
                    <asp:Series Name="Total" XValueMember="direccionarea" LegendText="Total de Evaluaciones"
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Total">
                    </asp:Series>
                    <asp:Series Name="Iniciadas" XValueMember="direccionarea" LegendText="Iniciadas "
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend>
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Font="Times New Roman, 14pt, style=Bold" Text="Iniciadas" Docking="Top">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
                <BorderSkin SkinStyle="Emboss" />
            </asp:Chart>
            <asp:Chart ID="CharDireccionIniciadasdetalle" runat="server" Width="400px" Height="400px"
                Visible="False" Palette="Pastel">
                <Series>
                    <asp:Series ChartType="Pie" Name="Series1" XValueMember="Estado" YValueMembers="Cantidad"
                        YValuesPerPoint="3">
                    </asp:Series>
                </Series>
                <Titles>
                    <asp:Title Font="Times New Roman, 14pt, style=Bold" Text="Iniciadas Detalle" Docking="Top">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="true">
                        <Area3DStyle Enable3D="True"></Area3DStyle>
                    </asp:ChartArea>
                </ChartAreas>
                <BorderSkin SkinStyle="Emboss" />
            </asp:Chart>
            <asp:Chart ID="ChartDireccionAprobadas" runat="server" Width="670px" Height="400px"
                Visible="false" Palette="SeaGreen">
                <Series>
                    <asp:Series Name="Total" XValueMember="direccionarea" LegendText="Total de Evaluaciones"
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Total">
                    </asp:Series>
                    <asp:Series Name="Aprobadas" XValueMember="direccionarea" LegendText="Aprobadas"
                        IsValueShownAsLabel="true" ChartArea="ChartArea1" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <Legends>
                    <asp:Legend>
                    </asp:Legend>
                </Legends>
                <Titles>
                    <asp:Title Font="Times New Roman, 14pt, style=Bold" Text="Aprobadas" Docking="Top">
                    </asp:Title>
                </Titles>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
                <BorderSkin SkinStyle="Emboss" />
            </asp:Chart>
        </asp:Panel>


    </ContentTemplate>
    
    </asp:TabPanel>
    
    <asp:TabPanel ID="TabPanelModuloReporte" runat="server" HeaderText="Reporte General">
       <ContentTemplate>
            <br />
            <br />
            <ul>
                <li runat="server" id="LIModuloReporteOperarios"><a href="javascript:window.open('../PMP/Admin/ReporteGeneral.aspx','PMPOper','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);">
                Ingresar al módulo de Reportes Operarios</a></li>
<%--                <li  runat="server" id="LIModuloReporteAdministrativos"><a href="javascript:window.open('../PMP/Admin/ReportesAdm.aspx','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);">
                Ingresar al módulo de Reportes Administrativos</a></li>--%>
                <li  runat="server" id="LIModuloReporteAdministrativos"><a href="javascript:window.open('../PMP/Admin/ReporteAdmFY.aspx','PMPAdminNvo','left=10,top=10,width=950,height=600,scrollbars=yes');void(0);">
                Ingresar al módulo de Reportes Administrativos</a></li>
                <li  runat="server" id="LI1"><a href="javascript:window.open('../PMP/Admin/ReporteObjetivos.aspx','PMPAdminNvo','left=10,top=10,width=950,height=600,scrollbars=yes');void(0);">
                Reporte Control de carga de Objetivos </a></li>

            </ul>
            <br />
            <br />                                  
            </ContentTemplate>
    </asp:TabPanel>	
    <asp:TabPanel ID="TabPanelCalibracion" runat="server" HeaderText="Calibración">
        <ContentTemplate>
            <br />
            <br />

                                     
<asp:UpdatePanel ID="UpdatePanelFiltros" runat="server">
<ContentTemplate>
            <div class="box formulario">
            <div class="form-item leftHalf">
            <label>Período:</label>
        <%--<asp:Label ID="lblPeriodo" runat="server" Text="Periodo: " CssClass="descripcion" Font-Bold="True"></asp:Label></td>--%>
                
            <asp:DropDownList ID="ddlPeriodo" runat="server" 
            DataTextField="Descripcion" DataValueField="PeriodoID" CssClass="formselect" 
                ondatabound="ddlPeriodo_DataBound" 
                onselectedindexchanged="ddlPeriodo_SelectedIndexChanged" 
                AutoPostBack="True">  </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourcePeriodosCalibracion" runat="server" DeleteMethod="Delete"
                    InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTipoPeriodoID"
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="PeriodoID" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Descripcion" Type="String" />
                        <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                        <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                        <asp:Parameter Name="CargaDesde" Type="DateTime" />
                        <asp:Parameter Name="CargaHasta" Type="DateTime" />
                        <asp:Parameter Name="PeriodoID" Type="Int32" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Descripcion" Type="String" />
                        <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                        <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                        <asp:Parameter Name="CargaDesde" Type="DateTime" />
                        <asp:Parameter Name="CargaHasta" Type="DateTime" />
                        <asp:Parameter Name="PeriodoID" Type="Int32" />
                    </InsertParameters>
                </asp:ObjectDataSource> 
            
            <asp:Repeater ID="FormView1" runat="server" >
              
                        <ItemTemplate>
                            <strong class="destacados">
                            <span>
                            (<asp:Label ID="CargaDesdeLabel" runat="server" 
                        Text='<%# Eval("CargaDesde","{0:dd/MM/yyyy}") %>' />
                            -
                            <asp:Label ID="CargaHastaLabel" runat="server" 
                        Text='<%# Eval("CargaHasta","{0:dd/MM/yyyy}") %>' />)
                        </span>
                            </strong>
                    
                        </ItemTemplate>
                    </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
                                
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
                DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                            <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                            <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                            <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                            <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="PeriodoID" Type="Int32" />
                            <asp:Parameter Name="Descripcion" Type="String" />
                            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                            <asp:Parameter Name="CargaDesde" Type="DateTime" />
                            <asp:Parameter Name="CargaHasta" Type="DateTime" />
                        </InsertParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Descripcion" Type="String" />
                            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                            <asp:Parameter Name="CargaDesde" Type="DateTime" />
                            <asp:Parameter Name="CargaHasta" Type="DateTime" />
                            <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                            <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                            <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                            <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                            <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
            </div>
        <div class="form-item rightHalf">           
            <asp:Label ID="lblEvaluado" runat="server" CssClass="descripcion" Font-Bold="True" Text="Evaluado:"></asp:Label>
            <asp:ListBox ID="ListBoxEvaluado" runat="server" 
                            DataTextField="ApellidoNombre" 
                            DataValueField="Legajo" AppendDataBoundItems="True"  CssClass="form-select" Rows="1">
                            <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                        </asp:ListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                <SelectParameters>
                    <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="8" Name="PeriodoID" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>                                    
                         
                   
        </div>
              
             
        <div class="form-item leftHalf">
            <asp:Label ID="lblArea" runat="server" CssClass="descripcion" Font-Bold="True" Text="Área:"></asp:Label>            
              
        <asp:ListBox ID="ListBoxArea" runat="server" 
                DataTextField="AreaDESC" 
                DataValueField="AreaID" 
                AppendDataBoundItems="True" CssClass="form-select" Rows="1">
                <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
            </asp:ListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceAreaByTipoFormulario" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataByTipoFormularioID" 
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="AreaDESC" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                        PropertyName="SelectedValue" Type="Int32"  DefaultValue="1"/>
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="AreaDESC" Type="String" />
                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>                                 

        </div>

        <div class="form-item rightHalf"> 
            <asp:Label ID="lblEvaluador" runat="server" CssClass="descripcion" Font-Bold="True" Text="Evaluador: "></asp:Label> 
                                    
            <asp:DropDownList ID="DropDownListEvaluador" runat="server"  CssClass="form-select" 
            AppendDataBoundItems="true" DataTextField="ApellidoNombre" DataValueField="Legajo" >
            <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
            </asp:DropDownList> 
                                   
        <asp:ObjectDataSource ID="ObjectDataSourceEvaluadores" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
            TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
            </SelectParameters>
            </asp:ObjectDataSource> 
    </div>
    <div class="form-item leftHalf">
        <asp:Label ID="lblCargo" runat="server" Text="Cargo: " CssClass="descripcion" Font-Bold="True"></asp:Label>
                    
        <asp:ObjectDataSource ID="ObjectDataSourceCargoByTipoFormulario" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataByTipoFormularioID" 
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.CargoTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="CargoDET" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                        PropertyName="SelectedValue" Type="Int32" DefaultValue="1"/>
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="CargoDET" Type="String" />
                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>

        <asp:ListBox ID="ListBoxCargos" runat="server" AppendDataBoundItems="True" 
            CssClass="form-select"
            DataTextField="CargoDET" DataValueField="CargoID" Rows="1" >
            <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
        </asp:ListBox> 
    </div>
    <div class="form-item rightHalf">
            <asp:Label ID="lblResultado" runat="server" CssClass="descripcion" Font-Bold="True" Text="Calificación:"></asp:Label>
                
        <asp:ListBox ID="ListBoxResultado" runat="server" 
            CssClass="form-select" Rows="1">
            <asp:ListItem Selected="True" Value="0" >Todos</asp:ListItem>
            <asp:ListItem Value="No Satisfactorio">No Satisfactorio</asp:ListItem>
            <asp:ListItem Value="Regularmente cumple / Necesita desarrollarse">Regularmente cumple / Necesita desarrollarse</asp:ListItem>
            <asp:ListItem Value="Bueno">Bueno</asp:ListItem>
            <asp:ListItem Value="Muy Bueno">Muy Bueno</asp:ListItem>
                    
                    
                    
        </asp:ListBox>  
    </div>
    <div class="form-item leftHalf">            
        <asp:Label ID="lblEstado" runat="server" Text="Estado: " CssClass="descripcion" Font-Bold="True"></asp:Label>
                 
        <asp:ListBox ID="ListBoxEstado" runat="server" AppendDataBoundItems="True" 
                CssClass="form-select"
            DataTextField="Descripcion" DataValueField="PasoID" Rows="1" >
            <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
            <%--<asp:ListItem Value="-1">No iniciado</asp:ListItem>--%>
        </asp:ListBox>
        <asp:ObjectDataSource ID="ObjectDataSourceEstados" runat="server" 
                        DeleteMethod="Delete" InsertMethod="Insert" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetTodos" 
                        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter" 
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_PasoID" Type="Int32" />
                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                        </DeleteParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Descripcion" Type="String" />
                            <asp:Parameter Name="Original_PasoID" Type="Int32" />
                            <asp:Parameter Name="Original_Descripcion" Type="String" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:Parameter Name="PasoID" Type="Int32" />
                            <asp:Parameter Name="Descripcion" Type="String" />
                        </InsertParameters>
                    </asp:ObjectDataSource>
        </div>
        <div class="form-item rightHalf">
        <asp:Label ID="lblLegajo" runat="server" CssClass="descripcion" 
                Font-Bold="True" Text="Legajo:"></asp:Label>
                          
        <asp:ListBox ID="ListBoxEvaluadoLegajo" runat="server" 
                DataTextField="Legajo" DataValueField="Legajo" 
                AppendDataBoundItems="True"  CssClass="form-select" Rows="1">
                <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                    </asp:ListBox>               
        <asp:ObjectDataSource ID="ObjectDataSourceEvaluadosLegajo" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetDataByPasoOrderByLegajo"     
                              
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID"
                PropertyName="SelectedValue" Type="Int32" />
                            <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource> 
    </div>
    <div class="form-item leftHalf"> 
        <label>Formulario:</label>
              
            <asp:ListBox ID="ListBoxFormularios" runat="server" CssClass="form-select"
                DataTextField="Descripcion" 
                DataValueField="TipoFormularioID" Rows="1" AutoPostBack="true" OnSelectedIndexChanged="ListBoxFormularios_SelectedIndexChanged">                     
            </asp:ListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceFormularios" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataByTipoPeriodoID" 
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.TipoFormularioTableAdapter">
                <SelectParameters>
                    <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div class="form-item rightHalf">
        <label>Sector:</label>
        <asp:ListBox ID="ListBoxSector" runat="server" 
        DataTextField="Descripcion" 
        DataValueField="SectorID" SelectionMode="Single" Rows="1"
        AppendDataBoundItems="True" CssClass="form-select">
        <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
            </asp:ListBox>
            <asp:ObjectDataSource ID="ObjectDataSourceSectorByTipoFormulario" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataByTipoFormularioID" 
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.SectorTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="SectorID" Type="Int32" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                        PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
         
                                 
        </div>
        <div class="controls"> 
        <table>
        <tr>
        <td><asp:Button ID="ButtonBuscarCalibracion" runat="server" OnClick="ButtonBuscarCalibracion_Click" Text="Buscar" /></td>
                                  
        <td> <asp:UpdateProgress ID="UpdateProgressCalibracion" runat="server" AssociatedUpdatePanelID="UpdatePanelFiltros">
                <ProgressTemplate>
                    <img src="../images/preloader.gif" class="preloader" />
                </ProgressTemplate>
            </asp:UpdateProgress></td>
        </tr>
        <tr>
        <td colspan="2"><asp:Label ID="LabelResultado" runat="server" Text="La búsqueda no produjo resultados para los parámetros indicados." 
        CssClass="destacados" Visible="false"></asp:Label> 
            </td>
        </tr>
        </table>       
                                    
                                    
                                     
    </div>
        </div>
    </ContentTemplate>
            </asp:UpdatePanel>
 
   
            <br />
            <br />                                  
        </ContentTemplate>
    </asp:TabPanel>	
    <asp:TabPanel ID="TabsUbicacionPMP"  runat="server" HeaderText="Ubicación PMP">
        <ContentTemplate>
            <div class="box formulario">
            <asp:Panel runat="server" ID="PanelRedirect" DefaultButton="btnConsultarUbicacion">
            <asp:UpdatePanel ID="UPConsultarUbicacion" runat="server">
                <ContentTemplate>

                <div class="form-item leftHalf">
                    <label>Legajo:</label>
                    
                    <asp:TextBox ID="tblegajoubicacion" runat="server"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                        TargetControlID="tblegajoubicacion" ValidChars="1234567890" Enabled="True" />
                    
                </div>
                <div class="form-item rightHalf">
<%--                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>--%>
                    <label>Periodo Fiscal:</label>
                    <asp:DropDownList ID="ddPeriodoUbicacion" runat="server" 
                        DataTextField="Descripcion" 
                        DataValueField="PeriodoFiscalID" 
                        onselectedindexchanged="DDTipoPMP_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Value="0">Seleccione</asp:ListItem>
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ODSPeriodoFIscal" runat="server" 
                        OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        
                        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.Eval_PeriodoFiscalTableAdapter">
                    </asp:ObjectDataSource>
                    <br />
                    <label>Tipo de PMP:</label>
                    <asp:DropDownList ID="DDTipoPMP" runat="server"  
                        onselectedindexchanged="DDTipoPMP_SelectedIndexChanged">
                        <%--<asp:ListItem Selected="True" Value="0">Seleccione</asp:ListItem>--%>
                        <asp:ListItem  Value="2">Mid Year Administrativos</asp:ListItem>
                        <asp:ListItem  Value="3">Full Year Administrativos</asp:ListItem>
                        <asp:ListItem  Value="1">Full Year Operarios</asp:ListItem>
                        
                    </asp:DropDownList>
                    <%--<asp:ObjectDataSource ID="ODSTipoPMP" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.eval_TipoPeriodoTableAdapter"></asp:ObjectDataSource>  --%>                  
                    <asp:HiddenField ID="HFPeriodoSelect" runat="server" />
<%--                    </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="DDTipoPMP" 
                                EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddPeriodoUbicacion" 
                                EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                    
                    
                </div>
                
                <div class="controls">
                <br />
                <asp:Button ID="btnConsultarUbicacion" runat="server" Text="Consultar" 
                        onclick="btnConsultarUbicacion_Click" />
                &nbsp;&nbsp;
                <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" 
                    onclick="btnLimpiar_Click" CausesValidation="False" />

                <br />
                <br />
                <div>
                <asp:HiddenField ID="HFETipoFormularioID" runat="server"/>
                            <asp:GridView ID="GVUbicacion" runat="server" AutoGenerateColumns="False" 
                                >
                                <Columns>
                                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                                        SortExpression="Legajo" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="True" 
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" ReadOnly="True" 
                                        SortExpression="Tipo" />
                                    <asp:BoundField DataField="Alta" DataFormatString="{0:dd-M-yyyy}" 
                                        HeaderText="Fecha de Inicio" SortExpression="Alta" />
                                    <asp:BoundField DataField="NomEvaluador" HeaderText="Evaluador" 
                                        SortExpression="NomEvaluador" ReadOnly="True" />
                                    <asp:TemplateField HeaderText="Estado" SortExpression="Ubicacion">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# getEstadoU() %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Ubicacion") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ubicación" SortExpression="Ubicacion">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# getUbicacionU() %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Ubicacion") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EmailEvaluador" HeaderText="Mail Evaluador" 
                                        SortExpression="EmailEvaluador" Visible="false"/>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSGVUbicacion" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="GetUbicacionByLegajoPeriodoID" 
                                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.UbicacionEvaluadoTableAdapter">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="tblegajoubicacion" Name="Legajo" 
                                        PropertyName="Text" Type="Int32" />
                                    <asp:ControlParameter ControlID="HFPeriodoSelect" Name="PeriodoID" 
                                        PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Label ID="lblDatosUbicacion" runat="server" Text=""></asp:Label>
                            <asp:Button ID="btnEnviarEvaluacion" runat="server" Text="Enviar información al Evaluador" 
                                CausesValidation="True" Visible="False" 
                                OnClientClick = " return confirm('¿Desea enviar el resultado al Evaluador?');" 
                                onclick="btnEnviarEvaluacion_Click" />
                            &nbsp;&nbsp;
                            <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" Enabled="False" 
                                onclick="btnConsultarUbicacion_Click" Visible="False" />

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnConsultarUbicacion" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnEnviarEvaluacion" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnActualizar" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnLimpiar" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <br />
                    <asp:UpdateProgress ID="UDPUbicacion" runat="server" AssociatedUpdatePanelID="UPConsultarUbicacion">
                        <ProgressTemplate>
                            <img src="../images/preloader.gif" class="preloader" alt="Cargando..." />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
              </div>
                </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:TabPanel>                          
   </asp:TabContainer>

    
</ContentTemplate>
    
</asp:TabPanel>
    <asp:TabPanel ID="TabPanelGeneralLinks" runat="server" HeaderText="Links">
    <ContentTemplate>
     <asp:TabContainer ID="TabContainerLinks" runat="server" ActiveTabIndex="1" 
            Width="100%">

   <asp:TabPanel Width="100%" runat="server" ID="TabPanelLinksMidYear" HeaderText="Mid Year">
    <ContentTemplate>

              <%--<h1>Mid Year</h1>--%>
              <br />
                <ul>
                   <%-- <asp:PlaceHolder runat="server" ID="plhFeedback">
                    <li>
                        Formularios para solicitar feedback a clientes internos:<br />
                        <ul>
                            <li>Mid Year Performance Ind. Contr. Form Feedback 2011:
                                <asp:HyperLink Target="_blank" ID="HyperLink3" NavigateUrl=""
                                    runat="server">Click Aquí</asp:HyperLink></li>
                            <li>Mid Year Performance Manager Form Feedback 2011:
                                <asp:HyperLink Target="_blank" ID="HyperLink4" NavigateUrl=""
                                    runat="server">Click Aquí</asp:HyperLink><br /><br /></li>
                        </ul>
                    </li>
                    </asp:PlaceHolder>--%>
                    <li runat="server" id="LiGuideDesarrollo">
                        <asp:HyperLink Target="_blank" ID="HyperLinkGuideDesarrollo" runat="server" NavigateUrl="~/PMP/documents/Guia_de_Desarrollo_de_Carrera_GPS_(español)_v_1.xls">Guía de Desarrollo de Carrera GPS (español) v1 (Solo aplicable a Product Supply)</asp:HyperLink>
                    </li>
                    <li runat="server" id="FactoresClavesDeExitoLI">
                        <asp:HyperLink Target="_blank" ID="HyperLinkActitudesGanadoras" runat="server" NavigateUrl="~/PMP/documents/Actitudes ganadoras-Estrategia 2016.pdf">Actitudes ganadores de la Estrategia 2016</asp:HyperLink>
                    </li>

                    <li runat="server" id="LiFormularioManager">
                        <asp:HyperLink Target="_blank" ID="HyperLinkFormularioManager" runat="server" NavigateUrl="~/pmp/documents/MID_YEAR_PERFORMANCE_MANAGER._FORM_13.docx">Formulario de evaluación de mitad de año (Managers & Supervisors)</asp:HyperLink>
                    </li>
                    <li runat="server" id="LikFormularioContributors">
                        <asp:HyperLink Target="_blank" ID="HyperLinkFormularioContributors" runat="server" NavigateUrl="~/pmp/documents/MID_YEAR_PERFORMANCE_Contribuidor_Ind_FORM_13.docx">Formulario de evaluación de mitad de año (Individual Contributor)</asp:HyperLink>
                    </li>
                    
                    <li runat="server" id="LiInstructivoAutoevaluacionManager">
                        <asp:HyperLink Target="_blank" ID="HyperLinkInstructivoManager" runat="server" NavigateUrl="~/PMP/documents/InstructivodeMYPR2011-2012N.ppt">Instructivo de MYPR (Managers, Supervisors & Individual Contributors)</asp:HyperLink>
                    </li>
                    

                    <li runat="server" id="LiTipsParaElEevaluadoLI">
                        <asp:HyperLink Target="_blank" ID="HyperLinkTipsEvaluado" runat="server" NavigateUrl="~/PMP/documents/TipsEvaluado.jpg">Tips para evaluados</asp:HyperLink>
                    </li>
                    <li runat="server" id="LiImperativosLidezgo">
                       <asp:HyperLink Target="_blank" ID="HyperLink2" runat="server" NavigateUrl="~/PMP/documents/IMPERATIVOS_DE_LIDERAZGO.ppt">Imperativos de Liderazgo SC Johnson </asp:HyperLink>
                    </li>
                    <%--<li runat="server" id="TipsParaElEevaluadorLI">
                        <asp:HyperLink Target="_blank" ID="HyperLinkTipsEvaluador" runat="server" NavigateUrl="~/PMP/documents/New_Picture_(2).bmp">Tips para el evaluador</asp:HyperLink>
                    </li>--%>
                </ul>
    </ContentTemplate>
    </asp:TabPanel>
    <asp:TabPanel Width="100%" runat="server" ID="TabPanelLinksFullYear" HeaderText="Full Year">
    <ContentTemplate>
             <%-- <h1>Full Year</h1>--%>
             <br />
                
                    <ul>
                    <li><asp:HyperLink ID="HyperLink3" Target="_blank" NavigateUrl="~/servicios/documents/Leadership_Imperative_mapping_to_competencies.docx" runat="server">Leadership Imperative mapping to competencies</asp:HyperLink></li>
                    <%--<li><asp:HyperLink ID="HyperLink3" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/IMPERATIVOS_DE_LIDERAZGO.pptx">Imperativos de Liderazgo SC Johnson.</asp:HyperLink></li>--%>                    
                    <li><asp:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl="~/servicios/documents/Leadership_Imperatives_Competency_List.xlsx" runat="server">Imperativos de Liderazgo SC Johnson</asp:HyperLink></li>
                    
                    <li>
                        <asp:HyperLink Target="_blank" ID="HyperLink10" runat="server" NavigateUrl="~/PMP/documents/Guia_de_Desarrollo_de_Carrera_GPS_(español)_v_1.xls">Guía de Desarrollo de Carrera GPS (español) v1 (Solo aplicable a Product Supply)</asp:HyperLink>
                    </li>
                 
                  <asp:Panel ID="POPRestringido" Visible="false" runat="server">  
                      
                   <li><asp:HyperLink ID="HyperLink15" runat="server" NavigateUrl="~/servicios/documents/Actitudes_ganadoras_Estrategia_2016.pdf" Target="_blank">Actitudes ganadores de la Estrategia 2016.</asp:HyperLink></li>
                    
 <%--                    <li><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/servicios/documents/SCJ_Skill_List_(English).xls" Target="_blank">SCJ SKILLS LIST (ENGLISH).</asp:HyperLink></li>
                    <li><asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/servicios/documents/SCJ_Skill_List_(Espanol).xlsx" Target="_blank">SCJ SKILLS LIST (Español).</asp:HyperLink></li>
--%>                    <li><asp:HyperLink ID="HyperLink5" runat="server">Formularios de PMP.</asp:HyperLink>
                      <ul>
                        
                            <li><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/servicios/documents/Form_PMP_FY_13_Individual_Contributors.doc" Target="_blank">Form PMP FY 13 Individual Contributors.</asp:HyperLink></li>
                            <li><asp:HyperLink ID="HyperLink8" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/HR124_Input_form_for_Contributors.doc">HR124 Input form for Contributors.</asp:HyperLink> </li>                                                
                            <asp:Panel ID="PanelPMPFULLMANAGER" runat="server" Visible="false">
<%-- 
                       <li><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/servicios/documents/PMP_for_Individual_Contributors_with_2016_LIs.doc" Target="_blank">Contribuidor individual.</asp:HyperLink></li>
                        <li><asp:HyperLink ID="HyperLink8" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/Appraisal_Input_Individual_Contributors.doc">Solicitud de feedback, Contribuidores Individuales </asp:HyperLink> </li>                                                
--%>                        </asp:Panel>
                        
                            <li><asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/servicios/documents/Form_PMP_FY_13_Supervisor_Managers.doc" Target="_blank">Form PMP FY 13 Supervisor Managers.</asp:HyperLink></li>
                            <li><asp:HyperLink ID="HyperLink9" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/HR125_Input_form_for_Managers.doc">HR125 Input form for Managers</asp:HyperLink> </li>                                                

                        <asp:Panel ID="PanelPMPFULLCONTRIBUTOR" runat="server" Visible="false">
<%--                        <li><asp:HyperLink ID="HyperLink7" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/PMP_for_People_Managers_with_2016_LIs.doc">Manager & Supervisor.</asp:HyperLink></li>
                        <li><asp:HyperLink ID="HyperLink9" runat="server" Target="_blank" NavigateUrl="~/servicios/documents/Appraisal_Input_Managers.doc">Solicitud de feedback, Gerentes </asp:HyperLink> </li>
--%>                        </asp:Panel>
                        </ul>
                     </li>
                        
                     </asp:Panel>
                     <li><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/servicios/documents/Proceso_PMP_FY_Guía_de_Usuario.pdf" Target="_blank">Proceso PMP FY - Guía de Usuario</asp:HyperLink></li>
                    </ul>
                
             
  </ContentTemplate>
  </asp:TabPanel>
    </asp:TabContainer>
            
    
</ContentTemplate>
    
</asp:TabPanel>
    </asp:TabContainer>
  

</asp:Content>
