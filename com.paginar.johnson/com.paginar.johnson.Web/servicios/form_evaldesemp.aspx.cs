﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.membership;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Text.RegularExpressions;
using com.paginar.johnson.DAL;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace com.paginar.johnson.Web.servicios
{
    public partial class form_evaldesemp : PageBase
    {
        private ControllerEvalDesemp _CED;
        public ControllerEvalDesemp CED
        {
            get
            {
                _CED = (ControllerEvalDesemp)this.Session["CED"];
                if (_CED == null)
                {
                    _CED = new ControllerEvalDesemp();
                    this.Session["CED"] = _CED;
                }
                return _CED;
            }
            set
            {
                this.Session["CED"] = value;
            }
        }


        
        private RepositoryEvalDesemp _Repositorio = new RepositoryEvalDesemp();
        public RepositoryEvalDesemp Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryEvalDesemp();
                return _Repositorio;
            }
        }    
        protected void Page_Load(object sender, EventArgs e)
        {
            lblLimitUpload.Text = "";
            lblDatosUbicacion.Text = "";
            //EvaluacionesDataList.DataBind();

            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Pestania"]))
                    TabContainerprincipal.ActiveTabIndex = 1;

                CED = new ControllerEvalDesemp();
                //trae los objetivos del periodo
                CED.GetObjetivosActuales(ObjectUsuario.usuarioid);
                UsuarioIDHiddenField.Value = this.ObjectUsuario.usuarioid.ToString();
                LegajoHiddenField.Value = this.ObjectUsuario.legajo.ToString();

                SetearPestaniaReportes();


               
                
                //AAAAAAAAAAA
                UsuariosEvaluacion U = new UsuariosEvaluacion(Page.User.Identity.Name);
                //Visible solo para Manager
                //plhFeedback.Visible = U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 8);
                //TipsParaElEevaluadoLI.Visible = U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 9);
                //TipsParaElEevaluadorLI.Visible = U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 8);
                //InstructivoAutoevaluaciónMYPRLI.Visible = (U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 8) || U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 9));
                //mostrar cuando se actualise el ink
                //InstructivoMYPRParaElEvaluadorLI.Visible = U.RealizaEvaluacionPeriodoActual(int.Parse(LegajoHiddenField.Value), 8);

                Repositorio.AdapterEvalDesemp.frmFormularioTableAdapter.GetDataByUsuarioID(int.Parse(UsuarioIDHiddenField.Value), null, "FORMS_HABILITADOS_X_USUARIO", null, null, int.Parse(UsuarioIDHiddenField.Value), null, null, null, null, 1);
                FormulariosController FCT = new FormulariosController();
                FormulariosDS.FormularioPmpOperariosDataTable FormularioDt = FCT.GetFormulariosByLegajoTipoPeriodo(int.Parse(ObjectUsuario.legajo.ToString()), 0, "TODOSLOSINICIADOS");
                foreach (FormulariosDS.FormularioPmpOperariosRow item in FormularioDt.Rows)
                {

                    if (item.TipoFormularioID == 10) //|| item.TipoFormularioID == 8
                    {
                        PanelPMPFULLMANAGER.Visible = true;
                        POPRestringido.Visible = true;
                    }
                    if (item.TipoFormularioID == 11) //|| item.TipoFormularioID == 9
                    {
                        PanelPMPFULLCONTRIBUTOR.Visible = true;
                        POPRestringido.Visible = true;
                    }

                    if (item.TipoFormularioID == 11 || item.TipoFormularioID == 10)
                        break;

                }

                
                //HyperLink3.NavigateUrl = "mailto:?subject=Solicitud de PMP Mid Year Feedback&body=Por favor, completarme el formulario con el feedback correspondiente a COMPLETAR ANTES DE ENVIAR y reenviármelo.%0A%0ahttp://" + System.Web.HttpContext.Current.Request.Url.Host + "/pmp/documents/MID_YEAR_PERFORMANCE_IND._CONTR._FORM-_FEEDBACK_2011.doc%0A%0AMuchas Gracias!";
               // HyperLink4.NavigateUrl = "mailto:?subject=Solicitud de PMP Mid Year Feedback&body=Por favor, completarme el formulario con el feedback correspondiente a COMPLETAR ANTES DE ENVIAR y reenviármelo.%0A%0ahttp://" + System.Web.HttpContext.Current.Request.Url.Host + "/pmp/documents/MID_YEAR_PERFORMANCE_MANAGER._FORM-_FEEDBACK_2011.doc%0A%0AMuchas Gracias!";
                string asignados = Request.QueryString["asignados"];
                switch (asignados)
                {
                    case "1":

                    TabContainerprincipal.ActiveTabIndex = 0;
                    TabContainerMisevaluaciones.ActiveTabIndex = 0;
                    TabContainerMisEvaluacionesMY.ActiveTabIndex = 1;
                    lnkAsignados_Click(null, null);
                        break;
                    case "2":

                    TabContainerprincipal.ActiveTabIndex = 0;
                    TabContainerMisevaluaciones.ActiveTabIndex = 0;
                    TabContainerMisEvaluacionesMY.ActiveTabIndex = 0;
                        break;

                }
                string openPop = Request.QueryString["openPop"];
                switch (openPop)
                {
                        case "1":
                            ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Form_PmpMYEnevaluador.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");    
                        break;

                        case "2":
                            ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Form_PmpMYEnevaluado.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");    
                        break;

                        case "3":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/ImpresionMY.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;

                        case "FY":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMPFY/Form_PmpFY.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                        case "FYI":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMPFY/Impresion.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                        case "OPERARIOSE1":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Form_PmpEnevaluado.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                        case "OPERARIOSE2":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Form_PmpEnevaluador.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                        case "OPERARIOSAU":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Form_PmpEnJp.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                        case "OPERARIOSIMP":
                        ClientScript.RegisterStartupScript(typeof(Page), "OpenEvalEvaluador", "<script type='text/javascript'>$(function() { popupPmpO('/PMP/Impresion.aspx?Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"] + "',1000,600);return false;});</script>");
                        break;
                    
                        /*
                         Form_PmpEnevaluado.aspx
                         Form_PmpEnevaluador.aspx
                         Form_PmpEnJp.aspx
                         */
                        

                }

                DDTipoPMP.DataBind();
                ddPeriodoUbicacion.DataBind();
                FormulariosDS.PeriodoDataTable PDT = new FormulariosDS.PeriodoDataTable();

                PDT = FachadaDA.Singleton.Periodo.GetDataByTipoPeriodoIDPeriodoFIscalID(int.Parse(DDTipoPMP.SelectedValue.ToString()), int.Parse(ddPeriodoUbicacion.SelectedValue.ToString()));
                if(PDT.Rows.Count>0)
                HFPeriodoSelect.Value = PDT.Rows[0].ItemArray[0].ToString();


                com.paginar.johnson.membership.Roles R = new Roles();
                bool EsAdministrador = (R.IsUserInRole(Page.User.Identity.Name, "Administradores generales") ||
                                        R.IsUserInRole(Page.User.Identity.Name, "RRHH") ||
                                        R.IsUserInRole(Page.User.Identity.Name, "Administradores Cadese"));

                if (!EsAdministrador)
                {
                   

                    HiddenFieldCluster.Value = ObjectUsuario.clusterID.ToString();

                    switch (ObjectUsuario.clusterID.ToString())
                    {
                        case "1":
                            //DropDownListCluster.Items.Add(new ListItem("Todos", "0"));
                            DropDownListCluster.Items.Add(new ListItem("Argentina", "1"));
                            
                            //DropDownListCluster.Items.Add(new ListItem("Chile", "2"));
                            //DropDownListCluster.Items.Add(new ListItem("Paraguay", "4"));
                            //DropDownListCluster.Items.Add(new ListItem("Uruguay", "3"));
                            break;
                        case "2":
                            DropDownListCluster.Items.Add(new ListItem("Chile", "2"));
                            break;
                        case "3":
                            DropDownListCluster.Items.Add(new ListItem("Uruguay", "3"));
                            break;
                        case "4":
                            DropDownListCluster.Items.Add(new ListItem("Paraguay", "4"));
                            break;
                    }

                    DropDownListCluster.SelectedValue = ObjectUsuario.clusterID.ToString();





                }
                else
                {

                    DropDownListCluster.Items.Add(new ListItem("Todos", "0"));
                    DropDownListCluster.Items.Add(new ListItem("Argentina", "1"));
                    DropDownListCluster.Items.Add(new ListItem("Chile", "2"));
                    DropDownListCluster.Items.Add(new ListItem("Paraguay", "4"));
                    DropDownListCluster.Items.Add(new ListItem("Uruguay", "3"));
                    DropDownListCluster.SelectedValue = "0";
                }

/*Calibracion en asignados*/
                //ButtonBuscarMiCalibracion.Visible = false;
                //OpenCal.Visible = false;
                btnCalibracion.Visible = false;
                int? periodoActualPMP;
                //periodoActualPMP = com.paginar.formularios.businesslogiclayer.PeriodoInfo.GetPeriodoActualID(DateTime.Now);
                periodoActualPMP = com.paginar.formularios.businesslogiclayer.PeriodoInfo.GetPeriodoActualByFechaAndType(DateTime.Now, 1);
                
                if (periodoActualPMP != null)
                {
                    string EvaluadorActual = this.ObjectUsuario.legajo.ToString();
                    FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable DTReporteCalibracionResum = new FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable();
                    FachadaDA.Singleton.GetReporteParaCalibracionResum.Fill(DTReporteCalibracionResum, periodoActualPMP, 1, 0, "0", "0", EvaluadorActual, "0", "0", "0", "0");


                    if (DTReporteCalibracionResum.Count > 0)
                    {
                        //ButtonBuscarMiCalibracion.Visible = true;
                        //OpenCal.Visible = true;
                        btnCalibracion.Visible = true;
                        TabContainer6.ActiveTab = TabPanelAsignados;
                        //TabPanelMisEvaluacionesFullYear.TabIndex = 1;
                        string Periodo = periodoActualPMP.ToString();
                        string Formulario = "1";
                        string Estado = "0";
                        string Evaluado = "0";
                        string EvaluadoLegajo = "0";
                        string Evaluador = EvaluadorActual;
                        string Sector = "0";
                        string Area = "0";
                        string Cargo = "0";
                        string Calificacion = "0";
                        string URL = string.Format("../PMP/Admin/PMPCalibracion.aspx?&PeriodoID={0}&FormularioID={1}&Estado={2}&Evaluado={3}&EvaluadoLegajo={4}&Evaluador={5}&Sector={6}&Area={7}&Cargo={8}&Calificacion={9}", Periodo, Formulario, Estado, Evaluado, EvaluadoLegajo, Evaluador, Sector, Area, Cargo, Calificacion);
                        //((Button)ButtonBuscarMiCalibracion).OnClientClick("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);");
                        //OpenCal.Attributes["onClick"] = ("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=1150,height=620,scrollbars=yes'); $('.FullAsignEdit').attr('disabled', 'disabled'); void(0);");
                        btnCalibracion.Attributes["onClick"] = ("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=1150,height=620,scrollbars=yes'); $('.FullAsignEdit').attr('disabled', 'disabled');  return false;");
                            //("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);");
                        //EjecutarScript("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);");


                    }
                    else 
                    {
                        TabContainer6.ActiveTab = TabPanelIniciados;
                        TabPanelMisEvaluacionesFullYear.TabIndex = 0;
                    }
                        
                }

            }

            
            

        }

        protected void SetearPestaniaReportes()
        {
           
            com.paginar.johnson.membership.Roles R = new Roles();

            bool PuedeCalibrar = ((Boolean)FachadaDA.Singleton.RelEvaluadosEvaluadores.EsEvaluadorAuditorPMPOperario(this.ObjectUsuario.legajo));

            //bool TienePMPManagerMY = ((Boolean)FachadaDA.Singleton.Evaluados.TienePMPManagerMY(this.ObjectUsuario.legajo));

            

            bool EsAdministrador = (R.IsUserInRole(Page.User.Identity.Name, "Administradores generales") ||
                                    R.IsUserInRole(Page.User.Identity.Name, "RRHH") ||
                                    R.IsUserInRole(Page.User.Identity.Name, "Administradores Cadese"));


            ControllerUsuarios CUsuario = new ControllerUsuarios();
            bool PuedeVerReporteSeguimiento = CUsuario.PuedeVerReporteSeguimiento(this.ObjectUsuario.usuarioid) ||  EsAdministrador;

            DSUsuarios.UsuarioDataTable DTU = new DSUsuarios.UsuarioDataTable();
            string EscalaSalarial = CUsuario.DameEscalaSalarial(this.ObjectUsuario.usuarioid);

            
               


           

            //Panel Calibración
            TabPanelCalibracion.Visible = (PuedeCalibrar || R.IsUserInRole(Page.User.Identity.Name, "Calibración PMP Operarios"));

            //reporte seguimiento


            

           //codigo movido a la linea 334


            



            if (PuedeVerReporteSeguimiento && EscalaSalarial != "Q" && !EsAdministrador)
            {
                DTU = CUsuario.GetUsuarioByUsuarioID(this.ObjectUsuario.usuarioid);
                if (DTU.Count > 0)
                {

                    this.Session["DireccionID"] = DTU.Rows[0]["direccionid"].ToString();
                    this.Session["AreaID"] = DTU.Rows[0]["AreaID"].ToString();
                }
                
              
               
                    if (this.ObjectUsuario.usuarioid == 12110)// solo para este tipito tiene q ver sólo dos direcciones ssc finanzas y ssc suply chain
                    {
                        limpiarDirecciones();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Session["DireccionID"].ToString()) && DropDownListDireccionRS.Items.FindByValue(Session["DireccionID"].ToString()) != null)
                            DropDownListDireccionRS.SelectedValue = Session["direccionid"].ToString();
                        else
                            PuedeVerReporteSeguimiento = false;
                       
                        DropDownListDireccionRS.Enabled = false;
                        DropDownListDireccionRS_SelectedIndexChanged(null, null);
                    }

                    if (EscalaSalarial == "I" || EscalaSalarial == "J" || EscalaSalarial == "K" || EscalaSalarial == "L" || EscalaSalarial == "M")
                    {
                        if (!string.IsNullOrEmpty(Session["AreaID"].ToString()) && DropDownListAreaRS.Items.FindByValue(Session["AreaID"].ToString()) != null)
                            DropDownListAreaRS.SelectedValue = Session["AreaID"].ToString();
                        else
                            PuedeVerReporteSeguimiento = false;

                        DropDownListAreaRS.Enabled = false;
                        
                    }

                   

                
            }

            TabPanelReportesdeSeguimiento.Visible = PuedeVerReporteSeguimiento;

            //Panel Reportes
            TabPanelReportes.Visible = EsAdministrador ||
            R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMPO") ||
                // R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMP Administrativos") ||
            PuedeCalibrar || R.IsUserInRole(Page.User.Identity.Name, "Calibración PMP Operarios")
            || PuedeVerReporteSeguimiento;


            //->
            if (TabPanelReportes.Visible)
            {
                DropDownListTipoEvaluadoRS.SelectedIndex = 1;
                CargarTabReportes();

                //DropDownListPeriodoFiscalRS.DataBind(); RF yase hace en la linea CargarTabReportes()
                if (DropDownListTipoEvaluadoRS.Items.Count > 0 && DropDownListTipoEvaluacionRS.Items.Count > 0)
                {
                    DropDownListTipoEvaluadoRS_SelectedIndexChanged(null, null);
                    DropDownListTipoEvaluacionRS_SelectedIndexChanged(null, null);
                }

                DropDownListDireccionRS.DataSourceID = "ObjectDataSourceDireccionRS";
                DropDownListDireccionRS.DataBind();
            }
            //->
           

            TabPanelModuloReporte.Visible = R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMPO") 
                                            //|| R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMP Administrativos")
                                            || EsAdministrador;

            LIModuloReporteOperarios.Visible = (R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMPO") || EsAdministrador);
            LIModuloReporteAdministrativos.Visible = R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMP Administrativos") || EsAdministrador;
            //LI1.Visible = R.IsUserInRole(Page.User.Identity.Name, "Modulo de Reportes PMP Administrativos") || EsAdministrador;

            
            TabsUbicacionPMP.Visible = (PuedeVerReporteSeguimiento || EsAdministrador);



            if (TabPanelReportesdeSeguimiento.Visible == false)
                TabContainerReportes.ActiveTabIndex = 1;

            if (TabPanelReportesdeSeguimiento.Visible == false && TabPanelModuloReporte.Visible == false)
                TabContainerReportes.ActiveTabIndex = 2;

            if (TabPanelReportesdeSeguimiento.Visible == false && TabPanelModuloReporte.Visible == false && TabPanelCalibracion.Visible==false)
                TabContainerReportes.ActiveTabIndex = 3;


        
        }

        private void limpiarDirecciones()
        {
            DropDownListDireccionRS.Items.Clear();
            DropDownListDireccionRS.Items.Add(new ListItem("SSC - Finanzas","19"));
            DropDownListDireccionRS.Items.Add(new ListItem("SSC - Supply Chain", "20"));
            DropDownListDireccionRS_SelectedIndexChanged(null, null);
        }
        
        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }


        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (val.ToString());

        }

        protected string DefaultValComentarios(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("S/C");
            else
            {
                int Maximalongitud = 10;
                string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

                return Texto;
            }

        }

        protected string DameEstado(object val)
        {
            string Value = string.Empty;

            switch (val.ToString())
            { 
                case "-1":
                    Value= "No iniciada";
                        break;
                case "1":
                case "2":
                case "3":
                    Value= "Abierta";
                    break;

                case "4":
                   Value= "Aprobada";
                        break;
                case "6":
                        Value = "No concluida";
                    break;
            }

            return Value;

            
            

        
        }

        protected string DameUbicacion(object val)
        {
           

            if (val.ToString() == "No Iniciado")
                return "En Evaluado";

            else
                return val.ToString();





        }
        protected string DamePeriodoByFecha(object val)
        {
            DateTime Fecha = DateTime.Parse(val.ToString());
            DateTime FechaInicioPeriodo = DateTime.Parse("01/07/" + Fecha.Year);
            string Periodo;

            if (Fecha < FechaInicioPeriodo)
            {
                Periodo = (Fecha.Year - 1) + "-" + Fecha.Year;
            }
            else
                Periodo = Fecha.Year + "-" + (Fecha.Year + 1);

            return Periodo;



        
        }

        protected int DamePeriodoIDByPeriodoFiscalTipoEvaluacion(int PeriodoFiscal, int TipoEvaluacion)
        {
            int PeriodoID = -1;
            ControllerReportesDeSeguimiento CRS = new ControllerReportesDeSeguimiento();
            DSReportesDeSeguimiento.eval_PeriodoDataTable PeriodDT = CRS.GetPeriodoByPeridoFiscalTipoPeriodo(PeriodoFiscal, TipoEvaluacion);
            if (PeriodDT.Rows.Count > 0)
            {
                PeriodoID = int.Parse(PeriodDT.Rows[0]["PeriodoID"].ToString());
            }
            return PeriodoID;
        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        #region MisEvaluaciones

      

        protected void GridViewAsignados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Usuarios usercontroller = new Usuarios();
            User Usuario = null;
            Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
            int LegajoLog = Usuario.GetLegajo();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                Label LinkButtonEstado = e.Row.FindControl("LinkButtonEstado") as Label;
                HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
                HiddenField HiddenFieldTipoPeriodoID = e.Row.FindControl("HiddenFieldTipoPeriodoID") as HiddenField;
                ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
                ImageButton ImageVerPMPAsignada = e.Row.FindControl("ImageVerPMPAsignada") as ImageButton;
                
                Label LabelEvaluador = e.Row.FindControl("LabelEvaluador") as Label;

                int FormularioID = int.Parse(HiddenFieldTipoFormularioID.Value);
                int legajo = int.Parse(HiddenFieldLegajo.Value);
                int PeriodoID = int.Parse(HiddenFieldPeriodoID.Value);
               

                int PasoID = int.Parse(HiddenFieldEstadoID.Value);
                if (HiddenFieldTipoPeriodoID .Value== "1")
                   PasoID = (PasoID==-1)?1:PasoID;
                else
                    PasoID = (PasoID == -1) ? 2 : PasoID;

                //trae el fomrmulario en paso
                string UrlForm = FormularioInfo.DameURLForm( int.Parse(HiddenFieldTipoFormularioID.Value) , int.Parse(HiddenFieldPeriodoID.Value), PasoID);
                

                bool Periodocarga =  ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));

                int? LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(legajo, PeriodoID, FormularioID, 1);
                int LegEvaluador;
                //trae el auditor
                int? LegajoAuditor = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(legajo, PeriodoID, FormularioID, 3);
                int LegajoAudit;

                string NombreAuditor = string.Empty;
                string NombreEvaluador = string.Empty;

                FormulariosDS.vwUsuariosDataTable vwUsuariosDT = new FormulariosDS.vwUsuariosDataTable();

                if (LegajoAuditor != null)
                {
                    LegajoAudit = (int)LegajoAuditor;
                    vwUsuariosDT = FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(LegajoAudit);
                    if (vwUsuariosDT.Rows.Count > 0)
                        NombreAuditor = vwUsuariosDT[0].Apellido + " " + vwUsuariosDT[0].Nombre;

                }

                if (LegajoEvaluador != null)
                {
                    LegEvaluador = (int)LegajoEvaluador;
                    vwUsuariosDT = FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(LegEvaluador);
                    if (vwUsuariosDT.Rows.Count > 0)
                        NombreEvaluador = vwUsuariosDT[0].Apellido + " " + vwUsuariosDT[0].Nombre;

                }

                
                // el evaluador nunca puede ver en paso 2
                if (PasoID == 2)
                {
                    ImageCommand.ImageUrl = "~/pmp/img/infopmp.png";
                    ImageCommand.Enabled = false;
                    
                    if (PasoID == 2)
                        ImageCommand.ToolTip = "La evaluación esta en poder del evaluado " + LabelEvaluador.Text;

                    return;
                    
                }
                // evaluador: puedeeditar en periodo de carga y paso=1
                // auditor:   puedeeditar en periodo de carga y paso=3
                if (Periodocarga && ((PasoID == 1 && LegajoLog == LegajoEvaluador) || (PasoID == 3 && LegajoLog == LegajoAuditor)))
                   {
                        

                       ImageCommand.ImageUrl = "~/pmp/img/editar.jpg";
                       ImageCommand.ToolTip = "Editar Evaluación";
                       if (FormularioID == 11 || FormularioID == 10 || FormularioID==12)
                           ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMPFY/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                       else
                       {
                           ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                           
                       }
                       return;    
                   }
                if((PasoID == 1 && LegajoLog == LegajoAuditor)|| (PasoID == 3 && LegajoLog == LegajoEvaluador))
                {
                  ImageCommand.ImageUrl = "~/pmp/img/infopmp.png";
                  ImageCommand.Enabled = false;
                 
                    if (PasoID == 1)
                        ImageCommand.ToolTip = "La evaluación esta en poder del evaluador " + NombreEvaluador;
                    else
                        ImageCommand.ToolTip = "La evaluación esta en poder del Auditor " + NombreAuditor;

                    

                    return;
                }
                //Sino no esta en el periodo de carga, o esta cerrada la evaluacion  tengo q abrir impresion
                if (!Periodocarga || HiddenFieldEstadoID.Value.Trim() == "4" || HiddenFieldEstadoID.Value.Trim() == "5")
                {
                    
                    ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                    if (Periodocarga)
                        ImageCommand.ToolTip = "Consultar evaluación en modo sólo lectura";
                    else
                        ImageCommand.ToolTip = "Finalizó el periodo de carga: Consultar evaluación en modo sólo lectura";

                    if (int.Parse(HiddenFieldTipoPeriodoID.Value) == 2)
                        ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "ImpresionMY.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                        ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "ExportarWordPMPO.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                        //ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    return;
                }

               
                 
               
                                

            }
        }

        protected void GridViewIniciados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                Label LinkButtonEstado = e.Row.FindControl("LinkButtonEstado") as Label;
                HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
                ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
                HiddenField HiddenFieldTipoPeriodoID = e.Row.FindControl("HiddenFieldTipoPeriodoID") as HiddenField;
                Label LabelEvaluador = (Label)e.Row.FindControl("LabelEvaluador");

                int FormularioID = int.Parse(HiddenFieldTipoFormularioID.Value);
                int Legajo = int.Parse(HiddenFieldLegajo.Value);
                int PeriodoID = int.Parse(HiddenFieldPeriodoID.Value);
                int PasoID = int.Parse(HiddenFieldEstadoID.Value);

                              
                if (HiddenFieldTipoPeriodoID.Value == "1")
                    PasoID = (PasoID == -1) ? 1 : PasoID;
                else
                    PasoID = (PasoID == -1) ? 2 : PasoID;

                //trae el form.aspx en paso 2 (en evaluado)
                string UrlForm = FormularioInfo.DameURLForm(int.Parse(HiddenFieldTipoFormularioID.Value), int.Parse(HiddenFieldPeriodoID.Value), PasoID);

                bool Periodocarga = ValidaPeriodo(PeriodoID);

                //trae el auditor
                int? LegajoAuditor = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(Legajo, PeriodoID, FormularioID, 3);
                int LegajoAudit;

                string NombreAuditor = string.Empty;
                FormulariosDS.vwUsuariosDataTable vwUsuariosDT = new FormulariosDS.vwUsuariosDataTable();

                if (LegajoAuditor != null)
                {
                    LegajoAudit = (int)LegajoAuditor;
                    vwUsuariosDT = FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(LegajoAudit);
                    if(vwUsuariosDT.Rows.Count>0)
                        NombreAuditor = vwUsuariosDT[0].Apellido + " " + vwUsuariosDT[0].Nombre;                    
                   
                }
               
                //popupPmpO
                if (Periodocarga && PasoID == 3)//el evaluado puede ver la pmp en modo lectura
                {
                    ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                    ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "ExportarWordPMPO.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    return;
                }


                //si esta en paso 1 o 3 no puede abrir su pmp
                if (PasoID == 1 || PasoID == 3)
                {
                    ImageCommand.ImageUrl = "~/pmp/img/infopmp.png";
                    ImageCommand.Enabled = false;
                    
                    if (PasoID == 1)
                        ImageCommand.ToolTip = "La evaluación esta en poder del evaluador " + LabelEvaluador.Text;
                    else
                        ImageCommand.ToolTip = "La evaluación esta en poder del auditor " + NombreAuditor;// +LabelEvaluador.Text;
                    return;
                }
                // si esta en periodo de carga y en el paso 2, entonces puede editar su pmp
                if (Periodocarga && PasoID==2)
                {
                    if (FormularioID == 11 || FormularioID == 10 || FormularioID == 12)
                        ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMPFY/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                    ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    
                    ImageCommand.ImageUrl = "~/pmp/img/editar.jpg";
                    ImageCommand.ToolTip = "Editar Evaluación";
                    return;
                }




                if (!Periodocarga || HiddenFieldEstadoID.Value.Trim() == "4") //Sino no esta en el periodo de carga o el form esta aprobado abro la impresion
                {

                    FormulariosDS.PeriodoDataTable PDT= FachadaDA.Singleton.Periodo.GetDataByID(PeriodoID);
                    bool ComenzoElPeriodoCarga=true;
                    if (PDT.Rows.Count > 0)
                    {
                        ComenzoElPeriodoCarga = !(PDT[0].CargaDesde > DateTime.Now);
                    }


                    if (!ComenzoElPeriodoCarga)
                    {

                        ImageCommand.ImageUrl = "~/pmp/img/infopmp.png";
                        ImageCommand.Enabled = false;
                        ImageCommand.ToolTip = "Aún no comenzó el periodo de carga." ;
                    }
                    else
                    {
                        ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                        if (!Periodocarga)
                            ImageCommand.ToolTip = "Finalizó el periodo de carga: Consultar evaluación en modo sólo lectura ";
                        else
                            ImageCommand.ToolTip = "Consultar evaluación en modo sólo lectura";

                        if (int.Parse(HiddenFieldTipoPeriodoID.Value) == 2) // si es MidYear
                            ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "ImpresionMY.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                        else
                            ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "ExportarWordPMPO.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                            //ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                        return;

                    }
                }

                    
                   
              
               

            }
        }

        protected void GridViewHistoricos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                Label LinkButtonEstado = e.Row.FindControl("LinkButtonEstado") as Label;
                HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
                HiddenField HiddenFieldEstado = e.Row.FindControl("HiddenFieldEstado") as HiddenField;
                ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
                HiddenField HiddenFieldTipoPeriodoID = e.Row.FindControl("HiddenFieldTipoPeriodoID") as HiddenField;

                int FormularioID = int.Parse(HiddenFieldTipoFormularioID.Value);
                //if ((FormularioID == 17) || (FormularioID == 18) || (FormularioID == 19))
                //{
                //    //http://intranetconosur.scj.com:8080/servicios/form_PMPContributors.asp?FormularioID=&TramiteID=19&FormEstado=NUEVO_PASO&ASP=form_PMPContributors.asp2085
                //    string urlform = (FormularioID == 17) ? "form_PMPIndividual.asp" : ((FormularioID == 18) ? "form_PMPManager.asp" : "form_PMPContributors.asp");
                //    string link = string.Format("abrirPMP('{0}','{1}','{2}');return false;", urlform, HiddenFieldTipoFormularioID.Value, Server.UrlEncode("&TramiteID=" + HiddenFieldLegajo.Value + "&FormEstado=NUEVO_PASO"));
                //    ImageCommand.Attributes.Add("onclick", link);
                   
                //    if (HiddenFieldEstado.Value != "Aprobado")
                //        ImageCommand.Visible = false;
                                       
                //    return;
                //}
                string folder= string.Empty;
                if (FormularioID == 10 || FormularioID == 11 || FormularioID == 12)
                    folder = "/PMPFY/";
                else
                    folder = "/PMP/";


                if (int.Parse(HiddenFieldTipoPeriodoID.Value) == 2)
                {
                    ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + folder + "ImpresionMY.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                }
                else
                {
                    if (folder == "/PMP/")
                        ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + folder + "ExportarWordPMPO.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else

                        ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + folder + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                }


                if (HiddenFieldEstadoID.Value != "4" && HiddenFieldEstadoID.Value != "6")
                    ImageCommand.Visible = false;



            }
        }

        #endregion
        
        #region Calibracion
        protected void ddlPeriodo_DataBound(object sender, EventArgs e)
        {
            DropDownList DDL = ((DropDownList)sender);
            FormulariosController f = new FormulariosController();
            foreach (ListItem item in DDL.Items)
            {
                bool IsAbierto = ((bool)f.ValidarPeriodoCarga(int.Parse(item.Value), DateTime.Now)); //ValidaPeriodo(int.Parse(item.Value));
                item.Text += IsAbierto ? " (Vigente)" : " (Cerrado)";
            }

            DropDownListEvaluador.DataSource = ObjectDataSourceEvaluadores;
            DropDownListEvaluador.DataBind();

            ListBoxEvaluado.DataSource = ObjectDataSourceEvaluados;
            ListBoxEvaluado.DataBind();

            ListBoxEvaluadoLegajo.DataSource = ObjectDataSourceEvaluadosLegajo;
            ListBoxEvaluadoLegajo.DataBind();
        }

        protected void ddlPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            ListBoxEvaluado.Items.Clear();
            ListItem Li1 = new ListItem();
            Li1.Value = "0";
            Li1.Text = "Todos";
            ListBoxEvaluado.Items.Add(Li1);
            ListBoxEvaluado.DataSource = ObjectDataSourceEvaluados;
            ListBoxEvaluado.DataBind();


            DropDownListEvaluador.Items.Clear();
            ListItem Li3 = new ListItem();
            Li3.Value = "0";
            Li3.Text = "Todos";
            DropDownListEvaluador.Items.Add(Li3);
            DropDownListEvaluador.DataSource = ObjectDataSourceEvaluadores;
            DropDownListEvaluador.DataBind();

            ListBoxEvaluadoLegajo.Items.Clear();
            ListItem Li4 = new ListItem();
            Li3.Value = "0";
            Li3.Text = "Todos";
            ListBoxEvaluadoLegajo.Items.Add(Li4);
            ListBoxEvaluadoLegajo.DataSource = ObjectDataSourceEvaluadosLegajo;
            ListBoxEvaluadoLegajo.DataBind();
           
            
        }

        protected void ListBoxFormularios_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            ListBoxArea.Items.Clear();
            
            ListItem Li1 = new ListItem();
            Li1.Value = "0";
            Li1.Text = "Todos";
            ListBoxArea.Items.Add(Li1);
            ListBoxArea.DataBind();
            ListBoxArea.SelectedValue = "0";


            
            ListBoxSector.Items.Clear();
            
            ListItem Li2 = new ListItem();
            Li2.Value = "0";
            Li2.Text = "Todos";
            ListBoxSector.Items.Add(Li2);
            ListBoxSector.DataBind();
            ListBoxSector.SelectedValue = "0";


            ListBoxCargos.Items.Clear();            
            ListItem Li3 = new ListItem();
            Li3.Value = "0";
            Li3.Text = "Todos";
            ListBoxCargos.Items.Add(Li3);
            ListBoxCargos.DataBind();
            ListBoxCargos.SelectedValue = "0";

            
            
        }
        
        protected void ButtonBuscarCalibracion_Click(object sender, EventArgs e)
        {
            FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable DTReporteCalibracionResum = new FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable();
            FachadaDA.Singleton.GetReporteParaCalibracionResum.Fill(DTReporteCalibracionResum, int.Parse(ddlPeriodo.SelectedValue), int.Parse(ListBoxFormularios.SelectedValue),
                int.Parse(ListBoxEstado.SelectedValue), ListBoxEvaluado.SelectedValue, ListBoxEvaluadoLegajo.SelectedValue, DropDownListEvaluador.SelectedValue, ListBoxSector.SelectedValue,
                ListBoxArea.SelectedValue, ListBoxCargos.SelectedValue, ListBoxResultado.SelectedValue);

            

            if (DTReporteCalibracionResum.Count > 0)
            {
                string Periodo = ddlPeriodo.SelectedValue;
                string Formulario =ListBoxFormularios.SelectedValue;
                string Estado = ListBoxEstado.SelectedValue;
                string Evaluado = ListBoxEvaluado.SelectedValue;
                string EvaluadoLegajo = ListBoxEvaluadoLegajo.SelectedValue;
                string Evaluador = DropDownListEvaluador.SelectedValue;
                string Sector = ListBoxSector.SelectedValue;
                string Area = ListBoxArea.SelectedValue;
                string Cargo = ListBoxCargos.SelectedValue;
                string Calificacion = ListBoxResultado.SelectedValue;

                string URL = string.Format("../PMP/Admin/PMPCalibracion.aspx?&PeriodoID={0}&FormularioID={1}&Estado={2}&Evaluado={3}&EvaluadoLegajo={4}&Evaluador={5}&Sector={6}&Area={7}&Cargo={8}&Calificacion={9}", Periodo, Formulario, Estado, Evaluado, EvaluadoLegajo, Evaluador, Sector, Area, Cargo, Calificacion);

                EjecutarScript("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);");
                LabelResultado.Visible = false;
            }
            else
            {
                LabelResultado.Visible = true;
            }

            //MultiViewReportes.SetActiveView(vwReporteParaCalibracion);
            //dtEvaluaciones = null;
            //GridViewEvaluados.DataSource = dtEvaluaciones;
            //GridViewEvaluados.DataBind();

        }

        protected void ButtonBuscarMiCalibracion_Click(object sender, EventArgs e)
        {

            int? periodoActualPMP;
            periodoActualPMP = com.paginar.formularios.businesslogiclayer.PeriodoInfo.GetPeriodoActualID(DateTime.Now);
            if (periodoActualPMP != null)
            {
                string EvaluadorActual = this.ObjectUsuario.legajo.ToString();
                FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable DTReporteCalibracionResum = new FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionResumDataTable();
                FachadaDA.Singleton.GetReporteParaCalibracionResum.Fill(DTReporteCalibracionResum, periodoActualPMP, 1, 0, "0", "0", EvaluadorActual, "0", "0", "0", "0");


                if (DTReporteCalibracionResum.Count > 0)
                {
                    string Periodo = periodoActualPMP.ToString();
                    string Formulario = "1";
                    string Estado = "0";
                    string Evaluado = "0";
                    string EvaluadoLegajo = "0";
                    string Evaluador = EvaluadorActual;
                    string Sector = "0";
                    string Area = "0";
                    string Cargo = "0";
                    string Calificacion = "0";

                    string URL = string.Format("../PMP/Admin/PMPCalibracion.aspx?&PeriodoID={0}&FormularioID={1}&Estado={2}&Evaluado={3}&EvaluadoLegajo={4}&Evaluador={5}&Sector={6}&Area={7}&Cargo={8}&Calificacion={9}", Periodo, Formulario, Estado, Evaluado, EvaluadoLegajo, Evaluador, Sector, Area, Cargo, Calificacion);

                    EjecutarScript("window.open('" + URL + "','PMPAdmin','left=10,top=10,width=950,height=620,scrollbars=yes');void(0);");
                    
                }
            }
            //MultiViewReportes.SetActiveView(vwReporteParaCalibracion);
            //dtEvaluaciones = null;
            //GridViewEvaluados.DataSource = dtEvaluaciones;
            //GridViewEvaluados.DataBind();

        }

        #endregion
        
        #region Objetivos
        protected void ODSObjetivos_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSObjetivos_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = CED;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = CED.PuedeAgregar();
        }

        
       
        protected void AgregarButton_Click(object sender, EventArgs e)
        {
             if (this.IsValid)
                EjecutarScript("popupObjetivos('/PMP/Objetivos.aspx',500,500);");           

        }


        protected void RepeaterObjetivos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item)
            {
                //HiddenField HiddenFieldObjetivoID = (HiddenField)e.Item.FindControl("HiddenFieldObjetivoID");

                HtmlTable Table = (HtmlTable)e.Item.FindControl("TableObjetivos");
             //Table.Attributes.Add("","");
                
            }
            if (e.Item.ItemType == ListItemType.Footer)
            {

                if (RepeaterObjetivos.Items.Count < 1)
                {
                    RepeaterObjetivos.Visible = false;
                    LabelEmptyDataObjetivosAnteriores.Visible = true;
                }
                else
                {
                    RepeaterObjetivos.Visible = true;
                    LabelEmptyDataObjetivosAnteriores.Visible = false;
                }

            }

            }

        protected void RepeaterObjetivosActual_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                CED.DelObjetivo(int.Parse(e.CommandArgument.ToString()), ObjectUsuario.usuarioid);
                RepeaterObjetivosActual.DataBind();
            }

           
        }

        protected void RepeaterObjetivosActual_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {

                if (RepeaterObjetivosActual.Items.Count < 1)
                {
                    RepeaterObjetivosActual.Visible = false;
                    LabelEmptyData.Visible = true;
                }
                else
                {
                    RepeaterObjetivosActual.Visible = true;
                    LabelEmptyData.Visible = false;
                }

            }
        }

        protected void ButtonAgregarAdjunto_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            if (FileUploadAdjunto.HasFile)
            {
                int fileSize = FileUploadAdjunto.PostedFile.ContentLength;
          
                // Allow only files less than 2,100,000 bytes (approximately 2 MB) to be uploaded.
                if (fileSize < 4200000)
                {

                    string filename = DateTime.Now.Ticks.ToString() + "_" + FileUploadAdjunto.FileName;//Path.GetFileNameWithoutExtension(FileUploadAdjunto.FileName) + "_" + DateTime.Now.Ticks.ToString() + Path.GetExtension(FileUploadAdjunto.FileName);
                    string strPath = MapPath("~/PMP/documents/") + filename;
                    FileUploadAdjunto.SaveAs(@strPath);
                    CED.UpdateAdjunto(this.ObjectUsuario.usuarioid, filename);
                    FormViewAdjunto.DataBind();

                }
                else
                {
                    // Notify the user why their file was not uploaded.
                    lblLimitUpload.Text = "El archivo que intento subir excede los 4 Mb.";
                }
            }
        }

        protected void FormViewAdjunto_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Adjunto")
            {
                CED.UpdateAdjunto(this.ObjectUsuario.usuarioid, "");
                FormViewAdjunto.DataBind();
            }
        }




        protected void CustomValidatorAdjunto_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (FileUploadAdjunto.HasFile)
            {
                string extension = Path.GetExtension(FileUploadAdjunto.FileName);
                extension = extension.ToLower();
                args.IsValid = (extension == ".txt" || extension == ".xls" || extension == ".doc" || extension == ".pdf" || extension == ".jpg" || extension == ".xlsx" || extension == ".docx");
                    

            }
            
        }

        #endregion

        #region Reportes

        protected string DameNombreColumna(object Area, object Direccion, object Evaluador)
        {
            if ( string.IsNullOrEmpty( Evaluador.ToString() ) )
            {
                return "Total";

            }
            else {
                return "<b>Evaluador:</b> " + Evaluador.ToString() + "<br/><b>Dirección:</b> " + Direccion.ToString() + " <b>Area:</b> " + Area.ToString();
            
              
            }


        }

        protected string DameNombreColumna(object Nombre1, object Nombre2)
        {
            if (!string.IsNullOrEmpty(Nombre1.ToString()))
                return (Nombre1.ToString());
            else
            {
                if (!string.IsNullOrEmpty(Nombre2.ToString()))
                {
                    if (DropDownListAgrupadoPorRS.SelectedValue == "Direccion")
                        return  Nombre2.ToString();
                    else
                    return ("<b>Dirección:</b> " + Nombre2.ToString());
                }
                else
                    return "Total";
                }

        }

        protected string DameEstilo(object Area, object Direccion)
        {
            if (!string.IsNullOrEmpty(Area.ToString()))
                return ("");
            else
            {
                if (!string.IsNullOrEmpty(Direccion.ToString()))

                    return ("background-color: #F1F1F1");
                else
                    return "";
            }

        }
        protected string CrearLinkMYDireccionArea(object AuxDireccionID, object AuxPuntaje, object AuxPasoID, object AuxAreaID, object CantidadEvaluaciones)
        {
            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            int Alto = 200 + 10 * int.Parse(CantidadEvaluaciones.ToString());
            Alto = (Alto >= 400) ? 400 : Alto;

            

            string DireccionID = AuxDireccionID.ToString();
            DireccionID = (DropDownListDireccionRS.SelectedValue != "0") ? DropDownListDireccionRS.SelectedValue : DireccionID;

            string AreaID = AuxAreaID.ToString();
            AreaID = (DropDownListAreaRS.SelectedValue != "0") ? DropDownListAreaRS.SelectedValue : AreaID;

            string PasoID = AuxPasoID.ToString();
            PasoID = (DropDownListEstadoRS.SelectedValue != "0") ? DropDownListEstadoRS.SelectedValue : PasoID;

            string LegajoEvaluador = DropDownListEvaluadorRS.SelectedValue;

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            decimal Puntaje = decimal.Parse(AuxPuntaje.ToString());
            

            Label LblCantidad = new Label();
            LblCantidad.ID = "LblCantidad" + Puntaje.GetType().Name + DireccionID + AreaID;
            HyperLink LnkCantidad = new HyperLink();
            LnkCantidad.ID = "LnkCantidad" + Puntaje.GetType().Name + DireccionID + AreaID;


            if (Puntaje > 0)
            {
                string Cluster = DameElementosListBox(DropDownListCluster);
                LnkCantidad.NavigateUrl = string.Format("javascript:popupPmpORep('/PMP/Admin/ReporteSeguimientoEvaluados.aspx?DireccionID={0}&AreaID={1}&PasoID={2}&PeriodoID={3}&Cluster={4}&LegajoEvaluador={5}',600,{6})", DireccionID, AreaID, PasoID, PeriodoID, Cluster, 0,Alto);
                LnkCantidad.Text = Puntaje.ToString();
                LnkCantidad.CssClass = "link";
                LnkCantidad.RenderControl(htmlWrite);

                
            }
            else
            {
                LblCantidad.Text = Puntaje.ToString();
                LblCantidad.RenderControl(htmlWrite);

            }
            return stringWrite.ToString();
        }

        protected string CrearLinkMYEvalaudorDireccion(object LegajoEvaluador, object Puntaje, int PasoIDAux, object DireccionIDAux, object CantidadEvaluaciones)
        {
           
            int TipoPeriodoID= DameTipoPeriodoID();

            int Alto = 200 + 10 * int.Parse(CantidadEvaluaciones.ToString());
            Alto = (Alto >= 400) ? 400 : Alto;

            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);
            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            LegajoEvaluador = (DropDownListEvaluadorRS.SelectedValue != "0") ? DropDownListEvaluadorRS.SelectedValue : LegajoEvaluador;


            
            string DireccionID = (DropDownListDireccionRS.SelectedValue != "0") ? DropDownListDireccionRS.SelectedValue : DireccionIDAux.ToString();
            string AreaID = DropDownListAreaRS.SelectedValue;

            //string cluster = DropDownListCluster.SelectedValue;//HiddenFieldCluster.Value;
            string Cluster = DameElementosListBox(DropDownListCluster);
            string PasoID = PasoIDAux.ToString();
                   PasoID = (DropDownListEstadoRS.SelectedValue != "0") ? DropDownListEstadoRS.SelectedValue : PasoID;

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            decimal PuntajeAux = decimal.Parse(Puntaje.ToString());

           

            Label LblCantidad = new Label();
            LblCantidad.ID = "LblCantidad" + Puntaje.GetType().Name + LegajoEvaluador.ToString() + PasoID.ToString();
            HyperLink LnkCantidad = new HyperLink();
            LnkCantidad.ID = "LnkCantidad" + Puntaje.GetType().Name + LegajoEvaluador.ToString() + PasoID.ToString();

            
            if (PuntajeAux > 0)
            {
                LnkCantidad.NavigateUrl = string.Format("javascript:popupPmpORep('/PMP/Admin/ReporteSeguimientoEvaluados.aspx?LegajoEvaluador={0}&PasoID={1}&PeriodoID={2}&DireccionID={3}&Cluster={4}&AreaID={5}',600,{6})", LegajoEvaluador, PasoID, PeriodoID, DireccionID, Cluster, AreaID, Alto);
                LnkCantidad.Text = Puntaje.ToString();
                LnkCantidad.CssClass = "link";
                LnkCantidad.RenderControl(htmlWrite);
                               
            }
            else
            {
                LblCantidad.Text = Puntaje.ToString();
                LblCantidad.RenderControl(htmlWrite);

            }
            return stringWrite.ToString();
        }

        protected string CrearLinkOp(object LegajoEvaluador, int PeriodoID, int Pasoid, int TipoFormularioID, string Calificacion, object Puntaje, int EstadoID2, int AreaID)
        {
            PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoID, 1);

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            decimal PuntajeAux = decimal.Parse(Puntaje.ToString());
            //switch(Puntaje.GetType().Name)

            Label LblCantidad = new Label();
            LblCantidad.ID = "LblCantidad" + Puntaje.GetType().Name + LegajoEvaluador.ToString() + Pasoid.ToString() + Calificacion;
            HyperLink LnkCantidad = new HyperLink();
            if (DropDownListEvaluadorRS.SelectedValue != "0")
                LegajoEvaluador = DropDownListEvaluadorRS.SelectedValue;

            LnkCantidad.ID = "LnkCantidad" + Puntaje.GetType().Name + LegajoEvaluador.ToString() + Pasoid.ToString() + Calificacion;
            if (PuntajeAux > 0)
            {
                LnkCantidad.NavigateUrl = string.Format("javascript:popupPmpORep('/PMP/Admin/ReporteSeguimientoByUsuario.aspx?LegajoEvaluador={0}&Pasoid={1}&TipoFormularioID={2}&Calificacion={3}&PeriodoID={4}&EstadoID2={5}&AreaID={6}',500,300)", LegajoEvaluador, Pasoid, TipoFormularioID, Calificacion, PeriodoID, EstadoID2, AreaID);
                LnkCantidad.Text = Puntaje.ToString();
                LnkCantidad.CssClass = "link";

                LnkCantidad.RenderControl(htmlWrite);

                //return string.Format("popup('ReporteSeguimientoByUsuario.aspx?LegajoEvaluador={0}&Pasoid={1}&TipoFormularioID={2}&Calificacion={3}&PeriodoID={4}',600,600)", LegajoEvaluador, Pasoid, TipoFormularioID, Calificacion, PeriodoID);
            }
            else
            {
                LblCantidad.Text = Puntaje.ToString();
                LblCantidad.RenderControl(htmlWrite);

            }
            return stringWrite.ToString();
        }

        private int DameTipoPeriodoID()
        {
            
            int TipoEvaluacion = int.Parse(DropDownListTipoEvaluacionRS.SelectedValue);
            String TipoEvaluado = DropDownListTipoEvaluadoRS.SelectedValue;

            int TipoPeriodoID = 1;

            if (TipoEvaluado == "Administrativos" && DropDownListTipoEvaluacionRS.SelectedValue == "1")
                TipoPeriodoID = 3;

            if (TipoEvaluado == "Administrativos" && DropDownListTipoEvaluacionRS.SelectedValue == "2")
                TipoPeriodoID = 2;

            if (TipoEvaluado == "Operarios" && DropDownListTipoEvaluacionRS.SelectedValue == "1")
                TipoPeriodoID = 1;

            return TipoPeriodoID;
        }      

        protected void ButtonBuscarReportesDeSeguimiento_Click(object sender, EventArgs e)
        {

            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            String AgrupadoPor = DropDownListAgrupadoPorRS.SelectedValue;
            int Evaluador = int.Parse(DropDownListEvaluadorRS.SelectedValue);            
            


            

            if (PeriodoID < 0)
            {
                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);
                return;
            }


                ControllerReportesDeSeguimiento CRS = new ControllerReportesDeSeguimiento();
                string Cluster = DameElementosListBox(DropDownListCluster);
                if (DropDownListTipoEvaluadoRS.SelectedValue == "Administrativos")
                {
                    switch (AgrupadoPor)
                    {
                        case "Evaluador":

                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable ReporteSeguimientoAdministrativosMYDT = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable();
                            ReporteSeguimientoAdministrativosMYDT = CRS.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster);

                            if (ReporteSeguimientoAdministrativosMYDT.Rows.Count > 0)
                            {
                                RepeaterEvaluacionesPorEvaluador.DataSource = ReporteSeguimientoAdministrativosMYDT;
                                RepeaterEvaluacionesPorEvaluador.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoAdministrativosPorEvaluador);


                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);
                            break;
                        case "Direccion/Area":
                            //ControllerReportesDeSeguimiento CRS = new ControllerReportesDeSeguimiento();

                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable RSDT = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable();
                            RSDT = CRS.Eval_GetReporteSeguimientoAgrupadoAreaDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster, 1);
                            if (RSDT.Rows.Count > 0)
                            {

                                RepeaterEvaluacionesPorDireccionArea.DataSource = RSDT;
                                RepeaterEvaluacionesPorDireccionArea.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoAdministrativosPorDireccionArea);
                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);

                            break;

                        case "Direccion":
                            //ControllerReportesDeSeguimiento CRS = new ControllerReportesDeSeguimiento();
                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable RSDT1 = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable();
                            RSDT1 = CRS.Eval_GetReporteSeguimientoAgrupadoAreaDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster, 0);
                            if (RSDT1.Rows.Count > 0)
                            {

                                RepeaterEvaluacionesPorDireccionArea.DataSource = RSDT1;
                                RepeaterEvaluacionesPorDireccionArea.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoAdministrativosPorDireccionArea);
                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);

                            break;
                    }

                }

                else {

                    switch (AgrupadoPor)
                    {
                        case "Evaluador":

                          

                                DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable ReporteSeguimientoOperariosDT = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable();
                                ReporteSeguimientoOperariosDT = CRS.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster);

                                if (ReporteSeguimientoOperariosDT.Rows.Count > 0)
                                {
                                       RepeaterReporteSeguimientoOperariosPorEvaluador.DataSource = ReporteSeguimientoOperariosDT;
                                        RepeaterReporteSeguimientoOperariosPorEvaluador.DataBind();
                                        MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoOperariosPorEvaluador);


                                       // drawChart(ReporteSeguimientoOperariosDT);

                                }
                                else
                                    MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);
                           
                            break;
                        case "Evaluador/Rating":



                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable ReporteSeguimientoOperariosDetalleDT = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable();
                            ReporteSeguimientoOperariosDetalleDT = CRS.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster);

                            if (ReporteSeguimientoOperariosDetalleDT.Rows.Count > 0)
                            {

                                RepeaterReporteSeguimientoOperariosPorEvaluadorDetalle.DataSource = ReporteSeguimientoOperariosDetalleDT;
                                RepeaterReporteSeguimientoOperariosPorEvaluadorDetalle.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoOperariosPorEvaluadorDetalle);
                               

                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);

                            break;
                        case "Direccion/Area":
                            

                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable RSDT = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable();
                            RSDT = CRS.Eval_GetReporteSeguimientoAgrupadoAreaDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster, 1);
                            if (RSDT.Rows.Count > 0)
                            {

                                RepeaterReporteSeguimientoOperariosPorDireccionArea.DataSource = RSDT;
                                RepeaterReporteSeguimientoOperariosPorDireccionArea.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoOperariosPorDireccionArea);


                                //drawChart(RSDT, "area");
                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);

                            break;

                        case "Direccion":
                            
                            DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable RSDT1 = new DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable();
                            RSDT1 = CRS.Eval_GetReporteSeguimientoAgrupadoAreaDireccion(PeriodoID, Evaluador, int.Parse(DropDownListEstadoRS.SelectedValue), 0, int.Parse(DropDownListDireccionRS.SelectedValue), int.Parse(DropDownListAreaRS.SelectedValue), Cluster, 0);
                            if (RSDT1.Rows.Count > 0)
                            {

                                RepeaterReporteSeguimientoOperariosPorDireccionArea.DataSource = RSDT1;
                                RepeaterReporteSeguimientoOperariosPorDireccionArea.DataBind();
                                MultiViewReporteSeguimiento.SetActiveView(ViewReporteSeguimientoOperariosPorDireccionArea);

                                //drawChart(RSDT1, "direccion");
                            }
                            else
                                MultiViewReporteSeguimiento.SetActiveView(ViewSinResultados);

                            break;
                    }
                }

         



        }

        protected void ButtonExportarReportesDeSeguimiento_Click(object sender, EventArgs e)
        {
           
            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);
            

            
                string URL=string.Empty;
                string Cluster = DameElementosListBox(DropDownListCluster);

                if (DropDownListTipoEvaluadoRS.SelectedValue == "Operarios")
                {
                    switch (DropDownListAgrupadoPorRS.SelectedValue)
                    {
                        case "Evaluador":
                            if (DropDownListIncluirDetalle.SelectedValue == "SI")
                               URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                            else
                                URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                           // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                            break;
                        case "Evaluador/Rating":
                            //if (DropDownListIncluirDetalle.SelectedValue == "SI")
                            //    URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                            //else
                            URL = string.Format("../PMP/Admin/ReporteSeguimientoAdmPorEvaluadorRatingImpresion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                            
                            break;
                        case "Direccion/Area":
                            if (DropDownListIncluirDetalle.SelectedValue == "SI")
                            URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionAgrupadoAreaDireccionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=1&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                            else
                                URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionAgrupadoAreaDireccion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=1&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                           // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                            break;

                        case "Direccion":
                            if (DropDownListIncluirDetalle.SelectedValue == "SI")
                                URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionAgrupadoAreaDireccionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=0&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                            else
                                URL = string.Format("../PMP/Admin/ReporteSeguimientoOpImpresionAgrupadoAreaDireccion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=0&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                           // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                            break;
                    }

                }

                else
                {

                   
                        switch (DropDownListAgrupadoPorRS.SelectedValue)
                        {
                            case "Evaluador":
                                if(DropDownListIncluirDetalle.SelectedValue=="SI")
                                   URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);

                                else
                                    URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                               // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                                break;
                            case "Direccion/Area":
                                if (DropDownListIncluirDetalle.SelectedValue == "SI")
                                   URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresionAgrupadoAreaDireccionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=1&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                                else
                                    URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresionAgrupadoAreaDireccion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=1&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                               // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                                break;

                            case "Direccion":
                                if (DropDownListIncluirDetalle.SelectedValue == "SI")
                                URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresionAgrupadoAreaDireccionConDetalle.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=0&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                                else
                                    URL = string.Format("../PMP/Admin/ReporteSeguimientoMYImpresionAgrupadoAreaDireccion.aspx?PeriodoID={0}&LegajoEvaluador={1}&PasoID={2}&DireccionID={3}&AreaID={4}&Cluster={5}&MostrarAreas=0&IncluirNoConcluidas={6}&IncluirDetalle={7}", PeriodoID, DropDownListEvaluadorRS.SelectedValue, DropDownListEstadoRS.SelectedValue, DropDownListDireccionRS.SelectedValue, DropDownListAreaRS.SelectedValue, Cluster, DropDownListIncluirNoConluidas.SelectedValue, DropDownListIncluirDetalle.SelectedValue);
                               // EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
                                break;
                        }

                    
                
                
                }

                EjecutarScript("window.open('" + URL + "','PMPReporteSeguimientoMY','left=10,top=10,width=800,height=620,scrollbars=yes');void(0);");
           

        }

        protected void RepeaterReporteSeguimientoOperariosPorEvaluadorDetalle_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //ObjectDataSource obds1;
            int PeriodoID;
            PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(int.Parse(DropDownListPeriodoFiscalRS.SelectedValue), 1);

            int Estado = int.Parse(DropDownListEstadoRS.SelectedValue);
            int Evaluador = int.Parse(DropDownListEvaluadorRS.SelectedValue);
            int DireccionID = int.Parse(DropDownListDireccionRS.SelectedValue);

            int EvaluadorFila;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HiddenFieldLegajoEvaluador = (HiddenField)e.Item.FindControl("HiddenFieldLegajoEvaluador");
                HiddenField HiddenFieldCantTotal = (HiddenField)e.Item.FindControl("HiddenFieldCantTotal");
               // HiddenField HiddenFieldAreaID = (HiddenField)e.Item.FindControl("HiddenFieldAreaID");
                int AreaID = 0;

                DSEvalDesemp.Eval_GetReporteSeguimientoByCalificacionByEvaluadorDataTable DTReporteSeguimiento = new DSEvalDesemp.Eval_GetReporteSeguimientoByCalificacionByEvaluadorDataTable();

                int LegajoEvaluador;
                int.TryParse(HiddenFieldLegajoEvaluador.Value, out LegajoEvaluador);

                EvaluadorFila = (DropDownListEvaluadorRS.SelectedValue == "0") ? LegajoEvaluador : int.Parse(DropDownListEvaluadorRS.SelectedValue);

                Repeater RepeaterAsignadoEvaluadosMB = (Repeater)e.Item.FindControl("RepeaterAsignadoEvaluadosMB");
                Repeater RepeaterAsignadoEvaluadosB = (Repeater)e.Item.FindControl("RepeaterAsignadoEvaluadosB");
                Repeater RepeaterAsignadoEvaluadosR = (Repeater)e.Item.FindControl("RepeaterAsignadoEvaluadosR");
                Repeater RepeaterAsignadoEvaluadosNS = (Repeater)e.Item.FindControl("RepeaterAsignadoEvaluadosNS");

                if (string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
                {
                    RepeaterAsignadoEvaluadosMB.Visible = false;
                    RepeaterAsignadoEvaluadosR.Visible = false;
                    RepeaterAsignadoEvaluadosB.Visible = false;
                    RepeaterAsignadoEvaluadosNS.Visible = false;
                
                }
                else
                {

                    
                    Repositorio.AdapterEvalDesemp.GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter.Fill(DTReporteSeguimiento, PeriodoID, EvaluadorFila, Estado, int.Parse(HiddenFieldCantTotal.Value), "MB", AreaID);
                    RepeaterAsignadoEvaluadosMB.DataSource = DTReporteSeguimiento;
                    RepeaterAsignadoEvaluadosMB.DataBind();

                    
                    Repositorio.AdapterEvalDesemp.GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter.Fill(DTReporteSeguimiento, PeriodoID, EvaluadorFila, Estado, int.Parse(HiddenFieldCantTotal.Value), "B", AreaID);
                    RepeaterAsignadoEvaluadosB.DataSource = DTReporteSeguimiento;
                    RepeaterAsignadoEvaluadosB.DataBind();

                    
                    Repositorio.AdapterEvalDesemp.GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter.Fill(DTReporteSeguimiento, PeriodoID, EvaluadorFila, Estado, int.Parse(HiddenFieldCantTotal.Value), "R", AreaID);
                    RepeaterAsignadoEvaluadosR.DataSource = DTReporteSeguimiento;
                    RepeaterAsignadoEvaluadosR.DataBind();

                    
                    Repositorio.AdapterEvalDesemp.GetReporteSeguimientoByCalificacionByEvaluadorTableAdapter.Fill(DTReporteSeguimiento, PeriodoID, EvaluadorFila, Estado, int.Parse(HiddenFieldCantTotal.Value), "NS", AreaID);
                    RepeaterAsignadoEvaluadosNS.DataSource = DTReporteSeguimiento;
                    RepeaterAsignadoEvaluadosNS.DataBind();
                }

            }

            //if (e.Item.ItemType == ListItemType.Footer)
            //{
            //    Repeater RepeaterTotales = (Repeater)e.Item.FindControl("RepeaterTotales");
            //    DSEvalDesemp.Eval_GetReporteSeguimientoTotalesByEvaluadorDataTable dtReporteSeguimientoTotales = new DSEvalDesemp.Eval_GetReporteSeguimientoTotalesByEvaluadorDataTable();
            //    Repositorio.AdapterEvalDesemp.GetReporteSeguimientoTotalesByEvaluadorTableAdapter.Fill(dtReporteSeguimientoTotales, PeriodoID, Evaluador, Estado, 0, DireccionID, 0);
            //    RepeaterTotales.DataSource = dtReporteSeguimientoTotales;
            //    RepeaterTotales.DataBind();

            //}
        }
        protected void RepeaterEvaluacionesMYPorDireccionArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Header)
            {
                if (DropDownListIncluirNoConluidas.SelectedValue == "NO")
                {
                    HtmlTableCell COL_TO_HIDE_HEADER0 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER0");
                    HtmlTableCell COL_TO_HIDE_HEADER1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER1");
                    HtmlTableCell COL_TO_HIDE_HEADER2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER2");
                    COL_TO_HIDE_HEADER0.Visible = false;
                    COL_TO_HIDE_HEADER1.Visible = false;
                    COL_TO_HIDE_HEADER2.Visible = false;

                }

                Label LabelTitulo = (Label)e.Item.FindControl("LabelTitulo");
                if (DropDownListAgrupadoPorRS.SelectedValue=="Direccion")
                    LabelTitulo.Text = "Dirección";



            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (DropDownListIncluirNoConluidas.SelectedValue == "NO")
                {
                    HtmlTableCell COL_TO_HIDE1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE1");
                    HtmlTableCell COL_TO_HIDE2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE2");

                    COL_TO_HIDE1.Visible = false;
                    COL_TO_HIDE2.Visible = false;


                }

            }
        }
        protected void RepeaterEvaluacionesMYPorEvaluador_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            

            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            

            int Evaluador = int.Parse(DropDownListEvaluadorRS.SelectedValue);

            

            int Estado = int.Parse(DropDownListEstadoRS.SelectedValue);

            int DireccionID = int.Parse(DropDownListDireccionRS.SelectedValue);
            if (e.Item.ItemType == ListItemType.Header)
            {
                if (DropDownListIncluirNoConluidas.SelectedValue == "NO")
                {
                    HtmlTableCell COL_TO_HIDE_HEADER0 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER0");
                    HtmlTableCell COL_TO_HIDE_HEADER1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER1");
                    HtmlTableCell COL_TO_HIDE_HEADER2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER2");
                    COL_TO_HIDE_HEADER0.Visible = false;
                    COL_TO_HIDE_HEADER1.Visible = false;
                    COL_TO_HIDE_HEADER2.Visible = false;

                }
                
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (DropDownListIncluirNoConluidas.SelectedValue == "NO")
                {
                    HtmlTableCell COL_TO_HIDE1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE1");
                    HtmlTableCell COL_TO_HIDE2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE2");

                    COL_TO_HIDE1.Visible = false;
                    COL_TO_HIDE2.Visible = false;
                
                
                }
            
            }


        }
        
        protected void DropDownListAgrupadoPorRS_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiViewReporteSeguimiento.SetActiveView(ViewVacia);
            //LLenarEstados(int.Parse(DropDownListTipoEvaluacionRS.SelectedValue), DropDownListTipoEvaluadoRS.SelectedValue);
            switch (DropDownListAgrupadoPorRS.SelectedValue)
            {
                case "Area":
                    if(Session["DireccionID"] == null)
                      DropDownListDireccionRS.Enabled = true;

                    DropDownListEvaluadorRS.Enabled = false;
                    DropDownListEvaluadorRS.SelectedIndex = 0;
                    if(Session["AreaID"]==null)
                       DropDownListAreaRS.Enabled = true;
                    PanelOpcionesMY.Visible = true;
                    break;
                case "Direccion":
                    if (Session["DireccionID"] == null)
                       DropDownListDireccionRS.Enabled = true;
                    DropDownListEvaluadorRS.Enabled = false;
                    DropDownListEvaluadorRS.SelectedIndex = 0;
                    if (Session["AreaID"] == null)
                        DropDownListAreaRS.Enabled = true;
                    PanelOpcionesMY.Visible = true;
                    break;
                case "Direccion/Area":
                    if (Session["DireccionID"] == null)
                        DropDownListDireccionRS.Enabled = true;
                    DropDownListEvaluadorRS.Enabled = false;
                    DropDownListEvaluadorRS.SelectedIndex = 0;
                    if (Session["AreaID"] == null)
                        DropDownListAreaRS.Enabled = true;
                    PanelOpcionesMY.Visible = true;
                    break;
                case "Evaluador":
                    DropDownListEvaluadorRS.Enabled = true;

                    DropDownListDireccionRS.Enabled = false;
                    if (Session["DireccionID"] != null)
                        DropDownListDireccionRS .SelectedValue= Session["DireccionID"].ToString();
                    else
                       DropDownListDireccionRS.SelectedIndex = 0;

                    DropDownListDireccionRS_SelectedIndexChanged(null, null);
                    DropDownListAreaRS.Enabled = false;

                    if (Session["AreaID"] != null)
                        DropDownListAreaRS.SelectedValue = Session["AreaID"].ToString();
                    else
                        DropDownListAreaRS.SelectedIndex = 0;
                    PanelOpcionesMY.Visible = true;

                    break;

                case "Evaluador/Rating":
                    DropDownListEvaluadorRS.Enabled = true;

                    DropDownListDireccionRS.Enabled = false;
                    if (Session["DireccionID"] != null)
                        DropDownListDireccionRS.SelectedValue = Session["DireccionID"].ToString();
                    else
                        DropDownListDireccionRS.SelectedIndex = 0;

                    DropDownListDireccionRS_SelectedIndexChanged(null, null);
                    DropDownListAreaRS.Enabled = false;

                    if (Session["AreaID"] != null)
                        DropDownListAreaRS.SelectedValue = Session["AreaID"].ToString();
                    else
                        DropDownListAreaRS.SelectedIndex = 0;

                    PanelOpcionesMY.Visible = false;

                    break;
            }




        }

        protected void DropDownListTipoEvaluadoRS_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiViewReporteSeguimiento.SetActiveView(ViewVacia);
            String TipoEvaluado = DropDownListTipoEvaluadoRS.SelectedValue;

            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            

           LLenarEvaluadores(PeriodoID);
           setearDropDownListIncluirNoConluidas(null);

          

            if (TipoEvaluado == "Administrativos")
            {
                DropDownListTipoEvaluacionRS.Enabled = true;
                //if(DropDownListAgrupadoPorRS.Items.FindByValue("Evaluador/Rating")!=null)
                   //DropDownListAgrupadoPorRS.Items.Remove(DropDownListAgrupadoPorRS.Items.FindByValue("Evaluador/Rating"));
            }

            else
            {
                DropDownListTipoEvaluacionRS.SelectedValue = "1";
                DropDownListTipoEvaluacionRS.Enabled = false;
                //if (DropDownListAgrupadoPorRS.Items.FindByValue("Evaluador/Rating") == null)
                    //DropDownListAgrupadoPorRS.Items.Add(new ListItem("Evaluador/Rating", "Evaluador/Rating"));
            }

            //DropDownListAgrupadoPorRS.SelectedIndex = 0;
            DropDownListAgrupadoPorRS_SelectedIndexChanged(null, null);
        }
        
        protected void DropDownListTipoEvaluacionRS_SelectedIndexChanged(object sender, EventArgs e)
        {

            MultiViewReporteSeguimiento.SetActiveView(ViewVacia);
            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);
            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);
            LLenarEvaluadores(PeriodoID);
            setearDropDownListIncluirNoConluidas(null);
        }

        protected void setearDropDownListIncluirNoConluidas( int? PeriodoID)
        {
        

            if (PeriodoID != null)
            {
                
                if (!ValidaPeriodo(int.Parse(PeriodoID.ToString())))
                {
                    DropDownListIncluirNoConluidas.SelectedValue = "SI";
                    DropDownListIncluirNoConluidas.Enabled = false;

                }
                else
                {
                    DropDownListIncluirNoConluidas.SelectedValue = "NO";
                    DropDownListIncluirNoConluidas.Enabled = true;

                }

            }
            
           
        }


        protected void DropDownListPeriodoFiscalRS_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiViewReporteSeguimiento.SetActiveView(ViewVacia);
            int TipoPeriodoID = DameTipoPeriodoID();
            int PeriodoFiscalID = int.Parse(DropDownListPeriodoFiscalRS.SelectedValue);

            int PeriodoID = DamePeriodoIDByPeriodoFiscalTipoEvaluacion(PeriodoFiscalID, TipoPeriodoID);

            

            LLenarEvaluadores(PeriodoID);
            setearDropDownListIncluirNoConluidas(PeriodoID);
       }

        protected void DropDownListDireccionRS_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiViewReporteSeguimiento.SetActiveView(ViewVacia);
            DropDownListAreaRS.Items.Clear();
            DropDownListAreaRS.Items.Add(new ListItem("Todos", "0"));
            DropDownListAreaRS.DataBind();

        }
        protected void LLenarEvaluadores(int PeriodoID)
        {

            
            ControllerReportesDeSeguimiento CRS = new ControllerReportesDeSeguimiento();

         
            DSReportesDeSeguimiento.EvaluadoresDataTable EvaluadoresDT;
            DropDownListEvaluadorRS.Items.Clear();
            

             DropDownListEvaluadorRS.Items.Add(new ListItem("Todos", "0"));
                EvaluadoresDT = CRS.GetEvaluadoresByPeriodoID(PeriodoID);
                DropDownListEvaluadorRS.DataSource = EvaluadoresDT;
                DropDownListEvaluadorRS.DataBind();
          
        }
        

        private string DameElementosListBox(ListBox lst)
        {
            int[] lstbItems = lst.GetSelectedIndices();
            String Elementos = "";

            foreach (int i in lstbItems)
            {
                if (lst.Items[i].Value.ToString() == "0")
                {
                    Elementos = lst.Items[i].Value.ToString();
                    break;
                }
                else
                {
                    Elementos = Elementos + "," + lst.Items[i].Value.ToString();
                }
            }
            return Elementos.Trim(',');

        }
    
        #endregion

        
        protected void btnConsultarUbicacion_Click(object sender, EventArgs e)
        {
         
            
            try
            {
                FormulariosDS.PeriodoDataTable PDT = new FormulariosDS.PeriodoDataTable();
                PDT = FachadaDA.Singleton.Periodo.GetDataByTipoPeriodoIDPeriodoFIscalID(int.Parse(DDTipoPMP.SelectedValue.ToString()), int.Parse(ddPeriodoUbicacion.SelectedValue.ToString()));
                HFPeriodoSelect.Value = PDT.Rows[0].ItemArray[0].ToString();
                string CargaHasta = PDT.Rows[0].ItemArray[5].ToString();                               
                DateTime FechaLImite = Convert.ToDateTime(CargaHasta);  
                FormulariosDS.UbicacionEvaluadoDataTable UbicacionEval = FachadaDA.Singleton.UbicacionEvaluadoTableAdapter.GetUbicacionByLegajoPeriodoID(int.Parse(tblegajoubicacion.Text), int.Parse(HFPeriodoSelect.Value.ToString()));
                HFETipoFormularioID.Value = UbicacionEval.Rows[0].ItemArray[7].ToString();
                GVUbicacion.Visible = true;
                GVUbicacion.DataBind();
                GVUbicacion.Visible = true;
                

                //int PeriodoID, int legajo, int TipoFormularioID

                int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(tblegajoubicacion.Text), Convert.ToInt32(HFPeriodoSelect.Value.ToString()), Convert.ToInt32(HFETipoFormularioID.Value.ToString()));//, Convert.ToInt32(DDTipoPMP.SelectedValue.ToString()));
                if (GVUbicacion.Rows.Count < 1)
                {
                    lblDatosUbicacion.Text = "<br />La búsqueda no produjo resultados para los parámetros indicados";
                    btnEnviarEvaluacion.Visible = false;
                    btnActualizar.Visible = false;
                }
                else
                {
                    btnEnviarEvaluacion.Visible = true;
                    btnActualizar.Visible = true;
                    btnLimpiar.Visible = true;
                    btnActualizar.Enabled = true;
                    btnLimpiar.Enabled = true;
                    btnEnviarEvaluacion.Visible = true;
                   // btnEnviarEvaluacion.Enabled = false;
                    string estado = GVUbicacion.Rows[0].Cells[5].Text.ToString().ToLower();

                    if (((PasoActualID >= 1 && PasoActualID <= 3) || PasoActualID == -1) && (DateTime.Compare(DateTime.Now, FechaLImite) < 0))
                    {
                        btnEnviarEvaluacion.Enabled = true;
                    }
                    else
                    {
                        btnEnviarEvaluacion.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                GVUbicacion.Visible = false;
                btnEnviarEvaluacion.Visible = false;
                btnActualizar.Visible = false;
                lblDatosUbicacion.Text = "<br />La búsqueda no produjo resultados para los parámetros indicados";
            }

          
            
        }


        protected string FormatDate(DateTime Fecha)
        {
            return Fecha.ToShortDateString();
        }

        public string getEstadoU()
        {
            if (tblegajoubicacion.Text.Length == 0)
                return " ";
            int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(tblegajoubicacion.Text), Convert.ToInt32(HFPeriodoSelect.Value.ToString()), Convert.ToInt32(HFETipoFormularioID.Value.ToString()));//, Convert.ToInt32(DDTipoPMP.SelectedValue.ToString()));

            /*
1	En Evaluador
2	En Evaluado
3	En Auditor
4	Aprobado
5	Sin Evaluación
6	No concluida             
             */

            if (PasoActualID >= 1 && PasoActualID <= 3)
                return "Abierta";
            if (PasoActualID == 4)
                return "Aprobada";
            if (PasoActualID == 5)
                return "Sin Evaluación";
            if (PasoActualID == 6)
                return "No concluida";
            if (PasoActualID == -1)
                return "No Iniciada";
            
                
            return " ";


        }

        public string getUbicacionU()
        {
            if (tblegajoubicacion.Text.Length == 0)
                return " ";
            int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(tblegajoubicacion.Text), Convert.ToInt32(HFPeriodoSelect.Value.ToString()), Convert.ToInt32(HFETipoFormularioID.Value.ToString()));//, Convert.ToInt32(DDTipoPMP.SelectedValue.ToString()));

                        /*
            1	En Evaluador
            2	En Evaluado
            3	En Auditor
            4	Aprobado
            5	Sin Evaluación
            6	No concluida             
                         */

            switch (PasoActualID.ToString())
            {
                case "1":
                    return "En Evaluador";
                    break;
                case "2":
                    return "En Evaluado";
                    break;
                case "3":
                    return "En Auditor";
                    break;
                case "4":
                    return "Aprobado";
                    break;
                case "5":
                    return "Sin Evaluación";
                    break;
                case "6":
                    return "No concluida";
                    break;
                case "-1":
                    return "---";
                    break;
                default:
                    return " ";
                    break;
            }

            return " ";

        }

        protected void btnEnviarEvaluacion_Click(object sender, EventArgs e)
        {
            NotificacionesController n = new NotificacionesController();

            FormulariosDS.PeriodoDataTable PDT = new FormulariosDS.PeriodoDataTable();
            PDT = FachadaDA.Singleton.Periodo.GetDataByTipoPeriodoIDPeriodoFIscalID(int.Parse(DDTipoPMP.SelectedValue.ToString()), int.Parse(ddPeriodoUbicacion.SelectedValue.ToString()));
            string CargaHasta = PDT.Rows[0].ItemArray[5].ToString();
            DateTime FechaLImite = Convert.ToDateTime(CargaHasta);   


            GridView gvSendMail = new GridView();
            gvSendMail.DataSource = ODSGVUbicacion;
            gvSendMail.DataBind();

            string datos = "<br /> <strong>Legajo:&nbsp;</strong>" + GVUbicacion.Rows[0].Cells[0].Text.ToString() +
                           "<br /> <strong>Nombre Evaluado:&nbsp;</strong>" + GVUbicacion.Rows[0].Cells[1].Text.ToString() +
                           "<br /> <strong>Tipo:&nbsp;</strong>" + GVUbicacion.Rows[0].Cells[2].Text.ToString() +
                           "<br /> <strong>Fecha de Inicio PMP:&nbsp;</strong>" + GVUbicacion.Rows[0].Cells[3].Text.ToString() +
                           "<br /> <strong>Evaluador:&nbsp;</strong>" + GVUbicacion.Rows[0].Cells[4].Text.ToString() +
                           "<br /> <strong>Estado:&nbsp;</strong>" + getEstadoU() +
                           "<br /> <strong>Ubicación:&nbsp;</strong>" + getUbicacionU();
            if (DateTime.Compare(DateTime.Now, FechaLImite) <= 0)
                datos += "<br /> <strong>Finalización del proceso " + GVUbicacion.Rows[0].Cells[2].Text.ToString() + ": 15 de agosto de 2012</strong>";





            if (gvSendMail.Rows[0].Cells[6].Text != null || gvSendMail.Rows[0].Cells[6].Text != "")
            {
                string envio = n.envioUbicacionPMP(gvSendMail.Rows[0].Cells[6].Text, datos, GVUbicacion.Rows[0].Cells[2].Text);
                if (envio == "true")
                {
                    lblDatosUbicacion.Text = "Se enviaron los datos al evaluador<BR /><BR />";

                }
                else
                {
                    lblDatosUbicacion.Text = "Ocurrio un error durante el envio, intente nuevamente más tarde<BR /><BR />";
                }
            }
            else
            {
                lblDatosUbicacion.Text = "El Evaluador no tiene definido su e-mail en el sistema. <br />La notificacion no será enviada";


            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            tblegajoubicacion.Text = "";
            lblDatosUbicacion.Text = "";
            btnEnviarEvaluacion.Visible = false;
            btnActualizar.Visible = false;

        }

        protected void DDTipoPMP_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            

            FormulariosDS.PeriodoDataTable PDT = new FormulariosDS.PeriodoDataTable();
            PDT = FachadaDA.Singleton.Periodo.GetDataByTipoPeriodoIDPeriodoFIscalID(int.Parse(DDTipoPMP.SelectedValue.ToString()), int.Parse(ddPeriodoUbicacion.SelectedValue.ToString()));
            if (PDT.Rows.Count > 0)
                HFPeriodoSelect.Value = PDT.Rows[0].ItemArray[0].ToString();

        }


        #region OnDemand
        //MY
        protected void lnkAsignados_Click(object sender, EventArgs e)
        {
            GridViewAsignados.DataSourceID = "ODSAsignadosAbiertos";
            GridViewAsignados.DataBind();

        }
        protected void lnkHistorico_Click(object sender, EventArgs e)
        {
            GridViewHistoricos.DataSourceID = "ODSIniciadasHistoricos";
            GridViewHistoricos.DataBind();
        }

        //FY

        protected void lnkAsignadosFY_Click(object sender, EventArgs e)
        {
            FULLAsignados.DataSourceID = "ODSFULLAsignadosAbiertos";
            FULLAsignados.DataBind();
        }

        protected void lnkHistoricoFY_Click(object sender, EventArgs e)
        {
            GridViewHistoricosFY.DataSourceID = "ODSFULLAsignadosHistoricos";
            GridViewHistoricosFY.DataBind();
        }


        #endregion


        #region LoadTabReportes

        protected void CargarTabReportes()
        {
            DropDownListPeriodoFiscalRS.DataSourceID = "ObjectDataSourcePeriodoFiscalRS";
            DropDownListPeriodoFiscalRS.DataBind();

            DropDownListAreaRS.DataSourceID = "ObjectDataSourceAreaRS";
            DropDownListAreaRS.DataBind();

            ddlPeriodo.DataSourceID = "ObjectDataSourcePeriodosCalibracion";
            ddlPeriodo.DataBind();

            FormView1.DataSourceID = "ObjectDataSourcePeriodo";
            FormView1.DataBind();

            ListBoxArea.DataSourceID = "ObjectDataSourceAreaByTipoFormulario";
            ListBoxArea.DataBind();

            ListBoxCargos.DataSourceID = "ObjectDataSourceCargoByTipoFormulario";
            ListBoxCargos.DataBind();

            ListBoxEstado.DataSourceID = "ObjectDataSourceEstados";
            ListBoxEstado.DataBind();

            ListBoxFormularios.DataSourceID = "ObjectDataSourceFormularios";
            ListBoxFormularios.DataBind();

            ListBoxSector.DataSourceID = "ObjectDataSourceSectorByTipoFormulario";
            ListBoxSector.DataBind();

            ddPeriodoUbicacion.DataSourceID = "ODSPeriodoFIscal";
            ddPeriodoUbicacion.DataBind();

            GVUbicacion.DataSourceID = "ODSGVUbicacion";
            GVUbicacion.DataBind();

        }

        #endregion


        #region Charts
        private void drawChart(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt)
        {
            pnlCharts.Visible = true;


            drawChartOperariosGral(dt);

            drawChartDireccionbyEstado(dt, "cantNoIniciadas", ChartDireccionNoIniciadas);
            drawChartDireccionbyEstadoIniciadas(dt, ChartDireccionIniciadas);
            drawChartDireccionbyEstadoIniciadasDetalle(dt, CharDireccionIniciadasdetalle);
            drawChartDireccionbyEstado(dt, "cantaprobadas", ChartDireccionAprobadas);
        }





        private void drawChart(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt, string direccionarea)
        {
            pnlCharts.Visible = true;
            drawChartOperariosGral(dt);

            drawChartDireccionbyEstado(dt, "cantNoIniciadas", ChartDireccionNoIniciadas, direccionarea);


            drawChartDireccionbyEstadoIniciadas(dt, ChartDireccionIniciadas, direccionarea);
            drawChartDireccionbyEstadoIniciadasDetalle(dt, CharDireccionIniciadasdetalle, direccionarea);

            drawChartDireccionbyEstado(dt, "cantaprobadas", ChartDireccionAprobadas, direccionarea);

        }

        private void drawChartOperariosGral(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("Estado");
            dtaux.Columns.Add("Cantidad");

            addRow("canttotal", "Total", dt, dtaux);

            addRow("cantnoiniciadas", "No Iniciadas", dt, dtaux);


            // agrupa las en evaluador, evaluado y auditor
            int cantidad = int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenevaluador"].ToString()) + int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenevaluado"].ToString()) + int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenauditor"].ToString());
            string estado = "Iniciadas";

            DataRow dr = dtaux.NewRow();
            dr["cantidad"] = cantidad;
            dr["estado"] = estado;
            dtaux.Rows.Add(dr);



            addRow("cantaprobadas", "Aprobadas", dt, dtaux);

            ChartOperariosGral.Visible = true;
            ChartOperariosGral.DataSource = dtaux;
            ChartOperariosGral.DataBind();

            formatChartPie(ChartOperariosGral);


            formatChart(ChartOperariosGral);
        }

        private void formatChartPie(Chart chart)
        {
            chart.Series[0]["PieLabelStyle"] = "Outside";

            chart.Series[0].BorderWidth = 1;
            chart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);



            chart.Legends.Add("LegendText");
            chart.Legends[0].Enabled = true;
            chart.Legends[0].Docking = Docking.Bottom;
            chart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

            // Set the legend to display pie chart values as percentages
            // Again, the P2 indicates a precision of 2 decimals
            chart.Series[0].LegendText = "#VALX" + " " + "#VALY";


            // By sorting the data points, they show up in proper ascending order in the legend
            chart.DataManipulator.Sort(PointSortOrder.Descending, ChartOperariosGral.Series[0]);
        }

        private void drawChartOperariosGral(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("Estado");
            dtaux.Columns.Add("Cantidad");

            addRow("cantidadtotal", "Total", dt, dtaux);

            addRow("cantnoiniciadas", "No Iniciadas", dt, dtaux);


            // agrupa las en evaluador, evaluado y auditor
            int cantidad = int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenlt"].ToString()) + int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenevaluado"].ToString()) + int.Parse(dt.Rows[dt.Rows.Count - 1]["cantenjp"].ToString());
            string estado = "Iniciadas";

            DataRow dr = dtaux.NewRow();
            dr["cantidad"] = cantidad;
            dr["estado"] = estado;
            dtaux.Rows.Add(dr);



            addRow("cantaprobadas", "Aprobadas", dt, dtaux);

            ChartOperariosGral.Visible = true;
            ChartOperariosGral.DataSource = dtaux;
            ChartOperariosGral.DataBind();

            formatChart(ChartOperariosGral);
            formatChartPie(ChartOperariosGral);

        }



        public void addRow(string rowname, string estadostring, DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt, DataTable dtaux)
        {
            int cantidad = int.Parse(dt.Rows[dt.Rows.Count - 1][rowname].ToString());
            string estado = estadostring;

            DataRow dr = dtaux.NewRow();
            dr["cantidad"] = cantidad;
            dr["estado"] = estado;
            dtaux.Rows.Add(dr);

        }

        public void addRow(string rowname, string estadostring, DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt, DataTable dtaux)
        {
            int cantidad = int.Parse(dt.Rows[dt.Rows.Count - 1][rowname].ToString());
            string estado = estadostring;

            DataRow dr = dtaux.NewRow();
            dr["cantidad"] = cantidad;
            dr["estado"] = estado;
            dtaux.Rows.Add(dr);

        }



        private void drawChartDireccionbyEstado(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt, string estado, Chart chart, string direccionarea)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("direccionarea");
            dtaux.Columns.Add("Cantidad");
            dtaux.Columns.Add("total");
            DataView dv;

            if (direccionarea == "area")
                dv = new DataView(dt, "  areaid is null  AND direccion <>'' ", "", DataViewRowState.CurrentRows);///para q traiga solo las direcciones
            else
                dv = new DataView(dt, " " + direccionarea + " <> '' ", "", DataViewRowState.CurrentRows);


            foreach (DataRow dr in dv.ToTable().Rows)
            {

                DataRow drnew = dtaux.NewRow();
                drnew["direccionarea"] = dr["direccion"].ToString();
                drnew["Cantidad"] = dr[estado];
                drnew["total"] = dr["canttotal"];
                dtaux.Rows.Add(drnew);

            }


            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new Color[] { Color.Red, Color.Blue };

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();


            formatChart(chart);
        }

        private static void formatChart(Chart chart)
        {

            chart.ChartAreas["ChartArea1"].BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
            chart.ChartAreas["ChartArea1"].BackSecondaryColor = System.Drawing.ColorTranslator.FromHtml("#ddddee");
            chart.ChartAreas["ChartArea1"].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;



            chart.Series[0].BackGradientStyle = GradientStyle.TopBottom;
            chart.Series[0].Color = System.Drawing.ColorTranslator.FromHtml("#0066cc");
            chart.Series[0].BackSecondaryColor = System.Drawing.ColorTranslator.FromHtml("#00aadd");


           
        }

        private void drawChartDireccionbyEstado(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt, string estado, Chart chart)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("direccionarea");
            dtaux.Columns.Add("Cantidad");
            dtaux.Columns.Add("total");

            DataView dv = new DataView(dt, " direccionevaluador  <> '' ", "", DataViewRowState.CurrentRows);

            DataTable desiredResult = GroupBy("evaluador", estado, dv.ToTable());




            foreach (DataRow dr in desiredResult.Rows)
            {

                DataRow drnew = dtaux.NewRow();
                drnew["direccionarea"] = dr["evaluador"].ToString();
                drnew["Cantidad"] = dr["count"];
                drnew["total"] = dr["cantidadtotal"];
                dtaux.Rows.Add(drnew);

            }


            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new Color[] { Color.Red, Color.Blue };

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();





        }


        public DataTable GroupBy(string i_sGroupByColumn, string i_sAggregateColumn, DataTable i_dSourceTable)
        {

            DataView dv = new DataView(i_dSourceTable);

            //getting distinct values for group column
            DataTable dtGroup = dv.ToTable(true, new string[] { i_sGroupByColumn });

            //adding column for the row count
            dtGroup.Columns.Add("Count", typeof(int));
            dtGroup.Columns.Add("cantidadtotal", typeof(int));

            //looping thru distinct values for the group, counting
            foreach (DataRow dr in dtGroup.Rows)
            {
                dr["Count"] = i_dSourceTable.Compute("Sum(" + i_sAggregateColumn + ")", i_sGroupByColumn + " = '" + dr[i_sGroupByColumn] + "'");
                dr["cantidadtotal"] = i_dSourceTable.Compute(" sum(canttotalevaluador) ", i_sGroupByColumn + " = '" + dr[i_sGroupByColumn] + "'");
            }

            //returning grouped/counted result
            return dtGroup;
        }


        private void drawChartDireccionbyEstadoIniciadas(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt, Chart chart, string direccionarea)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("direccionarea");
            dtaux.Columns.Add("Cantidad");
            dtaux.Columns.Add("total");

            DataView dv;

            if (direccionarea == "area")
                dv = new DataView(dt, "  areaid is null  AND direccion <>'' ", "", DataViewRowState.CurrentRows);///para q traiga solo las direcciones
            else
                dv = new DataView(dt, " " + direccionarea + " <> '' ", "", DataViewRowState.CurrentRows);


            foreach (DataRow dr in dv.ToTable().Rows)
            {
                DataRow drnew = dtaux.NewRow();
                drnew["direccionarea"] = dr["direccion"];
                drnew["Cantidad"] = int.Parse(dr["cantenevaluador"].ToString()) + int.Parse(dr["cantenevaluado"].ToString()) + int.Parse(dr["cantenauditor"].ToString());
                drnew["total"] = dr["canttotal"];
                dtaux.Rows.Add(drnew);

            }



            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new Color[] { Color.Red, Color.Blue };

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();

            formatChart(chart);
        }

        private void drawChartDireccionbyEstadoIniciadas(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt, Chart chart)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("direccionarea");
            dtaux.Columns.Add("Cantidad");
            dtaux.Columns.Add("total");



            DataView dv = new DataView(dt, " evaluador  <> '' ", "", DataViewRowState.CurrentRows);



            foreach (DataRow dr in dv.ToTable().Rows)
            {
                DataRow drnew = dtaux.NewRow();
                drnew["direccionarea"] = dr["evaluador"];
                drnew["Cantidad"] = int.Parse(dr["cantenlt"].ToString()) + int.Parse(dr["cantenevaluado"].ToString()) + int.Parse(dr["cantenjp"].ToString());
                drnew["total"] = dr["canttotalevaluador"];
                dtaux.Rows.Add(drnew);

            }



            chart.Palette = ChartColorPalette.None;
            chart.PaletteCustomColors = new Color[] { Color.Red, Color.Blue };

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();

            formatChart(chart);
        }



        private void drawChartDireccionbyEstadoIniciadasDetalle(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoAreaDireccionDataTable dt, Chart chart, string direccionarea)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("Estado");
            dtaux.Columns.Add("Cantidad");

            DataView dv;

            if (direccionarea == "area")
                dv = new DataView(dt, "  areaid is null  AND direccion <>'' ", "", DataViewRowState.CurrentRows);///para q traiga solo las direcciones
            else
                dv = new DataView(dt, " " + direccionarea + " <> '' ", "", DataViewRowState.CurrentRows);


            int cantenevaluador = 0;
            int cantenevaluado = 0;
            int cantenauditor = 0;

            foreach (DataRow dr in dv.ToTable().Rows)
            {

                cantenevaluador += int.Parse(dr["cantenevaluador"].ToString());
                cantenevaluado += int.Parse(dr["cantenevaluado"].ToString());
                cantenauditor += int.Parse(dr["cantenauditor"].ToString());
            }

            DataRow drnew = dtaux.NewRow();
            drnew["estado"] = "En Evaluador";
            drnew["Cantidad"] = cantenevaluador;
            dtaux.Rows.Add(drnew);



            DataRow drnew2 = dtaux.NewRow();
            drnew2["estado"] = "En Evaluado";
            drnew2["Cantidad"] = cantenevaluador;
            dtaux.Rows.Add(drnew2);


            DataRow drnew3 = dtaux.NewRow();
            drnew3["estado"] = "En Auditor";
            drnew3["Cantidad"] = cantenauditor;
            dtaux.Rows.Add(drnew3);

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();

            formatChart(chart);
            formatChartPie(chart);

        }


        private void drawChartDireccionbyEstadoIniciadasDetalle(DSReportesDeSeguimiento.Eval_GetReporteSeguimientoAgrupadoPorEvaluadorDireccionDataTable dt, Chart chart)
        {
            DataTable dtaux = new DataTable();

            dtaux.Columns.Add("Estado");
            dtaux.Columns.Add("Cantidad");

            DataView dv = new DataView(dt, " evaluador  <>  '' ", "", DataViewRowState.CurrentRows);


            int cantenevaluador = 0;
            int cantenevaluado = 0;
            int cantenauditor = 0;

            foreach (DataRow dr in dv.ToTable().Rows)
            {

                cantenevaluador += int.Parse(dr["cantenjp"].ToString());
                cantenevaluado += int.Parse(dr["cantenevaluado"].ToString());
                cantenauditor += int.Parse(dr["cantenjp"].ToString());
            }

            DataRow drnew = dtaux.NewRow();
            drnew["estado"] = "En Evaluador";
            drnew["Cantidad"] = cantenevaluador;
            dtaux.Rows.Add(drnew);



            DataRow drnew2 = dtaux.NewRow();
            drnew2["estado"] = "En Evaluado";
            drnew2["Cantidad"] = cantenevaluador;
            dtaux.Rows.Add(drnew2);


            DataRow drnew3 = dtaux.NewRow();
            drnew3["estado"] = "En Auditor";
            drnew3["Cantidad"] = cantenauditor;
            dtaux.Rows.Add(drnew3);

            chart.Visible = true;
            chart.DataSource = dtaux;
            chart.DataBind();

            formatChart(chart);
            formatChartPie(chart);
        }

        #endregion
    }
}
