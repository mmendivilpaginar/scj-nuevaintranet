﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="cumpleanios.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.cumpleanios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <asp:HiddenField ID="hdMes" runat="server" />
    <asp:HiddenField ID="hdClusterID" runat="server" />

    <h2>Cumplea&ntilde;os</h2>


<div class="form-item">
   <label>Mes:</label>
            <asp:DropDownList ID="drpMes" runat="server" AutoPostBack="True" 
                onselectedindexchanged="drpMEs_SelectedIndexChanged">
                <asp:ListItem Value="1">Enero</asp:ListItem>
                <asp:ListItem Value="2">Febrero</asp:ListItem>
                <asp:ListItem Value="3">Marzo</asp:ListItem>
                <asp:ListItem Value="4">Abril</asp:ListItem>
                <asp:ListItem Value="5">Mayo</asp:ListItem>
                <asp:ListItem Value="6">Junio</asp:ListItem>
                <asp:ListItem Value="7">Julio</asp:ListItem>
                <asp:ListItem Value="8">Agosto</asp:ListItem>
                <asp:ListItem Value="9">Septiembre</asp:ListItem>
                <asp:ListItem Value="10">Octubre</asp:ListItem>
                <asp:ListItem Value="11">Noviembre</asp:ListItem>
                <asp:ListItem Value="12">Diciembre</asp:ListItem>
            </asp:DropDownList>
   
</div>


<div class="form-item">    
    <label>Pa&iacute;s:</label>
            <asp:DropDownList ID="drpCluster" runat="server" AppendDataBoundItems="True" 
                AutoPostBack="True" DataSourceID="odsCluster" DataTextField="Descripcion" 
                DataValueField="ID" 
                onselectedindexchanged="drpCluster_SelectedIndexChanged">
                <asp:ListItem Value="0">Todos</asp:ListItem>
    </asp:DropDownList>
</div>


    <asp:ObjectDataSource ID="odsCluster" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getCluster" 
        TypeName="com.paginar.johnson.BL.ControllerCluster"></asp:ObjectDataSource>

<h3>Cumplea&ntilde;os del mes</h3>
    <span style="padding-left: 390px">
    
    <asp:ImageButton ID="imgExportar" runat="server"   ImageUrl="~/images/exportword.png" OnClick="imgExportar_Click"  Width="32px" ToolTip="Exportar a Word"/>
        &nbsp
    <asp:ImageButton ID="imgVerCalendario" runat="server"   ImageUrl="~/images/print_printer.png"  Width="25px" ToolTip="Ver listado en formato Calendario" />
<%--            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/printer.png" />
            
                <a href="imprimirCumpleanios.aspx"
                title="Impresion" target="_blank">Imprimir Cumpleaños
        </a>--%>

               
<%--                <asp:HyperLink ID="HyperLinkImprimirCumple" runat="server"  ImageUrl="~/images/print_printer.png" ToolTip="Ver listado en formato Calendario"  Width="32px"
         Target="_blank">
                

                </asp:HyperLink>--%>

                </span>
                <br /> <br />
   
    
    <asp:Panel ID="export" runat="server" Visible="false" >

         <div id="logo" runat="server">
          
             <asp:Image runat="server" ID="imglogo" ImageUrl="../images/logocumpleanios.png"  />
         </div>
     
         
    </asp:panel>

    <asp:GridView ID="gvCunpleanios" runat="server" AutoGenerateColumns="False"  onrowdatabound="grid_RowDataBound" CssClass="cumpleanios" Width="800px">
        <Columns>
            <asp:BoundField DataField="dia" >
        
            </asp:BoundField>
            <asp:TemplateField  >

                <ItemTemplate>
                <asp:Label ID="lblnombreapellido" runat="server" Text='<%# Eval("apellido")+", "+Eval("nombre") %>'></asp:Label>

                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                <asp:Panel ID="pnlUsuario" runat="server">
                                
                        <div class="userCard">
                                    <asp:HiddenField ID="hdUsuario" runat="server"  Value='<%# Bind("usuarioid") %>'/>
                                    <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                        <ItemTemplate>
                                             <div class="foto">                    
                                                <%--<img src=<%# "../images/personas/"+ Eval("usuariofoto")%>   />--%>
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                            </div>
                                            <div class="info">
                                                <ul>
                                                    <li> <%# Eval("nombre")%>   <%# Eval("apellido")%></li>
                                                    <li><%# Eval("UBICACIONDET")%>  </li>
                                                    <li><%# Eval("DireccionDET")%>  </li>                                   
                                                    <li> <%# Eval("areadesc")%>  </li>
                                                <%--    <li> <%# Eval("cargodet")%> </li>--%>
                                                    <li>Interno:  <%# Eval("interno")%> </li>
                                                </ul>                                                
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsUsuario" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="getDataUsuario" 
                                        TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" 
                                                PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>

                    </asp:Panel>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ubicaciondescripcion" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblEmptyCumple" runat="server" 
                Text="La busqueda realizada no produjo resultados"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>

</asp:Content>
