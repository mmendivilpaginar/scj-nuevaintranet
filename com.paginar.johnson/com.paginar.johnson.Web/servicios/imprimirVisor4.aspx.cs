﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.servicios
{
    public partial class imprimirVisor4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((Request["Legajo"].ToString() != null) && (Request["Mes"].ToString() != null) && (Request["Anio"].ToString() != null))
                {
                    int Legajo = int.Parse(Request["Legajo"].ToString());
                    int Mes = int.Parse(Request["Mes"].ToString());
                    int Anio = int.Parse(Request["Anio"].ToString());

                    lblGenerado.Text = "Generado: " + DateTime.Today.ToShortDateString();

                    string MesLiquidado = DarMes(Mes);
                    lblMesLiquidado.Text = "Mes cierre de liquidaci&oacute;n: " + MesLiquidado;

                    Visor4_BL ControllerVisor = new Visor4_BL();
                    string value = ControllerVisor.GetRetencionesInText(ControllerVisor.GetRetenciones(Legajo, Mes, Anio));

                    if (value.Trim() != "")
                    {
                        this.TextoFormateado.InnerText = value;
                    }
                    else 
                    {
                        this.TextoFormateado.InnerText ="No existen resultados para la búsqueda.";
                        //btnImprimir.Enabled = false;
                    }
                }
            }
        }

        private string DarMes(int nro_mes)
        {
            string Mes;
            switch (nro_mes)
            {
                case 1: Mes = "Enero";
                    break;
                case 2: Mes = "Febrero";
                    break;
                case 3: Mes = "Marzo";
                    break;
                case 4: Mes = "Abril";
                    break;
                case 5: Mes = "Mayo";
                    break;
                case 6: Mes = "Junio";
                    break;
                case 7: Mes = "Julio";
                    break;
                case 8: Mes = "Agosto";
                    break;
                case 9: Mes = "Setiembre";
                    break;
                case 10: Mes = "Octubre";
                    break;
                case 11: Mes = "Noviembre";
                    break;
                case 12: Mes = "Diciembre";
                    break;
                default: Mes = "No especificado";
                    break;
            }
            return Mes;
        }
    }
}