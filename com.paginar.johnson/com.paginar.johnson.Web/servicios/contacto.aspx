﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="contacto.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.contacto" %>
<%@ MasterType VirtualPath="~/MasterPages/TwoSidebars.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<script type="text/javascript" language="javascript">
    function redireccionaX() {
        setTimeout(transfer, 3000);

    }
    function transfer() {
        window.location = "/";
    }
</script>
    <div class="box">
    <h2>Contáctenos</h2>
    </div>
    <asp:ValidationSummary ID="sumario" runat="server" 
        CssClass="messages msg-error" ValidationGroup="Valida" />
    <asp:MultiView ID="MVContacto" runat="server">
    
        <asp:View ID="VContacto" runat="server">

        <div class="box formulario">
            <div class="form-item leftthird">
                <asp:Label ID="lblDe" runat="server" Text="De:"></asp:Label>
            </div>
            <div class="form-item">   
                <asp:Label ID="lblRemitente" runat="server" Text=""></asp:Label>
            </div>
            <div class="form-item leftthird">
                <asp:Label ID="lblPara" runat="server" Text="Para:"></asp:Label>
            </div>
            <div class="form-item">   
                <asp:Label ID="lblDestinatario" runat="server" Text="Área de Comunicaciones"></asp:Label>
            </div>
            <div class="form-item leftthird">
                <asp:Label ID="lblComentario" runat="server" Text="Comentario:"></asp:Label>
            </div>
            <div class="form-item">   
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    
                    ErrorMessage="El campo comentario no puede estar vacio, por favor ingrese texto." 
                    ControlToValidate="tbMEnsaje" ValidationGroup="Valida">*</asp:RequiredFieldValidator>
                <br />
                <asp:TextBox ID="tbMEnsaje" runat="server" Rows="5" TextMode="MultiLine" onkeyDown="checkTextAreaMaxLength(this,event,'350');" ></asp:TextBox>
                <asp:Label ID="lbllimitacion" runat="server" Text="Máximo de Caracteres 350"></asp:Label>
            </div>
          <div class="form-item leftthird">
            <asp:Button ID="btnEnviar" runat="server" Text="Enviar" 
                  onclick="btnEnviar_Click"
                  ValidationGroup="Valida" />&nbsp&nbsp<asp:Button ID="btnCancelar" 
                  runat="server" Text="Cancelar" onclick="btnCancelar_Click" 
                  onclientclick="redireccionaX()" />
          </div>
            <div class="form-item"> 
            </div>
        </div>           
     
        </asp:View>
        <asp:View ID="VOk" runat="server">
            <span lang="ES-TRAD" class="messages msg-exito">La operación fue realizada exitosamente.</span>
        </asp:View>
        <asp:View ID="VCancel" runat="server">
            <span lang="ES-TRAD" class="messages msg-error">Operación Cancelada por el 
            Usuario.</span>
        </asp:View>
        <asp:View ID="Verror" runat="server">
            <span lang="ES-TRAD" class="messages msg-error">Ha ocurrido un error al intentar enviar el mail, intentelo nuevamente más tarde.</span>
        </asp:View>

    </asp:MultiView>
</asp:Content>
