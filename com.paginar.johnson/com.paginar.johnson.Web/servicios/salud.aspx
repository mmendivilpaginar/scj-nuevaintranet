﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="salud.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.salud" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<%@ Register Src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script src="../js/init.js" type="text/javascript"></script>
    <div id="nobg">
    </div>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="Salud" ID="Panel1" runat="server">
            <ContentTemplate>
                <h5>
                    Auditor de la Obra Social Ceras Johnson<br />
                    Proveedor: Servicio para el Sistema de Salud, del Dr. Ángel R. D&#8217;Amico</h5>
                <br />

                
                <p>
                    Recordamos: si te sentís enfermo y estás en planta, pasá por consultorio médico
                    y regresá a trabajar sólo cuando el médico de planta te dé el alta.(auque tengas
                    un alta particular, el alta que te da la autorización para ingresar a tus tareas
                    nuevamente es el alta del médico de planta).
                </p>
                <table widh="100%" cellspacing="4">
                    <tr>
                        <td valign="top">
                            <img src="../images/obra-social.jpg" class="floatLeft" width="140" />
                        </td>
                        <td width="50%" valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <img src="/images/logo_Medicus.gif" align="left" width="60">
                                    </td>
                                    <td>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;MEDICUS</p>
                                        <p>
                                            &nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.medicus.com.ar/cartillas.php" target="_blank"
                                                class="link">B&uacute;squeda en Cartilla &gt;&gt; </a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                &nbsp;
                <asp:TreeView ID="TreeViewSalud" runat="server" DataSourceID="RelationalSystemDataSource1"
                    OnSelectedNodeChanged="TreeViewSalud_SelectedNodeChanged">
                    <DataBindings>
                        <asp:TreeNodeBinding TextField="Name" />
                    </DataBindings>
                </asp:TreeView>
                <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
                <br />
                <a name="contenido-abajo"></a>
                <asp:UpdatePanel ID="updContenido" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
                        <div id="divContenido">
                            <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                                <ItemTemplate>
                                    <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:FormView>
                            <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <br />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="TreeViewSalud" EventName="SelectedNodeChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Noticias" ID="Panel2" runat="server">
            <ContentTemplate>
                <table width="100%" cellpadding="1" cellspacing="0">
                    <tr>
                        <td>
                            <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="50" TagID="3" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
</asp:Content>
