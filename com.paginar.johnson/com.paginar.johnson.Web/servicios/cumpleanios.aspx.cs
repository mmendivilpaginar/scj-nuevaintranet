﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.servicios
{
    public partial class cumpleanios : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DateTime now= DateTime.Now;               
                int? mes=now.Month;
                drpMes.SelectedValue = mes.ToString();
                int? cluster = 0; //TRAE TODOS POR DEFAULT //this.ObjectUsuario.clusteridactual;
                drpCluster.SelectedValue = cluster.ToString();
            
                ControllerUCHome ch= new ControllerUCHome();
                gvCunpleanios.DataSource= ch.getCumpleanioMes(mes,cluster);
                gvCunpleanios.DataBind();
                
            }
            imgVerCalendario.Attributes.Add("onclick", " window.open('ImprimirCumpleanios.aspx?mes=" + drpMes.SelectedValue + "&cluster=" + drpCluster.SelectedValue+"')" );    
            //HyperLinkImprimirCumple.NavigateUrl = "ImprimirCumpleanios.aspx?mes=" + drpMes.SelectedValue + "&cluster=" + drpCluster.SelectedValue;
        }

        protected void Buscar()
        {
            int? mes=null;

            mes = int.Parse(drpMes.SelectedValue);

            int? cluster = null;

            if(drpCluster.SelectedIndex>0)  cluster = int.Parse(drpCluster.SelectedValue);

            ControllerUCHome ch= new ControllerUCHome();
            gvCunpleanios.DataSource= ch.getCumpleanioMes(mes,cluster);
            gvCunpleanios.DataBind();

        }

        protected void drpMEs_SelectedIndexChanged(object sender, EventArgs e)
        {
            Buscar();
        }

        protected void drpCluster_SelectedIndexChanged(object sender, EventArgs e)
        {
            Buscar();
        }


        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblnombreapellido = (Label)e.Row.FindControl("lblnombreapellido");
                Panel pnl = (Panel)e.Row.FindControl("pnlUsuario");
                pnl.Attributes.Add("style", "display:none");
                Literal ltjs = (Literal)e.Row.FindControl("ltjs");
                ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
            }
        }


        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

        protected void imgExportar_Click(object sender, ImageClickEventArgs e)
        {
           StringBuilder sb = new StringBuilder();
           StringWriter sw = new StringWriter(sb);
           HtmlTextWriter htw = new HtmlTextWriter(sw);


           Page page = new Page();
           HtmlForm form = new HtmlForm();


            gvCunpleanios.AllowPaging = false; 
            gvCunpleanios.DataBind();

         

            GridView gvaux1 = new GridView();
            gvaux1.Width = 200;
            gvaux1.AutoGenerateColumns = false;
            gvaux1.ShowHeader = false;
            gvaux1.CssClass = "";
            BoundField bfield = new BoundField();
            bfield.HeaderText = "";
            bfield.DataField = "Dia";
            gvaux1.Columns.Add(bfield);

            BoundField bfield2 = new BoundField();
            bfield2.HeaderText = "";
            bfield2.DataField = "ApNom";
            gvaux1.Columns.Add(bfield2);


   

            GridView gvaux2 = new GridView();
            gvaux2.Width = 200;
            gvaux2.AutoGenerateColumns = false;
            gvaux2.ShowHeader = false;
            gvaux2.CssClass = "";
            BoundField bfield21 = new BoundField();
            bfield21.HeaderText = "";
            bfield21.DataField = "Dia";
            gvaux2.Columns.Add(bfield21);

            BoundField bfield22 = new BoundField();
            bfield22.HeaderText = "";
            bfield22.DataField = "ApNom";
            gvaux2.Columns.Add(bfield22);


        


            GridView gvaux3 = new GridView();           
            gvaux3.Width = 200;
            gvaux3.AutoGenerateColumns = false;
            gvaux3.ShowHeader = false;
            gvaux3.CssClass = "";
            BoundField bfield31 = new BoundField();
            bfield31.HeaderText = "";
            bfield31.DataField = "Dia";
            gvaux3.Columns.Add(bfield31);

            BoundField bfield32 = new BoundField();
            bfield32.HeaderText = "";
            bfield32.DataField = "ApNom";
            gvaux3.Columns.Add(bfield32);




            int? mes = null;

            mes = int.Parse(drpMes.SelectedValue);

            int? cluster = null;

            if (drpCluster.SelectedIndex > 0) cluster = int.Parse(drpCluster.SelectedValue);

            ControllerUCHome ch = new ControllerUCHome();


            DataView dv1 = new DataView(ch.getCumpleanioMes(mes, cluster), " dia >=1 and dia <= 10 ", "", DataViewRowState.CurrentRows);
            DataView dv2 = new DataView(ch.getCumpleanioMes(mes, cluster), " dia >10 and dia <= 20 ", "", DataViewRowState.CurrentRows);
            DataView dv3 = new DataView(ch.getCumpleanioMes(mes, cluster), " dia >=21 and dia <= 31 ", "", DataViewRowState.CurrentRows);


            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Dia");
            dt1.Columns.Add("ApNom");

            DataTable dt2 = new DataTable();
            dt2.Columns.Add("Dia");
            dt2.Columns.Add("ApNom");

            DataTable dt3 = new DataTable();
            dt3.Columns.Add("Dia");
            dt3.Columns.Add("ApNom");

            int i = 1;
            foreach (DataRow dr in ((DataTable)ch.getCumpleanioMes(mes, cluster)).Rows)
            {
                if (i <= 25)
                {
                    DataRow drnew = dt1.NewRow();
                    drnew["Dia"] = dr["dia"].ToString();
                    drnew["ApNom"] = dr["ApNom"].ToString();
                    dt1.Rows.Add(drnew);
                }

                if (i>25 && i <= 50)
                {
                    DataRow drnew = dt2.NewRow();
                    drnew["Dia"] = dr["dia"].ToString();
                    drnew["ApNom"] = dr["ApNom"].ToString();
                    dt2.Rows.Add(drnew);
                }

                 if (i > 50)
                {
                    DataRow drnew = dt3.NewRow();
                    drnew["Dia"] = dr["dia"].ToString();
                    drnew["ApNom"] = dr["ApNom"].ToString();
                    dt3.Rows.Add(drnew);
                }

                 i++;
            }


            gvaux1.DataSource = dt1;
            gvaux1.DataBind();

            gvaux2.DataSource = dt2;
            gvaux2.DataBind();

            gvaux3.DataSource = dt3;
            gvaux3.DataBind();



        
            gvaux1.EnableViewState = false;
            gvaux1.AllowPaging = false;

            export.Visible = true;
            imglogo.ImageUrl = this.ASP_SITE.Replace(":8080/", "") + "/images/logocumpleanios.png";

            //css
            FileInfo fi = new FileInfo(Server.MapPath("../css/styles.css"));
            System.Text.StringBuilder sbCSS = new System.Text.StringBuilder();
            sbCSS.Append("  body { 	line-height: 1.3;     color: #2A3C6D;     font-family: Arial; } ");
            sbCSS.Append(" h2 { font-family:'Calibri (Body)';   color: #558EC6;    font-size:18px } ");
   

            sbCSS.Append(" .AspNet-GridView {     margin-bottom:10px;     clear:both; } ");
            sbCSS.Append(" .AspNet-GridView table{     width:330px; }  ");
            sbCSS.Append(" .AspNet-GridView table th, .AspNet-GridView table td {    padding:0px;   border:1px solid #FFFFFF;     font-size:14px; } ");   
            sbCSS.Append(" .AspNet-GridView th{     background-color:#E6E6E6;    color:#666666;    text-transform:uppercase;    font-weight:normal;} ");




            Literal ltimage = new Literal();
        //    ltimage.Text = "<img  src='" + this.ASP_SITE + imgLogoaux.ImageUrl.Replace("~/", "") + "'  />";
            
          //  ltimage.RenderControl(htw);

            Literal ltdiv = new Literal();
            ltdiv.Text = "<div  > <h2> Cumplea&ntilde;os de " + drpMes.SelectedItem.Text + " de 2014  </h2> </div>";

            Literal ltbr = new Literal();
            ltbr.Text = "<br>";


            Literal ltdiv1 = new Literal();
            ltdiv1.Text = "<div style='width:800px' >";

            Literal ltdiv2 = new Literal();
            ltdiv2.Text = "</div >";

            export.RenderControl(htw);// logo
         //   ltbr.RenderControl(htw);//separados
            
            ltdiv.RenderControl(htw);//div titulo
         //   ltbr.RenderControl(htw);//separados

            ltdiv1.RenderControl(htw);

            Literal lt1 = new Literal();
            lt1.Text = "<table> <tr><td valign='top'>";
            lt1.RenderControl(htw);

            gvaux1.RenderControl(htw);

            Literal lt11 = new Literal();
            lt1.Text = "</td>";
            lt1.RenderControl(htw);



            if (dv2.Count > 0)
            {
                Literal lt2 = new Literal();
                lt2.Text = "<td  valign='top'>";
                lt2.RenderControl(htw);

                gvaux2.RenderControl(htw);

                Literal lt21 = new Literal();
                lt21.Text = "</td>";
                lt21.RenderControl(htw);
            }

            if (dv3.Count > 0)
            {
                Literal lt3 = new Literal();
                lt3.Text = "<td valign='top'>";
                lt3.RenderControl(htw);

                gvaux3.RenderControl(htw);

                Literal lt31 = new Literal();
                lt31.Text = "</td>";
                lt31.RenderControl(htw);

            }

            Literal lt4 = new Literal();
            lt4.Text = "</tr></table>";
            lt4.RenderControl(htw);


            ltdiv2.RenderControl(htw);
        
            Response.Clear();
            Response.Buffer = true;

           
            Response.AddHeader("content-disposition", "attachment;filename=cumpleanios.doc");
            Response.Charset = "UTF-8";

        

            Response.ContentType = "application/vnd.ms-word";



            Response.Write("<html>");
            Response.Write("<head>");
            Response.Write("<META HTTP-EQUIV='Content-Type' CONTENT='text/html; charset=UTF-8'>");
            Response.Write("<meta name=ProgId content=Word.Document>");
            Response.Write("<meta name=Generator content='Microsoft Word 9'>");
            Response.Write("<meta name=Originator content='Microsoft Word 9'>");
            Response.Write("<style>");
            Response.Write("@page Section1 {size:595.45pt 841.7pt; margin:0.5in 0.25in 0.5in 0.25in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}");
            Response.Write("div.Section1 {page:Section1;}");
            Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.25in 0.5in 0.25in 0.65in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}");
            Response.Write("div.Section2 {page:Section2;}");
            Response.Write("</style>");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.Write("<div class=Section2>");
            

           StringBuilder sb2 = new StringBuilder(sb.ToString().Replace("=", "=3D").Replace("FVC_", ""));

         
        //   Response.Output.Write(getHeaderMime() + "<style type=3D\"text/css\">" + sbCSS.ToString().Replace("=", "=3D") + "</style>" + sb2.ToString() + getFootMime(imagUserBase64));
           Response.Write("<style type=3D\"text/css\">" + sbCSS.ToString().Replace("=", "=3D") + "</style>" + sb.ToString() );



           Response.Write("</div>");
           Response.Write("</body>");
           Response.Write("</html>");

            Response.Flush();

            Response.End();



        }


        public static string getHeaderMime()
        {
            StringWriter headerMime = new StringWriter();

            headerMime.WriteLine("From: \"Guardado por Windows Internet Explorer 8\"");
            headerMime.WriteLine("Subject: ");
            headerMime.WriteLine("Date: Thu, 19 Jan 2012 17:37:25 -0300");
            headerMime.WriteLine("MIME-Version: 1.0");
            headerMime.WriteLine("Content-Type: multipart/related;");
            headerMime.WriteLine("	type=\"text/html\";");
            headerMime.WriteLine("	boundary=\"----=_NextPart_000_0000_01CCD6D1.01BB9000\"");
            headerMime.WriteLine("X-MimeOLE: Produced By Microsoft MimeOLE V6.1.7600.16543");
            headerMime.WriteLine("");
            headerMime.WriteLine("This is a multi-part message in MIME format.");
            headerMime.WriteLine("");
            headerMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            headerMime.WriteLine("Content-Type: text/html;");
            headerMime.WriteLine("	charset=\"Windows-1252\"");
            headerMime.WriteLine("Content-Transfer-Encoding: quoted-printable");
            headerMime.WriteLine("Content-Location: http://scj/web/Word.html");
            headerMime.WriteLine("");
            headerMime.WriteLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            headerMime.WriteLine("<HTML><HEAD>");
            headerMime.WriteLine("<META content=3D\"text/html; charset=3Dwindows-1252\" =");
            headerMime.WriteLine("http-equiv=3DContent-Type>");
            headerMime.WriteLine("<META name=3DGENERATOR content=3D\"MSHTML 8.00.7600.16722\"></HEAD>");
            headerMime.WriteLine("<BODY>");
            return headerMime.ToString(); ;
        }

        public string getFootMime(string imguser)
        {

            StringWriter FootMime = new StringWriter();

            FootMime.WriteLine("</BODY></HTML>");
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            FootMime.WriteLine("Content-Type: image/jpeg");
            FootMime.WriteLine("Content-Transfer-Encoding: base64");
            FootMime.WriteLine("Content-Location: http://Word/logoprint.jpg");
            FootMime.WriteLine("");
            FootMime.WriteLine("/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/b");
            FootMime.WriteLine("AIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgIC");
            FootMime.WriteLine("AwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD");
            FootMime.WriteLine("AwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgANACqAwERAAIRAQMRAf/EAIMAAQACAwEBAQAAAAAAAAAA");
            FootMime.WriteLine("AAAHCAYJCgULAgEBAAAAAAAAAAAAAAAAAAAAABAAAAYCAQEGAgUIBA8AAAAAAgMEBQYHAQgACRES");
            FootMime.WriteLine("ExSoKRVpISIWFxgxIzM0Nlc4GWMkVDlBYkNlZqa2N2coaHjYCjoRAQAAAAAAAAAAAAAAAAAAAAD/");
            FootMime.WriteLine("2gAMAwEAAhEDEQA/AO/jgOA4DgOA4DgOA4HiyQ8CaOv6kxec1ATsroeN0Tk5UKG0BSE8wS8hPjsy");
            FootMime.WriteLine("ecjwHxAgx9IhBxj/AA8Dhq0jcm+q9o6EKpVfq7trdMybtlHOLXhUsp28bL5w/LK+m7mxSzZiO3H8");
            FootMime.WriteLine("GqcloCqdCTDUGGEw4vDZ45qvJhY1K4LvdLlm6XkuxRVg7GTWQvHVHMulUZJjbTm96IrRT3OnsdcR");
            FootMime.WriteLine("FWXMVE4JIa6IhtpSEAxK0aoIcnHFqjSzSxlkBqo6bf8Afa1x/wBxV8f7P2pwPoUcDS/sz1Qp7RHU");
            FootMime.WriteLine("s190XaauiD9E7l+6j4nO3F4eU0hZvvDlr9HFvkG5MHLaf8OIZwmFeJn6wx5wL6MY4G6DgOA4DgOA");
            FootMime.WriteLine("4DgOA4DgOA4DgOA4DgclG1fVJ2b2836jegGk1in0jX59sG1HJbbj6JAfOZOpY1akix5S1u60J5zL");
            FootMime.WriteLine("GIkjZ3A9sA1mJFrkFLg0SnADwElBjPWf0830iVPvljss5rt01VgDZHo9MIlWxLow20/RxAtQMrLY");
            FootMime.WriteLine("OwchVNKRxvF5WLgI1TypMW+USuBnnCmwsBZ6wIbOulNqfvVUEWhj3uS8UpO21RHEa+NNcsjR0k2a");
            FootMime.WriteLine("qMZbcWGPMK22DWkr4iBtTZ8se3KlLkY2l90pKuAUT5bIcsmksoUwfq3p5oiTELVkQtTaKUJEarJg");
            FootMime.WriteLine("UytTH4FcTsQmUiKEE0JB5qTAR5DnAsBznsz28DMgbU73WBqzce8LnvVs60zKK7MVtWiCBRiz5TG6");
            FootMime.WriteLine("3Emm8Vmsvc1oYczOyKMoUqFQwJSEqBOhAiwTk3BgB94PdCSrsmNkblb+9L+UySfvVeWldVEasJXq");
            FootMime.WriteLine("0IEWna5GxShVYk+Y105jZJGEyJrfT1SMS4sBQQJ06gzsLBgsIQcD2Ip1Ad4mTXHqNwZz2jtiSOdL");
            FootMime.WriteLine("SmqMwKxHKRrwWGwqMX2lr19C1ykpT8XSNMnYsYwqQiONIAMPaV4ffO8UPMXbk72a6oOnfeYdzr3s");
            FootMime.WriteLine("gzYRqdJ/MK/nsrWPkJ8tG7qfa/NixjY4mrinJofou3gCoGcDKghUYM5OYWPBOSgz3Y7bDZ22N89+");
            FootMime.WriteLine("66m29tzasI6fBerbrbD41b7zRsBl8uriwWiL1VXzmd9pIhFmsE/hQlLkY9uCgkJxocH5UYKHgOQy");
            FootMime.WriteLine("PcLenqQ0908tNIxOLsXx2dW9N7+McLlqy2YNL5jP6xq/FSmV+cVcdMSyRoO+JzsJzRrzUjiS5qcN");
            FootMime.WriteLine("JAVghCEfk8M1hGyO3Op3UR2m0rV7a3xecDb6J2ITtciuCbvkskzNJodqPLL0i8zja97c3dbFHtnf");
            FootMime.WriteLine("mPCcPkDyijSjMiMAIwIBACkKDbDfUvRpZuEXvhsx9pGfbuPa8FQ1RPXZZHz2lzqCQWh9o1BytacY");
            FootMime.WriteLine("etJXsIUuUphRiY0g3ORfk7Mh0X/zE9iv7fF/7gj+Yn+zaH+Ir+3/AJP2X/zb+g4G/fgOA4DgOA4D");
            FootMime.WriteLine("gOA4Hzg66kz501OrIhkVstbolLo7YWTJJhnwDxr11cTP40wrJc1EFl5OciXiuJh8XQACHGVhRpYf");
            FootMime.WriteLine("q9/6A3tdXbrDSqPU/IaCq+kbRgDhcKZGfB9j3V6jwq8l1ZEuTU9pp3R0pgzzIEEx+0zaEgGB+bSm");
            FootMime.WriteLine("tJSzsUFYU48EAWY1v66LbYGsEkve3tcbHgzNV0MWp5pbA3CLIqcm9toWhR8Br6tlrq5t0okMtsF8");
            FootMime.WriteLine("TlgC1Nza4CYS1PirjgoyRKxBz0dFvXaf7Y7xP03yA8DJDYBdMpnkvUELTmtDJbVgUzr6LJz1XjeI");
            FootMime.WriteLine("e5LJPNMuIEwjcnKUjYqz25CAecBmhfT/AOpPB9Ybc0qM0nn8hWS7YyvLRR2cwSSMK4oEEHjExiJ6");
            FootMime.WriteLine("JKZ5ryDg2Pf2iTKSFw1iUKcsBmDywix2BC8iTpt7cVnvj0u3TFSyKVQegaw1gZLYsWOARrYdGJHH");
            FootMime.WriteLine("J3L5JOW/LllWWYrSxcT3gvKgBfdPKCEwOMd7u4CuDr0zN6WGLdTuMH66zRe42jKIAqrQxlNZHdBO");
            FootMime.WriteLine("0KLY5HNFC2OLkTqaQpJKiosLTAGZKOJB2gNAA4Aywhkd+dNvd6X629Lwcf16mjg8U7E5/CLSiQMt");
            FootMime.WriteLine("IJbBnQd9yGVti16ZTHIJuWV8jyjCpMqTiPBgGMYOwSMwoJgS/uXrJ1GHXZfd9NYGpSncmsbkjNoR");
            FootMime.WriteLine("3WuaJWasXU6nCpfNmyQVXMGCVLY2pnrW9VNDi1bFlryoQZEM4eQHDTYLMNCCLl6PO+BvTp1QZkNZ");
            FootMime.WriteLine("qpLZVcWxsJMZZT7U+MS+XxOJXcjqFNHBhCJ7w1LFCFXVRqhehQHHqEwnoGRl4ESryWE+QrSnd/ZP");
            FootMime.WriteLine("fTaHd+wtXZbQDC9UNf2GCDyqQszw9yCczjVaT0NHIZGxJSUSx8VOC97EtyeJIjTEABgsRuTMgAaF");
            FootMime.WriteLine("d2Xpnb0LulBNKexrpNklpoN9YncgIG4jZkEidK7Q0DI4Gsf2ZKpdAFOYU0kkBBQiSx5PyAJg8AyE");
            FootMime.WriteLine("sXA2gfhJ2S/c/L//AJpfwk/qyX+JL9z/AOt/tf8A0f6L+k4HSzwHAcBwHAcBwHAcCgO6vTO1O3zS");
            FootMime.WriteLine("I1N2wxcinjQ3ZaWC2IG4lRqxGht8Qw8ttG4monNnkLWmUHGGEJXZC4EJhmmCICUI0wQg1cGf+v8A");
            FootMime.WriteLine("S2PQF3p+tOoDPG2mHR6C9lVVbNDQa8IY2Lg5VD+LtUekUuZY4ySsZynOROrahQqRhyMIu3vdoQyN");
            FootMime.WriteLine("d0AUVsOEO/FZvBd11xeBtyNqicDh0NiFOQqMNiX6o2iIxROtnEXhjKqKLKLMIakKQzIAZ/O97uCA");
            FootMime.WriteLine("G6TW3V+jdSK1Q1PQUDbIJEEp+V60tMNQtd5E9mJ06VVIZO+rzVDo/PawlKWER55gu4WAJRWCygAL");
            FootMime.WriteLine("CE/cBwHAcBwHAcBwHAcBwHAiS+7dYaBpK2Ltk/dEx1VX0rna5PkzBZjhiNsytyTtKYQs4xla8KyC");
            FootMime.WriteLine("0pAfyjOOCHH054HNv0Ptz7TkuyEppG8dmHzYRbf9DsF/RDL7YjvPU9WzlokMgFK6hbCnd5dzYm64");
            FootMime.WriteLine("ibyBasay/CKTltIOwBfaHAw6KdqXh3j2sGx7+wOjixvzHQtwPDI9s65S2O7O7tleyJa2ujW5IjSF");
            FootMime.WriteLine("je4t6wgBxB5IwGlGgCIIsCxjPA0e9DyWTaZszTbFqz3qdzR+faOmq2STXZywCZVouu+H2ZH0+HCo");
            FootMime.WriteLine("F70+OMnxYCFsbAlgULuwBSQp7D3sB7mMhLfTI6ntibc7NXNW1lukBHB5zFpNdGqaCMBQp5Iy1bEb");
            FootMime.WriteLine("nnVZOEYscJCg1WGdqmpI0upaZUWnUmNn9fAX5dUDwwrRMer3spXV4dQOrZATD3EqKyKzam0nKJja");
            FootMime.WriteLine("VMvdbqi1nVfXrXEnPGD8YlapM23a1PJwDcgDlK1qMjyHAvpCe4z1EtknTpPaubeq3OIiuW2L/j9d");
            FootMime.WriteLine("y9aCKJQR86NOOyU6rFSUgYcH+AhWZirAnBg0Is5wdgRnZ2i7OBf/APETZP8ANN/Cb5po+6D8AH4i");
            FootMime.WriteLine("fJfCivj33k/iK+7XzXxzv+P8I+y/1PKd3ueN+c7e36OBRh83C3wRb7n9PpOjjYH+QXgzXBFbr+w6");
            FootMime.WriteLine("IbCl0QOjjy5yhvPbTFXljp8xSJtLYk7oYX4al3EYAQMAyXngbobema2uKmtCw21sy9OMDrubTNvZ");
            FootMime.WriteLine("wlGnidlsXjTm+JWwJCcwk87K89CErAADCMXf7MZxnszwNHnSx1/s/ZSsqX6htv7tbQy22ZzM5VLn");
            FootMime.WriteLine("SumK0fIa/lxpjl0kiAqxdKuRofhuG8eGoSk4hOajTpFWSggT/mRjUBG969SPduNH7jbVV870yh1b");
            FootMime.WriteLine("0d2yS6wyah3mLmrbBtVK0TSMQeay0U3E5lLY+5jdpKQNoClKwl8Ew3JxJ2UQ/MhOO8vUeufVbqW6");
            FootMime.WriteLine("70iUuiqLVh9qmr57ey92Y0QnKMN1hXfNqWUzE2UKFJRzSwsbocwmnhCEXdAEzPZnv54H76WXUH2M");
            FootMime.WriteLine("3Cd97F1ukR5rZanaajndIsrdGyGlc0Qm449bM7i4X9SA0Zj6aoh7MxGBMMwHOe0Yv8pngYJ0nepX");
            FootMime.WriteLine("sJtLYErZrolkBsWr2PWcu6ZlZ0Sq6RVygpO0Ec1IZnCkpK8KlCiNyhaZBzxP+VafAewooQS8i8M/");
            FootMime.WriteLine("AAzDpSdT6zdyrzuyurePhCdvkMTUXrre0RfDYB5YqkS2jMIC6RWeDQrFAxy5sAFlPLKOCBWJGaJQ");
            FootMime.WriteLine("YHATQ90KrPnWH2Zidu7f0/KcQ9qVRjbcuuNXpOfFEZSCQV/C9sGylrZhC3IloiJDMmmFShsXFDxg");
            FootMime.WriteLine("tWQUFQqGARYis4C6PUV3O3L1KvNlglWxVosWN7UwJprzVYAIymGdCNp8TqJsLo3Thy8x2OUUVwd8");
            FootMime.WriteLine("Pd0vjBxkasvJPYFOQecANmf2O2S/fLEf4dPsd/u7SfxJfvl/Wv2R/wBHf0X+PwLF8BwHAr5tLrXB");
            FootMime.WriteLine("NuqQl9AWa6TBqgk4MYRSIyDvCRiflZEekDZJkSELitangktEe6NBGTweDnJpYcg7cYFntCGTenNq");
            FootMime.WriteLine("uhuegb5gMFIp6e67FyJLFDahQRiDNcqbpOyp484tljpEEcMPlqULMFQQWIRxJ+ALlPeMFkYcgC3N");
            FootMime.WriteLine("hQhmsyAzit5GJaCPWBD5NCH0baeWlcQM0rZVrC6CQKTSVJSdaFCvHkoYizAgM7M5CLGOzIQNX2oN");
            FootMime.WriteLine("ZVfqV+DOHPM7basDX00rUh6+Otw7ASR+eGv5r4oSSHDGBAneQfaVThMpChxkj6gg474MC4ER1z0x");
            FootMime.WriteLine("dR6dnuuVm1FBA1fNtbY2/RZmfoGji0bcbUbpJEUUNdT73Wt8YLXWg6mIEg1AFao0tQWvVHnhFgQ8");
            FootMime.WriteLine("d0PNd+lzq0/2XG7YekU2dJZE9r5tuOyiWP7aY3kWxP0lcJ35AYk+AYEbCvMVQxqiUOR+MFSk7wlA");
            FootMime.WriteLine("w57mA9hs6bevTVqfVumqVbY2ajqKwG+yYopNkjWOYGSFtsl9tNOB2ecRwCFY3Zk0hUBEUBGSPKXA");
            FootMime.WriteLine("Ad/AsZHkPQvrp+1pfd/x/ZdTbWyVSWpH6yaqgC60LbqurSXiANU0eJ8GPyAbQ0Hurq3uEjeMmK04");
            FootMime.WriteLine("1WEp+EyftK75QR8CcHHW2vHPZqO7YqT5Hi0YxT7vSTanKckoYqKGvUoBLVhqppy3CWGvWHUvGAH4");
            FootMime.WriteLine("VBLCV9XJec/W4E9nEkqCTU6gos8g8sZJ5BwAmknEmhyAwo0seBAMLMALOBBzjOM4z2Z4GsSpukxr");
            FootMime.WriteLine("hRF0NtuU5O9jK5YGqWnThNr7G7hXEa7Dkx+BYGvVV8paVS5UWDv58NONyymKDgIAFhAAsIQ/FmdI");
            FootMime.WriteLine("vVC1ruklzyJdciBvn02jtlWlRsestY06/wBsWDFzsHtEwsGvAtpxru8lG5MEPJK5MSYI88WS+8ec");
            FootMime.WriteLine("IYSZsx04tdNsZzOLAtoM3Pe5/ru1azPBDE/Nrc2pIEx3G13m1OrQnVMLiejmKKds5AgrBGmE+VDk");
            FootMime.WriteLine("rJGc5yPgZpRej1H67SC45BWhUpQmXfX9FVpKmxe7olTQ1xbXirTagrxPGEpLQkNbVIYidnzozjFO");
            FootMime.WriteLine("FCrGDAhLx2gyHux/UWtItqRnTBheJ021R91z3UOHZO+N4Z6XEJEkcG92wVIPgmUJTopQuh5WFAUW");
            FootMime.WriteLine("O4EfaEOBYwLARXWfTT1MpazqJt2noETVszoeCvleolcCRRiNFWkxv0ZRxhUsu3LdGyVtiyMkpHha");
            FootMime.WriteLine("ByPOJWCcTBnmjM7cBwGBWH0l9TrOyiPkxNh5dG3aiyNu258QyZqTO6KybXeWl/mrCnUGRs8rFfuL");
            FootMime.WriteLine("rH0BoUGSxKAZRl/1nPYLvBce3Nf4LdMmpGVzA5/KdKBtBPbcGCzOCZElOlSaPPkZLLfij0Cwbg0/");
            FootMime.WriteLine("DpAfnJJYyB5MwEXidmM4yE38D51Pvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB");
            FootMime.WriteLine("78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZH");
            FootMime.WriteLine("Ae/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvW");
            FootMime.WriteLine("RwOiv/ny/wCrn/51P+Mn8eX/AJc/65cD/9k=");
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            FootMime.WriteLine("Content-Type: image/gif");
            FootMime.WriteLine("Content-Transfer-Encoding: base64");
            FootMime.WriteLine("Content-Location: http://Word/user.gif");
            FootMime.WriteLine("");
            FootMime.WriteLine(imguser);
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000--");


            return FootMime.ToString();
        }


        public string ImageToBase64(MemoryStream ms,  System.Drawing.Imaging.ImageFormat format)
        {

            byte[] imageBytes = ms.ToArray();


            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;

        }

        private MemoryStream ConvertImage_FromUrlToStream(string Image_Url)
        {

            string fileName = Image_Url.Substring(Image_Url.LastIndexOf("/") + 1);
            string folderBaseImage = "../images/";
            string FullFileNameRelative = folderBaseImage + fileName;
            string FullFileNameAbsolute = this.Server.MapPath(FullFileNameRelative);

            if (!File.Exists(FullFileNameAbsolute))
            {
                return new MemoryStream();
            }
            else
            {
                byte[] imageInBytesFormat = File.ReadAllBytes(FullFileNameAbsolute);

                using (MemoryStream imageInStreamFormat = new MemoryStream(imageInBytesFormat, 0, imageInBytesFormat.Length))
                {
                    imageInStreamFormat.Write(imageInBytesFormat, 0, imageInBytesFormat.Length);
                    return imageInStreamFormat;
                }
            }


        }

   

    

      

    }
}