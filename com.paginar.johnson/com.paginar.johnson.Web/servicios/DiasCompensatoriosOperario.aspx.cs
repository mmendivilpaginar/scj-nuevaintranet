﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.servicios
{
    public partial class DiasCompensatoriosOperario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           //Días Compensatorio Operarios
            com.paginar.johnson.membership.Roles r = new com.paginar.johnson.membership.Roles();
            if( r.IsUserInRole(Page.User.Identity.Name, "Días Compensatorio Operarios"))
                IAsp.Attributes.Add("src", "/SessionTransfer.aspx?dir=2aspx&url=Compensatorios/DiasCompensatoriosOperario.aspx");
            else if (r.IsUserInRole(Page.User.Identity.Name, "Días Compensatorio Administrativos") || r.IsUserInRole(Page.User.Identity.Name, "Jefes Compensatorios") || r.IsUserInRole(Page.User.Identity.Name, "Jefes") || r.IsUserInRole(Page.User.Identity.Name, "Gerentes"))
            {
                IAsp.Attributes.Add("src", "/SessionTransfer.aspx?dir=2aspx&url=Compensatorios/DiasCompensatoriosAdministrativos.aspx");
            }
        }
    }
}