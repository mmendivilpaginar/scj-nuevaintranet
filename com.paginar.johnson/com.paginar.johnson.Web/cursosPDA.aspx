﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cursosPDA.aspx.cs" Inherits="com.paginar.johnson.Web.cursosPDA" %>

<%@ Register src="UserControl_Home/ucPDA.ascx" tagname="ucPDA" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cursos PDA</title>
    <script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../js/jquery.corner.js" type="text/javascript"></script>
    <link href="css/pdaStyle.css" rel="stylesheet" type="text/css" />
    <link id="fav1" runat="server" rel="shortcut icon" href="~/favicon.ico" type="image/x-icon"/>
    <link id="fav2" runat="server" rel="icon" href="~/favicon.ico" type="image/ico"/>
</head>
<body onload="loadMandatory();">
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <uc1:ucPDA ID="ucPDA1" runat="server" />
    
    </div>
    </form>
</body>
</html>
