﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web
{
    public partial class ForeignKeyFilter : System.Web.DynamicData.QueryableFilterUserControl
    {
        private const string NullValueString = "[null]";
        private new MetaForeignKeyColumn Column
        {
            get
            {
                return (MetaForeignKeyColumn)base.Column;
            }
        }

        public override Control FilterControl
        {
            get
            {
                return DropDownList1;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Column.IsRequired)
                {
                    DropDownList1.Items.Add(new ListItem("[Not Set]", NullValueString));
                }
                PopulateListControl(DropDownList1);
                //FiltrarByCluster(DropDownList1);
                // Set the initial value if there is one
                string initialValue = DefaultValue;
                if (!String.IsNullOrEmpty(initialValue))
                {
                    DropDownList1.SelectedValue = initialValue;
                }
                int resEvalClust = VerificarDDL(DropDownList1);
                if (resEvalClust > 1)
                {
                    DropDownList1.SelectedValue = resEvalClust.ToString();
                    DropDownList1.Enabled = false;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            FiltrarByCluster(DropDownList1);
        }
        private void FiltrarByCluster(DropDownList DropDownList1)
        {
            this.Session["_SelectValueFromFK"] = this.DropDownList1.SelectedValue;
            Usuario usr = new Usuario();
            usr = Session["Usuario"] as Usuario;
            if (usr.clusteridactual == 1) return;

            if (Column.ParentTable.Name.Equals("AgendaTelUbicacions"))
            {
                List <ListItem> lstForDelete= new List<ListItem>();
                SCJBODataContext SCJBO = new SCJBODataContext();
                var m1 = from R in SCJBO.AgendaTelUbicacions
                         where R.ClusterID == usr.clusteridactual
                         select new {id= R.AgendaUbicacionID.ToString()};
                
              

                foreach (ListItem item in DropDownList1.Items)
                {
                    int value = 0;
                    foreach (var item2 in m1)
	                {
                        if (item.Value == null || item.Value == "" || item.Value.Equals(item2.id))
                            value = 1;
	                }

                      if (value==0)  lstForDelete.Add(item);
                    
                }

                foreach (var itemDel in lstForDelete)
                {
                    DropDownList1.Items.Remove(itemDel);
                }
            }

            if (Column.ParentTable.Name.Equals("AgendaTelCategorias"))
            {
                List<ListItem> lstForDelete = new List<ListItem>();
                SCJBODataContext SCJBO = new SCJBODataContext();
                var m1 = from R in SCJBO.AgendaTelUbicacions
                         where R.ClusterID == usr.clusteridactual
                         from R2 in R.AgendaTelCategorias
                         where R2.AgendaUbicacionID==R.AgendaUbicacionID
                         select new { id = R2.AgendaCategoriaID.ToString() };



                foreach (ListItem item in DropDownList1.Items)
                {
                    int value = 0;
                    foreach (var item2 in m1)
                    {
                        if (item.Value == null || item.Value == "" || item.Value.Equals(item2.id))
                            value = 1;
                    }

                    if (value == 0) lstForDelete.Add(item);

                }

                foreach (var itemDel in lstForDelete)
                {
                    DropDownList1.Items.Remove(itemDel);
                }
            }

                     
        }

        public int VerificarDDL(DropDownList DropDownList1) 
        {
            int clusterAsetear = 0;
            if (DropDownList1.Items[2].ToString() == "Argentina" && DropDownList1.Items[3].ToString() == "Chile")
            {
                Usuario usr = new Usuario();
                usr = Session["Usuario"] as Usuario;
                if (usr.clusteriddefecto != 1)
                {
                    
                    switch (usr.clusteriddefecto.ToString())
                    {
                        case "2":
                            clusterAsetear = 2;
                            break;
                        case "3":
                            clusterAsetear = 3;
                            break;
                        case "4":
                            clusterAsetear = 4;
                            break;
                    }
                }
            }

            return clusterAsetear;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnFilterChanged();

            this.Session["_SelectValueFromFK"] = this.DropDownList1.SelectedValue;
        }

        public override IQueryable GetQueryable(IQueryable source)
        {
            string selectedValue = DropDownList1.SelectedValue;
            if (String.IsNullOrEmpty(selectedValue))
            {
               return source;
            }

            if (selectedValue == NullValueString)
            {
                return ApplyEqualityFilter(source, Column.Name, null);
            }

            IDictionary dict = new Hashtable();
            Column.ExtractForeignKey(dict, selectedValue);
            foreach (DictionaryEntry entry in dict)
            {
                string key = (string)entry.Key;
                if (DefaultValues != null)
                {
                    DefaultValues[key] = entry.Value;
                }
                source = ApplyEqualityFilter(source, Column.GetFilterExpression(key), entry.Value);
            }
            return source;
        }

  

    }
}
