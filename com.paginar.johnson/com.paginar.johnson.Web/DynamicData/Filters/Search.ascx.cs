﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq.Expressions;
using System.Reflection;

namespace com.paginar.johnson.Web.DynamicData.Filters
{
    public partial class Search : System.Web.DynamicData.QueryableFilterUserControl
    {
    private const string NullValueString = "[null]";

    protected void Page_Init(object sender, EventArgs e) {
       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }


    public override IQueryable GetQueryable(IQueryable source) {

        if (Column.TypeCode != TypeCode.String)
            return source;


  string searchString = TextBox1.Text;
  if (TextBox1.Text == "")
      searchString = String.Empty;
        Type type = typeof(String);
        

  
  string searchProperty = Column.DisplayName;
  ConstantExpression searchFilter = Expression.Constant(searchString);
      

  ParameterExpression parameter = Expression.Parameter(source.ElementType);
  MemberExpression property = Expression.Property(parameter, this.Column.Name);
  if (Nullable.GetUnderlyingType(property.Type) != null)
  {
    property = Expression.Property(property, "Value");
      
  }
  MethodInfo method = typeof(String).GetMethod("Contains", new[] { typeof(String) });
        
  var containsMethodExp = Expression.Call(property, method, searchFilter);
  var containsLambda = Expression.Lambda(containsMethodExp,parameter);
  

  var resultExpression = Expression.Call(typeof(Queryable), "Where", new Type[] { source.ElementType }, source.Expression, Expression.Quote(containsLambda));

        return source.Provider.CreateQuery(resultExpression);

    }


    
    protected void Button1_Click(object sender, EventArgs e)
    {
        OnFilterChanged();
    }

    

}

}