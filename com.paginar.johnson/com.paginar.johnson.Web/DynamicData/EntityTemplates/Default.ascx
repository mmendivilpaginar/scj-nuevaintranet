﻿<%@ Control Language="C#" CodeBehind="Default.ascx.cs" Inherits="com.paginar.johnson.Web.DefaultEntityTemplate" %>

<asp:EntityTemplate runat="server" ID="EntityTemplate1">
    <ItemTemplate>
        <div class="form-item">
        
            <asp:Label Font-Bold="true" ID="Label1" runat="server" OnInit="Label_Init" />:
        
            <asp:DynamicControl ID="DynamicControl1" runat="server" OnInit="DynamicControl_Init" />
        </div>
    </ItemTemplate>
</asp:EntityTemplate>

