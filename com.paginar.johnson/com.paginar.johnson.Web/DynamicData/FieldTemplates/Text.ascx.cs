﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace com.paginar.johnson.Web
{
    public partial class TextField : System.Web.DynamicData.FieldTemplateUserControl
    {
        private const int MAX_DISPLAYLENGTH_IN_LIST = 50;

        public override string FieldValueString
        {
            get
            {
                Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
                Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
                string value = base.FieldValueString;
                if (ContainerType == ContainerType.List)
                {
                    value = Server.HtmlDecode(value);
                    //if (value != null && value.Length > MAX_DISPLAYLENGTH_IN_LIST)
                    //{
                    //    value = value.Substring(0, MAX_DISPLAYLENGTH_IN_LIST - 3) + "...";
                    //}
                }
                return value;
            }
        }

        public override Control DataControl
        {
            get
            {
                return Literal1;
            }
        }

    }
}
