﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;
using System.Web.UI.WebControls;
using System;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web
{
    public static class Extensiones
    {
        /// <summary>
        /// Get the attribute or a default instance of the attribute
        /// if the Column attribute do not contain the attribute
        /// </summary>
        /// <typeparam name="T">Attribute type</typeparam>
        /// <param name="table">Column to search for the attribute on.</param>
        /// <returns>The found attribute or a default instance of the attribute of type T</returns>
        public static T GetAttributeOrDefault<T>(this MetaColumn column) where T : Attribute, new()
        {
            return column.Attributes.OfType<T>().DefaultIfEmpty(new T()).FirstOrDefault();
        }

        public static AdvancedFieldTemplate GetDependeeField<T>(this Control control, MetaColumn column) where T : Control
        {
            // get value of dev ddl (Community)
            var detailsView = control.GetParent<T>();
            // get parent column attribute
            var dependeeColumn = column.GetAttribute<DependeeColumnAttribute>();

            if (dependeeColumn != null)
            {
                // Get Parent FieldTemplate
                var dependeeDynamicControl = detailsView.FindDynamicControlRecursive(dependeeColumn.ColumnName) as DynamicControl;

                AdvancedFieldTemplate dependeeField = null;

                // setup the selection event
                if (dependeeDynamicControl != null)
                    dependeeField = dependeeDynamicControl.Controls[0] as AdvancedFieldTemplate;

                return dependeeField;
            }
            return null;
        }

        public static T GetAttributeOrDefault<T>(this MetaTable table) where T : Attribute, new()
        {
            return table.Attributes.OfType<T>().DefaultIfEmpty(new T()).FirstOrDefault();
        }

        public static T GetAttributet<T>(this MetaTable table) where T : Attribute
        {
            return table.Attributes.OfType<T>().FirstOrDefault();
        }

        //public static T GetAttributeOrDefault<T>(this MetaColumn column) where T : Attribute, new()
        //{
        //    return column.Attributes.OfType<T>().DefaultIfEmpty(new T()).FirstOrDefault();
        //}

        public static T GetAttribute<T>(this MetaColumn column) where T : Attribute
        {
            return column.Attributes.OfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Get the control by searching recursively for it.
        /// </summary>
        /// <param name="Root">The control to start the search at.</param>
        /// <param name="Id">The ID of the control to find</param>
        /// <returns>The control the was found or NULL if not found</returns>
        public static Control FindControlRecursive(this Control root, string Id)
        {
            if (root.ID == Id)
                return root;

            foreach (Control Ctl in root.Controls)
            {
                Control FoundCtl = FindControlRecursive(Ctl, Id);

                if (FoundCtl != null)
                    return FoundCtl;
            }
            return null;
        }

        /// <summary>
        /// Get the control by searching recursively for it.
        /// </summary>
        /// <param name="Root">The control to start the search at.</param>
        /// <param name="Id">The ID of the control to find</param>
        /// <returns>The control the was found or NULL if not found</returns>
        public static Control FindDynamicControlRecursive(this Control root, string dataField)
        {
            var dc = root as DynamicControl;
            if (dc != null)
            {
                if (dc.DataField == dataField)
                    return dc;
            }

            foreach (Control Ctl in root.Controls)
            {
                Control FoundCtl = FindDynamicControlRecursive(Ctl, dataField);

                if (FoundCtl != null)
                    return FoundCtl;
            }
            return null;
        }

        /// <summary>
        /// Get a parent control of T from the parent control tree of the control
        /// </summary>
        /// <typeparam name="T">Control of type T</typeparam>
        /// <param name="control">Control to search in for control type T</param>
        /// <returns>The found control of type T or null if not found</returns>
        public static T GetParent<T>(this Control control) where T : Control
        {
            var parentControl = control.Parent;
            while (parentControl != null)
            {
                var formView = parentControl as T;
                if (formView != null)
                    return formView;
                else
                    parentControl = parentControl.Parent;
            }
            return null;
        }

        /// <summary>
        /// Gets a value indicating if the column should be scaffolded. This honors the
        /// ScaffoldColumnAttribute as well as returns true if the column is an enumerated type.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static bool GetScaffold(this MetaColumn column)
        {
            // make sure we honor the ScaffoldColumnAttribute. The framework already does this
            // but we want to do this again as the first thing.
            var scaffoldAttribute = column.GetAttribute<ScaffoldColumnAttribute>();
            if (scaffoldAttribute != null)
            {
                return scaffoldAttribute.Scaffold;
            }

            // always return true for enumerated types
            return column.ColumnType.IsEnum || column.Scaffold;
        }
    }
}