﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web
{
    public partial class Cluster : FieldTemplateUserControl
    {
        private bool _allowNavigation = true;
        private string _navigateUrl;

        public string NavigateUrl
        {
            get
            {
                return _navigateUrl;
            }
            set
            {
                _navigateUrl = value;
            }
        }

        public bool AllowNavigation
        {
            get
            {
                return _allowNavigation;
            }
            set
            {
                _allowNavigation = value;
            }
        }

        protected string GetDisplayString()
        {
            return FormatFieldValue(ForeignKeyColumn.ParentTable.GetDisplayString(FieldValue));
        }

        protected string GetNavigateUrl()
        {
            var dissallow = Column.GetAttributeOrDefault<DisallowNavigationAttribute>();

            if (!AllowNavigation || dissallow.Hide)
            {
                // remove undeline :D
                HyperLink1.Style.Add(HtmlTextWriterStyle.TextDecoration, "none !important");
                return null;
            }

            if (String.IsNullOrEmpty(NavigateUrl))
            {
                return ForeignKeyPath;
            }
            else
            {
                return BuildForeignKeyPath(NavigateUrl);
            }
        }

        public override Control DataControl
        {
            get
            {
                return HyperLink1;
            }
        }
    }
}