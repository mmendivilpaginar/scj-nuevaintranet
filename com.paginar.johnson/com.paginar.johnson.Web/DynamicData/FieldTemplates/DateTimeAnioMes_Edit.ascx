﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeAnioMes_Edit.ascx.cs" Inherits="com.paginar.johnson.Web.DateTimeAnioMes_Edit" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:TextBox ID="TextBox1" runat="server" CssClass="DDTextBox" 
    Text='<%# FieldValueEditString %>' Columns="20" style="margin-bottom: 0px" 
    Width="127px"></asp:TextBox><asp:ImageButton ID="IMGCalen1" runat="server" 
            ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
            Enabled="True" PopupButtonID="IMGCalen1" 
    TargetControlID="TextBox1" Format="dd/MM" TodaysDateFormat="dd/MM">
        </asp:CalendarExtender>
        <%-- Format="dd/MM" TodaysDateFormat="dd/MM"--%>
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" CssClass="DDControl DDValidator" ControlToValidate="TextBox1" Display="Static" Enabled="false" />
<asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" CssClass="DDControl DDValidator" ControlToValidate="TextBox1" Display="Static" Enabled="false" />
<asp:DynamicValidator runat="server" ID="DynamicValidator1" CssClass="DDControl DDValidator" ControlToValidate="TextBox1" Display="Static" />
<asp:CustomValidator runat="server" ID="DateValidator" CssClass="DDControl DDValidator" ControlToValidate="TextBox1" Display="Static" EnableClientScript="false" Enabled="false" OnServerValidate="DateValidator_ServerValidate" />
