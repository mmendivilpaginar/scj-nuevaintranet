﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web
{
    public partial class ForeignKey_EditField : System.Web.DynamicData.FieldTemplateUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (DropDownList1.Items.Count == 0)
            {
                if (Mode == DataBoundControlMode.Insert || !Column.IsRequired)
                {
                    DropDownList1.Items.Add(new ListItem("[Not Set]", ""));
                }
                PopulateListControl(DropDownList1);
            }
            int resEvalClust = VerificarDDL(DropDownList1);
            if (resEvalClust > 1)
            {
                DropDownList1.SelectedValue = resEvalClust.ToString();
                DropDownList1.Enabled = false;
            }

            SetUpValidator(RequiredFieldValidator1);
            SetUpValidator(DynamicValidator1);
        }


        public int VerificarDDL(DropDownList DropDownList1)
        {
            int clusterAsetear = 0;
            if (DropDownList1.Items.Count > 4)
            {
                if (DropDownList1.Items[2].ToString() == "Argentina" && DropDownList1.Items[3].ToString() == "Chile")
                {
                    Usuario usr = new Usuario();
                    usr = Session["Usuario"] as Usuario;
                    if (usr.clusteriddefecto != 1)
                    {

                        switch (usr.clusteriddefecto.ToString())
                        {
                            case "2":
                                clusterAsetear = 2;
                                break;
                            case "3":
                                clusterAsetear = 3;
                                break;
                            case "4":
                                clusterAsetear = 4;
                                break;
                        }
                    }
                }
            }

            return clusterAsetear;
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);

            string selectedValueString = GetSelectedValueString();
            ListItem item = DropDownList1.Items.FindByValue(selectedValueString);
            if (item != null)
            {
                DropDownList1.SelectedValue = selectedValueString;
            }

        }

        protected override void ExtractValues(IOrderedDictionary dictionary)
        {
            // Es una cadena vacía, cámbiela a NULL
            string value = DropDownList1.SelectedValue;
            if (String.IsNullOrEmpty(value))
            {
                value = null;
            }

            ExtractForeignKey(dictionary, value);
        }

        public override Control DataControl
        {
            get
            {
                return DropDownList1;
            }
        }

    }
}
