﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;

/// <summary>
/// A class to add some extra features to the standard
/// FieldTemplateUserControl
/// </summary>
namespace com.paginar.johnson.Web
{
    public class AdvancedFieldTemplate : FieldTemplateUserControl
    {
        /// <summary>
        /// Handles the adding events to the drop down list
        /// </summary>
        public virtual event EventHandler SelectionChanged
        {
            add { }
            remove { }
        }

        /// <summary>
        /// Returns the selected value of the drop down list
        /// </summary>
        public virtual string SelectedValue
        {
            get
            {
                return null;
            }
        }
    }

}