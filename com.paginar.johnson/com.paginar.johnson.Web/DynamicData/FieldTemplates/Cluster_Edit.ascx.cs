﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.DynamicData;
using System.Collections.Specialized;

namespace com.paginar.johnson.Web
{
    public partial class Cluster_Edit : AdvancedFieldTemplate
    {

        public override event EventHandler SelectionChanged
        {
            add
            {
                DropDownList1.SelectedIndexChanged += value;
            }
            remove
            {
                DropDownList1.SelectedIndexChanged -= value;
            }
        }

        public override string SelectedValue
        {
            get
            {
                return DropDownList1.SelectedValue;
            }
        }

        public override Control DataControl
        {
            get
            {
                return DropDownList1;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (DropDownList1.Items.Count == 0)
            {
                if (!Column.IsRequired)
                    DropDownList1.Items.Add(new ListItem("[Not Set]", ""));

                PopulateListControl(DropDownList1, "");
            }

            // get dependee field
            var dependeeField = this.GetDependeeField<DetailsView>(Column);

            // add event handler if dependee exists
            if (dependeeField != null)
                dependeeField.SelectionChanged += SelectedIndexChanged;
        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;
            if (ddl != null)
                PopulateListControl(DropDownList1, ddl.SelectedValue);
        }

        protected void PopulateListControl(DropDownList ddl, String filterByValue)
        {
            var DC = new com.paginar.johnson.DAL.SCJBODataContext();

            var clusters = DC.Clusters.AsQueryable();

            var list = new List<ListItem>();
            list.Add(new ListItem() { Value = "", Text = "All" });

            // setup drop down list
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";

            //if (selectedValue != "")
            //    categories = categories.Where(fp => fp.CategoryID == selectedValue);

            list.AddRange(clusters.Select(c => new ListItem() { Value = c.ID.ToString(), Text = c.Descripcion }).ToList());
            ddl.DataSource = list;
            ddl.DataBind();
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);

            if (Mode == DataBoundControlMode.Edit)
            {
                var dependeeField = this.GetDependeeField<DetailsView>(Column);
                if (dependeeField != null)
                    PopulateListControl(DropDownList1, dependeeField.SelectedValue);

                string foreignkey = ForeignKeyColumn.GetForeignKeyString(Row);
                ListItem item = DropDownList1.Items.FindByValue(foreignkey);
                if (item != null)
                {
                    DropDownList1.SelectedValue = foreignkey;
                }
            }
        }

        protected override void ExtractValues(IOrderedDictionary dictionary)
        {
            // If it's an empty string, change it to null
            string val = DropDownList1.SelectedValue;
            if (val == String.Empty)
                val = null;

            ExtractForeignKey(dictionary, val);
        }
    }
}