﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using System.Data.Linq;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace com.paginar.johnson.Web
{
    public partial class Edit : System.Web.UI.Page
    {
        protected MetaTable table;

        protected void Page_Init(object sender, EventArgs e)
        {
            table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            FormView1.SetMetaTable(table);
            DetailsDataSource.EntityTypeName = table.EntityType.AssemblyQualifiedName;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           // Title = table.DisplayName;

        }

        protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == DataControlCommands.CancelCommandName)
            {
                Response.Redirect(table.ListActionPath);
            }
        }

        private void SetValueForClasiRubros(System.Data.Linq.ChangeAction e)
        {
            if (this.table.Name.Equals("clasiRubros"))
            {
                DAL.ValueCommon.CurrentUserId = (this.Page.Master as MasterBase).ObjectUsuario.usuarioid;

                switch (e)
                {
                    case ChangeAction.Insert:
                        DAL.ValueCommon.LastOperation = System.Data.Linq.ChangeAction.Insert;
                        break;
                    case ChangeAction.Update:
                        DAL.ValueCommon.LastOperation = System.Data.Linq.ChangeAction.Update;
                        break;
                    default:
                        break;
                }

            }
        }
        protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {

           //his.SetValueForClasiRubros(ChangeAction.Update);
            if (e.Exception == null || e.ExceptionHandled)
            {
                Response.Redirect(table.ListActionPath);
            }
        }

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            SetValueForClasiRubros(ChangeAction.Update);
        }

        protected void FormView1_DataBound(object sender, EventArgs e)
        {

            this.ValidUIForAgendaUbicacion();
        }

        private void ValidUIForAgendaUbicacion()
        {

            Usuario usr = new Usuario();
            usr = Session["Usuario"] as Usuario;

            
            if ((this.table.Name.Equals("AgendaTelUbicacions")) && usr.clusteridactual.ToString() != "1")
            {
                var fieldTempCluster = (this.FormView1.FindDynamicControlRecursive("Cluster").FindFieldTemplate("Cluster")) as FieldTemplateUserControl;
                if (fieldTempCluster != null)
                {
                    var comboCluster = fieldTempCluster.DataControl as DropDownList;
                    comboCluster.SelectedValue = usr.clusteridactual.ToString();
                    comboCluster.Enabled = false;

                }
            }


            if ((this.table.Name.Equals("Efemerides")) && usr.clusteridactual.ToString() != "1")
            {
                var fieldTempCluster = (this.FormView1.FindDynamicControlRecursive("Cluster1").FindFieldTemplate("Cluster1")) as FieldTemplateUserControl;
                if (fieldTempCluster != null)
                {
                    var comboCluster = fieldTempCluster.DataControl as DropDownList;
                    comboCluster.SelectedValue = usr.clusteridactual.ToString();
                    comboCluster.Enabled = false;

                }
            }

            if (this.table.Name.Equals("AgendaTelCategorias") && usr.clusteridactual.ToString() != "1")
            {
                var fieldTempCluster = (this.FormView1.FindDynamicControlRecursive("AgendaTelUbicacion").FindFieldTemplate("AgendaTelUbicacion")) as FieldTemplateUserControl;
                var comboUbicaciones = fieldTempCluster.DataControl as DropDownList;
                List<ListItem> lstForDelete = new List<ListItem>();
                SCJBODataContext SCJBO = new SCJBODataContext();
                var m1 = from R in SCJBO.AgendaTelUbicacions
                         where R.ClusterID == usr.clusteridactual
                         select new { id = R.AgendaUbicacionID.ToString() };



                foreach (ListItem item in comboUbicaciones.Items)
                {
                    int value = 0;
                    foreach (var item2 in m1)
                    {
                        if (item.Value == null || item.Value == "" || item.Value.Equals(item2.id))
                            value = 1;
                    }

                    if (value == 0) lstForDelete.Add(item);

                }

                foreach (var itemDel in lstForDelete)
                {
                    comboUbicaciones.Items.Remove(itemDel);
                }
            }

            if (this.table.Name.Equals("AgendaTels") && usr.clusteridactual.ToString() != "1")
            {
                var fieldTempCluster = (this.FormView1.FindDynamicControlRecursive("AgendaTelCategoria").FindFieldTemplate("AgendaTelCategoria")) as FieldTemplateUserControl;
                var comboCategorias = fieldTempCluster.DataControl as DropDownList;
                List<ListItem> lstForDelete = new List<ListItem>();
                SCJBODataContext SCJBO = new SCJBODataContext();
                var m1 = from row in SCJBO.AgendaTelUbicacions
                         where row.ClusterID == usr.clusteridactual
                         from row2 in row.AgendaTelCategorias
                         select new { id = row2.AgendaCategoriaID.ToString() };



                foreach (ListItem item in comboCategorias.Items)
                {
                    int value = 0;
                    foreach (var item2 in m1)
                    {
                        if (item.Value == null || item.Value == "" || item.Value.Equals(item2.id))
                            value = 1;
                    }

                    if (value == 0) lstForDelete.Add(item);

                }

                foreach (var itemDel in lstForDelete)
                {
                    comboCategorias.Items.Remove(itemDel);
                }
            }
        }

    }
}
