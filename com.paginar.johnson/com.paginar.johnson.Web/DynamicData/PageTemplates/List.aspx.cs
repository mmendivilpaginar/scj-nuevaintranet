﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using com.paginar.johnson.Web.DynamicData.PageTemplates;
using com.paginar.johnson.DAL;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace com.paginar.johnson.Web
{
    public partial class List : System.Web.UI.Page
    {
        protected MetaTable table;

        protected void Page_Init(object sender, EventArgs e)
        {
            table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            GridView1.SetMetaTable(table, table.GetColumnValuesFromRoute(Context));
            GridDataSource.EntityTypeName = table.EntityType.AssemblyQualifiedName;
            if (table.EntityType != table.RootEntityType)
            {
                GridQueryExtender.Expressions.Add(new OfTypeExpression(table.EntityType));

            }
            else
                this.OrderEntity();
        }

        private void OrderEntity()
        {

            
            OrderByExpression ord = null;
            switch (this.table.Name)
            {
                case "infComedors":
                    ord = new OrderByExpression() { Direction = SortDirection.Descending, DataField = "Fecha" };

                    break;
                default:
                    break;
            }
            if (ord != null)
                GridQueryExtender.Expressions.Add(ord);
        }


        protected void FilterByAgendaTelCategoriaByCluster(object sender, CustomExpressionEventArgs e)
        {
            

            if (this.table.Name.Equals("AgendaTelCategorias"))
            {
                object ValAll = this.Session["_SelectValueFromFK"];

                string valueFilter = (ValAll == null ? "" : ValAll.ToString());
                    if (valueFilter==string.Empty)
                    {
                          com.paginar.johnson.BL.Usuario usr =  Session["Usuario"] as com.paginar.johnson.BL.Usuario;
                        SCJBODataContext mod = new SCJBODataContext();
                        var res = from row in mod.AgendaTelUbicacions
                                  where row.ClusterID == usr.clusteridactual
                                  from row2 in row.AgendaTelCategorias
                                  select row2;
                        e.Query = res;
                    }
                
            }


            if (this.table.Name.Equals("AgendaTels"))
            {
                object ValAll = this.Session["_SelectValueFromFK"];

                string valueFilter = (ValAll == null ? "" : ValAll.ToString());
                if (valueFilter == string.Empty)
                {
                    com.paginar.johnson.BL.Usuario usr = Session["Usuario"] as com.paginar.johnson.BL.Usuario;
                    SCJBODataContext mod = new SCJBODataContext();
                    var res = from row in mod.AgendaTelUbicacions
                              where row.ClusterID == usr.clusteridactual
                              from row2 in row.AgendaTelCategorias
                              where row2.AgendaUbicacionID== row.AgendaUbicacionID
                              from row3 in row2.AgendaTels
                              select row3;
                    e.Query = res;
                }

            }
        }

        //private void FilterByAgendaTelUbicacion()
        //{


          

        //    if (this.table.Name.Equals("AgendaTelCategorias"))
        //    {
               
        //        SCJBODataContext mod = new SCJBODataContext();
        //        var res = from row in mod.AgendaTelUbicacions
        //                  where row.ClusterID == 2
        //                  from row2 in row.AgendaTelCategorias
        //                  select row2;
             
                
            
                
        //    }
           
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            //Title = table.DisplayName;


            // Deshabilitar varias opciones si la tabla es de solo lectura
            if (table.IsReadOnly)
            {
                GridView1.Columns[0].Visible = false;
                InsertHyperLink.Visible = false;
                GridView1.EnablePersistedSelection = false;
            }


        }

        protected void Label_PreRender(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            DynamicFilter dynamicFilter = (DynamicFilter)label.FindControl("DynamicFilter");
            QueryableFilterUserControl fuc = dynamicFilter.FilterTemplate as QueryableFilterUserControl;
            if (fuc != null && fuc.FilterControl != null)
            {
                label.AssociatedControlID = fuc.FilterControl.GetUniqueIDRelativeTo(label);
            }
            if (dynamicFilter.DataField=="AgendaTelCategoria") 
            {
                label.Text = "Agenda Categoría";
            }
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary(GridView1.GetDefaultValues());
            InsertHyperLink.NavigateUrl = table.GetActionPath(PageAction.Insert, routeValues);
            base.OnPreRenderComplete(e);
        }

        protected void DynamicFilter_FilterChanged(object sender, EventArgs e)
        {
            GridView1.PageIndex = 0;
        }
        public string tituloTabla(string titulo)
        {
            if (titulo == "Efemerides") 
            { 
                return "Efemérides";
            }
            else if (titulo == "AgendaTelCategorias") 
            {
                return "Agenda Categorías";
            }
            else{
                return titulo; 
            }
        }

        public string subtitulo(string subtit)
        {
            if (subtit == "AgendaTelCategoria")
            {
                return "Agenda Telefónica Categoría";
            }
            else
            {
                return subtit;
            }

        }

        protected void GridQueryExtender_Load(object sender, EventArgs e)
        {
            //if (this.table.Name.Equals("AgendaTelCategorias"))
            //{
            //    FilterByAgendaTelUbicacion();
            //}
        }

        

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.table.Name.Equals("AgendaTelCategorias"))
            {

                SCJBODataContext mod = new SCJBODataContext();
                var res = from row in mod.AgendaTels
                          where row.AgendaTelCategoriaID == (int)e.Keys["AgendaCategoriaID"]
                          select new { id = row.AgendaTelID.ToString() };

                if (res.Count() > 0)
                {
                    e.Cancel = true;
                    EjecutarScript("alert('No se pudo eliminar la categoria, tiene agendas asociadas')");
                }
            }

            if (this.table.Name.Equals("AgendaTelUbicacions"))
            {

                SCJBODataContext mod = new SCJBODataContext();
                var res = from row in mod.AgendaTelCategorias
                          where row.AgendaUbicacionID == (int)e.Keys["AgendaUbicacionID"]
                          select new { id = row.AgendaCategoriaID.ToString() };

                if (res.Count() > 0)
                {
                    e.Cancel = true;
                    EjecutarScript("alert('No se pudo eliminar la ubicación, tiene categorias asociadas')");
                }
            }
           
        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        

        //protected bool CanDeleteActionType(object strActionTypeID)
        //{
        //    //int ActionTypeID = Convert.ToInt32(strActionTypeID);
        //    //ActionsService aser = new ActionsService();
        //    //return (aser.NumberOfActionsForType(ActionTypeID) == 0);
        //}
    }
}
