﻿<%@ Page Language="C#" MasterPageFile="~/BO/MasterBO.Master" CodeBehind="Edit.aspx.cs" Inherits="com.paginar.johnson.Web.Edit" 
EnableEventValidation="false" ValidateRequest="false"%>


<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //cabecera de links
            $('#treeBO > table').wrap('<div class="header"></div>');

            //contenedor de links
            $('div[id*=treeBOn]').each(function (index) {
                $(this).addClass('content');
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="CPMAIN" Runat="Server">
    <asp:DynamicDataManager ID="DynamicDataManager1" runat="server" AutoLoadForeignKeys="true">
        <DataControls>
            <asp:DataControlReference ControlID="FormView1" />
        </DataControls>
    </asp:DynamicDataManager>

    <h2 class="DDSubHeader"><%--Editar entrada de la tabla--%> <%= table.DisplayName %></h2>

   <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                HeaderText="Lista de errores de validación" CssClass="DDValidator" />
            <asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" Display="None" CssClass="DDValidator" />

            <asp:FormView runat="server" ID="FormView1" 
        DataSourceID="DetailsDataSource" DefaultMode="Edit"
                OnItemCommand="FormView1_ItemCommand" 
        OnItemUpdated="FormView1_ItemUpdated" RenderOuterTable="false" 
        onitemupdating="FormView1_ItemUpdating" ondatabound="FormView1_DataBound">
                <EditItemTemplate>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
                    
                        <asp:DynamicEntity runat="server" Mode="Edit" />
                        <div class="controls">
                            <asp:Button ID="Button1" runat="server" CommandName="Update" Text="Actualizar" />
                            <asp:Button ID="Button2" runat="server" CommandName="Cancel" Text="Cancelar" CausesValidation="false" />
                        </div>
                        </asp:Panel>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <div class="DDNoItem">No existe tal elemento.</div>
                </EmptyDataTemplate>
            </asp:FormView>

            <asp:LinqDataSource ID="DetailsDataSource" runat="server" EnableUpdate="true" />

            <asp:QueryExtender TargetControlID="DetailsDataSource" ID="DetailsQueryExtender" runat="server">
                <asp:DynamicRouteExpression />
            </asp:QueryExtender>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

