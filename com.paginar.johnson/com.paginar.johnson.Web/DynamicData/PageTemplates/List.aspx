﻿<%@ Page Language="C#" MasterPageFile="~/BO/MasterBO.Master"CodeBehind="List.aspx.cs" Inherits="com.paginar.johnson.Web.List" %>

<%@ Register src="~/DynamicData/Content/GridViewPager.ascx" tagname="GridViewPager" tagprefix="asp" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //cabecera de links
            $('#treeBO > table').wrap('<div class="header"></div>');

            //contenedor de links
            $('div[id*=treeBOn]').each(function (index) {
                $(this).addClass('content');
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="CPMAIN" Runat="Server">
    <asp:DynamicDataManager ID="DynamicDataManager1" runat="server" AutoLoadForeignKeys="true">
        <DataControls>
            <asp:DataControlReference ControlID="GridView1" />
        </DataControls>
    </asp:DynamicDataManager>
    <h2 class="DDSubHeader"><%= tituloTabla(table.DisplayName)%></h2>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="DD">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                    HeaderText="Lista de errores de validación" CssClass="DDValidator" />
                <asp:DynamicValidator runat="server" ID="GridViewValidator" ControlToValidate="GridView1" Display="None" CssClass="DDValidator" />

                <asp:QueryableFilterRepeater runat="server" ID="FilterRepeater">
                    <ItemTemplate>
                        <div class="form-item">
                            <%--<asp:Label runat="server" Text='<%= Eval("DisplayName")%>' OnPreRender="Label_PreRender" />--%>
                            <label><asp:Label ID="ltTitulo" runat="server" Text='<%# Eval("DisplayName") %>' OnPreRender="Label_PreRender"></asp:Label></label>
                            <asp:DynamicFilter runat="server" ID="DynamicFilter" OnFilterChanged="DynamicFilter_FilterChanged" />
                        </div>
                    </ItemTemplate>
                </asp:QueryableFilterRepeater>
                <br />
            </div>

            <asp:GridView ID="GridView1" runat="server" DataSourceID="GridDataSource" EnablePersistedSelection="true"
                AllowPaging="True" AllowSorting="True" CssClass="DDGridView" 
                RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6" onrowdeleting="GridView1_RowDeleting">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:DynamicHyperLink runat="server" Action="Edit" Text="Editar"/>&nbsp;
                            <asp:LinkButton  ID="LinkButtonDelete" runat="server" CommandName="Delete" Text="Eliminar" OnClientClick='return confirm("¿Está seguro de que desea eliminar este elemento?");'
                            />&nbsp;
                            <asp:DynamicHyperLink runat="server" Text="Detalles" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

                <HeaderStyle CssClass="th" />

                <PagerStyle CssClass="DDFooter"/>        
                <PagerTemplate>
                    <asp:GridViewPager runat="server" />
                </PagerTemplate>
                <EmptyDataTemplate>
                    <div class="messages msg-info">
                        Actualmente no hay elementos en esta tabla.
                    </div>
                </EmptyDataTemplate>
                <RowStyle CssClass="td" />
            </asp:GridView>

            <asp:LinqDataSource ID="GridDataSource" runat="server" EnableDelete="true" />
            
            <asp:QueryExtender TargetControlID="GridDataSource" ID="GridQueryExtender" 
                runat="server" onload="GridQueryExtender_Load"  >
                <asp:DynamicFilterExpression ControlID="FilterRepeater"   />
               <asp:CustomExpression OnQuerying="FilterByAgendaTelCategoriaByCluster" />
            </asp:QueryExtender>

            <br />

            <div class="DDBottomHyperLink">
                <asp:DynamicHyperLink ID="InsertHyperLink" runat="server" Action="Insert"><img runat="server" src="~/DynamicData/Content/Images/plus.gif" alt="Insertar nuevo elemento" />Insertar nuevo elemento</asp:DynamicHyperLink>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

