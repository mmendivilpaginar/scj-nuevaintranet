﻿<%@ Page Language="C#" MasterPageFile="~/BO/MasterBO.Master" CodeBehind="Insert.aspx.cs" Inherits="com.paginar.johnson.Web.Insert" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //cabecera de links
            $('#treeBO > table').wrap('<div class="header"></div>');

            //contenedor de links
            $('div[id*=treeBOn]').each(function (index) {
                $(this).addClass('content');
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="CPMAIN" Runat="Server">
    <asp:DynamicDataManager ID="DynamicDataManager1" runat="server" AutoLoadForeignKeys="true">
        <DataControls>
            <asp:DataControlReference ControlID="FormView1" />
        </DataControls>
    </asp:DynamicDataManager>

    <h2 class="DDSubHeader"><%--Agregar nueva entrada a la tabla --%><%= table.DisplayName %></h2>

  <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                HeaderText="Lista de errores de validación" CssClass="DDValidator" />
            <asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" Display="None" CssClass="DDValidator" />

            <asp:FormView runat="server" ID="FormView1" 
        DataSourceID="DetailsDataSource" DefaultMode="Insert"
                OnItemCommand="FormView1_ItemCommand" 
        OnItemInserted="FormView1_ItemInserted" RenderOuterTable="false" 
        oniteminserting="FormView1_ItemInserting" 
        onitemupdated="FormView1_ItemUpdated" ondatabound="FormView1_DataBound">
                <InsertItemTemplate>
                    <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
                    
                    <asp:DynamicEntity runat="server" Mode="Insert" />
                    <div class="controls">
                        <asp:Button ID="Button1" runat="server" CommandName="Insert" Text="Insertar" />
                        <asp:Button ID="Button2" runat="server" CommandName="Cancel" Text="Cancelar" CausesValidation="false" />
                    </div>
                    </asp:Panel>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:LinqDataSource ID="DetailsDataSource" runat="server" EnableInsert="true" />
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

