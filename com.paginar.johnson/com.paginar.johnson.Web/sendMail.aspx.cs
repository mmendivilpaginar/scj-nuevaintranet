﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web
{
    public partial class sendMail : System.Web.UI.Page
    {
       
        string SMTP;
        string MailToTest;
        string MailRRHH;
        string smtp_host;
        string port;
        string from_dir;
        string pass;
        string password;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ( Request["From"] != null && Request["To"] !=null  && Request["Subject"] != null && Request["Body"] != null) 
                {
                    lbltmsg.Text = SendMail(Request["From"].ToString(), Request["To"].ToString(), Request["Subject"].ToString(), Decode(Request["Body"].ToString()));
                }
            }
        }


        public string Decode(string encodebody)
        {
            
            encodebody = encodebody.Replace("MENSAJE", "Copie la URL del tramite y peguela sobre el navegador. Asegurese de haber iniciado sesión en la Intranet de Johnson.");
            encodebody = encodebody.Replace("URLPNETQA", "http://qa.arsanida000.paginar.org/ASPForm.aspx?url=servicios%2Fform%5F");
            encodebody = encodebody.Replace("URLQA", "http://arsanida000.global.scj.loc/ASPForm.aspx?url=servicios%2Fform%5F");
            encodebody = encodebody.Replace("URLPROD", "http://arsanipa003.global.scj.loc/ASPForm.aspx?url=servicios%2Fform%5F");
            //TODO agregar url prod
            encodebody = encodebody.Replace("P1", "%2Easp%3FFormularioID%3D");

            encodebody = encodebody.Replace("P2", "%26ASP%3Dform%5F");

            encodebody = encodebody.Replace("P3", "%2Easp%26");
            encodebody = encodebody.Replace("P4", "TramiteID%3D");
            encodebody = encodebody.Replace("P5", "%26FormEstado%3DNUEVO%5FPASO");
            encodebody = encodebody.Replace("NS", "&nbsp;");
            encodebody = encodebody.Replace("BI", "<b>");
            encodebody = encodebody.Replace("BF", "</b>");
            encodebody = encodebody.Replace("BRI", "<br>");
            encodebody = encodebody.Replace("BRF", "<br />");





            return encodebody;

        }


        public void getConfiguracion()
        {
            SMTP = ConfigurationSettings.AppSettings["SMTP"];         
            MailToTest = ConfigurationSettings.AppSettings["MAILTOTEST"].ToString();
            MailRRHH = ConfigurationSettings.AppSettings["MAILRRHH"].ToString();
            smtp_host = ConfigurationSettings.AppSettings["smtp"];
            port = ConfigurationSettings.AppSettings["port"];
            from_dir = ConfigurationSettings.AppSettings["from"];
            pass = ConfigurationSettings.AppSettings["pass"];
            password = ConfigurationSettings.AppSettings["password"];
        }


        public string SendMail(string From,string To,  string Subject, string Body)
        {
            getConfiguracion();

            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = To;
            msg.From = from_dir ;
            msg.Subject = Subject;
            msg.Body = Body;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            string enviado;
            try
            {
                if (!string.IsNullOrEmpty(SMTP))
                    SmtpMail.SmtpServer = SMTP;
                System.Web.Mail.SmtpMail.Send(msg);
                enviado = "true";

            }
            catch (Exception ex)
            {
                enviado = ex.Message;
                //enviado = false;
            }

            return enviado;

        }


      
    }
}