﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web.Security;
using com.paginar.johnson.BL;
using System.Web;
using com.paginar.johnson.DAL;
using AjaxControlToolkit;
using System.Text;
using System.Web.UI.WebControls;
namespace com.paginar.johnson.Web
{
    public partial class _Default : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
         
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ControllerNoticias CN = new ControllerNoticias();

               // ucNot.infInfoDt = CN.Repositorio.AdapterDSNoticias.infInfo.GetInfoCluster((this.ObjectUsuario.clusteridactual == 9 ? 1 : this.ObjectUsuario.clusteridactual), null);
                ucNot.CargarDatosHome((this.ObjectUsuario.clusteridactual == 9 ? 1 : this.ObjectUsuario.clusteridactual));

                LoadNoticia();
            }
        }


        protected void LoadNoticia()
        {
            if (Request["InfoID"] != null)
            {
                Response.Redirect("/noticias/noticia.aspx?infoID=" + Request["InfoID"].ToString(), false);
            }

            if (Request["privInfoID"] != null)
            {
                Response.Redirect("/noticias/urlNoticia.aspx?priv=" + Request["privInfoID"].ToString() + "&InfoID=" + Request["InfoID"].ToString(), false);
            }

        }

       
    }



}
