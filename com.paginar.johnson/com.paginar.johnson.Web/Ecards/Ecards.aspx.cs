﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace com.paginar.johnson.Web.Ecards
{
    public partial class Ecards : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                setCardsbycluster();


                hdmailfrom.Value = this.ObjectUsuario.email;
                lblFrom.Text =   this.ObjectUsuario.nombre+ " "+this.ObjectUsuario.apellido ;

                //txtMessage.Attributes.Add("onkeypress", " ValidarCaracteres(this, 149);");
                //txtMessage.Attributes.Add("onkeydown", " ValidarCaracteres(this, 149);");
            }
        }

        private void setCardsbycluster()
        {

       
            string cluster ="";
            
            string clusterid = ((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString();



            switch(clusterid){
                case "1":
                         cluster = @"Argentina/";
                         break;
                case "2": cluster = @"Chile/";
                          break;

                case "3": cluster = @"Uruguay/";
                          break;

                case "4": cluster = @"Paraguay/";
                break;
            }

            string path = Server.MapPath("~");
            path = path + @"\Ecards\images\";

            path = path + cluster;

            string[] files = Directory.GetFiles(path, "*.*");
            string html = "";

          

            foreach (string f in files)
            {
                string fname = Path.GetFileName(f);
                html += "<li><img src='images/" + cluster + fname + "' />  </li>";
            }

         

            ltCards.Text = html;

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancelar1_Click(object sender, EventArgs e)
        {
            Response.Redirect("..Default.aspx", false);
        }
    }
}