﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Ecards.aspx.cs" Inherits="com.paginar.johnson.Web.Ecards.Ecards" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderhead" runat="server">

		
          <!-- Demo CSS -->
	     
	      <link rel="stylesheet" href="http://flexslider.woothemes.com/css/flexslider.css" type="text/css" media="screen" />
          	

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.simplyCountable.js"></script>


<%--    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>--%>
    <script type="text/javascript" src="http://flexslider.woothemes.com/js/jquery.flexslider.js"></script>


    <style>
        .flex-direction-nav a
        {
            opacity: 1;
        }
        .flex-direction-nav .flex-next
        {
            right: 0;
        }
        .flex-direction-nav .flex-prev
        {
            left: 0;
        }
        .slides  li
        {
            color: #191919;
            list-style:none!important;
            background:none;
            font-size:10px;
            padding:0!important;
        }
        
    </style>
    <script type="text/javascript" >




         	    $(window).load(function () {
         	        $('.flexslider').flexslider({
         	            animation: "slide",
                        animationLoop: false,
                        itemMargin: 100,
         	        });
         	       
         	    });


                function next(index) {
         	        $find('<%=TabContainerprincipal.ClientID%>').set_activeTabIndex(index);

         	    }


         	    function nextandShow(index) {
         	        $find('<%=TabContainerprincipal.ClientID%>').set_activeTabIndex(index);

         	     
                 showCard();

         	    }


                function showCard()
                {
              
                 var img;

                   $(".slides li").each(function() {
                      if($(this).attr('class')=="flex-active-slide")
                       img =$("img", this).prop("src");
                   });

                   var from =    $("[id$=lblFrom]").val();
                   var text =   $("[id$=txtMessage]").val();
                   var to    =  $("[id$=txtTo]").val(); 

                   var html = "Para: "+to + "<br> <hr>"+  text + "<br/>" + "<img src='"+img+"' >" +"<br>"+"Te desea cordialmente,"+"<br>"+from;

                  
                    $("[id$=hdMessage]").val(html);
                    $("#divMessage").html(html);

                
                }



         	    $(document).ready(function () {


     


         	        $("[id$=txtMessage]").simplyCountable({
         	            counter: '#counter2',
         	            countType: 'characters',
         	            maxCount: 150,
         	            countDirection: 'down'
         	        });




         	        function getInput(e) {
         	            var length = $(e.target).val().length;
         	            if (length >= 150)
         	                $(e.target).val().substr(0, 149)
         	        }


         	    });


//         	    function ValidarCaracteres(textareaControl, maxlength) {
//         	        if (textareaControl.value.length > maxlength) {
//         	            textareaControl.value = textareaControl.value.substring(0, maxlength);
//         	        }
//         	    }


//         	    $(function () {
//         	        $("[id$=txtMessage]").keypress(function () {
//         	            var maxlen = 149;
//         	            if ($(this).val().length > maxlen) {
//         	                $(this).val().substr(0, 149)
//         	                return false;
//         	            }
//         	        })

//         	    });




		</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

		     

<BR /><BR /><BR /><BR />


    <asp:TabContainer ID="TabContainerprincipal" runat="server" ActiveTabIndex="0">
        <asp:TabPanel ID="TabPaso1" runat="server" Width="100%" HeaderText="Paso 1: Elija la tarjeta">
            <ContentTemplate>
            <div id="divPaso1">

                <div style="width: 680px">
                    <div class="flexslider">
                        <ul class="slides">
                            <asp:Literal ID="ltCards" runat="server"></asp:Literal>
                        </ul>
                    </div>
                </div>


                  <asp:Button Text="Cancelar" runat="server" ID="btnCancelar1"      onclick="btnCancelar1_Click" />
                  <asp:Button Text="Continuar" runat="server" ID="btnContinuar" OnClientClick="javascript:next(1); return false;" />


            </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPaso2" runat="server" Width="100%" HeaderText="Paso 2: Complétela">
            <ContentTemplate>
            <div id="divPaso2">
                  <asp:HiddenField ID="hdmailfrom" runat="server" />
                <asp:Label runat="server" ID="lblFrom"></asp:Label>
                <asp:Label runat="server" ID="Label1" Text="Para: "></asp:Label>
                <asp:TextBox runat="server" ID="txtTo" Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ServiceMethod="getusermails" MinimumPrefixLength="3" ServicePath="~/wsBuscador.asmx"
                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtTo"
                    ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                </asp:AutoCompleteExtender>
                <br />
                <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" Height="80" Width="400"></asp:TextBox>
                <p>
                    <span id="counter2"></span>caracteres disponibles.</p>
                </cc1:TextboxLimitExtender>
                <asp:Button Text="Cancelar" runat="server" ID="btnCancelar2"  onclick="btnCancelar1_Click"  />
                <asp:Button Text="Atrás" runat="server" ID="btnAtras" OnClientClick="javascript:next(0);return false;"  />
                <asp:Button Text="Continuar" runat="server" ID="btnContinuar2" OnClientClick="javascript:nextandShow(2);return false;" />
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabPaso3" runat="server" Width="100%" HeaderText="Paso 3: Vea como quedo y envíale">
            <ContentTemplate>
            <div id="divPaso3">
                <div id="divMessage"></div>
                <asp:HiddenField ID="hdMessage" runat="server" />
                <asp:Button Text="Cancelar" runat="server" ID="btnCancelar3"   onclick="btnCancelar1_Click"  />
                <asp:Button Text="Atrás" runat="server" ID="btnAtras2"  OnClientClick="javascript:nextandShow(1);return false;"  />
                <asp:Button Text="Enviar" runat="server" ID="btnEnviar" OnClick="btnEnviar_Click" />
             </div>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>



</asp:Content>
