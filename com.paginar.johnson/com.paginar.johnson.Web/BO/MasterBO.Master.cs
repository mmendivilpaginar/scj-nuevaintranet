﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.BO
{
    public partial class MasterBO : MasterBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.SettingUserLoginInUI();
        }

        private void SettingUserLoginInUI()
        {
            this.lblUserLogin.Text = this.ObjectUsuario.GetFullNameWithSeparator(" ");
        }
    }
}