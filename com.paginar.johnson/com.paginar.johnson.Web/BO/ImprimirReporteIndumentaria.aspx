﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirReporteIndumentaria.aspx.cs" Inherits="com.paginar.johnson.Web.BO.ImprimirReporteIndumentaria" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Reporte Ropa de Trabajo</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
</head>
<body>
    <form id="formImprEst" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Reporte Ropa de Trabajo</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
     
        <div id="noprint">

            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
            <br /><br />
        </div>
        <h3>
            <asp:Label ID="lblreporte" runat="server"></asp:Label> </h3>
        <div>
            <asp:Label ID="lblPeriodo" runat="server" Text=""></asp:Label> 
            <asp:Label ID="lblArea" runat="server" Text=""></asp:Label> 
            <asp:Label ID="lblBrigadista" runat="server" Text=""></asp:Label>             
            <br /><br />
        </div>
        <div style="border-style:solid; border-color:#CCCCCC; border-width:1px">
                <asp:MultiView ID="MVRopaTrabajo" runat="server">
                        <asp:View ID="indumentaria_con_detalle" runat="server">
<asp:GridView ID="GVPanel1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ODSPanel1">
            <Columns>
                <asp:BoundField DataField="UsuarioID" HeaderText="UsuarioID" 
                    SortExpression="UsuarioID" Visible="False" />
                <asp:BoundField DataField="Area" HeaderText="Área" SortExpression="Area" />
                <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                    SortExpression="Legajo" />
                <asp:BoundField DataField="NombreCompleto" HeaderText="Nombrey Apellido" 
                    ReadOnly="True" SortExpression="NombreCompleto" />
                <asp:BoundField DataField="Jefe" HeaderText="Jefe del Solicitante" 
                    ReadOnly="True" SortExpression="Jefe" />
                <asp:BoundField DataField="Zapatos" HeaderText="Zapatos" ReadOnly="True" 
                    SortExpression="Zapatos" />
                <asp:BoundField DataField="Pantalon" HeaderText="Pantalón" ReadOnly="True" 
                    SortExpression="Pantalon" />
                <asp:BoundField DataField="Torso" HeaderText="Torso" ReadOnly="True" 
                    SortExpression="Torso" />
                <asp:BoundField DataField ="Sexo" HeaderText="Sexo" ReadOnly="true" SortExpression="Sexo" />

                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                       <%-- <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Especial" HeaderText="Pedido Especial" 
                    SortExpression="Jefe" />
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel1" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteDetalleSelect" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoId" 
                    Type="Int32" />
                <asp:QueryStringParameter Name="Area" QueryStringField="Area" Type="String" />
                <asp:QueryStringParameter Name="esBrigadista" QueryStringField="esBrigadista" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
                        </asp:View>
        
                        <asp:View ID="indumentaria_area_sin_detalle" runat="server">
   <asp:GridView ID="GVPanel2" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ODSPanel2">
            <Columns>
                <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                    SortExpression="Area" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" 
                    SortExpression="Descripcion" />
                <asp:BoundField DataField="talle" HeaderText="Talle" SortExpression="talle" />
                <asp:BoundField DataField="Column1" HeaderText="Cantidad" ReadOnly="True" 
                    SortExpression="Column1" />
                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                        
<%--                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel2" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteAreaSinDetallesSelect" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                    Type="Int32" />
                <asp:QueryStringParameter Name="Area" QueryStringField="Area" Type="String" />
                <asp:QueryStringParameter Name="esBrigadista" QueryStringField="esBrigadista" 
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
                        </asp:View>

                        <asp:View ID="indumentaria_sin_detalle" runat="server">
       <asp:GridView ID="GVPanel3" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ODSPanel3">
            <Columns>
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                    SortExpression="Descripcion" />
                <asp:BoundField DataField="talle" HeaderText="Talle" SortExpression="talle" />
                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ReadOnly="True" 
                    SortExpression="Cantidad" />
                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                        <%--<asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel3" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteSinDetalle" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="esBrigadista" Name="PeriodoID" 
                    QueryStringField="PeriodoId" Type="Int32" />
                <asp:QueryStringParameter DefaultValue="" Name="esBrigadista" 
                    QueryStringField="esBrigadista" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
                        </asp:View>

                    </asp:MultiView>
        </div>
        </div>
    </form>
</body>
</html>
