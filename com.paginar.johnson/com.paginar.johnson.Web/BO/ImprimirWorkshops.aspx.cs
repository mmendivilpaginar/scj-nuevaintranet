﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Text;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.BO
{
    public partial class ImprimirWorkshops : System.Web.UI.Page
    {
        ControllerWorkShop CW = new ControllerWorkShop();
        ControllerWorkShop CW2 = new ControllerWorkShop();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExportarSuscritos_Click(object sender, EventArgs e)
        {
            AmarArchivoyExportar();
        }


        public void AmarArchivoyExportar()
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page page = new Page();
            HtmlForm form = new HtmlForm();
            Label TituloGral = new Label();

            TituloGral.Text = "<strong>Listado de Suscritos - " +  DateTime.Now.ToShortDateString() + "</strong><br />";
            page.EnableEventValidation = false;
            page.DesignerInitialize();
            page.Controls.Add(form);
            form.Controls.Add(TituloGral);

            DSWorkShop.ws_WorkshopDataTable DTCabeceras = CW.WorkshopDisponiblesActivos();
            foreach (DSWorkShop.ws_WorkshopRow RowCabecera in DTCabeceras.Rows)
            {
                DataTable DTListadoUsuarios = new DataTable();
                GridView GVListadoUsuarios = new GridView();
                Label LblTituloSeccion = new Label();
                LblTituloSeccion.Text = "<br /><strong>" + RowCabecera.Nombre.ToString() + "</strong>";
                DTListadoUsuarios = CW2.WorkshopGetInscriptos(int.Parse(RowCabecera.IdWorkshop.ToString()));
                DTListadoUsuarios.PrimaryKey = null;
                DTListadoUsuarios.Columns.RemoveAt(0);
                GVListadoUsuarios.DataSource = DTListadoUsuarios;
                GVListadoUsuarios.AllowPaging = false;
                GVListadoUsuarios.DataBind();
                GVListadoUsuarios.EnableViewState = false; 
                form.Controls.Add(LblTituloSeccion);
                form.Controls.Add(GVListadoUsuarios);
            }

            page.RenderControl(htw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename= Listado de Suscritos " + DateTime.Now.ToShortDateString() + ".xls");
            Response.Charset = "UTF-8";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();

        }
    }
}