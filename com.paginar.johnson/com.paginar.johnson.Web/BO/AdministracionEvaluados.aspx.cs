﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Data;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class AdministracionEvaluados : PageBase
    {
        private SincronizadorScj _SScj;
        public SincronizadorScj SScj
        {
            get
            {
                _SScj = (SincronizadorScj)this.Session["_SScj"];
                if (_SScj == null)
                {
                    _SScj = new SincronizadorScj();
                    this.Session["_SScj"] = _SScj;
                }
                return _SScj;
            }
            set
            {
                _SScj = value;

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ObjectUsuario.clusteridactual != 1)
                    HiddenFieldClusterID.Value = ObjectUsuario.clusteridactual.ToString();
                else
                    HiddenFieldClusterID.Value = "0";
                SScj.GetVwUsuarios();
                ButtonBuscar.Attributes.Add("onclick", "return VerificarCambios();");

            }
        }

        protected void ButtonBuscar_Click(object sender, EventArgs e)
        {
            //if (!EditandoGV(GridViewUsuarios))
            {
                LiteralJS.Text = "<script type=\"text/javascript\"> __modificado = 0;</script>";
                DropDownListTodosUsuarios.Enabled = true;
                ButtonAgregar.Enabled = true;
                GridViewUsuarios.EditIndex = -1;
                HiddenFieldTipoPeriodoID.Value = SScj.GetTipoPeriodoID(int.Parse(DropDownListTipoFormulario.SelectedValue)).ToString();
                GridViewUsuarios.Columns[5].Visible = (HiddenFieldTipoPeriodoID.Value == "1");
                bool carga = false;

                carga = ValidaPeriodo(int.Parse(DropDownListPeriodo.SelectedValue));
                VisualizacionControlesByPeriodo(carga);

                HiddenFieldMODO.Value = "BUSCAR";


                SScj.BuscarUsuarios(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
                SScj.GetVwUsuariosDisponibles(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value,int.Parse(HiddenFieldClusterID.Value));
                DropDownListTodosUsuarios.DataBind();
                DataView dv = new DataView(FachadaDA.Singleton.Pasos.GetDataByTipoFormularioIDPeriodoID(int.Parse(DropDownListTipoFormulario.SelectedValue), int.Parse(DropDownListPeriodo.SelectedValue)), "PasoID<>4 AND PasoID<>2 AND PasoID<>5", null, DataViewRowState.Unchanged);
                FormulariosDS.PasosDataTable PasosDt = new FormulariosDS.PasosDataTable();
                PasosDt.Merge(dv.ToTable());

                GridViewUsuarios.DataBind();

                MultiViewSincro.SetActiveView(ViewGrilla);
            }
            
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }

        private void VisualizacionControlesByPeriodo(bool Estado)
        {
            PanelAgregarUsuarios.Visible = Estado;
        }

        protected void ObjectDataSourceUsuarios_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;

        }
        protected void ObjectDataSourceUsuarios_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = SScj;
        }

        protected void ButtonAgregar_Click(object sender, EventArgs e)
        {
            //SScj.
            if (Page.IsValid)
            {
                try
                {
                    GuardarTempo();
                    int legajo = int.Parse(DropDownListTodosUsuarios.SelectedValue);
                    SScj.InsertarUsuariosModuloImportacion(legajo, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue));
                    GridViewUsuarios.DataBind();
                    SScj.GetVwUsuariosDisponibles(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
                    DropDownListTodosUsuarios.DataBind();
                    SetEditModeRowGridView(GridViewUsuarios, legajo.ToString());
                    
                    DropDownListTodosUsuarios.Enabled = false;
                    ButtonAgregar.Enabled = false;
                    this.MaintainScrollPositionOnPostBack = false;
                    //EjecutarScript("__modificado = 1;");
                    LiteralJS.Text = "<script type=\"text/javascript\"> __modificado = 1;</script>";

                }
                catch (Exception exc)
                {

                    throw;
                }
            }
            else Page.MaintainScrollPositionOnPostBack = true;

        }

        private void ScrollGrid(Control _editControl)
        {
            
            EjecutarScript("document.getElementById('" + _editControl.ClientID + "').focus();");
            
        }

        private bool EditandoGV(GridView Gv)
        {
            foreach (GridViewRow item in Gv.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    if (item.RowState == DataControlRowState.Edit)
                        return true;
                }

            }
            return false;
        }

        private void SetEditModeRowGridView(GridView Gv, string Valor)
        {
            foreach (GridViewRow item in Gv.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    Label LegajoLabel = item.FindControl("LegajoLabel") as Label;
                    if (LegajoLabel != null)
                        if (LegajoLabel.Text == Valor)
                        {
                            item.RowState = DataControlRowState.Edit;
                            Gv.EditIndex = item.RowIndex;
                            
                            Gv.DataBind();

                            TextBox c = Gv.Rows[item.RowIndex].FindControl("TextBoxApellido") as TextBox;
                            ImageButton edit = Gv.Rows[item.RowIndex].Cells[Gv.Columns.Count-2].Controls[0] as ImageButton;
                            ImageButton cancel = Gv.Rows[item.RowIndex].Cells[Gv.Columns.Count - 2].Controls[2] as ImageButton;
                            ImageButton Delete = Gv.Rows[item.RowIndex].Cells[Gv.Columns.Count - 1].Controls[1] as ImageButton;
                            if(Delete!=null)
                            Delete.Visible = false;
                            cancel.Visible = false;
                            ScrollGrid(c);

                            DisabledGrid(Gv, item.RowIndex);
                            //this.SetFocus(c);

                            return;
                        }
                }
            }
        }

        protected void DisabledGrid(GridView Gv, int index)
        {
            foreach (GridViewRow item in Gv.Rows)
            {
                if (item.RowType == DataControlRowType.DataRow)
                {
                    if ((item.RowState ==  DataControlRowState.Normal)||(item.RowState ==  DataControlRowState.Alternate))
                    {
                        ImageButton edit = Gv.Rows[item.RowIndex].Cells[Gv.Columns.Count - 2].Controls[0] as ImageButton;
                       
                        ImageButton Delete = Gv.Rows[item.RowIndex].Cells[Gv.Columns.Count - 1].Controls[1] as ImageButton;
                        if (Delete != null)
                            Delete.Enabled = false;                        
                        if (edit != null)
                            edit.Enabled = false;
                    }
                }

            }
            
        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        private void GuardarTempo()
        {
            int PeriodoID = int.Parse(DropDownListPeriodo.SelectedValue);
            int TipoFormularioID = int.Parse(DropDownListTipoFormulario.SelectedValue);
            foreach (GridViewRow FilaEvaluado in GridViewUsuarios.Rows)
            {
                if ((FilaEvaluado.Visible) && (FilaEvaluado.RowState == DataControlRowState.Edit))
                {
                    DropDownList DropDownListLT = FilaEvaluado.FindControl("DropDownListLT") as DropDownList;
                    DropDownList DropDownListJP = FilaEvaluado.FindControl("DropDownListJP") as DropDownList;
                    DropDownList DropDownListSectores = FilaEvaluado.FindControl("DropDownListSectores") as DropDownList;
                    DropDownList DropDownListCargos = FilaEvaluado.FindControl("DropDownListCargos") as DropDownList;
                    HiddenField HiddenFieldInterno = FilaEvaluado.FindControl("HiddenFieldInterno") as HiddenField;
                    HiddenField HiddenFieldFoto = FilaEvaluado.FindControl("HiddenFieldFoto") as HiddenField;
                    HiddenField HiddenFieldLegajo = FilaEvaluado.FindControl("HiddenFieldLegajo") as HiddenField;

                    HiddenField HiddenFieldNombre = FilaEvaluado.FindControl("HiddenFieldNombre") as HiddenField;
                    HiddenField HiddenFieldApellido = FilaEvaluado.FindControl("HiddenFieldApellido") as HiddenField;
                    SScj.AgregarEvaluado(int.Parse(HiddenFieldLegajo.Value), PeriodoID, TipoFormularioID, HiddenFieldNombre.Value, HiddenFieldApellido.Value, DropDownListSectores.SelectedItem.Text, int.Parse(DropDownListSectores.SelectedValue), DropDownListCargos.SelectedItem.Text, int.Parse(DropDownListCargos.SelectedValue), HiddenFieldFoto.Value, HiddenFieldInterno.Value, int.Parse(DropDownListLT.SelectedValue), int.Parse(DropDownListJP.SelectedValue));
                }
            }
        }

        protected void DropDownListPeriodo_DataBound(object sender, EventArgs e)
        {
            DropDownList DDL = ((DropDownList)sender);
            FormulariosController f = new FormulariosController();
            foreach (ListItem item in DDL.Items)
            {
                bool IsAbierto = ((bool)f.ValidarPeriodoCarga(int.Parse(item.Value), DateTime.Now)); //ValidaPeriodo(int.Parse(item.Value));
                item.Text += IsAbierto ? " (Vigente)" : " (Cerrado)";
            }
        }

        protected void GridViewUsuarios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool carga = false;
                if (ViewState["HabilitaPeriodo"] != null)
                    carga = (bool)ViewState["HabilitaPeriodo"];
                else
                    carga = ValidaPeriodo(int.Parse(DropDownListPeriodo.SelectedValue));

                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldCargoID = e.Row.FindControl("HiddenFieldCargoID") as HiddenField;
                HiddenField HiddenFieldEvaluador = e.Row.FindControl("HiddenFieldEvaluador") as HiddenField;
                HiddenField HiddenFieldAuditor = e.Row.FindControl("HiddenFieldAuditor") as HiddenField;
                ImageButton ImageButtonBorrar = e.Row.FindControl("ImageButtonBorrar") as ImageButton;
                ImageButton ImageButtonEditar = e.Row.Cells[8].Controls[0] as ImageButton;
                HiddenField HiddenFieldEstado = e.Row.FindControl("HiddenFieldEstado") as HiddenField;
                DropDownList DropDownListLT = e.Row.FindControl("DropDownListLT") as DropDownList;
                
                DropDownList DropDownListJP = e.Row.FindControl("DropDownListJP") as DropDownList;
                if (DropDownListJP != null)
                    DropDownListJP.Enabled = false;
                DropDownList DropDownListSectores = e.Row.FindControl("DropDownListSectores") as DropDownList;
                
                DropDownList DropDownListCargos = e.Row.FindControl("DropDownListCargos") as DropDownList;
               
                if (ImageButtonBorrar != null)
                    ImageButtonBorrar.Visible = false;

                int EstadoActualID = int.Parse(HiddenFieldEstado.Value.Trim());

                if (EstadoActualID != 4)
                    if (DropDownListLT != null)
                        DropDownListLT.Enabled = true;
                if (EstadoActualID < 3)
                    if (DropDownListJP != null)
                        DropDownListJP.Enabled = true;

                if (ImageButtonEditar!=null)
                    ImageButtonEditar.Visible = ((EstadoActualID != 4) && (EstadoActualID != 6) && (EstadoActualID != 5));

                if (DropDownListLT != null)
                {
                    ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
                    DSUsuarios.UsuarioDataTable Evaluado = new DSUsuarios.UsuarioDataTable();

                    int Cluster = 0;

                    Evaluado = ControllerUsuarios1.GetUsuariosByLegajo(int.Parse(HiddenFieldLegajo.Value));
                    //SelectedValue='<%# SetEvaluador(Eval("LT")) %>'

                    foreach (DSUsuarios.UsuarioRow EvaluadoRow in Evaluado.Rows)
                    {
                        Cluster = EvaluadoRow.Cluster;

                    }

                    //Parameter ObjectDataSourceUsuariosEvaluadores1ParameterCLuster = new Parameter("ClusterID", DbType.Int32, Cluster.ToString());
                    //ObjectDataSourceUsuariosEvaluadores1.SelectParameters["ClusterID"] = ObjectDataSourceUsuariosEvaluadores1ParameterCLuster;

                    Parameter ObjectDataSourceUsuariosEvaluadores1ParameterLegajo = new Parameter("Legajo", DbType.Int32, HiddenFieldLegajo.Value);
                    ObjectDataSourceUsuariosEvaluadores1.SelectParameters["Legajo"] = ObjectDataSourceUsuariosEvaluadores1ParameterLegajo;


                    ObjectDataSourceUsuariosEvaluadores1.DataBind();
                    DropDownListLT.DataSourceID = "ObjectDataSourceUsuariosEvaluadores1";
                    DropDownListLT.DataBind();

                    if (DropDownListLT.Items.FindByValue(HiddenFieldEvaluador.Value) != null)
                        DropDownListLT.SelectedValue = HiddenFieldEvaluador.Value;
                    else
                        DropDownListLT.SelectedValue = "";

                    if (DropDownListCargos.Items.FindByValue(HiddenFieldCargoID.Value) != null)
                        DropDownListCargos.SelectedValue = HiddenFieldCargoID.Value;
                    else
                        DropDownListCargos.SelectedValue = "";

                   // DropDownListLT.Enabled = false;    
                    
                }

                if (DropDownListJP != null)
                {
                    ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
                    DSUsuarios.UsuarioDataTable Evaluado = new DSUsuarios.UsuarioDataTable();

                    int Cluster = 0;

                    Evaluado = ControllerUsuarios1.GetUsuariosByLegajo(int.Parse(HiddenFieldLegajo.Value));
                    
                    if(Evaluado.Rows.Count>0)
                        Cluster = Evaluado[0].Cluster;
                                       
                    DropDownListJP.DataSource = SScj.getUsuariosAuditores(int.Parse(HiddenFieldLegajo.Value));
                    DropDownListJP.DataBind();

                    if (DropDownListJP.Items.FindByValue(HiddenFieldAuditor.Value) != null)
                        DropDownListJP.SelectedValue = HiddenFieldAuditor.Value;
                    else
                        DropDownListJP.SelectedValue = "";                    

                }


                if (HiddenFieldEstado.Value.Trim() == "-1")
                {
                    if (DropDownListLT!=null)
                        DropDownListLT.Enabled = true;
                    if (DropDownListJP != null)
                        DropDownListJP.Enabled = true;
                    if (DropDownListSectores != null)
                        DropDownListSectores.Enabled = true;
                    if (DropDownListCargos != null)
                        DropDownListCargos.Enabled = true;

                    ImageButtonBorrar.Visible = true;
                                   
                }
                
                if (!carga)
                {
                    if (DropDownListLT != null)
                        DropDownListLT.Enabled = false;
                    if (DropDownListJP != null)
                        DropDownListJP.Enabled = false;
                    if (DropDownListSectores != null)
                        DropDownListSectores.Enabled = false;
                    if (DropDownListCargos != null)
                        DropDownListCargos.Enabled = false;
                    if (ImageButtonEditar != null)
                        ImageButtonEditar.Visible = false;

                }
            }
        }

        protected void GridViewUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.MaintainScrollPositionOnPostBack = true;
            if (e.CommandName == "Eliminar")
            {
               
                int PeriodoID = int.Parse(DropDownListPeriodo.SelectedValue);
                int TipoFormularioID = int.Parse(DropDownListTipoFormulario.SelectedValue);
                SScj.DeleteUsuariosModuloImportacion(int.Parse(e.CommandArgument.ToString()), PeriodoID, TipoFormularioID);

                GridViewRow GvR = (e.CommandSource as ImageButton).Parent.Parent as GridViewRow;
                GvR.Visible = false;
                SScj.GetVwUsuariosDisponibles(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
                DropDownListTodosUsuarios.DataBind();
                
            }

            if (e.CommandName == "Edit")
            {
                EjecutarScript("__modificado = 1;");
                DropDownListTodosUsuarios.Enabled = false;
                ButtonAgregar.Enabled = false;
            }


            if (e.CommandName == "Update")
            {
                DropDownListTodosUsuarios.Enabled = true;
                ButtonAgregar.Enabled = true;
                int PeriodoID = int.Parse(DropDownListPeriodo.SelectedValue);
                int TipoFormularioID = int.Parse(DropDownListTipoFormulario.SelectedValue);
                int Legajo =  int.Parse((GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("LegajoLabel") as Label).Text);
                string Nombre = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("TextBoxNombre") as TextBox).Text;
                string Apellido = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("TextBoxApellido") as TextBox).Text;
                DropDownList DropDownListSectores = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("DropDownListSectores") as DropDownList);
                DropDownList DropDownListCargos = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("DropDownListCargos") as DropDownList);
                HiddenField HiddenFieldInterno = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldInterno") as HiddenField);
                HiddenField HiddenFieldFoto = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldFoto") as HiddenField);
                DropDownList DropDownListLT = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("DropDownListLT") as DropDownList);
                DropDownList DropDownListJP = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("DropDownListJP") as DropDownList);



                HiddenField HiddenFieldJobGrade = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldJobGrade") as HiddenField);
                HiddenField HiddenFieldLinea = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldLinea") as HiddenField);
                HiddenField HiddenFieldDireccion = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldDireccion") as HiddenField);
                HiddenField HiddenFieldDireccionID = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldDireccionID") as HiddenField);
                HiddenField HiddenFieldLegajoReal = (GridViewUsuarios.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("HiddenFieldLegajoReal") as HiddenField);

                int? LegajoJP = (DropDownListJP.SelectedIndex == 0) ? null : int.Parse(DropDownListJP.SelectedValue) as int?;
                int? DireccionID = (string.IsNullOrEmpty(HiddenFieldDireccionID.Value)) ? null : int.Parse(HiddenFieldDireccionID.Value) as int?;
                SScj.ActualizarEvaluado(Legajo, PeriodoID, TipoFormularioID, Nombre, Apellido, DropDownListSectores.SelectedItem.Text, int.Parse(DropDownListSectores.SelectedValue), DropDownListCargos.SelectedItem.Text, int.Parse(DropDownListCargos.SelectedValue), HiddenFieldFoto.Value, HiddenFieldInterno.Value, int.Parse(DropDownListLT.SelectedValue), LegajoJP, HiddenFieldJobGrade.Value, HiddenFieldDireccion.Value, DireccionID);
                ButtonBuscar_Click(ButtonBuscar, null);
                EjecutarScript("__modificado = 0;");
            }

            if (e.CommandName == "Cancel")
            {
                DropDownListTodosUsuarios.Enabled = true;
                ButtonAgregar.Enabled = true;
            }
        }

        protected void GridViewUsuarios_DataBound(object sender, EventArgs e)
        {
            bool carga = false;
            if (ViewState["HabilitaPeriodo"] != null)
                carga = (bool)ViewState["HabilitaPeriodo"];
            else
                carga = ValidaPeriodo(int.Parse(DropDownListPeriodo.SelectedValue));
            PanelAgregarUsuarios.Visible = carga;
            
            PanelAgregarUsuarios.Visible = true;
            if (!carga)
            {
                
                PanelAgregarUsuarios.Visible = false;
            }
            GridViewUsuarios.Columns[GridViewUsuarios.Columns.Count - 1].Visible = carga;
            GridViewUsuarios.Columns[GridViewUsuarios.Columns.Count - 2].Visible = carga;



        }

        protected void ButtonSINC_Click(object sender, EventArgs e)
        {
            HiddenFieldMODO.Value = "SINC";
            SScj.BuscarUsuarios(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
            GridViewUsuarios.DataBind();
            SScj.GetVwUsuariosDisponibles(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
            DropDownListTodosUsuarios.DataBind();
        }

        protected void ButtonCancelar_Click(object sender, EventArgs e)
        {
            SScj.BuscarUsuarios(int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListTipoFormulario.SelectedValue), HiddenFieldMODO.Value, int.Parse(HiddenFieldClusterID.Value));
            GridViewUsuarios.DataBind();
        }

        protected void ButtonGrabar_Click(object sender, EventArgs e)
        {
            //SincronizadorScj S = new SincronizadorScj(); 
            int TipoFormularioID = int.Parse(DropDownListTipoFormulario.SelectedValue);
            try
            {
                int PeriodoID = int.Parse(DropDownListPeriodo.SelectedValue);
                TipoFormularioID = int.Parse(DropDownListTipoFormulario.SelectedValue);

                GuardarTempo();

                if (HiddenFieldMODO.Value == "BUSCAR")
                    SScj.GrabarCambios();
                if (HiddenFieldMODO.Value == "SINC")
                    SScj.SincronizarCambios(PeriodoID, TipoFormularioID);
                //omb.ShowMessage("La operación fue realizada exitosamente.");
                LabelResultado.Text = "La operación fue realizada exitosamente.";
            }
            catch (Exception exc)
            {
                LabelResultado.Text = "Ha ocurrido un error. Por favor intenta nuevamente.<br>" + exc.InnerException;
                //omb.ShowMessage("Ha ocurrido un error. Por favor intenta nuevamente.<br>"+ exc.InnerException);
            }
            MultiViewSincro.SetActiveView(ViewResultado);


            string refresh = string.Format("<META HTTP-EQUIV=\"refresh\" content=\"10;URL={0}?TipoFormularioID={1}\">", Request.Url.GetLeftPart(UriPartial.Path), TipoFormularioID);
            LabelRefresh.Text = refresh;
            //Timer1.Enabled = true;
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //MultiViewSincro.SetActiveView(ViewGrilla);

            Timer1.Enabled = false;
            MultiViewSincro.SetActiveView(ViewGrilla);
        }

        protected void ObjectDataSourceEvaluados_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }
        protected void ObjectDataSourceEvaluados_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = SScj;
        }

        protected void ObjectDataSourceUsuariosEvaluadores_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }
        protected void ObjectDataSourceUsuariosEvaluadores_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = SScj;
        }

        protected void GridViewUsuarios_DataBinding(object sender, EventArgs e)
        {
           
        }

        protected void DropDownListTipoFormulario_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void ObjectDataSourceUsuarios_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.Cancel = true;
        }

        protected void GridViewUsuarios_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ////Normal turning to edit
            //GridViewUsuarios.EditIndex = e.NewEditIndex;
            ////BindGrid();

            ////Set the focus to control on the edited row
            //GridViewUsuarios.Rows[e.NewEditIndex].FindControl("TextBoxApellido").Focus();
            this.MaintainScrollPositionOnPostBack = true;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            foreach (GridViewRow item in GridViewUsuarios.Rows)
            {
                if (item.RowState == DataControlRowState.Edit)
                {
                    args.IsValid = false;
                    return;
                }
            }
        }

        protected void DropDownListPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectDataSourcePeriodo.DataBind();
            FormView1.DataBind();
        }

        protected void DropDownListTipoPeriodo_SelectedIndexChanged(object sender, EventArgs e)
        {

            ObjectDataSourcePeriodos.DataBind();
            DropDownListPeriodo.DataBind();
            ObjectDataSourcePeriodo.DataBind();
            FormView1.DataBind();
        }

        protected string SetEvaluador(object SelectValue)
        {
            
            if ((SelectValue == System.DBNull.Value) || (SelectValue == null) || (SelectValue.ToString()=="-1"))
                return "-1";
                
           

           
            ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable Evaluador = new DSUsuarios.UsuarioDataTable();

            string Estado= string.Empty;

            Evaluador = ControllerUsuarios1.GetUsuariosByLegajo(int.Parse(SelectValue.ToString()));

            foreach (DSUsuarios.UsuarioRow EvaluadorRow in Evaluador.Rows)
            {
                Estado = EvaluadorRow.Estado.ToString();
                
            }


            int EstadoEvaluador = int.Parse(Estado);
            if (EstadoEvaluador != 2)
                return "-1";
            
            else    
                return SelectValue.ToString();
           }

        

        
    }
}