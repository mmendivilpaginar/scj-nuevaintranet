﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="NewsLetterABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.NewsLetterABM" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
     .hidden {display:none}
</style>

<link rel="stylesheet" href="../css/jquery.Jcrop.css" type="text/css" />
<script src="../js/jquery.Jcrop.js"></script>

<script language="javascript">
    $(document).ready(function () {
        // Uncheck each checkbox on body load
        $('#all_noticias .selectit').each(function () { this.checked = false; });
        $('#sel_noticias .selectit').each(function () { this.checked = false; });

        $('#all_noticias .selectit').click(function () {
            var newsid = $(this).val();
            $('#news' + newsid).toggleClass('innertxt_bg');
        });

        $('#sel_noticias .selectit').click(function () {
            var newsid = $(this).val();
            $('#news' + newsid).toggleClass('innertxt_bg');
        });


        //valida tamaño de la imagen

        function ValidaTamanoImagen() {

        }


        $("#move_right").click(function () {

            var news = $('#sel_noticias .innertxt2').size();
            var sel_noticias = $('#all_noticias .innertxt_bg').size();

            $('#all_noticias .innertxt_bg').each(function () {
                var news_id = $(this).attr('noticiaid');

                var news_clone = $(this).clone(true);
                $(news_clone).removeClass('innertxt');
                $(news_clone).removeClass('innertxt_bg');
                $(news_clone).addClass('innertxt2');

                $(news_clone).children("[id*='Categoria_" + news_id + "']").addClass("hidden");
                $(news_clone).children("[id*='Categoria_" + news_id + "']").addClass("hidden");
                $(news_clone).children("[id*='select" + news_id + "']").attr('checked', false);


                var nodo = $(this);



                $('#select' + news_id).each(function () {
                    //this.checked = false;
                    if (this.checked) {

                        //Valido noticia privada
                        var privada = $("[id*='Privada_" + news_id + "']").val();
                        var privacidad = $("[id$=hdPrivada]").val();
                        var agrega;



                        if (privacidad == "") {
                            $("[id$=hdPrivada]").val(privada);
                            agrega = true;
                        }
                        else {

                            if (privada == privacidad) {
                                agrega = true;
                            }
                            else {
                                agrega = false;
                                alert("Las noticias deben ser del mismo tipo");
                            }
                        }

                        //Crop Image 
                        var flyer = $("[id*='Flyer_" + news_id + "']").val();
                        var path = $("[id*='Path_" + news_id + "']").val();
                        var categoria = $("[id*='hdCategoria_" + news_id + "']").val();




                        if (flyer == "1" ||
                             (
                              path !=""
                              && (categoria == "Novedades" || categoria == "Vivi SCJ" || categoria == "Avisos" || categoria == "Acordate")
                              )//path
                         ) //if
                        {

                            
                            $('#divCrop').html("<img src='../noticias/Imagenes/" + path + "' id='target' alt='Flyer' />");



                            $("[id*='hdPath']").val(path);

                            $("[id*='hdInfoID']").val(news_id);
                            agrega = false; //agrego luego del postback, cuando recorte

                            LoadCrop();
                            ShowCrop(flyer);
                        }

                        //


                        if (agrega) {

                            $('#sel_noticias').append(news_clone);
                            var ids = $("[id$=hdNoticiasIds]").val();

                            if (ids != '') ids += ',';
                            ids = ids + news_id;

                            $("[id$=hdNoticiasIds]").val(ids);

                            nodo.remove();
                        }
                    }
                });

            });
        });


        $("#move_left").click(function () {


            $('#sel_noticias .innertxt2').each(function () {
                var news_id = $(this).attr('noticiaid');


                var news_clone = $(this).clone(true);
                $(news_clone).removeClass('innertxt');
                $(news_clone).removeClass('innertxt_bg');
                $(news_clone).addClass('innertxt2');
                $(news_clone).children("[id*='Categoria_" + news_id + "']").removeClass("hidden");
                $(news_clone).children("[id*='CropImage']").addClass("hidden");
                var nodo = $(this);


                $('#select' + news_id).each(function () {
                    if (this.checked) {
                        $(news_clone).find('input').attr('checked', false);

                        $(news_clone).removeClass('innertxt2');
                        //$(news_clone).removeClass('innertxt_bg');
                        $(news_clone).addClass('innertxt_bg');
                        $('#all_noticias').append(news_clone);

                        var ids = $("[id$=hdNoticiasIds]").val();


                        if (ids.indexOf(news_id + ',') >= 0) { ids = ids.replace(news_id + ',', ''); }
                        if (ids.indexOf(',' + news_id) >= 0) { ids = ids.replace(',' + news_id, ''); }
                        if (ids.indexOf(news_id) >= 0) { ids = ids.replace(news_id, ''); }

                        $("[id$=hdNoticiasIds]").val(ids);



                        nodo.remove();

                        if ($("[id$=hdNoticiasIds]").val() == "") {

                            $("[id$=hdPrivada]").val("");

                        }


                    }
                });


            });
        });


    });

    function something() {
        var fObj = new Date();
        var horas = fObj.getHours();
        var minutos = fObj.getMinutes();
        var segundos = fObj.getSeconds();
    }
     

 
</script>

<%--Crop Image--%>
<script type="text/javascript">

    function ValidaCrop(source, arguments) {
       var w   =  $('[id*=hdw]').val();
       var h = $('[id*=hdh]').val();

       
       arguments.IsValid = false;

       if (w == 155 && h == 65) {
           arguments.IsValid = true;
       }

       if (w == 220 && h == 92) {
           arguments.IsValid = true;
       }

       if (w == 400 && h == 168) {
           arguments.IsValid = true;
       }

       if (w == 800 && h == 335) {
           arguments.IsValid = true;
       }

       if (w == 1024 && h == 429) {
           arguments.IsValid = true;
       }

       if (!arguments.IsValid)
           alert("Las medidas no coinciden con las sugeridas. Verifique");
    }


    function LoadCropEdit(InfoID, Path) {


        var path = $("[id*='Path_" + InfoID + "']").val();
        $('#divCrop').html("<img src='../noticias/Imagenes/" + Path + "' id='target' alt='Flyer' />");

        $("[id*='hdPath']").val(path);

        $("[id*='hdInfoID']").val(InfoID);
        
        LoadCrop();
        ShowCrop("0");
    }



    function ShowCrop(flyer) {
        $('#divCropImage').show();
        var id = $('#divCropImage');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('#maskCrop').css({ 'width': maskWidth, 'height': maskHeight });
        $('#maskCrop').fadeIn(1000); 
        $('#maskCrop').fadeTo('slow', 0.8);
        var winH = $(window).height();
        var winW = $(window).width();
        //$(id).css('top', winH / 2 - $(id).height() / 2);
        $(id).css('top', 0);
        $(id).css('left', winW / 2 - $(id).width() / 2);
        $(id).fadeIn(1000);

        if(flyer=="1")
            $('[id$=btnMantener]').attr('disabled', 'true');


    }


    function  LoadCrop(){

        var jcrop_api;

        $('#target').Jcrop({
            onChange: showCoords,
            onSelect: showCoords,
            onRelease: clearCoords,
            setSelect: [0, 0, 400, 168]
            
        }, function () {
            jcrop_api = this;
        });

        $('#coords').on('change', 'input', function (e) {
            var x1 = $('[id$=hdx1]').val(),
          x2 = $('[id$=hdx2]').val(),
          y1 = $('[id$=hdy1]').val(),
          y2 = $('[id$=hdy2]').val();
            jcrop_api.setSelect([x1, y1, x2, y2]);
        });

    }

    // Simple event handler, called from onChange and onSelect
    // event handlers, as per the Jcrop invocation above
    function showCoords(c) {
        $('[id$=hdx1]').val(c.x);
        $('[id$=hdy1]').val(c.y);
        $('[id$=hdx2]').val(c.x2);
        $('[id$=hdy2]').val(c.y2);
        $('[id*=hdw]').val(c.w);
        $('[id*=hdh]').val(c.h);
       
    };

    function clearCoords() {
        $('#coords input').val('');
    };



</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
<div class="newFormStyle">
    <h2>
        <asp:Label ID="lblModo" runat="server" Text="Nuevo"></asp:Label> Newsletter
    </h2>

     <br />

        <asp:MultiView ActiveViewIndex="0" runat="server" ID="Vista">
            <asp:View runat="server" ID="Datos">
                <div>

                        <asp:ValidationSummary ValidationGroup="Grabar" DisplayMode="BulletList" ID="ValidationSummary1"
                            runat="server" />
                    <div class="leftHalf">
                        <div class="form-item">
                            <label>Estado</label>
                            <asp:DropDownList ID="drpEstado" runat="server" Width="100px">
                                <asp:ListItem Value="-1">Seleccionar</asp:ListItem>
                                <asp:ListItem Value="1">Activo</asp:ListItem>
                                <asp:ListItem Value="0">Inactivo</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="cpvEstado" runat="server" ControlToValidate="drpEstado"
                                ErrorMessage="Seleccione un Estado" Operator="NotEqual" ValidationGroup="Grabar"
                                ValueToCompare="-1">*</asp:CompareValidator>
                        </div>
                        <div class="form-item">
                            <label>Estado de Envío</label>
                            <asp:DropDownList ID="drpEnvio" runat="server" Width="100px">
                                <asp:ListItem Value="-1">Seleccionar</asp:ListItem>
                                <asp:ListItem Value="1">Enviado</asp:ListItem>
                                <asp:ListItem Value="0">No Enviado</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="cpvEnvio" runat="server" ControlToValidate="drpEnvio" ErrorMessage="Seleccione un Estado"
                                Operator="NotEqual" ValidationGroup="Grabar" ValueToCompare="-1">*</asp:CompareValidator>
                        </div>
                        <div class="form-item">
                            <label>Nombre</label>
                            <asp:TextBox ID="txtTitulo" runat="server" ReadOnly="True" Width="200px"></asp:TextBox>
                        </div>
                        <div class="form-item">
                            <label>Fecha de Envío</label>
                            <asp:TextBox ID="FechaTextBox" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                            <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                            <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                                PopupButtonID="IMGCalen1" TargetControlID="FechaTextBox" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="FechaTextBox"
                                ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                                Operator="DataTypeCheck" Type="Date" ValidationGroup="Agregar" Display="Dynamic">*</asp:CompareValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"
                                ValidationGroup="Buscar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                                ControlToValidate="FechaTextBox" SetFocusOnError="True">*</asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfFecha" runat="server" ErrorMessage="Ingrese una Fecha"
                                ControlToValidate="FechaTextBox" ValidationGroup="Grabar">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="rightHalf">
                        <div class="form-item">
                            <label>Destinatarios</label>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                        
                        

                            <asp:TextBox ID="txtDestinatarios" runat="server" Width="300px"></asp:TextBox>
                            <asp:ImageButton ID="imgAgregarDestinatario" runat="server" ImageUrl="~/images/verMas.png"
                                OnClick="imgAgregarDestinatario_Click" ValidationGroup="Destinatarios" 
                                    ToolTip="Agregar mail" />

                                
                            <asp:AutoCompleteExtender ID="txtDestinatarios_AutoCompleteExtender" runat="server"
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="2" ServiceMethod="getDestinatarios"
                                EnableCaching="true" UseContextKey="True" ServicePath="~/wsNewsletter.asmx" TargetControlID="txtDestinatarios"
                                CompletionInterval="200">
                            </asp:AutoCompleteExtender>

                            <label class="error">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDestinatarios"
                                    ErrorMessage="Ingrese un Destinatario" ValidationGroup="Destinatarios">*</asp:RequiredFieldValidator>
                            </label>
                            <label class="error">
                                <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="txtDestinatarios" ErrorMessage="Email inválido" ValidationGroup="Destinatarios">
                                </asp:RegularExpressionValidator>
                            </label>
                             <label class="error">
                                <asp:CustomValidator ID="cvExisteDestinatario" runat="server" ErrorMessage="El destinatario ya se encuentra en la lista"
                                OnServerValidate="cvExisteDestinatario_ServerValidate" ValidateEmptyText="True" ValidationGroup="Destinatarios"></asp:CustomValidator>
                             </label>
                        
                        <div id="agregarDestinarios">    
                            <div id="lblInfo">Ingresar mail y click en el bóton agregar(+)"</div>
                        </div>
                            
                            <asp:GridView ID="gvDestinatarios" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                OnRowCommand="gvDestinatarios_RowCommand" Width="50px" GridLines="None" CssClass="mt-10">
                                <Columns>
                                    <asp:BoundField DataField="email" />
                                    <asp:TemplateField  >
                                        <ItemTemplate  >
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png"
                                                CommandName="Quitar" CommandArgument='<%# Eval("email") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </ContentTemplate>

                            </asp:UpdatePanel>

                            <asp:CustomValidator ID="cvDestinatarios" runat="server" ErrorMessage="Agregue Destinatarios"
                                OnServerValidate="cvDestinatarios_ServerValidate" ValidateEmptyText="True" ValidationGroup="Grabar"></asp:CustomValidator>
                            <asp:HiddenField ID="HiddenField1" runat="server" />


                        </div>
                    </div>                   
                    <div class="form-item clear">

                        <table>
                            <tr>
                                <td valign="top">
                                    <label>Últimas Noticias cargadas</label>
                                    <div id="divNoticias" class="greyBox">
                                        <asp:Repeater ID="rptNoticias" runat="server" DataSourceID="odsNoticias">
                                            <HeaderTemplate>
                                                <div id="all_noticias">
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <div id='<%# "noticia"+Eval("InfoID") %>' noticiaid='<%# Eval("InfoID") %>' class="innertxt_bg">
                                                    <input type="checkbox" id='<%# "select"+Eval("InfoID") %>' value='<%# Eval("InfoID") %>' class="selectit">
                                                    
                                                    <span  id='<%# "Categoria"+"_"+Eval("InfoID") %>'> <%# Eval("CategoriaDescripcion")+" | "  %></span>  

                                                    <asp:Label Font-Italic="true" runat="server" ID="LiteralTitulo" Text='<%#  Eval("Titulo")%>'></asp:Label>
                                               
                                                    <input type="hidden" name="opcion" id='<%# "hdCategoria"+"_"+Eval("InfoID") %>'   value='<%# Eval("CategoriaDescripcion") %>'>
                                                    <input type="hidden" name="opcion" id='<%# "Privada"+"_"+Eval("InfoID") %>'  value='<%# Eval("Privacidad") %>'>
                                                    <input type="hidden" name="opcion" id='<%# "Path"+"_"+Eval("InfoID") %>'  value='<%# Eval("Path") %>'>
                                                    <input type="hidden" name="opcion" id='<%# "Flyer"+"_"+Eval("InfoID") %>'  value='<%# Eval("Flyer") %>'>

                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>

                                <td valign="middle" class="agregarQuitarNoticias">
                                    <div class="alignCenter">
                                        <a href="javascript:void(0);" id="move_right" class="form-submit">&#62;&#62;</a><br /><br />
                                        <a href="javascript:void(0);" id="move_left" class="form-submit">&#60;&#60;</a>
                                    </div>
                                </td>

                                <td valign="top">
                                   <label>Noticias que conforman el Newsletter</label>
                                    <asp:HiddenField id="hdPrivada" runat="server" />
                                   <div id="sel_noticias" class="greyBox">
                                  
                                   
                                    
                                    <asp:Repeater ID="rpNoticasSel" runat="server">
                              
                                            <ItemTemplate>
                                                <div id='<%# "noticia"+Eval("InfoID") %>' noticiaid='<%# Eval("InfoID") %>' class="innertxt2">
                                                 <input type="checkbox" id='<%# "select"+Eval("InfoID") %>' value='<%# Eval("InfoID") %>'  class="selectit">
                                                 <span id='<%# "Categoria"+"_"+Eval("InfoID") %>' class="hidden" > <%# Eval("CategoriaDescripcion")+" | "  %></span>  
                                                 <asp:Label Font-Italic="true" runat="server" ID="LiteralTitulo" Text='<%# Eval("Titulo")%>'></asp:Label>
                                                 <input type="hidden" name="opcion" id='<%# "Privada"+"_"+Eval("InfoID") %>'  value='<%# Eval("Privacidad") %>'>
                                                 <input type="hidden" name="opcion" id='<%# "Path"+"_"+Eval("InfoID") %>'  value='<%# Eval("Path") %>'>
                                                 <input type="hidden" name="opcion" id='<%# "Flyer"+"_"+Eval("InfoID") %>'  value='<%# Eval("Flyer") %>'>

                                                 <asp:ImageButton ID="CropImage"
                                                  ImageUrl="~/images/crop.png"  runat="server"   ToolTip="Recortar Imagen"
                                                  OnClientClick='<%# "LoadCropEdit(" + Eval("InfoID") + "," + "&#8217;" + Eval("Path")  + "&#8217;" + ");  return false;" %>' 
                                                   Visible='<%# Boolean.Parse(  ( Eval("Path").ToString()!="")  && ( Eval("CategoriaDescripcion").ToString()  == "Novedades" || Eval("CategoriaDescripcion").ToString()   == "Vivi SCJ" || Eval("CategoriaDescripcion").ToString()   == "Avisos" || Eval("CategoriaDescripcion").ToString()   == "Acordate" )   ?"True":"False" )      %>' />
                                                 
                                                </div>
                                            </ItemTemplate>
                                    </asp:Repeater>
                                    
                                     </div>

                                </td> 
                                </tr> 
                                </table>


                     <div class="messages msg-info">
                     <asp:Label runat="server" ID="lblinfo1"   Font-Bold="true" Text="Cantidad Máxima de noticias por Categoría" ></asp:Label>
                     <br />
                     Novedades: 4 noticias, A family Company: 3 noticias, Vivi SCJ: 2 noticias, Acordate: 4 noticias
                     <br />
                     Equipo SCJ/Avisos: sin límite
                    </div>
                    
                    </div>
               


                        <asp:CustomValidator ID="cvNoticias" runat="server" ErrorMessage="Seleccione Noticias"
                            OnServerValidate="cvNoticias_ServerValidate" ValidateEmptyText="True" ValidationGroup="Grabar"></asp:CustomValidator>
                        <asp:HiddenField ID="hdNewsletterID" runat="server" />
                        <asp:HiddenField ID="hdNoticiasIds" runat="server" />
                        <asp:HiddenField ID="hdNoticiasIdsFilter" runat="server" />
                        <asp:HiddenField ID="hdClusterID" runat="server" />
                        <asp:ObjectDataSource ID="odsNoticias" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="getNoticias" TypeName="com.paginar.johnson.BL.ControllerNewsletter">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="30" Name="top" Type="String" />
                                <asp:ControlParameter ControlID="hdClusterID" DefaultValue="" Name="clusterid" 
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="hdNoticiasIdsFilter" Name="filter" 
                                    PropertyName="Value" Type="String" />
                                <asp:Parameter DefaultValue="" Name="ids" Type="String" />
                                <asp:Parameter DefaultValue="" Name="newsletterid" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    <div class="form-item">
                        <asp:Button ID="btnCrear" runat="server" Text="Crear" OnClick="btnCrear_Click" ValidationGroup="Grabar" class="form-submit"/>
                        <asp:Button ID="btnModificar" runat="server" Text="Modificar" OnClick="btnModificar_Click" ValidationGroup="Grabar"  Visible="false" class="form-submit"/>
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onclick="btnCancelar_Click" class="form-submit"/>
                        <asp:Button ID="btnVistaPrevia" runat="server" Text="Vista Previa"   ValidationGroup="Preview" onclick="btnVistaPrevia_Click" class="form-submit"/>
                    </div>

                    <asp:CustomValidator ID="cvCantidadNew" runat="server" ErrorMessage="Verifique la cantidad de noticias por Categoría"  
                                OnServerValidate="cvCantidadNew_ServerValidate" ValidateEmptyText="True" ValidationGroup="Preview"></asp:CustomValidator>
                </div>

                    <asp:Panel runat="server" ID="pnlCrop">
                        <div id="maskCrop"></div>
                        <div id="boxes" class="preview">
                            <div id="divCropImage" class="window" style="padding-top:20px;display:none">
                                <div class="inline-labels">
                                <div id="imageCropinvisible" style="display:none">

                                </div>

                                <div class="messages msg-alerta">
                                    <label>Recuerde usar las siguientes medidas: 155x65 | 220x92 | 400x168 | 800x335 | 1024x429</label>
                                </div>

                                <asp:HiddenField ID="hdx1" runat="server" />
                                <asp:HiddenField ID="hdx2" runat="server" />
                                <asp:HiddenField ID="hdy1" runat="server" />
                                <asp:HiddenField ID="hdy2" runat="server" />                   
                                
                                <div class="newFormStyle">
                                    <div class="form-item">
                                        <label>Ancho</label>   
                                        <asp:TextBox ID="hdw"  runat="server"  />
                                    </div>
                                    <div class="form-item">
                                        <label>Alto</label>    
                                        <asp:TextBox ID="hdh"  runat="server"/>
                                    </div>
                                    <div class="controls">
                                        <asp:Button ID="btnRecortar" CssClass="btnRecortar" Text="Recortar" runat="server" OnClick="btnRecortar_Click"  ValidationGroup="groupRecortar"/>
                                        &nbsp
                                        <asp:Button ID="btnMantener" CssClass="btnRecortar" Text="No Recortar" runat="server" OnClick="btnMantener_Click" />
                                    </div>
                                    <div class="controls">

                                    <asp:HiddenField ID="hdInfoID" runat="server" />                   
                                </div>
                            </div>
                                
                                <asp:CustomValidator id="cvRecortar"
                          
                          ClientValidationFunction="ValidaCrop"
                          Display="Static" 
                           ErrorMessage="*" ValidationGroup="groupRecortar"         
                          runat="server"/>
                                <div id="divCrop" class="noticia newFormStyle"></div>
                                <asp:HiddenField  runat="server" ID="hdPath" />

                            </div>
                        </div>
                    </asp:Panel>

                


    </asp:View>

            <asp:View runat="server" ID="Exito">
                  <div class="messages msg-exito">
                    <span lang="ES-TRAD">La operación fue realizada exitosamente</span>
                </div>
                <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="2000" OnTick="Timer1_Tick">
                </asp:Timer>
            </asp:View>
        </asp:MultiView>


        <asp:Panel runat="server" ID="pnlPreview" >
        <div id="mask"></div>

  
            <!--Beg Vista Previa -->
            <div id="boxes" class="preview">
             
            <div id="Preview" class="window">
      
           <br />
            <a href="#"class="close">Cerrar</a>
                <asp:UpdatePanel ID="updEnviar" runat="server">
                <ContentTemplate>
                  <asp:Button ID="btnEnviar" runat="server" Text="Enviar Email" onclick="btnEnviar_Click" />  
                   <asp:Label ID="lblEnvio"  runat="server" ></asp:Label>
                </ContentTemplate>
                </asp:UpdatePanel>
             

             <br />
		    <div class="noticia newFormStyle">

			 

		    <div id="CPHMain_main_RNoticias" class="AspNet-DataList">
                <asp:Literal ID="hdPreview" runat="server" />
                 <br />
        
           
		    </div>
              
                    
	
							
	       </div>

						

      
            </div>   
        
                        
            </div>



            <!-- Mask to cover the whole screen -->
        <!--End Vista Previa -->



 
        </asp:Panel>





    
    </div>    
</asp:Content>
