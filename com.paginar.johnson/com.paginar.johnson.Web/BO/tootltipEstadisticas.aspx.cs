﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace com.paginar.johnson.Web.BO
{
    public partial class tootltipEstadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string desde = Request.QueryString["desde"];
            string Hasta = Request.QueryString["hasta"];
            string indicador = string.Empty;
            string tipo = Request.QueryString["tipo"];
        

            switch (tipo)
            { 
                case "user_access":
                    indicador = "Usuarios que accedieron al sitio";
                    MVtooltip.SetActiveView(Vuser_access_site);
                break;

                case "sec_mas_cons":
                    indicador = "Sección más consultada";
                    MVtooltip.SetActiveView(Vsec_mas_cons);
                break;

                case "top_noticia":
                    indicador = "Noticia más visitada";
                    MVtooltip.SetActiveView(VTop_noticia);  
                break;

                case "horario_concurrencia":
                    indicador = "Horario de mayor concurrencia";
                    MVtooltip.SetActiveView(Vhorario_concurrencia);
                break;
                case "hombres_mujeres":
                    indicador = "Cantidad de hombres y mujeres";
                    MVtooltip.SetActiveView(Vhombres_mujeres);
                break;
                case "noticia_mas_visitada":
                    indicador = "Noticia más visitada";
                break;
                case "grupo_etario":
                    lblRango.Text = "<strong>Edades: </strong>" + Request.QueryString["rango"];
                    indicador = "Grupo etário";
                    MVtooltip.SetActiveView(VGrupo_Etario);
                break;

                case "Banner_mas_visitado":
                if (Request.QueryString["indicador"] != null) indicador = Request.QueryString["indicador"].ToString();
                MVtooltip.SetActiveView(banner_mas_consultado);
                break;
                
            }

            //banners
      

            LblInformation.Text = "<strong>Indicador:</strong> " + indicador + "<br /><strong>Rango:</strong> de " + desde + " a " + Hasta;
        }
    }
}