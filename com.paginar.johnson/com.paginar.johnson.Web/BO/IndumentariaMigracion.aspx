﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="IndumentariaMigracion.aspx.cs" Inherits="com.paginar.johnson.Web.BO.IndumentariaMigracion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Migración Pedido de Ropa
    </h2>
    <asp:DropDownList ID="DDLCodigoMigracion" runat="server" 
    DataSourceID="ODSCodigoMigracion" DataTextField="codigoMigracion" 
    DataValueField="codigoMigracion">
    </asp:DropDownList>
    <asp:ObjectDataSource ID="ODSCodigoMigracion" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="GetCodigosMigracion" 
    TypeName="com.paginar.johnson.DAL.DSIndumentariaTableAdapters.IndumentariaMigracionTableAdapter">
</asp:ObjectDataSource>
    <asp:Button ID="btnRefresh" runat="server" Text="Refrescar" />
    <div style="overflow:auto">
    <asp:GridView ID="GVDatosMigrar" runat="server" 
    DataSourceID="ODSListadoMigracion" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="codigoMigracion" HeaderText="codigoMigracion" 
                SortExpression="codigoMigracion" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
            <asp:BoundField DataField="Sexo" HeaderText="Sexo" SortExpression="Sexo" />
            <asp:BoundField DataField="MP" HeaderText="MP" SortExpression="MP" />
            <asp:BoundField DataField="Sector" HeaderText="Sector" 
                SortExpression="Sector" />
            <asp:CheckBoxField DataField="Brigadista" HeaderText="Brigadista" 
                SortExpression="Brigadista" />
            <asp:BoundField DataField="Remera" HeaderText="Remera" 
                SortExpression="Remera" />
            <asp:BoundField DataField="PantalonA" HeaderText="PantalonA" 
                SortExpression="PantalonA" />
            <asp:BoundField DataField="PantalonB" HeaderText="PantalonB" 
                SortExpression="PantalonB" />
            <asp:BoundField DataField="ZapatosA" HeaderText="ZapatosA" 
                SortExpression="ZapatosA" />
            <asp:BoundField DataField="ZapatosB" HeaderText="ZapatosB" 
                SortExpression="ZapatosB" />
            <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" 
                SortExpression="Observaciones" />
        </Columns>
    </asp:GridView>
    </div>

    <asp:ObjectDataSource ID="ODSListadoMigracion" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="GetDataByCodigoMigracion" 
    
        
        TypeName="com.paginar.johnson.DAL.DSIndumentariaTableAdapters.IndumentariaMigracionTableAdapter" 
        InsertMethod="Insert">
        <InsertParameters>
            <asp:Parameter Name="codigoMigracion" Type="Int32" />
            <asp:Parameter Name="Legajo" Type="Int32" />
            <asp:Parameter Name="Sexo" Type="String" />
            <asp:Parameter Name="MP" Type="String" />
            <asp:Parameter Name="Sector" Type="String" />
            <asp:Parameter Name="Brigadista" Type="Boolean" />
            <asp:Parameter Name="Remera" Type="String" />
            <asp:Parameter Name="PantalonA" Type="String" />
            <asp:Parameter Name="PantalonB" Type="String" />
            <asp:Parameter Name="ZapatosA" Type="String" />
            <asp:Parameter Name="ZapatosB" Type="String" />
            <asp:Parameter Name="Observaciones" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLCodigoMigracion" Name="codigoMigracion" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
</asp:ObjectDataSource>

    <br />
    <br />

    <asp:Button ID="btnMigrar" runat="server" Text="Migrar Datos" 
        onclick="btnMigrar_Click" />
</asp:Content>
