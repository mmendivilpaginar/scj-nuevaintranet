﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pdaReporteInscriptosPorCursos.aspx.cs" Inherits="com.paginar.johnson.Web.BO.pdaReporteInscriptosPorCursos" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PDA :: Reporte Inscriptos por Dirección y Área</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="Curso" runat="server" />
    <asp:HiddenField ID="HFDireccion" runat="server" />
    <asp:HiddenField ID="HFArea" runat="server" />
    <table width="100%">
        <tr>
            <td>
   <span runat="server" ID="PanelEvaluador">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label1" runat="server" Text="Curso:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="lblCurso" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    <br />
   </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelTituloDireccion" runat="server" Text="Direccón:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="LabelDireccion" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    <br />
    </span>
    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="lblAreaTit" runat="server" Text="Área:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="lblArea" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    </span>

               
   <span runat="server" ID="PanelArea">
    <br />
    </span>

                &nbsp;
            </td>
            <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" /><br /></td>
        </tr>
    </table>
   <div class="AspNet-GridViewRepPMP">



   </div>
    <br />

    </form>
</body>
</html>