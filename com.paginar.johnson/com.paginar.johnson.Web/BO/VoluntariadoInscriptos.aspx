﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoluntariadoInscriptos.aspx.cs"  EnableEventValidation="false" Inherits="com.paginar.johnson.Web.BO.VoluntariadoInscriptos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Voluntariado - Inscriptos</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>

    
</head>
<body>
    <form id="FormImpWSInscriptos" runat="server">
    <div id="wrapper">
        <div id="header">
            <div class="bg"></div>
            <h1>Voluntariado</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
        <div id="noprint">

            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />&nbsp;&nbsp;<asp:Button ID="exportar"
                runat="server" Text="Exportar" onclick="exportar_Click" />
            <br /><br />
        </div>
        <h1><asp:Label ID="lblNombreVoluntariado" runat="server" Text="Label"></asp:Label></h1>
        <h3>Usuarios Suscriptos</h3>
        <asp:GridView ID="GVInscriptos" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="IdInscripto" DataSourceID="ODSInscriptos">
            <Columns>
                <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                    SortExpression="Legajo" />
                <asp:TemplateField HeaderText="Usuario" SortExpression="Comentarios">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Usuario") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Usuario") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FechaInscripsion" HeaderText="FechaInscripsion" 
                    SortExpression="FechaInscripsion" DataFormatString="{0:d}" />
                <asp:CheckBoxField DataField="Notificado" HeaderText="Notificado" 
                    SortExpression="Notificado" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSInscriptos" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="InscriptosVoluntariados" 
            TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
            <SelectParameters>
                <asp:QueryStringParameter Name="idVol" QueryStringField="idvol" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
