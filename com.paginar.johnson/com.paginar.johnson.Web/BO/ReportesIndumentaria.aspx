﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="ReportesIndumentaria.aspx.cs" Inherits="com.paginar.johnson.Web.BO.ReportesIndumentaria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
    .cabeceraN
    {
        color:Green;
        background-color:Lime;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<div id="wrapperIndumentaria">
<h2>Reportes Ropa de Trabajo</h2>
    <div class="form-item leftHalf">
    <label>Tipo de Reporte</label>
        <asp:DropDownList ID="DDTipodeReporte" runat="server" AutoPostBack="True" 
            onselectedindexchanged="DDTipodeReporte_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="1">Solicitud de control de entrega de indumentaria</asp:ListItem>
            <asp:ListItem Value="2">Solicitud de indumentaria para proveedores</asp:ListItem>
            <asp:ListItem Value="3">Solicitudes de indumentaria sin detalle</asp:ListItem>
        </asp:DropDownList> 
    </div>
    <div class="form-item rightHalf">
        <label>Período</label>
        <asp:DropDownList ID="DDLPeriodo" runat="server" AppendDataBoundItems="True" 
            ondatabound="DropDownList1_DataBound">
        </asp:DropDownList>
    </div>
    <div class="form-item leftHalf">
        <label>Brigadista</label>
        <asp:DropDownList ID="DDLBrigadista" runat="server">
            <asp:ListItem Selected="True">Todos</asp:ListItem>
            <asp:ListItem Value="true">Sí</asp:ListItem>
            <asp:ListItem Value="false">No</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-item rightHalf">
        <label>Área</label>
        <asp:DropDownList ID="DDLAreas" runat="server" DataSourceID="ODSDDAreas" 
            DataTextField="Area" DataValueField="Area" AppendDataBoundItems="True" 
            ondatabound="DDLAreas_DataBound">
            <asp:ListItem Text="Todas" Value="" />  
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ODSDDAreas" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="getAreas" 
            TypeName="com.paginar.johnson.BL.ControllerTramites"></asp:ObjectDataSource>
    </div>
    <div class="form-item form-end">
      <asp:Button ID="Filtrar" runat="server" Text="Buscar" 
            onclick="Filtrar_Click" />&nbsp;
        <asp:Button ID="btnExportar" runat="server" onclick="btnExportar_Click" 
        Text="Exportar" />
        <asp:HyperLink ID="hlImprimir" runat="server" Target="_blank"><asp:Image ID="imgPrint" runat="server" ImageUrl="~/images/printer.png"  ToolTip="Imprimir Reporte"/></asp:HyperLink>

        <asp:Literal ID="ltlImprimir" runat="server"></asp:Literal>
        </div>
    <div class="form-item rightHalf">
  
    </div>
        <h3><asp:Label ID="lblTituloReporte" runat="server" Text=""></asp:Label></h3>
        
    <asp:Panel ID="PSoliConDetalle" runat="server">
        <asp:GridView ID="GVPanel1" runat="server" AutoGenerateColumns="False" 
            AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="UsuarioID" HeaderText="UsuarioID" 
                    SortExpression="UsuarioID" Visible="False" />
                <asp:BoundField DataField="Area" HeaderText="Área" SortExpression="Area" />
                <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                    SortExpression="Legajo" />
                <asp:BoundField DataField="NombreCompleto" HeaderText="Nombrey Apellido" 
                    ReadOnly="True" SortExpression="NombreCompleto" />
                <asp:BoundField DataField="Jefe" HeaderText="Jefe del Solicitante" 
                    ReadOnly="True" SortExpression="Jefe" />
                <asp:BoundField DataField="Zapatos" HeaderText="Zapatos" ReadOnly="True" 
                    SortExpression="Zapatos" />
                <asp:BoundField DataField="Pantalon" HeaderText="Pantalón" ReadOnly="True" 
                    SortExpression="Pantalon" />
                <asp:BoundField DataField="Torso" HeaderText="Torso" ReadOnly="True" 
                    SortExpression="Torso" />
                         <asp:BoundField DataField="Sexo" HeaderText="Sexo" ReadOnly="True" 
                    SortExpression="Sexo" />
                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                       <%-- <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Especial" HeaderText="Pedido Especial" />
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel1" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteDetalleSelect" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLPeriodo" Name="PeriodoID" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLAreas" Name="Area" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DDLBrigadista" Name="esBrigadista" 
                    PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>

    </asp:Panel>    
    <asp:Panel ID="PSoliSinDetalleporArea" runat="server">
        <asp:GridView ID="GVPanel2" runat="server" AutoGenerateColumns="False" 
            AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                    SortExpression="Area" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                    SortExpression="Descripcion" />
                <asp:BoundField DataField="talle" HeaderText="Talle" SortExpression="talle" />
                <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" ReadOnly="True" 
                    SortExpression="CANTIDAD" />
                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                        
<%--                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel2" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteAreaSinDetallesSelect" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLPeriodo" Name="PeriodoID" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLAreas" Name="Area" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="DDLBrigadista" Name="esBrigadista" 
                    PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
    <asp:Panel ID="PSoliSinDetalle" runat="server">
        <asp:GridView ID="GVPanel3" runat="server" AutoGenerateColumns="False" 
            AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                    SortExpression="Descripcion" />
                <asp:BoundField DataField="talle" HeaderText="Talle" SortExpression="talle" />
                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ReadOnly="True" 
                    SortExpression="Cantidad" />
                <asp:TemplateField HeaderText="Brigadista" SortExpression="esBrigadista">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# esBrigadista(Eval("esBrigadista")) %>'></asp:Label>
                        <%--<asp:CheckBox ID="CheckBox1" runat="server" 
                            Checked='<%# Bind("esBrigadista") %>' Enabled="false" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No hay contenidos para mostar.
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSPanel3" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getIndumentariaReporteSinDetalle" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentariaReporte">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLPeriodo" Name="PeriodoID" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLBrigadista" Name="esBrigadista" 
                    PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
</div>    
</asp:Content>