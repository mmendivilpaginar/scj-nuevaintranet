﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="WorkshopReporte.aspx.cs" Inherits="com.paginar.johnson.Web.BO.WorkshopReporte" %>
<%@ Register assembly="Microsoft.Web.DynamicData" namespace="Microsoft.Web.DynamicData" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.AspNet-GridView
{
    margin-bottom: 0px !important;
}
.AspNet-GridView .AspNet-GridView
{
    margin-bottom: 0px !important;
}
.wrappreppda
{
    margin-bottom: 10px !important;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperIndumentaria">
        <h2>Reporte Workshop
        </h2>
        <h3>Gestión de Talento - Cursos de Inglés</h3>

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnFiltrar">
            
                <div>
                    <div class="form-item leftHalf">
                        <label>Año</label>
                        <asp:TextBox ID="tbAnioUsuario" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="tbAnioUsuario_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbAnioUsuario">
                        </asp:FilteredTextBoxExtender>
                    </div>
                    <div class="form-item rightHalf">
                        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar"  style="float:right"
                            onclick="btnFiltrar_Click" />
                    </div> 
                </div>
        </asp:Panel>

        <%--<h3>Consulta por xxx</h3>--%>
            <asp:GridView ID="GVReporteWorkShop" runat="server" AutoGenerateColumns="False" DataSourceID="ODSWorkshopReporte">
                <Columns>
                    <asp:BoundField DataField="Usuario" HeaderText="Nombre" ReadOnly="True" SortExpression="Usuario" />
                    <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha" SortExpression="FechaInscripcion" />
                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" SortExpression="Legajo" />
                    <asp:BoundField DataField="Nombre" HeaderText="Curso" SortExpression="Nombre" />
                    <asp:CheckBoxField DataField="Notificada" HeaderText="Fue Notificado" SortExpression="Notificada" />
                </Columns>
            </asp:GridView>

        <asp:ObjectDataSource ID="ODSWorkshopReporte" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="WorkshopGetSuscriptosByAnio" TypeName="com.paginar.johnson.BL.ControllerWorkShop">
            <SelectParameters>
                <asp:ControlParameter ControlID="tbAnioUsuario" Name="anio" PropertyName="Text" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

    </div>
</asp:Content>
