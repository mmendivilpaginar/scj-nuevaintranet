﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pdaReporteInscripcionDirArea.aspx.cs" Inherits="com.paginar.johnson.Web.BO.pdaReporteInscripcionDirArea" %>

<%@ Register assembly="Microsoft.Web.DynamicData" namespace="Microsoft.Web.DynamicData" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PDA :: Reporte Inscriptos por Dirección y Área</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="Curso" runat="server" />
    <asp:HiddenField ID="HFDireccion" runat="server" />
    <asp:HiddenField ID="HFArea" runat="server" />
    <table width="100%">
        <tr>
            <td>
   <span runat="server" ID="PanelEvaluador">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label1" runat="server" Text="Curso:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="lblCurso" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    <br />
   </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelTituloDireccion" runat="server" Text="Direccón:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="LabelDireccion" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    <br />
    </span>
    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="lblAreaTit" runat="server" Text="Área:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="lblArea" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    </span>

               
   <span runat="server" ID="PanelArea">
    <br />
    </span>

                &nbsp;
            </td>
            <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" /><br /></td>
        </tr>
    </table>
   <div class="AspNet-GridViewRepPMP">
       <asp:GridView ID="GVReporter" runat="server" AutoGenerateColumns="False" 
           DataKeyNames="Usuarioid" DataSourceID="ODSReporte">
           <Columns>
               <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                   InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                   Visible="False" />
               <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                   SortExpression="Legajo" />
               <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                   SortExpression="Apellido" />
               <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                   SortExpression="Nombre" />
               <asp:BoundField DataField="FechaInscripcion" HeaderText="FechaInscripcion" 
                   SortExpression="FechaInscripcion" />
               <asp:BoundField DataField="Direccion" HeaderText="Direccion" ReadOnly="True" 
                   SortExpression="Direccion" />
               <asp:BoundField DataField="Area" HeaderText="Area" ReadOnly="True" 
                   SortExpression="Area" />
           </Columns>
       </asp:GridView>

       <asp:ObjectDataSource ID="ODSReporte" runat="server" 
           OldValuesParameterFormatString="original_{0}" 
           SelectMethod="getDetalleReportePorAreaDireccionCurso" 
           TypeName="com.paginar.johnson.BL.ControllerPDA">
           <SelectParameters>
               <asp:ControlParameter ControlID="HFArea" Name="AreaID" PropertyName="Value" 
                   Type="Int32" />
               <asp:ControlParameter ControlID="HFDireccion" Name="DireccionID" 
                   PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="Curso" Name="idCurso" PropertyName="Value" 
                   Type="Int32" />
           </SelectParameters>
       </asp:ObjectDataSource>

   </div>
    <br />

    </form>
</body>
</html>