﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imprimirEstadisticas.aspx.cs" Inherits="com.paginar.johnson.Web.BO.imprimirEstadisticas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <title>Estadisticas</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
<body>
    <form id="formImprEst" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Información Estadística</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
     
        <div id="noprint">

            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
            <br /><br />
        </div>
        <h3>
            <asp:Label ID="lblIndicador" runat="server"></asp:Label> </h3>
        <div><asp:Label ID="lblTitIndicador" runat="server" Text="Label"></asp:Label></div>

                <asp:MultiView ID="MVEstadisticas" runat="server">
                        <asp:View ID="user_access_site" runat="server">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                ondatabound="GridView1_DataBound" ShowFooter="True" >
                                <Columns>


                                    <asp:TemplateField HeaderText="Periodo" SortExpression="UnidadPeriodica">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="LBLUsrTotSub" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="LBLTotalUser_access" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="LBLCantUuser_access" runat="server" 
                                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontro información con los parametros de búsqueda ingresados
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSuser_access_site" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="GetEstadisticas_user_access_site" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>
        
                        <asp:View ID="secc_mas_consult" runat="server">
                            <asp:GridView ID="GridView2" runat="server" 
                                AutoGenerateColumns="False"
                                ondatabound="GridView2_DataBound" ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="PERIODO" 
                                        SortExpression="UnidadPeriodica">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                                Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalSecc_mas_visitada" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pagina" SortExpression="Pagina">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>'></asp:Label>
                                            <asp:Label ID="Link" runat="server" Text='<%# Bind("Titulo") %>' 
                                                Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalGenSecMasCons" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSSecc_mas_consultada" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="Get_seccion_mas_consultada" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>
                    <asp:View ID="banner_mas_consultado" runat="server">
                        <asp:GridView ID="GridView7" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            OnDataBound="GridView7_DataBound" ShowFooter="True"
                            AllowSorting="False" >
                            <Columns>
                                <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                                    <EditItemTemplate>
                                        <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSecc_mas_visitada" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pagina" SortExpression="Pagina">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>' Visible="false"></asp:Label>                                    
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("titulo") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="Link" runat="server" Text='<%# Bind("Titulo") %>' Visible="False"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGenSecMasCons" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No se encontro información con los parametros de búsqueda ingresados
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </asp:View>

                        <asp:View ID="grup_etario" runat="server">
                            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                                ShowFooter="True" 
                                ondatabound="GridView3_DataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="PERIODO" 
                                        SortExpression="UnidadPeriodica">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotSubTotGrupoEtario" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="rango" HeaderText="rango" ReadOnly="True" 
                                        SortExpression="rango" />
                                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalGrupoEtario" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCantUnitariaGrupoEtario" runat="server" 
                                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSGrupoEtario" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="Get_grupos_etarios" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>

                        <asp:View ID="hombres_mujeres" runat="server">
                            <asp:GridView ID="GridView4" runat="server" 
                                AutoGenerateColumns="False" 
                                ondatabound="GridView4_DataBound" ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotSubTotetiq" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOT. HOMBRES" SortExpression="MASCULINO">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("MASCULINO") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblhombrestotales" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblhombresunidades" runat="server" 
                                                Text='<%# Bind("MASCULINO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOT. MUJERES" SortExpression="FEMENINO">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("FEMENINO") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblmujerestotales" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblmujeresunidades" runat="server" 
                                                Text='<%# Bind("FEMENINO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL PERIODO" SortExpression="TOTALPARCIAL">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("TOTALPARCIAL") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalesTHM" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblhombresymujeresunidad" runat="server" 
                                                Text='<%# Bind("TOTALPARCIAL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSHombres_Mujeres" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="Get_Hombres_Mujeres" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>

                        <asp:View ID="hora_mayor_concurrencia" runat="server">
                            <asp:GridView ID="GridView5" runat="server" 
                                AutoGenerateColumns="False" 
                                ondatabound="GridView5_DataBound" ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="PERIODO" 
                                        SortExpression="UnidadPeriodica">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTitTotMayConc" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="rango" SortExpression="rango">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("rango") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblrango" runat="server" Text='<%# Bind("rango") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotGralHorConc" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotUnitHorConc" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ODSHorarioConcurrencia" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="Get_horarios_concurrencia" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>

                        <asp:View ID="noticia_mas_visitada" runat="server">
                        <asp:GridView ID="GridView6" runat="server" 
                                AutoGenerateColumns="False" 
                                ondatabound="GridView6_DataBound" ShowFooter="True">
                            <Columns>
                                <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTitTotNot" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pagina" SortExpression="Pagina">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                                    <EditItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotGralTopNot" runat="server"></asp:Label>
                                    </FooterTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotUnitTopNot" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                            <asp:ObjectDataSource ID="ODSTopNoticias" runat="server" 
                                OldValuesParameterFormatString="original_{0}" SelectMethod="Get_top_noticias" 
                                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                                        Type="DateTime" />
                                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                                        Type="DateTime" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </asp:View>

                    </asp:MultiView>
<asp:MultiView ID="MVGrafica" runat="server">
        <asp:View ID="Guser_access_site" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart1" runat="server" DataSourceID="ODSGuser_access" 
                Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="UnidadPeriodica" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidad">
                        </AxisY>
                        <AxisX Title="Linea de Tiempo">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
            <asp:ObjectDataSource ID="ODSGuser_access" runat="server" 
                InsertMethod="InsertRegistroEstadistico" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_user_access_site_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <InsertParameters>
                    <asp:Parameter Name="UsuarioID" Type="Int32" />
                    <asp:Parameter Name="paginaVisitada" Type="String" />
                    <asp:Parameter Name="titulo" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="Gsecc_mas_consult" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart2" runat="server" DataSourceID="ODSGsec_mas_consultada" 
                Width="600px" Height="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="Titulo" YValueMembers="Cantidad" 
                        ChartType="Bar">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidad">
                        </AxisY>
                        <AxisX Title="Secciones / Subsecciones">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
            <asp:ObjectDataSource ID="ODSGsec_mas_consultada" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_secc_mas_consult_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="Ggrup_etario" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart3" runat="server" DataSourceID="ODSGgrupo_etareo" 
                IsMapAreaAttributesEncoded="True" Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="rango" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades / Promedios">
                        </AxisY>
                        <AxisX Title="Grupos Etáreos">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:ObjectDataSource ID="ODSGgrupo_etareo" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_grupo_etario_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
        <asp:View ID="Ghombres_mujeres" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart4" runat="server"  Width="600px" 
                DataSourceID="ODSGHombres_mujeres">
                <Series>
                    <asp:Series Name="Hombres" Legend="Legend1" XValueMember="UnidadPeriodica" 
                        YValueMembers="MASCULINO">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Legend="Legend1" Name="Mujeres" 
                        XValueMember="UnidadPeriodica" YValueMembers="FEMENINO">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades / Promedios">
                        </AxisY>
                        <AxisX Title="Linea de Tiempo">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Name="Legend1">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
            <asp:ObjectDataSource ID="ODSGHombres_mujeres" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_hombres_mujeres_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
        <asp:View ID="Ghora_mayor_concurrencia" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart5" runat="server" 
                DataSourceID="ODSGhora_mayor_concurrencia" Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="rango" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades / Promedios">
                        </AxisY>
                        <AxisX Title="Rangos Horarios">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:ObjectDataSource ID="ODSGhora_mayor_concurrencia" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_horario_concurrencia_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
        <asp:View ID="Gnoticia_mas_visitada" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart6" runat="server" Width="600px" 
                BackGradientStyle="HorizontalCenter" DataSourceID="ODSGtop_noticias" 
                Height="600px">
                <Series>
                    <asp:Series Name="Series1" ChartType="Bar" 
                        CustomProperties="DrawSideBySide=True, LabelStyle=Center" 
                        IsXValueIndexed="True" XValueMember="Titulo" XValueType="String" 
                        YValueMembers="Cantidad">
                        <SmartLabelStyle AllowOutsidePlotArea="No" />
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades">
                        </AxisY>
                        <AxisX Title="Noticias">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:ObjectDataSource ID="ODSGtop_noticias" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_noticias_mas_visitadas_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                    <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                        Type="DateTime" />
                    <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
    </asp:MultiView>

    </form>
</body>
</html>
