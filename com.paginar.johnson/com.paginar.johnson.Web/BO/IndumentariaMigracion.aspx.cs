﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class IndumentariaMigracion : System.Web.UI.Page
    {
        int UsuarioId;
        int PeriodoId;
        DSIndumentaria.Indumentaria_PeriodoDataTable PeriodoActual;        
        ControllerIndumentaria CI = new ControllerIndumentaria();
        ControllerTramites CT = new ControllerTramites();
        ControllerUCHome UCH = new ControllerUCHome();
        ControllerUsuarios CU = new ControllerUsuarios();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PeriodoActual = CI.SeleccionarPeriodoActual();
                PeriodoId = int.Parse(PeriodoActual.Rows[0][0].ToString());
            }
        }

        protected void procesarPedido(int usuarioId, string apellido, string nombre, int legajo,string remera, string pantalon, string zapato, string observaciones, bool brigadista)
        {
            string observacionesIns = observaciones != "0" ? observaciones : string.Empty;
            PeriodoActual = CI.SeleccionarPeriodoActual();
            //DSIndumentaria.frmTramiteDataTable Traminte = CI.IndumentariaTramiteActualSeleccionar(usuarioId, int.Parse(PeriodoActual.Rows[0][0].ToString()));
            int tramiteID;
            int his_secuencia;
            
            if ( zapato.Split('|').Length > 1 )
                observacionesIns = observacionesIns + (zapato.Split('|'))[1];

            tramiteID = int.Parse(CI.tramite_alta(usuarioId, "ALTA", usuarioId, apellido, nombre, legajo, "", "").ToString());
            if (tramiteID != 0)
            {
                his_secuencia = 1;
                CI.IndumentariaActualizarPasoTramite(tramiteID, 1);

                CT.InsertarPasoNuevoHistorico(tramiteID, usuarioId, usuarioId, "E", 1, observacionesIns, his_secuencia);

                CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 1, (zapato.Split('|'))[0].Trim(), brigadista);

                CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 2, pantalon.Trim(), brigadista);

                CI.IndumentariaPedidoInsertar(tramiteID, int.Parse(PeriodoActual.Rows[0][0].ToString()), 3, remera.Trim(), brigadista);
            }
        }

        protected string procesarPantalon(string a, string b)
        {
            if (a == "0")
                return b;
            else
                return a;
        }

        protected string procesarZapato(string a, string b)
        {
            if (a == "0")
                return b + "|Zapato de seguridad puntera plastica (con orden medica)";
            else
                return a;        
        }

        protected void btnMigrar_Click(object sender, EventArgs e)
        {
            DSIndumentaria.IndumentariaMigracionDataTable TM = new DSIndumentaria.IndumentariaMigracionDataTable();
            
            TM = CI.getPeriodoMigrar(1);

            foreach (DSIndumentaria.IndumentariaMigracionRow item in TM.Rows)
            {
                if (int.Parse(item.Legajo.ToString()) != 0)
                {
                    if ((CU.GetUsuariosByLegajo(item.Legajo)).Rows.Count > 0)
                    {
                        DSUsuarios.UsuarioRow UDR = (DSUsuarios.UsuarioRow)(CU.GetUsuariosByLegajo(item.Legajo)).Rows[0];
                        procesarPedido(UDR.LoginID, UDR.Apellido, UDR.Nombre, UDR.Legajo, item.Remera, procesarPantalon(item.PantalonA, item.PantalonB), procesarZapato(item.ZapatosA, item.ZapatosB), item.Observaciones, bool.Parse(item.Brigadista.ToString()));
                    }
                }
            }



        }
    }
}