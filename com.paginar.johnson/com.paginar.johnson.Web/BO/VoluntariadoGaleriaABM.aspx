﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="VoluntariadoGaleriaABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.VoluntariadoGaleriaABM" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    $(document).ready(function () {


    });




    function chekAndLimit(elemento, max) {
        txt = elemento;
        MaxLength = max;        
        if (txt.value.length > (MaxLength - 1)) {
            document.getElementById(elemento.id).value = document.getElementById(elemento.id).value.substring(0, MaxLength - 1);
        }
        else return true;

        }
    


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Administración de Galerías Fotográficas Voluntariado</h2>
    <asp:MultiView ID="MVEventos" runat="server">
        <asp:View ID="VEventoCampos" runat="server">
            <asp:GridView ID="GVEventos" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataKeyNames="IdEvento" 
                DataSourceID="ODSEventoList">
                <Columns>
                    <asp:CommandField SelectText="Editar" ShowSelectButton="True" />
                    <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenField3" runat="server" Value='<%# Eval("IdEvento") %>' />
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenField3" runat="server" Value='<%# Eval("IdEvento") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                        SortExpression="Descripcion" />
                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" 
                        DataFormatString="{0:d}" />
                    <asp:TemplateField HeaderText="Imágenes">
                        <ItemTemplate>
                            <asp:HyperLink ID="HLImagenes" runat="server" 
                                NavigateUrl='<%# UrlRetornoImagen(Eval("IdEvento")) %>'>Imagenes</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No exiten Eventos para mostrar
                </EmptyDataTemplate>
            </asp:GridView>

            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                Text="Agregar" />
            <asp:ObjectDataSource ID="ODSEventoList" runat="server" 
                DeleteMethod="EventoDelete" InsertMethod="EventoInsert" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="EventoSelectAll" 
                TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
                UpdateMethod="EventoUpdate">
                <DeleteParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Fecha" Type="DateTime" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Fecha" Type="DateTime" />
                </UpdateParameters>
            </asp:ObjectDataSource>

            <asp:FormView ID="FVEventos" runat="server" DataKeyNames="IdEvento" 
                DataSourceID="ODSEvento" oniteminserted="FVEventos_ItemInserted" 
                onitemcreated="FVEventos_ItemCreated" onitemdeleted="FVEventos_ItemDeleted" 
                onitemupdated="FVEventos_ItemUpdated">
                <EditItemTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="UpdateButton">
                    <asp:HiddenField ID="IdEventoLabel1" runat="server"  Value='<%# Eval("IdEvento") %>' />
                    <strong> Nombre:</strong>
                    <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>'  onKeyDown="chekAndLimit(this,150)" />
                    <br />
                    <strong>Descripción:</strong><br />
                    <asp:TextBox ID="DescripcionTextBox" runat="server"  onKeyDown="chekAndLimit(this,250)"
                        Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
                    <br />
                    <strong>Fecha:</strong>
                    <asp:TextBox ID="FechaTextBox" runat="server" Text='<%# Bind("Fecha","{0:d}") %>' />
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1" TargetControlID="FechaTextBox" Enabled="True">                        
                    </asp:CalendarExtender>
                     <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="FechaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="" Display="Dynamic">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="FechaTextBox">*</asp:RequiredFieldValidator>
                (dd/mm/aaaa)

                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Aceptar" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                    
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="InsertButton">
                    <strong>Nombre:</strong>
                    <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>'  onKeyDown="chekAndLimit(this,150)" />
                    <br />
                    <strong>Descripción:</strong> <br />
                    <asp:TextBox ID="DescripcionTextBox" runat="server"  onKeyDown="chekAndLimit(this,250)"
                        Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
                    <br />
                    <strong> Fecha:</strong> 
                    <asp:TextBox ID="FechaTextBox" runat="server" Text='<%# Bind("Fecha","{0:d}") %>' 
                        SkinID="form-date" />
                    <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1" TargetControlID="FechaTextBox" Enabled="True">
                    </asp:CalendarExtender>
                     <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="FechaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="" Display="Dynamic">*</asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="FechaTextBox">*</asp:RequiredFieldValidator>

<%--                    <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                        TargetControlID="FechaTextBox" PopupButtonID="Image1"></asp:CalendarExtender>--%>
                    <br />

                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Agregar" onclick="InsertButton_Click" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                    
                    </asp:Panel>
                </InsertItemTemplate>
                <ItemTemplate>
                    <strong>Nombre:</strong>
                    <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
                    <br />
                    <strong>Descripción:</strong>
                    <asp:Label ID="DescripcionLabel" runat="server" 
                        Text='<%# Bind("Descripcion") %>' />
                    <br />
                    <strong>Fecha:</strong>
                    <asp:Label ID="FechaLabel" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                        CommandName="Edit" Text="Editar" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Borrar" OnClientClick="return confirm('Esta acción eliminará todas las imágenes contenidas en esta galería\n ¿Desea continuar?');" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                        CommandName="New" Text="Nuevo" />
                </ItemTemplate>
            </asp:FormView>

            <asp:ObjectDataSource ID="ODSEvento" runat="server" DeleteMethod="EventoDelete" 
                InsertMethod="EventoInsert" OldValuesParameterFormatString="{0}" 
                SelectMethod="EventoSelect" 
                TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
                UpdateMethod="EventoUpdate">
                <DeleteParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Fecha" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="GVEventos" Name="IdEvento" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Fecha" Type="DateTime" />
                </UpdateParameters>
            </asp:ObjectDataSource>

        </asp:View>
        <asp:View ID="VEventoImagenes" runat="server">
        <h2>
            <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label> </h2>
            <asp:GridView ID="GVImagenes" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="IdImagen" DataSourceID="ODSImagenesListado" 
                onselectedindexchanged="GVImagenes_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField SelectText="Seleccionar" ShowSelectButton="True" />
                    <asp:TemplateField HeaderText="Imagen">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("IdImagen") %>'/>
                            <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Eval("IdEvento") %>'/>
                            <asp:Image ID="Image1" runat="server" 
                                ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("IdImagen") %>'/>
                            <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Eval("IdEvento") %>'/>
                            <asp:Image ID="Image1" runat="server" 
                                ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                        SortExpression="Descripcion" />
                </Columns>
                <EmptyDataTemplate>
                    No hay Imágenes cargadas en este evento.
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                Text="Agregar" />
            <asp:ObjectDataSource ID="ODSImagenesListado" runat="server" 
                DeleteMethod="ImagenDelete" InsertMethod="ImagenInsert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="ImagenSelectByEvento" 
                TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
                UpdateMethod="ImagenUpdate">
                <DeleteParameters>
                    <asp:Parameter Name="IdImagen" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Imagen" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:QueryStringParameter DefaultValue="" Name="IdEvento" 
                        QueryStringField="IdEvento" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="IdImagen" Type="Int32" />
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Imagen" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
            <br />
            <asp:FormView ID="FVImagenes" runat="server" DataKeyNames="IdImagen" 
                DataSourceID="ODSImagenesFV" onitemcreated="FVImagenes_ItemCreated" 
                onitemdeleted="FVImagenes_ItemDeleted" oniteminserted="FVImagenes_ItemInserted" 
                onitemupdated="FVImagenes_ItemUpdated" ondatabound="FVImagenes_DataBound" 
                onmodechanged="FVImagenes_ModeChanged">
                <EditItemTemplate>
                <asp:Panel ID="Panel3" runat="server" DefaultButton="UpdateButton">
                    <asp:HiddenField ID="IdEventoTextBox" runat="server"  Value='<%# Bind("IdEvento") %>' />
                    <strong>Imagen:</strong>
                   <br />
                     <%--    <asp:Image ID="Image1" runat="server" 
                                ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' />
                    <br />--%>
                    <asp:TextBox ID="ImagenTextBox" runat="server" Text='<%# Bind("Imagen") %>' 
                        ReadOnly="True" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="ImagenTextBox">*</asp:RequiredFieldValidator>
                    <br />
                    <asp:FileUpload ID="FUImagen" runat="server" />
                    <asp:Button ID="btnImagenAdd" runat="server" onclick="btnImagenAdd_Click" 
                        Text="Subir Imagen" CausesValidation="False" />
                    <br /> <br />
                     <div class="messages msg-info">
                     <asp:Label runat="server" ID="lblmessageFile"  Text="El peso máximo sugerido para los archivos es de  500 KB" ></asp:Label>
                    </div>
                    <br /> <br />
                    <strong>Descripción: </strong><br />
                    <asp:TextBox ID="DescripcionTextBox" runat="server"  onKeyDown="chekAndLimit(this,250)"
                        Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Actualizar" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                    
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                <asp:Panel ID="Panel4" runat="server" DefaultButton="InsertButton">
                    <asp:HiddenField ID="IdEventoTextBox" runat="server" Value='<%# Bind("IdEvento") %>' />
                    <br />
                    <strong>Imagen:</strong>
                   <br />
 <%--                        <asp:Image ID="Image1" runat="server" 
                                ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' />
                    <br />--%>

                    <asp:TextBox ID="ImagenTextBox" runat="server" Text='<%# Bind("Imagen") %>' 
                        ReadOnly="True" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="ImagenTextBox">*</asp:RequiredFieldValidator>
                    <br />
                    <asp:FileUpload ID="FUImagen" runat="server" />
                    <br />
                    <asp:Button ID="btnImagenAdd" runat="server" onclick="btnImagenAdd_Click" 
                        Text="Subir Imagen" CausesValidation="False" />
                    <br /> <br />
                    <div class="messages msg-info">
                     <asp:Label runat="server" ID="lblmessageFile"  Text="El peso máximo sugerido para los archivos es de  500 KB" ></asp:Label>
                    </div>
                    <br /> <br />
                    <strong>Descripción:</strong><br />
                    <asp:TextBox ID="DescripcionTextBox" runat="server"  onKeyDown="chekAndLimit(this,250)"
                        Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                        CommandName="Insert" Text="Agregar" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                    
                    </asp:Panel>
                </InsertItemTemplate>
                <ItemTemplate>
<%--                    IdImagen:
                    <asp:Label ID="IdImagenLabel" runat="server" Text='<%# Eval("IdImagen") %>' />
                    <br />
                    IdEvento:
                    <asp:Label ID="IdEventoLabel" runat="server" Text='<%# Bind("IdEvento") %>' />
                    <br />--%>
                    <strong> Imagen:</strong>
                    <asp:Label ID="ImagenLabel" runat="server" Text='<%# Bind("Imagen") %>' />
                    <br />
                    <strong>Descripción:</strong>
                    <asp:Label ID="DescripcionLabel" runat="server" 
                        Text='<%# Bind("Descripcion") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                        CommandName="Edit" Text="Editar" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                        CommandName="Delete" Text="Borrar" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                        CommandName="New" Text="Agregar" />
                </ItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ODSImagenesFV" runat="server" 
                DeleteMethod="ImagenDelete" InsertMethod="ImagenInsert" 
                OldValuesParameterFormatString="{0}" SelectMethod="ImagenSelect" 
                TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
                UpdateMethod="ImagenUpdate">
                <DeleteParameters>
                    <asp:Parameter Name="IdImagen" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Imagen" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="GVImagenes" Name="IdImagen" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="IdImagen" Type="Int32" />
                    <asp:Parameter Name="IdEvento" Type="Int32" />
                    <asp:Parameter Name="Imagen" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
            <br />
            <br />
            <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                Text="Volver a Eventos" />
        </asp:View>
    </asp:MultiView>
</asp:Content>
