﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="VoluntariadoReporte.aspx.cs" Inherits="com.paginar.johnson.Web.BO.VoluntariadoReporte" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperIndumentaria">
<h2>Reporte Voluntariado</h2>
<div class="form-item leftHalf">
<label>Estado</label>
&nbsp;<asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem Value="true">Todos</asp:ListItem>
        <asp:ListItem Value="false">Activos</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="form-item rightHalf">
      <asp:Button ID="Filtrar" runat="server" Text="Buscar" onclick="Filtrar_Click" />
        <asp:Button ID="btnExportar" runat="server" 
        Text="Exportar" onclick="btnExportar_Click" />
</div>
<div class="form-item leftHalf">
</div>
<div class="form-item rightHalf">
</div>
    <div>
        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
    </div>
    <asp:GridView ID="GVReporte" runat="server" AutoGenerateColumns="False" 
         DataKeyNames="IdActividad">
        <Columns>
            <asp:BoundField DataField="IdActividad" HeaderText="IdActividad" 
                InsertVisible="False" ReadOnly="True" SortExpression="IdActividad" 
                Visible="False" />
            <asp:BoundField DataField="Actividad" HeaderText="Actividad" 
                SortExpression="Actividad" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" 
                SortExpression="Descripcion" Visible="False" />
            <asp:BoundField DataField="Que" HeaderText="Que" SortExpression="Que" 
                Visible="False" />
            <asp:BoundField DataField="Cuando" HeaderText="Cuando" SortExpression="Cuando" 
                Visible="False" />
            <asp:BoundField DataField="AQuienes" HeaderText="AQuienes" 
                SortExpression="AQuienes" Visible="False" />
            <asp:BoundField DataField="Imagen" HeaderText="Imagen" SortExpression="Imagen" 
                Visible="False" />
            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Estado") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblEstado" runat="server" Text='<%# estado(Eval("Estado")) %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="legajo" HeaderText="legajo" 
                SortExpression="legajo" />
            <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                SortExpression="Apellido" />
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                SortExpression="Nombre" />
            <asp:BoundField DataField="FechaInscripsion" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="FechaInscripsion" SortExpression="FechaInscripsion" />
        </Columns>
    </asp:GridView>

    <asp:ObjectDataSource ID="ODSReporteVoluntariado" runat="server" 
         OldValuesParameterFormatString="original_{0}" 
         SelectMethod="vol_ActividadSelectReporte" 
         TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="todos" 
                PropertyName="SelectedValue" Type="Boolean" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>
</asp:Content>
