﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class NuestrasMarcasABM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AvisoCluster.Visible = false;
            el_panel.Visible = false;
            if (this.ObjectUsuario.clusteriddefecto == 1)
            {
                el_panel.Visible = true;
                if ((FormViewContenidos.CurrentMode == FormViewMode.Insert) ||
                    (FormViewContenidos.CurrentMode == FormViewMode.Edit))
                {
                    FormViewContenidos.Visible = true;
                }
               
            }
            else
            {
                AvisoCluster.Visible = true;
                
            }

        }

        protected void TreeViewContenido_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewContenido.SelectedNode.DataPath;
            FormViewContenidos.ChangeMode(FormViewMode.Edit);
            FormViewContenidos.DataBind();
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");
            DsContenido.Content_ItemsMenuUrlToolTipDataTable dt = cc.Content_ItemsMenuUrlToolTipGet(int.Parse(HiddenFieldItemID.Value));
            if (dt.Rows.Count > 0)
            {
                DsContenido.Content_ItemsMenuUrlToolTipRow dr = (DsContenido.Content_ItemsMenuUrlToolTipRow)dt.Rows[0];
                ToolTipTextBox.Text = dr.tooltip;
                LinkTextBox.Text = dr.navigateurl.Trim().Replace("javascript:", "").Replace("popup('", "").Replace("');", "").Trim();
            }

        }


        protected void FormViewContenidos_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            TreeViewContenido.DataBind();
        }

        protected void FormViewContenidos_DataBound(object sender, EventArgs e)
        {
            FormViewContenidos.Visible = true;
            if (FormViewContenidos.CurrentMode == FormViewMode.Edit)
                if (TreeViewContenido.SelectedNode != null)
                    if (TreeViewContenido.SelectedNode.Parent != null)
                    {
                        ListItem Li = new ListItem(TreeViewContenido.SelectedNode.Parent.Value, TreeViewContenido.SelectedNode.Parent.DataPath);
                        DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
                        parentIdDropDownList.Items.Add(Li);
                    }
                    else
                    {
                        FormViewContenidos.Visible = false;
                    }

        }

        protected void FormViewContenidos_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.NewValues["parentId"] = int.Parse(HiddenFieldItemParentID.Value);
            e.NewValues["Content_TipoID"] = int.Parse(HiddenFieldContentTipoID.Value);
            e.NewValues["Contenido"] = "-";
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");
            cc.Content_ItemsMenuUrlToolTipUpdate(int.Parse(FormViewContenidos.SelectedValue.ToString()), "javascript: popup('" + LinkTextBox.Text + "');", ToolTipTextBox.Text);
        }

        protected void ButtonCrearHijo_Click(object sender, EventArgs e)
        {
            FormViewContenidos.ChangeMode(FormViewMode.Insert);
        }

        protected void FormViewContenidos_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.Values["parentId"] = int.Parse(HiddenFieldItemParentID.Value);
            e.Values["Content_TipoID"] = int.Parse(HiddenFieldContentTipoID.Value);
            e.Values["Contenido"] = "-";
        }

        protected void FormViewContenidos_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            TreeViewContenido.DataBind();
        }

        protected void FormViewContenidos_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            TreeViewContenido.DataBind();
        }

        protected void ObjectDataSource1_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            int content_itemid = (int)e.ReturnValue;
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");
            cc.Content_ItemsMenuUrlToolTipAdd(content_itemid, "javascript: popup('" + LinkTextBox.Text + "');", ToolTipTextBox.Text);
            cc.Content_ItemsByClusterAdd(content_itemid, 1);//1 por q es solo Arg.
        }

        protected void FormViewContenidos_ItemDeleting(object sender, FormViewDeleteEventArgs e)
        {
            ControllerContenido cc = new ControllerContenido();
            cc.Content_ItemsMenuUrlToolTipDelete(int.Parse(FormViewContenidos.SelectedValue.ToString()));
            cc.Content_ItemsByClusterDelete(int.Parse(FormViewContenidos.SelectedValue.ToString()), 1); //1 por q es solo Arg.
        }



   

    }
}