﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="VoluntariadoABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.VoluntariadoABM" %>
<%@ Register Assembly="com.paginar.johnson.Web" Namespace="com.paginar.johnson.Web" TagPrefix="cc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc2" %>
<%@ Register assembly="com.paginar.johnson.BL" namespace="com.paginar.HierarchicalDataSource" tagprefix="cc1" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">

  <h2>Voluntariado Corporativo de SCJ Argentina</h2>

    <div id="accordion">
    <h3><a href="#">Contenido</a></h3> 
    <div class="panelColOne">

    <asp:TreeView ID="TreeViewContenido" runat="server" 
        DataSourceID="RelationalSystemDS" 
                    onselectednodechanged="TreeViewContenido_SelectedNodeChanged">
         <DataBindings>
                    <asp:TreeNodeBinding TextField="Name" />
                </DataBindings>
                <SelectedNodeStyle Font-Bold="True" />
    </asp:TreeView>
    <cc1:RelationalSystemDataSource  
        runat="server" Path="1" ID="RelationalSystemDS" IncludeRoot="true" />
                <asp:Button ID="ButtonCrearHijo" runat="server" Text="Crear" 
                    onclick="ButtonCrearHijo_Click" />
     </div>
     <h3><a href="#">Nuevo/Modificar</a></h3> 
     <div class="panelColTwo">
                <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
                <asp:HiddenField ID="HiddenFieldContentTipoID" runat="server" />
                <asp:FormView ID="FormViewContenidos" runat="server" DataKeyNames="Content_ItemId" 
                    DataSourceID="ObjectDataSource1" 
                    onitemupdated="FormViewContenidos_ItemUpdated" 
                    ondatabound="FormViewContenidos_DataBound" 
                    onitemupdating="FormViewContenidos_ItemUpdating" 
                    oniteminserting="FormViewContenidos_ItemInserting" 
                    oniteminserted="FormViewContenidos_ItemInserted" 
                    
                    onitemdeleted="FormViewContenidos_ItemDeleted">
                    <EditItemTemplate>
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Content_ItemId") %>'/>
                        <table>
                            <tr>
                                <td>
                                    <label>
                                    Contenido Superior</label></td>
                                <td>
                                    <asp:DropDownList ID="parentIdDropDownList" 
                                        runat="server" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <label>
                                    Titulo</label></td>
                                <td>
                                    
                                    <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' 
                                        TextMode="MultiLine" Width="360px" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ErrorMessage="Agregar Titulo" ControlToValidate="TituloTextBox" 
                                        Display="Static"  ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
</td>
                            </tr>
                            <tr>
                                <td valign="top">                                    
                                    <label>
                                    Contenido</label></td>
                                <td>
                                    
                                    &nbsp;</td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Contenido") %>' Width="500px" />
                                    
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ErrorMessage="Agregar Contenido"  ControlToValidate="Editor1" 
                                        Display="Static" ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>                               
                                <td colspan="3">                                   
                                    <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Editar"/>
                                        
                                        <asp:Button ID="BtnActualizar"
                                            runat="server" Text="Actualizar"  CommandName="Update" ValidationGroup="Editar"/>
                                    &nbsp;<asp:Button ID="Button1" runat="server" CommandName="Delete" 
                                        Text="Eliminar" 
                                        onclientclick="return confirm('Desea eliminar este registro de forma permanente?');" />&nbsp;
                                     <asp:Button ID="Button3"
                                            runat="server" CausesValidation="False" Text="Cancelar"  CommandName="Cancel"/>
                                    
                                    &nbsp;
                                </td>
                            </tr>
                            
                        </table>
                       
                            
                        
                        
                                        
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                    <table>
                            <tr>
                                <td>
                                    <label>
                                    Contenido Superior</label></td>
                                <td>
                                    <asp:DropDownList ID="parentIdDropDownList" 
                                        runat="server" Enabled="false">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <label>
                                    Titulo</label></td>
                                <td>
                                    
                                    <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' 
                                        TextMode="MultiLine" Width="360px" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ErrorMessage="Agregar Titulo" ControlToValidate="TituloTextBox" 
                                        Display="Static"  ValidationGroup="Agregar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
</td>
                            </tr>
                            <tr>
                                <td valign="top">                                    
                                    <label>
                                    Contenido</label></td>
                                <td>
                                    
                                    &nbsp;</td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Contenido") %>' Width="500px" />
                                    
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ErrorMessage="Agregar Contenido"  ControlToValidate="Editor1" 
                                        Display="Static" ValidationGroup="Agregar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>                               
                                <td colspan="3">                                   
                                    <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Agregar"/>
                                        
                                        <asp:Button ID="BtnAgregar"
                                            runat="server" Text="Agregar"  CommandName="Insert" ValidationGroup="Agregar"/>
   &nbsp;
                                     <asp:Button ID="Button3"
                                            runat="server" CausesValidation="False" Text="Cancelar"  CommandName="Cancel"/>
                                    
                                    
                                </td>
                            </tr>
                            
                        </table>
                        
                    </InsertItemTemplate>
                    
                </asp:FormView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetContentById" 
                    TypeName="com.paginar.johnson.BL.ControllerContenido" 
                    UpdateMethod="UpdateContent_Items" InsertMethod="InsertContent_Items" DeleteMethod="DeleteContent_Items">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" 
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Content_ItemId" Type="Int32" />
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Contenido" Type="String" />
                        <asp:Parameter Name="parentId" Type="Int32" />
                    </UpdateParameters>
                    <DeleteParameters>
                    <asp:Parameter Name="Content_ItemId" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>                    
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Contenido" Type="String" />
                        <asp:Parameter Name="parentId" Type="Int32" />
                    </InsertParameters>
                </asp:ObjectDataSource>
    </div>
    </div><!--/panel2Col-->    
</asp:Content>