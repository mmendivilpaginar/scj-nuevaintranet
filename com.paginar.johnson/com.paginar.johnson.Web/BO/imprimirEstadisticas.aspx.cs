﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Diagnostics;

using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting; 

namespace com.paginar.johnson.Web.BO
{
    public partial class imprimirEstadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Chart1.ChartAreas[0].AxisX.Interval = 1;
            Chart2.ChartAreas[0].AxisX.Interval = 1;
            Chart3.ChartAreas[0].AxisX.Interval = 1;
            Chart4.ChartAreas[0].AxisX.Interval = 1;
            Chart5.ChartAreas[0].AxisX.Interval = 1;
            Chart6.ChartAreas[0].AxisX.Interval = 1;


            Chart2.ChartAreas[0].AxisX.IsReversed = true;
            Chart6.ChartAreas[0].AxisX.IsReversed = true;
            if (!Page.IsPostBack)
            {
                ControllerEstadisticas CRTE = new ControllerEstadisticas();
                string indicador = Request.QueryString["indicador"];
                string tipo = Request.QueryString["tipo"];
                setIndicador(indicador);

                if (tipo == "Reporte")
                {
                    switch (indicador)
                    {
                        case "user_access_site":
                            string SortDirection;
                            MVEstadisticas.SetActiveView(user_access_site);
                            MVEstadisticas.Visible = true;
                            DSEstadisticas.LogDataTable DTuser_access_site = new DSEstadisticas.LogDataTable();
                            DTuser_access_site = CRTE.GetEstadisticas_user_access_site(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));
                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_user_access_site = new DataView(DTuser_access_site);
                                VW_user_access_site.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView1.DataSource = VW_user_access_site;
                            }
                            else
                                GridView1.DataSource = DTuser_access_site;

                            GridView1.AllowPaging = false;
                            GridView1.DataBind();
                            break;

                        case "secc_mas_consult":
                            MVEstadisticas.SetActiveView(secc_mas_consult);
                            MVEstadisticas.Visible = true;
                            GridView2.AllowPaging = false;

                            DSEstadisticas.SeccionConsultadaDataTable DT_secc_mas_consult = new DSEstadisticas.SeccionConsultadaDataTable();
                            DT_secc_mas_consult = CRTE.Get_seccion_mas_consultada(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_secc_mas_consult = new DataView(DT_secc_mas_consult);
                                VW_secc_mas_consult.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView2.DataSource = VW_secc_mas_consult;
                            }
                            else
                                GridView2.DataSource = DT_secc_mas_consult;

                            GridView2.DataBind();
                            break;

                        case "grup_etario":
                            MVEstadisticas.SetActiveView(grup_etario);
                            MVEstadisticas.Visible = true;
                            GridView3.AllowPaging = false;

                            DSEstadisticas.GruposEtariosDataTable DT_grup_etario = new DSEstadisticas.GruposEtariosDataTable();
                            DT_grup_etario = CRTE.Get_grupos_etarios(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_grup_etario = new DataView(DT_grup_etario);
                                VW_grup_etario.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView3.DataSource = VW_grup_etario;
                            }
                            else
                                GridView3.DataSource = DT_grup_etario;
                            GridView3.DataBind();
                            break;

                        case "hombres_mujeres":
                            MVEstadisticas.SetActiveView(hombres_mujeres);
                            MVEstadisticas.Visible = true;
                            GridView4.AllowPaging = false;
                            DSEstadisticas.Hombres_MujeresDataTable DT_hombres_mujeres = new DSEstadisticas.Hombres_MujeresDataTable();
                            DT_hombres_mujeres = CRTE.Get_Hombres_Mujeres(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_hombres_mujeres = new DataView(DT_hombres_mujeres);
                                VW_hombres_mujeres.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView4.DataSource = VW_hombres_mujeres;
                            }
                            else
                                GridView4.DataSource = DT_hombres_mujeres;

                            GridView4.DataBind();
                            break;

                        case "hora_mayor_concurrencia":

                            MVEstadisticas.SetActiveView(hora_mayor_concurrencia);
                            MVEstadisticas.Visible = true;
                            GridView5.AllowPaging = false;

                            DSEstadisticas.HorariosConcurrenciaDataTable DT_hora_mayor_concurrencia = new DSEstadisticas.HorariosConcurrenciaDataTable();
                            DT_hora_mayor_concurrencia = CRTE.Get_horarios_concurrencia(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_hora_mayor_concurrencia = new DataView(DT_hora_mayor_concurrencia);
                                VW_hora_mayor_concurrencia.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView5.DataSource = VW_hora_mayor_concurrencia;
                            }
                            else
                                GridView5.DataSource = DT_hora_mayor_concurrencia;

                            GridView5.DataBind();

                            break;

                        case "noticia_mas_visitada":
                            MVEstadisticas.SetActiveView(noticia_mas_visitada);
                            MVEstadisticas.Visible = true;
                            GridView6.AllowPaging = false;
                            DSEstadisticas.TopNoticiasDataTable DT_noticia_mas_visitada = new DSEstadisticas.TopNoticiasDataTable();
                            DT_noticia_mas_visitada = CRTE.Get_top_noticias(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_noticia_mas_visitada = new DataView(DT_noticia_mas_visitada);
                                VW_noticia_mas_visitada.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView6.DataSource = VW_noticia_mas_visitada;
                            }
                            else
                                GridView6.DataSource = DT_noticia_mas_visitada;

                            GridView6.DataBind();

                            break;
                        case "Banner_mas_visitado":
                            lblIndicador.Text = "Clicks en banners";
                            MVEstadisticas.SetActiveView(banner_mas_consultado);
                            MVEstadisticas.Visible = true;
                            GridView7.AllowPaging = false;
                            DSEstadisticas.SeccionConsultadaDataTable DT_banner_mas_visitado = new DSEstadisticas.SeccionConsultadaDataTable();
                            DT_banner_mas_visitado = CRTE.Get_banner_mas_consultado(Request.QueryString["modo"].ToString(), DateTime.Parse(Request.QueryString["desde"]), DateTime.Parse(Request.QueryString["hasta"]));
                                

                            if (!string.IsNullOrEmpty(Request.QueryString["SortExpression"].ToString()))
                            {
                                if (Request.QueryString["SortDirection"].ToString() == "Descending")
                                    SortDirection = "Desc";
                                else
                                    SortDirection = "Asc";

                                DataView VW_secc_mas_consult = new DataView(DT_banner_mas_visitado);
                                VW_secc_mas_consult.Sort = Request.QueryString["SortExpression"].ToString() + " " + SortDirection;
                                GridView7.DataSource = VW_secc_mas_consult;
                            }
                            else
                                GridView7.DataSource = DT_banner_mas_visitado;

                            GridView7.DataBind();

                            break;

                        default:
                            MVEstadisticas.Visible = false;
                            break;

                    }
                }
                else if (tipo == "Grafica")
                {
                    MVEstadisticas.Visible = false;
                    MVGrafica.Visible = true;
                    switch (indicador)
                    {
                        case "user_access_site":
                            MVGrafica.SetActiveView(Guser_access_site);
                            Chart1.DataBind();

                            break;

                        case "secc_mas_consult":
                            MVGrafica.SetActiveView(Gsecc_mas_consult);
                            Chart2.DataBind();

                            break;

                        case "grup_etario":
                            MVGrafica.SetActiveView(Ggrup_etario);
                            Chart3.DataBind();

                            break;

                        case "hombres_mujeres":
                            MVGrafica.SetActiveView(Ghombres_mujeres);
                            Chart4.DataBind();

                            break;

                        case "hora_mayor_concurrencia":
                            MVGrafica.SetActiveView(Ghora_mayor_concurrencia);
                            Chart5.DataBind();

                            break;

                        case "noticia_mas_visitada":
                            MVGrafica.SetActiveView(Gnoticia_mas_visitada);
                            Chart6.DataBind();

                            break;
                        default:
                            MVGrafica.Visible = true;
                            break;

                    }
                }
                string modo = Request.QueryString["modo"];
                string modoyfecha = string.Empty;

                switch (modo)
                {
                    case "Dias":
                        modoyfecha = "<strong>Unidad de Consulta:</strong> Diario";
                        break;

                    case "Semanal":
                        modoyfecha = "<strong>Unidad de Consulta:</strong> Semanal";
                        break;

                    case "Mensual":
                        modoyfecha = "<strong>Unidad de Consulta:</strong> Mensual";
                        break;

                    case "Anual":
                        modoyfecha = "<strong>Unidad de Consulta:</strong> Anual";
                        break;

                }

                modoyfecha += "<br /><strong>Rango:</strong> " + Request.QueryString["desde"] + " - " + Request.QueryString["hasta"];
                lblTitIndicador.Text = modoyfecha;
            }
             

        }

        private ControllerNoticias cnr = new ControllerNoticias();

        public void setIndicador(string indicador)
        {
            switch (indicador)
            {
                case "user_access_site":

                    lblIndicador.Text = "Usuarios que accedieron al sitio";

                    break;

                case "secc_mas_consult":
                    lblIndicador.Text = "Sección más consultada";

                    break;

                case "grup_etario":

                    lblIndicador.Text = "Grupo etario";
                    break;

                case "hombres_mujeres":

                    lblIndicador.Text = "Cantidad de hombres y mujeres";
                    break;

                case "hora_mayor_concurrencia":

                    lblIndicador.Text = "Horario de mayor concurrencia";

                    break;

                case "noticia_mas_visitada":

                    lblIndicador.Text = "Noticia más visitada";

                    break;
                default:
                    lblIndicador.Text = "";
                    break;

            }        
        }


        public string devolverTituloNoticia(string url)
        {
            int elid;
            string titulo = string.Empty;
            string[] cadena = url.Split('=');
            elid = int.Parse(cadena[1]);
            DSNoticias.infInfoDataTable noticia = cnr.GetInfInfoByInfoID(elid);
            foreach (DSNoticias.infInfoRow NotiRow in noticia.Rows)
            {
                titulo = NotiRow.Titulo.ToString();
            }
            return titulo;
        }

        public string devolverIdNoticia(string url)
        {

            string titulo = string.Empty;
            string[] cadena = url.Split('=');
            return cadena[1];
        }


        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView1.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("LBLCantUuser_access")).Text);

            }

            GridViewRow rowFoot = GridView1.FooterRow;
            if (rowFoot != null)
            {
                //LBLUsrTotSub
                Label LBLTotalesUser_access = (Label)rowFoot.FindControl("LBLUsrTotSub");


                    LBLTotalesUser_access.Text = "Total: ";

                Label LabelTotal = (Label)rowFoot.FindControl("LBLTotalUser_access");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }

        }
        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            
            foreach (GridViewRow row in GridView2.Rows)
            {
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\" >" + ((Label)row.FindControl("Link")).Text + "</a>";
                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text);

            }

            GridViewRow rowFoot = GridView2.FooterRow;
            if (rowFoot != null)
            {

                Label LBLTotalesSeccionConsultada = (Label)rowFoot.FindControl("lblTotalSecc_mas_visitada");

                    LBLTotalesSeccionConsultada.Text = "Total: ";
             
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGenSecMasCons");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }


        }


        protected void GridView3_DataBound(object sender, EventArgs e)
        {

            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView3.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text);

            }

            GridViewRow rowFoot = GridView3.FooterRow;
            if (rowFoot != null)
            {
                //LBLUsrTotSub
                Label LBLTotalesGrupoEtario = (Label)rowFoot.FindControl("lblTotSubTotGrupoEtario");


                    LBLTotalesGrupoEtario.Text = "Total: ";

                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGrupoEtario");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }

        }

        protected void GridView4_DataBound(object sender, EventArgs e)
        {

            int sumtotaltotal = 0;
            int TotalHombres = 0;
            int TotalMujeresa = 0;
            foreach (GridViewRow row in GridView4.Rows)
            {

                TotalHombres += int.Parse(((Label)row.FindControl("lblhombresunidades")).Text);
                TotalMujeresa += int.Parse(((Label)row.FindControl("lblmujeresunidades")).Text);
                ((Label)row.FindControl("lblhombresymujeresunidad")).Text = (int.Parse(((Label)row.FindControl("lblhombresunidades")).Text) + int.Parse(((Label)row.FindControl("lblmujeresunidades")).Text)).ToString();


            }
            sumtotaltotal = TotalMujeresa + TotalHombres;
            GridViewRow rowFoot = GridView4.FooterRow;
            if (rowFoot != null)
            {

                Label lblTotSubTotetiq = (Label)rowFoot.FindControl("lblTotSubTotetiq");



                    lblTotSubTotetiq.Text = "Total: ";



                Label LabelTotalH = (Label)rowFoot.FindControl("lblhombrestotales");
                Label LabelTotalM = (Label)rowFoot.FindControl("lblmujerestotales");
                Label lblTotalesTHM = (Label)rowFoot.FindControl("lblTotalesTHM");

                lblTotalesTHM.Text = sumtotaltotal.ToString();
                LabelTotalH.Text = TotalHombres.ToString();
                LabelTotalM.Text = TotalMujeresa.ToString();
            }


        }


        protected void GridView5_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
       
            foreach (GridViewRow row in GridView5.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblTotUnitHorConc")).Text);
        

            }

            GridViewRow rowFoot = GridView5.FooterRow;
            if (rowFoot != null)
            {
                Label LBLTotalesTotMayConc = (Label)rowFoot.FindControl("lblTitTotMayConc");


                    LBLTotalesTotMayConc.Text = "Total: ";

                Label LabelTotal = (Label)rowFoot.FindControl("lblTotGralHorConc");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }


        }


        protected void GridView6_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;

            foreach (GridViewRow row in GridView6.Rows)
            {
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\">" + devolverTituloNoticia(((Label)row.FindControl("Label2")).Text) + "</a>";
                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblTotUnitTopNot")).Text);

            }

            GridViewRow rowFoot = GridView6.FooterRow;
            if (rowFoot != null)
            {

                Label lblTotNot = (Label)rowFoot.FindControl("lblTitTotNot");

                    lblTotNot.Text = "Total: ";

                Label LabelTotal = (Label)rowFoot.FindControl("lblTotGralTopNot");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }
        }


        protected void GridView7_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView7.Rows)
            {
                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text);
                string tituloCad = "&titulo=" + ((Label)row.FindControl("Link")).Text;
                string indicador = "&indicador=Clicks sobre Banners";
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\" >" + ((Label)row.FindControl("Link")).Text + "</a>";
                string modo = Request.QueryString["modo"];
                switch (modo)
                {
                    case "Dias":

                     //   ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Semanal":

                        string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                     //   ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Mensual":

                      //  string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                      //  ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Anual":
                       // ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;

                }

            }

            GridViewRow rowFoot = GridView7.FooterRow;
            if (rowFoot != null)
            {

                Label LBLTotalesSeccionConsultada = (Label)rowFoot.FindControl("lblTotalSecc_mas_visitada");

                if (GridView7.AllowPaging == true && GridView7.PageCount > 1)
                {
                    LBLTotalesSeccionConsultada.Text = "Sub Total: ";
                   // BTNVTGV2.Visible = true;
                }
                else
                {
                    LBLTotalesSeccionConsultada.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGenSecMasCons");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }


        }

    }
}