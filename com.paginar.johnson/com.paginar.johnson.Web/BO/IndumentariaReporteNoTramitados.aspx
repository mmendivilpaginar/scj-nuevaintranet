﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="IndumentariaReporteNoTramitados.aspx.cs" Inherits="com.paginar.johnson.Web.BO.IndumentariaReporteNoTramitados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Ropa de trabajo - Trámites no iniciados</h2>
<div id="wrapperIndumentaria">
<div class="form-item leftHalf">
        <label>Período</label>
        <asp:DropDownList ID="DDLPeriodo" runat="server" AppendDataBoundItems="True" 
            ondatabound="DDLPeriodo_DataBound">
        </asp:DropDownList>
</div>
<div class="form-item rightHalf">

</div>
<div class="form-item form-end">
    <asp:Button ID="Button1" runat="server" Text="Buscar" onclick="Button1_Click" />
    <asp:Button ID="Button2" runat="server" Text="Exportar" 
        onclick="Button2_Click" />
</div>
    <asp:Panel ID="PanelContenidos" runat="server">
        <asp:GridView ID="GVReporteNoTramitados" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" DataSourceID="ODSUsuariosNoTramitaron">
            <Columns>
                <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                    SortExpression="Legajo" />
                <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                    SortExpression="Apellido" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                    SortExpression="Nombre" />
                <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                    SortExpression="Area" />
                <asp:BoundField DataField="Direccion" HeaderText="Dirección" ReadOnly="True" 
                    SortExpression="Direccion" />
                <asp:BoundField DataField="Rol" HeaderText="Tipo" ReadOnly="True" 
                    SortExpression="Rol" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSUsuariosNoTramitaron" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="getUsuariosNoIniciaronTramite" 
            TypeName="com.paginar.johnson.BL.ControllerIndumentaria">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLPeriodo" Name="periodoid" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
</div>
</asp:Content>
