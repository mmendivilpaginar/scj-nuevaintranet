﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Net.Mail;

namespace com.paginar.johnson.Web.BO
{
    public partial class NewsLetterVistaPrevia : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                ControllerNewsletter cn = new ControllerNewsletter();


                if (Request["NotidiasIDs"] != null)
                {
                    
                    string path = Server.MapPath("~");
                    AlternateView htmlView = null;
                    hdPreview.Text =  cn.getBodyNewsletter(GetUrlSite(), 
                        cn.getNoticias("100", 0, "", Request["NotidiasIDs"].ToString(), null), 
                        Request["Fecha"].ToString(),
                        Request["Nro"].ToString(), path, true, ref htmlView).ToString();
                }



      

            }
        }



        private string formatFecha(int dia, int mes, int anio)
        {
            string strmes = "";
            switch (mes)
            {
                case 1:
                    strmes = "Enero";
                    break;
                case 2:
                    strmes = "Febrero";
                    break;
                case 3:
                    strmes = "Marzo";
                    break;
                case 4:
                    strmes = "Abril";
                    break;
                case 5:
                    strmes = "Mayo";
                    break;

                case 6:
                    strmes = "Junio";
                    break;

                case 7:
                    strmes = "Julio";
                    break;

                case 8:
                    strmes = "Agosto";
                    break;

                case 9:
                    strmes = "Septiembre";
                    break;

                case 10:
                    strmes = "Octubre";
                    break;

                case 11:
                    strmes = "Noviembre";
                    break;

                case 12:
                    strmes = "Diciembre";
                    break;

            }



            return dia + " de " + strmes + " de " + anio;
        }
        protected string GetUrlSite()
        {
            string url = this.ASP_SITE.ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return this.ASP_SITE.ToString().Replace(":8080", "");//produccion
        }


    }
}