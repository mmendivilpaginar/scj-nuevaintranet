﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="NuestrasMarcasABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.NuestrasMarcasABM" %>
<%@ Register Assembly="com.paginar.johnson.Web" Namespace="com.paginar.johnson.Web"
    TagPrefix="cc3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register assembly="com.paginar.johnson.BL" namespace="com.paginar.HierarchicalDataSource" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Nuestras Marcas</h2>

    <asp:Label ID="AvisoCluster" runat="server" Text="Sección no habilitada" 
        Visible="False"></asp:Label>

    <div class="panel panel2Col" id="el_panel" runat="server">
        <div class="panelColOne">
            <!--[contenido]-->
            <asp:TreeView ID="TreeViewContenido" runat="server" 
                DataSourceID="RelationalSystemDS" 
                            onselectednodechanged="TreeViewContenido_SelectedNodeChanged">
                 <DataBindings>
                            <asp:TreeNodeBinding TextField="Name" />
                        </DataBindings>
                        <SelectedNodeStyle Font-Bold="True" />
            </asp:TreeView>
            <cc1:RelationalSystemDataSource  
                runat="server" Path="43" ID="RelationalSystemDS" IncludeRoot="true" />
            <asp:Button ID="ButtonCrearHijo" runat="server" Text="Nuevo" onclick="ButtonCrearHijo_Click" />
            <!--[/contenido]-->
        </div>
        <div class="panelColTwo">
            <!--[contenido]-->
            <asp:HiddenField ID="HiddenFieldItemID" runat="server"  />                
            <asp:HiddenField ID="HiddenFieldItemParentID" runat="server" Value="43" />    
            <asp:HiddenField ID="HiddenFieldContentTipoID" runat="server" Value="4" />
            <asp:FormView ID="FormViewContenidos" runat="server" DataKeyNames="Content_ItemId" 
                DataSourceID="ObjectDataSource1" 
                onitemupdated="FormViewContenidos_ItemUpdated" 
                ondatabound="FormViewContenidos_DataBound" 
                onitemupdating="FormViewContenidos_ItemUpdating" 
                oniteminserting="FormViewContenidos_ItemInserting" 
                oniteminserted="FormViewContenidos_ItemInserted" 
                onitemdeleted="FormViewContenidos_ItemDeleted" 
                onitemdeleting="FormViewContenidos_ItemDeleting">
                <EditItemTemplate>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="BtnActualizar">
                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Content_ItemId") %>'/>
                    <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Editar"/>
                    <div class="form-item">
                        <asp:DropDownList ID="parentIdDropDownList"  Visible="false"
                            runat="server" Enabled="false">
                        </asp:DropDownList>
                    </div>
                    <div class="form-item">
                        <label>Titulo</label>
                        <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="Agregar Titulo" ControlToValidate="TituloTextBox" 
                            Display="Dynamic"  ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item">
                        <label>Link</label>
                        <asp:TextBox ID="LinkTextBox" runat="server" />
                        <br />
                        Ej. http://www.sitio.com
                        <asp:RequiredFieldValidator ID="rfLink" runat="server"
                            ErrorMessage="Agregar Link" ControlToValidate="LinkTextBox" 
                            Display="Static"  ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reLink" runat="server" Display="Dynamic"
                            ControlToValidate="LinkTextBox" ErrorMessage="Link no Válido" 
                            ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" 
                            ValidationGroup="Editar"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <label>ToolTip</label>
                        <asp:TextBox ID="ToolTipTextBox" runat="server" />
                        <asp:HiddenField ID="ContenidoHiddem" runat="server" Value='<%# Bind("Contenido") %>' />
                    </div>
                    <div class="controls">
                        <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar"  CommandName="Update" ValidationGroup="Editar"/>
                        <asp:Button ID="Button1" runat="server" CommandName="Delete" Text="Eliminar" onclientclick="return confirm('Desea eliminar este registro de forma permanente?');" />
                        <asp:Button ID="Button3" runat="server" CausesValidation="False" Text="Cancelar"  CommandName="Cancel"/>
                    </div>
                    
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="BtnAgregar">
                    <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Agregar"/>
                    <div class="form-item">
                        <asp:DropDownList ID="parentIdDropDownList" Visible="false" runat="server" Enabled="false"></asp:DropDownList>
                    </div>
                    <div class="form-item">
                        <label>Titulo</label>
                        <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="Agregar Titulo" ControlToValidate="TituloTextBox" 
                            Display="Dynamic"  ValidationGroup="Agregar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item">
                        <label>Link</label>
                        <asp:TextBox ID="LinkTextBox" runat="server" />
                        <br />
                        Ej. http://www.sitio.com
                        <asp:RequiredFieldValidator ID="rfLink" runat="server"
                            ErrorMessage="Agregar Link" ControlToValidate="LinkTextBox" 
                            Display="Dynamic"  ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reLink" runat="server" 
                            ControlToValidate="LinkTextBox" ErrorMessage="Link no Válido" Display="Dynamic"
                            ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" 
                            ValidationGroup="Editar"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-item">
                        <label>ToolTip</label>
                        <asp:TextBox ID="ToolTipTextBox" runat="server" Width="360px" />
                        <asp:HiddenField ID="ContenidoHiddem" runat="server" Value='<%# Bind("Contenido") %>' />
                    </div>
                    <div class="controls">
                        <asp:Button ID="BtnAgregar" runat="server" Text="Agregar"  CommandName="Insert" ValidationGroup="Agregar"/>
                        <asp:Button ID="Button3" runat="server" CausesValidation="False" Text="Cancelar"  CommandName="Cancel"/>
                    </div>
                    
                    </asp:Panel>  
                </InsertItemTemplate>
                    
            </asp:FormView>

                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetContentById" 
                TypeName="com.paginar.johnson.BL.ControllerContenido" 
                UpdateMethod="UpdateContent_Items" InsertMethod="InsertContent_Items" 
                DeleteMethod="DeleteContent_Items" oninserted="ObjectDataSource1_Inserted">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" 
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Content_ItemId" Type="Int32" />
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Contenido" Type="String" />
                    <asp:Parameter Name="parentId" Type="Int32" />
                </UpdateParameters>
                <DeleteParameters>
                <asp:Parameter Name="Content_ItemId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>                    
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Contenido" Type="String" />
                    <asp:Parameter Name="parentId" Type="Int32" />
                </InsertParameters>
            </asp:ObjectDataSource>
                

            <!--[/contenido]-->
        </div>
    </div><!--/panel2Col-->

</asp:Content>
