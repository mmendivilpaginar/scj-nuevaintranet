﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.IO;
using com.paginar.johnson.utiles;
using FredCK.FCKeditorV2;

namespace com.paginar.johnson.Web.BO
{
    public partial class imprimirNoticiasABM : System.Web.UI.Page
    {
        private ControllerNoticias _NC;
        public ControllerNoticias NC
        {
            get
            {
                _NC = (ControllerNoticias)this.Session["NC"];
                if (_NC == null)
                {
                    _NC = new ControllerNoticias();
                    this.Session["NC"] = _NC;
                }
                return _NC;
            }
            set
            {
                this.Session["NC"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CategoriasDropDownList.DataBind();
                if (Request.QueryString["Estado"] != null)
                {
                    EstadoDropDownList.SelectedValue = Request.QueryString["Estado"];
                }

                if (Request.QueryString["Destacados"] != null)
                {
                    DestacadosDropDownList.SelectedValue = Request.QueryString["Destacados"];
                }

                if (Request.QueryString["Categorias"] != null)
                {
                    CategoriasDropDownList.SelectedValue = Request.QueryString["Categorias"];
                }

                if (Request.QueryString["BuscarPor"] != null)
                {
                    BuscarPorDropDownList.SelectedValue = Request.QueryString["BuscarPor"];
                }

                if (Request.QueryString["Desde"] != null)
                {
                    DesdeTextBox.Text = Request.QueryString["Desde"];
                }

                if (Request.QueryString["Hasta"] != null)
                {
                    HastaTextBox.Text = Request.QueryString["Hasta"];
                }

    
                
                //"Estado=" + EstadoDropDownList.SelectedValue.ToString() + "&Destacados=" + DestacadosDropDownList.SelectedValue.ToString() + 
                //"&Categorias=" + CategoriasDropDownList.SelectedValue.ToString() + "&BuscarPor=" + BuscarPorDropDownList.SelectedValue.ToString() + 
                //"&Desde=" + DesdeTextBox.Text.ToString() + "&Hasta=" + HastaTextBox.Text.ToString();

                NC = new ControllerNoticias();
                GVNoticias.DataBind();
            }

            GVNoticias.Visible = true;
            //ODSNotiImp.DataBind();
            //
            //BuscarButton_Click(BuscarButton, null);
        }
        protected void BuscarButton_Click(object sender, EventArgs e)
        {
            GVNoticias.Visible = true;
            GVNoticias.DataBind();
        }

        public string RemoveFONT(string strText)
        {
            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<[/]?(font|span|xml|del|ins|[ovwxp]:\\w+)[^>]*?>");
            return regEx.Replace(strText, "");
        }

        protected string GetIntString(object Valor)
        {
            Int32? ValorInt = null;
            if (Valor != null)
            {
                if (Valor.ToString() == string.Empty) return string.Empty;
                ValorInt = int.Parse(Valor.ToString());
            }
            else
                return string.Empty;

            if (((Int32)ValorInt) == 1)
                return "Si";
            else
                return "No";

        }

        protected string GetUrl(Int32 InfoID, int Tipo, Int32 privacidad)
        {
            //string UrlPagina = string.Format( "/novedades/{0}?InfoID={1}",(Tipo==1)?"nota.asp":"notasnl.aspx",InfoID);/noticias/noticia.aspx
            string UrlPagina = string.Empty;
            if (privacidad == 1)
            {
                UrlPagina = string.Format("noticias/{0}InfoID={1}", (Tipo == 1) ? "noticia.aspx?priv=" + CodValidacion(InfoID) + "&" : "urlNoticia.aspx?priv=" + CodValidacion(InfoID) + "&", InfoID);
            }
            else
            {
                UrlPagina = string.Format("noticias/{0}InfoID={1}", (Tipo == 1) ? "noticia.aspx?" : "urlNoticia.aspx?", InfoID);
            }

            //1: Intra, 2 News
            string URL = "http://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath;
            return URL + UrlPagina;
        }


        protected string CodValidacion(int infoid)
        {
            int codigo;
            string retorno = string.Empty;
            codigo = infoid % 15;
            switch (codigo)
            {
                case 0:
                    retorno = "2A55S";
                    break;
                case 1:
                    retorno = "2EW85";
                    break;
                case 2:
                    retorno = "DKE55";
                    break;
                case 3:
                    retorno = "SAP2Z";
                    break;
                case 4:
                    retorno = "LS5S2";
                    break;
                case 5:
                    retorno = "6SDRS";
                    break;
                case 6:
                    retorno = "ASE3Q";
                    break;
                case 7:
                    retorno = "WY7T4";
                    break;
                case 8:
                    retorno = "8ZW8S";
                    break;
                case 9:
                    retorno = "8ERSA";
                    break;
                case 10:
                    retorno = "3B843";
                    break;
                case 11:
                    retorno = "YYW5S";
                    break;
                case 12:
                    retorno = "P8SD9";
                    break;
                case 13:
                    retorno = "W2S3E";
                    break;
                case 14:
                    retorno = "Q31RT";
                    break;

            }
            return retorno;
        }

        protected void ODSNotiImp_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {

                //if (Request.QueryString["Estado"] != null)
                //{
                //    e.InputParameters["Estado"] = Request.QueryString["Estado"];
                //}

                //if (Request.QueryString["Destacados"] != null)
                //{
                //    e.InputParameters["Destacado"] = Request.QueryString["Destacados"];
                //}

                //if (Request.QueryString["Categorias"] != null)
                //{
                //    e.InputParameters["CategoriaID"] = Request.QueryString["Categorias"];
                //}

                //if (Request.QueryString["BuscarPor"] != null)
                //{
                //    e.InputParameters["TipoBusq"] = Request.QueryString["BuscarPor"];
                //}

                //if (Request.QueryString["Desde"] != null)
                //{
                //    e.InputParameters["Desde"] = Request.QueryString["Desde"];
                //}

                //if (Request.QueryString["Hasta"] != null)
                //{
                //    e.InputParameters["Hasta"] = Request.QueryString["Hasta"];
                //}


                
          //  e.InputParameters[""]
        }

        protected void ODSNotiImp_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            
            DSNoticias.infInfoDataTable NoticiasImprimir = (DSNoticias.infInfoDataTable)e.ReturnValue;
            if (NoticiasImprimir.Count > 0)
                Button3.Enabled = true;
            else
                Button3.Enabled = false;
            
        }




    }
}