﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="VoluntariadoActividadesABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.VoluntariadoActividadesABM1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">



    function chekAndLimit(elemento, max) {
        txt = elemento;
        MaxLength = max;        
        if (txt.value.length > (MaxLength - 1)) {
            document.getElementById(elemento.id).value = document.getElementById(elemento.id).value.substring(0, MaxLength - 1);
        }
        else return true;

        }
    

</script>
<style type="text/css">
    .AspNet-GridView
    {
        overflow-x: auto !important;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Administración de Actividades de Voluntariado</h2>
    <asp:GridView ID="GVActividades" runat="server" DataSourceID="ODSActividades" 
        AutoGenerateColumns="False" DataKeyNames="IdActividad" AllowPaging="True" 
        PageSize="5">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                        CommandName="Select" Text="Ver/Editar"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IdActividad" InsertVisible="False" 
                SortExpression="IdActividad" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("IdActividad") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("IdActividad") %>'></asp:Label>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Actividad" SortExpression="Actividad">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Actividad") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Actividad") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                SortExpression="Descripcion" />
            <asp:BoundField DataField="Que" HeaderText="Que" SortExpression="Que" />
            <asp:BoundField DataField="Cuando" HeaderText="Cuando" 
                SortExpression="Cuando" />
            <asp:BoundField DataField="AQuienes" HeaderText="A Quienes" 
                SortExpression="AQuienes" />
            <asp:TemplateField HeaderText="Imagen" SortExpression="Imagen">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Imagen") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' runat="server" />
                 <%--   <asp:Label ID="Label1" runat="server" Text='<%# RutaImagen(Eval("Imagen")) %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="Estado" HeaderText="Estado" 
                SortExpression="Estado" />
            <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                <ItemTemplate>
                    <a href="VoluntariadoInscriptos.aspx?idvol=<%# Eval("IdActividad").ToString() %>&Nombre=<%# Server.UrlEncode(Eval("Actividad").ToString()) %>" target="_blank"><asp:Label ID="Label2" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label></a>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Aún no se registraron Actividades
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSActividades" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="ActividadSelectAll" 
        TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
    </asp:ObjectDataSource>
    <br />
    <br />
    <asp:Button ID="btnAddActividad" runat="server" onclick="btnAddActividad_Click" 
        Text="Agregar" />
    <br />
    <br />
    <asp:FormView ID="FVActividades" runat="server" DataKeyNames="IdActividad" 
        DataSourceID="ODSFVActividad" onitemcreated="FVActividades_ItemCreated" 
        onitemdeleted="FVActividades_ItemDeleted" 
        oniteminserted="FVActividades_ItemInserted" 
        onitemupdated="FVActividades_ItemUpdated" 
        onitemdeleting="FVActividades_ItemDeleting" 
        ondatabound="FVActividades_DataBound" 
        onmodechanged="FVActividades_ModeChanged">
        <EditItemTemplate>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="UpdateButton">
<%--            IdActividad:
            <asp:Label ID="IdActividadLabel1" runat="server" 
                Text='<%# Eval("IdActividad") %>' />
            <br />--%>
            <strong>Actividad:</strong><br /> <asp:TextBox ID="ActividadTextBox" onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("Actividad") %>' />
            <asp:RequiredFieldValidator ID="RFVActividadTextBox" runat="server" 
                ControlToValidate="ActividadTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>Descripción:</strong><br /> <asp:TextBox ID="DescripcionTextBox" 
                onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
            
            <br />
            <strong>Que:</strong><br /> <asp:TextBox ID="QueTextBox" runat="server" 
                onKeyDown="chekAndLimit(this,250)" Text='<%# Bind("Que") %>' Rows="3" 
                TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RFVQueTextBox" runat="server" 
                ControlToValidate="QueTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>Cuando:</strong><br /> <asp:TextBox ID="CuandoTextBox" runat="server" onKeyDown="chekAndLimit(this,250)" Text='<%# Bind("Cuando") %>' />
            <asp:RequiredFieldValidator ID="RFVCuandoTextBox" runat="server" 
                ControlToValidate="CuandoTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>A Quienes:</strong><br /> <asp:TextBox ID="AQuienesTextBox" onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("AQuienes") %>' />
            <asp:RequiredFieldValidator ID="RFVAQuienesTextBox" runat="server" 
                ControlToValidate="AQuienesTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            Imagen:
            </strong>
            <br /><asp:TextBox ID="ImagenTextBox" runat="server" 
                Text='<%# Bind("Imagen") %>' ReadOnly="True" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="ImagenTextBox">*</asp:RequiredFieldValidator>
                <br />
            <asp:FileUpload ID="FUImagen" runat="server" /><br />
            <asp:Button ID="Button1" runat="server" Text="Subir Imagen" onclick="Button1_Click" 
                CausesValidation="False" />
            <br />
            Estado:
            <asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' />
            &nbsp;(Marque para habilitar)<br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Actualizar" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            
            </asp:Panel>
        </EditItemTemplate>
        <InsertItemTemplate>
        <asp:Panel ID="Panel3" runat="server" DefaultButton="InsertButton">
        <strong>
            Actividad:</strong><br />&nbsp;<asp:TextBox ID="ActividadTextBox" onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("Actividad") %>' />
            <asp:RequiredFieldValidator ID="RFVActividadTextBox" runat="server" 
                ControlToValidate="ActividadTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>Descripción:</strong><br /> <asp:TextBox ID="DescripcionTextBox" 
                onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("Descripcion") %>' Rows="3" TextMode="MultiLine" />
            <br />
           <strong> Que:</strong><br />&nbsp;<asp:TextBox ID="QueTextBox" runat="server" 
                onKeyDown="chekAndLimit(this,250)" Text='<%# Bind("Que") %>' Rows="3" 
                TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RFVQueTextBox" runat="server" 
                ControlToValidate="QueTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>Cuando:</strong><br /> <asp:TextBox ID="CuandoTextBox" runat="server" onKeyDown="chekAndLimit(this,250)" Text='<%# Bind("Cuando") %>' />
            <asp:RequiredFieldValidator ID="RFVCuandoTextBox" runat="server" 
                ControlToValidate="CuandoTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>A Quienes:</strong><br />&nbsp;<asp:TextBox ID="AQuienesTextBox" onKeyDown="chekAndLimit(this,250)" runat="server" 
                Text='<%# Bind("AQuienes") %>' />
            <asp:RequiredFieldValidator ID="RFVAQuienesTextBox" runat="server" 
                ControlToValidate="AQuienesTextBox" SetFocusOnError="True">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            Imagen:</strong>
            <br />&nbsp;<asp:TextBox ID="ImagenTextBox" runat="server" Text='<%# Bind("Imagen") %>' ReadOnly="True" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="ImagenTextBox">*</asp:RequiredFieldValidator>
            <br />
            <asp:FileUpload ID="FUImagen" runat="server" /><br />
            <asp:Button ID="Button1" runat="server" Text="Subir Imagen" 
                onclick="Button1_Click" CausesValidation="False" />
            <br /><strong>
            Estado:</strong><br />
            <asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' />
            &nbsp;(Marque para habilitar)<br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Agregar" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            
            </asp:Panel>
        </InsertItemTemplate>
        <ItemTemplate>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="EditButton">
                <div>
                    <asp:Label ID="lblError" runat="server" Text="Existen personas inscriptas en este Voluntariado.<br /> Para poder continuar deberá elimiar a los inscriptos vinculados, para ello presione el boton eliminar inscriptos" CssClass="messages msg-error" 
                        Visible="False"></asp:Label>
                    <br />
                    <asp:Button ID="btnDelInscriptos" runat="server" 
                        Text="Eliminar Inscriptos a este Voluntariado" Visible="false" 
                        onclick="btnDelInscriptos_Click" onclientclick="return confirm('Esta Ud. por eliminar los suscriptos a este Voluntariado. \n ¿Desea continuar?')" />
                </div>
            <asp:HiddenField ID="HFIdActividad" Value='<%# Eval("IdActividad") %>' runat="server" />
<%--            IdActividad:
            <asp:Label ID="IdActividadLabel" runat="server" 
                Text='<%# Eval("IdActividad") %>' />
            <br />--%>
            <strong>Actividad: </strong>
            <asp:Label ID="ActividadLabel" runat="server" Text='<%# Bind("Actividad") %>' />
            <br />
            <strong>Descripción:</strong>
            <asp:Label ID="DescripcionLabel" runat="server" 
                Text='<%# Bind("Descripcion") %>' />
            <br />
            <strong>Que:</strong>
            <asp:Label ID="QueLabel" runat="server" Text='<%# Bind("Que") %>' />
            <br />
            <strong>
            Cuando:
            </strong>
            <asp:Label ID="CuandoLabel" runat="server" Text='<%# Bind("Cuando") %>' />
            <br />
            <strong>
            A Quienes:
            </strong>
            <asp:Label ID="AQuienesLabel" runat="server" Text='<%# Bind("AQuienes") %>' />
            <br />
            <strong>
            Imagen:
            </strong>
            <br />
            
            <asp:Image ID="Image2" ImageUrl='<%# RutaImagen(Eval("Imagen")) %>' runat="server" />
           <%-- <asp:Label ID="ImagenLabel" runat="server" Text='<%# Bind("Imagen") %>' />--%>
            <br />
            <strong>
            Estado:
            </strong>
            <asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' Enabled="false" />
            <br />
            <strong>
            Cantidad:
            </strong>
            <asp:Label ID="CantidadLabel" runat="server" Text='<%# Bind("Cantidad") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                CommandName="Edit" Text="Editar" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                CommandName="Delete" Text="Borrar" OnClientClick="return confirm('Esta acción eliminara esta actividad. \n¿Desea Continuar?')" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                CommandName="New" Text="Nuevo" />
            
            </asp:Panel>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSFVActividad" runat="server" 
        DeleteMethod="ActividadDelete" InsertMethod="ActividadInsert" 
        OldValuesParameterFormatString="{0}" SelectMethod="ActividadSelect" 
        TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
        UpdateMethod="ActividadUpdate">
        <DeleteParameters>
            <asp:Parameter Name="IdActividad" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Actividad" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Que" Type="String" />
            <asp:Parameter Name="Cuando" Type="String" />
            <asp:Parameter Name="AQuienes" Type="String" />
            <asp:Parameter Name="Imagen" Type="String" />
            <asp:Parameter Name="Estado" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVActividades" Name="IdActividad" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="IdActividad" Type="Int32" />
            <asp:Parameter Name="Actividad" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Que" Type="String" />
            <asp:Parameter Name="Cuando" Type="String" />
            <asp:Parameter Name="AQuienes" Type="String" />
            <asp:Parameter Name="Imagen" Type="String" />
            <asp:Parameter Name="Estado" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</asp:Content>
