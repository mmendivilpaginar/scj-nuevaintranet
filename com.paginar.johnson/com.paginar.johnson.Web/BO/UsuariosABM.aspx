﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true"
    CodeBehind="UsuariosABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.UsuariosABM" %>

<%@ Register TagPrefix="CC1" Namespace="com.paginar.johnson.validators" Assembly="com.paginar.johnson.validators" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">
    function VerificarClusterPertenciaEsClusterAcceso(sender, args) {
        args.IsValid = false;
        var ClusterDropDownListID = sender.attributes["ClusterDropDownList"].value;
        var ClusterPertenenciaValue = $(ClusterDropDownListID).val();
        var ClustersCheckBoxListID = sender.attributes["ClustersCheckBoxList"].value;
        $(ClustersCheckBoxListID).find("input:checked")
            .each(function () {
            if ($(this).val() == ClusterPertenenciaValue)
                args.IsValid = true;
                             });
             
    
    }

    $(document).ready(function () {
        $('#tab-container').easytabs();
    });

    </script>
                <script src="../js/jquery.shorten.min.js" type="text/javascript"></script>
                <script src="../js/jquery.hashchange.min.js" type="text/javascript"></script>
                <script src="../js/jquery.easytabs.min.js" type ="text/javascript"></script>

    <style type="text/css">
        .etabs
        {
            margin: 0;
            padding: 0;
        }

        .tab
        {
            display: inline-block;
            zoom: 1;
            *display: inline;
            background: #eee;
            border: solid 1px #999;
            border-bottom: none;
            -moz-border-radius: 4px 4px 0 0;
            -webkit-border-radius: 4px 4px 0 0;
        }

            .tab a
            {
                font-size: 14px;
                line-height: 2em;
                display: block;
                padding: 0 10px;
                outline: none;
            }

                .tab a:hover
                {
                    text-decoration: underline;
                }

            .tab.active
            {
                background: #fff;
                padding-top: 6px;
                position: relative;
                top: 1px;
                border-color: #666;
            }

            .tab a.active
            {
                font-weight: bold;
            }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">

    <h2>
        Usuarios</h2>
    <asp:MultiView ID="MVUsuarios" runat="server">
        <asp:View ID="VGrilla" runat="server">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="BuscarButton">
            
            <div class="form-item leftHalf">
                <label>
                    Apellido / Nombre / Legajo / &Aacute;rea / Direcci&oacute;n</label>
                <asp:TextBox ID="ApellidoTextBox" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ControlToValidate="ApellidoTextBox" ID="rqfApellidoNom" ValidationGroup="Buscar"
                                runat="server" ErrorMessage="Ingrese un texto para buscar">*</asp:RequiredFieldValidator>
          
            </div>
            <div class="form-item rightHalf">
                <label>
                    Grupo</label>
                <asp:DropDownList AppendDataBoundItems="true" ID="GrupoDropDownList" runat="server"
                    DataSourceID="ODSGrupos" DataTextField="Descrip" DataValueField="Numero">
                    <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ODSGrupos" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.GrupoTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_Numero" Type="Int32" />
                        <asp:Parameter Name="Original_Descrip" Type="String" />
                        <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                        <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                        <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="FHAlta" Type="DateTime" />
                        <asp:Parameter Name="UsrAlta" Type="Int32" />
                        <asp:Parameter Name="FHBaja" Type="DateTime" />
                        <asp:Parameter Name="UsrBaja" Type="Int32" />
                        <asp:Parameter Name="FHMod" Type="DateTime" />
                        <asp:Parameter Name="UsrMod" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="FHAlta" Type="DateTime" />
                        <asp:Parameter Name="UsrAlta" Type="Int32" />
                        <asp:Parameter Name="FHBaja" Type="DateTime" />
                        <asp:Parameter Name="UsrBaja" Type="Int32" />
                        <asp:Parameter Name="FHMod" Type="DateTime" />
                        <asp:Parameter Name="UsrMod" Type="Int32" />
                        <asp:Parameter Name="Original_Numero" Type="Int32" />
                        <asp:Parameter Name="Original_Descrip" Type="String" />
                        <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                        <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                        <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </div>
            <div class="controls">
                <asp:Button ID="BuscarButton" runat="server" OnClick="BuscarButton_Click" Text="Buscar" ValidationGroup="Buscar" />
                <asp:Button ID="ButtonNuevo" runat="server" OnClick="ButtonNuevo_Click" Text="Nuevo Usuario" />
             
                             <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="messages msg-error" ValidationGroup="Buscar" />
            </div>
            <asp:GridView ID="GridUsuarios" runat="server" AutoGenerateColumns="False" DataKeyNames="UsuarioID"
                DataSourceID="ODSUsuarios" OnRowCommand="GridUsuarios_RowCommand" 
                ondatabound="GridUsuarios_DataBound">
                <Columns>
                    <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="UsuarioID">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UsuarioID") %>'></asp:Label></EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("UsuarioID") %>'
                                CommandName="Select" Text='<%# Eval("UsuarioID") %>'></asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre" SortExpression="NombreCompleto" />
                    <asp:BoundField DataField="LegajoWD" HeaderText="Legajo" SortExpression="LegajoWD" />
                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                    <asp:TemplateField HeaderText="Cluster" SortExpression="ClusterDesc">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ClusterDesc") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblCluster" runat="server" Text='<%# Bind("ClusterDesc") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FHUltAcc" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Último Acceso"
                        SortExpression="FHUltAcc" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="ClonarLinkButton" runat="server" CommandName="Clonar" CommandArgument='<%# Eval("UsuarioID") %>'>Clonar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="ODSUsuarios" runat="server" SelectMethod="GetByBusqueda"
                TypeName="com.paginar.johnson.BL.ControllerUsuarios" OnSelecting="ODSUsuarios_Selecting">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ApellidoTextBox" Name="Apellido" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="GrupoDropDownList" Name="GrupoID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </asp:Panel>
        </asp:View>
        <asp:View ID="VABM" runat="server">
            <asp:FormView ID="FVUsuario" runat="server" DataKeyNames="UsuarioID" DataSourceID="ODSUsuario"
                DefaultMode="Edit" OnItemUpdating="FVUsuario_ItemUpdating" OnItemInserting="FVUsuario_ItemInserting"
                OnItemCommand="FVUsuario_ItemCommand" ondatabound="FVUsuario_DataBound">
                <ItemTemplate>
                    <asp:CustomValidator ID="ValidadorIdentifInsert" runat="server" 
                        ControlToValidate="IdentifTextBox" CssClass="messages msg-error" 
                        ErrorMessage="El nombre de usuario ya existe. Por favor ingrese otro nombre para el nuevo usuario." 
                        onservervalidate="ValidadorIdentif_ServerValidate" Display="None" 
                        SetFocusOnError="True"></asp:CustomValidator>
                   

                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="messages msg-error" />
                    <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Nombre de usuario
                            </label>
                            <asp:TextBox ID="IdentifTextBox" runat="server" Text=''></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="IdentifTextBox" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre de usuario">*</asp:RequiredFieldValidator>
                            
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Clave
                            </label>
                            <asp:TextBox ID="ClaveTextBox" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="ClaveTextBox" ID="RequiredFieldValidator16"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Clave">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Confirme clave
                            </label>
                            <asp:TextBox ID="ConfirmeTextBox" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <asp:HiddenField ID="ClaveHiddenField" runat="server" Value='<%# Eval("Clave") %>' />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="ConfirmeTextBox"
                                ControlToValidate="ClaveTextBox" ErrorMessage="La clave y su confirmación deben ser iguales.">*</asp:CompareValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Cluster de pertenencia:
                            </label>
                            <asp:DropDownList ID="ClusterDropDownList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" SelectedValue='<%# Bind("Cluster") %>'
                                AppendDataBoundItems="True" >
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ClusterDropDownList" ID="RequiredFieldValidator11"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Cluster">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSCluster" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.ClusterTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="ID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Estado:
                            </label>
                            <asp:DropDownList ID="EstadoDropDownList" runat="server" SelectedValue='<%# Bind("Estado") %>'>
                                <asp:ListItem Value="2">Habilitado</asp:ListItem>
                                <asp:ListItem Value="1">Deshabilitado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-item full">
                            <label>
                                Acceso a Clusters
                            </label>
                            <asp:CheckBoxList ID="ClustersCheckBoxList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" OnDataBound="ClustersCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ClustersCheckBoxList"
                                runat="server" ErrorMessage="Seleccionar al menos un cluster al que tendra acceso el usuario"
                                ID="checkboxListvalidator">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                    </div>
                    <!--/box-->
                    <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Apellido:
                            </label>
                            <asp:TextBox ID="ApellidoTextBox" runat="server" Text='<%# Bind("Apellido") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="ApellidoTextBox" ID="RequiredFieldValidator4"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Apellido">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Nombre:
                            </label>
                            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="NombreTextBox" ID="RequiredFieldValidator5"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Email:
                            </label>
                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ControlToValidate="EmailTextBox" ID="RequiredFieldValidator6"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Email">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Fecha de Nacimiento:
                            </label>
                            <asp:TextBox ID="FHNacimientoTextBox" runat="server" Text='<%# Bind("FHNacimiento","{0:dd/MM/yyyy}") %>'
                                SkinID="form-date" />
                            <asp:ImageButton CausesValidation="false" ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                            <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" PopupButtonID="Image2" TargetControlID="FHNacimientoTextBox">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ControlToValidate="FHNacimientoTextBox" ID="RequiredFieldValidator7"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Fecha de Nacimiento">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="FHNacimientoTextBox"
                                ErrorMessage="El formato de la fecha ingresada en Fecha de Nacimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </div>
                        <div class="form-item full">
                            <label>
                                Foto:
                            </label>
                            <asp:FileUpload ID="UsuarioFotoFileUpload" runat="server" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Imagenes"
                                ControlToValidate="UsuarioFotoFileUpload" runat="Server" ErrorMessage="Solo se permiten archivos JPG"
                                ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG)$" />
                        </div>
                    </div>
                    <!--/box-->
                    <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Legajo:
                            </label>
                            <asp:TextBox ID="LegajoTextBox" runat="server" Text='<%# Bind("LegajoWD") %>' />
                            <asp:FilteredTextBoxExtender ID="LegajoTextBox_FilteredTextBoxExtender" runat="server"
                                Enabled="True" FilterType="Numbers" TargetControlID="LegajoTextBox">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ControlToValidate="LegajoTextBox" ID="RequiredFieldValidator8"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Legajo">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorLegajo" runat="server" 
                                ControlToValidate="LegajoTextBox" 
                                ErrorMessage="El legajo ya existe, ingrese otro" 
                                onservervalidate="CustomValidatorLegajo_ServerValidate">*</asp:CustomValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Interno:
                            </label>
                            <asp:TextBox ID="InternoTextBox" runat="server" Text='<%# Bind("Interno") %>' />
                            <label>
                                Interno IP:
                            </label>
                            <asp:TextBox ID="InternoIPTextBox" runat="server" Text='<%# Bind("Internoip") %>' />
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Ubicación:
                            </label>
                            <asp:DropDownList ID="UbicacionDropDownList" runat="server" DataSourceID="ODSUbicaciones"
                                DataTextField="Descripcion" DataValueField="UbicacionID" SelectedValue='<%# Bind("UbicacionID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="UbicacionDropDownList" ID="RequiredFieldValidator9"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Ubicación">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSUbicaciones" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.UbicacionTableAdapter">
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Sector:
                            </label>
                            <asp:DropDownList ID="SectorIDDropDownList" runat="server" DataSourceID="ODSSectores"
                                DataTextField="Descripcion" DataValueField="SectorID" SelectedValue='<%# Bind("SectorID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="SectorIDDropDownList" ID="RequiredFieldValidator10"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Sector">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSSectores" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.SectorTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="SectorID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Areas:
                            </label>
                            <asp:DropDownList ID="AreaDropDownList" runat="server" DataSourceID="ODSArea" DataTextField="AreaDESC"
                                DataValueField="AreaID" SelectedValue='<%# Bind("AreaID") %>' AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="AreaDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Area">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSArea" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.AreasTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Dirección:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="DireccionDropDownList" runat="server"
                                DataSourceID="ODSDireccion" DataTextField="DireccionDET" DataValueField="DireccionID"
                                SelectedValue='<%# Bind("DireccionID") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DireccionDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Dirección">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSDireccion" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Cargo:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="CargoDropDownList" runat="server"
                                DataSourceID="ODSCargo" DataTextField="CargoDET" DataValueField="CargoID" SelectedValue='<%# Bind("CargoID") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="CargoDropDownList" Enabled="false"
                                ErrorMessage="Seleccione un valor para el campo Cargo">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSCargo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.CargoTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Reporta a:
                            </label>
                            <asp:DropDownList ID="ACargoDeDropDownList" runat="server" DataSourceID="ODSACArgoDe"
                                DataTextField="NombreCompleto" DataValueField="UsuarioID" 
                                AppendDataBoundItems="true" SelectedValue='<%# ChekDBValue(Eval("ACargoDe"))  %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                                <%--<asp:ListItem Value="0">Sin Asignar</asp:ListItem>--%>
                            </asp:DropDownList>
                            
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                ErrorMessage="Seleccione un valor para el campo Reporta a:" 
                                ControlToValidate="ACargoDeDropDownList">*</asp:RequiredFieldValidator>--%>
                            
                            <asp:ObjectDataSource ID="ODSACArgoDe" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetUsuarios" TypeName="com.paginar.johnson.BL.ControllerUsuarios">
                            </asp:ObjectDataSource>
                            <span>
                            <asp:Label ID="lblErrorAcargoDe" runat="server" CssClass="messages msg-error" 
                                Visible="False"></asp:Label></span>
                        </div>
                    </div>
                    <!--/box-->
                    <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Esta en QesQ:
                            </label>
                            <asp:DropDownList ID="EstaEnQeQDropDownList" runat="server" SelectedValue='<%# Bind("EstaEnQeQ") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Es PayRoll:
                            </label>
                            <asp:DropDownList ID="EsPayRollDropDownList" runat="server" SelectedValue='<%# Bind("EsPayRoll") %>'>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-item full">
                            <label>
                                Grado de Escala Salarial al 30 de Junio:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="EscalaSalarialDropDownList" runat="server"
                                DataSourceID="ODSEscalaSalarial" DataTextField="Codigo" DataValueField="Cod_ID"
                                SelectedValue='<%# Bind("EscalaSalarial") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="EscalaSalarialDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Escala Salarial">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSEscalaSalarial" runat="server" DeleteMethod="Delete"
                                InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
                                TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.EscalaSalarialTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                    <!--/box-->
                    <div class="box formulario">
                        <div class="form-item mcol">
                            <label>
                                Formularios
                            </label>
                            <asp:CheckBoxList ID="FormulariosCheckBoxList" runat="server" DataSourceID="ODSFormularios"
                                DataTextField="frm_Titulo" DataValueField="frm_ID" OnDataBound="FormulariosCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <%--<CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="FormulariosCheckBoxList" 
                                runat="server" ErrorMessage="Seleccionar al menos un formulario al que tendra acceso el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists1">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>--%>
                        </div>
                    </div>
                    <!--/box-->
                    <div class="box formulario">
                        <div class="form-item mcol">
                            <label>
                                Grupos
                            </label>
                            <asp:CheckBoxList ID="GruposCheckBoxList" runat="server" DataSourceID="ODSGrupo"
                                DataTextField="Descrip" DataValueField="Numero" OnDataBound="GruposCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="GruposCheckBoxList" 
                                runat="server" ErrorMessage="Seleccionar al menos un grupo al que pertenecera el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists2">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                    </div>
                    <!--/box-->
                    <asp:ObjectDataSource ID="ODSFormularios" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.frmFormularioTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="frm_ID" Type="Int32" />
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ODSGrupo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.GrupoTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <div class="controls">
                        <asp:Button ID="ClonarButton" runat="server" CausesValidation="True" Text="Clonar"
                            OnClick="ClonarButton_Click" />
                        <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancelar" />
                    </div>
                </ItemTemplate>
                <EditItemTemplate>

                    <asp:Panel ID="Panel2" runat="server" DefaultButton="UpdateButton">
                    <div id="tab-container" class='tab-container' style="background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px;">
                             

                    <ul class='etabs'>
                            <li class='tab' ><a href="#tabs-1">Datos Principales</a></li>
                            <li class='tab' ><a href="#tabs-2">Datos Adicionales</a></li>
                            <li class='tab' ><a href="#tabs-3">Permisos Grupos</a></li>
                        </ul>
                          
                    
                    <asp:HiddenField ID="UsuarioIDHiddenField" runat="server" Value='<%# Eval("UsuarioID") %>' />
                    <asp:HiddenField ID="ImagenHiddenField" runat="server" Value='<%# Eval("UsuarioFoto") %>' />
                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Bind("LoginID") %>' />

                    <div id="boxes">

                        <div id="dialog" class="window">
                        <div class="leyenda"><strong>Atención:</strong> Los siguientes Usuarios estan a cargo del Actual |</div> 
                        <a href="#"class="close"/>Cerrar</a>
                        <p>
                            <asp:Label ID="lblListadoUsuariosDependientes" runat="server"></asp:Label>
                        </p>
                        </div>



                        <!-- Mask to cover the whole screen -->
                          <div id="mask"></div>
                        </div>
                        
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
                        ErrorMessage="El nombre de usuario ya existe. Por favor ingrese otro nombre para el nuevo usuario." 
                        ControlToValidate="IdentifTextBox" 
                        onservervalidate="ValidadorIdentifUpdate_ServerValidate"></asp:CustomValidator>

                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="messages msg-error" />

                       
                     <div id="tabs-1">
                            <br />
                    <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Nombre de usuario</label>
                            <asp:TextBox ID="IdentifTextBox" runat="server" Text='<%# Bind("Identif") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="IdentifTextBox" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre de usuario">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Clave</label>
                            <asp:TextBox ID="ClaveTextBox" runat="server" Text='<%# Bind("Clave") %>' TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Confirme clave</label>
                            <asp:TextBox ID="ConfirmeTextBox" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:HiddenField ID="ClaveHiddenField" runat="server" Value='<%# Eval("Clave") %>' />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="ConfirmeTextBox"
                                ControlToValidate="ClaveTextBox" ErrorMessage="La clave y su confirmación deben ser iguales.">*</asp:CompareValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Apellido:</label>
                            <asp:TextBox ID="ApellidoTextBox" runat="server" Text='<%# Bind("Apellido") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="ApellidoTextBox" ID="RequiredFieldValidator4"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Apellido">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Nombre:</label>
                            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="NombreTextBox" ID="RequiredFieldValidator5"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Legajo:</label>
                            <asp:TextBox ID="LegajoTextBox" runat="server" Text='<%# Bind("LegajoWD") %>' />
                            <asp:FilteredTextBoxExtender ID="LegajoTextBox_FilteredTextBoxExtender" runat="server"
                                Enabled="True" FilterType="Numbers" TargetControlID="LegajoTextBox">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ControlToValidate="LegajoTextBox" ID="RequiredFieldValidator8"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Legajo">*</asp:RequiredFieldValidator>

                            <asp:CustomValidator ID="CustomValidator2" runat="server" 
                                ControlToValidate="LegajoTextBox" 
                                ErrorMessage="El legajo ya existe en el sistema, ingrese otro" 
                                onservervalidate="CustomValidator2_ServerValidate">*</asp:CustomValidator>

                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Estado:</label>
                            <asp:DropDownList ID="EstadoDropDownList" runat="server" 
                                SelectedValue='<%# Bind("Estado") %>' 
                                onselectedindexchanged="EstadoDropDownList_SelectedIndexChanged" 
                                AutoPostBack="True" > 
                                <asp:ListItem Value="2">Habilitado</asp:ListItem>
                                <asp:ListItem Value="1">Deshabilitado</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Email:</label>
                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Fecha de Nacimiento:</label>
                            <asp:TextBox ID="FHNacimientoTextBox" runat="server" Text='<%# Bind("FHNacimiento","{0:dd/MM/yyyy}") %>'
                                SkinID="form-date" />
                            <asp:ImageButton CausesValidation="false" ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                            <asp:CalendarExtender ID="FHNacimientoTextBox_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="FHNacimientoTextBox" DaysModeTitleFormat="dd/MM/yyyy" Format="dd/MM/yyyy"
                                PopupButtonID="Image2">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ControlToValidate="FHNacimientoTextBox" ID="RequiredFieldValidator7"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Fecha de Nacimiento">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="FHNacimientoTextBox"
                                ErrorMessage="El formato de la fecha ingresada en Fecha de Nacimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Ubicación:
                            </label>
                            <asp:DropDownList ID="UbicacionDropDownList" runat="server" DataSourceID="ODSUbicaciones"
                                DataTextField="Descripcion" DataValueField="UbicacionID" SelectedValue='<%# Bind("UbicacionID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="UbicacionDropDownList" ID="RequiredFieldValidator9"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Ubicación">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSUbicaciones" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.UbicacionTableAdapter">
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Área:
                            </label>
                            <asp:HiddenField runat="server" ID="hdareaId"  Value='<%# Eval("AreaID") %>' />
                            <asp:DropDownList ID="AreaDropDownList" runat="server" DataSourceID="ODSArea" DataTextField="AreaDESC"
                                DataValueField="AreaID" 
                               SelectedValue='<%# Bind("AreaID") %>'                                 AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="AreaDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Area">*</asp:RequiredFieldValidator>
                         
                            <asp:ObjectDataSource ID="ODSArea" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataFilter" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.AreasTableAdapter"
                                UpdateMethod="Update">
                                <SelectParameters  >
                                  <asp:ControlParameter ControlID="hdareaId" Type="String" Name="AreaID" PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Cluster de pertenencia:
                            </label>
                            <asp:DropDownList ID="ClusterDropDownList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" SelectedValue='<%# Bind("Cluster") %>'
                                AppendDataBoundItems="True" AutoPostBack="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ClusterDropDownList" ID="RequiredFieldValidator11"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Cluster">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSCluster" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.ClusterTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="ID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                        <div class="form-item full">
                            <label>
                                Acceso a Clusters
                            </label>
                            <asp:CheckBoxList ID="ClustersCheckBoxList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" OnDataBound="ClustersCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ClustersCheckBoxList"
                                runat="server" ErrorMessage="Seleccionar al menos un cluster al que tendra acceso el usuario"
                                ID="checkboxListvalidator">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                            <asp:CustomValidator ID="CustomValidatorClusterPertenenciaClusterAcceso" runat="server" ErrorMessage="Debe seleccionar el cluster de pertenencia como cluster al que podrá tener acceso el usuario" 
                                ControlToValidate="ClusterDropDownList" ClientValidationFunction="VerificarClusterPertenciaEsClusterAcceso">*</asp:CustomValidator>

                            
                        </div>

                      


                    </div>
                    <!--/box-->
                     </div>
                     <div id="tabs-2">
                       <br />
                       <div class="box formulario">
                           <div class="form-item full">
                            <label>
                                Foto:
                            </label>
                            <asp:FileUpload ID="UsuarioFotoFileUpload" runat="server" />
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# "~/images/personas/"+ Eval("UsuarioFoto") %>'
                                Text='<%# Eval("UsuarioFoto").ToString()!=""? " VER ": "" %>'></asp:HyperLink>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Imagenes"
                                ControlToValidate="UsuarioFotoFileUpload" runat="Server" ErrorMessage="Solo se permiten archivos JPG"
                                ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG)$" />
                        </div>

                           <div class="form-item leftHalf">
                            <label>
                                Interno:</label>
                            <asp:TextBox ID="InternoTextBox" runat="server" Text='<%# Bind("Interno") %>' />
                        </div>

                              
                        <div class="form-item rightHalf">
                            <label>
                                Interno IP:</label>
                            <asp:TextBox ID="InternoIPTextBox" runat="server" Text='<%# Bind("Internoip") %>' />
                        </div>

                           <div class="form-item leftHalf">
                            <label>
                                Sector:
                            </label>
                            <asp:DropDownList ID="SectorIDDropDownList" runat="server" DataSourceID="ODSSectores"
                                DataTextField="Descripcion" DataValueField="SectorID" SelectedValue='<%# Bind("SectorID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                                  <asp:ListItem Text="No Seleccionado" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="SectorIDDropDownList" ID="RequiredFieldValidator10"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Sector">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSSectores" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.SectorTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="SectorID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                           <div class="form-item rightHalf">
                            <label>
                                Dirección:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="DireccionDropDownList" runat="server"
                                DataSourceID="ODSDireccion" DataTextField="DireccionDET" DataValueField="DireccionID"
                                SelectedValue='<%# Bind("DireccionID") %>'>
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                                <asp:ListItem Text="No Seleccionado" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DireccionDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Dirección">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSDireccion" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                           <div class="form-item leftHalf">
                            <label>
                                Cargo:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="True" ID="CargoDropDownList" runat="server"
                                DataSourceID="ODSCargo" DataTextField="CargoDET" DataValueField="CargoID" 
                                SelectedValue='<%# CheckDBCargoValue(Eval("CargoID")) %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                                <asp:ListItem Text="No Seleccionado" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="CargoDropDownList" Enabled="false"
                                ErrorMessage="Seleccione un valor para el campo Cargo">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSCargo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.CargoTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                           <div class="form-item rightHalf">
                            <label>
                                Reporta a:
                            </label>
                            <asp:DropDownList ID="ACargoDeDropDownList" runat="server" DataSourceID="ODSACArgoDe"
                                DataTextField="NombreCompleto" DataValueField="UsuarioID" SelectedValue='<%# ChekDBValue(Eval("ACargoDe")) %>'
                                AppendDataBoundItems="true">
                                <asp:ListItem Text="Seleccionar"  Value=""></asp:ListItem>
                               <%-- <asp:ListItem Value="0">Sin Asignar</asp:ListItem>--%>
                            </asp:DropDownList>
                           <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                ErrorMessage="Seleccione un valor para el campo Reporta a:" 
                                ControlToValidate="ACargoDeDropDownList">*</asp:RequiredFieldValidator>--%>
                            <asp:ObjectDataSource ID="ODSACArgoDe" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetUsuarios" TypeName="com.paginar.johnson.BL.ControllerUsuarios">
                            </asp:ObjectDataSource>
                            <span>
                            <asp:Label ID="lblErrorAcargoDe" runat="server" CssClass="messages msg-error" 
                                Visible="False"></asp:Label></span>
                        </div>
                       </div>
                    

                     </div>
                     <div id="tabs-3"   >
                        <div class="box formulario">
                           <div class="form-item leftHalf">
                            <label>
                                Esta en QesQ:
                            </label>
                            <asp:DropDownList ID="EstaEnQeQDropDownList" runat="server" SelectedValue='<%# Bind("EstaEnQeQ") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                           <div class="form-item rightHalf">
                            <label>
                                Es PayRoll:
                            </label>
                            <asp:DropDownList ID="EsPayRollDropDownList" runat="server" SelectedValue='<%# Bind("EsPayRoll") %>'>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                           <div class="form-item full">
                            <label>
                                Grado de Escala Salarial al 30 de Junio:
                            </label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="EscalaSalarialDropDownList" runat="server"
                                DataSourceID="ODSEscalaSalarial" DataTextField="Codigo" DataValueField="Cod_ID"
                                SelectedValue='<%# Bind("EscalaSalarial") %>'>
                                <asp:ListItem Text="Seleccionar" Value="0"></asp:ListItem>
                                  <asp:ListItem Text="No Seleccionado" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="EscalaSalarialDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Escala Salarial">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSEscalaSalarial" runat="server" DeleteMethod="Delete"
                                InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
                                TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.EscalaSalarialTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                           <div class="box formulario">
                        <div class="form-item mcol">
                            <label>
                                Formularios
                            </label>
                            <asp:CheckBoxList ID="FormulariosCheckBoxList" runat="server" DataSourceID="ODSFormularios"
                                DataTextField="frm_Titulo" DataValueField="frm_ID" OnDataBound="FormulariosCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <%--<CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="FormulariosCheckBoxList"
                                runat="server" ErrorMessage="Seleccionar al menos un formulario al que tendra acceso el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists1">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>--%>
                        </div>
                    </div>
                           <div class="box formulario">
                           <div class="form-item mcol">
                            <label>
                                Grupos
                            </label>
                            <asp:CheckBoxList ID="GruposCheckBoxList" runat="server" DataSourceID="ODSGrupo"
                                DataTextField="Descrip" DataValueField="Numero" OnDataBound="GruposCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="GruposCheckBoxList" Enabled="false"
                                runat="server" ErrorMessage="Seleccionar al menos un grupo al que pertenecera el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists2">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                    </div>
                        </div>
                     </div>

                   </div>
                  
                    <asp:ObjectDataSource ID="ODSFormularios" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.frmFormularioTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="frm_ID" Type="Int32" />
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ODSGrupo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.GrupoTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <br />
                    <div class="controls">
                        <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                            Text="Actualizar" />
                        <asp:Button ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancelar" />
                    </div>
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                   <asp:Panel ID="Panel3" runat="server" DefaultButton="InsertButton">
                    <div id="tab-container" class='tab-container' style="background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px;">
                        <ul class='etabs'>
                            <li class='tab' ><a href="#tabs-1">Datos Principales</a></li>
                            <li class='tab' ><a href="#tabs-2">Datos Adicionales</a></li>
                            <li class='tab' ><a href="#tabs-3">Permisos Grupos</a></li>
                        </ul>
                        <div id="tabs-1">
                            <br />
                        <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Nombre de usuario</label>
                            <asp:TextBox ID="IdentifTextBox" runat="server" Text='<%# Bind("Identif") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="IdentifTextBox" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre de usuario">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item leftHalf">
                            <label>
                                Clave</label>
                            <asp:TextBox ID="ClaveTextBox" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="ClaveTextBox" ID="RequiredFieldValidator16"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Clave">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Confirme clave</label>
                            <asp:TextBox ID="ConfirmeTextBox" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <asp:HiddenField ID="ClaveHiddenField" runat="server" Value='<%# Eval("Clave") %>' />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="ConfirmeTextBox"
                                ControlToValidate="ClaveTextBox" ErrorMessage="La clave y su confirmación deben ser iguales.">*</asp:CompareValidator>
                        </div>

                        <div class="form-item leftHalf">
                            <label>
                                Apellido:</label>
                            <asp:TextBox ID="ApellidoTextBox" runat="server" Text='<%# Bind("Apellido") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="ApellidoTextBox" ID="RequiredFieldValidator4"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Apellido">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Nombre:</label>
                            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
                            <asp:RequiredFieldValidator ControlToValidate="NombreTextBox" ID="RequiredFieldValidator5"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Nombre">*</asp:RequiredFieldValidator>
                        </div>

                        <div class="form-item leftHalf">
                            <label>
                                Legajo:</label>
                            <asp:TextBox ID="LegajoTextBox" runat="server" Text='<%# Bind("LegajoWD") %>' />
                            <asp:FilteredTextBoxExtender ID="LegajoTextBox_FilteredTextBoxExtender" runat="server"
                                Enabled="True" FilterType="Numbers" TargetControlID="LegajoTextBox">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ControlToValidate="LegajoTextBox" ID="RequiredFieldValidator8"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Legajo">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorLegajo" runat="server" 
                                ControlToValidate="LegajoTextBox" 
                                ErrorMessage="El legajo ya existe, ingrese otro" 
                                onservervalidate="CustomValidatorLegajo_ServerValidate">*</asp:CustomValidator>

                        </div>

                        <div class="form-item rightHalf">
                            <label>
                                Estado:</label>
                            <asp:DropDownList ID="EstadoDropDownList" runat="server" SelectedValue='<%# Bind("Estado") %>'>
                                <asp:ListItem Value="2">Habilitado</asp:ListItem>
                                <asp:ListItem Value="1">Deshabilitado</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        
                        <div class="form-item leftHalf">
                            <label>
                                Email:</label>
                            <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El formato del campo Email es incorrecto"
                                ControlToValidate="EmailTextBox" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"
                                ValidationGroup="Actualizar">*</asp:RegularExpressionValidator>
                        </div>

                        
                        <div class="form-item rightHalf">
                            <label>
                                Fecha de Nacimiento:</label>
                            <asp:TextBox ID="FHNacimientoTextBox" runat="server" Text='<%# Bind("FHNacimiento","{0:dd/MM/yyyy}") %>'
                                SkinID="form-date" />
                            <asp:ImageButton CausesValidation="false" ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                            <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" Enabled="True"
                                Format="dd/MM/yyyy" PopupButtonID="Image2" TargetControlID="FHNacimientoTextBox">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ControlToValidate="FHNacimientoTextBox" ID="RequiredFieldValidator7"
                                runat="server" ErrorMessage="Ingrese un valor en el campo Fecha de Nacimiento">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="FHNacimientoTextBox"
                                ErrorMessage="El formato de la fecha ingresada en Fecha de Nacimiento es incorrecto. El formato de fecha es dd/mm/yyyy."
                                Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </div>


                          <div class="form-item leftHalf">
                            <label>
                                Ubicación:</label>
                            <asp:DropDownList ID="UbicacionDropDownList" runat="server" DataSourceID="ODSUbicaciones"
                                DataTextField="Descripcion" DataValueField="UbicacionID" SelectedValue='<%# Bind("UbicacionID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="UbicacionDropDownList" ID="RequiredFieldValidator9"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Ubicación">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSUbicaciones" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.UbicacionTableAdapter">
                            </asp:ObjectDataSource>
                        </div>

                        <div class="form-item rightHalf">
                            <label>
                                Areas:</label>
                            <asp:DropDownList ID="AreaDropDownList" runat="server" DataSourceID="ODSArea" DataTextField="AreaDESC"
                                DataValueField="AreaID" SelectedValue='<%# Bind("AreaID") %>' AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="AreaDropDownList"
                                ErrorMessage="Seleccione un valor para el campo Area">*</asp:RequiredFieldValidator>
                            <asp:HiddenField runat="server" ID="hdareaId"  Value="0"/>
                            <asp:ObjectDataSource ID="ODSArea" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataFilter" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.AreasTableAdapter"
                                UpdateMethod="Update">
                                <SelectParameters  >
                                   
                                      <asp:ControlParameter ControlID="hdareaId" Type="String" Name="AreaID" PropertyName="Value" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="AreaDESC" Type="String" />
                                    <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                    <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>


                        <div class="form-item leftHalf">
                            <label>
                                Cluster de pertenencia:</label>
                            <asp:DropDownList ID="ClusterDropDownList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" SelectedValue='<%# Bind("Cluster") %>'
                                AppendDataBoundItems="True" >
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ClusterDropDownList" ID="RequiredFieldValidator11"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Cluster">*</asp:RequiredFieldValidator>
                            
                            <asp:ObjectDataSource ID="ODSCluster" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.ClusterTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="ID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Abreviatura" Type="String" />
                                    <asp:Parameter Name="PathImage" Type="String" />
                                    <asp:Parameter Name="Original_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                    <asp:Parameter Name="Original_PathImage" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                      
                        <div class="form-item full">
                            <label>
                                Acceso a Clusters</label>
                            <asp:CheckBoxList ID="ClustersCheckBoxList" runat="server" DataSourceID="ODSCluster"
                                DataTextField="Descripcion" DataValueField="ID" OnDataBound="ClustersCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ClustersCheckBoxList"
                                runat="server" ErrorMessage="Seleccionar al menos un cluster al que tendra acceso el usuario"
                                ID="checkboxListvalidator">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                            <asp:CustomValidator ID="CustomValidatorClusterPertenenciaClusterAcceso" runat="server" ErrorMessage="Debe Seleccionar el cluster de pertenencia como cluster al que podrá tener acceso el usuario" 
                                ControlToValidate="ClusterDropDownList" ClientValidationFunction="VerificarClusterPertenciaEsClusterAcceso">*</asp:CustomValidator>

                        </div>
                    </div>
                        </div>
                        <div id="tabs-2">
                             <br />
                       <div class="box formulario">
                   


                        <div class="form-item full">
                            <label>
                                Foto:</label>
                            <asp:FileUpload ID="UsuarioFotoFileUpload" runat="server" />
                            <asp:RegularExpressionValidator Display="Static" ID="RegularExpressionValidator1"
                                ValidationGroup="Imagenes" ControlToValidate="UsuarioFotoFileUpload" runat="Server"
                                ErrorMessage="Solo se permiten archivos JPG" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG)$" />
                        </div>
                
                
                        <div class="form-item leftHalf">
                            <label>
                                Interno:</label>
                            <asp:TextBox ID="InternoTextBox" runat="server" Text='<%# Bind("Interno") %>' />
                          
                        </div>

                        
                        <div class="form-item rightHalf">
                            <label>
                                Interno IP:</label>
                            <asp:TextBox ID="InternoIPTextBox" runat="server" Text='<%# Bind("Internoip") %>' />
                        </div>


                 
                        <div class="form-item leftHalf">
                            <label>
                                Sector:</label>
                            <asp:DropDownList ID="SectorIDDropDownList" runat="server" DataSourceID="ODSSectores"
                                DataTextField="Descripcion" DataValueField="SectorID" SelectedValue='<%# Bind("SectorID") %>'
                                AppendDataBoundItems="True">
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="SectorIDDropDownList" ID="RequiredFieldValidator10" Enabled="false"
                                runat="server" ErrorMessage="Seleccione un valor para el campo Sector">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSSectores" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.SectorTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="SectorID" Type="Int32" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                    <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                 
                        <div class="form-item rightHalf">
                            <label>
                                Dirección:</label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="DireccionDropDownList" runat="server"
                                DataSourceID="ODSDireccion" DataTextField="DireccionDET" DataValueField="DireccionID"
                                SelectedValue='<%# Bind("DireccionID") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DireccionDropDownList" Enabled="false"
                                ErrorMessage="Seleccione un valor para el campo Dirección">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSDireccion" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="DireccionDET" Type="String" />
                                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
     
                 
                        <div class="form-item leftHalf">
                            <label>
                                Cargo:</label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="CargoDropDownList" runat="server"
                                DataSourceID="ODSCargo" DataTextField="CargoDET" DataValueField="CargoID" SelectedValue='<%# Bind("CargoID") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="CargoDropDownList" Enabled="false"
                                ErrorMessage="Seleccione un valor para el campo Cargo">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSCargo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.CargoTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="CargoDET" Type="String" />
                                    <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                    <asp:Parameter Name="Original_CargoDET" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>

                        <div class="form-item rightHalf">
                            <label>
                                Reporta a:</label>
                            <asp:DropDownList ID="ACargoDeDropDownList" runat="server" DataSourceID="ODSACArgoDe"
                                DataTextField="NombreCompleto" DataValueField="UsuarioID" SelectedValue='<%# ChekDBValue(Eval("ACargoDe")) %>'
                                AppendDataBoundItems="true">
                                <asp:ListItem Text="Seleccionar"  Value=""></asp:ListItem>
                               <%-- <asp:ListItem Value="0">Sin Asignar</asp:ListItem>--%>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                ErrorMessage="Seleccione un valor para el campo Reporta a:" 
                                ControlToValidate="ACargoDeDropDownList">*</asp:RequiredFieldValidator>--%>
                            <asp:ObjectDataSource ID="ODSACArgoDe" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetUsuarios" TypeName="com.paginar.johnson.BL.ControllerUsuarios">
                            </asp:ObjectDataSource>
                            <span>
                            <asp:Label ID="lblErrorAcargoDe" runat="server" CssClass="messages msg-error" 
                                Visible="False"></asp:Label></span>

                        </div>
                    </div>
                    <!--/box-->
                        </div>
                        <div id="tabs-3">
                           
                          <div class="box formulario">
                        <div class="form-item leftHalf">
                            <label>
                                Esta en QesQ:</label>
                            <asp:DropDownList ID="EstaEnQeQDropDownList" runat="server" SelectedValue='<%# Bind("EstaEnQeQ") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-item rightHalf">
                            <label>
                                Es PayRoll:</label>
                            <asp:DropDownList ID="EsPayRollDropDownList" runat="server" SelectedValue='<%# Bind("EsPayRoll") %>'>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-item full">
                            <label>
                                Grado de Escala Salarial al 30 de Junio:</label>
                            <asp:DropDownList AppendDataBoundItems="true" ID="EscalaSalarialDropDownList" runat="server"
                                DataSourceID="ODSEscalaSalarial" DataTextField="Codigo" DataValueField="Cod_ID"
                                SelectedValue='<%# Bind("EscalaSalarial") %>'>
                                <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="EscalaSalarialDropDownList" Enabled="false"
                                ErrorMessage="Seleccione un valor para el campo Escala Salarial">*</asp:RequiredFieldValidator>
                            <asp:ObjectDataSource ID="ODSEscalaSalarial" runat="server" DeleteMethod="Delete"
                                InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
                                TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.EscalaSalarialTableAdapter"
                                UpdateMethod="Update">
                                <DeleteParameters>
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Codigo" Type="String" />
                                    <asp:Parameter Name="Descripcion" Type="String" />
                                    <asp:Parameter Name="Original_Cod_ID" Type="Int32" />
                                    <asp:Parameter Name="Original_Codigo" Type="String" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                          <!--/box-->
                         <div class="box formulario">
                        <div class="form-item mcol">
                            <label>
                                Formularios</label>
                            <asp:CheckBoxList ID="FormulariosCheckBoxList" runat="server" DataSourceID="ODSFormularios"
                                DataTextField="frm_Titulo" DataValueField="frm_ID" OnDataBound="FormulariosCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <%--<CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="FormulariosCheckBoxList" 
                                runat="server" ErrorMessage="Seleccionar al menos un formulario al que tendra acceso el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists1">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>--%>
                        </div>
                    </div>
                          <!--/box-->
                         <div class="box formulario">
                        <div class="form-item mcol">
                            <label>
                                Grupos</label>
                            <asp:CheckBoxList ID="GruposCheckBoxList" runat="server" DataSourceID="ODSGrupo"
                                DataTextField="Descrip" DataValueField="Numero" OnDataBound="GruposCheckBoxList_DataBound">
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="GruposCheckBoxList" Enabled="false"
                                runat="server" ErrorMessage="Seleccionar al menos un grupo al que pertenecera el usuario"
                                ID="RequiredFieldValidatorForCheckBoxLists2">*
                            </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                    </div>
                          <!--/box-->
                        </div>
                    </div>

               
                    
                    <asp:CustomValidator ID="ValidadorIdentif" runat="server" 
                        ControlToValidate="IdentifTextBox" CssClass="messages msg-error" 
                        ErrorMessage="El nombre de usuario ya existe. Por favor ingrese otro nombre para el nuevo usuario." 
                        onservervalidate="ValidadorIdentif_ServerValidate" Display="None"></asp:CustomValidator>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                 
               
                    <asp:ObjectDataSource ID="ODSFormularios" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.frmFormularioTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="frm_ID" Type="Int32" />
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="frm_ASP" Type="String" />
                            <asp:Parameter Name="frm_PrintASP" Type="String" />
                            <asp:Parameter Name="frm_Titulo" Type="String" />
                            <asp:Parameter Name="frm_Orden" Type="Int32" />
                            <asp:Parameter Name="frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="frm_GuardaPrevia" Type="Boolean" />
                            <asp:Parameter Name="Original_frm_ID" Type="Int32" />
                            <asp:Parameter Name="Original_frm_ASP" Type="String" />
                            <asp:Parameter Name="Original_frm_PrintASP" Type="String" />
                            <asp:Parameter Name="Original_frm_Titulo" Type="String" />
                            <asp:Parameter Name="Original_frm_Orden" Type="Int32" />
                            <asp:Parameter Name="Original_frm_Habilitado" Type="Int16" />
                            <asp:Parameter Name="Original_frm_Evaluacion" Type="Int16" />
                            <asp:Parameter Name="Original_frm_GuardaPrevia" Type="Boolean" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ODSGrupo" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.GrupoTableAdapter"
                        UpdateMethod="Update">
                        <DeleteParameters>
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FHBaja" Type="DateTime" />
                            <asp:Parameter Name="UsrBaja" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                            <asp:Parameter Name="Original_Numero" Type="Int32" />
                            <asp:Parameter Name="Original_Descrip" Type="String" />
                            <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                            <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                            <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                            <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                       <br />
                    <div class="controls">
                        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                            Text="Insertar" CssClass="form-submit" />
                        <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancelar" />
                    </div>
                    </asp:Panel>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ODSUsuario" runat="server" SelectMethod="GetUsuario" TypeName="com.paginar.johnson.BL.ControllerUsuarios"
                UpdateMethod="UpdateUsuario" OnObjectCreating="ODSUsuario_ObjectCreating" OnObjectDisposing="ODSUsuario_ObjectDisposing"
                InsertMethod="InsertUsuario" OldValuesParameterFormatString="{0}">
                <%-- <SelectParameters>
                    <asp:ControlParameter ControlID="GridUsuarios" Name="UsuarioID" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>--%>
                <InsertParameters>
                    <asp:Parameter Name="Apellido" Type="String" />
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="FHNacimiento" Type="DateTime" />
                    <asp:Parameter Name="Legajo" Type="Int32" />
                    <asp:Parameter Name="Interno" Type="String" />
                    <asp:Parameter Name="InternoIP" Type="String" />
                    <asp:Parameter Name="UbicacionID" Type="String" />
                    <asp:Parameter Name="SectorID" Type="Int32" />
                    <asp:Parameter Name="EsPayRoll" Type="Int32" />
                    <asp:Parameter Name="Cluster" Type="Int32" />
                    <asp:Parameter Name="EscalaSalarial" Type="Int32" />
                    <asp:Parameter Name="UsuarioFoto" Type="String" />
                    <asp:Parameter Name="AreaID" Type="Int32" />
                    <asp:Parameter Name="DireccionID" Type="Int32" />
                    <asp:Parameter Name="CargoID" Type="Int32" />
                    <asp:Parameter Name="ACargoDe" Type="Int32" />
                    <asp:Parameter Name="EstaEnQeQ" Type="Int32" />
                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                    <asp:Parameter Name="Identif" Type="String" />
                    <asp:Parameter Name="Clave" Type="String" />
                    <asp:Parameter Name="LegajoWD" Type="Int32" />
                </InsertParameters>
                <%-- <SelectParameters>
                    <asp:ControlParameter ControlID="GridUsuarios" Name="UsuarioID" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>--%>
                <UpdateParameters>
                    <asp:Parameter Name="UsuarioID" Type="Int32" />
                    <asp:Parameter Name="LoginID" Type="Int32" />
                    <asp:Parameter Name="Apellido" Type="String" />
                    <asp:Parameter Name="Nombre" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="FHNacimiento" Type="DateTime" />
                    <asp:Parameter Name="Legajo" Type="Int32" />
                    <asp:Parameter Name="Interno" Type="String" />
                    <asp:Parameter Name="InternoIP" Type="String" />
                    <asp:Parameter Name="UbicacionID" Type="String" />
                    <asp:Parameter Name="SectorID" Type="Int32" />
                    <asp:Parameter Name="EsPayRoll" Type="Int32" />
                    <asp:Parameter Name="Cluster" Type="Int32" />
                    <asp:Parameter Name="EscalaSalarial" Type="Int32" />
                    <asp:Parameter Name="UsuarioFoto" Type="String" />
                    <asp:Parameter Name="AreaID" Type="Int32" />
                    <asp:Parameter Name="DireccionID" Type="Int32" />
                    <asp:Parameter Name="CargoID" Type="Int32" />
                    <asp:Parameter Name="ACargoDe" Type="Int32" />
                    <asp:Parameter Name="EstaEnQeQ" Type="Int32" />
                    <asp:Parameter Name="FHMod" Type="DateTime" />
                    <asp:Parameter Name="UsrMod" Type="Int32" />
                    <asp:Parameter Name="Identif" Type="String" />
                    <asp:Parameter Name="Clave" Type="String" />
                    <asp:Parameter Name="LegajoWD" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="VResultado" runat="server" OnActivate="VResultado_Activate">
            <div class="messages msg-exito">
                <span lang="ES-TRAD">La operación fue realizada exitosamente</span>
            </div>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="5000" OnTick="Timer1_Tick">
            </asp:Timer>
        </asp:View>
    </asp:MultiView>
</asp:Content>
