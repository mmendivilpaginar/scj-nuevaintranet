﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="NoticiaCategoria.aspx.cs" Inherits="com.paginar.johnson.Web.BO.NoticiaCategoria" %>
<%@ MasterType VirtualPath="~/BO/MasterBO.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Categorías</h2>
    <asp:HiddenField ID="HFUserID" runat="server" />
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
    <asp:MultiView ID="MVCategorias" runat="server">
        <asp:View ID="VSeleccion" runat="server">
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            <br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-text"></asp:TextBox>
            <br /><br />
            <asp:Button ID="Button1" runat="server" Text="Buscar" CssClass="form-submit" 
                onclick="Button1_Click" />
            <br /><br />


            <asp:GridView ID="GVListadoCategorias" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataKeyNames="CategoriaID" 
                DataSourceID="ODSCategorias" onrowcommand="GVListadoCategorias_RowCommand">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButtonEdiar" runat="server" CausesValidation="False" 
                                CommandName="Editar" CommandArgument='<%# Eval("CategoriaID") %>' Text="Editar"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonEliminar" runat="server" CausesValidation="False" 
                                CommandName="Eliminar" CommandArgument='<%# Eval("CategoriaID") %>' onclientclick="return confirm('¿Desea eliminar esta categoría?')" Text="Eliminar"></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonDetalle" runat="server" CausesValidation="False" 
                                CommandName="Detalle" CommandArgument='<%# Eval("CategoriaID") %>' Text="Detalle"></asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CategoriaID" SortExpression="CategoriaID" 
                        Visible="False">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("CategoriaID") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("CategoriaID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Descrip" HeaderText="Categoría" 
                        SortExpression="Descrip" />
                </Columns>
                <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                    </div>
                </EmptyDataTemplate>
                <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ODSCategorias" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getInfCategorias" 
                TypeName="com.paginar.johnson.BL.ControllerNoticias"></asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ODSCategoriasSearch" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getInfCat_Search" 
                TypeName="com.paginar.johnson.BL.ControllerNoticias">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TextBox1" Name="Search" PropertyName="Text" 
                        Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click"><asp:Image ID="Image1" runat="server" ImageUrl="~/DynamicData/Content/Images/plus.gif" />Insertar nuevo elemento</asp:LinkButton>

        </asp:View>
        <asp:View ID="VEdicionDetalle" runat="server">
        <div class="form-item">
            <asp:FormView ID="FVEdicionDetalle" runat="server" DataKeyNames="CategoriaID" 
                DataSourceID="ODSFormDetalles" ondatabound="FVEdicionDetalle_DataBound" 
                oniteminserted="FVEdicionDetalle_ItemInserted" 
                oniteminserting="FVEdicionDetalle_ItemInserting">
                <EditItemTemplate>
<%--                    CategoriaID:
                    <asp:Label ID="CategoriaIDLabel1" runat="server" 
                        Text='<%# Eval("CategoriaID") %>' />
--%>                    <label>
                    Categoría:
                    </label>
                    <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DescripTextBox"
                        ErrorMessage="Debe indicar el nombre de la categoría">*</asp:RequiredFieldValidator>
                    <br />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CssClass="form-submit" CausesValidation="True" 
                        CommandName="Update" Text="Actualizar" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CssClass="form-submit"
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" 
                        onclick="UpdateCancelButton_Click" />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <label>
                    Categoría:
                    </label>
                    <asp:HiddenField ID="HFUsrAltaInsert" runat="server" Value='<%# Bind("UsrAlta") %>' />
                    <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DescripTextBox"
                        ErrorMessage="Debe indicar el nombre de la categoría">*</asp:RequiredFieldValidator>
                    <br />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CssClass="form-submit"
                        CommandName="Insert" Text="Agregar" onclick="InsertButton_Click" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CssClass="form-submit"
                        CausesValidation="False" CommandName="Cancel" Text="Cancelar" 
                        onclick="InsertCancelButton_Click" />
                </InsertItemTemplate>
                <ItemTemplate>
                    <%--CategoriaID:
                    <asp:Label ID="CategoriaIDLabel" runat="server" 
                        Text='<%# Eval("CategoriaID") %>' />
                    <br />--%>
                    <label>
                    Categoría
                    </label>
                    <asp:Label ID="DescripLabel" runat="server" Text='<%# Bind("Descrip") %>' />
                    <br />
                    <br />
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="form-submit" 
                        onclick="LinkButton2_Click">Volver</asp:LinkButton>
                    <br />
                    <br />
                </ItemTemplate>
            </asp:FormView>

            <asp:ObjectDataSource ID="ODSFormDetalles" runat="server" 
                OldValuesParameterFormatString="{0}" 
                SelectMethod="getinfCategoria_ById" 
                TypeName="com.paginar.johnson.BL.ControllerNoticias" 
                UpdateMethod="infCat_Update" InsertMethod="infCat_Insert">
                <InsertParameters>
                    <asp:Parameter Name="Descrip" Type="String" />
                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFIDEdicionDetalle" Name="CategoriaID" 
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="CategoriaID" Type="Int32" />
                    <asp:Parameter Name="Descrip" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>

            <asp:HiddenField ID="HFIDEdicionDetalle" runat="server" />

        </div>
        </asp:View>
    </asp:MultiView>
    
    
</asp:Content>
