﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirNewsletter.aspx.cs" Inherits="com.paginar.johnson.Web.BO.ImprimirNewsletter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Listado de Newsletter</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
</head>
<body>
    <form id="formNoticiasABM" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Listado Newsletter</h1>
            <img id="logo" src="../images/logoprint.gif" alt="Listado Newsletter"/>
        </div>
        
           <div id="noprint">
             <div class="controls">               
                <asp:Button ID="Button3" runat="server" OnClientClick="window.print(); return false;"
                    Text="Imprimir"  />
            </div>
           </div>


       <asp:GridView ID="gvNewsletter" runat="server" AutoGenerateColumns="False"  >
                    <Columns>
                        <asp:BoundField DataField="titulo" HeaderText="Nombre" />
                        <asp:BoundField DataField="fhalta" DataFormatString="{0: dd/MM/yyyy}" HeaderText="Fecha de Alta" />
                        <asp:BoundField DataField="estado" HeaderText="Estado" />
                        <asp:BoundField DataField="envio" HeaderText="Estado Envío" />
                        <asp:BoundField DataField="fechaenvio" DataFormatString="{0: dd/MM/yyyy}" HeaderText="Fecha de Envio" />                     
                    </Columns>
                </asp:GridView>


       </div>
    
    </form>
</body>
</html>