﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


namespace com.paginar.johnson.Web.BO
{
    public partial class WorkshopsABM : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            DropDownList DDEstado = (DropDownList)FVWorkshop.Controls[0].FindControl("IdEstadoTextBox");
            if (!Page.IsPostBack)
            {
                AvisoCluster.Visible = false;
                if (this.ObjectUsuario.clusteriddefecto != 1)
                {
                    AvisoCluster.Visible = true;
                    pnlContenido.Visible = false;
                }
                else
                {
                    pnlContenido.Visible = true;
                }
            }
            
        }

        protected void GVCursos_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        public string cortarCadena(string cadenaCompleta)
        {
            string cadenaCortada = string.Empty;
            if (cadenaCompleta.Length > 12)
            {
                cadenaCortada = cadenaCompleta.Substring(0, 12) + "...";
            }
            else
            {
                cadenaCortada = cadenaCompleta;
            }


            return cadenaCortada;
        
        }
             

        public string Estado(string IdEstado)
        {
            switch (int.Parse(IdEstado))
            {
                case 1:
                        return "Pendiente";
                        break;
                case 2:
                        return "Abierto";
                        break;
                case 3:
                        return "Cerrado";
                        break;

                default:
                        return " ";
                    break;
            }
        }

        protected void BTNAgregarCurso_Click(object sender, EventArgs e)
        {
            FVWorkshop.ChangeMode(FormViewMode.Insert);

        }

        //protected void Subir_Click(object sender, EventArgs e)
        //{
        //    int a = 0;

        //    FileUpload FU = new FileUpload();
        //    FU = (FileUpload)FVWorkshop.Controls[0].FindControl("FileUpLoad2");
        //    Label L1 = (Label)FVWorkshop.Controls[0].FindControl("DocumentoX");
        //    HiddenField HF = (HiddenField)FVWorkshop.Controls[0].FindControl("Documento");

        //    if (null != FU.PostedFile)
        //    {
        //            FU.PostedFile.SaveAs(Server.MapPath("Document") + "\\" + FU.FileName);
        //            HF.Value = FU.FileName.ToString();
        //            L1.Text = "Archivo: " + FU.FileName.ToString();
       
        //    }

        //}

        protected void FVWorkshop_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            
        }

        protected void FVWorkshop_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVCursos.DataBind();
            
        }

        protected void FVWorkshop_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVCursos.DataBind();

        }

        protected void FVWorkshop_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVCursos.DataBind();

        }




        //protected void DDIdEstadoTextBox_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    HiddenField HFEstado = (HiddenField)FVWorkshop.Controls[0].FindControl("IdEstadoTextBox");

        //}


    }
}