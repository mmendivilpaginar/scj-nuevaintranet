﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="IndumentariaAdministraciondePeriodos.aspx.cs" Inherits="com.paginar.johnson.Web.BO.IndumentariaAdministraciondePeriodos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
    function AdvertenciaClonado() {

        if (confirm('Al realizar esta acción estará Clonando todos los tramites del período seleccionado con los datos del nuevo período. Quedando los nuevos trámites generados asignados a este último.\n ¿Desea continuar?'))
            return true;
        else
            return false;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Ropa de Trabajo - Administración de Períodos<asp:HiddenField ID="HFClone" 
        runat="server" />
    <asp:HiddenField ID="HFPeriodoActual" runat="server" />
    <asp:HiddenField ID="HFUltimoPeriodo" runat="server" />
    </h2>
    <asp:Label ID="blFeedback" runat="server"></asp:Label>
    <asp:GridView ID="GVPeriodos" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="PeriodoId" DataSourceID="ODSPeriodos" 
        onselectedindexchanged="GVPeriodos_SelectedIndexChanged" 
        onrowcommand="GVPeriodos_RowCommand">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" Visible='<%# verificarPeriodo(Eval("Hasta","{0:dd/MM/yyyy}")) %>' CausesValidation="False" 
                        CommandName="Select" Text="Seleccionar"></asp:LinkButton>
                    <asp:Label ID="Label4" runat="server" Text="Seleccionar" 
                        ToolTip="Perídodo vencido, no puede ser editado" Visible='<%# !verificarPeriodo(Eval("Hasta","{0:dd/MM/yyyy}")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" 
                        CommandName="Update" Text="Actualizar"></asp:LinkButton>
                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" 
                        CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                        CommandName="Clonar" CommandArgument='<%# Eval("PeriodoId") %>' Text="Clonar"></asp:LinkButton>
                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PeriodoId" InsertVisible="False" 
                SortExpression="PeriodoId" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("PeriodoId") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("PeriodoId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                SortExpression="Descripcion" />
            <asp:TemplateField HeaderText="Desde" SortExpression="Desde">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Desde") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Desde","{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Hasta" SortExpression="Hasta">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Hasta") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Hasta","{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSPeriodos" runat="server" 
        InsertMethod="tramite_iniciar" OldValuesParameterFormatString="original_{0}" 
        SelectMethod="SeleccionarPeriodosTodos" 
        TypeName="com.paginar.johnson.BL.ControllerIndumentaria" 
        UpdateMethod="IndumentariaPeriodoEditar">
        <InsertParameters>
            <asp:Parameter Name="UsrNro" Type="Int32" />
            <asp:Parameter Name="Modo" Type="String" />
            <asp:Parameter Name="UsuarioId" Type="Int32" />
            <asp:Parameter Name="Apellido" Type="String" />
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="Legajo" Type="Int32" />
            <asp:Parameter Name="Sector" Type="String" />
            <asp:Parameter Name="Area" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Desde" Type="DateTime" />
            <asp:Parameter Name="Hasta" Type="DateTime" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="PeriodoId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" 
        onclick="btnNuevo_Click" />
    &nbsp;
    <asp:Button ID="btn_editar" runat="server" Text="Editar" 
        onclick="btn_editar_Click" ToolTip="Editar último período" />
    <br />
    
    <br />
    <asp:FormView ID="FVPeriodo" runat="server" DataKeyNames="PeriodoId" 
        DataSourceID="ODSPeriodo" oniteminserted="FVPeriodo_ItemInserted" 
        onitemupdated="FVPeriodo_ItemUpdated" 
        onitemcommand="FVPeriodo_ItemCommand" 
        oniteminserting="FVPeriodo_ItemInserting" 
        onitemupdating="FVPeriodo_ItemUpdating">
        <EditItemTemplate>
            <strong>Descripción:</strong><br />
            <asp:TextBox ID="DescripcionTextBox" runat="server" 
                Text='<%# Bind("Descripcion") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="DescripcionTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <br />
            <strong>Desde:</strong><br />
            <asp:TextBox ID="DesdeTextBox" runat="server" Text='<%# Bind("Desde","{0:dd/MM/yyyy}") %>' />
            <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="DesdeTextBox">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="DesdeTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                ControlToCompare="HastaTextBox" ControlToValidate="DesdeTextBox" 
                ErrorMessage="CompareValidator" ForeColor="Red" Operator="LessThan" Type="Date" 
                ValidationGroup="a">*</asp:CompareValidator>
            <br />
            <strong>Hasta:</strong><br />
            <asp:TextBox ID="HastaTextBox" runat="server" Text='<%# Bind("Hasta","{0:dd/MM/yyyy}") %>' />
            <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="HastaTextBox">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="HastaTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" 
                ControlToCompare="DesdeTextBox" ControlToValidate="HastaTextBox" 
                ErrorMessage="CompareValidator" ForeColor="Red" Operator="GreaterThan" 
                Type="Date" ValidationGroup="a">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToCompare="HastaTextBox" ForeColor="Red"
                ControlToValidate="DesdeTextBox" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                Operator="LessThanEqual" Type="Date" ValidationGroup="a" Display="Dynamic">*</asp:CompareValidator>

            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" ValidationGroup="a"
                CommandName="Update" Text="Actualizar" />
&nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" 
                CommandName="Cancel" Text="Cancelar" />
        </EditItemTemplate>
        <InsertItemTemplate>
            <strong>Descripción:</strong><br />
            <asp:TextBox ID="DescripcionTextBox" runat="server" 
                Text='<%# Bind("Descripcion") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="DescripcionTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <br />
            <strong>Desde:</strong><br />
            <asp:TextBox ID="DesdeTextBox" runat="server" Text='<%# Bind("Desde","{0:dd/MM/yyyy}") %>' />
            <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="DesdeTextBox">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="DesdeTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                ControlToCompare="HastaTextBox" ControlToValidate="DesdeTextBox" 
                ErrorMessage="CompareValidator" ForeColor="Red" Operator="LessThan" Type="Date" 
                ValidationGroup="a">*</asp:CompareValidator>
            <br />
            <strong>Hasta:</strong><br />
            <asp:TextBox ID="HastaTextBox" runat="server" Text='<%# Bind("Hasta","{0:dd/MM/yyyy}") %>' />
            <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="HastaTextBox">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="HastaTextBox" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="a">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" 
                ControlToCompare="DesdeTextBox" ControlToValidate="HastaTextBox" 
                ErrorMessage="CompareValidator" ForeColor="Red" Operator="GreaterThan" 
                Type="Date" ValidationGroup="a">*</asp:CompareValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" 
                ControlToValidate="HastaTextBox" ErrorMessage="*" ForeColor="Red" 
                onservervalidate="CustomValidator1_ServerValidate" ValidateEmptyText="True" 
                ValidationGroup="a"></asp:CustomValidator>
            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToCompare="HastaTextBox" ForeColor="Red"
                ControlToValidate="DesdeTextBox" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                Operator="LessThanEqual" Type="Date" ValidationGroup="a" Display="Dynamic">*</asp:CompareValidator>
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Aceptar" ValidationGroup="a" />
            <asp:LinkButton ID="CloneButton" runat="server" CausesValidation="True" 
                CommandName="Clonar" Text="Aceptar" ValidationGroup="a" Visible="False" OnClientClick="return AdvertenciaClonado();" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" 
                onclick="InsertCancelButton_Click" />
        </InsertItemTemplate>
        <ItemTemplate>
            <strong>Descripción:</strong><br />
            <asp:Label ID="DescripcionLabel" runat="server" 
                Text='<%# Eval("Descripcion") %>' />
            <br />
            <strong>Desde:</strong><br />
            <asp:Label ID="DesdeLabel" runat="server" Text='<%# Eval("Desde","{0:dd/MM/yyyy}") %>' />
            <br />
            <strong>Hasta:</strong><br />
            <asp:Label ID="HastaLabel" runat="server" Text='<%# Bind("Hasta","{0:dd/MM/yyyy}") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                CommandName="Edit" Text="Editar" />
&nbsp;
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSPeriodo" runat="server" 
        InsertMethod="IndumentariaPeriodoInsertar" 
        OldValuesParameterFormatString="{0}" 
        SelectMethod="IndumentariaPeriodoSeleccionarPorId" 
        TypeName="com.paginar.johnson.BL.ControllerIndumentaria" 
        UpdateMethod="IndumentariaPeriodoEditar" 
        onselecting="ODSPeriodo_Selecting">
        <InsertParameters>
            <asp:Parameter Name="Desde" Type="DateTime" />
            <asp:Parameter Name="Hasta" Type="DateTime" />
            <asp:Parameter Name="Descripcion" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVPeriodos" Name="PeriodoId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Desde" Type="DateTime" />
            <asp:Parameter Name="Hasta" Type="DateTime" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="PeriodoId" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:Literal ID="ltrlErrorMsg" runat="server"></asp:Literal>
</asp:Content>
