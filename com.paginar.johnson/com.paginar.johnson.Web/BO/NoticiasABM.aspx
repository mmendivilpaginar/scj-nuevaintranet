<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="NoticiasABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.NoticiasABM" %>
<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
.AspNet-GridView
{
    overflow-x: auto !important;
    }
</style>

        <style type="text/css">

        .LockOff {
            display: none;
            visibility: hidden;
        }

        .LockOn {
            display: block; 
            visibility: visible;
            position: absolute;
            z-index: 999;
            top: 750px;
            left: 100px;
            width: 100%;
            height: 100%;        
            text-align: center;
            padding-top: 20%;
            filter: alpha(opacity=75);
            opacity: 0.75;
        }

        .PrProgress{
            width:300px;
            margin:0 auto;
        }
        .PrContainer{
            border: solid 1px #808080;
            border-width: 1px 0px;
        }
        .PrHeader{
            background: url(../img/sprite.png) repeat-x 0px 0px;
            border-color: #808080 #808080 #ccc;
            border-style: solid;
            border-width: 0px 1px 1px;
            padding: 0px 10px;
            color: #000000;
            font-size: 9pt;
            font-weight: bold;
            line-height: 1.9;  
            white-space:nowrap;
            font-family: arial,helvetica,clean,sans-serif;
        }
        .PrBody{
            background-color: #f2f2f2;
            border-color: #808080;
            border-style: solid;
            border-width: 0px 1px;
            padding: 10px;
        }    
        
    </style>

       <script type="text/javascript">
           function skm_LockScreen(str) {
               var lock = document.getElementById('skm_LockPane');
               if (lock) {
                   lock.className = 'LockOn';
               }
           }
     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<script src="../js/jquery.corner.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
                        
            $('.noticia .tags li').corner('10px');
});
        </script>


    <h2>
        Administraci�n de Noticias</h2>
    <%--<asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">--%>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
    <asp:TabPanel runat="server" ID="TabNoticias" TabIndex="0">
    <HeaderTemplate>
    Administraci�n de Noticias
    </HeaderTemplate>
    <ContentTemplate>

        <div id="skm_LockPane" class="LockOff">
            <div class="PrProgress">
                <div id="innerPopup" class="PrContainer">
                    <div class="PrHeader">
                        Subiendo archivo aguarde un instante.
                    </div>
                    <div class="PrBody">
                        <img src="../images/clock.jpg"  width="16px" alt="Subiendo..." />
                    </div>
                </div>
            </div>
        </div>

        <asp:Panel ID="PanelPrincipalNoticias" runat="server" 
            DefaultButton="BuscarButton">
        
    <asp:MultiView ID="MVN" runat="server">
        <asp:View ID="VBuc" runat="server" OnActivate="VBuc_Activate">
        <asp:Panel ID="Panel1" runat="server" DefaultButton="BuscarButton">
            <div class="controls">
                <asp:Button ID="ButtonNuevaNoticia" runat="server" OnClick="ButtonNuevaNoticia_Click"
                    Text="Nueva Noticia" />
            </div>
            <div class="form-item leftHalf">
                <label>
                    Estado</label>
                <asp:DropDownList ID="EstadoDropDownList" runat="server">
                    <asp:ListItem Value="" Text="Seleccionar"></asp:ListItem>
                    <asp:ListItem Value="1">Activo</asp:ListItem>
                    <asp:ListItem Value="2">Inactivo</asp:ListItem>
                    <asp:ListItem Value="3">Archivado</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item rightHalf">
                <label>
                    Destacada</label>
                <asp:DropDownList ID="DestacadosDropDownList" runat="server">
                    <asp:ListItem  Value=""  Text="Seleccionar"></asp:ListItem>
                    <asp:ListItem Value="1">Destacada</asp:ListItem>
                    <asp:ListItem Value="2">General</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item leftHalf">
                <label>
                    Categor�a</label>
				<asp:DropDownList ID="CategoriasDropDownList" runat="server" DataSourceID="ObjectDataSourceCategorias"
                    DataTextField="Descrip" DataValueField="CategoriaID" 
                    AppendDataBoundItems="True">
                    <asp:ListItem  Value=""  Text="Seleccione una categor�a"></asp:ListItem>

                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceCategorias" runat="server" SelectMethod="GetData"
                    TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter">

                </asp:ObjectDataSource>
                
            </div>
            <div class="form-item rightHalf">
                <label>
                    &nbsp;</label>
                <asp:DropDownList ID="BuscarPorDropDownList" runat="server">
                    <asp:ListItem Value="FALTA">Fecha de alta</asp:ListItem>
                    <asp:ListItem Value="FMODIF">Fecha de modificaci�n</asp:ListItem>
                    <asp:ListItem Value="FAsoc">Fecha de Publicaci�n - Recordatorios</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item leftHalf">
                <label>
                    Entre</label>
                <asp:TextBox ID="DesdeTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="DesdeTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="DesdeTextBox" PopupButtonID="Image1"></asp:CalendarExtender>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="DesdeTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                (dd/mm/aaaa)
            </div>
            <div class="form-item rightHalf">
                <label>
                    y</label>
                <asp:TextBox ID="HastaTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="HastaTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                    PopupButtonID="Image2" TargetControlID="HastaTextBox"></asp:CalendarExtender>
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="HastaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>

                (dd/mm/aaaa)
            </div>
            <div class="controls">
                <asp:Button ID="BuscarButton" runat="server" Text="Buscar" OnClick="BuscarButton_Click" />
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/images/printer.png" />&nbsp;
                <asp:HyperLink ID="HyperLink3" runat="server" 
                    NavigateUrl="~/BO/imprimirNoticiasABM.aspx" Target="_blank">Imprimir Listado</asp:HyperLink>

            </div>
            
            </asp:Panel>
            <asp:GridView ID="GVNoticias" Visible="False" runat="server" DataSourceID="ObjectDataSourceNoticias"
                AutoGenerateColumns="False" DataKeyNames="InfoID" AllowSorting="True" OnRowCommand="GVNoticias_RowCommand"
                AllowPaging="True" PageSize="8" onrowdatabound="GVNoticias_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="InfoID">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("InfoID") %>'></asp:Label></EditItemTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenFieldInfoID" runat="server" Value='<%# Eval("InfoID") %>' />
                            <asp:LinkButton ID="InfoIDLinkButton" runat="server" CommandArgument='<%# Eval("InfoID") %>'
                                CommandName="Select" Text='<%# Eval("InfoID") %>'></asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Categor�a"   >
                         <ItemTemplate>
                            <asp:Label ID="categoria" runat="server" Text='<%# Bind("CategoriaDescrip") %>'></asp:Label>
                          

                             <asp:Repeater runat="server" ID="rptTags">
                                 <ItemTemplate>
                                       <br />
                                       <asp:Image ImageUrl="~/images/tag.png" runat="server" ID="imgTag" />
                                       <asp:Label runat="server" ID="lblTag" Text='<%# Eval("descripcion") %>'  Font-Size="Smaller" ></asp:Label>
                                      <br />
                                 </ItemTemplate>
                             </asp:Repeater>
                         </ItemTemplate>
                         <ItemStyle Width="300px" />

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="T�tulo" SortExpression="Titulo">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Titulo") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="TituloLabel" runat="server" Text='<%# Bind("Titulo") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                   <asp:BoundField DataField="FHAlta" HeaderText="Fecha Alta" SortExpression="FHAlta"
                        DataFormatString="{0:dd/MM/yyyy}" />
                     <asp:BoundField DataField="FechaAsoc" HeaderText="Fecha Publicaci�n" SortExpression="FechaAsoc"
                        DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:TemplateField HeaderText="Recordatorios">
                        <ItemTemplate>
                           <asp:Repeater ID="RepeaterRecordatorios" runat="server">
                                 <HeaderTemplate>
                                  <table style="border: 0px;">
                                 </HeaderTemplate>
                                 <ItemTemplate>
                             <%--     <tr><td style="border: 0px;"><asp:Label ID="Label1" runat="server" Text='<%# Eval("FHRecordatorio","{0:dd/MM/yyyy}") %>'></asp:Label></td></tr>--%>
                                 </ItemTemplate>
                                 <FooterTemplate>
                                 </table>
                                 </FooterTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DestacadaDesc" HeaderText="Destacada" SortExpression="DestacadaDesc" />
                    <asp:BoundField DataField="EstadoDesc" HeaderText="Estado" SortExpression="EstadoDesc" />
                    <asp:BoundField DataField="Votos" HeaderText="Votos" SortExpression="Votos" />
                    <asp:TemplateField HeaderText="Privacidad" SortExpression="Privacidad">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# GetIntString(Eval("Privacidad")) %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="URL Intra" InsertVisible="False" SortExpression="InfoID">
                        <ItemTemplate>
                            <asp:Label ID="Label31" runat="server" Text='<%# GetUrl(Convert.ToInt32(Eval("InfoID")),1,Convert.ToInt32(Eval("Privacidad"))) %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="URL News" InsertVisible="False" SortExpression="InfoID">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# GetUrl(Convert.ToInt32(Eval("InfoID")),2,Convert.ToInt32(Eval("Privacidad"))) %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton4" runat="server" CommandArgument='<%# ((GridViewRow)Container).RowIndex %>'
                                CommandName="Comentarios">Comentarios</asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton8" runat="server" 
                                CommandArgument='<%# Eval("InfoID") %>' CommandName="Clonar">Clonar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de b�squeda ingresado.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceNoticias" runat="server"
                SelectMethod="GetBusquedaABM" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                OnSelecting="ObjectDataSourceNoticias_Selecting">
                <SelectParameters>
                    <asp:ControlParameter ControlID="EstadoDropDownList" Name="Estado" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="DestacadosDropDownList" Name="Destacado" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="CategoriasDropDownList" Name="CategoriaID" PropertyName="SelectedValue"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="BuscarPorDropDownList" Name="TipoBusq" PropertyName="SelectedValue"
                        Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="Desde" PropertyName="Text" Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="Hasta" PropertyName="Text" Type="DateTime" />
                </SelectParameters>









            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="VABM" runat="server" OnActivate="VABM_Activate">
            <asp:FormView ID="FVNoticia" runat="server" DataKeyNames="InfoID" DataSourceID="ODSNoticia"
                DefaultMode="Edit" OnItemUpdating="FormView1_ItemUpdating" OnItemInserted="FVNoticia_ItemInserted"
                OnItemInserting="FVNoticia_ItemInserting" OnItemUpdated="FVNoticia_ItemUpdated"                
                OnItemDeleted="FVNoticia_ItemDeleted" OnDataBound="FVNoticia_DataBound" 
                onprerender="FVNoticia_PreRender">
                <EditItemTemplate>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="UpdateButton">
                    <asp:ValidationSummary ValidationGroup="UPDATE" DisplayMode="BulletList" ID="ValidationSummary2"
                        runat="server" />
                    <asp:HiddenField ID="HiddenFieldUsuario" runat="server" Value='<%# Eval("UsrAlta") %>'/>
                    <asp:HiddenField ID="InfoIDLabel1" runat="server" Value='<%# Eval("InfoID") %>' />
                    <div class="form-item leftHalf">
                        <label>
                            Estado</label>
                        <asp:DropDownList ID="EstadoDropDownList" runat="server" SelectedValue='<%# Bind("Estado") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                            <asp:ListItem Value="2">Inactivo</asp:ListItem>
                            <asp:ListItem Value="3">Archivado</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RangeValidator1" InitialValue=""
                            ControlToValidate="EstadoDropDownList" runat="server" Text="*" ErrorMessage="Seleccionar un valor para el campo Estado"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Destacada</label>
                        <asp:DropDownList ID="DestacadosDropDownList" runat="server" SelectedValue='<%# Bind("Destacada") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Destacada</asp:ListItem>
                            <asp:ListItem Value="2">General</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RequiredFieldValidator6"
                            InitialValue="" ControlToValidate="DestacadosDropDownList" runat="server" Text="*"
                            ErrorMessage="Seleccionar un valor para el campo Destacado"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Categor�a</label>
                        <asp:DropDownList ID="CategoriaDropDownList" runat="server" DataSourceID="ODSCstegorias"
                            DataTextField="Descrip" AppendDataBoundItems="true" DataValueField="CategoriaID"
                            SelectedValue='<%# Bind("CategoriaID") %>' >
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ODSCstegorias" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                            OldValuesParameterFormatString="{0}" SelectMethod="GetDataNuevasandID" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter"
                            UpdateMethod="Update">

                            </asp:ObjectDataSource>
                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RequiredFieldValidator7"
                            InitialValue="" ControlToValidate="CategoriaDropDownList" runat="server" Text="*"
                            ErrorMessage="Seleccionar un valor para el campo Categoria"></asp:RequiredFieldValidator>

                          <br /><br />
                        <label>
                            Tags</label>

                        <asp:Repeater runat="server" ID="rptTags" DataSourceID="odsTags">
                        <ItemTemplate>
                          <asp:HiddenField runat="server" ID="hdTagId"  Value='<%# Eval("TagId")  %>' />
                          <asp:CheckBox  runat="server" ID="chkTag" Checked='<%# ((int)Eval("Check1")) == 1 ? true : false  %>' />
                          <asp:Label runat="server" ID="lblDesripcion" Text='<%# Eval("Descripcion") %>' ></asp:Label>
                          <br />
                         </ItemTemplate>
                        </asp:Repeater>
                        <br />
                <asp:ObjectDataSource ID="odsTags" runat="server" SelectMethod="getTagsByInfoId" TypeName="com.paginar.johnson.BL.ControllerNoticias" DeleteMethod="BannerDelete" InsertMethod="BannerClusterAdd" OldValuesParameterFormatString="original_{0}">
                        <SelectParameters>
                          <asp:ControlParameter ControlID="InfoIDLabel1" Name="InfoID"  PropertyName="Value" Type="Int32" />
                        </SelectParameters>                   
                </asp:ObjectDataSource>

                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            T�tulo</label>
                        <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>'  MaxLength="255" /><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" ValidationGroup="UPDATE" runat="server" ErrorMessage="Ingrese un valor en el campo Titulo"
                            ControlToValidate="TituloTextBox">*</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Fecha de Publicaci�n</label>
                        <asp:TextBox ID="FechaAsocTextBox" runat="server" Text='<%# Bind("FechaAsoc", "{0:dd/MM/yyyy}") %>'
                            SkinID="form-date"></asp:TextBox>
                        <asp:ImageButton ID="FechaPublicImage" runat="server" ImageUrl="~/images/Calendar.png" />
                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RequiredFieldValidator3"
                            runat="server" ControlToValidate="FechaAsocTextBox" ErrorMessage="Ingrese un valor en el campo Fecha de Publicaci�n">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ValidationGroup="UPDATE" ID="CompareValidator2" runat="server"
                            ControlToValidate="FechaAsocTextBox" ErrorMessage="El formato de la fecha ingresada en Fecha de Publicaci�n es incorrecto. El formato de fecha es dd/mm/yyyy."
                            Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        <asp:CustomValidator ValidationGroup="UPDATE" ID="CustomValidator2" runat="server"
                            ControlToValidate="FechaAsocTextBox" ErrorMessage="Las fechas de recordatorio deben ser mayor a la fecha de publicaci�n"
                            OnServerValidate="CustomValidator2_ServerValidate">*</asp:CustomValidator>
                        <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" Enabled="True"
                            Format="dd/MM/yyyy" PopupButtonID="FechaPublicImage" TargetControlID="FechaAsocTextBox"></asp:CalendarExtender>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Copete</label>
                        <asp:TextBox ID="CopeteTextBox" runat="server" Text='<%# Bind("Copete") %>' 
                            MaxLength="1000" />
                    </div>
                    <h3>Links y Videos</h3>
                    <asp:FormView ID="FVLink" runat="server" DataSourceID="ODSLinkNoticias" DefaultMode="Insert"
                        OnItemInserting="FVLink_ItemInserting" OnItemUpdated="FVLink_ItemUpdated" OnItemInserted="FVLink_ItemInserted">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenFieldInfoID" runat="server" Value='<%# Bind("InfoID") %>' />
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("LinkID") %>' />
                            <div class="form-item leftHalf">
                                <label>Descripci�n</label>
                                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' MaxLength="255"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DescripTextBox" Enabled="false"
                                    Display="Dynamic" ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Links">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="form-item rightHalf">
                                <label>URL</label>
                                <asp:TextBox ID="URLTextBox" runat="server" Text='<%# Bind("URL") %>' MaxLength="255" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="URLTextBox"
                                    Display="Dynamic" ErrorMessage="Ingrese un valor en el campo URL" ValidationGroup="Links">*</asp:RequiredFieldValidator>
                                <asp:LinkButton ID="UpdateButton" ValidationGroup="Links" runat="server" CausesValidation="True"
                                    CommandName="Update" Text="Modificar" />
                                <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                    Text="Cancelar" />
                            </div>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <div class="form-item leftHalf">
                                <label>
                                    Descripci�n</label>
                                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' MaxLength="255"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DescripTextBox" Enabled="false"
                                    Display="Dynamic" ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Links">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="form-item rightHalf">
                                <label>
                                    URL</label>
                                <asp:TextBox ID="URLTextBox" runat="server" Text='<%# Bind("URL") %>' MaxLength="255"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="URLTextBox"
                                    Display="Dynamic" ErrorMessage="Ingrese un valor en el campo URL" ValidationGroup="Links">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="controls">
                                <asp:LinkButton ID="InsertButton" ValidationGroup="Links" runat="server" CausesValidation="True"
                                    CommandName="Insert" Text="Agregar" CssClass="form-submit" />
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:ObjectDataSource ID="ODSLinkNoticias" runat="server" DeleteMethod="DeleteLink"
                        InsertMethod="InsertLink" OnObjectCreating="ObjectDataSourceLinkNoticias_ObjectCreating"
                        OnObjectDisposing="ObjectDataSourceLinkNoticias_ObjectDisposing" SelectMethod="GetLinkByLinkID"
                        TypeName="com.paginar.johnson.BL.ControllerNoticias" UpdateMethod="UpdateLink">
                        <DeleteParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="URL" Type="String" />
                        </InsertParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GVLinks" Name="LinkID" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="URL" Type="String" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="GVLinks" runat="server" AutoGenerateColumns="False" DataKeyNames="LinkID"
                        DataSourceID="ODSLinksNoticias" OnRowCommand="GVLinks_RowCommand" ShowHeader="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Descrip" SortExpression="Descrip">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("URL") %>' Target="_blank"
                                        Text='<%# Eval("Descrip") %>'></asp:HyperLink>

                                       <asp:Image ImageUrl="~/images/videoicon.png" runat="server" Width="16px" Height="16px"  Visible='<%# Eval("URL").ToString().Contains("youtube")||Eval("URL").ToString().Contains("youtu.be")||Eval("URL").ToString().Contains("vimeo") ? true: false %>' />
                            </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" />--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png"
                                        AlternateText="Borrar" CommandArgument='<%# Eval("LinkID") %>' CommandName="Delete"
                                        CausesValidation="false" />--%>
                                    <asp:LinkButton ID="ImageButton1" CommandArgument='<%# Eval("LinkID") %>' CommandName="Delete" runat="server">Eliminar</asp:LinkButton>   
                                        </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSLinksNoticias" runat="server" OldValuesParameterFormatString="{0}"
                        SelectMethod="GetLinkByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                        DeleteMethod="DeleteLink" OnObjectCreating="ODSLinksNoticias_ObjectCreating"
                        OnObjectDisposing="ODSLinksNoticias_ObjectDisposing" InsertMethod="InsertInfo">
                        <DeleteParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Titulo" Type="String" />
                            <asp:Parameter Name="Copete" Type="String" />
                            <asp:Parameter Name="Fuente" Type="String" />
                            <asp:Parameter Name="Estado" Type="Int32" />
                            <asp:Parameter Name="Destacada" Type="Int32" />
                            <asp:Parameter Name="Texto" Type="String" />
                            <asp:Parameter Name="CategoriaID" Type="Int32" />
                            <asp:Parameter Name="TipoInfoID" Type="Int32" />
                            <asp:Parameter Name="Privacidad" Type="Int32" />
                            <asp:Parameter Name="FHAlta" Type="DateTime" />
                            <asp:Parameter Name="UsrAlta" Type="Int32" />
                            <asp:Parameter Name="FechaAsoc" Type="DateTime" />
                        </InsertParameters>
                    </asp:ObjectDataSource>
                    <div class="form-item full">
                        <label>
                            Fuente</label>
                        <asp:TextBox ID="FuenteTextBox" runat="server" Text='<%# Bind("Fuente") %>' MaxLength="255" />
                    </div>
                    <h3>
                        Adjuntos</h3>				
                       <div class="messages msg-info">
                           
                           <asp:Label runat="server" ID="lblinfo1" Font-Size="Smaller"   Font-Bold="true" Text="Si el adjunto es una foto, para que �sta se visualice, debe ser un archivo de extensi�n .jpg o .png" ></asp:Label>
                           <br />
                            <asp:Label  Font-Size="Smaller"  Font-Bold="true" runat="server" ID="Label6" Text="Si el adjunto No es una Imagen, el campo Descripci�n es obligatorio"></asp:Label>
                            <br />
                            <asp:Label  Font-Size="Smaller" Font-Bold="true" runat="server" ID="Label7" Text="Si el adjunto es una Imagen, el valor que ingrese en el campo descripci�n debe ser un hiperv�nculo"></asp:Label>
                            <br />
                           <asp:Label  Font-Size="Smaller"  Font-Bold="true" runat="server" ID="Label8" Text="Si el adjunto es un video, el formato debe ser .mp4"></asp:Label>
                            <br />
                        </div>	
                    <div class="form-item leftHalf">
                        <label>
                            Descripci�n</label>
                        <asp:TextBox ID="DescripTextBox" runat="server" MaxLength="249" />
                       
                        <label>
                            Flyer</label>
                        <asp:CheckBox runat="server" ID="chkFlyer" />



                        <%--<asp:Panel ID="divErrorImg" runat="server" class="messages msg-error" Visible="false">
                            <asp:Label runat="server" ID="lblErrorImg" Text="Si el adjunto no es una imagen, la descripci�n es requer�da."></asp:Label>
                        </asp:Panel>--%>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DescripTextBox"
                            Display="Dynamic" ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Imagenes">*</asp:RequiredFieldValidator>--%>
                    </div>
                    
                    <br />
              

                    <div class="form-item rightHalf">


                        <label>Ruta</label>
                        <asp:FileUpload ID="FUImagenes" runat="server" />                        
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ControlToValidate="FUImagenes" ErrorMessage="Ingrese imagen a agregar" ValidationGroup="Imagenes">*</asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Imagenes"
                            ControlToValidate="FUImagenes" runat="Server" ErrorMessage="*" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG)$" />--%>
                        
                        <%--<asp:Label Text="Solo se permiten archivos JPG" Font-Size="Smaller" runat="server"
                            ID="LabelMensaje"></asp:Label>--%>
                             <asp:Label Text="No puede ingresar otra imagen" Font-Size="Smaller" runat="server" Visible="false"
                            ID="LabelMensajeFlyer"></asp:Label>
                    </div>

                    <div class="controls">
                                           
                    <asp:Button ID="BtnAgregarImagenes" ValidationGroup="Imagenes" runat="server" Text="Agregar"  OnClientClick="skm_LockScreen('Subiendo Archivo...'); return true;" 
                            OnClick="BtnAgregarImagenes_Click" /><br />
                    </div>

                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ErrorMessage="Falt� completar el campo Descripci�n, debe adjuntar el archivo nuevamente."
                        CssClass="messages msg-error" ControlToValidate="DescripTextBox" ValidationGroup="Imagenes"
                        ValidateEmptyText="true" OnServerValidate="CustomValidator4_ServerValidate"></asp:CustomValidator>
                    </div>

                    <asp:GridView ID="GVImagen" runat="server" AutoGenerateColumns="False" DataKeyNames="ImagenID"
                        DataSourceID="ODSImagenesNoticias" OnRowCommand="GVImagen_RowCommand" ShowHeader="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Descrip" SortExpression="Descrip">
                                <ItemTemplate>
                                    <asp:Label Text='<%# Eval("Descrip") %>' ID="HyperLink1" runat="server"></asp:Label>
                                     <asp:Image ID="imgvideo" ImageUrl="~/images/videoicon.png" runat="server" Width="16px" Height="16px" Visible='<%# Eval("path").ToString().Contains(".mp4")? true: false %>' />

                                </ItemTemplate>


                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flyer" >
                                <ItemTemplate>
                                       <asp:CheckBox runat="server" ID="chkFlyer"  Text="Flyer" Enabled="false"   Checked='<%# Convert.ToBoolean(Eval("Flyer")) %>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/delete.png"
                                        AlternateText="BORRAR" CommandArgument='<%# Eval("ImagenID") %>' CommandName="Delete"
                                        CausesValidation="false" />--%>
                                    <asp:LinkButton ID="ImageButton2" CommandArgument='<%# Eval("ImagenID") %>' CommandName="Delete" runat="server">Eliminar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSImagenesNoticias" runat="server" DeleteMethod="DeleteImagen"
                        InsertMethod="InsertImagen" OldValuesParameterFormatString="{0}" SelectMethod="GetImagenByInfoID"
                        TypeName="com.paginar.johnson.BL.ControllerNoticias" UpdateMethod="UpdateInfo"
                        OnObjectCreating="ODSImagenesNoticias_ObjectCreating" OnObjectDisposing="ODSImagenesNoticias_ObjectDisposing">
                        <DeleteParameters>
                            <asp:Parameter Name="ImagenID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="Path" Type="String" />
                        </InsertParameters>
                        <SelectParameters>
                            <%-- <asp:ControlParameter ControlID="InfoIDLabel1" Name="InfoID" 
                                PropertyName="Value" Type="Int32" />--%>
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Titulo" Type="String" />
                            <asp:Parameter Name="Copete" Type="String" />
                            <asp:Parameter Name="Fuente" Type="String" />
                            <asp:Parameter Name="Estado" Type="Int32" />
                            <asp:Parameter Name="Destacada" Type="Int32" />
                            <asp:Parameter Name="Texto" Type="String" />
                            <asp:Parameter Name="CategoriaID" Type="Int32" />
                            <asp:Parameter Name="TipoInfoID" Type="Int32" />
                            <asp:Parameter Name="Cluster" Type="Int32" />
                            <asp:Parameter Name="Privacidad" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <h3>
                        Recordatorios</h3>
                    <asp:FormView ID="FVRecordatorios" runat="server" DataKeyNames="InfRecordatorioID"
                        DataSourceID="ODSRecordatorio" DefaultMode="Insert" OnItemInserted="FVRecordatorios_ItemInserted"
                        OnItemInserting="FVRecordatorios_ItemInserting">
                        <InsertItemTemplate>
                            <div class="form-item">
                                <label>
                                    Fecha:</label>
                                <asp:TextBox ID="FHRecordatorioTextBox" runat="server" Text='<%# Bind("FHRecordatorio") %>'
                                    SkinID="form-date" />
                                <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                                (dd/mm/aaaa)
                                <asp:CustomValidator ID="FHRecordatorioCustomValidator" runat="server" ErrorMessage="CustomValidator"
                                    Display="Dynamic" Text="*" ValidationGroup="Recordatorio" ControlToValidate="FHRecordatorioTextBox"
                                    OnServerValidate="FHRecordatorioCustomValidator_ServerValidate"></asp:CustomValidator>
                                <asp:RequiredFieldValidator ValidationGroup="Recordatorio" ID="RequiredFieldValidator3"
                                    Display="Dynamic" runat="server" ControlToValidate="FHRecordatorioTextBox" ErrorMessage="Ingrese un valor en el campo Fecha de Recordatorio">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ValidationGroup="Recordatorio" ID="CompareValidator2" runat="server"
                                    Display="Dynamic" ErrorMessage="CompareValidator" ControlToValidate="FHRecordatorioTextBox"
                                    Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                                <asp:CalendarExtender ID="FHRecordatorioTextBox_CalendarExtender" runat="server"
                                    Enabled="True" PopupButtonID="Image2" Format="dd/MM/yyyy" TargetControlID="FHRecordatorioTextBox"></asp:CalendarExtender>
                            </div>
                            <asp:ValidationSummary ValidationGroup="Recordatorio" ID="ValidationSummary1" runat="server"
                                CssClass="messages msg-error" />
                            <div class="controls">
                                <asp:Button ValidationGroup="Recordatorio" ID="ButtonInsertar" runat="server" CommandName="Insert"
                                    Text="Agregar" />
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:ObjectDataSource ID="ODSRecordatorio" runat="server" InsertMethod="InsertarRecordatorio"
                        OldValuesParameterFormatString="{0}" OnObjectCreating="ODSRecordatorio_ObjectCreating"
                        OnObjectDisposing="ODSRecordatorio_ObjectDisposing" SelectMethod="GetRecordatoriosByInfoID"
                        TypeName="com.paginar.johnson.BL.ControllerNoticias">
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="FHRecordatorio" Type="DateTime" />
                        </InsertParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="GVRecordatorios" runat="server" AutoGenerateColumns="False" DataKeyNames="InfRecordatorioID"
                        DataSourceID="ODSRecordatorios" ShowHeader="False">
                        <Columns>
                            <asp:BoundField DataField="FHRecordatorio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="FHRecordatorio"
                                SortExpression="FHRecordatorio" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("InfRecordatorioID") %>'
                                        CommandName="Delete" Text="Eliminar"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSRecordatorios" runat="server" DeleteMethod="DeleteRecordatorio"
                        OnObjectCreating="ODSRecordatorios_ObjectCreating" OnObjectDisposing="ODSRecordatorios_ObjectDisposing"
                        SelectMethod="GetRecordatoriosByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias">
                        <DeleteParameters>
                            <asp:Parameter Name="InfRecordatorioID" Type="Int32" />
                        </DeleteParameters>
                    </asp:ObjectDataSource>
                    <div class="form-item">
                        <label>
                            Clusters</label>
                        <asp:CheckBoxList ID="ClusterCBL" runat="server" DataSourceID="ODSClusters" DataTextField="Descripcion"
                            DataValueField="ID" ondatabound="ClusterCBL_DataBound">
                        </asp:CheckBoxList>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Debe seleccionar al menos un cluster en el que se visualizara la Noticia"
                            OnServerValidate="CustomValidator1_ServerValidate" Display="Dynamic">*</asp:CustomValidator>
                    </div>
                    <asp:ObjectDataSource ID="ODSClusters" runat="server" OldValuesParameterFormatString="{0}"
                        SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.ClusterTableAdapter">
                    </asp:ObjectDataSource>
                    <div class="form-item">
                        <label>
                            Texto</label>
                        <%--<cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Texto") %>' />--%>
                        <FCKeditorV2:FCKeditor ID="Editor1" runat="server" Value='<%#Bind("Texto") %>' Height="400px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FCKeditorV2:FCKeditor>
<%--                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RequiredFieldValidator9"
                            runat="server" ControlToValidate="Editor1" ErrorMessage="Ingrese un valor en el campo Texto"
                            Display="Dynamic">*</asp:RequiredFieldValidator>--%>
                        <asp:HiddenField ID="TipoInfoIDTextBox" runat="server" Value='<%# Bind("TipoInfoID") %>' />
                    </div>
                    <div class="form-item full">
                        <label>
                            Privacidad</label>
                        <asp:DropDownList ID="PrivacidadDropDownList" runat="server" SelectedValue='<%# Bind("Privacidad") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Si</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="UPDATE" ID="RequiredFieldValidator8"
                            runat="server" ControlToValidate="PrivacidadDropDownList" ErrorMessage="Seleccionar un valor para el campo Privacidad"
                            InitialValue="" Text="*"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controls">
                        <asp:Button ID="UpdateButton" ValidationGroup="UPDATE" runat="server" CausesValidation="True"
                            CommandName="Update" Text="Actualizar" />
                        <asp:Button ID="UpdateCancelButton" CommandName="Cancel" Text="Cancelar" runat="server"
                            CausesValidation="False" OnClick="UpdateCancelButton_Click" />
                        <asp:Button ID="Button1" CausesValidation="false" runat="server" CommandName="Delete"
                            OnClientClick="return confirm('Confirma que desea eliminar este registro?');"
                            Text="Borrar" />
                    </div>
                    
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                <asp:Panel ID="Panelx" runat="server" DefaultButton="InsertButton">
                    <!--Beg Vista Previa -->
                        <div id="boxes" class="preview">

                        <div id="dialog" class="window">
                     
                            <div >
                                <div id="content-inner">


                                    <div id="content-area">

                                        <div class="noticia">

                                            <div id="CPHMain_main_RNoticias" class="AspNet-DataList">
                                                <a href="#" class="close" /><font color="black">Cerrar</font>  </a>

                                                

                               <asp:DataList ID="RNoticias" runat="server" OnItemDataBound="RNoticias_ItemDataBound">
                                   <ItemTemplate>
                                       <ul class="tags">
                                           <li><span id="CategoriaDescrip">
                                               <%# Eval("CategoriaDescrip")%></span></li>
                                           <li runat="server" id="RecordatorioItem" class="recordatorio">
                                               <span>
                                                   <asp:Label ID="RecordatorioLiteral" Text="Recordatorio" runat="server" Visible="false"></asp:Label>
                                                   <a class="linkToolTip" href="#Recordatorios" rel="#Recordatorios">Recordatorio</a>
                                               </span>
                                           </li>
                                       </ul>
                                       <div id="Recordatorios">
                                           <asp:Repeater ID="RepeaterRecordatorios" runat="server">
                                               <HeaderTemplate>
                                                   <table style="border: 1px;">
                                               </HeaderTemplate>
                                               <ItemTemplate>
                                                   <tr>
                                                       <td>
                                                           <asp:Label ID="Label1" runat="server" Text='<%# Bind("FHRecordatorio","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                   </tr>
                                               </ItemTemplate>
                                               <FooterTemplate>
                                                   </table>
                                               </FooterTemplate>
                                           </asp:Repeater>

                                       </div>
                                       <h3>
                                           <asp:HiddenField ID="RecordatorioHiddenField" runat="server" Value='<%# Eval("Recordatorio") %>' />
                                           <%# Eval("Titulo")%>
                                       </h3>
                                       <div class="nota">
                                           <div class="ImgDes" runat="server" id="DivImagen">
                                               <a href="#"  runat="server" ID="lnkImg"  target="_blank"> 
                                                  <asp:Image ID="ImagenNoticia" runat="server" CssClass="ImgDes1" />
                                               </a>
                                               <br />
                                               <b>
                                                   <asp:Label ID="ImagenNoticiaDescripcion" runat="server" Text=""></asp:Label></b>
                                           </div>
                                           <div runat="server" id="divNoFlyer">
                                               <%--<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString() +" "+ Eval("Copete")%>--%>
                                               <i>
                                                   <label>Fecha Publicaci�n:</label>
                                                   <asp:Label Font-Italic="true" runat="server" ID="LabelFechaAsoc" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                                               </i>
                                               <br />
                                               <asp:Label Font-Italic="true" runat="server" ID="LiteralCopete" Text='<%#Eval("Copete")%>'></asp:Label>
                                               <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                                               <asp:Literal ID="LiteralTexto" runat="server" Text='<%# Eval("texto").ToString()%>'></asp:Literal>
                                               <ul class="attachnew">
                                                   <asp:Repeater ID="RepeaterArchivos" runat="server">
                                                       <ItemTemplate>
                                                           <li>
                                                               <%--<img src="../css/images/attach.png" title="Ver Adjunto" border="0" />--%>
                                                               <asp:HyperLink ID="HyperLinkArchivoAdjunto" Text='<%# Eval("Descrip") %>' runat="server"
                                                                   NavigateUrl='<%# "~/noticias/Imagenes/"+Eval("Path") %>' Target="_blank" Visible='<%# Eval("Path").ToString()!="" %>'>  ss</asp:HyperLink>
                                                           </li>
                                                       </ItemTemplate>
                                                   </asp:Repeater>
                                               </ul>
                                           </div>
                                           <div runat="server" id="divFlyer" visible="false">
                                               <i>
                                                   <label>Fecha Publicaci�n:</label>
                                                   <asp:Label Font-Italic="true" runat="server" ID="Label2" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                                               </i>
                                               <br />
                                               <br />
                                               <asp:Label Font-Italic="true" runat="server" ID="Label3" Text='<%#Eval("Copete")%>'></asp:Label>
                                               <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                                               <br />
                                                <a href="#"  runat="server" ID="lnkImgflyer"  target="_blank"> 
                                                   <asp:Image ID="imgFlyer" runat="server" Width="450px" />
                                                </a>
                                               <br />
                                           </div>

                                           <div align="center">
                                            <asp:Literal ID="ltvideo" runat="server"></asp:Literal>

                                          
                                           </div>
                                       </div>
                                       <%# Eval("Fuente")%>
                                       <asp:HiddenField ID="InfoIDHiddenField" runat="server" Value='<%# Eval("InfoID")%>' />
                                       <asp:SlideShowExtender SlideShowServicePath="~/WsNoticias.asmx" ID="ImagenNoticia_SlideShowExtender"
                                           runat="server" SlideShowServiceMethod="GetSlidesDetalles" TargetControlID="ImagenNoticia"
                                           UseContextKey="True" NextButtonID="" PlayButtonID="" PlayButtonText="play" StopButtonText="stop"
                                           PreviousButtonID="" AutoPlay="true" ImageDescriptionLabelID="ImagenNoticiaDescripcion" Loop="True" PlayInterval="3000">
                                       </asp:SlideShowExtender>

                                       <%--Links videos--%>
                                       <br /><br />
                                      <div  id="divlinks"  style="margin-left:20px">

                                           <asp:DataList ID="RTLinks" runat="server" OnItemDataBound="RTLinks_ItemDataBound">
                                               <ItemTemplate>
                                                   <br />
                                                   <asp:Literal ID="ltvideo" runat="server"></asp:Literal>
                                                   <asp:HyperLink Target="_blank" ID="HyperLink1" NavigateUrl='<%# Eval("url")%>' Text='<%# Eval("descrip")%>' runat="server" />

                                               </ItemTemplate>
                                           </asp:DataList>
                                       </div>

                                   </ItemTemplate>
                               </asp:DataList>

                                                <%--<table cellspacing="0" cellpadding="-1" summary="">
									<tbody>
										<tr>
											<td class="AspNet-DataList-Item">
										<ul class="tags">
											<li style="-moz-border-radius: 10px 10px 10px 10px;"><span>
											<asp:Label ID="lblCategoria" runat="server"></asp:Label></span></li>
											
										</ul>
										<h3>
                                            <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
										</h3>
										<div class="nota">
                                        <div runat="server" id="divNoFlyer">  
											<div align="center">
                                                  <asp:Image ID="ImagenNoticia" runat="server" />

                                                <!--~/noticias/Imagenes/images634323488463226989.jpg-->
                                            </div>
											
											<i>
                                                <asp:Label ID="lblFecha" runat="server" Text=""></asp:Label></span>
											</i>-<span style="font-style: italic;" id="CPHMain_main_RNoticias_LiteralCopete_0"><asp:Label
                                                ID="lblCopete" runat="server" Text=""></asp:Label></span>
											
											<p>
                                                <asp:Label ID="lblCuerpo" runat="server" Text=""></asp:Label>
                                            </p>
											<ul class="attachnew">
												
                                                <asp:Label ID="lblAttach" runat="server" Text=""></asp:Label>
											</ul>
                                         </div>
                                         <div runat="server" id="divFlyer" visible="false">
                                             <i>
                                                <asp:Label ID="Label2" runat="server" Text=""></asp:Label></span>
											</i><span style="font-style: italic;" id="Span1"><asp:Label
                                                ID="lblCopeteFlyer" runat="server" Text=""></asp:Label></span>
                                                <asp:Image ID="imgFlyer" runat="server" Width="450px"  Height="450px"/>
                                                <br />
                                         </div>
										</div>
										<asp:Label ID="lblFuente" runat="server" Text=""></asp:Label>
										
										
									
											</td>
										</tr>
									</tbody>
								</table>--%>
                                            </div>
                                            <br>


                                            <br>
                                            <br>
                                        </div>


                                    </div>
                                </div>
                            </div>

                           <div id="sidebar-right">

                           </div>
                                     
                        </div>



                        <!-- Mask to cover the whole screen -->
                          <div id="mask"></div>
                        </div>
                    <!--End Vista Previa -->
                    <asp:ValidationSummary ValidationGroup="INSERT" ID="ValidationSummary2" runat="server"
                        CssClass="messages msg-error " />
                    <div class="form-item leftHalf">
                        <label>
                            Estado</label>
                        <asp:DropDownList ID="EstadoDropDownList" runat="server" SelectedValue='<%# Bind("Estado") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Activo</asp:ListItem>
                            <asp:ListItem Value="2">Inactivo</asp:ListItem>
                            <asp:ListItem Value="3">Archivado</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RangeValidator1" runat="server"
                            ControlToValidate="EstadoDropDownList" ErrorMessage="Seleccionar un valor para el campo Estado"
                            InitialValue="" Text="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Destacada</label>
                        <asp:DropDownList ID="DestacadosDropDownList" runat="server" SelectedValue='<%# Bind("Destacada") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Destacada</asp:ListItem>
                            <asp:ListItem Value="2">General</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator6"
                            runat="server" ControlToValidate="DestacadosDropDownList" ErrorMessage="Seleccionar un valor para el campo Destacado"
                            InitialValue="" Text="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Categor�a</label>
                        <asp:DropDownList ID="CategoriaDropDownList" runat="server" DataSourceID="ODSCstegorias"
                            DataTextField="Descrip" DataValueField="CategoriaID" SelectedValue='<%# Bind("CategoriaID") %>'>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator7"
                            runat="server" ControlToValidate="CategoriaDropDownList" ErrorMessage="Seleccionar un valor para el campo Categoria"
                            InitialValue="" Text="*" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:ObjectDataSource ID="ODSCstegorias" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                            OldValuesParameterFormatString="{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter"
                            UpdateMethod="Update"></asp:ObjectDataSource>

                        <br /><br />
                        <label>
                            Tags</label>
                <asp:Repeater runat="server" ID="rptTags" DataSourceID="odsTags">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdTagId"  Value='<%# Eval("TagId")  %>' />
                        <asp:CheckBox  runat="server" ID="chkTag" />
                        <asp:Label runat="server" ID="lblDesripcion" Text='<%# Eval("Descripcion") %>' ></asp:Label>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
                <br />
                <asp:ObjectDataSource ID="odsTags" runat="server" SelectMethod="getTags" TypeName="com.paginar.johnson.BL.ControllerNoticias" DeleteMethod="BannerDelete" InsertMethod="BannerClusterAdd" OldValuesParameterFormatString="original_{0}">
                </asp:ObjectDataSource>
                <br />
                <br />
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            T�tulo</label>
                        <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' MaxLength="255" />
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="Ingrese un valor en el campo Titulo" ControlToValidate="TituloTextBox"
                            Display="Dynamic">*</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-item leftHalf">
                        <label>
                            Fecha de Publicaci�n</label>
                        <asp:TextBox ID="FechaAsocTextBox" runat="server" Text='<%# Bind("FechaAsoc", "{0:dd/MM/yyyy}") %>'
                            SkinID="form-date"></asp:TextBox>
                        <asp:ImageButton ID="FechaPublicImageButton" runat="server" ImageUrl="~/images/Calendar.png" />
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator3"
                            runat="server" ControlToValidate="FechaAsocTextBox" ErrorMessage="Ingrese un valor en el campo Fecha de Publicaci�n"
                            Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ValidationGroup="INSERT" ID="CompareValidator2" runat="server"
                            ControlToValidate="FechaAsocTextBox" ErrorMessage="El formato de la fecha ingresada en Fecha de Publicaci�n es incorrecto. El formato de fecha es dd/mm/yyyy."
                            Operator="DataTypeCheck" Type="Date" Display="Dynamic">*</asp:CompareValidator>
                        <asp:CustomValidator ValidationGroup="INSERT" ID="CustomValidator2" runat="server"
                            ControlToValidate="FechaAsocTextBox" ErrorMessage="Las fechas de recordatorio deben ser mayor a la fecha de publicaci�n"
                            OnServerValidate="CustomValidator2_ServerValidate" Display="Dynamic">*</asp:CustomValidator>
                        <asp:CalendarExtender ID="TextBox2_CalendarExtender" runat="server" Enabled="True"
                            Format="dd/MM/yyyy" PopupButtonID="FechaPublicImageButton" TargetControlID="FechaAsocTextBox"></asp:CalendarExtender>
                        <asp:CustomValidator ValidationGroup="INSERT" ID="CustomValidator3" runat="server"
                            ControlToValidate="FechaAsocTextBox" Display="Dynamic" ErrorMessage="La fecha de publicaci�n debe ser mayor o igual a la fecha actual."
                            OnServerValidate="CustomValidator3_ServerValidate1" Enabled="False">*</asp:CustomValidator>
                    </div>
                    <div class="form-item rightHalf">
                        <label>
                            Copete</label>
                        <asp:TextBox ID="CopeteTextBox" runat="server" Text='<%# Bind("Copete") %>' MaxLength="1000" />
                    </div>
                    <h3>
                        Links</h3>

                    <div class="messages msg-info">
                        
                            <asp:Label Font-Size="Smaller"   Font-Bold="true" runat="server" ID="Label6" Text="Puede ingresar links a videos de Youtube o Vimeo en el campos URL"></asp:Label>
                            <br />
                           
                        </div>	

                    <asp:FormView ID="FVLink" runat="server" DataSourceID="ODSLinkNoticiasInsert" DefaultMode="Insert"
                        OnItemInserted="FVLink_ItemInserted" OnItemUpdated="FVLink_ItemUpdated">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenFieldInfoID" runat="server" Value='<%# Bind("InfoID") %>' />
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("LinkID") %>' />
                            <div class="form-item">
                                <label>
                                    Descripci�n</label>
                                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DescripTextBox"  Enabled="False"
                                    ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Links"
                                    Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="form-item">
                                <label>
                                    URL</label>
                                <asp:TextBox ID="URLTextBox" runat="server" Text='<%# Bind("URL") %>' MaxLength="255" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="URLTextBox"
                                    ErrorMessage="Ingrese un valor en el campo URL" ValidationGroup="Links" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                            <asp:LinkButton ID="UpdateButton" ValidationGroup="Links" runat="server" CausesValidation="True"
                                CommandName="Update" Text="Modificar" />
                            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                                CommandName="Cancel" Text="Cancelar" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <div class="form-item leftHalf">
                                <label>
                                    Descripci�n</label>
                                <asp:TextBox ID="DescripTextBox" runat="server" Text='<%# Bind("Descrip") %>' 
                                    MaxLength="255" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DescripTextBox" Enabled="false"
                                    ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Links"
                                    Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="form-item rightHalf">
                                <label>
                                    URL</label>
                                <asp:TextBox ID="URLTextBox" runat="server" Text='<%# Bind("URL") %>' 
                                    MaxLength="255" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="URLTextBox"
                                    ErrorMessage="Ingrese un valor en el campo URL" ValidationGroup="Links" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                            <div class="controls">
                                <asp:LinkButton ID="InsertButton" ValidationGroup="Links" runat="server" CausesValidation="True" 
                                    CommandName="Insert" Text="Agregar" />
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:ObjectDataSource ID="ODSLinkNoticiasInsert" runat="server" DeleteMethod="DeleteLink"
                        InsertMethod="InsertLink" OldValuesParameterFormatString="{0}" SelectMethod="GetLinkByLinkID"
                        TypeName="com.paginar.johnson.BL.ControllerNoticias" UpdateMethod="UpdateLink"
                        OnObjectCreating="ODSLinkNoticiasInsert_ObjectCreating" OnObjectDisposing="ODSLinkNoticiasInsert_ObjectDisposing">
                        <DeleteParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="URL" Type="String" />
                        </InsertParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="GVLinks" Name="LinkID" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="URL" Type="String" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="GVLinks" runat="server" AutoGenerateColumns="False" DataKeyNames="LinkID"
                        DataSourceID="ODSLinksNoticiasInsert" ShowHeader="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Descrip" SortExpression="Descrip">
                                <ItemTemplate>

                                   <%-- <asp:LinkButton ID="LinkButton2" runat="server" CommandArgument='<%# Eval("LinkID") %>'
                                        CommandName="Select" Text='<%# Eval("Descrip") %>'></asp:LinkButton>--%>
                                   
                                    <asp:HyperLink
                                            ID="HyperLink2" runat="server" NavigateUrl='<%# Eval("URL") %>' Target="_blank" 
                                            Text='<%# Eval("Descrip") %>'></asp:HyperLink>

                                   <asp:Image ID="Image4" ImageUrl="~/images/videoicon.png" runat="server" Width="16px" Height="16px" Visible='<%# Eval("URL").ToString().Contains("youtube")||Eval("URL").ToString().Contains("youtu.be")||Eval("URL").ToString().Contains("vimeo") ? true: false %>' />
                            
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--      <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" />--%>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png"
                                        AlternateText="Borrar" CommandArgument='<%# Eval("LinkID") %>' CommandName="Delete"
                                        CausesValidation="false" />--%>
                                    <asp:LinkButton ID="LinkButton3" runat="server"  CommandArgument='<%# Eval("LinkID") %>' CommandName="Delete">Eliminar</asp:LinkButton>
                                        </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSLinksNoticiasInsert" runat="server" OldValuesParameterFormatString="{0}"
                        SelectMethod="GetLinkByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                        UpdateMethod="UpdateInfo" DeleteMethod="DeleteLink" OnObjectCreating="ODSLinksNoticiasInsert_ObjectCreating"
                        OnObjectDisposing="ODSLinksNoticiasInsert_ObjectDisposing">
                        <SelectParameters>
                            <%--
                            <asp:ControlParameter ControlID="InfoIDLabel1" Name="InfoID" 
                                PropertyName="Value" Type="Int32" />--%>
                        </SelectParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="LinkID" Type="Int32" />
                        </DeleteParameters>
                    </asp:ObjectDataSource>
                    <div class="form-item full">
                        <label>
                            Fuente</label>
                        <asp:TextBox ID="FuenteTextBox" runat="server" Text='<%# Bind("Fuente") %>' MaxLength="255"/>
                    </div>
                    <h3>
                        Adjuntos</h3>			
                       <div class="messages msg-info">
                            <asp:Label runat="server" ID="Label9" Font-Size="Smaller"   Font-Bold="true" Text="Si el adjunto es una foto, para que �sta se visualice, debe ser un archivo de extensi�n .jpg o .png" ></asp:Label>
                            <br />
                            <asp:Label Font-Size="Smaller"  Font-Bold="true" runat="server" ID="infoImg" Text="Si el adjunto No es una Imagen, el campo Descripci�n es obligatorio"></asp:Label>
                            <br />
                            <asp:Label  Font-Size="Smaller"   Font-Bold="true" runat="server" ID="infoImg2" Text="Si el adjunto es una Imagen, el valor que ingrese en el campo descripci�n debe ser un hiperv�nculo"></asp:Label>
                            <br />
                           <asp:Label  Font-Size="Smaller" Font-Bold="true" runat="server" ID="Label8" Text="Si el adjunto es un video, el formato debe ser .mp4"></asp:Label>
                            
                            <br />
                        </div>
	
                    <div class="form-item leftHalf">
                        <label>
                            Descripci�n</label>
                        <asp:TextBox ID="DescripTextBox" runat="server" MaxLength="249" />
                        

                        <label>
                            Flyer</label>

              
                            
                        <asp:CheckBox runat="server" ID="chkFlyer" />

                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DescripTextBox" 
                            ErrorMessage="Ingrese un valor en el campo Descripci�n" ValidationGroup="Imagenes"
                            Display="Dynamic">*</asp:RequiredFieldValidator>--%>

<%--                    <asp:Panel ID="divErrorImg" runat="server" class="messages msg-error" Visible="false">
                        <asp:Label runat="server" ID="lblErrorImg" Text="Si el adjunto no es una imagen, la descripci�n es requer�da."></asp:Label>
                    </asp:Panel>
--%>
                    </div>
                   
                 
                    <div class="form-item rightHalf">
  

                        <label>
                            Ruta</label>
                        <asp:FileUpload ID="FUImagenes" runat="server" />                        
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator5" runat="server"
                            ControlToValidate="FUImagenes" ErrorMessage="Ingrese imagen a agregar" ValidationGroup="Imagenes">*</asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Imagenes"
                            ControlToValidate="FUImagenes" runat="Server" ErrorMessage="*" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG)$" />--%><br />
                        <%--<asp:Label Text="Solo se permiten archivos JPG" Font-Size="Smaller" runat="server"
                            ID="LabelMensaje"></asp:Label>--%>
                          <asp:Label Text="No puede ingresar otra imagen" Font-Size="Smaller" runat="server" Visible="false"
                            ID="LabelMensajeFlyer"></asp:Label>
                    </div>



                    <div class="controls">
                         
                        <asp:Button ID="BtnAgregarImagenesINS" runat="server" OnClick="BtnAgregarImagenesINS_Click"  OnClientClick="skm_LockScreen('Subiendo Archivo...'); return true;" 
                            Text="Agregar" ValidationGroup="Imagenes" />
                    </div>
                    <asp:CustomValidator ID="CustomValidator4" runat="server" ErrorMessage="Falt� completar el campo Descripci�n, debe adjuntar el archivo nuevamente."
                        CssClass="messages msg-error" ControlToValidate="DescripTextBox" ValidationGroup="Imagenes"
                        ValidateEmptyText="true" OnServerValidate="CustomValidator4_ServerValidate"></asp:CustomValidator>



                    <asp:GridView ID="GVImagen" runat="server" AutoGenerateColumns="False" DataKeyNames="ImagenID"
                        DataSourceID="ODSImagenesNoticiasInsert" ShowHeader="False">
                        <Columns>
                            <asp:TemplateField HeaderText="Descrip" SortExpression="Descrip">
                                <ItemTemplate>
                                    <asp:Label Text='<%# Eval("Descrip") %>' ID="HyperLink1" runat="server"></asp:Label>
                                        <asp:Image ID="imgvideo" ImageUrl="~/images/videoicon.png" runat="server" Width="16px" Height="16px" Visible='<%# Eval("path").ToString().Contains(".mp4")? true: false %>' />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Flyer" >
                                <ItemTemplate>
                                       <asp:CheckBox runat="server" ID="chkFlyer"  Text="Flyer" Enabled="false"   Checked='<%# Convert.ToBoolean(Eval("Flyer")) %>'/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/delete.png"
                                        AlternateText="BORRAR" CommandArgument='<%# Eval("ImagenID") %>' CommandName="Delete"
                                        CausesValidation="false" />--%>
                                    <asp:LinkButton ID="ImageButton2" CommandArgument='<%# Eval("ImagenID") %>' CommandName="Delete" runat="server">Eliminar</asp:LinkButton>   
                                        </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSImagenesNoticiasInsert" runat="server" DeleteMethod="DeleteImagen"
                        InsertMethod="InsertImagen" OldValuesParameterFormatString="{0}" SelectMethod="GetImagenByInfoID"
                        TypeName="com.paginar.johnson.BL.ControllerNoticias" UpdateMethod="UpdateInfo"
                        OnObjectCreating="ODSImagenesNoticiasInsert_ObjectCreating" OnObjectDisposing="ODSImagenesNoticiasInsert_ObjectDisposing">
                        <DeleteParameters>
                            <asp:Parameter Name="ImagenID" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Descrip" Type="String" />
                            <asp:Parameter Name="Path" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="Titulo" Type="String" />
                            <asp:Parameter Name="Copete" Type="String" />
                            <asp:Parameter Name="Fuente" Type="String" />
                            <asp:Parameter Name="Estado" Type="Int32" />
                            <asp:Parameter Name="Destacada" Type="Int32" />
                            <asp:Parameter Name="Texto" Type="String" />
                            <asp:Parameter Name="CategoriaID" Type="Int32" />
                            <asp:Parameter Name="TipoInfoID" Type="Int32" />
                            <asp:Parameter Name="Cluster" Type="Int32" />
                            <asp:Parameter Name="Privacidad" Type="Int32" />
                            <asp:Parameter Name="FHMod" Type="DateTime" />
                            <asp:Parameter Name="UsrMod" Type="Int32" />
                        </UpdateParameters>
                    </asp:ObjectDataSource>
                    <h3>
                        Recordatorios</h3>
                    <asp:FormView ID="FVRecordatoriosINS" runat="server" DataKeyNames="InfRecordatorioID"
                        DataSourceID="ODSRecordatorioINS" DefaultMode="Insert" OnItemInserted="FVRecordatoriosINS_ItemInserted">
                        <InsertItemTemplate>
                            <div class="form-item">
                                <label>
                                    Fecha:</label>
                                <asp:TextBox ID="FHRecordatorioTextBox" runat="server" Text='<%# Bind("FHRecordatorio") %>'
                                    SkinID="form-date" />
                                <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                                (dd/mm/aaaa)
                                <asp:CustomValidator ID="FHRecordatorioCustomValidatorINS" runat="server" ErrorMessage="CustomValidator"
                                    Display="Dynamic" Text="*" ValidationGroup="Recordatorio" ControlToValidate="FHRecordatorioTextBox"
                                    OnServerValidate="FHRecordatorioCustomValidatorINS_ServerValidate"></asp:CustomValidator>
                                <asp:RequiredFieldValidator ValidationGroup="Recordatorio" ID="RequiredFieldValidator3"
                                    Display="Dynamic" runat="server" ControlToValidate="FHRecordatorioTextBox" ErrorMessage="Ingrese un valor en el campo Fecha de Recordatorio">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ValidationGroup="Recordatorio" ID="CompareValidator2" runat="server"
                                    Display="Dynamic" ErrorMessage="CompareValidator" ControlToValidate="FHRecordatorioTextBox"
                                    Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                                <asp:CalendarExtender ID="FHRecordatorioTextBox_CalendarExtender" runat="server"
                                    Enabled="True" PopupButtonID="Image2" Format="dd/MM/yyyy" TargetControlID="FHRecordatorioTextBox"></asp:CalendarExtender>
                            </div>
                            <asp:ValidationSummary ValidationGroup="Recordatorio" ID="ValidationSummary1" runat="server" />
                            <div class="controls">
                                <asp:Button ValidationGroup="Recordatorio" ID="ButtonInsertar" runat="server" CommandName="Insert"
                                    Text="Agregar" />
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                    <asp:ObjectDataSource ID="ODSRecordatorioINS" runat="server" InsertMethod="InsertarRecordatorio"
                        OnObjectCreating="ODSRecordatorioINS_ObjectCreating" OnObjectDisposing="ODSRecordatorioINS_ObjectDisposing"
                        SelectMethod="GetRecordatoriosByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias">
                        <InsertParameters>
                            <asp:Parameter Name="InfoID" Type="Int32" />
                            <asp:Parameter Name="FHRecordatorio" Type="DateTime" />
                        </InsertParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="GVRecordatoriosINS" runat="server" AutoGenerateColumns="False"
                        DataKeyNames="InfRecordatorioID" DataSourceID="ODSRecordatoriosINS" ShowHeader="False">
                        <Columns>
                            <asp:BoundField DataField="FHRecordatorio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="FHRecordatorio"
                                SortExpression="FHRecordatorio" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("InfRecordatorioID") %>'
                                        CommandName="Delete" Text="Eliminar"></asp:LinkButton></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSRecordatoriosINS" runat="server" DeleteMethod="DeleteRecordatorio"
                        OnObjectCreating="ODSRecordatoriosINS_ObjectCreating" OnObjectDisposing="ODSRecordatoriosINS_ObjectDisposing"
                        SelectMethod="GetRecordatoriosByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias">
                        <DeleteParameters>
                            <asp:Parameter Name="InfRecordatorioID" Type="Int32" />
                        </DeleteParameters>
                    </asp:ObjectDataSource>
                    <div class="form-item">
                        <label>
                            Clusters</label>
                        <asp:CheckBoxList ID="ClusterCBL" runat="server" DataSourceID="ODSClusters" DataTextField="Descripcion"
                            DataValueField="ID" ondatabound="ClusterCBL_DataBound">
                        </asp:CheckBoxList>
                        <asp:CustomValidator ValidationGroup="INSERT" ID="CustomValidator1" runat="server"
                            ErrorMessage="Debe seleccionar al menos un cluster en el que se visualizara la Noticia"
                            OnServerValidate="CustomValidator1_ServerValidate1" Display="Dynamic">*</asp:CustomValidator>
                        <asp:ObjectDataSource ID="ODSClusters" runat="server" OldValuesParameterFormatString="{0}"
                            SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.ClusterTableAdapter"
                            DeleteMethod="Delete" UpdateMethod="Update">
                            <DeleteParameters>
                                <asp:Parameter Name="Original_ID" Type="Int32" />
                                <asp:Parameter Name="Original_Descripcion" Type="String" />
                                <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                <asp:Parameter Name="Original_PathImage" Type="String" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Descripcion" Type="String" />
                                <asp:Parameter Name="Abreviatura" Type="String" />
                                <asp:Parameter Name="PathImage" Type="String" />
                                <asp:Parameter Name="Original_ID" Type="Int32" />
                                <asp:Parameter Name="Original_Descripcion" Type="String" />
                                <asp:Parameter Name="Original_Abreviatura" Type="String" />
                                <asp:Parameter Name="Original_PathImage" Type="String" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>
                    </div>
                    <div class="form-item">
                        <label>
                            Texto</label>
                        <%--<cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Texto") %>' />--%>
                        <FCKeditorV2:FCKeditor ID="Editor1" runat="server" Value='<%#Bind("Texto") %>' Height="400px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FCKeditorV2:FCKeditor>
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator9"
                            runat="server" ControlToValidate="Editor1" ErrorMessage="Ingrese un valor en el campo Texto"
                            Display="Dynamic" InitialValue=".">*</asp:RequiredFieldValidator>
                        <asp:HiddenField ID="TipoInfoIDTextBox" runat="server" Value='<%# Bind("TipoInfoID") %>' />
                    </div>
                    <div class="form-item full">
                        <label>
                            Privacidad</label>
                        <asp:DropDownList ID="PrivacidadDropDownList" runat="server" SelectedValue='<%# Bind("Privacidad") %>'>
                            <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                            <asp:ListItem Value="1">Si</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="INSERT" ID="RequiredFieldValidator8"
                            runat="server" ControlToValidate="PrivacidadDropDownList" ErrorMessage="Seleccionar un valor para el campo Privacidad"
                            InitialValue="" Text="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <div class="controls">
                        <asp:Button ID="InsertButton" ValidationGroup="INSERT" runat="server" CausesValidation="True" Enabled="false" ToolTip="Realice una Vista Previa antes de guardar"
                            CommandName="Insert" Text="Insertar" />
                        <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                            Text="Cancelar" OnClick="InsertCancelButton_Click" />
                        <asp:Button ID="PreviewButton" runat="server" Text="Vista Previa" 
                            onclick="PreviewButton_Click" />

                    </div>
                    
                    </asp:Panel>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ODSNoticia" runat="server" SelectMethod="GetInfInfoByInfoID"
                TypeName="com.paginar.johnson.BL.ControllerNoticias" UpdateMethod="UpdateInfo"
                OnObjectCreating="ODSNoticia_ObjectCreating" OnObjectDisposing="ODSNoticia_ObjectDisposing"
                DeleteMethod="DeleteInfo" InsertMethod="InsertInfo" OnSelecting="ODSNoticia_Selecting">


                <DeleteParameters>
                    <asp:Parameter Name="InfoID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Copete" Type="String" />
                    <asp:Parameter Name="Fuente" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="Destacada" Type="Int32" />
                    <asp:Parameter Name="Texto" Type="String" />
                    <asp:Parameter Name="CategoriaID" Type="Int32" />
                    <asp:Parameter Name="TipoInfoID" Type="Int32" />
                    <asp:Parameter Name="Privacidad" Type="Int32" />
                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                    <asp:Parameter Name="FechaAsoc" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="GVNoticias" Name="InfoID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="InfoID" Type="Int32" />
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Copete" Type="String" />
                    <asp:Parameter Name="Fuente" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="Destacada" Type="Int32" />
                    <asp:Parameter Name="Texto" Type="String" />
                    <asp:Parameter Name="CategoriaID" Type="Int32" />
                    <asp:Parameter Name="TipoInfoID" Type="Int32" />
                    <asp:Parameter Name="Privacidad" Type="Int32" />
                    <asp:Parameter Name="FHMod" Type="DateTime" />
                    <asp:Parameter Name="UsrMod" Type="Int32" />
                    <asp:Parameter Name="FechaAsoc" Type="DateTime" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="VComentarios" runat="server">
            <br />
            <asp:LinkButton ID="LinkButton7" runat="server" CommandArgument="VBuc" CommandName="SwitchViewByID">Volver</asp:LinkButton>
            <br />
            <br />
            <asp:Label ID="TituloLabel" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
            <br />
            <asp:GridView ID="GVComentarios" runat="server" AutoGenerateColumns="False" DataKeyNames="comentarioID"
                DataSourceID="ODSComentarios" OnRowCommand="GVComentarios_RowCommand" ShowHeader="False">
                <Columns>
                    <asp:TemplateField HeaderText="usuariofoto" SortExpression="usuariofoto">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("usuariofoto") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image2" runat="server" Height="50px" ImageUrl='<%# "~/images/personas/"+Eval("usuariofoto") %>'
                                Width="50px" /></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="nombrecompleto" SortExpression="nombrecompleto">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("nombrecompleto") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("nombrecompleto") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha" SortExpression="Fecha">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("Fecha") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Fecha","{0:dd/MM/yyyy}") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="comentarios" SortExpression="comentarios">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("comentarios") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("comentarios") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument='<%# Eval("comentarioID") %>'
                                CommandName="Select">Editar</asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton6" runat="server" CommandArgument='<%# Eval("comentarioID") %>'
                                CausesValidation="false" CommandName="Delete" OnClientClick="return confirm(&quot;Est� seguro de eliminar este registro?&quot;);">Eliminar</asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="messages msg-info">
                        El registro seleccionado no contiene comentarios asociados.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:HiddenField ID="InfoIDHiddenField" runat="server" />
            <asp:ObjectDataSource ID="ODSComentarios" runat="server" DeleteMethod="DeleteComentario"
                SelectMethod="getComments" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                UpdateMethod="UpdateComment" OnObjectCreating="ODSComentarios_ObjectCreating"
                OnObjectDisposing="ODSComentarios_ObjectDisposing" InsertMethod="InsertInfo">

                <DeleteParameters>
                    <asp:Parameter Name="comentarioID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Copete" Type="String" />
                    <asp:Parameter Name="Fuente" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="Destacada" Type="Int32" />
                    <asp:Parameter Name="Texto" Type="String" />
                    <asp:Parameter Name="CategoriaID" Type="Int32" />
                    <asp:Parameter Name="TipoInfoID" Type="Int32" />
                    <asp:Parameter Name="Cluster" Type="Int32" />
                    <asp:Parameter Name="Privacidad" Type="Int32" />
                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                    <asp:Parameter Name="FechaAsoc" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="InfoIDHiddenField" Name="infoID" PropertyName="Value"
                        Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="comentarioID" Type="Int32" />
                    <asp:Parameter Name="comentarios" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:FormView ID="FVComentario" runat="server" DataSourceID="ODSComment" DefaultMode="Edit"
                OnItemUpdated="FVComentario_ItemUpdated">
                <EditItemTemplate>
                <asp:Panel ID="Panel3" runat="server" DefaultButton="Button4">
                    <asp:HiddenField ID="comentarioIDTextBox" runat="server" Value='<%# Bind("comentarioID") %>' />
                    <asp:Label ID="nombrecompletoTextBox" runat="server" Text='<%# Eval("nombrecompleto") %>' />
                    <br />
                    Fecha:
                    <asp:Label ID="FechaTextBox" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy}") %>' />
                    <br />
                    comentarios:
                    <asp:TextBox ID="comentariosTextBox" runat="server" Text='<%# Bind("comentarios") %>'
                        TextMode="MultiLine" />
                    <br />
                    &nbsp;<br />
                    <asp:Button ID="Button4" runat="server" CommandName="Update" Text="Actualizar" />
                    <asp:Button ID="CancelarBtn" runat="server" OnClick="CancelarBtn_Click" Text="Cancelar" />
                    
                    </asp:Panel>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:Panel ID="Panel4" runat="server" DefaultButton="InsertButton">
                    
                    comentarioID:
                    <asp:TextBox ID="comentarioIDTextBox" runat="server" Text='<%# Bind("comentarioID") %>' />
                    <br />
                    InfoID:
                    <asp:TextBox ID="InfoIDTextBox" runat="server" Text='<%# Bind("InfoID") %>' />
                    <br />
                    usuarioID:
                    <asp:TextBox ID="usuarioIDTextBox" runat="server" Text='<%# Bind("usuarioID") %>' />
                    <br />
                    comentarios:
                    <asp:TextBox ID="comentariosTextBox" runat="server" Text='<%# Bind("comentarios") %>' />
                    <br />
                    usuariofoto:
                    <asp:TextBox ID="usuariofotoTextBox" runat="server" Text='<%# Bind("usuariofoto") %>' />
                    <br />
                    Fecha:
                    <asp:TextBox ID="FechaTextBox" runat="server" Text='<%# Bind("Fecha") %>' />
                    <br />
                    nombrecompleto:
                    <asp:TextBox ID="nombrecompletoTextBox" runat="server" Text='<%# Bind("nombrecompleto") %>' />
                    <br />
                    infInfoRow:
                    <asp:TextBox ID="infInfoRowTextBox" runat="server" Text='<%# Bind("infInfoRow") %>' />
                    <br />
                    RowError:
                    <asp:TextBox ID="RowErrorTextBox" runat="server" Text='<%# Bind("RowError") %>' />
                    <br />
                    RowState:
                    <asp:TextBox ID="RowStateTextBox" runat="server" Text='<%# Bind("RowState") %>' />
                    <br />
                    Table:
                    <asp:TextBox ID="TableTextBox" runat="server" Text='<%# Bind("Table") %>' />
                    <br />
                    ItemArray:
                    <asp:TextBox ID="ItemArrayTextBox" runat="server" Text='<%# Bind("ItemArray") %>' />
                    <br />
                    HasErrors:
                    <asp:CheckBox ID="HasErrorsCheckBox" runat="server" Checked='<%# Bind("HasErrors") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                        Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                        CommandName="Cancel" Text="Cancel" />
                </asp:Panel>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ODSComment" runat="server"
                SelectMethod="getCommentsBycomentarioID" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                UpdateMethod="UpdateComment" OnObjectCreating="ODSComment_ObjectCreating" OnObjectDisposing="ODSComment_ObjectDisposing"
                DeleteMethod="DeleteComentario" InsertMethod="InsertInfo">

                <DeleteParameters>
                    <asp:Parameter Name="comentarioID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Copete" Type="String" />
                    <asp:Parameter Name="Fuente" Type="String" />
                    <asp:Parameter Name="Estado" Type="Int32" />
                    <asp:Parameter Name="Destacada" Type="Int32" />
                    <asp:Parameter Name="Texto" Type="String" />
                    <asp:Parameter Name="CategoriaID" Type="Int32" />
                    <asp:Parameter Name="TipoInfoID" Type="Int32" />
                    <asp:Parameter Name="Cluster" Type="Int32" />
                    <asp:Parameter Name="Privacidad" Type="Int32" />
                    <asp:Parameter Name="FHAlta" Type="DateTime" />
                    <asp:Parameter Name="UsrAlta" Type="Int32" />
                    <asp:Parameter Name="FechaAsoc" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="GVComentarios" Name="comentarioID" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="comentarioID" Type="Int32" />
                    <asp:Parameter Name="comentarios" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
        </asp:View>
        <asp:View ID="VResultado" runat="server" OnActivate="VResultado_Activate">
            <span lang="ES-TRAD" class="messages msg-exito">La operaci�n fue realizada exitosamente</span>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="5000" OnTick="Timer1_Tick">
            </asp:Timer>
        </asp:View>
        <asp:View ID="VErrorAtras" runat="server">
        <span lang="ES-TRAD" class="messages msg-error">Error al retroceder, el registro ya fue guardado, intente editar la noticia a traves de grilla de busqueda</span>
        </asp:View>
    </asp:MultiView>
    </asp:Panel>
    </ContentTemplate>
    </asp:TabPanel>
    <asp:TabPanel runat="server" TabIndex="1" ID="TabNotReporte">
    <HeaderTemplate>
    Reporte Noticias
    </HeaderTemplate>
    <ContentTemplate>
        <asp:Panel ID="PanelReporteNoticias" runat="server" DefaultButton="btnFlitroNotiReporte">
        
        <asp:MultiView ID="MVReporteNoticias" runat="server">
        <asp:View ID="VReporteNoticias" runat="server">

        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Buscar" runat="server" />
                <div class="box filtros">
                    <asp:RadioButtonList ID="RBLFiltroNotiReporte" runat="server" 
                        RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" 
                        onselectedindexchanged="RBLFiltroNotiReporte_SelectedIndexChanged">
                        <asp:ListItem Value="" Selected="True">TODOS</asp:ListItem>
                        <asp:ListItem Value="HOY">HOY</asp:ListItem>
                        <asp:ListItem Value="RANGO">RANGO</asp:ListItem>
                    </asp:RadioButtonList>
                <asp:TextBox ID="TextBox1" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="TextBox1"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="TextBox1" SetFocusOnError="True">*</asp:RangeValidator>
                <asp:CalendarExtender TargetControlID="TextBox1" ID="CalendarExtender1" 
                        runat="server" PopupButtonID="btnc" Enabled="True" Format="dd/MM/yyyy">
                </asp:CalendarExtender><asp:ImageButton ID="btnc" runat="server" ImageUrl="~/images/Calendar.png">
                    </asp:ImageButton>
                    -
                <asp:TextBox ID="TextBox2" runat="server" Columns="10" SkinID="form-date"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="TextBox2"
                    ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToCompare="TextBox2"
                    ControlToValidate="TextBox1" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                    Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="TextBox2" SetFocusOnError="True">*</asp:RangeValidator>

                <asp:CalendarExtender TargetControlID="TextBox2" PopupButtonID="btnhasta" 
                        ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:ImageButton ID="btnhasta" runat="server" ImageUrl="~/images/Calendar.png" />

                    <asp:Button ID="btnFlitroNotiReporte" runat="server" 
                        onclick="btnFlitroNotiReporte_Click" Text="Buscar" 
                        ValidationGroup="Buscar" />
                    <asp:Button ID="Export" runat="server" Text="Exportar" onclick="Export_Click" /><br /><br />
                    <asp:GridView ID="GVReporteNoticia" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="InfoID" 
                        DataSourceID="ODSReporteNoticias" PageSize="8" 
                        onrowcommand="GVReporteNoticia_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblIDReporte" runat="server" Text='<%# Eval("InfoID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TITULO">
                                <ItemTemplate>
                                    <asp:Label ID="lblTituloReporteNotica" runat="server" 
                                        Text='<%# Eval("Titulo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CategoriaDescrip" HeaderText="CATEGORIA" 
                                ReadOnly="True" SortExpression="CategoriaDescrip" />
                            <asp:BoundField DataField="FHAlta" HeaderText="FECHA DE ALTA" 
                                SortExpression="FHAlta" />
                            <asp:BoundField DataField="Votos" HeaderText="ME GUSTA" ReadOnly="True" 
                                SortExpression="Votos" />
                            <asp:BoundField DataField="VotosNeg" HeaderText="NO ME GUSTA" ReadOnly="True" 
                                SortExpression="VotosNeg" />
                            <asp:TemplateField HeaderText="PRIVACIDAD">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrivacidad" runat="server" Text='<%# ((Eval("Privacidad").ToString() == "0" )? "No" : "Si") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COMENTARIOS">
                            <ItemTemplate>
                            <asp:LinkButton ID="LinkButtonComentarioReporte" runat="server" CommandArgument='<%# ((GridViewRow)Container).RowIndex %>'
                                CommandName="Comentarios">Comentarios</asp:LinkButton></ItemTemplate>

                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="ODSReporteNoticias" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="getInfoByReporte" 
                        TypeName="com.paginar.johnson.BL.ControllerNoticias">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="TextBox1" Name="desde" PropertyName="Text" 
                                Type="DateTime" />
                            <asp:ControlParameter ControlID="TextBox2" Name="hasta" PropertyName="Text" 
                                Type="DateTime" />
                            <asp:ControlParameter ControlID="RBLFiltroNotiReporte" Name="modo" 
                                PropertyName="SelectedValue" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                </asp:View>
                <asp:View ID="VComentarioReporte" runat="server" >

            <br />
            <asp:LinkButton ID="lbAtrasReport" runat="server" CommandArgument="VBuc" 
                        onclick="lbAtrasReport_Click">Volver</asp:LinkButton>
            <br />
            <br />
            <asp:Label ID="lblTituComentReport" runat="server"></asp:Label>
            <br />
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="comentarioID"
                DataSourceID="ODSComentarioRepNoticia" OnRowCommand="GVComentarios_RowCommand" 
                        ShowHeader="False" AllowPaging="True" PageSize="8">
                <Columns>
                    <asp:TemplateField HeaderText="usuariofoto" SortExpression="usuariofoto">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("usuariofoto") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image2" runat="server" Height="50px" ImageUrl='<%# "~/images/personas/"+Eval("usuariofoto") %>'
                                Width="50px" /></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="nombrecompleto" SortExpression="nombrecompleto">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("nombrecompleto") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("nombrecompleto") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fecha" SortExpression="Fecha">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("Fecha") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("Fecha","{0:dd/MM/yyyy}") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="comentarios" SortExpression="comentarios">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("comentarios") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("comentarios") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument='<%# Eval("comentarioID") %>'
                                CommandName="Select">Editar</asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton6" runat="server" CommandArgument='<%# Eval("comentarioID") %>'
                                CausesValidation="false" CommandName="Delete" OnClientClick="return confirm(&quot;Est� seguro de eliminar este registro?&quot;);">Eliminar</asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="messages msg-info">
                        El registro seleccionado no contiene comentarios asociados.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:HiddenField ID="HFRepComent" runat="server" />
            <asp:ObjectDataSource ID="ODSComentarioRepNoticia" runat="server" DeleteMethod="BannerDelete"
                SelectMethod="getComments" TypeName="com.paginar.johnson.BL.ControllerNoticias"
                UpdateMethod="BannerUpdate" OnObjectCreating="ODSComentarios_ObjectCreating"
                OnObjectDisposing="ODSComentarios_ObjectDisposing" InsertMethod="BannerClusterAdd" 
                        OldValuesParameterFormatString="original_{0}">
                <DeleteParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                    <asp:Parameter Name="clusterid" Type="Int32" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFRepComent" Name="infoID" PropertyName="Value"
                        Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                    <asp:Parameter Name="titulo" Type="String" />
                    <asp:Parameter Name="cuerpo" Type="String" />
                    <asp:Parameter Name="imagen" Type="String" />
                    <asp:Parameter Name="fhalta" Type="DateTime" />
                    <asp:Parameter Name="habilitado" Type="Boolean" />
                    <asp:Parameter Name="original_bannerid" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>
            <br />
            <br />

                </asp:View>
        </asp:MultiView>
        </asp:Panel>
    </ContentTemplate>
    </asp:TabPanel>
</asp:TabContainer>

</asp:Content>
