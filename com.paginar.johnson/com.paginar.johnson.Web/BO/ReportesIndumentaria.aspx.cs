﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

namespace com.paginar.johnson.Web.BO
{
    public partial class ReportesIndumentaria : System.Web.UI.Page
    {
        ControllerIndumentaria CI = new ControllerIndumentaria();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblTituloReporte.Text = DDTipodeReporte.SelectedItem.ToString();
            if (!Page.IsPostBack)
            {
                DropDownList1_DataBound(null, null);
                PSoliConDetalle.Visible = true;
                PSoliSinDetalle.Visible = false;
                PSoliSinDetalleporArea.Visible = false;
                btnExportar.Visible = false;
                imgPrint.Visible = false;
                
            }
        }

        protected void DropDownList1_DataBound(object sender, EventArgs e)
        {
            foreach (DSIndumentaria.Indumentaria_PeriodoRow item in ((DSIndumentaria.Indumentaria_PeriodoDataTable)CI.SeleccionarPeriodosTodos()).Rows)
	        {
                DDLPeriodo.Items.Add(new ListItem(procesarPeriodo(item.Desde, item.Hasta), item.PeriodoId.ToString())); 
	        } 
            
            
        }

        private string procesarPeriodo(DateTime Desde, DateTime Hasta)
        {
            string fecha = String.Format("{0:dd}-{0:MM}-{0:yyyy}", Desde, Desde, Desde);
            fecha = "Desde  " + fecha + " hasta " + String.Format("{0:dd}-{0:MM}-{0:yyyy}", Hasta, Hasta, Hasta);
            return fecha;

        }

        protected string descomponerTerna(object valor, int poscicionDeRetorno)
        {
            if (valor.ToString() == "")
                return "";
            string[] retornar = valor.ToString().Split('|');
            return retornar[poscicionDeRetorno];
        }

        protected string esBrigadista(object valor)
        {
            if (valor.ToString() == "0")
                return "Todas";
            if (valor.ToString() == "True" || valor.ToString() == "1")
                return "Sí";
            else
                return "No";
            
        }

        protected void DDTipodeReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (DDTipodeReporte.SelectedValue.ToString())
            {
                case "1":
                    PSoliConDetalle.Visible = true;
                    PSoliSinDetalle.Visible = false;
                    PSoliSinDetalleporArea.Visible = false;
                    DDLAreas.Enabled = true;                    
                    GVPanel1.Visible = false;
                    
                    break;
                case "2":
                    PSoliConDetalle.Visible = false;
                    PSoliSinDetalle.Visible = false;
                    PSoliSinDetalleporArea.Visible = true;
                    DDLAreas.Enabled = true;
                    GVPanel2.Visible = false;

                    break;
                case "3":
                    PSoliConDetalle.Visible = false;
                    PSoliSinDetalle.Visible = true;
                    PSoliSinDetalleporArea.Visible = false;
                    DDLAreas.Enabled = false;
                    GVPanel3.Visible = false;

                    break;
            }
            ltlImprimir.Text = "";
            hlImprimir.NavigateUrl = "";
            hlImprimir.Visible = false;
            btnExportar.Visible = false;
            imgPrint.Visible = false;

        }

        protected string cadenaArea(string valor)
        {
            string retorno;
            if (valor == "Todas")
                retorno = "";
            else
                retorno = "&Area=" + valor;
            return retorno;
        }

        protected string cadenaBrigadista(string valor)
        {
            string retorno;
            if (valor == "Todos")
                retorno = "";
            else
                retorno = "&esBrigadista=" + valor;
            return retorno;
        }


        protected void Filtrar_Click(object sender, EventArgs e)
        {
            ltlImprimir.Text = "<a href='ImprimirReporteIndumentaria.aspx?TipoReporte=" + DDTipodeReporte.SelectedValue.ToString() + "&PeriodoID=" + DDLPeriodo.SelectedValue.ToString() + cadenaArea( DDLAreas.SelectedItem.ToString()) + cadenaBrigadista(DDLBrigadista.SelectedValue ) + "' target='_blank'>Imprimir</a>";
            hlImprimir.NavigateUrl = "ImprimirReporteIndumentaria.aspx?TipoReporte=" + DDTipodeReporte.SelectedValue.ToString() + "&PeriodoID=" + DDLPeriodo.SelectedValue.ToString() + cadenaArea( DDLAreas.SelectedItem.ToString()) + cadenaBrigadista(DDLBrigadista.SelectedValue );
            hlImprimir.Visible = true;
            btnExportar.Visible = true;
            imgPrint.Visible = true;
            switch (DDTipodeReporte.SelectedValue.ToString())
            {
                case "1":
                    
                    GVPanel1.Visible = true;
                    GVPanel1.DataSourceID = "ODSPanel1";
                    GVPanel1.DataBind();
                    //GVPanel1.DataSourceID = null;
                    break;
                case "2":
                    GVPanel2.Visible = true;
                    GVPanel2.DataSourceID = "ODSPanel2";
                    GVPanel2.DataBind();
                    //GVPanel2.DataSourceID = null;
                    break;
                case "3":
                    GVPanel3.Visible = true;
                    GVPanel3.DataSourceID = "ODSPanel3";
                    GVPanel3.DataBind();
                    //GVPanel3.DataSourceID = null;
                    break;
            }

        }

        private void ExportAExcel(string nombreDelReporte, GridView wControl)
        {

            
            wControl.AllowPaging = false;

            wControl.HeaderStyle.BackColor.Equals(Color.Red);
            wControl.HeaderStyle.ForeColor.Equals(Color.Red);
            wControl.HeaderStyle.BorderColor.Equals(Color.Red);

            

            wControl.DataBind();


            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            Literal litFechaActual = new Literal();
            litFechaActual.ID = "litFchs";
            litFechaActual.Text = "<br /><strong>Fecha de Generación:</strong> " + DateTime.Now.ToShortDateString() + "<br />";           
            Label LF1 = new Label();
            LF1.ID = "LBF1";
            Literal litBrigadista = new Literal();
            litBrigadista.Text = "<br /><strong>Condición Brigadista:</strong> " + esBrigadista(DDLBrigadista.SelectedIndex);
            Literal litArea = new Literal();

            if (DDLPeriodo.SelectedItem.ToString() != "Seleccione")
            {
                LF1.Text = "<strong>Período:</strong> " + DDLPeriodo.SelectedItem.ToString();
            }
            else
            {
                LF1.Text = "<strong>Período:</strong> Todos";
            }

            if (DDLAreas.SelectedItem.ToString() == "")
            {
                litArea.Text = "<br /><strong>Áreas:</strong> Todas<br /><br />";
            }
            else
            {
                litArea.Text = "<br /><strong>Áreas:</strong> " + DDLAreas.SelectedItem.ToString() + "<br /><br />";
            }
            
            litFechaActual.RenderControl(htw);
            LF1.RenderControl(htw);
            litBrigadista.RenderControl(htw);
            litArea.RenderControl(htw);

            form.Controls.Add(litFechaActual);
            form.Controls.Add(LF1);
            form.Controls.Add(litBrigadista);
            form.Controls.Add(litArea);
            //form.Controls.Add();
            form.Controls.Add(wControl);

           pageToRender.Controls.Add(new LiteralControl(
                            @"<html>
                              <head>
<style>                                    

body 
{
    text-decoration: none;
	line-height: 1.3;
    color: #000000 !Important;
    font-family: Arial;
}
a {
    color:#000000;
}

h1 {
    background-color:#CFCFCF;
    layer-background-color:#CFCFCF;
}

h2 {
    background-color:#CFCFCF;
    color: #558EC6;
    margin-bottom: 20px;
}
h2 {
    color:#000000;
    font-size:45px;
}
h3 {
    color:#000000;
    margin-bottom: 10px;    
    clear:both;
}
h4 {
    color:#000000;
    font-size:13px;/*antes 14px*/
    margin-bottom: 10px;
}
p 
{
    color:#000000;
    margin-bottom: 15px;
}

.AspNet-GridView 
{
    text-decoration: none;
    margin-bottom:10px;
    clear:both;
}
.AspNet-GridView table{
    width:100%;
}
.AspNet-GridView table th,
.AspNet-GridView table td 
{
    text-decoration: none;
    padding:5px;
    border:1px solid #D9D9D9;
    color: #000000 !important;
    font-size:12px;
}

.AspNet-GridView th
{
    text-decoration: none;
    background-color:#E6E6E6;
    color:#000000;
    text-transform:uppercase;
    font-weight:normal;
}

</style>
</head><body>"));

           pageToRender.Controls.Add(new LiteralControl(
               @"<div id='header'><div><h1>" + lblTituloReporte.Text + "</h1></div></div>"));
                
            pageToRender.Controls.Add(form);
                                              
           pageToRender.Controls.Add(new LiteralControl(@"
                                  
                              </body>
                              </html>"));

            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=" + nombreDelReporte + " " + DateTime.Now.ToShortDateString() + ".xls");
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);
            
            response.Write(sw.ToString());
        
            response.End();
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            switch (DDTipodeReporte.SelectedValue.ToString())
            {
                case "1":
                    ExportAExcel(DDTipodeReporte.SelectedItem.ToString(), GVPanel1);
                    break;
                case "2":
                    ExportAExcel(DDTipodeReporte.SelectedItem.ToString(), GVPanel2);
                    break;
                case "3":
                    ExportAExcel(DDTipodeReporte.SelectedItem.ToString(), GVPanel3);
                    break;
            }
        }

        protected void DDLAreas_DataBound(object sender, EventArgs e)
        {
            //((DropDownList)sender).Items.Clear();
            //((DropDownList)sender).DataBind();
        }

    }
}