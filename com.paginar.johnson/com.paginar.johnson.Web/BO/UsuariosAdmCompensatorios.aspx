﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="UsuariosAdmCompensatorios.aspx.cs" Inherits="com.paginar.johnson.Web.BO.UsuariosAdmCompensatorios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Usuarios con Compensatorios</h2>
    <asp:Label ID="AvisoCluster" runat="server" Text="Sección no habilitada" 
        Visible="False"></asp:Label>
    <asp:Panel ID="pnlContenido" runat="server">			
<div id="wrapperUsuariosActivos" class="newFormStyle">
 <div class="form-item leftHalf">
                <label>
                    Area</label>
                 <asp:DropDownList ID="DDLArea" runat="server" DataSourceID="ODSAreas" 
                    DataTextField="AreaDESC" DataValueField="AreaDESC" AppendDataBoundItems="True">
                    <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                 </asp:DropDownList>
                <asp:ObjectDataSource ID="ODSAreas" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.AreasTableAdapter">
                </asp:ObjectDataSource>
            </div>
            <div class="form-item rightHalf">
     
            </div>

            <div class="controls form-end">
                <asp:Button ID="BuscarButton" runat="server"  Text="Buscar" 
                    onclick="BuscarButton_Click" />
                <asp:Button ID="ButtonDescarga" runat="server" 
                    Text="Descargar Reporte" onclick="ButtonDescarga_Click" />
            </div>  
        <div style="overflow:auto">
            <asp:GridView ID="GVUsuariosCompensatorios" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ODSUsuariosCompensatorios" AllowPaging="True">
                <Columns>
                    <asp:BoundField DataField="nombrecompleto" HeaderText="Apellido y Nombre" 
                        ReadOnly="True" SortExpression="nombrecompleto" />
                    <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                        SortExpression="Area" />
                    <asp:BoundField DataField="TramiteID" HeaderText="Id Tramite" 
                        SortExpression="TramiteID" />
                    <asp:BoundField DataField="FeriadoTrabajado" HeaderText="Feriado Trabajado" 
                        ReadOnly="True" SortExpression="FeriadoTrabajado" />
                    <asp:TemplateField HeaderText="Trabajo en Remoto" 
                        SortExpression="TrabajoRemoto">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# retornaSiNo(Eval("TrabajoRemoto")) %>' ></asp:Label>
                            <%--<asp:CheckBox ID="CheckBox1" runat="server" 
                                Checked='<%# Eval("TrabajoRemoto") %>' Enabled="false" />--%>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asistencia" SortExpression="Asistencia">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" 
                                Checked='<%# Bind("Asistencia") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# retornaSiNo(Eval("Asistencia")) %>' ></asp:Label>
                            <%--<asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Eval("Asistencia") %>' 
                                Enabled="false" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha Solicitud" 
                        ReadOnly="True" SortExpression="FechaSolicitud" />
                    <asp:BoundField DataField="DiaSolicitado" HeaderText="Día Solicitado" 
                        ReadOnly="True" SortExpression="DiaSolicitado" />
                    <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" 
                        SortExpression="Estado" />
                    <asp:BoundField DataField="ObservacionesJefe" HeaderText="Observaciones Jefe" 
                        ReadOnly="True" SortExpression="ObservacionesJefe" />
                    <asp:BoundField DataField="ObservacionesRRHH" HeaderText="Observaciones RRHH" 
                        ReadOnly="True" SortExpression="ObservacionesRRHH" />
                    <asp:BoundField DataField="FechaAprobacion" HeaderText="Fecha Aprobación" 
                        ReadOnly="True" SortExpression="FechaAprobacion" />
                </Columns>
                <EmptyDataTemplate>
                    En estos momentos no hay datos para mostrar.
                </EmptyDataTemplate>
            </asp:GridView>

            <asp:ObjectDataSource ID="ODSUsuariosCompensatorios" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByUsuariosConCompensatoriosPorArea" 
                TypeName="com.paginar.johnson.BL.ControllerUsuarios">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLArea" Name="Area" 
                        PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </div>
        </asp:Panel>
</asp:Content>
