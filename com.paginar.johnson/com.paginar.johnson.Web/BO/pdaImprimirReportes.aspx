﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pdaImprimirReportes.aspx.cs" Inherits="com.paginar.johnson.Web.BO.pdaImprimirReportes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Workshop</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
    <style type="text/css">
        #FormImprimirWKTodos tr td:first-child
        {
            width: 15% !Important;    
        }
    </style>
</head>
<body>
    <form id="formImprPda" runat="server">
    <asp:HiddenField ID="HFCursoid" runat="server" />
    <asp:HiddenField ID="HFDireccionId" runat="server" />
    <asp:HiddenField ID="HFAreaid" runat="server" />
        <asp:HiddenField ID="HFAnio" runat="server" />
    <asp:HiddenField ID="HFEstado" runat="server"  Value=""/>

        <div id="wrapper">
            

            <div id="header">
                <div class="bg">
                    
        <asp:ObjectDataSource ID="ODScursos" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="pda_cursoSelectByIdReporte" 
            TypeName="com.paginar.johnson.BL.ControllerPDA">
            <SelectParameters>
                <asp:ControlParameter ControlID="HFCursoid" Name="idCurso" 
                    PropertyName="Value" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        

                </div>
                <h1>Capacitación PDA</h1>
                <img id="logo" src="../images/logoprint.gif" />
            </div>
     
        <div id="noprint">

             <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />
              <br /><br />
        </div>
        
<asp:MultiView ID="MVReportesPDA" runat="server">    
        <asp:View ID="VInscPorCurso" runat="server">
            <div style="border-style:solid; border-color:#CCCCCC; border-width:1px">
        
            <asp:Repeater ID="RptInscPorCursos" runat="server" DataSourceID="ODScursos" 
                onitemdatabound="RptInscPorCursos_ItemDataBound">
                <HeaderTemplate>
                    <asp:Literal ID="ltrlFecha" runat="server"></asp:Literal>
                    <br />
                <div style="border-style:solid; border-color:#CCCCCC; border-width:1px">
                <h3>Solicitudes de inscripción por Cursos</h3>
                </HeaderTemplate>
            <ItemTemplate>
                <asp:HiddenField ID="HFidCurso" runat="server" Value='<%# Eval("idCurso") %>' />
                <div class="AspNet-GridView wrappreppda">
                <table width="100%">
                <thead>
                <tr class="AspNet-GridView-Header">
                <td colspan="6"><b><asp:Label ID="lblNombreCurso" runat="server" Text='<%# Eval("Nombre") %>' ></asp:Label></b>
                </td>
                </tr>
                </thead>
                 </table>
                 
            <asp:GridView ID="GVInscPorCurso" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="Usuarioid" DataSourceID="ODSReporteInsPorCurso">
                <Columns>
                    <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                        Visible="False" />
                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                        SortExpression="Legajo" />
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                        SortExpression="Apellido" />
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                        SortExpression="Nombre" />
                    <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Inscripcion" 
                        SortExpression="FechaInscripcion" />
                    <asp:BoundField DataField="Direccion" HeaderText="Dirección" ReadOnly="True" 
                        SortExpression="Direccion" />
                    <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                        SortExpression="Area" />
                </Columns>
            </asp:GridView>
            <table>
            <tbody>
            <tr>
            <td colspan="6" align="right">
            <span style="float:right"><b>Subtotal:&nbsp;&nbsp;</b><asp:Label ID="lblCantInscCurso" runat="server" Text='<%# cantidadInscriptos(Eval("idCurso")) %>' ></asp:Label></span>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            <asp:ObjectDataSource ID="ODSReporteInsPorCurso" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="getReporteInscripcionPorCurso" 
                TypeName="com.paginar.johnson.BL.ControllerPDA">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFidCurso" Name="idCurso" 
                        PropertyName="Value" Type="Int32" />
                        
                    <asp:ControlParameter ControlID="HFEstado" Name="idEstado" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HFAnio" Name="anio" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            </ItemTemplate>
            <FooterTemplate>
            </div>
            </FooterTemplate>
            </asp:Repeater>
            
             
            
                <div class="AspNet-GridView" style="width:100%">
                <asp:Label ID="lblTotalInscriptos" runat="server" Text=""></asp:Label></span>
                 </div>
                  </div> 
        </asp:View>
        <asp:View ID="VInscAreaDir" runat="server">
        <div style="border-style:solid; border-color:#CCCCCC; border-width:1px">
 
            <asp:Repeater ID="RptAreaDirecion" runat="server" 
                DataSourceID="ODSReporteAreaDireccion" 
                onitemdatabound="RptAreaDirecion_ItemDataBound">
                <HeaderTemplate>
                <asp:Literal ID="ltrlFecha" runat="server"></asp:Literal>
                <br />
                <div style="border-style:solid; border-color:#CCCCCC; border-width:1px">
                <h3>Solicitudes de inscripción por Dirección / Área</h3>
                </HeaderTemplate>
            <ItemTemplate>                
            <asp:HiddenField ID="HFdirID" runat="server" Value='<%# Eval("direccionid") %>'/>
            <asp:HiddenField ID="HFAreID" runat="server" Value='<%# Eval("areaid") %>'/>
            
                <div class="AspNet-GridView wrappreppda">
                <table width="100%">
                    
                <thead>
                <tr class="AspNet-GridView-Header">
                <td colspan="2" style="width:50%"><b><asp:Label ID="lblDireccion" runat="server" Text='<%# Eval("Direccion") %>' ></asp:Label></b>
                <td colspan="3" style="width:50%"><b><asp:Label ID="lblarea" runat="server" Text='<%# Eval("Area") %>' ></asp:Label></b>
                </td>
                </tr>
                </thead>
                 </table>
                 <tr>
                 <td colspan="5">
       <asp:GridView ID="GVReporter" runat="server" AutoGenerateColumns="False" 
           DataKeyNames="Usuarioid" DataSourceID="ODSReporte">
           <Columns>
               <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                   InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                   Visible="False" />
               <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                   SortExpression="Legajo" >
               <ControlStyle Width="15%" />
               <FooterStyle Width="15%" />
               <HeaderStyle Width="15%" />
               <ItemStyle Width="15%" />
               </asp:BoundField>
               <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                   SortExpression="Apellido" />
               <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                   SortExpression="Nombre" />
               <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Inscripcion" 
                   SortExpression="FechaInscripcion" />
               <asp:BoundField DataField="curso" HeaderText="Curso" ReadOnly="True" 
                   SortExpression="curso" />
           </Columns>
       </asp:GridView>

                 </td>
                 </tr>
            <table>
            <tbody>
            <tr>
            <td colspan="5" align="right">
            <span style="float:right"><b>Subtotal:&nbsp;&nbsp;</b><asp:Label ID="Label1" runat="server" Text='<%# procesarCantidadReporte2(Eval("cantidad")) %>'></asp:Label></span>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
       <asp:ObjectDataSource ID="ODSReporte" runat="server" 
           OldValuesParameterFormatString="original_{0}" 
           SelectMethod="getDetalleReportePorAreaDireccionCurso" 
           TypeName="com.paginar.johnson.BL.ControllerPDA">
           <SelectParameters>
               <asp:ControlParameter ControlID="HFAreID" Name="AreaID" PropertyName="Value" 
                   Type="Int32" />
               <asp:ControlParameter ControlID="HFdirID" Name="DireccionID" 
                   PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="HFCursoid" Name="idCurso" PropertyName="Value" 
                   Type="Int32" />
           </SelectParameters>
       </asp:ObjectDataSource> 
          </ItemTemplate>
            <FooterTemplate>
            </div>
            </FooterTemplate>
            </asp:Repeater>
            <div class="AspNet-GridView" style="width:100%">
            <span style="float:right;width:100%"><asp:Label ID="Label2" runat="server" Text="Total:" ></asp:Label>
            &nbsp;
            <asp:Label ID="lblTotalReporte2" runat="server" Text=""></asp:Label></span>
            </div>
            </div>
            <asp:ObjectDataSource ID="ODSReporteAreaDireccion" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="getReporteAreaDireccion" 
                TypeName="com.paginar.johnson.BL.ControllerPDA">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFAreaid" Name="AreaID" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HFDireccionId" Name="DireccionID" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HFCursoid" Name="idCurso" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HFAnio" Name="anio" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </asp:View>

    </asp:MultiView>

        </div>

    </form>
</body>
</html>

