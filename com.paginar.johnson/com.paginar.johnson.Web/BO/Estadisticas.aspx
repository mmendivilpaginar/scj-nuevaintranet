﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="Estadisticas.aspx.cs" Inherits="com.paginar.johnson.Web.BO.Estadisticas" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register assembly="Microsoft.Web.DynamicData" namespace="Microsoft.Web.DynamicData" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <script type="text/javascript">

        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

    function openuser(url) 
    {
        var newwindow;
        newwindow = window.open(url, 'name', 'height=470,width=520,scrollbars=1');
        if (window.focus) { newwindow.focus() }
    }


    $(document).ready(function() {
        $("#CPMAIN_hlGoogleAnalytics").attr("title", "User: intranetscj@gmail.com \n\r Pass: SCJ2011Intra");
        $("#CPMAIN_hlGoogleAnalytics").attr("style", "float: Right");

        $('#hc_switch').click(function () {
            $('#hc_torta').toggle();
            $('#hc_pie').toggle();
        });

        $('#hm_switch').click(function () {
            $('#pai_h_m').toggle();
            $('#barras_h_m').toggle();
        });
    });
</script>
    <h2>Información Estadística</h2>
    <asp:Label ID="AvisoCluster" runat="server" Text="Sección no habilitada" 
        Visible="False"></asp:Label>
    <asp:Panel ID="pnlContenido" runat="server">	
    <div style="clear:both;">
        <asp:HyperLink ID="hlGoogleAnalytics" runat="server" 
            NavigateUrl="http://www.google.com/intl/es/analytics/" Target="_blank" >Google Analytics</asp:HyperLink>
    </div>

    <div style="clear:both;">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="Buscar" />
    <asp:Panel ID="PanelControles" runat="server" DefaultButton="Buscar">
    
    <asp:Label ID="LBLMensajes" runat="server" Text=""></asp:Label>
        <div class="form-item leftHalf">
        <label>
        Indicadores</label>
            <asp:DropDownList ID="DDLIndicadores" runat="server" AutoPostBack="True" 
                onselectedindexchanged="DDLIndicadores_SelectedIndexChanged">
                <asp:ListItem Value="Banner_mas_visitado">Clics sobre banners</asp:ListItem> 
                <asp:ListItem Value="hombres_mujeres">Cantidad de hombres y mujeres</asp:ListItem>
                <asp:ListItem Value="grup_etario">Grupo etario</asp:ListItem>
                <asp:ListItem Value="hora_mayor_concurrencia">Horario de mayor concurrencia</asp:ListItem>
                <asp:ListItem Value="noticia_mas_visitada">Noticia mas visitada</asp:ListItem>                
                <asp:ListItem Value="secc_mas_consult">Sección más consultada</asp:ListItem>
                <asp:ListItem Selected="True" Value="user_access_site">Usuarios que accedieron al sitio</asp:ListItem>                

            </asp:DropDownList>
            <br />
            <br />
        </div>
        <div class="form-item rightHalf">
        <label>Tipo de consulta</label>
            <asp:DropDownList ID="DDLTipo" runat="server" 
                onselectedindexchanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem>Reporte</asp:ListItem>
                <asp:ListItem Value="Grafica">Gráfica</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            <div class="form-item leftHalf">
            <label>Unidad de Consulta</label>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                    RepeatDirection="Horizontal" AutoPostBack="True" 
                    onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="Dias">Diario</asp:ListItem>
                    <asp:ListItem>Semanal</asp:ListItem>
                    <asp:ListItem>Mensual</asp:ListItem>
                    <asp:ListItem>Anual</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div>
            <div class="form-item leftHalf" ID="Dias1" runat="server">
                <label>
                    Desde</label>
                <asp:TextBox ID="DesdeTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="DesdeTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="DesdeTextBox" PopupButtonID="Image1"></asp:CalendarExtender>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="DesdeTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ErrorMessage="El campo Desde no puede estar vacío" Text="*" 
                    ControlToValidate="DesdeTextBox" ValidationGroup="Buscar"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="DesdeTextBox" SetFocusOnError="True">*</asp:RangeValidator>
                (dd/mm/aaaa)
            </div>

            <div class="form-item rightHalf" ID="Dias2" runat="server">
                <label>
                    Hasta</label>
                <asp:TextBox ID="HastaTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="HastaTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                    PopupButtonID="Image2" TargetControlID="HastaTextBox"></asp:CalendarExtender>
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="HastaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="El campo Hasta no puede estar vacío" ValidationGroup="Buscar" 
                    ControlToValidate="HastaTextBox">*</asp:RequiredFieldValidator>
                
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToCompare="HastaTextBox"
                    ControlToValidate="DesdeTextBox" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                    Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="HastaTextBox" SetFocusOnError="True">*</asp:RangeValidator>                
                <asp:CustomValidator ID="CustomValidaDia" runat="server" ErrorMessage="" 
                    ValidationGroup="Buscar" onservervalidate="CustomValidaDia_ServerValidate">*</asp:CustomValidator> 


                (dd/mm/aaaa)
            </div>
            
        </div>

        <div class="controls">
        <asp:Button runat="server" Text="Buscar" ID="Buscar" onclick="Buscar_Click" 
                ValidationGroup="Buscar"/>&nbsp;&nbsp;<%--<asp:Button 
                ID="pdf" runat="server" Text="Exportar a pdf" onclick="pdf_Click" />--%><asp:Button runat="server" Text="Limpiar" ID="ButtonLimpiar" onclick="ButtonLimpiar_Click" 
                />                
                <asp:ImageButton ID="imagenPrint" runat="server" 
                ImageUrl="~/images/printer.png" ToolTip="Imprimir Estadisticas" 
                Visible="False" />
                <asp:Label ID="lblimprimir" runat="server" Visible="False"></asp:Label>
            <asp:Button ID="btnExportar" runat="server" onclick="btnExportar_Click" 
                Text="Exportar" Visible="False" />
        </div>
        </asp:Panel>
    
    </div>

    <asp:MultiView ID="MVEstadisticas" runat="server">
        <asp:View ID="user_access_site" runat="server">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ODSuser_access_site" AllowPaging="True" 
                ondatabound="GridView1_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted" >
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="LBLUsrTotSub" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CANTIDAD" SortExpression="Cantidad">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="LBLTotalUser_access" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LBLCantUuser_access" runat="server" 
                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="BTNVTG1" runat="server" onclick="BTNVTG1_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSuser_access_site" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetEstadisticas_user_access_site" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        
        <asp:View ID="secc_mas_consult" runat="server">
            <asp:GridView ID="GridView2" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSSecc_mas_consultada" 
                ondatabound="GridView2_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" 
                        SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalSecc_mas_visitada" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pagina" SortExpression="Pagina">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>'></asp:Label>
                            <asp:Label ID="Link" runat="server" Text='<%# Bind("Titulo") %>' 
                                Visible="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                        <FooterTemplate>
                            <asp:Label ID="lblTotalGenSecMasCons" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="BTNVTGV2" runat="server" onclick="BTNVTGV2_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSSecc_mas_consultada" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_seccion_mas_consultada" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

        <asp:View ID="banner_mas_consultado" runat="server">
            <asp:GridView ID="GridView7" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSbanner_mas_consultado" 
                ondatabound="GridView7_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" 
                        SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalSecc_mas_visitada" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pagina" SortExpression="Pagina">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>' Visible="false"></asp:Label>
                            <asp:LinkButton ID="lnkPagina" runat="server" Text='<%# Bind("titulo") %>'   PostBackUrl='<%# Bind("Pagina") %>' />
                            <asp:Label ID="Link" runat="server" Text='<%# Bind("Titulo") %>' 
                                Visible="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cantidad" SortExpression="Cantidad">
                        <FooterTemplate>
                            <asp:Label ID="lblTotalGenSecMasCons" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCantUnitariaSecMasCons" runat="server" 
                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="Button1" runat="server" onclick="BTNVTGV2_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSbanner_mas_consultado" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_banner_mas_consultado" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

        <asp:View ID="grup_etario" runat="server">
            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                DataSourceID="ODSGrupoEtario" ShowFooter="True" AllowPaging="True" 
                ondatabound="GridView3_DataBound" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" 
                        SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotSubTotGrupoEtario" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RANGO" SortExpression="rango">
                        <EditItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("rango") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblRango" runat="server" Text='<%# Bind("rango") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CANTIDAD" SortExpression="Cantidad">
                        <EditItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalGrupoEtario" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCantUnitariaGrupoEtario" runat="server" 
                                Text='<%# Bind("Cantidad") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="BTNVTGE" runat="server" onclick="BTNVTGE_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSGrupoEtario" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="Get_grupos_etarios" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

        <asp:View ID="hombres_mujeres" runat="server">
            <asp:GridView ID="GridView4" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSHombres_Mujeres" 
                ondatabound="GridView4_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotSubTotetiq" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TOT. HOMBRES" SortExpression="MASCULINO">
                        <EditItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("MASCULINO") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblhombrestotales" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblhombresunidades" runat="server" 
                                Text='<%# Bind("MASCULINO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TOT. MUJERES" SortExpression="FEMENINO">
                        <EditItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("FEMENINO") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblmujerestotales" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblmujeresunidades" runat="server" 
                                Text='<%# Bind("FEMENINO") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TOTAL PERIODO" SortExpression="TOTALPARCIAL">
                        <EditItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("TOTALPARCIAL") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalesTHM" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblhombresymujeresunidad" runat="server" 
                                Text='<%# Bind("TOTALPARCIAL") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                 <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="BTNGVHM" runat="server" onclick="BTNGVHM_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSHombres_Mujeres" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Hombres_Mujeres" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

        <asp:View ID="hora_mayor_concurrencia" runat="server">
            <asp:GridView ID="GridView5" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSHorarioConcurrencia" 
                ondatabound="GridView5_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
                <Columns>
                    <asp:TemplateField HeaderText="PERIODO" 
                        SortExpression="UnidadPeriodica">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTitTotMayConc" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RANGO" SortExpression="rango">
                        <EditItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("rango") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblrango" runat="server" Text='<%# Bind("rango") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CANTIDAD" SortExpression="Cantidad">
                        <EditItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotGralHorConc" runat="server"></asp:Label>
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTotUnitHorConc" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="BTNVTMC" runat="server" onclick="BTNVTMC_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSHorarioConcurrencia" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_horarios_concurrencia" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

        <asp:View ID="noticia_mas_visitada" runat="server">
        <asp:GridView ID="GridView6" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSTopNoticias" 
                ondatabound="GridView6_DataBound" ShowFooter="True" AllowSorting="True" 
                onprerender="GridView1_PreRender" onsorted="GridView1_Sorted">
            <Columns>
                <asp:TemplateField HeaderText="PERIODO" SortExpression="UnidadPeriodica">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("UnidadPeriodica") %>'></asp:Label>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTitTotNot" runat="server"></asp:Label>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("UnidadPeriodica") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PAGINA" SortExpression="Pagina">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Pagina") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Pagina") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CANTIDAD" SortExpression="Cantidad">
                    <EditItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Cantidad") %>'></asp:Label>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotGralTopNot" runat="server"></asp:Label>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblTotUnitTopNot" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                 <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
        </asp:GridView>
            <asp:Button ID="BTNGVNOTICIAS" runat="server" onclick="BTNGVNOTICIAS_Click" 
                Text="Ver Todos" Visible="False" />
            <asp:ObjectDataSource ID="ODSTopNoticias" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="Get_top_noticias" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>

    </asp:MultiView>
    <asp:MultiView ID="MVGrafica" runat="server">
        <asp:View ID="Guser_access_site" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart1" runat="server" DataSourceID="ODSGuser_access" 
                Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="UnidadPeriodica" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidad">
                        </AxisY>
                        <AxisX Title="Linea de Tiempo">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
            <asp:ObjectDataSource ID="ODSGuser_access" runat="server" 
                InsertMethod="InsertRegistroEstadistico" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_user_access_site_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <InsertParameters>
                    <asp:Parameter Name="UsuarioID" Type="Int32" />
                    <asp:Parameter Name="paginaVisitada" Type="String" />
                    <asp:Parameter Name="titulo" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="Gsecc_mas_consult" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart2" runat="server" DataSourceID="ODSGsec_mas_consultada" 
                Width="600px" Height="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="Titulo" YValueMembers="Cantidad" 
                        ChartType="Bar">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidad">
                        </AxisY>
                        <AxisX Title="Secciones / Subsecciones ">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
            <asp:ObjectDataSource ID="ODSGsec_mas_consultada" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_secc_mas_consult_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
        <asp:View ID="Ggrup_etario" runat="server">
        <div ALIGN="CENTER">   


        <br /><br /><br /><br />
            <asp:Chart ID="Chart3" runat="server" DataSourceID="ODSGgrupo_etareo" 
                IsMapAreaAttributesEncoded="True" Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="rango" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades / Promedios">
                        </AxisY>
                        <AxisX Title="Grupos Etáreos">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>

            <asp:ObjectDataSource ID="ODSGgrupo_etareo" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_grupo_etario_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
        <asp:View ID="Ghombres_mujeres" runat="server">
        <div ALIGN="CENTER">
         <input id="hm_switch" type="button" value="Cambiar Gráfica" class="form-submit" style="float:left"/>
        <div id="barras_h_m">
            <asp:Chart ID="Chart4" runat="server"  Width="600px" 
                DataSourceID="ODSGHombres_mujeres">
                <Series>
                    <asp:Series Name="Hombres" Legend="Legend1" XValueMember="UnidadPeriodica" 
                        YValueMembers="MASCULINO">
                    </asp:Series>
                    <asp:Series ChartArea="ChartArea1" Legend="Legend1" Name="Mujeres" 
                        XValueMember="UnidadPeriodica" YValueMembers="FEMENINO">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades">
                        </AxisY>
                        <AxisX Title="Linea de Tiempo">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Name="Legend1">
                    </asp:Legend>
                </Legends>
            </asp:Chart>
            
            <asp:ObjectDataSource ID="ODSGHombres_mujeres" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_hombres_mujeres_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </div>
            <div id="pai_h_m" style="display:none">
            <asp:Chart ID="Chart9" runat="server" DataSourceID="ODS_Hombres_Mujeres_PIE" 
                Height="500px" Width="500px">
                <Series>
                    <asp:Series Name="Series1" ChartType="Pie" XValueMember="GENERO" 
                        YValueMembers="CANTIDAD" Label="Genero #VALX Promedio #VAL">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>           
            <asp:ObjectDataSource ID="ODS_Hombres_Mujeres_PIE" runat="server" 
                InsertMethod="InsertRegistroEstadistico" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_hombre_mujeres_pai" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <InsertParameters>
                    <asp:Parameter Name="UsuarioID" Type="Int32" />
                    <asp:Parameter Name="paginaVisitada" Type="String" />
                    <asp:Parameter Name="titulo" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </div>
        </div>
        </asp:View>
        <asp:View ID="Ghora_mayor_concurrencia" runat="server">
        <div ALIGN="CENTER">
            <input id="hc_switch" type="button" value="Cambiar Gráfica" class="form-submit" style="float:left"/>
        <div id="hc_torta">
        <br /><br /><br /><br />
            <asp:Chart ID="Chart5" runat="server" 
                DataSourceID="ODSGhora_mayor_concurrencia" Width="600px">
                <Series>
                    <asp:Series Name="Series1" XValueMember="rango" YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades / Promedios">
                        </AxisY>
                        <AxisX Title="Rangos Horarios">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            </div>
            <div id="hc_pie" style="display:none">
            <asp:Chart ID="Chart8" runat="server" 
                DataSourceID="ODSGhora_mayor_concurrencia" Height="500px" Width="500px">
                <Series>
                    <asp:Series Name="Series1" ChartType="Pie" IsValueShownAsLabel="True" 
                        Label="Promedio #VAL (Usrs.)\nRango #VALX (hrs.)" XValueMember="rango" 
                        YValueMembers="Cantidad">
                        <SmartLabelStyle MaxMovingDistance="250" 
                            MinMovingDistance="20" AllowOutsidePlotArea="Yes" CalloutStyle="Box" />
                    </asp:Series>
                </Series>
<%--
                <Series>
                    <asp:Series Name="Series1" ChartType="Pie" XValueMember="rango" 
                        YValueMembers="Cantidad">
                    </asp:Series>
                </Series>
--%>                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            </div>
            <asp:ObjectDataSource ID="ODSGhora_mayor_concurrencia" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_horario_concurrencia_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
        <asp:View ID="Gnoticia_mas_visitada" runat="server">
        <div ALIGN="CENTER">
            <asp:Chart ID="Chart6" runat="server"
                ondatabound="Chart6_DataBound" Width="600px" 
                BackGradientStyle="HorizontalCenter" DataSourceID="ODSGtop_noticias" 
                ondatabinding="Chart6_DataBinding" Height="600px">
                <Series>
                    <asp:Series Name="Series1" ChartType="Bar" 
                        CustomProperties="DrawSideBySide=True, LabelStyle=Center" 
                        IsXValueIndexed="True" XValueMember="Titulo" XValueType="String" 
                        YValueMembers="Cantidad">
                        <SmartLabelStyle AllowOutsidePlotArea="No" />
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                        <AxisY Title="Cantidades">
                        </AxisY>
                        <AxisX Title="Noticias">
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:ObjectDataSource ID="ODSGtop_noticias" runat="server" 
                InsertMethod="InsertRegistroEstadistico" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_Estadisticas_noticias_mas_visitadas_gu" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <InsertParameters>
                    <asp:Parameter Name="UsuarioID" Type="Int32" />
                    <asp:Parameter Name="paginaVisitada" Type="String" />
                    <asp:Parameter Name="titulo" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="RadioButtonList1" Name="modo" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        </asp:View>
    </asp:MultiView>
    </asp:Panel>
    </asp:Content>

