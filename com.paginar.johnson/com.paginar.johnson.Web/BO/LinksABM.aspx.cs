﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Data;

namespace com.paginar.johnson.Web.BO
{
    public partial class LinksABM : PageBase
    {
        ControllerContenido c = new ControllerContenido();

        protected void Page_Load(object sender, EventArgs e)
        {
            if( (FormViewContenidos.CurrentMode == FormViewMode.Insert) || 
                (FormViewContenidos.CurrentMode == FormViewMode.Edit) )
            {
                FormViewContenidos.Visible = true;
            }

        }

        protected void TreeViewContenido_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewContenido.SelectedNode.DataPath;
            FormViewContenidos.ChangeMode(FormViewMode.Edit);
            FormViewContenidos.DataBind();
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");
            DsContenido.Content_ItemsMenuUrlToolTipDataTable dt=  cc.Content_ItemsMenuUrlToolTipGet(int.Parse(HiddenFieldItemID.Value));
            if (dt.Rows.Count > 0)
            {
                DsContenido.Content_ItemsMenuUrlToolTipRow dr = (DsContenido.Content_ItemsMenuUrlToolTipRow)dt.Rows[0];
                ToolTipTextBox.Text = dr.tooltip;
                LinkTextBox.Text = dr.navigateurl.Trim().Replace("javascript:", "").Replace("popup('", "").Replace("');", "").Trim();
                /**/

                DsContenido.Content_ItemsByClusterDataTable ClusterByIDItem = new DsContenido.Content_ItemsByClusterDataTable();
                ClusterByIDItem = cc.Content_ItemsGetClustersByID(dr.content_itemid);
                //Cargo el checklist
                CheckBoxList CheckListClusters = (CheckBoxList)FormViewContenidos.FindControl("CheckBoxListClustersByItems");
                DsContenido.ClusterDataTable CCDT = new DsContenido.ClusterDataTable();
                CCDT = cc.GetDataByExcludeTodos();               
                foreach (DsContenido.ClusterRow CICR in CCDT.Rows)
                {
                    //Solo argentina puede marcar todos los cluster los administratodes de otros paises solo pueden seleccionar su cluster
                    if (this.ObjectUsuario.clusteriddefecto == 1)
                    {
                        CheckListClusters.Items.Add(new ListItem(CICR.Descripcion, CICR.ID.ToString()));
                    }
                    else if (this.ObjectUsuario.clusteriddefecto != 1 && CICR.ID == this.ObjectUsuario.clusteriddefecto)
                    {
                        CheckListClusters.Items.Add(new ListItem(CICR.Descripcion, CICR.ID.ToString()));
                    }
                
                }
                //Marco el checklist
                foreach (DsContenido.Content_ItemsByClusterRow CIBCR in ClusterByIDItem.Rows)
                {
                    if (CheckListClusters.Items.FindByValue(CIBCR.ClusterID.ToString()) != null)
                        CheckListClusters.Items.FindByValue(CIBCR.ClusterID.ToString()).Selected = true;
                }
                /**/
            }

        }


        protected void FormViewContenidos_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            TreeViewContenido.DataBind();
        }

        protected void FormViewContenidos_DataBound(object sender, EventArgs e)
        {
            FormViewContenidos.Visible = true;
            if (FormViewContenidos.CurrentMode == FormViewMode.Edit)
                if (TreeViewContenido.SelectedNode != null)
                    if (TreeViewContenido.SelectedNode.Parent != null)
                    {
                        ListItem Li = new ListItem(TreeViewContenido.SelectedNode.Parent.Value, TreeViewContenido.SelectedNode.Parent.DataPath);
                        DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
                        parentIdDropDownList.Items.Add(Li);
                    }
                    else
                    {
                        FormViewContenidos.Visible = false;
                    }

        }

        protected void FormViewContenidos_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            //int value = 1;
            //if (value == 1)
            //    throw new Exception("Hola mundo error de prueba!");
            
            ControllerContenido ccc = new ControllerContenido();
            int valor = 0;
            FormView FV = sender as FormView;

            if (int.TryParse(FV.SelectedValue.ToString(), out valor))
            {

                CheckBoxList Checklist = FormViewContenidos.FindControl("CheckBoxListClustersByItems") as CheckBoxList;
                ccc.Content_ItemsByItemsDelete(valor);
                for (int i = 0; i < Checklist.Items.Count; i++)
                {
                    if (Checklist.Items[i].Selected)
                    {

                        ccc.Content_ItemsByClusterAdd(valor, int.Parse(Checklist.Items[i].Value));
                    }
                }
            }

            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.NewValues["parentId"] = int.Parse(HiddenFieldItemParentID.Value);
            e.NewValues["Content_TipoID"] = int.Parse(HiddenFieldContentTipoID.Value);
            e.NewValues["Contenido"] = "-";
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");
            cc.Content_ItemsMenuUrlToolTipUpdate(int.Parse(FormViewContenidos.SelectedValue.ToString()),"javascript: popup('" + LinkTextBox.Text + "');", ToolTipTextBox.Text );
        }

        protected void ButtonCrearHijo_Click(object sender, EventArgs e)
        {




                FormViewContenidos.ChangeMode(FormViewMode.Insert);

                ControllerContenido cc2 = new ControllerContenido();
                //Cargo el checklist
                CheckBoxList CheckListClustersins = (CheckBoxList)FormViewContenidos.FindControl("CheckBoxListClustersByItems");


                if ((sender.GetType().ToString() == "System.Web.UI.WebControls.CheckBoxList" && CheckListClustersins.Items.Count > 0) || CheckListClustersins==null)
                     return;
                CheckListClustersins.Items.Clear();
                DsContenido.ClusterDataTable CCDT = new DsContenido.ClusterDataTable();
                CCDT = cc2.GetDataByExcludeTodos();
                foreach (DsContenido.ClusterRow CICR in CCDT.Rows)
                {
                 //   CheckListClustersins.Items.Add(new ListItem(CICR.Descripcion, CICR.ID.ToString()));
                    if (this.ObjectUsuario.clusteriddefecto == 1)
                    {
                        CheckListClustersins.Items.Add(new ListItem(CICR.Descripcion, CICR.ID.ToString()));
                    }
                    else if (this.ObjectUsuario.clusteriddefecto != 1 && CICR.ID == this.ObjectUsuario.clusteriddefecto)
                    {
                        CheckListClustersins.Items.Add(new ListItem(CICR.Descripcion, CICR.ID.ToString()));
                    }

                }
                CheckListClustersins.Visible = true;
                    
            
        }

        protected void FormViewContenidos_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.Values["parentId"] = int.Parse(HiddenFieldItemParentID.Value);
            e.Values["Content_TipoID"] = int.Parse(HiddenFieldContentTipoID.Value); 
            e.Values["Contenido"] = "-";
        }

        protected void FormViewContenidos_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {

           
            TreeViewContenido.DataBind();
        }

        protected void FormViewContenidos_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            TreeViewContenido.DataBind();
        }

        protected void ObjectDataSource1_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            int content_itemid = (int)e.ReturnValue;
            ControllerContenido cc = new ControllerContenido();
            TextBox ToolTipTextBox = (TextBox)FormViewContenidos.FindControl("ToolTipTextBox");
            TextBox LinkTextBox = (TextBox)FormViewContenidos.FindControl("LinkTextBox");            
            cc.Content_ItemsMenuUrlToolTipAdd(content_itemid, "javascript: popup('" + LinkTextBox.Text + "');", ToolTipTextBox.Text);



                CheckBoxList Checklist = FormViewContenidos.FindControl("CheckBoxListClustersByItems") as CheckBoxList;

                for (int i = 0; i < Checklist.Items.Count; i++)
                {
                    if (Checklist.Items[i].Selected)
                    {
                        cc.Content_ItemsByClusterAdd(content_itemid, int.Parse(Checklist.Items[i].Value));
                    }
                }
            //cc.Content_ItemsByClusterAdd(content_itemid, 1);//1 por q es solo Arg.
        }

        protected void FormViewContenidos_ItemDeleting(object sender, FormViewDeleteEventArgs e)
        {
            ControllerContenido cc = new ControllerContenido();
            int valor;
            FormView FV = sender as FormView;
            if (int.TryParse(FV.SelectedValue.ToString(), out valor))
            {

                cc.Content_ItemsByItemsDelete(valor);
            }
            cc.Content_ItemsMenuUrlToolTipDelete(int.Parse(FormViewContenidos.SelectedValue.ToString()));
            cc.Content_ItemsByClusterDelete(int.Parse(FormViewContenidos.SelectedValue.ToString()), 1); //1 por q es solo Arg.
        }

        //protected void TreeViewContenido_DataBound(object sender, EventArgs e)
        //{
        //    DAL.DsContenido.Content_ItemsDataTable dt = c.Content_ItemsGetMenu(47, this.ObjectUsuario.clusteridactual);

        //    if (dt.Rows.Count > 0)
        //    {
        //        TreeNode itemborrar = TreeViewContenido.FindNode(e.ToString());// .FindItem(e.Item.ValuePath);
        //        if (itemborrar != null)
        //        {
        //            if (itemborrar.Parent != null)
        //            {
        //                TreeNode parent = itemborrar.Parent;
        //                if (parent != null)
        //                {
        //                    parent.ChildNodes.Remove(itemborrar);
        //                }
        //                else
        //                {
        //                    TreeViewContenido.Nodes.Remove(itemborrar);
        //                }
        //            }
        //            else TreeViewContenido.Nodes.Remove(itemborrar);
        //        }

        //    }
        //    else
        //    {
        //        DAL.DsContenido.Content_ItemsRow dr = (DAL.DsContenido.Content_ItemsRow)dt.Rows[0];
        //        /*
        //        if (!dr.IstooltipNull())
        //            //if (dr.tooltip.ToString().Trim() != "")
        //                //e. .Node.ToolTip = dr.tooltip.ToString();

        //        if (!dr.IsnavigateurlNull())
        //        {
        //            if (dr.navigateurl.ToString() != "")
        //                e.Item.NavigateUrl = dr.navigateurl.ToString();
        //        }
        //        else
        //            e.Item.NavigateUrl = String.Empty;

        //         * */
        //    }
        //}

        protected void TreeViewContenido_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            if (this.ObjectUsuario.clusteriddefecto != 1)
            {
                TreeView TreeDepurar = (TreeView)sender;
                //DAL.DsContenido.Content_ItemsDataTable dt = c.Content_ItemsGetMenu(47, this.ObjectUsuario.clusteridactual);
                DAL.DsContenido.Content_ItemsDataTable dt = c.Content_ItemsGetMenu(47, this.ObjectUsuario.clusteriddefecto);
                DataView dv = new DataView(dt, "Titulo= '" + e.Node.Text + "'", "", DataViewRowState.CurrentRows);
                if (dv.Count == 0)
                {
                    //this.TreeViewContenido.Nodes.Remove(e.Node);

                    this.TreeViewContenido.Nodes.Remove(e.Node);
                    //e.Node.Target = null;
                    e.Node.NavigateUrl = "#";

                    e.Node.ToolTip = "Perteneciente a otro Cluster";

                    //e.Node.Text = null;

                }
            }

            //.Remove(e.Node);
            //int esta = dt.Select("Titulo=" + e.Node.Text).Count();

        }

  




    }
}