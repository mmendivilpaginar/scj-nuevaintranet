﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using com.paginar.johnson.BL;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace com.paginar.johnson.Web.BO
{
    public partial class VoluntariadoGaleriaABM : System.Web.UI.Page
    {
        ControllerVoluntariado CV = new ControllerVoluntariado();
        string IdEvento = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
                string VAvtiva = Request.QueryString["VAvtiva"];

                if (Request.QueryString["IdEvento"] != null)
                {
                    IdEvento = Request.QueryString["IdEvento"];
                }

                if (VAvtiva == "Imagenes")
                {
                    MVEventos.SetActiveView(VEventoImagenes);
                    string Titulo = CV.EventoSelect(int.Parse(IdEvento)).Rows[0]["Nombre"].ToString();
                    lblTitulo.Text = Titulo;
                }
                else
                    MVEventos.SetActiveView(VEventoCampos);

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            FVEventos.ChangeMode(FormViewMode.Insert);


        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {
           // GVEventos.DataBind();
            //FVEventos.ChangeMode(FormViewMode.ReadOnly);
        }

        public string UrlRetornoImagen(object IdEvento)
        {
            return "~/BO/VoluntariadoGaleriaABM.aspx?VAvtiva=Imagenes&IdEvento=" + IdEvento.ToString();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            FVImagenes.ChangeMode(FormViewMode.Insert);

        }

        public bool CheckFileType(string FileName)
        {
            string Ext = Path.GetExtension(FileName);
            if (Ext.ToLower() == ".jpg" || Ext.ToLower() == ".jpeg")
                return true;
            else
                return false;
        }

        protected void btnImagenAdd_Click(object sender, EventArgs e)
        {
            FileUpload FU = (FileUpload)FVImagenes.FindControl("FUImagen");
            TextBox TBI = (TextBox)FVImagenes.FindControl("ImagenTextBox");
            if (FU.HasFile)
            {
                HttpPostedFile file = (HttpPostedFile)FU.PostedFile;
               // if (file.ContentLength < 1000000)
                //{
                    if (CheckFileType(FU.FileName))
                    {
                        string FilePath = "~/Voluntariado/Imagenes/" + FU.FileName;
                        string FileNameRename = DateTime.Now.ToString();
                        FileNameRename = FileNameRename.Replace(" ", "");
                        FileNameRename = FileNameRename.Replace(":", "");
                        FileNameRename = FileNameRename.Replace("/", "");
                        FileNameRename = FileNameRename.Replace(".", "");
                        FileNameRename = FileNameRename + Path.GetExtension(FU.FileName);
                        string FileNameRenameTemp = FileNameRename;
                        string FileRename = "~/Voluntariado/Imagenes/" + FileNameRename;
                        //FU.SaveAs(Server.MapPath(FilePath));
                        //TBI.Text = FU.FileName;
                        FU.SaveAs(Server.MapPath(FileRename));
                        CreateThumbnail(Server.MapPath(FileRename), Server.MapPath("~/Voluntariado/Thumbnail/" + FileNameRename), 60);
                        TBI.Text = FileNameRename;


                    }

                //}
                //else
                //{
                  //  Label lblmsg = (Label)FVImagenes.FindControl("lblmessageFile");
                  //  lblmsg.ForeColor = Color.Red;

                //}
                                  
                //lblmessageFile.
                   
            }
            HiddenField hfIdEvento = (HiddenField)FVImagenes.FindControl("IdEventoTextBox");
            hfIdEvento.Value = IdEvento;
            
        }

        protected void FVEventos_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVEventos.DataBind();
        }

        protected void FVEventos_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVEventos.DataBind();
        }

        protected void FVEventos_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVEventos.DataBind();
        }

        protected void FVImagenes_ItemCreated(object sender, EventArgs e)
        {
            GVImagenes.DataBind();
        }

        protected void FVImagenes_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVImagenes.DataBind();
        }

        protected void FVImagenes_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVImagenes.DataBind();
        }

        protected void FVImagenes_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVImagenes.DataBind();
        }

        protected void FVEventos_ItemCreated(object sender, EventArgs e)
        {
            GVEventos.DataBind();
        }



        public string RutaImagen(object Imagen)
        {
            if (Imagen != null)
                return "/Voluntariado/Thumbnail/" + Imagen.ToString();
            else
            {
                return "";
            }
        }

        protected void FVImagenes_DataBound(object sender, EventArgs e)
        {
            if (FVImagenes.CurrentMode == FormViewMode.Edit || FVImagenes.CurrentMode == FormViewMode.Insert)
            {
                TextBox TBimagen = (TextBox)FVImagenes.FindControl("ImagenTextBox");
                TBimagen.Enabled = false;
            }
        }

        protected void GVImagenes_SelectedIndexChanged(object sender, EventArgs e)
        {
            FVImagenes.DataBind();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            MVEventos.SetActiveView(VEventoCampos);

            string path = HttpContext.Current.Request.Url.AbsolutePath;         

            Response.Redirect(path);            
        }

        #region Thumbnail


        public static void CreateThumbnail(string ImageFrom, string ImageTo, int ImageHeight)
        {
            System.Drawing.Image i = System.Drawing.Image.FromFile(ImageFrom);

            i.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            i.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            System.Drawing.Image th = i.GetThumbnailImage
                                (
                                ImageHeight * i.Width / i.Height,
                                ImageHeight,
                                new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback),
                                IntPtr.Zero
                                );
            i.Dispose();

            EncoderParameters ep = new EncoderParameters(1);
            ep.Param[0] = new EncoderParameter(Encoder.Quality, (long)80);
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            th.Save(ImageTo, ici, ep);
            th.Dispose();
            return;
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }


        #endregion

        protected void FVImagenes_ModeChanged(object sender, EventArgs e)
        {
            FormView F =  (FormView)sender;

            //if (F.CurrentMode == FormViewMode.Insert)
            //{
            //    TextBox IdEventoTextBox = (TextBox)FVImagenes.FindControl("IdEventoTextBox");
            //    IdEventoTextBox.Text = Request.QueryString["IdEvento"];
            //}
        }

        protected void GVEventos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            { 
            
            }
        }




    }
}