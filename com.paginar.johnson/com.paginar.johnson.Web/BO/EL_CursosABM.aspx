﻿<%@ Page Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="EL_CursosABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.EL_CursosABM" EnableEventValidation="false" ValidateRequest="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
.AspNet-GridView {
    overflow: auto;
}
</style>
<script type="text/javascript" src="/PMPFY/js/jquery.maxlength-min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('textarea[id$=DescripcionTextBox]').maxlength({
            events: [], // Array of events to be triggerd    
            maxCharacters: 500, // Characters limit   
            status: true, // True to show status indicator bewlow the element    
            statusClass: "status", // The class on the status div  
            statusText: "", // The status text  
            notificationClass: "notification", // Will be added when maxlength is reached  
            showAlert: false, // True to show a regular alert message    
            alertText: "You have typed too many characters.", // Text in alert message   
            slider: false // True Use counter slider    
        });
    });


    function button_click(objTextBox, objBtnID) {
        if (window.event.keyCode == 13) {
            document.getElementById(objBtnID).focus();
            document.getElementById(objBtnID).click();
        }
    }

    function ValidarOperariosAdministrativos(sender, args) {

        args.IsValid = true;
          if( $("input:checked").length==0)
              args.IsValid = false;

    
    
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Administración E-Learning: Cursos</h2>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="Lista" runat="server">

            <div class="controls">
            <div class="form-item leftHalf">
            <label>Código: </label>
            <asp:TextBox ID="TextBoxCodigo" runat="server" Text="" MaxLength="250"></asp:TextBox>
            </div>
            
            <div class="form-item rightHalf">            
            <label>Descripción: </label>
            <asp:TextBox ID="TextBoxDescripcion" runat="server" Text="" MaxLength="500"></asp:TextBox>
            </div>

            <div class="form-item leftHalf">
            <asp:Button ID="ButtonBuscarCurso" runat="server"  Text="Buscar Curso" onclick="ButtonBuscarCurso_Click" />
            </div>

            <div class="form-item rightHalf">
            </div>

            </div>
        <asp:GridView ID="GridViewEL_Cursos" runat="server"  AllowSorting="true"
            DataSourceID="ObjectDataSourceCursos" AutoGenerateColumns="False" 
            DataKeyNames="CursoID" onrowcommand="GridViewEL_Cursos_RowCommand" 
               AllowPaging="True">
            <Columns>
                <asp:TemplateField HeaderText="CursoID" SortExpression="CursoID" Visible="false">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("CursoID") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("CursoID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Código" SortExpression="Codigo">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Codigo") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Codigo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Offering_ID" SortExpression="OfferingID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBoxOfferingID" runat="server" Text='<%# Bind("OfferingID") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LabelOfferingID" runat="server" Text='<%# Bind("OfferingID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descripción" SortExpression="Descripcion">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Link" SortExpression="Link" Visible="false">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Link") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Link") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

<%--                <asp:CommandField ShowEditButton="True" ButtonType="Image" EditImageUrl="~/images/page_edit.png"
                        CancelImageUrl="~/images/cancel.png" ValidationGroup="Grabar" UpdateImageUrl="~/images/page_save.png" />--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonModificar" runat="server"  ToolTip="Modificar Curso" CommandName="Select"
                                CommandArgument='<%# Eval("CursoID") %>' ImageUrl="~/images/page_edit.png" />
                        </ItemTemplate>
                  </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonBorrar" runat="server"  ToolTip="Eliminar Curso"
                                CommandArgument='<%# Eval("CursoID") %>' CommandName="Eliminar" 
                                ImageUrl="~/images/delete.png"
                                OnClientClick='<%# Eval("CursoID", "return confirm(\"Desea Borrar el curso ?\")") %>' 
                                 />
                        </ItemTemplate>
                    </asp:TemplateField>
            </Columns>
        </asp:GridView>

           <div class="controls">
           <asp:Button ID="ButtonNuevoCurso" runat="server" OnClick="ButtonNuevoCurso_Click" Text="Agregar Curso" />
            </div>

    <asp:ObjectDataSource ID="ObjectDataSourceCursos" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="getCursosByDescripcionCodigo" 
        TypeName="com.paginar.johnson.BL.ControllerE_Learning" 
        ConvertNullToDBNull="True">       
        <SelectParameters>
            <asp:ControlParameter ControlID="TextBoxDescripcion" Name="Descripcion" 
                PropertyName="Text" Type="String"  ConvertEmptyStringToNull="false"/>
            <asp:ControlParameter ControlID="TextBoxCodigo" Name="Codigo" 
                PropertyName="Text" Type="String" ConvertEmptyStringToNull="false"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    </asp:View>
    <asp:View ID="NuevoModificar" runat="server">

        <asp:FormView ID="FormViewCurso" runat="server" DataKeyNames="CursoID" 
            DataSourceID="ObjectDataSource" 
            oniteminserted="FormViewCurso_ItemInserted" 
            onitemcommand="FormViewCurso_ItemCommand" 
            onitemupdated="FormViewCurso_ItemUpdated">
            <EditItemTemplate>
            <asp:Panel ID="Panel1" runat="server" DefaultButton="UpdateButton">
             <div class="form-item leftHalf">
               <strong> Código: </strong> <asp:TextBox ID="EstadoTextBox" runat="server" Text='<%# Bind("Codigo") %>' />
               <asp:RequiredFieldValidator ID="RequiredFieldValidatorEstado" runat="server" ErrorMessage="*" ControlToValidate="EstadoTextBox" ValidationGroup="Update"></asp:RequiredFieldValidator>
                 <asp:CustomValidator ID="CustomValidatorCodigo" runat="server" ControlToValidate="EstadoTextBox" ErrorMessage="El Código ya existe"  
                     onservervalidate="CustomValidatorCodigo_ServerValidate" ValidationGroup="Update"></asp:CustomValidator>
             </div>
             <div class="form-item rightHalf">
                <asp:HiddenField ID="HiddenFieldCursoID" runat="server" Value='<%# Bind("CursoID") %>' />
              </div>

              <div class="form-item leftHalf">
              <strong> Offering ID: </strong> <asp:TextBox ID="TextBoxOfferingID" runat="server" Text='<%# Bind("OfferingID") %>' MaxLength="100" />
              
              </div>

              <div class="form-item rightHalf">

              </div>

                          
               <div class="form-item leftHalf">
                <strong>Descripción:</strong>   <asp:TextBox ID="DescripcionTextBox" 
                       runat="server" Text='<%# Bind("Descripcion") %>' MaxLength="500" Rows="5" 
                       TextMode="MultiLine" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescripcion" runat="server" ErrorMessage="*" ControlToValidate="DescripcionTextBox" ValidationGroup="Update"></asp:RequiredFieldValidator>
              </div>

              <div class="form-item rightHalf">

              </div>

             <div class="form-item leftHalf">
                <strong>Operarios:</strong>  
                 <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Operarios") %>' />
                 &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>Administrativos:</strong>  
                 <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Administrativos") %>' />
                 <br />
                 <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Seleccione al menos un Perfil" 
                     ClientValidationFunction="ValidarOperariosAdministrativos" ValidationGroup="Update"></asp:CustomValidator>

                
              </div>

              <div class="form-item rightHalf">

              </div>
                
             <div class="form-item leftHalf">
                <strong> Link: </strong> <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLink" runat="server" ErrorMessage="*" ControlToValidate="LinkTextBox" ValidationGroup="Update"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reLink" runat="server" ControlToValidate="LinkTextBox" ErrorMessage="Link no Válido" 
                            ValidationExpression="(http(s)?://|HTTP(S)?://)([a-zA-Z0-9\s\=\-\.\?\,\'\/\\\+&amp;%\$#_:]*)?$" 
                            ValidationGroup="Update"></asp:RegularExpressionValidator>
                <br />
                </div>

                <div class="form-item rightHalf">

              </div>

                
              <div class="controls">
                <asp:Button ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Actualizar Curso"  ValidationGroup="Update" />
                <asp:Button ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                    </div>
                
                </asp:Panel>
            </EditItemTemplate>
            <InsertItemTemplate>
            <asp:Panel ID="Panel2" runat="server" DefaultButton="ButtonInsert">
             <div class="form-item leftHalf">
               <strong> Código: </strong> <asp:TextBox ID="EstadoTextBox" runat="server" Text='<%# Bind("Codigo") %>' />
               <asp:RequiredFieldValidator ID="RequiredFieldValidatorEstado" runat="server" ErrorMessage="*" ControlToValidate="EstadoTextBox" ValidationGroup="Insert"></asp:RequiredFieldValidator>
               <asp:CustomValidator ID="CustomValidatorCodigo" runat="server" ControlToValidate="EstadoTextBox" ErrorMessage="El Código ya existe" 
                     onservervalidate="CustomValidatorCodigo_ServerValidate" ValidationGroup="Insert"></asp:CustomValidator>
              </div>
             <div class="form-item rightHalf">
                 <asp:HiddenField ID="HiddenFieldCursoID" runat="server"  />
              </div>

              <div class="form-item leftHalf">
              <strong> Offering ID: </strong> <asp:TextBox ID="TextBoxOfferingID" runat="server" Text='<%# Bind("OfferingID") %>' MaxLength="100" />
              
              </div>

              <div class="form-item rightHalf">

              </div>
              <div class="form-item leftHalf">
                <strong>Descripción:</strong>   <asp:TextBox ID="DescripcionTextBox" 
                      runat="server" Text='<%# Bind("Descripcion") %>'  MaxLength="500" Rows="5" 
                      TextMode="MultiLine"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescripcion" runat="server" ErrorMessage="*" ControlToValidate="DescripcionTextBox" ValidationGroup="Insert"></asp:RequiredFieldValidator>
              </div>

               <div class="form-item rightHalf">

              </div>

              <div class="form-item leftHalf">
                <strong>Operarios:</strong>  
                 <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Operarios") %>' />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <strong>Administrativos:</strong>  
                 <asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# Bind("Administrativos") %>' />
                 <br />
                 <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Seleccione al menos un Perfil" 
                     ClientValidationFunction="ValidarOperariosAdministrativos" ValidationGroup="Insert"></asp:CustomValidator>                
              </div>

              <div class="form-item rightHalf">

              </div>
                
             <div class="form-item leftHalf">
                <strong> Link: </strong> <asp:TextBox ID="LinkTextBox" runat="server" Text='<%# Bind("Link") %>' />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLink" runat="server" ErrorMessage="*" ControlToValidate="LinkTextBox" ValidationGroup="Insert"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reLink" runat="server" ControlToValidate="LinkTextBox" ErrorMessage="Link no Válido" 
                            ValidationExpression="(http(s)?://|HTTP(S)?://)([a-zA-Z0-9\s\=\-\.\?\,\'\/\\\+&amp;%\$#_:]*)?$" 
                            ValidationGroup="Insert"></asp:RegularExpressionValidator>
                <br />
                </div>

                <div class="form-item rightHalf">

              </div>

              <div class="controls">
                <asp:Button ID="ButtonInsert" runat="server" Text="Agregar Curso"  CausesValidation="true" CommandName="Insert" ValidationGroup="Insert" />
                <asp:Button ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
              </div>
                
                </asp:Panel>
            </InsertItemTemplate>
            <ItemTemplate>
               
            </ItemTemplate>
        </asp:FormView>

   <asp:ObjectDataSource ID="ObjectDataSource" runat="server"   
            InsertMethod="EL_CursoInsert"  SelectMethod="GetCursoByID" 
    TypeName="com.paginar.johnson.BL.ControllerE_Learning"  
            UpdateMethod="EL_CursoModificar" >
       <InsertParameters>
           <asp:Parameter Name="Descripcion" Type="String" />
           <asp:Parameter Name="Link" Type="String" />
           <asp:Parameter Name="Codigo" Type="String" />
           <asp:Parameter Name="OfferingID" Type="String" />
           <asp:Parameter Name="Operarios" Type="Boolean"/>
           <asp:Parameter Name="Administrativos" Type="Boolean" />
           
       </InsertParameters>
       <SelectParameters>
           <asp:ControlParameter ControlID="GridViewEL_Cursos" Name="CursoID" 
               PropertyName="SelectedValue" Type="Int32" />
       </SelectParameters>
       <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Link" Type="String" />
            <asp:Parameter Name="Codigo" Type="String" />
            <asp:Parameter Name="OfferingID" Type="String" />
            <asp:Parameter Name="CursoID" Type="Int32" />
            <asp:Parameter Name="Operarios" Type="Boolean"/>
            <asp:Parameter Name="Administrativos" Type="Boolean" />
                        
       </UpdateParameters>
        </asp:ObjectDataSource>
    </asp:View>
    </asp:MultiView>
</asp:Content>

