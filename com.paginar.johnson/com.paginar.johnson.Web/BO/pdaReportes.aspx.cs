﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;


namespace com.paginar.johnson.Web.BO
{
    public partial class pdaReportes : System.Web.UI.Page
    {
        int totalInscriptos;
        int totalReporte2;
        ControllerPDA CPDA = new ControllerPDA();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                cargarDDLcursos();
                cargarDDLDirecciones();
                int? va = null;
                //if (DDLDireccion.SelectedValue == "")
                //    va = null;
                //else
                //    va = int.Parse(DDLDireccion.SelectedValue);

                //cargarDDLAreas();
                cargarLBcursos();
                DDLDireccion.Enabled = false;
                DDLAreas.Enabled = false;
                DDLEstado.Enabled = false;
            }
            string cursosSelect = string.Empty;
            foreach (ListItem item in lbCursos.Items)
            {
                if (item.Selected == true)
                    cursosSelect += "," + item.Value.ToString();
            }
            cursosSelect = cursosSelect.TrimStart(',');

            HFCursosSelect.Value = cursosSelect;
            HFCursosSelect.DataBind();
            cargarDDLAreas();

        }

        private void cleadLbCursos()
        {
            foreach (ListItem item in lbCursos.Items)
            {
                item.Selected = false;
                    
            }        
        }

        private void ExportAExcel()
        {
            string nombreReporte = DDTipodeReporte.SelectedItem + " - " + DateTime.Now.ToShortDateString();
            switch (DDTipodeReporte.SelectedValue.ToString())
            {
                case "1":
                    Exportar(RptInscPorCursos, nombreReporte, DDTipodeReporte.SelectedItem.ToString());
                    break;
                case "2":
                    Exportar(RptAreaDirecion, nombreReporte, DDTipodeReporte.SelectedItem.ToString());
                    break;
            }

            

        }

        protected string devolverEstado(string estadoid)
        {
            string valorRetorno = string.Empty;
            /*
            idEstado	Descripcion
            1	Pendiente
            2	Abierto
            3	Cerrado 
             */

            switch (estadoid)
	        {
                case "1":
                    valorRetorno = "Pendiente";
                    break;
                case "2":
                    valorRetorno = "Abierto";
                    break;
                case "3":
                    valorRetorno = "Cerrado";
                    break;
	        }   
            return valorRetorno;
        }

        protected string procesarCantidadReporte2(object cant)
        {
            totalReporte2 = totalReporte2 + int.Parse(cant.ToString());
            return cant.ToString();
        }

        protected void Filtrar_Click(object sender, EventArgs e)
        {
            ltrlError.Text = "";
            ltlImprimir.Text = "<a href='pdaImprimirReportes.aspx?Reporteid=" + DDTipodeReporte.SelectedValue.ToString() + "&Cursoid=" + DDLCursos.SelectedValue.ToString() + "&DireccionId=" + DDLDireccion.SelectedValue.ToString() + "&Areaid" + DDLAreas.SelectedValue.ToString() + "&Anio=" + tbAnio.Text + "' target='_blank'>Imprimir</a>";
            hlImprimir.NavigateUrl = "~/BO/pdaImprimirReportes.aspx?Reporteid=" + DDTipodeReporte.SelectedValue.ToString() + "&Cursoid=" + DDLCursos.SelectedValue.ToString() + "&DireccionId=" + DDLDireccion.SelectedValue.ToString() + "&Areaid" + DDLAreas.SelectedValue.ToString() + "&Anio=" + tbAnio.Text;

            switch (DDTipodeReporte.SelectedValue)
            {
                case "1":
                    totalInscriptos = 0;
                    procesarReporteTipo1();
                    //RptInscPorCursos.DataBind();
                    break;
                case "2":
                    totalReporte2 = 0;
                    procesarReporteTipo2();
                    //RptAreaDirecion.DataBind();
                    break;

                
            }
        }

        protected string traerTotalReporte2()
        {
            return "Total: " + totalReporte2;
        }

        private void procesarReporteTipo2()
        {
            MVReportesPDA.SetActiveView(VInscAreaDir);
        }


        protected string cantidadInscriptos(object idCurso)
        {
            int? anio = null;
            if (tbAnio.Text.Length > 0 ) 
                anio = int.Parse(tbAnio.Text);
            DAL.DSPda.pda_ReporteInscripcionPorCursoDataTable curso = CPDA.getReporteInscripcionPorCurso(int.Parse(idCurso.ToString()), null, anio);
            totalInscriptos = totalInscriptos + curso.Count;
            return curso.Count.ToString();            
        }

        protected int devolverTotalInscriptosReporte1()
        {
            return totalInscriptos;
        }

        private void procesarReporteTipo1()
        {            
            int CursosSeleccionados = 0;
            foreach (ListItem item in lbCursos.Items)
            {
                if (item.Selected == true)
                    CursosSeleccionados++;
            }
            if (CursosSeleccionados == 0)
            {
                ltrlError.Text = "Debe seleccionar al menos 1 curso.";
            }
            else
            {
                MVReportesPDA.SetActiveView(VInscPorCurso);
                int? idNestado = null;
                if (DDLEstado.SelectedValue != "" && DDLEstado.SelectedValue != null)
                    idNestado = int.Parse(DDLEstado.SelectedValue);
                int? cur = null;
                if(DDLCursos.SelectedValue !=  "")
                    cur = int.Parse(DDLCursos.SelectedValue);
            }
                
       }

        protected void cargarLBcursos()
        {
            
            DAL.DSPda.pda_cursoDataTable CDT = CPDA.pda_cursoSelectAll();
            lbCursos.Items.Clear();
            string idclinte = lbCursos.ClientID;
            //lbCursos.Attributes.Add("onclick", );
            
            //lbCursos.Items.Add(new ListItem("Todos", ""));
            foreach (DAL.DSPda.pda_cursoRow items in CDT)
            {
                lbCursos.Items.Add(new ListItem(items.Nombre , items.idCurso.ToString()));
            }
            lbCursos.DataBind();
            
        }

        protected void cargarDDLcursos()
        {
            DAL.DSPda.pda_cursoDataTable CDT = CPDA.pda_cursoSelectAll();
            DDLCursos.Items.Clear();
            DDLCursos.Items.Add(new ListItem("Todos", ""));
            foreach (DAL.DSPda.pda_cursoRow items in CDT)
            {
                DDLCursos.Items.Add(new ListItem(items.Nombre + " - Periodo: " + items.PeriodoTentativo, items.idCurso.ToString()));
            }
            DDLCursos.DataBind();
        }

        protected void cargarDDLDirecciones()
        {
            DAL.DSPda.DireccionDataTable DDT = CPDA.getDirecciones();
            DDLDireccion.Items.Clear();
            DDLDireccion.Items.Add(new ListItem("Seleccione",""));
            foreach (DAL.DSPda.DireccionRow item in DDT)
            {
                DDLDireccion.Items.Add(new ListItem(item.DireccionDET,item.DireccionID.ToString()));
            }
        }

        protected void cargarDDLAreas()
        {
            int? idDir = null;
            idDir = DDLDireccion.SelectedValue == "" ? idDir : int.Parse(DDLDireccion.SelectedValue.ToString());
                DAL.DSPda.AreasDataTable ADT = CPDA.getAreasByDireccionId(idDir);
                DDLAreas.Items.Clear();
                DDLAreas.Items.Add(new ListItem("Seleccione", ""));
                foreach (DAL.DSPda.AreasRow item in ADT)
                {
                    DDLAreas.Items.Add(new ListItem(item.AreaDESC, item.AreaID.ToString()));
                }
                DDLAreas.DataBind();

        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            ExportAExcel();
        }

        protected void resetDDLS()
        {
            cleadLbCursos();
            DDLCursos.SelectedIndex = 0;
            DDLEstado.SelectedIndex = 0;
            DDLAreas.SelectedIndex = 0;
            DDLDireccion.SelectedIndex = 0;
        }

        protected void DDTipodeReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            resetDDLS();
            
            DDLDireccion.Enabled = false;
            DDLAreas.Enabled = false;
            DDLCursos.Enabled = false;
            btnExportar.Enabled = false;
            ltlImprimir.Visible = false;
            hlImprimir.Visible = false;
            tbApellido.Text = "";
            tbLegajo.Text = "";
            GVRepUsuariosP.Enabled = false;
            GVRepUsuariosP.Visible = false;
            MVReportesPDA.SetActiveView(Vdefault);
            lbCursos.Enabled = false;
            ltlImprimir.Text = "<a href='pdaImprimirReportes.aspx?Reporteid=" + DDTipodeReporte.SelectedValue.ToString() + "&Cursoid=" + DDLCursos.SelectedValue.ToString() + "&DireccionId=" + DDLDireccion.SelectedValue.ToString() + "&Areaid" + DDLAreas.SelectedValue.ToString() + "&Anio=" + tbAnio.Text + "' target='_blank'>Imprimir</a>";
            hlImprimir.NavigateUrl = "~/BO/pdaImprimirReportes.aspx?Reporteid=" + DDTipodeReporte.SelectedValue.ToString() + "&Cursoid=" + DDLCursos.SelectedValue.ToString() + "&DireccionId=" + DDLDireccion.SelectedValue.ToString() + "&Areaid" + DDLAreas.SelectedValue.ToString() + "&Anio=" + tbAnio.Text;
            switch (DDTipodeReporte.SelectedValue)
            {
                case "1":
                    ltlImprimir.Visible = true;
                    btnExportar.Enabled = true;
                    DDLCursos.Enabled = true;
                    hlImprimir.Visible = true;
                    cargarLBcursos();
                    lbCursos.Enabled = true;
                    //MVReportesPDA.SetActiveView(VInscPorCurso);                    
                    break;

                case "2":
                    //tbAnio.Enabled = true;
                    ltlImprimir.Visible = true;
                    btnExportar.Enabled = true;
                    hlImprimir.Visible = true;
                    disableLBcursos();
                    DDLDireccion.Enabled = true;
                    DDLAreas.Enabled = true;
                    //MVReportesPDA.SetActiveView(VInscAreaDir);
                    break;

                case "3":
                    //tbAnio.Enabled = true;
                    MVReportesPDA.SetActiveView(VConsPorUsuario);
                    disableLBcursos();
                    break;

                default:
                    MVReportesPDA.SetActiveView(VInscPorCurso);
                    break;
            }
            
        }

        protected void disableLBcursos()
        {
            lbCursos.Items.Clear();
            lbCursos.Items.Add(new ListItem("No disponible para este reporte", ""));

        }

        protected string procesarCadenaMesAno(string p)
        {
            string valorRetorno = string.Empty;
            string[] arrayDate = p.Split('/');
            valorRetorno = arrayDate[1].ToString() + "/" + arrayDate[2].ToString().Substring(0, 4);
            return valorRetorno;
        }



        protected void DDLDireccion_SelectedIndexChanged(object sender, EventArgs e)
        {
            int? val = null;
            //if (((DropDownList)sender).SelectedValue != "")
            //    val = int.Parse(((DropDownList)sender).SelectedValue);
            //cargarDDLAreas(val);
        }

        protected void btnFiltrar_Click(object sender, EventArgs e)
        {
            GVRepUsuariosP.Enabled = true;
            GVRepUsuariosP.Visible = true;
            GVRepUsuariosP.DataBind();
        }

        public string RemoveDiacritics(string input)
        {
            string stFormD = input.Normalize(NormalizationForm.FormD);
            int len = stFormD.Length;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[i]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        protected void Exportar(Repeater gv, string filename, string titulo)
        {
            Response.Clear();
            Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name> Reportes PDA</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.AddHeader("content-disposition", "attachment;filename=" + RemoveDiacritics(filename) + ".xls");
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1252");
            Response.Charset = "utf-8";          
//            Response.Charset = "utf-8";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";

            Response.Write(generarExportarTodo(gv, titulo));

            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }

        private string generarExportarTodo(Repeater gv, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Repeater gvaux = new Repeater();
            gvaux = gv;


            Page page = new Page();
            HtmlForm form = new HtmlForm();

            gvaux.EnableViewState = false;


            page.EnableEventValidation = false;


            page.DesignerInitialize();
            page.Controls.Add(new LiteralControl(
                             @"<html>
                              <head>
<style>                                    

body 
{
    text-decoration: none;
	line-height: 1.3;
    color: #000000 !Important;
    font-family: Arial;
}
a {
    color:#000000;
}

h1 {
    background-color:#CFCFCF;
    layer-background-color:#CFCFCF;
}

h2 {
    background-color:#CFCFCF;
    color: #558EC6;
    margin-bottom: 20px;
}
h2 {
    color:#000000;
    font-size:45px;
}
h3 {
    color:#000000;
    margin-bottom: 10px;    
    clear:both;
}
h4 {
    color:#000000;
    font-size:13px;/*antes 14px*/
    margin-bottom: 10px;
}
p 
{
    color:#000000;
    margin-bottom: 15px;
}

.AspNet-GridView 
{
    text-decoration: none;
    margin-bottom:10px;
    clear:both;
}
.AspNet-GridView table{
    width:100%;
}
.AspNet-GridView table th,
.AspNet-GridView table td 
{
    text-decoration: none;
    padding:5px;
    border:1px solid #D9D9D9;
    color: #000000 !important;
    font-size:12px;
}

.AspNet-GridView th
{
    text-decoration: none;
    background-color:#E6E6E6;
    color:#000000;
    text-transform:uppercase;
    font-weight:normal;
}

</style>
</head><body>"));

            page.Controls.Add(form);

            Label lbl = new Label();
            lbl.Text = DateTime.Now.ToShortDateString();
            form.Controls.Add(lbl);

            form.Controls.Add(gvaux);
            if (DDTipodeReporte.SelectedValue == "1")
            {
                ((Literal)RptInscPorCursos.Controls[0].Controls[0].FindControl("ltrlFecha")).Text = "";
                form.Controls.Add(lblTotalInscriptos);
            }
            else
            {
                ((Literal)RptAreaDirecion.Controls[0].Controls[0].FindControl("ltrlFecha")).Text = "";
                form.Controls.Add(lblTotalReporte2);
            }
            page.RenderControl(htw);

            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
            string HTML;
            HTML = regEx.Replace(sb.ToString(), "");


            return HTML;
        }

        protected void RptInscPorCursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            lblTotalInscriptos.Text = "<div class=\"AspNet-GridView wrappreppda\"><table><thead><tr><td colSpan=5 align=right><div><b>Total de inscripciones en los cursos de capacitación PDA:&nbsp;&nbsp;</b><span id=CPMAIN_RptAreaDirecion_Label1_10>" + totalInscriptos.ToString() + "</span></div></td></tr></thead></table></div>";
                //"<div class=\"AspNet-GridView\"><table width=\"100%\"><thead><tr class=\"AspNet-GridView-Header\"><td colspan=\"6\"><span style=\"float:right\"><b>Total de inscripciones en los cursos de capacitación PDA:</b> " + totalInscriptos.ToString() + "</span></td></tr></thead></table></div>";
            
            ((Literal)RptInscPorCursos.Controls[0].Controls[0].FindControl("ltrlFecha")).Text = DateTime.Now.ToShortDateString();
            if (RptInscPorCursos.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
                    lblFooter.Visible = true;
                }
            }
        }

        protected void RptAreaDirecion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            lblTotalReporte2.Text = "<div class=\"AspNet-GridView wrappreppda\"><table><thead><tr><td colSpan=5 align=right><div><b>Total de inscripciones en los cursos de capacitación PDA::&nbsp;&nbsp;</b><span id=CPMAIN_RptAreaDirecion_Label1_10>" + totalReporte2.ToString() + "</span></div></td></tr></thead></table></div>";
                //"<div class=\"AspNet-GridView\"><table width=\"100%\"><thead><tr class=\"AspNet-GridView-Header\"><td colspan=\"6\"><span style=\"float:right\"><b>Total de inscripciones en los cursos de capacitación PDA:</b> " + totalReporte2.ToString() + "</span></td></tr></thead></table></div>";
            
            ((Literal)RptAreaDirecion.Controls[0].Controls[0].FindControl("ltrlFecha")).Text = DateTime.Now.ToShortDateString();
            if (RptAreaDirecion.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
                    lblFooter.Visible = true;
                }
            }
        }

        protected void lnkselectall_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in lbCursos.Items)
            {
                item.Selected = true;                    
            }

        }

        protected void lnkdeselectall_Click(object sender, EventArgs e)
        {
            cleadLbCursos();
        }

    }

    
}