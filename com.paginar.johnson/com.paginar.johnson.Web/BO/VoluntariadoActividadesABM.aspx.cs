﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace com.paginar.johnson.Web.BO
{
    public partial class VoluntariadoActividadesABM1 : System.Web.UI.Page
    {
        ControllerVoluntariado CV = new ControllerVoluntariado();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAddActividad_Click(object sender, EventArgs e)
        {
            FVActividades.ChangeMode(FormViewMode.Insert);
        }

        protected void FVActividades_ItemCreated(object sender, EventArgs e)
        {
            GVActividades.DataBind();
        }

        protected void FVActividades_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVActividades.DataBind();
        }

        protected void FVActividades_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVActividades.DataBind();
        }

        protected void FVActividades_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVActividades.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            FileUpload FU = (FileUpload)FVActividades.FindControl("FUImagen");
            TextBox TBI = (TextBox)FVActividades.FindControl("ImagenTextBox");
            if (FU.HasFile)
            {
                if (CheckFileType(FU.FileName))
                {
                    string FilePath = "~/Voluntariado/Imagenes/" + FU.FileName;
                    string FileNameRename = DateTime.Now.ToString();
                    FileNameRename = FileNameRename.Replace(" ", "");
                    FileNameRename = FileNameRename.Replace(":", "");
                    FileNameRename = FileNameRename.Replace("/", "");
                    FileNameRename = FileNameRename.Replace(".", "");
                    FileNameRename = FileNameRename + Path.GetExtension(FU.FileName);
                    string FileNameRenameTemp = FileNameRename;
                    string FileRename = "~/Voluntariado/Imagenes/" + FileNameRename;
                    //FU.SaveAs(Server.MapPath(FilePath));
                    //TBI.Text = FU.FileName;
                    FU.SaveAs(Server.MapPath(FileRename));
                    CreateThumbnail(Server.MapPath(FileRename), Server.MapPath("~/Voluntariado/Thumbnail/" + FileNameRename), 60);
                    TBI.Text = FileNameRename;


                }
            }

        }
        public static void CreateThumbnail(string ImageFrom, string ImageTo, int ImageHeight)
        {
            System.Drawing.Image i = System.Drawing.Image.FromFile(ImageFrom);

            i.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            i.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            System.Drawing.Image th = i.GetThumbnailImage
                                (
                                ImageHeight * i.Width / i.Height,
                                ImageHeight,
                                new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback),
                                IntPtr.Zero
                                );
            i.Dispose();

            EncoderParameters ep = new EncoderParameters(1);
            ep.Param[0] = new EncoderParameter(Encoder.Quality, (long)80);
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            th.Save(ImageTo, ici, ep);
            th.Dispose();
            return;
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }

        public bool CheckFileType(string FileName)
        {
            string Ext = Path.GetExtension(FileName);
            if (Ext.ToLower() == ".jpg" || Ext.ToLower() == ".jpeg")
                return true;
            else
                return false;
        }

        public string RutaImagen(object Imagen)
        {
            if (Imagen != null)
                return "/Voluntariado/Thumbnail/" + Imagen.ToString();
            else
                return "/Voluntariado/Thumbnail/noimage.jpg";
        }

        protected void FVActividades_ItemDeleting(object sender, FormViewDeleteEventArgs e)
        {            
            int IdActividad = int.Parse(e.Keys["IdActividad"].ToString());

            DSVoluntariado.vol_actividadInscriptosDataTable Inscriptos = CV.InscriptosVoluntariados(IdActividad);
            if (Inscriptos.Rows.Count > 0)
            {
                ((Button)FVActividades.FindControl("btnDelInscriptos")).Visible = true;
                ((Label)FVActividades.FindControl("lblError")).Visible = true;
                e.Cancel = true;
            }
            
        }

        protected void btnDelInscriptos_Click(object sender, EventArgs e)
        {

                CV.ActividadInscriptosDelete(int.Parse(((HiddenField)FVActividades.FindControl("HFIdActividad")).Value.ToString()));

        }

        protected void FVActividades_DataBound(object sender, EventArgs e)
        {
            //if (FVActividades.CurrentMode == FormViewMode.ReadOnly)
            //{

            //    int IdActividad = int.Parse(((HiddenField)FVActividades.FindControl("HFIdActividad")).Value.ToString());

            //    DSVoluntariado.vol_actividadInscriptosDataTable Inscriptos = CV.ActividadInscriptos(IdActividad);
            //    if (Inscriptos.Rows.Count > 0)
            //    {
            //        ((Button)FVActividades.FindControl("btnDelInscriptos")).Visible = true;
            //        ((Label)FVActividades.FindControl("lblError")).Visible = true;
            //    }
            //    else
            //    {
            //        ((Button)FVActividades.FindControl("btnDelInscriptos")).Visible = false;
            //        ((Label)FVActividades.FindControl("lblError")).Visible = false;
            //    }            
                
            //}
        }

        protected void FVActividades_ModeChanged(object sender, EventArgs e)
        {
            int a = 0;
            //((TextBox)((FormView)sender).FindControl("ActividadTextBox"))
            //chekAndLimit(elemento, max)
            //limitarEntrada
        }

        
    }
}