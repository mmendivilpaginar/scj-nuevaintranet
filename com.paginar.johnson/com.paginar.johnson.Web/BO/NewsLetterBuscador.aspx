﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="NewsLetterBuscador.aspx.cs" Inherits="com.paginar.johnson.Web.BO.NewsLetterBuscador" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<div class="newFormStyle">
 <h2>Administración de Newsletter</h2>


    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo Newsletter" 
        onclick="btnNuevo_Click" />
    <br /><br />
    <div>
        <h3>Búsqueda de Newsletter</h3>
        <div class="box formulario">
          <asp:ValidationSummary ValidationGroup="Grabar" DisplayMode="BulletList" ID="ValidationSummary1"
                            runat="server" />
            <div class="form-item full">
                <label>Estado</label>
                <asp:DropDownList ID="drpEstado" runat="server" Width="100px">
                    <asp:ListItem Value="-1">Todos</asp:ListItem>
                    <asp:ListItem Value="1">Activo</asp:ListItem>
                    <asp:ListItem Value="0">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item full">
                <label>Nombre</label>
                <asp:TextBox runat="server" ID="txtNombre" Width="200" MaxLength="200" />
                <%--  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                                TargetControlID="txtNombre" InvalidChars="!&quot;·$%&amp;/()=?¿_-{}[]" />--%>
            </div>
            <div class="full">
                <div class="form-item leftHalf">                
                    <label>
                       Creado Desde:
                    </label>
                    <asp:TextBox ID="DesdeTextBox" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                    <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                    <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                        PopupButtonID="IMGCalen1" TargetControlID="DesdeTextBox" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="DesdeTextBox"
                        ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                        Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"
                        ValidationGroup="Buscar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                        ControlToValidate="DesdeTextBox" SetFocusOnError="True">*</asp:RangeValidator>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="DesdeTextBox"
                        ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                        Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                </div>
                <div class="form-item rightHalf">
                    <label>Hasta:</label>
                    <asp:TextBox ID="HastaTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                    <asp:ImageButton ID="IMGCalen2" runat="server" ImageUrl="~/images/Calendar.png" />
                    <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                        PopupButtonID="IMGCalen2" TargetControlID="HastaTextBox" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="HastaTextBox"
                        ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                        Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="HastaTextBox"
                        ControlToValidate="DesdeTextBox" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                        Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"
                        ValidationGroup="Buscar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                        ControlToValidate="HastaTextBox" SetFocusOnError="True">*</asp:RangeValidator>
                </div>
            </div>
            <div class="controls">
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar" ValidationGroup="Buscar"
                    OnClick="BtnBuscar_Click" />
                <asp:Button ID="btnImprimir" runat="server" Text="Imprimir Listado" Style="background-image: url no-repeat(/images/printer.png)"
                    ValidationGroup="Buscar" onclick="btnImprimir_Click" />

                 <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" Style="background-image: url no-repeat(/images/printer.png)" visible="false"
                    ValidationGroup="Buscar" onclick="btnLimpiar_Click" />

                <br /><br />
                <asp:CustomValidator ID="cvBuscar" runat="server" 
                    ErrorMessage="Seleccione un criterio de Búsqueda" 
                    onservervalidate="cvBuscar_ServerValidate" ValidateEmptyText="True" 
                    ValidationGroup="Buscar"></asp:CustomValidator>
                <asp:GridView ID="gvNewsletter" runat="server" AutoGenerateColumns="False" 
                    onrowcommand="gvNewsletter_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="titulo" HeaderText="Nombre" />
                        <asp:BoundField DataField="fhalta" DataFormatString="{0: dd/MM/yyyy}" HeaderText="Fecha de Alta" />
                        <asp:BoundField DataField="estado" HeaderText="Estado" />
                        <asp:BoundField DataField="envio" HeaderText="Estado Envio" />
                        <asp:BoundField DataField="fechaenvio" DataFormatString="{0: dd/MM/yyyy}" HeaderText="Fecha de Envio" />
                        <%--<asp:BoundField DataField="Destinatarios" HeaderText="Destinatarios" />--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgPreview" ImageUrl="~/images/folder_explore.png" CommandArgument='<%# Eval("newsletterid") %>'  CommandName="Preview" ToolTip="Vista Previa"
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                <asp:ImageButton ID="imgEditar" ImageUrl="~/images/page_edit.png" CommandArgument='<%# Eval("newsletterid") %>' CommandName="Editar" ToolTip="Editar"
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgBorrar" ImageUrl="~/images/delete.png" CommandArgument='<%# Eval("newsletterid") %>' CommandName="Borrar" ToolTip="Eliminar" OnClientClick="javascript:var resp=confirm('¿Esta seguro de Eliminar el Newsletter?'); return resp;"
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEnviar" ImageUrl="~/images/icon_mail.gif"   ToolTip="Enviar manualmente" CommandArgument='<%# Eval("newsletterid") %>' CommandName="Enviar"  OnClientClick="javascript:var resp=confirm('¿Esta seguro de Enviar el Newsletter?'); return resp;"
                                Visible='<%# Boolean.Parse(  Eval("envio").ToString()=="Enviado"?"False":"True" ) &&   Boolean.Parse(  Eval("Estado").ToString()=="Activo"?"True":"False" )  %>'
                                
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                     <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                    </div>
                </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlPreview">
        <div id="mask">
        </div>
        <!--Beg Vista Previa -->
        <div id="boxes" class="preview">
            <div id="Preview" class="window">
                
                <a href="#" class="close">Cerrar</a>
                
                <div class="noticia">
                    <%--<iframe src="NewsLetterVistaPrevia.aspx" frameBorder="0" width="820" height="600" style="background: #FFF"/>--%>
                    <div>
                       <asp:Literal ID="hdPreview" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
</asp:Content>
