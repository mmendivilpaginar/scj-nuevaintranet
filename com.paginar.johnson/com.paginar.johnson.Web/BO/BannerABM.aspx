﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true"
    CodeBehind="BannerABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.BannerABM" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Administración de Banners</h2>
    
    <div class="controls">
        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo Banner" onclick="btnNuevo_Click" />
    </div>

    <asp:HiddenField ID="hdBannerID" runat="server" />

    
    <asp:FormView ID="frmBanner" runat="server" DataKeyNames="BannerID" 
        DataSourceID="odsBanner" oniteminserting="frmBanner_ItemInserting">
        
        <EditItemTemplate>
            <div class="box formulario">
                <asp:HiddenField ID="hdBannerID"  runat="server" Value='<%#Bind("BannerID") %>' />
                <div class="form-item">
                    <label>Título:</label>
                    <cc2:Editor ID="edTitulo" runat="server"   Content='<%# Bind("Titulo") %>' />
                </div>
                <div class="form-item">
                    <label>Cuerpo:</label>
                    <cc2:Editor ID="edCuerpo" runat="server" Content='<%# Bind("Cuerpo") %>' />
                </div>
                <asp:HiddenField ID="hdFHAlta" runat="server" Value='<%# Bind("FHAlta") %>' />
                <div class="form-item">
                    <label>Imagen:</label>    
                    <asp:HiddenField ID="HdImagen" runat="server" Value='<%# Bind("Imagen") %>' />
                </div>
                <div class="form-item">
                    <asp:FileUpload ID="fuImage" runat="server" />
                    <asp:Button ID="btnSubir" runat="server" Text="Subir Imagen"  ValidationGroup="SubirImagen"   onclick="btnSubir_Click" />
                    <asp:CustomValidator CssClass="messages msg-error" ID="cvImg_ServerValidate" runat="server"  ErrorMessage="Formato Incorrecto"   ValidationGroup="SubirImagen"  onservervalidate="cvImg_ServerValidate" Display="Dynamic"></asp:CustomValidator> 
                    <br />
                    <asp:Image ID="Image" Visible='<%# !string.IsNullOrEmpty(Eval("Imagen").ToString()) %>' runat="server" ImageUrl='<%# "../noticias/Imagenes/"+Eval("Imagen") %>' />
                </div>
                <div class="form-item">
                    <label>Habilitado: <asp:CheckBox ID="HabilitadoCheckBox" runat="server" Checked='<%# Bind("Habilitado") %>' /></label>
                    <asp:Repeater ID="rpCluster" runat="server" DataSourceID="odsClusters">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>                                        
                            <asp:HiddenField ID="hdCluster" runat="server" Value='<%# Eval("clusterid") %>' />
                            <li><asp:CheckBox ID="chkCluster"  runat="server" Text='<%# Eval("Descripcion") %>'  Checked='<%# Boolean.Parse(Eval("checkeado").ToString()) %>'   /></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="odsClusters" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="getClusterByBannerID" 
                        TypeName="com.paginar.johnson.BL.ControllerNoticias" >
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hdBannerID" Name="bannerid" 
                                PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <div class="form-item">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
                        onservervalidate="CustomValidator1_ServerValidate" CssClass="messages msg-error" >Debe llenar al menos un campo en el formulario.</asp:CustomValidator>
                </div>
                <div class="controls">
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Actualizar" CssClass="form-submit" />
                    <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" CssClass="form-submit" />
                </div>
            </div>
        </EditItemTemplate>

        <InsertItemTemplate>
            <div class="box formulario">
                <div class="form-item">
                    <label>Título:</label>
                    <cc2:Editor ID="edTitulo" runat="server"    Content='<%# Bind("Titulo") %>' />
                </div>
                <div class="form-item">
                    <label>Cuerpo:</label>
                    <cc2:Editor ID="edCuerpo" runat="server" Content='<%# Bind("Cuerpo") %>' />
                </div>
                <div class="form-item">
                    <label>Imagen:</label>
                    <asp:HiddenField ID="HdImagen" runat="server" Value='<%# Bind("Imagen") %>' />
                </div>
                <div class="form-item">
                    <asp:FileUpload ID="fuImage" runat="server" />
                    <asp:Button ID="btnSubir" runat="server" Text="Subir Imagen"  ValidationGroup="SubirImagen"   onclick="btnSubir_Click" />
                    <asp:CustomValidator CssClass="messages msg-error" ID="cvImg_ServerValidate" runat="server"  ErrorMessage="Formato Incorrecto"   ValidationGroup="SubirImagen"  onservervalidate="cvImg_ServerValidate" Display="Dynamic"></asp:CustomValidator> 
                    <br />
                    <asp:Image Visible="false" ID="Image" runat="server" ImageUrl='<%# "../noticias/Imagenes/"+Eval("Imagen") %>' />
                </div>
                <div class="form-item">
                    <label>Habilitado: <asp:CheckBox ID="HabilitadoCheckBox" runat="server" Checked='<%# Bind("Habilitado") %>' /></label>
                    <asp:Repeater ID="rpCluster" runat="server" DataSourceID="odsClusters">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>                
                            <asp:HiddenField ID="hdCluster" runat="server" Value='<%# Eval("id") %>' /> 
                            <li><asp:CheckBox ID="chkCluster" runat="server" Text='<%# Eval("Descripcion") %>' /></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="odsClusters" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="getCluster" TypeName="com.paginar.johnson.BL.ControllerCluster">
                    </asp:ObjectDataSource>
                </div>
                <div class="form-item">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
                        onservervalidate="CustomValidator1_ServerValidate" CssClass="messages msg-error" >Debe llenar al menos un campo en el formulario.</asp:CustomValidator>
                </div>
                <div class="controls">
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Nuevo" CssClass="form-submit" />
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancelar" CssClass="form-submit" />
                </div>
            </div>
        </InsertItemTemplate>

        <ItemTemplate>
            <div class="box formulario">
                <div class="form-item">
                    <label>BannerID:</label>
                     <asp:Label ID="BannerIDLabel" runat="server" Text='<%# Eval("BannerID") %>' />
                </div>
                <div class="form-item">
                    <label>Título:</label>
                    <asp:Label ID="TituloLabel" runat="server" Text='<%# Bind("Titulo") %>' />
                </div>
                <div class="form-item">
                    <label>Cuerpo:</label>
                    <asp:Label ID="CuerpoLabel" runat="server" Text='<%# Bind("Cuerpo") %>' />
                </div>
                <div class="form-item">
                    <label>Imagen:</label>
                    <asp:Label ID="ImagenLabel" runat="server" Text='<%# Bind("Imagen") %>' />
                </div>
                <div class="form-item">
                    <label>FHAlta:</label>
                    <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Bind("FHAlta") %>' />
                </div>
                <div class="form-item">
                    <label>Habilitado: <asp:CheckBox ID="HabilitadoCheckBox" runat="server" Checked='<%# Bind("Habilitado") %>' Enabled="false" /></label>
                    <asp:Repeater ID="rpCluster" runat="server" DataSourceID="odsClusters">
                        <HeaderTemplate>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><asp:CheckBox ID="chkCluster" runat="server" Text='<%# Eval("Descripcion") %>' Value='<%# Eval("id") %>' /></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="odsClusters" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="getCluster" TypeName="com.paginar.johnson.BL.ControllerCluster">
                    </asp:ObjectDataSource>
                </div>
                <div class="controls">
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edit" CssClass="form-submit" />
                    <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" CssClass="form-submit" />
                    <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" CssClass="form-submit" />
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>

    <asp:ObjectDataSource ID="odsBanner" runat="server" 
      DeleteMethod="BannerDelete" 
      InsertMethod="BannerAdd"
      OldValuesParameterFormatString="original_{0}" 
      SelectMethod="getBannerByID" TypeName="com.paginar.johnson.BL.ControllerNoticias"
        UpdateMethod="BannerUpdate" oninserted="odsBanner_Inserted" 
        onupdated="odsBanner_Updated">
        <DeleteParameters>
            <asp:Parameter Name="bannerid" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="titulo" Type="String" />
            <asp:Parameter Name="cuerpo" Type="String" />
            <asp:Parameter Name="imagen" Type="String" />
            <asp:Parameter Name="fhalta" Type="DateTime" />
            <asp:Parameter Name="habilitado" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="hdBannerID" Name="bannerid" PropertyName="Value"
                Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="bannerid" Type="Int32" />
            <asp:Parameter Name="titulo" Type="String" />
            <asp:Parameter Name="cuerpo" Type="String" />
            <asp:Parameter Name="imagen" Type="String" />
            <asp:Parameter Name="fhalta" Type="DateTime" />
            <asp:Parameter Name="habilitado" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>

   


    <asp:GridView ID="gvBanners" runat="server" AutoGenerateColumns="False" DataKeyNames="BannerID"
        DataSourceID="odsBanners" onrowcommand="gvBanners_RowCommand">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkEditar" runat="server" 
                        CommandArgument='<%# Bind("BannerID") %>' CommandName="Editar">Editar</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Título" SortExpression="Titulo">
                <ItemTemplate>                    
                    <asp:Literal ID="ltTitulo" runat="server" Text='<%# Bind("Titulo") %>'></asp:Literal>
                </ItemTemplate>
               
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cuerpo" SortExpression="Cuerpo">
                <ItemTemplate>
                    <asp:Literal ID="ltCuerpo" runat="server" Text='<%# Bind("Cuerpo") %>'></asp:Literal>
                </ItemTemplate>

            </asp:TemplateField>
            <asp:TemplateField HeaderText="Imagen" SortExpression="Imagen">
                <ItemTemplate>                    
                    <asp:Image ID="Imagen1" Visible='<%# !string.IsNullOrEmpty(Eval("Imagen").ToString()) %>'    runat="server" ImageUrl='<%# "../noticias/Imagenes/"+Eval("Imagen") %>' Width="50px" Height="50px" />
                </ItemTemplate>

            </asp:TemplateField>
            <asp:BoundField DataField="FHAlta" HeaderText="Fecha de Alta" SortExpression="FHAlta" 
                DataFormatString="{0: dd/MM/yyyy}" />
            <asp:CheckBoxField DataField="Habilitado" HeaderText="Habilitado" SortExpression="Habilitado" />

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkEliminar" runat="server" 
                        CommandArgument='<%# Bind("BannerID") %>' CommandName="Eliminar" 
                        onclientclick="return confirm('Está seguro que desea eliminar el Banner seleccionado?');">Eliminar</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsBanners" runat="server" OldValuesParameterFormatString="original_{0}"
     SelectMethod="getAllBanners" TypeName="com.paginar.johnson.BL.ControllerNoticias">
    </asp:ObjectDataSource>
</asp:Content>
