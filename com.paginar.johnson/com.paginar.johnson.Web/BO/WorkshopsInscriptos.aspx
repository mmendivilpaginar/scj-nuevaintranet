﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkshopsInscriptos.aspx.cs" Inherits="com.paginar.johnson.Web.BO.WorkshopsInscriptos" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Workshops - Inscriptos</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>

    
</head>
<body>
    <form id="FormImpWSInscriptos" runat="server">
    <div id="wrapper">
        <div id="header">
            <div class="bg"></div>
            <h1>Workshops</h1>
            <img id="logo" src="../images/logoprint.gif" />
        </div>
        <div id="noprint">

            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />&nbsp;&nbsp;<asp:Button ID="exportar"
                runat="server" Text="Exportar" onclick="exportar_Click" />
            <br /><br />
        </div>
        <h1><asp:Label ID="lblNombreCurso" runat="server" Text="Label"></asp:Label></h1>
        <h3>Usuarios Suscriptos</h3>
        <asp:GridView ID="GVInscriptos" runat="server" DataSourceID="ODSWSInscrptos" 
            AutoGenerateColumns="False" DataKeyNames="IdInscripcion" 
            ondatabound="GVInscriptos_DataBound" onrowdatabound="GVInscriptos_RowDataBound">
            <AlternatingRowStyle Wrap="True" />
            <Columns>
                <asp:BoundField DataField="Usuario" HeaderText="Usuario" ReadOnly="True" 
                    SortExpression="Usuario">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Suscripción" 
                    SortExpression="FechaInscripcion">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Legajo" HeaderText="Legajo" SortExpression="Legajo">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:CheckBoxField DataField="Notificada" HeaderText="Notificado" 
                    SortExpression="Notificada">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:CheckBoxField>
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ODSWSInscrptos" runat="server" 
            DeleteMethod="WorkshopDelete" InsertMethod="WorkshopInsert" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="WorkshopGetInscriptos" 
            TypeName="com.paginar.johnson.BL.ControllerWorkShop" 
            UpdateMethod="WorkshopUpdate">
            <DeleteParameters>
                <asp:Parameter Name="IdWorkshop" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Nombre" Type="String" />
                <asp:Parameter Name="Objetivo" Type="String" />
                <asp:Parameter Name="Metodologia" Type="String" />
                <asp:Parameter Name="Requisitos" Type="String" />
                <asp:Parameter Name="Duracion" Type="String" />
                <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
                <asp:Parameter Name="IdEstado" Type="Int32" />
                <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
                <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="IdWorkshop" QueryStringField="idws" 
                    Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="IdWorkshop" Type="Int32" />
                <asp:Parameter Name="Nombre" Type="String" />
                <asp:Parameter Name="Objetivo" Type="String" />
                <asp:Parameter Name="Metodologia" Type="String" />
                <asp:Parameter Name="Requisitos" Type="String" />
                <asp:Parameter Name="Duracion" Type="String" />
                <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
                <asp:Parameter Name="IdEstado" Type="Int32" />
                <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
                <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
            </UpdateParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
