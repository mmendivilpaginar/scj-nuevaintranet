﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="UsuariosActivos.aspx.cs" Inherits="com.paginar.johnson.Web.BO.UsuariosActivos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperUsuariosActivos" class="newFormStyle">
    <h2>Reporte Usuarios Activos<asp:HiddenField ID="HFApeOrd" runat="server" />
        <asp:HiddenField ID="HFLegOrd" runat="server" />
    </h2>

    <asp:Label ID="AvisoCluster" runat="server" Text="Sección no habilitada" 
        Visible="False"></asp:Label>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="form-item leftHalf">
                <label>
                    Apellido / Nombre / Legajo / &Aacute;rea / Direcci&oacute;n</label>
                <asp:TextBox ID="ApellidoTextBox" runat="server"></asp:TextBox>
            </div>
            <div class="form-item rightHalf">
                <label>
                    Grupo</label>
                <asp:DropDownList AppendDataBoundItems="true" ID="GrupoDropDownList" runat="server"
                    DataSourceID="ODSGrupos" DataTextField="Descrip" DataValueField="Numero">
                    <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ODSGrupos" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.GrupoTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_Numero" Type="Int32" />
                        <asp:Parameter Name="Original_Descrip" Type="String" />
                        <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                        <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                        <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="FHAlta" Type="DateTime" />
                        <asp:Parameter Name="UsrAlta" Type="Int32" />
                        <asp:Parameter Name="FHBaja" Type="DateTime" />
                        <asp:Parameter Name="UsrBaja" Type="Int32" />
                        <asp:Parameter Name="FHMod" Type="DateTime" />
                        <asp:Parameter Name="UsrMod" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Descrip" Type="String" />
                        <asp:Parameter Name="FHAlta" Type="DateTime" />
                        <asp:Parameter Name="UsrAlta" Type="Int32" />
                        <asp:Parameter Name="FHBaja" Type="DateTime" />
                        <asp:Parameter Name="UsrBaja" Type="Int32" />
                        <asp:Parameter Name="FHMod" Type="DateTime" />
                        <asp:Parameter Name="UsrMod" Type="Int32" />
                        <asp:Parameter Name="Original_Numero" Type="Int32" />
                        <asp:Parameter Name="Original_Descrip" Type="String" />
                        <asp:Parameter Name="Original_FHAlta" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrAlta" Type="Int32" />
                        <asp:Parameter Name="Original_FHBaja" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrBaja" Type="Int32" />
                        <asp:Parameter Name="Original_FHMod" Type="DateTime" />
                        <asp:Parameter Name="Original_UsrMod" Type="Int32" />
                    </UpdateParameters>
                </asp:ObjectDataSource>
            </div>
            <div class="controls form-end">
                <asp:Button ID="BuscarButton" runat="server"  Text="Buscar" 
                    onclick="BuscarButton_Click" />
                <asp:Button ID="ButtonDescarga" runat="server" 
                    Text="Descargar Reporte" onclick="ButtonDescarga_Click" />
            </div>  
        <div style="overflow:auto">
        <asp:GridView ID="GVUsuarios" runat="server"  DataSourceID="ODSUsuarios" 
            AutoGenerateColumns="False" DataKeyNames="UsuarioID" AllowPaging="True" 
                AllowSorting="True" onsorting="GVUsuarios_Sorting" >
            <Columns>
                <asp:BoundField DataField="UsuarioID" HeaderText="UsuarioID" 
                    InsertVisible="False" ReadOnly="True" />
                <asp:TemplateField HeaderText="Apellido" SortExpression="Apellido">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Apellido") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                <asp:TemplateField HeaderText="Legajo" SortExpression="Legajo">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Legajowd") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Legajowd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GUI" HeaderText="GUI" />
                <asp:BoundField DataField="Ubicacion" HeaderText="Ubicación" />
                <asp:BoundField DataField="Direccion" HeaderText="Dirección" />
                <asp:BoundField DataField="Area" HeaderText="Área" />
                <asp:BoundField DataField="Cargo" HeaderText="Cargo" />
                <asp:BoundField DataField="Interno" HeaderText="Interno" />
                <asp:BoundField DataField="Jobgrade" HeaderText="Job Grade" />
                <asp:BoundField DataField="Cluster" HeaderText="Cluster" />
            </Columns>
            <EmptyDataTemplate>
                La busqueda no produjo resultados.
            </EmptyDataTemplate>
        </asp:GridView>
        </div>
        <asp:ObjectDataSource ID="ODSUsuarios" runat="server" 
            OldValuesParameterFormatString="original_{0}" 
            SelectMethod="GetUsuariosActivosByBusqueda"             
            TypeName="com.paginar.johnson.BL.ControllerUsuarios">
            <SelectParameters>
                <asp:ControlParameter ControlID="ApellidoTextBox" Name="Apellido" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="GrupoDropDownList" Name="GrupoID" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </asp:Panel>
</div>
</asp:Content>
