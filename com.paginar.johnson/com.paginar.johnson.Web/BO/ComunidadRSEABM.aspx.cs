﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.BO
{
    public partial class ComunidadRSEABM : PageBase
    {
    protected void Page_Load(object sender, EventArgs e)
        {

            FormViewContenidos.Visible = (TreeViewContenido.SelectedNode != null);
            if (!Page.IsPostBack)
            {
                ControllerContenido cc = new ControllerContenido();
                if (this.ObjectUsuario.clusteriddefecto == 1)
                {
                    RelationalSystemDS.Path = cc.Content_TipoGetIdrootByClusterId("Comunidad - Responsabilidad Social Empresaria", this.ObjectUsuario.clusteridactual).ToString();
                    HiddenFieldContentTipoID.Value = cc.Content_TipoGetContent_tipoIDByClusterId("Comunidad - Responsabilidad Social Empresaria", this.ObjectUsuario.clusteridactual).ToString();
                }
                else
                {
                    RelationalSystemDS.Path = cc.Content_TipoGetIdrootByClusterId("Comunidad - Responsabilidad Social Empresaria", this.ObjectUsuario.clusteriddefecto).ToString();
                    HiddenFieldContentTipoID.Value = cc.Content_TipoGetContent_tipoIDByClusterId("Comunidad - Responsabilidad Social Empresaria", this.ObjectUsuario.clusteriddefecto).ToString();                    
                }
                EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 0 });      });");
            }
        }


        protected void TreeViewContenido_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewContenido.SelectedNode.DataPath;
            FormViewContenidos.ChangeMode(FormViewMode.Edit);
            FormViewContenidos.DataBind();
            EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 1 });      });");
        }

        protected void FormViewContenidos_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            TreeViewContenido.DataBind();
            EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 0 });      });");
        }

        protected void FormViewContenidos_DataBound(object sender, EventArgs e)
        {
            FormViewContenidos.Visible = true;
            if (FormViewContenidos.CurrentMode == FormViewMode.Edit)
                if (TreeViewContenido.SelectedNode != null)
                    if (TreeViewContenido.SelectedNode.Parent != null)
                    {
                        ListItem Li = new ListItem(TreeViewContenido.SelectedNode.Parent.Value, TreeViewContenido.SelectedNode.Parent.DataPath);
                        DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
                        parentIdDropDownList.Items.Add(Li);
                    }
                    else
                    {
                        FormViewContenidos.Visible = false;
                    }

            if (FormViewContenidos.CurrentMode == FormViewMode.Insert)
                if (TreeViewContenido.SelectedNode != null)
                {
                    ListItem Li = new ListItem(TreeViewContenido.SelectedNode.Value, TreeViewContenido.SelectedNode.DataPath);
                    DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
                    parentIdDropDownList.Items.Add(Li);

                }



        }

        protected void FormViewContenidos_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.NewValues["parentId"] = parentIdDropDownList.SelectedItem.Value;
            e.NewValues["Content_TipoID"] = HiddenFieldContentTipoID.Value;
        }

        protected void ButtonCrearHijo_Click(object sender, EventArgs e)
        {

            if (TreeViewContenido.SelectedNode != null)
            {
                FormViewContenidos.ChangeMode(FormViewMode.Insert);
                EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 1 });      });");
            }
            else
            {
                JS.ShowAlert("Seleccione el Contenido Principal al que esta agregando.", Controls);
                EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 0 });      });");
            }
        }

        protected void FormViewContenidos_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            DropDownList parentIdDropDownList = FormViewContenidos.FindControl("parentIdDropDownList") as DropDownList;
            e.Values["parentId"] = parentIdDropDownList.SelectedItem.Value;
            e.Values["Content_TipoID"] = HiddenFieldContentTipoID.Value;
        }

        protected void FormViewContenidos_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            TreeViewContenido.DataBind();
            EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 0 });      });");
        }

        protected void FormViewContenidos_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            TreeViewContenido.DataBind();
            EjecutarScript("  $(document).ready(function () {   $('#accordion').accordion({ active: 0 });      });");
        }


        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }
    }
    
}