﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;

namespace com.paginar.johnson.Web.BO
{
    public partial class VoluntariadoReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Filtrar_Click(object sender, EventArgs e)
        {
            GVReporte.DataSourceID = "ODSReporteVoluntariado";
            GVReporte.DataBind();
        }

        private void ExportAExcel(string nombreDelReporte, GridView wControl)
        {
            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            Label L = new Label();
            //L.Text = DDTipodeReporte.SelectedValue.ToString();
            form.Controls.Add(L);
            form.Controls.Add(wControl);
            pageToRender.DesignerInitialize();
            pageToRender.Controls.Add(new LiteralControl(
                             @"<html>
                              <head>
<style>                                    

body 
{
    text-decoration: none;
	line-height: 1.3;
    color: #000000 !Important;
    font-family: Arial;
}
a {
    color:#000000;
}

h1 {
    background-color:#CFCFCF;
    layer-background-color:#CFCFCF;
}

h2 {
    background-color:#CFCFCF;
    color: #558EC6;
    margin-bottom: 20px;
}
h2 {
    color:#000000;
    font-size:45px;
}
h3 {
    color:#000000;
    margin-bottom: 10px;    
    clear:both;
}
h4 {
    color:#000000;
    font-size:13px;/*antes 14px*/
    margin-bottom: 10px;
}
p 
{
    color:#000000;
    margin-bottom: 15px;
}

.AspNet-GridView 
{
    text-decoration: none;
    margin-bottom:10px;
    clear:both;
}
.AspNet-GridView table{
    width:100%;
}
.AspNet-GridView table th,
.AspNet-GridView table td 
{
    text-decoration: none;
    padding:5px;
    border:1px solid #D9D9D9;
    color: #000000 !important;
    font-size:12px;
}

.AspNet-GridView th
{
    text-decoration: none;
    background-color:#E6E6E6;
    color:#000000;
    text-transform:uppercase;
    font-weight:normal;
}

</style>
</head><body>"));


            pageToRender.Controls.Add(form);
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=" + nombreDelReporte + ".xls");
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);
            response.Write(sw.ToString());
            response.End();
        }

        protected string estado(object es)
        { 
            string retorno = string.Empty;
            if (es.ToString() == "1" || es.ToString().ToLower() == "true")
                retorno = "Activo";
            else
                retorno = "No Activo";
            return retorno;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            ExportAExcel("Reporte Voluntariado al dia " + DateTime.Now.ToShortDateString(), GVReporte);
        }
    }
}