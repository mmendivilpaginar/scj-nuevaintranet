﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace com.paginar.johnson.Web.BO
{
    public partial class ComunidadABM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string retornaEstado(object o)
        {
            string retorno = "Deshabilitado";
            if (o.ToString().Length > 0)
            {
                if (bool.Parse(o.ToString()))
                {
                    retorno = "Habilitado";
                }
            
            }

            return retorno;

        }

        public string RutaImagen(object Imagen)
        {
            if (Imagen != null)
                return "/RelacionesComunidad/Logos/" + Imagen.ToString();
            else
                return "/RelacionesComunidad/Logos/noimage.jpg";
        }

        public bool CheckFileType(string FileName)
        {
            string Ext = Path.GetExtension(FileName);
            if (Ext.ToLower() == ".jpg" || Ext.ToLower() == ".jpeg")
                return true;
            else
                return false;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            FileUpload FU = (FileUpload)FVAliado.FindControl("FUImagen");
            TextBox TBI = new TextBox();
            if(FVAliado.CurrentMode == FormViewMode.Insert)
                TBI = (TextBox)FVAliado.FindControl("imgLogoTextBoxInsert");
            else
                TBI = (TextBox)FVAliado.FindControl("imgLogoTextBox");

            if (FU.HasFile)
            {
                if (CheckFileType(FU.FileName))
                {
                    string FilePath = "~/RelacionesComunidad/Logos/" + FU.FileName;
                    string FileNameRename = DateTime.Now.ToString();
                    FileNameRename = FileNameRename.Replace(" ", "");
                    FileNameRename = FileNameRename.Replace(":", "");
                    FileNameRename = FileNameRename.Replace("/", "");
                    FileNameRename = FileNameRename.Replace(".", "");
                    FileNameRename = FileNameRename + Path.GetExtension(FU.FileName);
                    string FileNameRenameTemp = FileNameRename;
                    string FileRename = "~/RelacionesComunidad/Logos/" + FileNameRename;
                    FU.SaveAs(Server.MapPath(FileRename));                    
                    TBI.Text = FileNameRename;
                }
            }

        }



        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            FVAliado.ChangeMode(FormViewMode.Insert);
            
        }

        protected void FVAliado_ItemCreated(object sender, EventArgs e)
        {
            GVAliados.DataBind();
        }

        protected void FVAliado_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVAliados.SelectedIndex = -1;
            GVAliados.DataBind();
            FVAliado.DataBind();
            
        }

        protected void FVAliado_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVAliados.SelectedIndex = -1;
            GVAliados.DataBind();
            FVAliado.DataBind();
        }

        protected void FVAliado_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVAliados.SelectedIndex = -1;
            GVAliados.DataBind();
            FVAliado.DataBind();
        }
    }
}