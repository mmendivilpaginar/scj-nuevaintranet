﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.BO
{
    public partial class NoticiaCategoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                MVCategorias.SetActiveView(VSeleccion);
                HFUserID.Value = Master.ObjectUsuario.usuarioid.ToString();

            }
        }



        ControllerNoticias CN = new ControllerNoticias();



        protected void GVListadoCategorias_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            lblMessage.Text = "";
            ltrlError.Text = "";

            switch (e.CommandName.ToString())
            {
                case "Editar":
                    MVCategorias.SetActiveView(VEdicionDetalle);
                    HFIDEdicionDetalle.Value = e.CommandArgument.ToString();
                    FVEdicionDetalle.DataBind();
                    FVEdicionDetalle.ChangeMode(FormViewMode.Edit);
                    lblMessage.Text = "";
                    break;

                case "Eliminar":

                    int result = CN.infCat_Delete(int.Parse(e.CommandArgument.ToString()));
                    lblMessage.Text = "";
                    ltrlError.Text = "";
                    if (result == 0)
                        lblMessage.Text = "<div class=\"messages msg-exito\">El registro fue eliminado de forma satisfactoria.</div>";
                    else
                        lblMessage.Text = "<div class=\"messages msg-error\">Existen noticias con la categoría que intenta eliminar, diríjase a la sección de noticias, allí usted podrá filtrar por dicha categoría y cambiar las noticias.</div>";

                    GVListadoCategorias.DataSourceID = "ODSCategorias";
                    GVListadoCategorias.DataBind();

                    break;

                case "Detalle":
                    MVCategorias.SetActiveView(VEdicionDetalle);
                    HFIDEdicionDetalle.Value = e.CommandArgument.ToString();
                    FVEdicionDetalle.DataBind();
                    FVEdicionDetalle.ChangeMode(FormViewMode.ReadOnly);
                    lblMessage.Text = "";
                    break;
            }

            
        }





        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {
            HFIDEdicionDetalle.Value = "-1";
            MVCategorias.SetActiveView(VSeleccion);
            ltrlError.Text = "";
        }



        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            MVCategorias.SetActiveView(VSeleccion);
            GVListadoCategorias.DataSourceID = "ODSCategorias";
            GVListadoCategorias.DataBind();
            ltrlError.Text = "";
            lblMessage.Text = "";
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            ltrlError.Text = "";
            lblMessage.Text = "";

            GVListadoCategorias.SelectedIndex = -1;
            GVListadoCategorias.DataSourceID = "ODSCategoriasSearch";
            GVListadoCategorias.DataBind();
        }

        protected void InsertCancelButton_Click(object sender, EventArgs e)
        {
            ltrlError.Text = "";
            lblMessage.Text = "";
            MVCategorias.SetActiveView(VSeleccion);
            GVListadoCategorias.DataSourceID = "ODSCategorias";
            GVListadoCategorias.DataBind();


        }


        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            ltrlError.Text = "";
            lblMessage.Text = "";

            MVCategorias.SetActiveView(VEdicionDetalle);
            FVEdicionDetalle.ChangeMode(FormViewMode.Insert);

        }


        protected void FVEdicionDetalle_DataBound(object sender, EventArgs e)
        {
            if (((FormView)sender).CurrentMode == FormViewMode.Insert)
                ((HiddenField)((FormView)sender).FindControl("HFUsrAltaInsert")).Value = HFUserID.Value;
        }


        protected void InsertButton_Click(object sender, EventArgs e)
        {

            ltrlError.Text = "";
            lblMessage.Text = "";

            MVCategorias.SetActiveView(VSeleccion);
            GVListadoCategorias.DataSourceID = "ODSCategorias";
            GVListadoCategorias.DataBind();

        }


        protected void FVEdicionDetalle_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            ltrlError.Text = "";
            lblMessage.Text = "";

            GVListadoCategorias.DataSourceID = "ODSCategorias";
            GVListadoCategorias.DataBind();
            ltrlError.Text = "<div class=\"messages msg-exito\">La nueva Categoría fue agregada correctamente.</div>";
        }

        protected void FVEdicionDetalle_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            ltrlError.Text = "";
            lblMessage.Text = "";

            if (CN.boolSearchExistCategoty(((TextBox)FVEdicionDetalle.FindControl("DescripTextBox")).Text))
            {
                e.Cancel = true;
                ltrlError.Text = "<div class=\"messages msg-error\">La Categoría que intenta ingresar ya existe.</div>";
                ((TextBox)FVEdicionDetalle.FindControl("DescripTextBox")).Text = "";
            }
            
        }


    }
}