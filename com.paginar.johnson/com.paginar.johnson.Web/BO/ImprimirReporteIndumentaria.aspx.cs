﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class ImprimirReporteIndumentaria : System.Web.UI.Page
    {
        ControllerIndumentaria CI = new ControllerIndumentaria();
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (!Page.IsPostBack)
            {
                switch (Request.QueryString["TipoReporte"].ToString())
	            {
                    case "1":
                        MVRopaTrabajo.SetActiveView(indumentaria_con_detalle);
                        lblreporte.Text = "Solicitud de control de entrega de indumentaria";
                        break;
                    case "2":
                        MVRopaTrabajo.SetActiveView(indumentaria_area_sin_detalle);
                        lblreporte.Text = "Solicitud de indumentaria para proveedores";
                        break;
                    case "3":
                        MVRopaTrabajo.SetActiveView(indumentaria_sin_detalle);
                        lblreporte.Text = "Solicitudes de indumentaria sin detalle";
                        break;
	            }

                if (Request.QueryString["Area"] == "" || Request.QueryString["Area"] == null)
                {
                    lblArea.Text = "<strong>Area:</strong> Todas<br />";                  
                }
                else
                {
                    lblArea.Text = "<strong>Area:</strong> " + Request.QueryString["Area"] + "<br />";
                }

                if (Request.QueryString["esBrigadista"] == "" || Request.QueryString["esBrigadista"] == null)
                {                    
                    lblBrigadista.Text = "<strong>Brigadista: </strong> Todos" + "<br />";
                }
                else
                {
                    lblBrigadista.Text = "<strong>Brigadista:</strong> " + esBrigadista(Request.QueryString["esBrigadista"]) + "<br />";   
                }

                DSIndumentaria.Indumentaria_PeriodoDataTable IndPeriodo = CI.IndumentariaPeriodoSeleccionarPorId(int.Parse(Request.QueryString["PeriodoID"].ToString()));

                lblPeriodo.Text = "<strong>Perídodo: </strong> Desde " + ((DateTime)IndPeriodo[0][1]).ToShortDateString() + " hasta " + ((DateTime)IndPeriodo[0][1]).ToShortDateString() + "<br />";
            }

        }

        protected string esBrigadista(object valor)
        {
            if (valor.ToString() == "0")
                return "Todas";
            if (valor.ToString().ToLower() == "true" || valor.ToString() == "1")
                return "Sí";
            else
                return "No";

        }
    }

}