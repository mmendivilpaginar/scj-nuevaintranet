﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="FeriadosPXII.aspx.cs" Inherits="com.paginar.johnson.Web.BO.FeriadosPXII" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Feriados PXXI</h2>
<div class="form-item leftHalf">
                <label>
                    Periodos:</label>
                <asp:DropDownList ID="DropDownListAnio" runat="server" AutoPostBack="True">
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem Selected="True">2012</asp:ListItem>
                </asp:DropDownList>

            </div>

                <asp:GridView ID="FeriadosGV" runat="server" 
        AutoGenerateColumns="False" DataSourceID="ODSFeriadosPXXI" 
        ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="CALEN_DATA" HeaderText="CALEN_DATA" 
                            SortExpression="CALEN_DATA" DataFormatString="{0:dd/MM}" />
                        
                    </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSFeriadosPXXI" runat="server" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByAnio" 
        TypeName="com.paginar.johnson.DAL.DSTramitesTableAdapters.feriadosTableAdapter">
        <InsertParameters>
            <asp:Parameter Name="CALEN_DATA" Type="DateTime" />
            <asp:Parameter Name="CALEN_EMP" Type="String" />
            <asp:Parameter Name="CALEN_ESTAB" Type="String" />
            <asp:Parameter Name="CALEN_TIPO" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownListAnio" Name="Anio" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
