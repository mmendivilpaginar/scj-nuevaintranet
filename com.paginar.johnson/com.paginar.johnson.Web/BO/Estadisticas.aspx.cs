﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Diagnostics;

using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Text;
using System.IO;



namespace com.paginar.johnson.Web.BO
{
    public partial class Estadisticas : PageBase
    {
        bool flagDatabound = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            Chart1.ChartAreas[0].AxisX.Interval = 1;       
            Chart2.ChartAreas[0].AxisX.Interval = 1;
            Chart3.ChartAreas[0].AxisX.Interval = 1;
            Chart4.ChartAreas[0].AxisX.Interval = 1;
            Chart5.ChartAreas[0].AxisX.Interval = 1;       
            Chart6.ChartAreas[0].AxisX.Interval = 1;

            
            Chart2.ChartAreas[0].AxisX.IsReversed = true;
            Chart6.ChartAreas[0].AxisX.IsReversed = true;

            //Pie Grupo Etario
            //Chart7.Series["Series1"]["PieLabelStyle"] = "outside";
            //Chart7.Series[0]["PieStartAngle"] = "90";

            //Fin: Pie Grupo etario

            if (!Page.IsPostBack)
            {
                AvisoCluster.Visible = false;
                if (this.ObjectUsuario.clusteriddefecto != 1)
                {
                    AvisoCluster.Visible = true;
                    pnlContenido.Visible = false;
                }
                else
                {
                    pnlContenido.Visible = true;
                }
                

                LlenarPeriodoSemanal();
                LlenarMeses();
                LLenarAnos();
                SwitchPeriodos();
                imagenPrint.Visible = false;
                lblimprimir.Visible = false;
                //hlGoogleAnalytics.ToolTip ="User: intranetscj@gmail.com \n\r Pass: SCJ2011Intra"
            }
            else 
            {
                imagenPrint.Visible = true;
                lblimprimir.Visible = true;

            }
            actualizarLinkImpresion();
      
        }

        private ControllerNoticias cnr = new ControllerNoticias();

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void actualizarLinkImpresion()
        {
            string indicador = DDLIndicadores.SelectedValue.ToString();
            lblimprimir.Text = "<a href=\"imprimirEstadisticas.aspx?modo=" + RadioButtonList1.SelectedValue + "&desde=" + DesdeTextBox.Text + "&hasta=" + HastaTextBox.Text + "&indicador=" + indicador + "&tipo=" + DDLTipo.SelectedValue + "\" target=\"_blank\" ></a>";
            string SortExpression = string.Empty;
            string SortDirection = string.Empty;

            switch (MVEstadisticas.ActiveViewIndex)
            {
                case 0: // user_access_site
                    SortExpression = GridView1.SortExpression;
                    SortDirection = GridView1.SortDirection.ToString();
                    break;
                case 1: // secc_mas_consult
                    SortExpression = GridView2.SortExpression;
                    SortDirection = GridView2.SortDirection.ToString();
                    break;
                case 2: //grup_etario
                    SortExpression = GridView3.SortExpression;
                    SortDirection = GridView3.SortDirection.ToString();
                    break;
                case 3: //hombres_mujeres
                    SortExpression = GridView4.SortExpression;
                    SortDirection = GridView4.SortDirection.ToString();
                    break;
                case 4: //hora_mayor_concurrencia
                    SortExpression = GridView5.SortExpression;
                    SortDirection = GridView5.SortDirection.ToString();
                    break;
                case 5: //noticia_mas_visitada
                    SortExpression = GridView6.SortExpression;
                    SortDirection = GridView6.SortDirection.ToString();
                    break;
                case 6 ://banner
                    SortExpression = GridView7.SortExpression;
                    SortDirection = GridView7.SortDirection.ToString();
                    break;
            }

            imagenPrint.OnClientClick = string.Format("javascript:popup('imprimirEstadisticas.aspx?modo={0}&desde={1}&hasta={2}&indicador={3}&SortExpression={4}&SortDirection={5}&tipo={6}',850,620)", RadioButtonList1.SelectedValue, DesdeTextBox.Text, HastaTextBox.Text, indicador, SortExpression, SortDirection, DDLTipo.SelectedValue);
            
        }
        protected void LlenarMeses()
        {
            List<string> montList = new List<string>();
            string tempstring = string.Empty;

            for (int i = 1; i <= 12; i++)
            {
                tempstring = i.ToString();
                montList.Add(tempstring);
            }

        }

        protected void LLenarAnos()
        { 
            List<string> anoList = new List<string>();
            string year = System.DateTime.Now.Year.ToString();
            string tempstring = string.Empty;
            for (int i = int.Parse(year)-4; i <= int.Parse(year); i++)
            {
                tempstring = i.ToString();
                anoList.Add(tempstring);

            }
            anoList.Reverse();
        
        }

        protected void LlenarPeriodoSemanal()
        {
            int currentYear = DateTime.Today.Year;
            DateTime firstDayOfYear = new DateTime(currentYear, 1, 1);
            DateTime lastDayOfYear = new DateTime(currentYear, 12, 31);
            int weekNumber = 0;
            List<string> weekList = new List<string>();

            DateTime tempDate = firstDayOfYear;
            string tempString = string.Empty;

            while (tempDate <= lastDayOfYear)
            {
                weekNumber++;
                tempString = weekNumber.ToString() + " | ";
                tempString += tempDate.ToShortDateString() + " - ";
                tempDate = GetLastDayOfWeek(tempDate, lastDayOfYear);
                tempString += tempDate.ToShortDateString().Trim();
                weekList.Add(tempString);
                tempDate = tempDate.AddDays(1);
            }

        }


        static DateTime FirstDateOfWeek(int year, int weekNum, CalendarWeekRule rule)
        {
            Debug.Assert(weekNum >= 1);

            DateTime jan1 = new DateTime(year, 1, 1);

            int daysOffset = DayOfWeek.Sunday - jan1.DayOfWeek;
            DateTime firstSunday = jan1.AddDays(daysOffset);
            Debug.Assert(firstSunday.DayOfWeek == DayOfWeek.Sunday);

            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(jan1, rule, DayOfWeek.Sunday);

            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            DateTime result = firstSunday.AddDays(weekNum * 7);

            return result;
        }



        protected void SwitchPeriodos()
        {
            BTNGVHM.Visible = false;
            BTNGVNOTICIAS.Visible = false;
            BTNVTG1.Visible = false;
            BTNVTGE.Visible = false;
            BTNVTGV2.Visible = false;
            BTNVTMC.Visible = false;
            
            MVEstadisticas.Visible = false;
            MVGrafica.Visible = false;
            Dias1.Visible = true;
            Dias2.Visible = true;
            actualizarLinkImpresion();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                MVEstadisticas.Visible = false;
                MVGrafica.Visible = false;
                string tipo = DDLTipo.SelectedValue.ToString();
                string indicadores = DDLIndicadores.SelectedValue.ToString();
                SwitchPeriodos();
                int SumTotalUnitario = 0;
                if (tipo == "Reporte")
                {
                    MVEstadisticas.Visible = true;
                    switch (indicadores)
                    {
                        case "user_access_site":
                            MVEstadisticas.SetActiveView(user_access_site);
                            MVEstadisticas.Visible = true;
                            GridView1.AllowPaging = true;
                            GridView1.DataBind();
                            if (GridView1.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;

                            break;

                        case "secc_mas_consult":
                            MVEstadisticas.SetActiveView(secc_mas_consult);
                            MVEstadisticas.Visible = true;
                            GridView2.AllowPaging = true;
                            GridView2.DataBind();
                            if (GridView2.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;

                            break;
                        case "Banner_mas_visitado":
                            MVEstadisticas.SetActiveView(banner_mas_consultado);
                            MVEstadisticas.Visible = true;
                           // GridView2.AllowPaging = true;
                            GridView7.DataBind();
                            if (GridView7.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;

                            break;

                        case "grup_etario":
                            MVEstadisticas.SetActiveView(grup_etario);
                            MVEstadisticas.Visible = true;
                            GridView3.AllowPaging = true;
                            GridView3.DataBind();
                            if (GridView3.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;

                            break;

                        case "hombres_mujeres":
                            MVEstadisticas.SetActiveView(hombres_mujeres);
                            MVEstadisticas.Visible = true;
                            GridView4.AllowPaging = true;
                            GridView4.DataBind();
                            if (GridView4.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;
                            break;

                        case "hora_mayor_concurrencia":

                            MVEstadisticas.SetActiveView(hora_mayor_concurrencia);
                            MVEstadisticas.Visible = true;
                            GridView5.AllowPaging = true;
                            GridView5.DataBind();
                            if (GridView5.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;
                            break;

                        case "noticia_mas_visitada":
                            MVEstadisticas.SetActiveView(noticia_mas_visitada);
                            MVEstadisticas.Visible = true;
                            GridView6.AllowPaging = true;
                            GridView6.DataBind();
                            if (GridView6.Rows.Count > 0)
                                btnExportar.Visible = true;
                            else
                                btnExportar.Visible = false;
                            break;
                        default:
                            MVEstadisticas.Visible = false;
                            break;

                    }
                }
                if (tipo == "Grafica")
                {
                    MVGrafica.Visible = true;
                    switch (indicadores)
                    {
                        case "user_access_site":
                            MVGrafica.SetActiveView(Guser_access_site);
                            Chart1.DataBind();
                            break;

                        case "secc_mas_consult":
                            MVGrafica.SetActiveView(Gsecc_mas_consult);
                            Chart2.DataBind();
                            break;

                        case "grup_etario":
                            MVGrafica.SetActiveView(Ggrup_etario);
                            Chart3.DataBind();
                            break;

                        case "hombres_mujeres":
                            MVGrafica.SetActiveView(Ghombres_mujeres);
                            Chart4.DataBind();
                            break;

                        case "hora_mayor_concurrencia":
                            MVGrafica.SetActiveView(Ghora_mayor_concurrencia);
                            Chart5.DataBind();
                            break;

                        case "noticia_mas_visitada":
                            MVGrafica.SetActiveView(Gnoticia_mas_visitada);
                            Chart6.DataBind();
                            break;

                        default:
                            MVEstadisticas.Visible = false;
                            break;
                    }
                }
                
               
            }
        }


        private DateTime GetLastDayOfWeek(DateTime firstDayOfWeek, DateTime lastDayOfYear)
        {
            DateTime lastDayOfWeek = firstDayOfWeek;
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Sunday) { lastDayOfWeek = firstDayOfWeek.AddDays(6); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Monday) { lastDayOfWeek = firstDayOfWeek.AddDays(5); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Tuesday) { lastDayOfWeek = firstDayOfWeek.AddDays(4); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Wednesday) { lastDayOfWeek = firstDayOfWeek.AddDays(3); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Thursday) { lastDayOfWeek = firstDayOfWeek.AddDays(2); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Friday) { lastDayOfWeek = firstDayOfWeek.AddDays(1); }
            if (firstDayOfWeek.DayOfWeek == DayOfWeek.Saturday) { lastDayOfWeek = firstDayOfWeek; }

            if (lastDayOfWeek > lastDayOfYear) { lastDayOfWeek = lastDayOfYear; }

            return lastDayOfWeek;
        }


        public string formatearFecha(string fecha)
        {
            string[] valorRetorno;
            string valorRetornoResultante = string.Empty;
            if (fecha.IndexOf("-") > 0)
            {
                valorRetorno = fecha.Split('-');
            }
            else
            {
                valorRetorno = fecha.Split('/');
            }
            
            if (RadioButtonList1.SelectedValue == "Mensual")
            {
                valorRetornoResultante = valorRetorno[1];
                valorRetornoResultante += "-";
                valorRetornoResultante += valorRetorno[2]; //valorRetorno.Substring(3, 7);

            }
            else if (RadioButtonList1.SelectedValue == "Anual")
            {
                valorRetornoResultante = valorRetorno[2].ToString(); //valorRetorno.Substring(6, 4);
            }
            else 
            {
                valorRetornoResultante = fecha;
            }

            return valorRetornoResultante;

        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //HastaTextBox.Text = "";
            //DesdeTextBox.Text = "";
            imagenPrint.Visible = false;
            lblimprimir.Visible = false;
            SwitchPeriodos();
            LBLMensajes.Visible = false;
        }

        protected void DDLIndicadores_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList DropIndicadores = (DropDownList)sender;
            HastaTextBox.Text = "";
            DesdeTextBox.Text = "";
            imagenPrint.Visible = false;
            lblimprimir.Visible = false;
            RadioButtonList1.SelectedIndex = 0;
            SwitchPeriodos();
            LBLMensajes.Visible = false;
            if (DDLIndicadores.SelectedValue == "Banner_mas_visitado")
                DDLTipo.Enabled = false;
            else
                DDLTipo.Enabled = true;
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            
            foreach (GridViewRow row in GridView1.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("LBLCantUuser_access")).Text);
                //((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"#\" rel=\"tootltipEstadisticas.aspx?tipo=user_access&desde=" + DesdeTextBox.Text + "&hasta=" + HastaTextBox.Text + "&modo=" + RadioButtonList1.SelectedValue + " \" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";
                //respaldo //((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=user_access&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";
                if (flagDatabound)
                {
                    switch (RadioButtonList1.SelectedValue)
                    {
                        case "Dias":

                            ((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=user_access&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";

                            break;
                        case "Semanal":

                            string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=user_access&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";

                            break;
                        case "Mensual":

                            string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=user_access&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";

                            break;
                        case "Anual":
                            ((Label)row.FindControl("LBLCantUuser_access")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=user_access&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("LBLCantUuser_access")).Text + "</a>";

                            break;

                    }
                }
            }

            GridViewRow rowFoot = GridView1.FooterRow;
            if (rowFoot != null)
            {
             
                Label LBLTotalesUser_access = (Label)rowFoot.FindControl("LBLUsrTotSub");

                if (GridView1.AllowPaging == true && GridView1.PageCount > 1)
                {
                    LBLTotalesUser_access.Text = "Sub Total: ";
                    BTNVTG1.Visible = true;
                }
                else
                {
                    LBLTotalesUser_access.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("LBLTotalUser_access");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }
           
        }


        protected void BTNVTG1_Click(object sender, EventArgs e)
        {
            GridView1.AllowPaging = false;
            GridView1.DataBind();
        }




        public string devolverTituloNoticia(string url)
        {
            int elid;
            string titulo = string.Empty;
            string[] cadena = url.Split('=');
            elid = int.Parse(cadena[1]);
            DSNoticias.infInfoDataTable noticia =  cnr.GetInfInfoByInfoID(elid);
            foreach (DSNoticias.infInfoRow NotiRow in noticia.Rows)
            {
                titulo = NotiRow.Titulo.ToString();
            }
            return titulo;
        }

        public string devolverIdNoticia(string url)
        {
       
            string titulo = string.Empty;
            string[] cadena = url.Split('=');
            return cadena[1];
        }


      
        protected string[] returnDatesPart(DateTime FechaDelaSemana)
        { 
            string[] Fecha;
            //Fecha =  FechaDelaSemana.ToString().IndexOf('-');
            if (FechaDelaSemana.ToString().IndexOf('-') > 0)
            {
                Fecha = FechaDelaSemana.ToString().Split('-');
            }
            else
            {
                Fecha = FechaDelaSemana.ToString().Split('/');
            }
            return Fecha;
        }

        protected string[] BegEndWeek(string[] DatePart)
        {
            int ano      = int.Parse(DatePart[0]);
            int semana   = int.Parse(DatePart[1]);
            string[] PrimeroYUltimo = new string[1];
            PrimeroYUltimo[0] = FirstDateOfWeek(ano, semana, CalendarWeekRule.FirstDay).ToShortDateString();
            PrimeroYUltimo[1] = FirstDateOfWeek(ano, semana, CalendarWeekRule.FirstDay).AddDays(6).ToShortDateString();
            return PrimeroYUltimo;
            
        }

        public static int GetWeekNumber(DateTime dtPassed)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dtPassed, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return weekNum;
        }

        public DateTime FirstDayOfMonthFromDateTime(DateTime dateTime)
        {
           return new DateTime(dateTime.Year, dateTime.Month, 1);
        }


        public DateTime LastDayOfMonthFromDateTime(DateTime dateTime)
        {
           DateTime firstDayOfTheMonth = new DateTime(dateTime.Year, dateTime.Month, 1);
           return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        protected void CustomValidaDia_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string fechaInicial = DesdeTextBox.Text;
            string fechaFinal = HastaTextBox.Text;
            CustomValidator elcustom = (CustomValidator)source;
            bool vale = false;
            string ErrorMessage = string.Empty;
            string seleccionado = RadioButtonList1.SelectedValue.ToString();
            LBLMensajes.Visible = false;

            switch (seleccionado) 
            { 
                case "Dias":
                    vale = true;
                    break;
                case "Semanal":
                   
                    string[] fechaMenorArr = fechaInicial.Split('/');
                    string[] fechaMayorArr = fechaFinal.Split('/');
                    DateTime fechaMenor = DateTime.Parse(fechaInicial);
                    if (fechaMenor.DayOfWeek != DayOfWeek.Sunday)
                          fechaMenor = FirstDateOfWeek(int.Parse(fechaMenorArr[2]), GetWeekNumber(DateTime.Parse(fechaInicial)), CalendarWeekRule.FirstDay);//DateTime.Parse(fechaInicial);
                   
                    DateTime fechaMayor = FirstDateOfWeek(int.Parse(fechaMayorArr[2]), GetWeekNumber(DateTime.Parse(fechaFinal)), CalendarWeekRule.FirstDay).AddDays(6);//DateTime.Parse(fechaFinal);
                    DesdeTextBox.Text = fechaMenor.ToShortDateString();
                    HastaTextBox.Text = fechaMayor.ToShortDateString();
                   
                    TimeSpan diferencia = fechaMayor.Subtract(fechaMenor);
                    int diasDiferencia = diferencia.Days;
                    if (diasDiferencia < 13)
                    {
                        ErrorMessage += string.Format("Debe seleccionar un período de al menos 14 dias, usted seleccionó {0}", diasDiferencia.ToString());
                    }
                    else
                    {
                        // Evaluamos si corregimos las fechas dado que los rangos son semanales
                        TimeSpan result1T = DateTime.Parse(fechaInicial).Subtract(fechaMenor);
                        int result1 = result1T.Days;

                        TimeSpan result2T = DateTime.Parse(fechaFinal).Subtract(fechaMayor);
                        int result2 = result2T.Days;

                        if (result1 != 0 || result2 != 0)
                        {
                            LBLMensajes.Visible = true;
                            LBLMensajes.Text = "<div><span class=\"messages msg-info\" id=\"msngadvice\">Las fechas fueron corregidas a inicio y fin de semana dado que la unidad consultada es Semanal</span></div> ";
                        }
                        
                        vale = true;
                        
                    }

                    break;

                case "Mensual":
                    DateTime fechaMenorMes = FirstDayOfMonthFromDateTime(DateTime.Parse(fechaInicial));
                    DateTime fechaMayorMes = LastDayOfMonthFromDateTime(DateTime.Parse(fechaFinal));
                    DesdeTextBox.Text = FirstDayOfMonthFromDateTime(DateTime.Parse(fechaInicial)).ToShortDateString();
                    HastaTextBox.Text = LastDayOfMonthFromDateTime(DateTime.Parse(fechaFinal)).ToShortDateString();
                    DateTime fechaDosMeses = fechaMenorMes.AddMonths(2).AddDays(-1);
                    TimeSpan diferenciaMes = fechaDosMeses.Subtract(fechaMayorMes);
                    int diasDiferenciaMes = diferenciaMes.Days;
                    
                    if (diasDiferenciaMes > 0)
                    {
                        ErrorMessage += string.Format("Debe seleccionar un período de al menos 2 meses, la fecha \"Hasta\" debe ser posterior a {0}", fechaDosMeses.ToShortDateString());
                    }
                    else
                    {
                        // Evaluamos si corregimos las fechas dado que los rangos son Mensuales
                        TimeSpan result1T = DateTime.Parse(fechaInicial).Subtract(fechaMenorMes);
                        int result1 = result1T.Days;

                        TimeSpan result2T = DateTime.Parse(fechaFinal).Subtract(fechaMayorMes);
                        int result2 = result2T.Days;

                        if (result1 != 0 || result2 != 0)
                        {
                            LBLMensajes.Visible = true;
                            LBLMensajes.Text = "<div><span class=\"messages msg-info\" id=\"msngadvice\">Las fechas fueron corregidas a inicio y fin de mes (dentro de los meses seleccionados en las fechas \"Desde\" y \"Hasta\") dado que la unidad consultada es Mensual</span></div> ";
                        }

                        vale = true;
                    }


                    break;

                case "Anual":

                    DateTime fechaMenorAno = DateTime.Parse("01/01/"+DateTime.Parse(fechaInicial).Year.ToString());
                    DateTime fechaMayorAno = DateTime.Parse("31/12/"+DateTime.Parse(fechaFinal).Year.ToString());
                    DateTime fechaDosAnos = fechaMenorAno.AddYears(2).AddDays(-1);
                    TimeSpan diferenciaAno = fechaDosAnos.Subtract(fechaMayorAno);
                    
                    int diasDiferenciaAno = diferenciaAno.Days;
                    DesdeTextBox.Text = DateTime.Parse("01/01/"+DateTime.Parse(fechaInicial).Year).ToShortDateString();
                    HastaTextBox.Text = DateTime.Parse("31/12/"+DateTime.Parse(fechaFinal).Year).ToShortDateString();
                    if (diasDiferenciaAno > 0)
                    {
                        ErrorMessage += string.Format("Debe seleccionar un período de al menos 2 años, la fecha \"Hasta\" debe ser posterior a {0}", fechaDosAnos.ToShortDateString());
                    }
                    else
                    {
                        // Evaluamos si corregimos las fechas dado que los rangos son Mensuales
                        TimeSpan result1T = DateTime.Parse(fechaInicial).Subtract(fechaMenorAno);
                        int result1 = result1T.Days;

                        TimeSpan result2T = DateTime.Parse(fechaFinal).Subtract(fechaMayorAno);
                        int result2 = result2T.Days;

                        if (result1 != 0 || result2 != 0)
                        {
                            LBLMensajes.Visible = true;
                            LBLMensajes.Text = "<div><span class=\"messages msg-info\" id=\"msngadvice\">Las fechas fueron corregidas a inicio y fin de Año (dentro de los años seleccionados en las fechas \"Desde\" y \"Hasta\") dado que la unidad consultada es Año</span></div> ";
                        }

                        vale = true;
                    }

                    break;

            }
            actualizarLinkImpresion();
            elcustom.ErrorMessage = ErrorMessage;

            if (vale)
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        protected void BTNVTGV2_Click(object sender, EventArgs e)
        {
            GridView2.AllowPaging = false;
            GridView2.DataBind();
        }

        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView2.Rows)
            {
                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text);
                string tituloCad = "&titulo=" + ((Label)row.FindControl("Link")).Text;
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\" >" + ((Label)row.FindControl("Link")).Text + "</a>";
                switch (RadioButtonList1.SelectedValue)
                {
                    case "Dias":

                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue +  tituloCad + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Semanal":

                        string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + fecha[0]+ "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + tituloCad +"')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Mensual":

                        string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + tituloCad +"')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Anual":
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=sec_mas_cons&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;

                }

            }

            GridViewRow rowFoot = GridView2.FooterRow;
            if (rowFoot != null)
            {

                Label LBLTotalesSeccionConsultada = (Label)rowFoot.FindControl("lblTotalSecc_mas_visitada");

                if (GridView2.AllowPaging == true && GridView2.PageCount > 1)
                {
                    LBLTotalesSeccionConsultada.Text = "Sub Total: ";
                    BTNVTGV2.Visible = true;
                }
                else
                {
                    LBLTotalesSeccionConsultada.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGenSecMasCons");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }
      

        }

        protected void GridView7_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView7.Rows)
            {
                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text);
                string tituloCad = "&titulo=" + ((Label)row.FindControl("Link")).Text;
                string indicador = "&indicador=Clicks sobre Banners";
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\" >" + ((Label)row.FindControl("Link")).Text + "</a>";
                switch (RadioButtonList1.SelectedValue)
                {
                    case "Dias":

                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=Banner_mas_visitado&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Semanal":

                        string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=Banner_mas_visitado&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Mensual":

                        string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=Banner_mas_visitado&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;
                    case "Anual":
                        ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=Banner_mas_visitado&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + tituloCad + indicador + "')\" >" + ((Label)row.FindControl("lblCantUnitariaSecMasCons")).Text + "</a>";

                        break;

                }

            }

            GridViewRow rowFoot = GridView7.FooterRow;
            if (rowFoot != null)
            {

                Label LBLTotalesSeccionConsultada = (Label)rowFoot.FindControl("lblTotalSecc_mas_visitada");

                if (GridView7.AllowPaging == true && GridView7.PageCount > 1)
                {
                    LBLTotalesSeccionConsultada.Text = "Sub Total: ";
                    BTNVTGV2.Visible = true;
                }
                else
                {
                    LBLTotalesSeccionConsultada.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGenSecMasCons");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }


        }

        protected void GridView3_DataBound(object sender, EventArgs e)
        {

            int SumTotalUnitario = 0;
            foreach (GridViewRow row in GridView3.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text);
                if (flagDatabound)
                {
                    switch (RadioButtonList1.SelectedValue)
                    {
                        case "Dias":

                            ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=grupo_etario&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "&rango=" + ((Label)row.FindControl("lblRango")).Text + "')\" >" + ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text + "</a>";

                            break;
                        case "Semanal":

                            string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=grupo_etario&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "&rango=" + ((Label)row.FindControl("lblRango")).Text + "')\" >" + ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text + "</a>";

                            break;
                        case "Mensual":

                            string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=grupo_etario&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "&rango=" + ((Label)row.FindControl("lblRango")).Text + "')\" >" + ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text + "</a>";

                            break;
                        case "Anual":
                            ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=grupo_etario&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "&rango=" + ((Label)row.FindControl("lblRango")).Text + "')\" >" + ((Label)row.FindControl("lblCantUnitariaGrupoEtario")).Text + "</a>";

                            break;

                    }
                }

            }

            GridViewRow rowFoot = GridView3.FooterRow;
            if (rowFoot != null)
            {
                //LBLUsrTotSub
                Label LBLTotalesGrupoEtario = (Label)rowFoot.FindControl("lblTotSubTotGrupoEtario");

                if (GridView3.AllowPaging == true && GridView3.PageCount > 1)
                {
                    LBLTotalesGrupoEtario.Text = "Sub Total: ";
                    BTNVTGE.Visible = true;
                }
                else
                {
                    LBLTotalesGrupoEtario.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotalGrupoEtario");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }

        }

        protected void BTNVTGE_Click(object sender, EventArgs e)
        {
            GridView3.AllowPaging = false;
            GridView3.DataBind();
        }

        protected void GridView4_DataBound(object sender, EventArgs e)
        {
      
            int sumtotaltotal = 0;
            int TotalHombres = 0;
            int TotalMujeresa = 0;
            foreach (GridViewRow row in GridView4.Rows)
            {
                //&genero=FEM
                TotalHombres    += int.Parse(((Label)row.FindControl("lblhombresunidades")).Text);
                TotalMujeresa   += int.Parse( ((Label)row.FindControl("lblmujeresunidades")).Text);
                ((Label)row.FindControl("lblhombresymujeresunidad")).Text = (int.Parse(((Label)row.FindControl("lblhombresunidades")).Text) + int.Parse(((Label)row.FindControl("lblmujeresunidades")).Text)).ToString();
                if (flagDatabound)
                {
                    switch (RadioButtonList1.SelectedValue)
                    {
                        case "Dias":

                            ((Label)row.FindControl("lblhombresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=MASC&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblhombresunidades")).Text + "</a>";
                            ((Label)row.FindControl("lblmujeresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=FEM&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblmujeresunidades")).Text + "</a>";

                            break;
                        case "Semanal":

                            string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblhombresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=MASC&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblhombresunidades")).Text + "</a>";
                            ((Label)row.FindControl("lblmujeresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=FEM&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblmujeresunidades")).Text + "</a>";

                            break;
                        case "Mensual":

                            string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblhombresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=MASC&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblhombresunidades")).Text + "</a>";
                            ((Label)row.FindControl("lblmujeresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=FEM&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblmujeresunidades")).Text + "</a>";

                            break;
                        case "Anual":
                            ((Label)row.FindControl("lblhombresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=MASC&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblhombresunidades")).Text + "</a>";
                            ((Label)row.FindControl("lblmujeresunidades")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=hombres_mujeres&genero=FEM&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblmujeresunidades")).Text + "</a>";

                            break;

                    }
                }

            }
            sumtotaltotal = TotalMujeresa + TotalHombres;
            GridViewRow rowFoot = GridView4.FooterRow;
            if (rowFoot != null)
            {

                Label lblTotSubTotetiq = (Label)rowFoot.FindControl("lblTotSubTotetiq");


                if (GridView4.AllowPaging == true && GridView4.PageCount > 1)
                {
                          
                    lblTotSubTotetiq.Text        = "Sub Total: ";
                    BTNGVHM.Visible = true;
                }
                else
                {

                    lblTotSubTotetiq.Text        = "Total: ";

                }
               
                Label LabelTotalH = (Label)rowFoot.FindControl("lblhombrestotales");
                Label LabelTotalM = (Label)rowFoot.FindControl("lblmujerestotales");
                Label lblTotalesTHM = (Label)rowFoot.FindControl("lblTotalesTHM");

                lblTotalesTHM.Text = sumtotaltotal.ToString();
                LabelTotalH.Text = TotalHombres.ToString();
                LabelTotalM.Text = TotalMujeresa.ToString();
            }
      

        }

        

        protected void BTNGVHM_Click(object sender, EventArgs e)
        {
            GridView4.AllowPaging = false;
            GridView4.DataBind();

        }

        protected void BTNVTMC_Click(object sender, EventArgs e)
        {
            GridView5.AllowPaging = false;
            GridView5.DataBind();

        }

        protected void BTNGVNOTICIAS_Click(object sender, EventArgs e)
        {
            GridView6.AllowPaging = false;
            GridView6.DataBind();

        }

        protected void GridView5_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            string rango;
            foreach (GridViewRow row in GridView5.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblTotUnitHorConc")).Text);
                rango = ((Label)row.FindControl("lblrango")).Text;
                //rango = rango.Replace("-", " a ");
                ((Label)row.FindControl("lblrango")).Text = rango.Replace("-", " a ");
                if (flagDatabound)
                {
                    switch (RadioButtonList1.SelectedValue)
                    {
                        case "Dias":

                            ((Label)row.FindControl("lblTotUnitHorConc")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=horario_concurrencia&rango=" + rango + "&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitHorConc")).Text + "</a>";

                            break;
                        case "Semanal":

                            string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblTotUnitHorConc")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=horario_concurrencia&rango=" + rango + "&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitHorConc")).Text + "</a>";

                            break;
                        case "Mensual":

                            string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblTotUnitHorConc")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=horario_concurrencia&rango=" + rango + "&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitHorConc")).Text + "</a>";

                            break;
                        case "Anual":
                            ((Label)row.FindControl("lblTotUnitHorConc")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=horario_concurrencia&rango=" + rango + "&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitHorConc")).Text + "</a>";

                            break;

                    }
                }
                

            }

            GridViewRow rowFoot = GridView5.FooterRow;
            if (rowFoot != null)
            {
                Label LBLTotalesTotMayConc = (Label)rowFoot.FindControl("lblTitTotMayConc");

                if (GridView5.AllowPaging == true && GridView5.PageCount > 1)
                {
                    LBLTotalesTotMayConc.Text = "Sub Total: ";
                    BTNVTMC.Visible = true;
                }
                else
                {
                    LBLTotalesTotMayConc.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotGralHorConc");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }


        }

        protected void GridView6_DataBound(object sender, EventArgs e)
        {
            int SumTotalUnitario = 0;
            string elid;
            foreach (GridViewRow row in GridView6.Rows)
            {

                SumTotalUnitario += int.Parse(((Label)row.FindControl("lblTotUnitTopNot")).Text);
                elid = devolverIdNoticia(((Label)row.FindControl("Label2")).Text);
                ((Label)row.FindControl("Label2")).Text = "<a href=\"" + ((Label)row.FindControl("Label2")).Text + "\" target=\"_blank\">" + devolverTituloNoticia(((Label)row.FindControl("Label2")).Text) + "</a>";
                if (flagDatabound)
                {
                    switch (RadioButtonList1.SelectedValue)
                    {
                        case "Dias":

                            ((Label)row.FindControl("lblTotUnitTopNot")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=top_noticia&notidi=" + elid + "&desde=" + ((Label)row.FindControl("Label1")).Text + "&hasta=" + ((Label)row.FindControl("Label1")).Text + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitTopNot")).Text + "</a>";

                            break;
                        case "Semanal":

                            string[] fecha = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblTotUnitTopNot")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=top_noticia&notidi=" + elid + "&desde=" + fecha[0] + "&hasta=" + fecha[1] + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitTopNot")).Text + "</a>";

                            break;
                        case "Mensual":

                            string[] AnoMes = ((Label)row.FindControl("Label1")).Text.Split('-');
                            ((Label)row.FindControl("lblTotUnitTopNot")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=top_noticia&notidi=" + elid + "&desde=" + FirstDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&hasta=" + LastDayOfMonthFromDateTime(DateTime.Parse("05/" + AnoMes[1] + "/" + AnoMes[0])) + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitTopNot")).Text + "</a>";

                            break;
                        case "Anual":
                            ((Label)row.FindControl("lblTotUnitTopNot")).Text = "<a class=\"tooluser\" href=\"javascript:openuser('tootltipEstadisticas.aspx?tipo=top_noticia&notidi=" + elid + "&desde=" + DateTime.Parse("01/01/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&hasta=" + DateTime.Parse("31/12/" + ((Label)row.FindControl("Label1")).Text).ToShortDateString() + "&modo=" + RadioButtonList1.SelectedValue + "')\" >" + ((Label)row.FindControl("lblTotUnitTopNot")).Text + "</a>";

                            break;

                    }
                }

            }

            GridViewRow rowFoot = GridView6.FooterRow;
            if (rowFoot != null)
            {

                Label lblTotNot = (Label)rowFoot.FindControl("lblTitTotNot");

                if (GridView6.AllowPaging == true && GridView6.PageCount > 1)
                {
                    lblTotNot.Text = "Sub Total: ";
                    BTNGVNOTICIAS.Visible = true;
                }
                else
                {
                    lblTotNot.Text = "Total: ";
                }
                Label LabelTotal = (Label)rowFoot.FindControl("lblTotGralTopNot");

                LabelTotal.Text = SumTotalUnitario.ToString();
            }
        }

        protected void ButtonLimpiar_Click(object sender, EventArgs e)
        {
            HastaTextBox.Text = "";
            DesdeTextBox.Text = "";
            imagenPrint.Visible = false;
            SwitchPeriodos();
            //RadioButtonList1.SelectedIndex = 0;
            //DDLIndicadores.SelectedIndex = 0;
            //DDLTipo.SelectedIndex = 0;
            //MVEstadisticas.Visible = false;
            //imagenPrint.Visible = false;
            //LBLMensajes.Visible = false;
        }

        protected void GridView1_PreRender(object sender, EventArgs e)
        {

            if (((GridView)sender).Rows.Count == 0)
                imagenPrint.Visible = false;
        }

        protected void GridView1_Sorted(object sender, EventArgs e)
        {
            actualizarLinkImpresion();
        }

        protected void Chart6_DataBound(object sender, EventArgs e)
        {

            Chart Grafica = (Chart)sender;
            
            int a = 0;
        }

        protected void Chart6_DataBinding(object sender, EventArgs e)
        {
            Chart Grafica = (Chart)sender;

            int a = 0;
        }


        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }

                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }


        protected void ExportarExcel(GridView GV, string NombreArchivo)
        {
            //gv.Columns[0].ItemStyle
            GV.Columns[0].ItemStyle.Reset();
            GV.Columns[1].ItemStyle.Reset();
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page page = new Page();
            HtmlForm form = new HtmlForm();
            Label h1 = new Label();
            Label parametros = new Label();
            Table table = new Table();

            if (GV.HeaderRow != null)
            {
                PrepareControlForExport(GV.HeaderRow);
                table.Rows.Add(GV.HeaderRow);
            }

            foreach (GridViewRow row in GV.Rows)
            {
                PrepareControlForExport(row);
                foreach (DataControlFieldCell cell in row.Cells)
                {
                    PrepareControlForExport(cell);
                }
                table.Rows.Add(row);
            }

            if (GV.FooterRow != null)
            {
                PrepareControlForExport(GV.FooterRow);
                table.Rows.Add(GV.FooterRow);
            }




            h1.Text = "<strong>" + NombreArchivo + "</strong>";
            parametros.Text = "<br />Unidad de consulta: " + RadioButtonList1.SelectedValue.ToString() + "<br /> Desde: " + DesdeTextBox.Text + " Hasta: " + HastaTextBox.Text;

            GV.EnableViewState = false;


            page.EnableEventValidation = false;


            page.DesignerInitialize();

            page.Controls.Add(form);

            form.Controls.Add(h1);

            form.Controls.Add(parametros);

            //form.Controls.Add(GV);
            form.Controls.Add(table);

            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreArchivo + DateTime.Now.ToShortDateString() + " " + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();

        
        }

        

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            bool flagAcceso = false;
            string indicadores = DDLIndicadores.SelectedValue.ToString();
            string tipo = DDLTipo.SelectedValue.ToString(); //Reporte //Grafica

            if (tipo == "Reporte")
                flagAcceso = true;

            switch (indicadores)
            {
                case "user_access_site":
                    if (flagAcceso)
                    {
                        GridView1.AllowPaging = false;
                        flagDatabound = false;
                        GridView1.DataBind();
                        flagDatabound = true;
                        ExportarExcel(GridView1, "Usuarios que accedieron al sitio");
                        if (GridView1.Rows.Count > 0)
                            btnExportar.Visible = true;
                        else
                            btnExportar.Visible = false;
                    }
                    break;

                case "secc_mas_consult":
                    GridView2.AllowPaging = false;
                    flagDatabound = false;
                    GridView2.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView2,"Sección más Consultada");
                        if (GridView2.Rows.Count > 0)
                            btnExportar.Visible = true;
                        else
                            btnExportar.Visible = false;
                    
                    break;

                case "grup_etario":
                    GridView3.AllowPaging = false;
                    flagDatabound = false;
                    GridView3.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView3,"Grupos Etarios");
                        if (GridView3.Rows.Count > 0)
                            btnExportar.Visible = true;
                        else
                            btnExportar.Visible = false;

                    break;

                case "hombres_mujeres":
                    GridView4.AllowPaging = false;
                    flagDatabound = false;
                    GridView4.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView4, "Cantidad de Hombres y Mujeres");

                    break;

                case "hora_mayor_concurrencia":
                    GridView5.AllowPaging = false;
                    flagDatabound = false;
                    GridView5.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView5, "Horario de Mayor Concurrencia");

                    break;

                case "noticia_mas_visitada":
                    GridView6.AllowPaging = false;
                    flagDatabound = false;
                    GridView6.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView6, "Noticias Más Visitadas");

                    break;

                case "Banner_mas_visitado":
                    GridView7.AllowPaging = false;
                    flagDatabound = false;
                    GridView7.DataBind();
                    flagDatabound = true;
                    ExportarExcel(GridView7, "Banner mas visitado");

                    break;
                default:
                    MVEstadisticas.Visible = false;
                    break;

            }
				

        }

       

    }

    

}