﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imprimirNoticiasABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.imprimirNoticiasABM" uiCulture="Auto" Culture="Auto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Listado de Noticias</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <!--link href="../css/styles.css" rel="stylesheet" type="text/css" /-->
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
</head>
<body>
    <form id="formNoticiasABM" runat="server">
      <div id="wrapper">

        <div id="header">
            <div class="bg"></div>
            <h1>Listado Noticias</h1>
            <img id="logo" src="../images/logoprint.gif" alt="Listado Noticias"/>
        </div>
        
        <div id="noprint">
           <div id="Filtros" style="display:none;">
            <div class="form-item leftHalf">
                <asp:ScriptManager ID="ScriptManager1" runat="server" 
                    EnableScriptGlobalization="True">
                </asp:ScriptManager>
                <label>
                    Estado</label>
                <asp:DropDownList ID="EstadoDropDownList" runat="server">
                    <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                    <asp:ListItem Value="1">Activo</asp:ListItem>
                    <asp:ListItem Value="2">Inactivo</asp:ListItem>
                    <asp:ListItem Value="3">Archivado</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item rightHalf">
                <label>
                    Destacada</label>
                <asp:DropDownList ID="DestacadosDropDownList" runat="server">
                    <asp:ListItem Text="Seleccionar" Value=""></asp:ListItem>
                    <asp:ListItem Value="1">Destacada</asp:ListItem>
                    <asp:ListItem Value="2">General</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item leftHalf">
                <label>
                    Categoría</label>
                <asp:DropDownList ID="CategoriasDropDownList" runat="server" DataSourceID="ObjectDataSourceCategorias"
                    DataTextField="Descrip" DataValueField="CategoriaID" AppendDataBoundItems="true">
                    <asp:ListItem Value="" Text="Seleccione una categoría"></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceCategorias" runat="server" SelectMethod="GetData"
                    TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter">
                </asp:ObjectDataSource>
            </div>
            <div class="form-item rightHalf">
                <label>
                    &nbsp;</label>
                <asp:DropDownList ID="BuscarPorDropDownList" runat="server">
                    <asp:ListItem Value="FALTA">Fecha de alta</asp:ListItem>
                    <asp:ListItem Value="FMODIF">Fecha de modificación</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-item leftHalf">
                <label>
                    Entre</label>
                <asp:TextBox ID="DesdeTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="DesdeTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="DesdeTextBox" PopupButtonID="Image1">
                </asp:CalendarExtender>
                (dd/mm/aaaa)
            </div>
            <div class="form-item rightHalf">
                <label>
                    y</label>
                <asp:TextBox ID="HastaTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator2" runat="server" Display="Dynamic" ErrorMessage="CompareValidator"
                    ControlToValidate="HastaTextBox" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                    PopupButtonID="Image2" TargetControlID="HastaTextBox">
                </asp:CalendarExtender>
                (dd/mm/aaaa)
            </div>
            </div>
            <div class="controls">
                <asp:Button ID="BuscarButton" runat="server" Text="Buscar" OnClick="BuscarButton_Click"  Visible="false"/>
                <asp:Button ID="Button3" runat="server" OnClientClick="window.print(); return false;"
                    Text="Imprimir" Enabled="False" />
            </div>
        </div>

            <asp:GridView ID="GVNoticias" Visible="False" runat="server" DataSourceID="ODSNotiImp"
                AutoGenerateColumns="False" DataKeyNames="InfoID" AllowSorting="True" 
                PageSize="8">
                <Columns>
                    <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="InfoID">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("InfoID") %>'></asp:Label></EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="InfoIDLinkButton" runat="server" CommandArgument='<%# Eval("InfoID") %>'
                                CommandName="Select" Text='<%# Eval("InfoID") %>'></asp:LinkButton></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CategoriaDescrip" HeaderText="Categoría" SortExpression="CategoriaDescrip" />
                    <asp:TemplateField HeaderText="Título" SortExpression="Titulo">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Titulo") %>'></asp:TextBox></EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="TituloLabel" runat="server" Text='<%# Bind("Titulo") %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FHAlta" HeaderText="Fecha Alta" SortExpression="FHAlta"
                        DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundField DataField="DestacadaDesc" HeaderText="Destacada" SortExpression="DestacadaDesc" />
                    <asp:BoundField DataField="EstadoDesc" HeaderText="Estado" SortExpression="EstadoDesc" />
                    <asp:BoundField DataField="Votos" HeaderText="Votos" SortExpression="Votos" />
                    <asp:TemplateField HeaderText="Privacidad" SortExpression="Privacidad">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# GetIntString(Eval("Privacidad")) %>'></asp:Label></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <EmptyDataTemplate>
                    <div class="messages msg-info">
                        No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>

 
            <asp:ObjectDataSource ID="ODSNotiImp" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetBusquedaABM" 
                TypeName="com.paginar.johnson.BL.ControllerNoticias" 
                DeleteMethod="BannerDelete" InsertMethod="BannerClusterAdd" 
                UpdateMethod="BannerUpdate" onselected="ODSNotiImp_Selected">
                <DeleteParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                    <asp:Parameter Name="clusterid" Type="Int32" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="EstadoDropDownList" Name="Estado" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="DestacadosDropDownList" Name="Destacado" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="CategoriasDropDownList" Name="CategoriaID" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="BuscarPorDropDownList" Name="TipoBusq" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="DesdeTextBox" Name="Desde" PropertyName="Text" 
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="HastaTextBox" Name="Hasta" PropertyName="Text" 
                        Type="DateTime" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="bannerid" Type="Int32" />
                    <asp:Parameter Name="titulo" Type="String" />
                    <asp:Parameter Name="cuerpo" Type="String" />
                    <asp:Parameter Name="imagen" Type="String" />
                    <asp:Parameter Name="fhalta" Type="DateTime" />
                    <asp:Parameter Name="habilitado" Type="Boolean" />
                    <asp:Parameter Name="original_bannerid" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>

       </div>
    
    </form>
</body>
</html>
