﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="pdaABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.pdaABM" ValidateRequest="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">



    function chekAndLimit(elemento, max) {
        txt = elemento;
        MaxLength = max;
        if (txt.value.length > (MaxLength - 1)) {
            document.getElementById(elemento.id).value = document.getElementById(elemento.id).value.substring(0, MaxLength - 1);
        }
        else return true;

    }
    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperIndumentaria">
    <h2>Administración de cursos PDA</h2>
    <asp:Literal ID="lblmsg" runat="server"></asp:Literal>
<asp:Panel ID="Panel1" runat="server">
    <asp:GridView ID="GVpda" runat="server" 
        AutoGenerateColumns="False" DataKeyNames="idCurso" 
        DataSourceID="ODSgrillapda">
        <Columns>
            <asp:BoundField DataField="idCurso" HeaderText="Curso id" InsertVisible="False" 
                ReadOnly="True" SortExpression="idCurso" />
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                SortExpression="Nombre" />
            <asp:TemplateField HeaderText="Estado" SortExpression="idEstado">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("idEstado") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# dameEstado(Eval("idEstado")) %>'></asp:Label>                   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Copete" HeaderText="Copete" 
                SortExpression="Copete" Visible="False" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                SortExpression="Descripcion" Visible="False" />
            <asp:BoundField DataField="Duracion" HeaderText="Duración" 
                SortExpression="Duracion" Visible="False" />
            <asp:TemplateField HeaderText="Período Tentativo" 
                SortExpression="PeriodoTentativo" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" 
                        Text='<%# Bind("PeriodoTentativo") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# procesarCadenaMesAno(Eval("PeriodoTentativo").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>

                    <asp:ImageButton ID="ImageButtonModificar" runat="server"  ToolTip="Ver más" CommandName="Select"
                        ImageUrl="~/images/verMas.png" />
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                        CommandName="Select" Text="Ver mas..."></asp:LinkButton>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            En estos momentos no hay cursos cargados
        </EmptyDataTemplate>
    </asp:GridView>   
    <asp:ObjectDataSource ID="ODSgrillapda" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="pda_cursoSelectAll" 
        TypeName="com.paginar.johnson.BL.ControllerPDA"></asp:ObjectDataSource>

    <div class="form-item leftHalf">
    </div>
    <div class="form-item rightHalf">
    <asp:Button ID="btnNuevo" runat="server" onclick="btnNuevo_Click" style="float:right"
    Text="Nuevo" />
    </div>
    
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:Panel ID="Panel2" runat="server">
    
    <asp:FormView ID="FVpda" runat="server" DataKeyNames="idCurso" 
        DataSourceID="ODSpdadetalle" oniteminserting="FVpda_ItemInserting" 
        oniteminserted="FVpda_ItemInserted" onitemupdated="FVpda_ItemUpdated" 
        ondatabinding="FVpda_DataBinding" ondatabound="FVpda_DataBound" 
        onitemupdating="FVpda_ItemUpdating" onitemcommand="FVpda_ItemCommand">
        <EditItemTemplate>
            <b>id Curso:</b><br />
            <asp:Label ID="idCursoLabel1" runat="server" Text='<%# Eval("idCurso") %>' />
            <br />
            <br />
            <b>Nombre:</b><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' MaxLength="150" />
            <br />
            <br />
            <b>Estado:</b><br />
            <asp:DropDownList ID="idEstadoDropDown" runat="server" DataSourceID="ODSEstado" 
                DataTextField="Descripcion" DataValueField="idEstado" 
                SelectedValue='<%# Bind("idEstado") %>'>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ODSEstado" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="pda_estados" 
                TypeName="com.paginar.johnson.BL.ControllerPDA"></asp:ObjectDataSource>
            <%--<asp:TextBox ID="idEstadoTextBox" runat="server" 
                Text='<%# Bind("idEstado") %>' />--%>
            <br /><br />
            <b>Copete:</b><br />
            <asp:TextBox ID="CopeteTextBox" runat="server" onKeyDown="chekAndLimit(this,2000)" 
                Text='<%# Bind("Copete") %>' Rows="8" TextMode="MultiLine" />
            <br />
            <br />
            <b>Descripcion:</b><br />
            <asp:TextBox ID="DescripcionTextBox" runat="server" onKeyDown="chekAndLimit(this,2000)" 
                Text='<%# Bind("Descripcion") %>' Rows="8" TextMode="MultiLine" />
            <br />
            <br />
            <b>Duración:</b><br />
            <asp:TextBox ID="DuracionTextBox" runat="server" MaxLength="150"
                Text='<%# Bind("Duracion") %>' />
            <br />
            <br />
            <b>Período Tentativo:</b><br />
            <asp:TextBox ID="PeriodoTentativoTextBoxx" runat="server" MaxLength="50"
                Text='<%# Bind("PeriodoTentativo","{0:MM/yyyy}") %>' />
            <br />
            <br />
            <%--<asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True"  ImageUrl="~/images/page_save.png" ToolTip="Guardar cambios" CommandName="Update"/>--%>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True"
                CommandName="Update" Text="Actualizar" />
                <%--<asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false"  ImageUrl="~/images/back.png" ToolTip="Salir sin guardar" CommandName="Cancel"/>--%>
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                 CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
        </EditItemTemplate>
        <InsertItemTemplate>
            <b>Nombre:</b><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' MaxLength="150" />
            <br />
            <br />            
            <b>Estado:</b><br />
            <asp:DropDownList ID="idEstadoDropDown" runat="server" DataSourceID="ODSEstado" 
                DataTextField="Descripcion" DataValueField="idEstado" 
                SelectedValue='<%# Bind("idEstado") %>'>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ODSEstado" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="pda_estados" 
                TypeName="com.paginar.johnson.BL.ControllerPDA"></asp:ObjectDataSource>
            <br />
            <b>Copete:</b><br />
            <asp:TextBox ID="CopeteTextBox" runat="server" onKeyDown="chekAndLimit(this,2000)" 
                Text='<%# Bind("Copete") %>' Rows="8" TextMode="MultiLine" />
            <br />
            <br />
            <b>Descripcion:</b><br />
            <asp:TextBox ID="DescripcionTextBoxs" runat="server" onKeyDown="chekAndLimit(this,2000)" 
                Text='<%# Bind("Descripcion") %>' Rows="8" TextMode="MultiLine" />
            <br />
            <br />
            <b>Duración:</b><br />
            <asp:TextBox ID="DuracionTextBoxx" runat="server"  MaxLength="150"
                Text='<%# Bind("Duracion") %>' />
            <br />
            <br />
            <b>Período Tentativo:</b><br />
            <asp:TextBox ID="PeriodoTentativoTextBox" runat="server" MaxLength="50"
                Text='<%# Bind("PeriodoTentativo") %>' />
            <br />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True"
                CommandName="Insert" Text="Agregar" />
            <%--<asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="false" 
                CommandName="Insert" ImageUrl="~/images/page_save.png" ToolTip="Guardar" />--%>
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server"
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" /><%--<asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" 
                CommandName="Cancel" ImageUrl="~/images/back.png" ToolTip="Salir sin guardar" />--%>
        </InsertItemTemplate>
        <ItemTemplate>
            <b>id Curso:</b><br />
            <asp:Label ID="idCursoLabel" runat="server" Text='<%# Eval("idCurso") %>' />
            <br />
            <br />
            <b>Nombre:</b><br />
            <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
            <br />
            <br />
            <b>Estado:</b><br />
            <asp:DropDownList ID="idEstadoDropDown" runat="server" DataSourceID="ODSEstado" Enabled="false"
                DataTextField="Descripcion" DataValueField="idEstado" 
                SelectedValue='<%# Bind("idEstado") %>'>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ODSEstado" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="pda_estados" 
                TypeName="com.paginar.johnson.BL.ControllerPDA"></asp:ObjectDataSource>
            <br /><br />
            <b>Copete:</b><br />
            <asp:Label ID="CopeteLabel" runat="server" Text='<%# Bind("Copete") %>' />
            <br />
            <br />
            <b>Descripcion:</b><br />
            <asp:Label ID="DescripcionLabel" runat="server" 
                Text='<%# Bind("Descripcion") %>' />
            <br />
            <br />
            <b>Duración:</b><br />
            <asp:Label ID="DuracionLabel" runat="server" Text='<%# Bind("Duracion") %>' />
            <br />
            <br />
            <b>Período Tentativo:</b><br />
            <asp:Label ID="PeriodoTentativoLabel" runat="server" 
                Text='<%# Bind("PeriodoTentativo") %>' />
            <br />
            <br />
            <%--<asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True"  ImageUrl="~/images/page_edit.png" ToolTip="Editar" CommandName="Edit"/>--%>
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False"
                CommandName="Edit" Text="Editar" />
            &nbsp;
            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                CommandName="Cancel" Text="Volver" />
            <%--<asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false"  ImageUrl="~/images/back.png" ToolTip="Volver" CommandName="Cancel"/>--%>
            <%--<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                CommandName="Delete" Text="Eliminar" />--%>
<%--            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                CommandName="New" Text="Nuevo" />--%>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSpdadetalle" runat="server" 
        DeleteMethod="pda_cursoDelete" InsertMethod="pda_cursoInsert" 
        OldValuesParameterFormatString="{0}" 
        SelectMethod="pda_cursoSelectByID" 
        TypeName="com.paginar.johnson.BL.ControllerPDA" UpdateMethod="pda_cursoUpdate">
        <DeleteParameters>
            <asp:Parameter Name="idCurso" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="idEstado" Type="Int32" />
            <asp:Parameter Name="Copete" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="PeriodoTentativo" Type="String" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVpda" Name="CursoId" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="idCurso" Type="Int32" />
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="idEstado" Type="Int32" />
            <asp:Parameter Name="Copete" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="PeriodoTentativo" Type="String" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    </asp:Panel> 
    </div>
</asp:Content>
