﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="PoliticasdeEmpresaABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.PoliticasdeEmpresaABM" %>

<%@ Register Assembly="com.paginar.johnson.Web" Namespace="com.paginar.johnson.Web"
    TagPrefix="cc3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register assembly="com.paginar.johnson.BL" namespace="com.paginar.HierarchicalDataSource" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <h2>Política de la Empresa</h2>

    <div id="accordion">
        <h3><a href="#">Contenido</a></h3>     
        <div class="panelColOne">
            <!--[contenido]-->
            <asp:TreeView ID="TreeViewContenido" runat="server" 
            DataSourceID="RelationalSystemDS" 
                        onselectednodechanged="TreeViewContenido_SelectedNodeChanged" NodeWrap="true">
                <DataBindings>
                        <asp:TreeNodeBinding TextField="Name" />
                    </DataBindings>
                    <SelectedNodeStyle Font-Bold="True" />
            </asp:TreeView>
            <cc1:RelationalSystemDataSource runat="server"  ID="RelationalSystemDS" IncludeRoot="true"/>
            <asp:Button ID="ButtonCrearHijo" runat="server" Text="Nuevo" onclick="ButtonCrearHijo_Click" />
            <!--[/contenido]-->
        </div>
        <h3><a href="#">Nuevo/Modificar</a></h3> 
        <div class="panelColTwo">
            <!--[contenido]-->
            <div class="box">
                <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
                <asp:HiddenField ID="HiddenFieldContentTipoID" runat="server" />
                
                <asp:FormView ID="FormViewContenidos" runat="server" DataKeyNames="Content_ItemId"
                    DataSourceID="ObjectDataSource1" OnItemUpdated="FormViewContenidos_ItemUpdated"
                    OnDataBound="FormViewContenidos_DataBound" OnItemUpdating="FormViewContenidos_ItemUpdating"
                    OnItemInserting="FormViewContenidos_ItemInserting" OnItemInserted="FormViewContenidos_ItemInserted"
                    OnItemDeleted="FormViewContenidos_ItemDeleted">
                    
                    <EditItemTemplate>
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="BtnActualizar">
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Content_ItemId") %>' />
                        <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Editar" />                        
                        <div class="form-item full">
                            <label>Contenido Superior</label>
                            <asp:DropDownList ID="parentIdDropDownList" runat="server" Enabled="false"></asp:DropDownList>
                        </div>
                        <div class="form-item full">
                            <label>Titulo</label>
                            <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' 
                                TextMode="SingleLine" MaxLength="65" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Agregar Titulo" Display="Dynamic"
                                ControlToValidate="TituloTextBox" ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item full">
                            <label>Contenido</label>
                            <cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Contenido") %>'  />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Agregar Contenido" Display="Dynamic"
                                        ControlToValidate="Editor1" ValidationGroup="Editar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="controls">
                            <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar" CommandName="Update" ValidationGroup="Editar" />
                            <asp:Button ID="Button1" runat="server" CommandName="Delete" Text="Eliminar" OnClientClick="return confirm('Desea eliminar este registro de forma permanente?');" />
                            <asp:Button ID="Button3" runat="server" CausesValidation="False" Text="Cancelar" CommandName="Cancel" />
                        </div>
                        
                        </asp:Panel>
                    </EditItemTemplate>

                    <InsertItemTemplate>
                    <asp:Panel ID="Panel2" runat="server" DefaultButton="BtnAgregar">
                        <asp:ValidationSummary ShowMessageBox="true" ShowSummary="false" ID="ValidationSummary1" runat="server" ValidationGroup="Agregar" />
                        <div class="form-item full">
                            <label>Contenido Superior</label>
                            <asp:DropDownList ID="parentIdDropDownList" runat="server" Enabled="false"></asp:DropDownList>
                        </div>
                        <div class="form-item full">
                            <label>Titulo</label>
                            <asp:TextBox ID="TituloTextBox" runat="server" Text='<%# Bind("Titulo") %>' 
                                TextMode="SingleLine" MaxLength="65" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Agregar Titulo" Display="Dynamic"
                                ControlToValidate="TituloTextBox" ValidationGroup="Agregar"
                                ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="form-item full">
                            <label>Contenido</label>
                            <cc2:Editor ID="Editor1" runat="server" Content='<%# Bind("Contenido") %>' />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Agregar Contenido" Display="Dynamic"
                                ControlToValidate="Editor1" ValidationGroup="Agregar" ForeColor="#FF3300">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="controls">
                            <asp:Button ID="BtnAgregar" runat="server" Text="Nuevo" CommandName="Insert" ValidationGroup="Agregar" />
                            <asp:Button ID="Button3" runat="server" CausesValidation="False" Text="Cancelar" CommandName="Cancel" />
                        </div>
                        
                        </asp:Panel>
                    </InsertItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetContentById"
                    TypeName="com.paginar.johnson.BL.ControllerContenido" UpdateMethod="UpdateContent_Items"
                    InsertMethod="InsertContent_Items" DeleteMethod="DeleteContent_Items">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Content_ItemId" Type="Int32" />
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Contenido" Type="String" />
                        <asp:Parameter Name="parentId" Type="Int32" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="Content_ItemId" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Titulo" Type="String" />
                        <asp:Parameter Name="Contenido" Type="String" />
                        <asp:Parameter Name="parentId" Type="Int32" />
                        <asp:Parameter Name="Content_TipoID" Type="Int32" />
                    </InsertParameters>
                </asp:ObjectDataSource>
            
            </div><!--/formulario-->

            <!--[/contenido]-->
        </div>

    </div><!--/panel2Col-->

</asp:Content>
