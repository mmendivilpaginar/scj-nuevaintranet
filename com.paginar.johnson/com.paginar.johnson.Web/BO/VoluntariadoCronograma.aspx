﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="VoluntariadoCronograma.aspx.cs" Inherits="com.paginar.johnson.Web.BO.VoluntariadoCronograma" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
<h2>Cronograma de Actividades</h2>
    <asp:GridView ID="GVCronograma" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataKeyNames="IdCronograma" 
        DataSourceID="ODSGVCronograma" 
        onselectedindexchanged="GVCronograma_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Seleccionar" />
            <asp:BoundField DataField="IdCronograma" HeaderText="IdCronograma" 
                InsertVisible="False" ReadOnly="True" SortExpression="IdCronograma" 
                Visible="False" />
            <asp:TemplateField HeaderText="Actividad" SortExpression="Actividad">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Actividad") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Actividad") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Estado") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# devuelveEstado(Eval("Estado")) %>' ></asp:Label>
                    <%--<asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Estado") %>' 
                        Enabled="false" />--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No hay Cronogramas Dispobibles en este momento.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSGVCronograma" runat="server" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="CronogramaSelectAll" 
        TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
    </asp:ObjectDataSource>
    <asp:Button ID="btnAgregarCronograma" runat="server" 
        onclick="btnAgregarCronograma_Click" Text="Nuevo" />
<br /><br />

    <asp:FormView ID="FVCronograma" runat="server" DataKeyNames="IdCronograma" 
        DataSourceID="ODSFVCronograma" onitemcreated="FVCronograma_ItemCreated" 
        onitemdeleted="FVCronograma_ItemDeleted" 
        oniteminserted="FVCronograma_ItemInserted" 
        onitemupdated="FVCronograma_ItemUpdated">
        <EditItemTemplate>
<%--            IdCronograma:<br />
            <asp:Label ID="IdCronogramaLabel1" runat="server" 
                Text='<%# Eval("IdCronograma") %>' />--%>
            <br />
            <strong>Actividad:</strong><br />
            <asp:TextBox ID="ActividadTextBox" runat="server" 
                Text='<%# Bind("Actividad") %>' />
            <br />
            <strong>Estado:</strong><br />
            <asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' />
            (Seleccione para activar)<br />&nbsp;<asp:LinkButton ID="UpdateButton" 
                runat="server" CausesValidation="True" 
                CommandName="Update" Text="Actualizar" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            <asp:Panel ID="Panel4" runat="server" DefaultButton="UpdateButton">

            </asp:Panel>
        </EditItemTemplate>
        <InsertItemTemplate>
        <asp:Panel ID="Panel3" runat="server" DefaultButton="UpdateButton">
            <strong>Actividad:</strong><br />
            <asp:TextBox ID="ActividadTextBox" runat="server" 
                Text='<%# Bind("Actividad") %>' />
            <br />
            <strong>Estado:</strong><br />
            <asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' />
            (Seleccione para activar)<br />&nbsp;<asp:LinkButton ID="UpdateButton" 
                runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Agregar" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            
            </asp:Panel>
        </InsertItemTemplate>
        <ItemTemplate>
<%--            IdCronograma:<br />
            <asp:Label ID="IdCronogramaLabel" runat="server" 
                Text='<%# Eval("IdCronograma") %>' />
            <br />--%>
            <strong>Actividad:</strong>
            <asp:Label ID="ActividadLabel" runat="server" Text='<%# Bind("Actividad") %>' />
            <br />
            <strong>Estado</strong>
            <asp:Label ID="Label2" runat="server" Text='<%# devuelveEstado(Eval("Estado")) %>' ></asp:Label>
            <%--<asp:CheckBox ID="EstadoCheckBox" runat="server" 
                Checked='<%# Bind("Estado") %>' Enabled="false" />--%>


            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                CommandName="Edit" Text="Editar" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                CommandName="Delete" Text="Borrar" OnClientClick="return confirm('Eliminando esta actividad tembien eliminará las fechas que esta pudiera tener asociadas. \n¿Desea Continuar?')" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                CommandName="New" Text="Nuevo" />
            <br /><br />
            <asp:Button ID="btnAgregarFecha" runat="server" Text="Agregar Fecha" 
                onclick="btnAgregarFecha_Click" />
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSFVCronograma" runat="server" 
        DeleteMethod="CronogramaDelete" InsertMethod="CronogramaInsert" SelectMethod="CronocramaSelect" 
        TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
        UpdateMethod="CronogramaUpdate">
        <DeleteParameters>
            <asp:Parameter Name="IdCronograma" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Actividad" Type="String" />
            <asp:Parameter Name="Estado" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVCronograma" Name="IdCronograma" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="IdCronograma" Type="Int32" />
            <asp:Parameter Name="Actividad" Type="String" />
            <asp:Parameter Name="Estado" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>

    <asp:HiddenField ID="HFIdCronograma" runat="server" />
    <br /><br />
    <asp:HiddenField ID="HFTituloFechas" runat="server" />
    <h3><asp:Label ID="lblTituloFechas" runat="server"></asp:Label></h3>
    <asp:GridView ID="GVCronogramaFecha" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="IdCronogramaFecha" DataSourceID="ODSGVCronogramaFecha" 
        AllowPaging="True">
        <Columns>
            <asp:CommandField ShowSelectButton="True" SelectText="Seleccionar" />
            <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" 
                DataFormatString="{0:d}" />
            <asp:TemplateField HeaderText="TBD" SortExpression="TBD">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("TBD") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# devuelveTBD(Eval("TBD")) %>' ></asp:Label>
                    <%--<asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("TBD") %>' 
                        Enabled="false" />--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSGVCronogramaFecha" runat="server" 
        DeleteMethod="CronogramaFechaDelete" InsertMethod="CronogramaFechaInsert" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="CronogramaFechaByCronSelect" 
        TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
        UpdateMethod="CronogramaFechaUpdate">
        <DeleteParameters>
            <asp:Parameter Name="IdCronogramaFecha" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="IdCronograma" Type="Int32" />
            <asp:Parameter Name="Fecha" Type="String" />
            <asp:Parameter Name="TBD" Type="Boolean" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVCronograma" Name="IdCronograma" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="IdCronogramaFecha" Type="Int32" />
            <asp:Parameter Name="IdCronograma" Type="Int32" />
            <asp:Parameter Name="Fecha" Type="String" />
            <asp:Parameter Name="TBD" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <br /><br />
    <asp:FormView ID="FVFechas" runat="server" DataKeyNames="IdCronogramaFecha" 
                DataSourceID="FVCronogramasFechas" ondatabound="FVFechas_DataBound" 
                onitemcreated="FVFechas_ItemCreated" onitemdeleted="FVFechas_ItemDeleted" 
                oniteminserted="FVFechas_ItemInserted" onitemupdated="FVFechas_ItemUpdated">
            <EditItemTemplate>
            <asp:Panel ID="Panel2" runat="server" DefaultButton="UpdateButton">
                <asp:HiddenField ID="IdCronogramaFechaLabel1" Value='<%# Eval("IdCronogramaFecha") %>' runat="server" />
             <%--     IdCronogramaFecha:<br />
                
              <asp:Label ID="IdCronogramaFechaLabel1" runat="server" 
                    Text='<%# Eval("IdCronogramaFecha") %>' />
                <br />--%>
                <asp:HiddenField ID="IdCronogramaTextBox" Value='<%# Bind("IdCronograma") %>' runat="server" />
<%--                IdCronograma:<br />
                <asp:TextBox ID="IdCronogramaTextBox" runat="server" 
                    Text='<%# Bind("IdCronograma") %>' />
                <br />--%>
                <strong>Fecha:</strong> <br />
                <asp:TextBox ID="FechaTextBox" runat="server" Text='<%# Bind("Fecha" , "{0:d}") %>' 
                    SkinID="form-date" />
                <asp:CalendarExtender ID="FechaTextBox_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="FechaTextBox" PopupButtonID="ImageButton1">
                </asp:CalendarExtender>
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="~/images/Calendar.png" />
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="FechaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="" Display="Dynamic">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" 
                                ControlToValidate="FechaTextBox"></asp:RequiredFieldValidator>
                (dd/mm/aaaa)
                <br />
                <strong> TBD:</strong> <br />
                <asp:CheckBox ID="TBDCheckBox" runat="server" Checked='<%# Bind("TBD") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                    CommandName="Update" Text="Actualizar" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                
                </asp:Panel>
            </EditItemTemplate>
            <InsertItemTemplate>
            <asp:Panel ID="Panel1" runat="server" DefaultButton="InsertButton">
            <asp:HiddenField ID="IdCronogramaTextBox" Value='<%# Bind("IdCronograma") %>' runat="server" />
<%--                IdCronograma:
                <asp:TextBox ID="IdCronogramaTextBox" runat="server" 
                    Text='<%# Bind("IdCronograma") %>' />
                <br />--%>
                <strong> Fecha:</strong> <br />
                <asp:TextBox ID="FechaTextBox" runat="server" Text='<%# Bind("Fecha") %>' 
                    SkinID="form-date" />
                <asp:CalendarExtender ID="FechaTextBox_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="FechaTextBox" PopupButtonID="ImageButton1">
                </asp:CalendarExtender>
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="~/images/Calendar.png" />
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="FechaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="" Display="Dynamic">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" 
                    ControlToValidate="FechaTextBox"></asp:RequiredFieldValidator>
                (dd/mm/aaaa)
                <br />
                <strong> TBD:</strong> <br />
                <asp:CheckBox ID="TBDCheckBox" runat="server" Checked='<%# Bind("TBD") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                    CommandName="Insert" Text="Agregar" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                    CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
                
                </asp:Panel>
            </InsertItemTemplate>
            <ItemTemplate>
              <%--  IdCronogramaFecha:
                <asp:Label ID="IdCronogramaFechaLabel" runat="server" 
                    Text='<%# Eval("IdCronogramaFecha") %>' />
                <br />
                IdCronograma:
                <asp:Label ID="IdCronogramaLabel" runat="server" 
                    Text='<%# Bind("IdCronograma") %>' />
                <br />--%>
                <strong> Fecha:</strong>
                <asp:Label ID="FechaLabel" runat="server" Text='<%# Bind("Fecha", "{0:d}") %>' />
                <br />
                <strong>TBD:</strong>
                <asp:Label ID="Label4" runat="server" Text='<%# devuelveTBD(Eval("TBD")) %>' ></asp:Label>
                <%--<asp:CheckBox ID="TBDCheckBox" runat="server" Checked='<%# Bind("TBD") %>' 
                    Enabled="false" />--%>
                <br />
                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                    CommandName="Edit" Text="Editar" />
                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                    CommandName="Delete" Text="Borrar" />
                &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                    CommandName="New" Text="Nuevo" />
            </ItemTemplate>
        </asp:FormView>
        <asp:ObjectDataSource ID="FVCronogramasFechas" runat="server" 
            DeleteMethod="CronogramaFechaDelete" InsertMethod="CronogramaFechaInsert" 
            SelectMethod="CronogramaFechaSelect" 
            TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
            UpdateMethod="CronogramaFechaUpdate">
            <DeleteParameters>
                <asp:Parameter Name="IdCronogramaFecha" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="IdCronograma" Type="Int32" />
                <asp:Parameter Name="Fecha" Type="String" />
                <asp:Parameter Name="TBD" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GVCronogramaFecha" Name="IdCronogramaFecha" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="IdCronogramaFecha" Type="Int32" />
                <asp:Parameter Name="IdCronograma" Type="Int32" />
                <asp:Parameter Name="Fecha" Type="String" />
                <asp:Parameter Name="TBD" Type="Boolean" />
            </UpdateParameters>
        </asp:ObjectDataSource>
</asp:Content>
