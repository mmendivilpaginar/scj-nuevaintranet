﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class VoluntariadoCronograma : System.Web.UI.Page
    {
        ControllerVoluntariado CV = new ControllerVoluntariado();
         
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAgregarCronograma_Click(object sender, EventArgs e)
        {
            FVCronograma.ChangeMode(FormViewMode.Insert);
        }


        protected void GVCronograma_SelectedIndexChanged(object sender, EventArgs e)
        {
            HFIdCronograma.Value = ((GridView)sender).SelectedDataKey.Value.ToString();
            DSVoluntariado.vol_CronogramaDataTable DTCronograma = new DSVoluntariado.vol_CronogramaDataTable();
            DTCronograma = CV.CronocramaSelect(int.Parse(((GridView)sender).SelectedDataKey.Value.ToString()));           
            lblTituloFechas.Text = "Fechas definidas para : " + DTCronograma.Rows[0][1].ToString();
            FVCronograma.ChangeMode(FormViewMode.ReadOnly);
            FVCronograma.DataBind();
                

            FVFechas.ChangeMode(FormViewMode.ReadOnly);
            FVFechas.DataBind();
            

        }

        protected void btnAgregarFecha_Click(object sender, EventArgs e)
        {
            FVFechas.ChangeMode(FormViewMode.Insert);            
        }

        protected void FVFechas_DataBound(object sender, EventArgs e)
        {
            if (((FormView)sender).CurrentMode == FormViewMode.Insert || ((FormView)sender).CurrentMode == FormViewMode.Edit)
            {
                if (HFIdCronograma.Value != null)
                    ((HiddenField)FVFechas.FindControl("IdCronogramaTextBox")).Value = HFIdCronograma.Value;
            }
        }

        protected void FVFechas_ItemCreated(object sender, EventArgs e)
        {
            GVCronogramaFecha.DataBind();
        }

        protected void FVFechas_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            GVCronogramaFecha.DataBind();
        }

        protected void FVFechas_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVCronogramaFecha.DataBind();
        }

        protected void FVFechas_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVCronogramaFecha.DataBind();
        }

        protected void FVCronograma_ItemCreated(object sender, EventArgs e)
        {
            GVCronograma.DataBind();
        }

        protected void FVCronograma_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            HFIdCronograma.Value = null;
            GVCronograma.DataBind();
            FVFechas.ChangeMode(FormViewMode.ReadOnly);
            GVCronogramaFecha.DataBind();
            lblTituloFechas.Text = "";
        }

        protected void FVCronograma_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVCronograma.DataBind();
        }

        protected void FVCronograma_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVCronograma.DataBind();
        }

        protected string devuelveEstado(object estado)
        {
            bool status = bool.Parse(estado.ToString());
            if (status) return "Activo";
            return "No Activo";
        }

        protected string devuelveTBD(object esTBD)
        {
            bool statusTBD = bool.Parse(esTBD.ToString());
            if (statusTBD) return "Fecha a Definir - TBD";
            return "Fecha Estimada";
        
        }
    }
}