﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.BO
{
    public partial class IndumentariaAdministraciondePeriodos : System.Web.UI.Page
    {
        ControllerIndumentaria CI = new ControllerIndumentaria();
        ControllerTramites CT = new ControllerTramites();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            ltrlErrorMsg.Text = "";
            FVPeriodo.ChangeMode(FormViewMode.Insert);
        }

        protected void FVPeriodo_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GVPeriodos.DataBind();
            GVPeriodos.SelectedIndex = -1;
            FVPeriodo.DataBind();
            blFeedback.Text = "El período fue creado de forma exitosa.";
        }

        protected void FVPeriodo_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVPeriodos.DataBind();
            GVPeriodos.SelectedIndex = -1;
            FVPeriodo.DataBind();
            blFeedback.Text = "<div class=\"messages msg-exito\">El período fue actualizado de forma exitosa.</div>";
        }

        protected void GVPeriodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            HFPeriodoActual.Value = ((GridView)sender).SelectedValue.ToString();
            blFeedback.Text = "";

        }

        protected void GVPeriodos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Clonar")
            {
                FVPeriodo.ChangeMode(FormViewMode.Insert);
                FVPeriodo.DataBind();
                ((LinkButton)FVPeriodo.FindControl("InsertButton")).Visible = false;
                ((LinkButton)FVPeriodo.FindControl("CloneButton")).Visible = true;
                HFClone.Value = e.CommandArgument.ToString();
                DSIndumentaria.Indumentaria_PeriodoDataTable DSPeriodoOriginal = CI.IndumentariaPeriodoSeleccionarPorId(int.Parse(e.CommandArgument.ToString()));
                ((TextBox)FVPeriodo.FindControl("DescripcionTextBox")).Text = "";
                ((TextBox)FVPeriodo.FindControl("DesdeTextBox")).Text = DateTime.Parse(DSPeriodoOriginal.Rows[0][1].ToString()).ToShortDateString();
                ((TextBox)FVPeriodo.FindControl("HastaTextBox")).Text = DateTime.Parse(DSPeriodoOriginal.Rows[0][2].ToString()).ToShortDateString();
            }
        }

        protected bool estaDentroDePeiodosAnteriores(DateTime FechaDesde, DateTime FechaHasta, bool inserta)
        {
            bool valorRetorno = false;

            DSIndumentaria.Indumentaria_PeriodoDataTable PI = CI.SeleccionarPeriodosTodos();

            foreach (DSIndumentaria.Indumentaria_PeriodoRow item in PI.Rows)
            {
                if (inserta)
                {
                    if ((DateTime.Compare(FechaDesde, item.Desde) >= 0 && DateTime.Compare(FechaDesde, item.Hasta) <= 0) || (DateTime.Compare(FechaHasta, item.Desde) >= 0 && DateTime.Compare(FechaHasta, item.Hasta) <= 0))
                    {
                        valorRetorno = true;
                    }
                }
                else
                {
                    if (HFPeriodoActual.Value == item.PeriodoId.ToString())
                        continue;

                    if ((DateTime.Compare(FechaDesde, item.Desde) >= 0 && DateTime.Compare(FechaDesde, item.Hasta) <= 0) || (DateTime.Compare(FechaHasta, item.Desde) >= 0 && DateTime.Compare(FechaHasta, item.Hasta) <= 0))
                    {
                        valorRetorno = true;
                    }
                
                }
            }

            return valorRetorno;
        }

        protected void FVPeriodo_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName != "Cancel" && e.CommandName != "Edit")
            {
                if (Page.IsValid)
                {
                    if (e.CommandName == "Clonar")
                    {
                        int tramiteID;
                        int his_secuencia;
                        int idNuevoPeriodo = CI.IndumentariaPeriodoInsertar(DateTime.Parse(((TextBox)FVPeriodo.FindControl("DesdeTextBox")).Text), DateTime.Parse(((TextBox)FVPeriodo.FindControl("HastaTextBox")).Text), ((TextBox)FVPeriodo.FindControl("DescripcionTextBox")).Text);

                        DSIndumentaria.frmTramiteDataTable TramitesaClonar = CI.IndumentariaTramiteSeleccionarPeriodoId(int.Parse(HFClone.Value));

                        foreach (DSIndumentaria.frmTramiteRow item in TramitesaClonar.Rows)
                        {
                            tramiteID = int.Parse(CI.tramite_alta_Clon(item.tra_UsrAlta, "ALTA", item.tra_UsrAlta, item.tra_Apellido, item.tra_Nombre, item.tra_Legajo, "", "").ToString());
                            his_secuencia = 1;
                            CI.IndumentariaActualizarPasoTramite(tramiteID, 1);
                            CT.InsertarPasoNuevoHistorico(tramiteID, item.tra_UsrAlta, item.tra_UsrAlta, "E", 1, "", his_secuencia);

                            DSIndumentaria.IndumentariaPedidoDataTable PedidoClonar = CI.IndumentariaPedidoSeleccionar(item.tra_ID);
                            foreach (DSIndumentaria.IndumentariaPedidoRow itemPedido in PedidoClonar.Rows)
                            {
                                CI.IndumentariaPedidoInsertar(tramiteID, idNuevoPeriodo, itemPedido.IndumentariaId, itemPedido.Talle, itemPedido.esBrigadista);
                            }
                        }

                        GVPeriodos.DataBind();
                        GVPeriodos.SelectedIndex = -1;
                        FVPeriodo.DataBind();
                    }
                }
            }
        }

        protected bool verificarPeriodo(object FechaFin)
        {
            bool esVisible = false;

            IFormatProvider culture = new System.Globalization.CultureInfo("es-AR", true);
            DateTime Fecha = DateTime.Parse(FechaFin.ToString() + " 00:00:00");
            if (DateTime.Now.CompareTo(Fecha) < 0)
                esVisible = true;
            return esVisible;
        
        }


        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime dt =  DateTime.Parse(((TextBox)FVPeriodo.FindControl("HastaTextBox")).Text.ToString() + " 23:59:59");
            if (DateTime.Now.CompareTo(dt) > 0)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void FVPeriodo_ItemInserting(object sender, FormViewInsertEventArgs e)
        {

            if (estaDentroDePeiodosAnteriores(DateTime.Parse(((TextBox)FVPeriodo.FindControl("DesdeTextBox")).Text), DateTime.Parse(((TextBox)FVPeriodo.FindControl("HastaTextBox")).Text), true))
            {
                e.Cancel = true;
                ltrlErrorMsg.Text = "<div class=\"messages msg-error\">Una de las Fechas que intenta ingresar se encuentra en el rango de otro período.</div>";
            }
            else
                ltrlErrorMsg.Text = "";



        }

        protected void FVPeriodo_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
                        
            if (estaDentroDePeiodosAnteriores(DateTime.Parse(((TextBox)FVPeriodo.FindControl("DesdeTextBox")).Text), DateTime.Parse(((TextBox)FVPeriodo.FindControl("HastaTextBox")).Text), false))
            {
                e.Cancel = true;
                ltrlErrorMsg.Text = "<div class=\"messages msg-error\">Una de las Fechas que intenta actualizar se encuentra en el rango de otro período.</div>";
            }
            else
                ltrlErrorMsg.Text = "";

        }

        protected void InsertCancelButton_Click(object sender, EventArgs e)
        {
            ltrlErrorMsg.Text = "";
        }

        protected void btn_editar_Click(object sender, EventArgs e)
        {            
            ltrlErrorMsg.Text = "";
            
            FVPeriodo.ChangeMode(FormViewMode.Edit);
            FVPeriodo.DataBind();
        }

        protected void ODSPeriodo_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (getPostBackControlName() == "btn_editar")
            {
                string id = CI.GetIdLastPeriodo();
                e.InputParameters["PeriodoId"] = id;
                HFPeriodoActual.Value = id;
            }
        }

        private string getPostBackControlName()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            //return control.ID;
            return control == null ? String.Empty : control.ID;

        }
    }
}