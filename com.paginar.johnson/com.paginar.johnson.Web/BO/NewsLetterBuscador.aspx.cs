﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Text;
using System.Configuration;
using com.paginar.formularios.businesslogiclayer;
using System.Net.Mail;

namespace com.paginar.johnson.Web.BO

{
    public partial class NewsLetterBuscador : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Buscar();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewsletterABM.aspx", false);
        }




        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            drpEstado.SelectedIndex = 0;
            txtNombre.Text = "";
            DesdeTextBox.Text = "";
            HastaTextBox.Text = "";


            gvNewsletter.DataSource = null;
            gvNewsletter.DataBind();
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Buscar();
                btnLimpiar.Visible = true;
            }
        }

        private void Buscar()
        {
            ControllerNewsletter cn = new ControllerNewsletter();
            int? estado = null;
            if (drpEstado.SelectedValue != "-1")
                estado = int.Parse(drpEstado.SelectedValue); 

            DateTime? fhdesde = null;
            if (DesdeTextBox.Text.Trim() != "")
                fhdesde = DateTime.Parse(DesdeTextBox.Text.Trim());

            DateTime? fhhasta = null;
            if (HastaTextBox.Text.Trim() != "")
                fhhasta = DateTime.Parse(HastaTextBox.Text.Trim());

            gvNewsletter.DataSource = cn.Buscar(estado, fhdesde, fhhasta, txtNombre.Text.Trim());
            gvNewsletter.DataBind();

            
        }


        private string formatFecha(int dia, int mes, int anio)
        {
            string strmes = "";
            switch (mes)
            {
                case 1:
                    strmes = "Enero";
                    break;
                case 2:
                    strmes = "Febrero";
                    break;
                case 3:
                    strmes = "Marzo";
                    break;
                case 4:
                    strmes = "Abril";
                    break;
                case 5:
                    strmes = "Mayo";
                    break;

                case 6:
                    strmes = "Junio";
                    break;

                case 7:
                    strmes = "Julio";
                    break;

                case 8:
                    strmes = "Agosto";
                    break;

                case 9:
                    strmes = "Septiembre";
                    break;

                case 10:
                    strmes = "Octubre";
                    break;

                case 11:
                    strmes = "Noviembre";
                    break;

                case 12:
                    strmes = "Diciembre";
                    break;

            }



            return dia + " de " + strmes + " de " + anio;
        }

        protected string GetUrlSite()
        {
            string url = this.ASP_SITE.ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return this.ASP_SITE.ToString().Replace(":8080", "");//produccion
              
        }


  

        protected void gvNewsletter_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Preview")
            {
                
                ControllerNewsletter cn = new ControllerNewsletter();
                int newsletterid = int.Parse(e.CommandArgument.ToString());

                DSNewsLetter.news_NewsLetterDataTable dt = cn.getNewsletterByID(newsletterid);

                string nro = int.Parse(dt.Rows[0]["titulo"].ToString().Substring(0, 5)).ToString();


                DSNewsLetter.news_RInfInfoNewsLetterDataTable dn = cn.getNoticiasByID(newsletterid);
                string noticiasids = "";

                foreach (DSNewsLetter.news_RInfInfoNewsLetterRow  dr in dn.Rows)
                {
                    if (noticiasids != "") noticiasids += ",";
                    noticiasids += dr["infoid"].ToString();
                }


                hdPreview.Text = "<iframe src='NewsLetterVistaPrevia.aspx?NotidiasIDs=" + noticiasids + "&Fecha=" + dt.Rows[0]["fechaenvio"].ToString() + "&Nro=" + nro + "  ' frameBorder='0' width='835' height='600' style='background: #FFF'/>  ";

                EjecutarScript("$(document).ready(function () { NewsletterPreview(); });");

            }

            if (e.CommandName == "Editar")
            {
                Response.Redirect("NewsLetterABM.aspx?newsletterid=" + e.CommandArgument);
            }

            if (e.CommandName == "Borrar")
            {
                ControllerNewsletter cn = new ControllerNewsletter();
                cn.BorrarNewsletter(int.Parse(e.CommandArgument.ToString()));
                Response.Redirect("NewsLetterBuscador.aspx", false);
            }


            if (e.CommandName == "Enviar")
            {
                ControllerNewsletter cn = new ControllerNewsletter();
                NotificacionesController n = new NotificacionesController();
                int newsletterID = int.Parse(e.CommandArgument.ToString());

                DSNewsLetter.news_NewsLetterDataTable dt = cn.getNewsletterByID(newsletterID);
                string nro = int.Parse(dt.Rows[0]["titulo"].ToString().Substring(0, 5)).ToString();
                string fecha = string.Format("{0: dd/MM/yyyy}", dt.Rows[0]["fechaenvio"]);
                    
                if (dt.Rows[0]["EstadoEnvio"].ToString() == "False")
                {
                    string path = Server.MapPath("~");
                    AlternateView htmlView=null;
                    string body = cn.getBodyNewsletter(GetUrlSite(), cn.getNoticias("100", 0, "", "", newsletterID), fecha, nro, path, false, ref htmlView).ToString();

                    if (ConfigurationManager.AppSettings["DEBUG_MODE"] == "false")
                    {
                        string destinatarios = "";
                        DSNewsLetter.news_DestinatariosDataTable dtDestinatarios =  cn.getDestinatarioByID(newsletterID);
                        foreach (DSNewsLetter.news_DestinatariosRow dr in dtDestinatarios.Rows)
                        {
                            if (destinatarios != "") destinatarios += ",";
                            destinatarios += dr.email;
                        }

                        n.EnvioMailNewsletter("G057040@scj.com", destinatarios, "SCJ News", htmlView);
                    }
                    else
                    {
                        n.EnvioMailNewsletter("G057040@scj.com", "evaluador@mom", "SCJ News", htmlView);
                    }

                    cn.ActualizaNewsletter(newsletterID);

                    Buscar();

                    EjecutarScript("$(document).ready(function() { alert('Envío Realizado'); });");
                    

                }
                else
                    EjecutarScript("alert('No puede realizar esta acción');");
            }

        }


        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }


        protected void cvBuscar_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if ((drpEstado.SelectedValue !="") || (txtNombre.Text.Trim() != "")
                || (DesdeTextBox.Text.Trim() != "") || (HastaTextBox.Text.Trim() != ""))

                args.IsValid = true;
            else
                args.IsValid = false;
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            
           EjecutarScript(" window.open('ImprimirNewsletter.aspx?Estado="+drpEstado.SelectedValue+"&Nombre="+txtNombre.Text+"&FHDesde="+DesdeTextBox.Text+"&FHHasta="+HastaTextBox.Text+"');");
            

        }
    }
}
