﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class EL_CursosABM : System.Web.UI.Page
    {

      


        protected void Page_Load(object sender, EventArgs e)
        {
            this.TextBoxCodigo.Attributes.Add("onkeypress", "button_click(this,'" + this.ButtonBuscarCurso.ClientID + "')");
            this.TextBoxDescripcion.Attributes.Add("onkeypress", "button_click(this,'" + this.ButtonBuscarCurso.ClientID + "')");

        }

        protected void ButtonNuevoCurso_Click(object sender, EventArgs e)
        {
            
                MultiView1.ActiveViewIndex = 1;
                FormViewCurso.ChangeMode(FormViewMode.Insert);
           
        }

        protected void GridViewEL_Cursos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            this.MaintainScrollPositionOnPostBack = true;
            if (e.CommandName == "Eliminar")
            {
                ControllerE_Learning ControllerEL = new ControllerE_Learning();
                ControllerEL.EL_CursoDelete(int.Parse(e.CommandArgument.ToString()));
                GridViewEL_Cursos.DataBind();
                
            }

            if (e.CommandName == "Edit")
            {
                
            }


            if (e.CommandName == "Select")
            {
                MultiView1.ActiveViewIndex = 1;
                FormViewCurso.ChangeMode(FormViewMode.Edit);
                //FormViewCurso.DataBind();
            }

            if (e.CommandName == "Cancel")
            {

                MultiView1.ActiveViewIndex = 0;
                GridViewEL_Cursos.DataBind();

            }
        }

        protected void FormViewCurso_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (Page.IsValid)
            {
                MultiView1.ActiveViewIndex = 0;
                GridViewEL_Cursos.DataBind();
            }
        }

        protected void FormViewCurso_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {

                MultiView1.ActiveViewIndex = 0;
                GridViewEL_Cursos.DataBind();

            }

           

        }

        protected void FormViewCurso_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (Page.IsValid)
            {
                MultiView1.ActiveViewIndex = 0;
                GridViewEL_Cursos.DataBind();
            }
        }

        protected void ButtonBuscarCurso_Click(object sender, EventArgs e)
        {
            ObjectDataSourceCursos.DataBind();
            GridViewEL_Cursos.DataBind();
           
        }

        protected void CustomValidatorCodigo_ServerValidate(object source, ServerValidateEventArgs args)
        {


            string Codigo = ((TextBox)FormViewCurso.FindControl("EstadoTextBox")).Text;
            int? CursoID = null;

            if(!string.IsNullOrEmpty(((HiddenField)FormViewCurso.FindControl("HiddenFieldCursoID")).Value))
                CursoID= int.Parse(((HiddenField)FormViewCurso.FindControl("HiddenFieldCursoID")).Value);

            ControllerE_Learning ControllerEL = new ControllerE_Learning();
            if (ControllerEL.ExisteCodigo(Codigo,CursoID) == "true")            
               args.IsValid = false;
            else
                args.IsValid = true;


        }
    }
}