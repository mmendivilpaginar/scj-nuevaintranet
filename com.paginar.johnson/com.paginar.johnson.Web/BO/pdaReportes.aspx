﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="pdaReportes.aspx.cs" Inherits="com.paginar.johnson.Web.BO.pdaReportes" %>
<%@ Register assembly="Microsoft.Web.DynamicData" namespace="Microsoft.Web.DynamicData" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
    function popupReporteDirArea(url, ancho, alto) {
        var posicion_x;
        var posicion_y;
        posicion_x = (screen.width / 2) - (ancho / 2);
        posicion_y = (screen.height / 2) - (alto / 2);
        window.open(url, "Inscriptos", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

    }

    function selectaAllCursos(listid) { 
        for(i=0;i<ListBox1.Items.Count;i++)
        {
            $(listid).Items[i].Selected = true;
        }    
    }
</script>
<style type="text/css">
.AspNet-GridView
{
    margin-bottom: 0px !important;
}
.AspNet-GridView .AspNet-GridView
{
    margin-bottom: 0px !important;
}
.wrappreppda
{
    margin-bottom: 10px !important;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperIndumentaria">
<h2>Reportes cursos PDA
        
        </h2>
<asp:ObjectDataSource ID="ODScursos" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="pda_cursoSelectByIdReporte" 
            TypeName="com.paginar.johnson.BL.ControllerPDA">
            <SelectParameters>
                <asp:ControlParameter ControlID="HFCursosSelect" Name="idCurso" 
                    PropertyName="Value" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
        

        <asp:DropDownList ID="DDLCursos" runat="server" 
            DataTextField="Nombre" DataValueField="idCurso" 
            AppendDataBoundItems="True" Visible="False">
            <asp:ListItem Value="">Todos</asp:ListItem>
        </asp:DropDownList>
   <div class="form-item leftHalf" style="height:100px !important">
    <label>Tipo de Reporte</label>
        <asp:DropDownList ID="DDTipodeReporte" runat="server" AutoPostBack="True" 
            onselectedindexchanged="DDTipodeReporte_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="1">Solicitudes de inscripción por Cursos</asp:ListItem>
            <asp:ListItem Value="2">Solicitudes de inscripción por Dirección / Área</asp:ListItem>
            <asp:ListItem Value="3">Consulta por usuario</asp:ListItem>
        </asp:DropDownList> 
    </div>
    <div class="form-item rightHalf" style="height:100px !important">
        <label>Curso</label>
                
        

        <asp:ListBox ID="lbCursos" runat="server" SelectionMode="Multiple">
        </asp:ListBox><br />
        <asp:LinkButton ID="lnkselectall" runat="server" onclick="lnkselectall_Click">Seleccionar todo</asp:LinkButton>&nbsp;/&nbsp;<asp:LinkButton 
            ID="lnkdeselectall" runat="server" onclick="lnkdeselectall_Click">Quitar seleccionados</asp:LinkButton>
        <asp:HiddenField ID="HFCursosSelect" runat="server" />
        

    </div>
        
    <div class="form-item leftHalf">
    <label>Dirección</label>
        <asp:DropDownList ID="DDLDireccion" runat="server" DataTextField="DireccionDET" 
            DataValueField="DireccionID" AppendDataBoundItems="True" 
            AutoPostBack="True">
            <asp:ListItem Selected="True">Seleccionar</asp:ListItem>
        </asp:DropDownList>
        
    &nbsp;</div>
    <div class="form-item rightHalf">
    <label>Área</label>
        <asp:DropDownList ID="DDLAreas" runat="server" 
            DataTextField="AreaDESC" DataValueField="AreaID">
            <asp:ListItem Selected="True">Seleccionar</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DDLEstado" runat="server" DataSourceID="ODSEstado" Style="display:none"
            DataTextField="Descripcion" DataValueField="idEstado" 
            AppendDataBoundItems="True">
        <asp:ListItem Value="">Todos</asp:ListItem>
    </asp:DropDownList>
        <asp:ObjectDataSource ID="ODSEstado" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="pda_estados" 
            TypeName="com.paginar.johnson.BL.ControllerPDA"></asp:ObjectDataSource>
    </div>

        
    <div class="form-item leftHalf">
        <label>Año</label>
        <asp:TextBox ID="tbAnio" runat="server"></asp:TextBox>
        <asp:FilteredTextBoxExtender ID="tbAnio_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbAnio">
        </asp:FilteredTextBoxExtender>
    </div>
    <div class="form-item form-end">
      <asp:Button ID="Filtrar" runat="server" Text="Buscar" 
            onclick="Filtrar_Click" />&nbsp;
        <asp:Button ID="btnExportar" runat="server" onclick="btnExportar_Click" 
        Text="Exportar" />
        <asp:HyperLink ID="hlImprimir" runat="server" Target="_blank"><asp:Image ID="imgPrint" runat="server" ImageUrl="~/images/printer.png"  ToolTip="Imprimir Reporte"/></asp:HyperLink>

        <asp:Literal ID="ltlImprimir" runat="server"></asp:Literal>
        </div>
    <div class="form-item rightHalf">
  
    </div>
    <div>
        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
    </div>
    <h3><asp:Label ID="lblTituloReporte" runat="server" Text=""></asp:Label></h3>

<%--solicitudes de inscripción por curso--%>
    <asp:MultiView ID="MVReportesPDA" runat="server">    
        <asp:View ID="VInscPorCurso" runat="server">
        
        
            <asp:Repeater ID="RptInscPorCursos" runat="server" DataSourceID="ODScursos" 
                onitemdatabound="RptInscPorCursos_ItemDataBound">
                <HeaderTemplate>
                <h3>Solicitudes de inscripción por Cursos</h3>
                <br />
                    <asp:Literal ID="ltrlFecha" runat="server"></asp:Literal>
                </HeaderTemplate>
            <ItemTemplate>
                <asp:HiddenField ID="HFidCurso" runat="server" Value='<%# Eval("idCurso") %>' />
                <div class="AspNet-GridView wrappreppda">
                <table width="100%">
                <thead>
                <tr class="AspNet-GridView-Header">
                <td colspan="6"><b><asp:Label ID="lblNombreCurso" runat="server" Text='<%# Eval("Nombre") %>' ></asp:Label></b>  (Estado: <asp:Label ID="Label2" runat="server" Text='<%# devolverEstado(Eval("idEstado").ToString()) %>' ></asp:Label>)
                </td>
                </tr>
                </thead>
                 </table>
                 
            <asp:GridView ID="GVInscPorCurso" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="Usuarioid" DataSourceID="ODSReporteInsPorCurso">
                <Columns>
                    <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                        Visible="False" />
                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                        SortExpression="Legajo" />
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                        SortExpression="Apellido" />
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                        SortExpression="Nombre" />
                    <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Inscripcion" 
                        SortExpression="FechaInscripcion" />
                    <asp:BoundField DataField="Direccion" HeaderText="Dirección" ReadOnly="True" 
                        SortExpression="Direccion" />
                    <asp:BoundField DataField="Area" HeaderText="Área" ReadOnly="True" 
                        SortExpression="Area" />
                </Columns>
            </asp:GridView>
            <table>
            <tbody>
            <tr>
            <td colspan="6" align="right">
            <div style="float:right"><b>Subtotal:&nbsp;&nbsp;</b><asp:Label ID="lblCantInscCurso" runat="server" Text='<%# cantidadInscriptos(Eval("idCurso")) %>' ></asp:Label></div>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
            <asp:ObjectDataSource ID="ODSReporteInsPorCurso" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="getReporteInscripcionPorCurso" 
                TypeName="com.paginar.johnson.BL.ControllerPDA">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFidCurso" Name="idCurso" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="DDLEstado" Name="idEstado" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="tbAnio" Name="anio" PropertyName="Text" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
            </ItemTemplate>
            <FooterTemplate>
             <table>    
             <tr>
             <td>
             <asp:Label ID="lblEmptyData"
                    Text="<div class='messages msg-info'>No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.</div>" runat="server" Visible="false">
             </asp:Label>
             </td>
             </tr>
             </table>             
            </FooterTemplate>            
            </asp:Repeater>
            
               
            

                <asp:Literal ID="lblTotalInscriptos" runat="server" Text=""></asp:Literal>

        </asp:View>
        <asp:View ID="VInscAreaDir" runat="server">
        
 
            <asp:Repeater ID="RptAreaDirecion" runat="server" 
                DataSourceID="ODSReporteAreaDireccion" 
                onitemdatabound="RptAreaDirecion_ItemDataBound">
                <HeaderTemplate>
                <h3>Solicitudes de inscripción por Dirección / Área</h3>
                <br />
                <asp:Literal ID="ltrlFecha" runat="server"></asp:Literal>
                </HeaderTemplate>
            <ItemTemplate>                
            <asp:HiddenField ID="HFdirID" runat="server" Value='<%# Eval("direccionid") %>'/>
            <asp:HiddenField ID="HFAreID" runat="server" Value='<%# Eval("areaid") %>'/>
            
                <div class="AspNet-GridView wrappreppda">
                <table width="100%">
                    
                <thead>
                <tr class="AspNet-GridView-Header">
                <td colspan="5"><b><asp:Label ID="lblDireccion" runat="server" Text='<%# Eval("Direccion") %>' ></asp:Label></b>::<b><asp:Label ID="lblarea" runat="server" Text='<%# Eval("Area") %>' ></asp:Label></b>
                </td>
                </tr>
                </thead>
                 </table>
                 <tr>
                 <td colspan="5">
       <asp:GridView ID="GVReporter" runat="server" AutoGenerateColumns="False" 
           DataKeyNames="Usuarioid" DataSourceID="ODSReporte">
           <Columns>
               <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                   InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                   Visible="False" />
               <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                   SortExpression="Legajo" />
               <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                   SortExpression="Apellido" />
               <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                   SortExpression="Nombre" />
               <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Inscripcion" 
                   SortExpression="FechaInscripcion" />
               <asp:BoundField DataField="curso" HeaderText="Curso" ReadOnly="True" 
                   SortExpression="curso" />
           </Columns>
       </asp:GridView>

                 </td>
                 </tr>
            <table>
            <thead>
            <tr>
            <td colspan="5" align="right">
            <div style="float:right"><b>Subtotal:&nbsp;&nbsp;</b><asp:Label ID="Label1" runat="server" Text='<%# procesarCantidadReporte2(Eval("cantidad")) %>'></asp:Label></div>
            </td>
            </tr>
            </thead>
            </table>
            </div>
       <asp:ObjectDataSource ID="ODSReporte" runat="server" 
           OldValuesParameterFormatString="original_{0}" 
           SelectMethod="getDetalleReportePorAreaDireccionCurso" 
           TypeName="com.paginar.johnson.BL.ControllerPDA">
           <SelectParameters>
               <asp:ControlParameter ControlID="HFAreID" Name="AreaID" PropertyName="Value" 
                   Type="Int32" />
               <asp:ControlParameter ControlID="HFdirID" Name="DireccionID" 
                   PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="DDLCursos" Name="idCurso" PropertyName="SelectedValue" 
                   Type="Int32" />
           </SelectParameters>
       </asp:ObjectDataSource> 
            </ItemTemplate>
            <FooterTemplate>
             <table>    
             <tr>
             <td>
             <asp:Label ID="lblEmptyData"
                    Text="<div class='messages msg-info'>No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.</div>" runat="server" Visible="false">
             </asp:Label>

             </td>
             </tr>
             </table>              
            </FooterTemplate>
            </asp:Repeater>


<asp:Literal ID="lblTotalReporte2" runat="server"></asp:Literal>
          
            <!--/div-->
            <asp:ObjectDataSource ID="ODSReporteAreaDireccion" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="getReporteAreaDireccion" 
                TypeName="com.paginar.johnson.BL.ControllerPDA">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLAreas" Name="AreaID" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="DDLDireccion" Name="DireccionID" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="DDLCursos" Name="idCurso" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="tbAnio" Name="anio" PropertyName="Text" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </asp:View>
        <asp:View ID="VConsPorUsuario" runat="server">
        <h3>Consulta por usuario</h3>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnFiltrar">
            
                <div>
                    <div class="form-item leftHalf">
                    <label>Legajo</label>
                        <asp:TextBox ID="tbLegajo" runat="server"></asp:TextBox>
   
                    </div>
                    <div class="form-item rightHalf">
                    <label>Apellido</label>       
                    <asp:TextBox ID="tbApellido" runat="server"></asp:TextBox>
                    </div>     
                    <div class="form-item leftHalf">
                        <label>Año</label>
                        <asp:TextBox ID="tbAnioUsuario" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="tbAnioUsuario_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbAnioUsuario">
                        </asp:FilteredTextBoxExtender>
                    </div>
                    <div class="form-item rightHalf">
                        <asp:Button ID="btnFiltrar" runat="server" Text="Filtrar"  style="float:right"
                            onclick="btnFiltrar_Click" />
                    </div> 
                </div>
        </asp:Panel>

            <asp:GridView ID="GVRepUsuariosP" runat="server" AutoGenerateColumns="False" style="overflow:auto"
                DataSourceID="ODSRepUsuario" Enabled="False" Visible="False">
                <Columns>
                    <asp:BoundField DataField="Usuarioid" HeaderText="Usuarioid" 
                        InsertVisible="False" ReadOnly="True" SortExpression="Usuarioid" 
                        Visible="False" />
                    <asp:BoundField DataField="NombreCurso" HeaderText="NombreCurso" 
                        SortExpression="NombreCurso" />
                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                        SortExpression="Legajo" />
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                        SortExpression="Apellido" />
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                        SortExpression="Nombre" />
                    <asp:BoundField DataField="FechaInscripcion" HeaderText="FechaInscripcion" 
                        SortExpression="FechaInscripcion" />
                    <asp:BoundField DataField="Direccion" HeaderText="Direccion" ReadOnly="True" 
                        SortExpression="Direccion" />
                    <asp:BoundField DataField="Area" HeaderText="Area" ReadOnly="True" 
                        SortExpression="Area" />
                    <asp:BoundField DataField="idCurso" HeaderText="idCurso" InsertVisible="False" 
                        ReadOnly="True" SortExpression="idCurso" Visible="False" />
                </Columns>
                <EmptyDataTemplate>
             <asp:Label ID="lblEmptyData"
                    Text="<div class='messages msg-info'>No se encontraron Registros que coincidan con el criterio de búsqueda ingresado.</div>" runat="server" Visible="false">
             </asp:Label>

                </EmptyDataTemplate>
            </asp:GridView>

            <asp:ObjectDataSource ID="ODSRepUsuario" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getReporteUsuarios" 
                TypeName="com.paginar.johnson.BL.ControllerPDA">
                <SelectParameters>
                    <asp:ControlParameter ControlID="tbLegajo" Name="Legajo" PropertyName="Text" 
                        Type="Int32" />
                    <asp:ControlParameter ControlID="tbApellido" Name="Apellido" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="tbAnioUsuario" Name="anio" PropertyName="Text" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </asp:View>
        <asp:View ID="Vdefault" runat="server">
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
