﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;
using com.paginar.johnson.utiles;
using FredCK.FCKeditorV2;
using com.paginar.johnson.DAL;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using AjaxControlToolkit;

namespace com.paginar.johnson.Web.BO
{
    public partial class NoticiasABM : PageBase
    {
        private ControllerNoticias _NC;
        public ControllerNoticias NC
        {
            get
            {
                _NC = (ControllerNoticias)this.Session["NC"];
                if (_NC == null)
                {
                    _NC = new ControllerNoticias();
                    this.Session["NC"] = _NC;
                }
                return _NC;
            }
            set
            {
                this.Session["NC"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //RadioButtonList RBL = (RadioButtonList)RBLFiltroNotiReporte;
            //if (RBL.SelectedIndex == 0 || RBL.SelectedIndex == 1)
            //{
            //    TextBox1.Text = "";
            //    TextBox2.Text = "";
            //    TextBox1.Enabled = false;
            //    TextBox2.Enabled = false;
            //}
            //else
            //{
            //    TextBox1.Enabled = true;
            //    TextBox2.Enabled = true;
            //}

            //TextBox1.Text = TextBox1.Text.Trim();
            //TextBox2.Text = TextBox2.Text.Trim();
            

            if (!Page.IsPostBack)
            {
                NC = new ControllerNoticias();
                //MVReporte.SetActiveView(VReporteNoticia);
                MVN.SetActiveView(VBuc);
                MVReporteNoticias.SetActiveView(VReporteNoticias);
                BuscarButton.Focus();
            }
            else
            {
                Page.MaintainScrollPositionOnPostBack = false;
            }
        }

        protected void ObjectDataSourceNoticias_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.Cancel = !Page.IsPostBack;
        }

        protected void BuscarButton_Click(object sender, EventArgs e)
        {
            GVNoticias.Visible = true;


            HyperLink3.NavigateUrl = "~/BO/imprimirNoticiasABM.aspx?Estado=" + EstadoDropDownList.SelectedValue.ToString() + "&Destacados=" + DestacadosDropDownList.SelectedValue.ToString() + "&Categorias=" + CategoriasDropDownList.SelectedValue.ToString() + "&BuscarPor=" + BuscarPorDropDownList.SelectedValue.ToString() + "&Desde=" + DesdeTextBox.Text.ToString() + "&Hasta=" + HastaTextBox.Text.ToString();
            GVNoticias.DataBind();
        }

        protected string GetIntString(object Valor)
        {
            Int32? ValorInt = null;
            if (Valor != null)
            {
                if (Valor.ToString() == string.Empty) return string.Empty;
                ValorInt = int.Parse(Valor.ToString());
            }
            else
                return string.Empty;

            if (((Int32)ValorInt )== 1)
                return "Si";
            else
                return "No";
            
        }

        protected string GetUrl(int InfoID, int Tipo, int privacidad)
        {
            //string UrlPagina = string.Format( "/novedades/{0}?InfoID={1}",(Tipo==1)?"nota.asp":"notasnl.aspx",InfoID);/noticias/noticia.aspx
            string UrlPagina = string.Empty;
            if (privacidad == 1)
            {
                UrlPagina = string.Format("noticias/{0}InfoID={1}", (Tipo == 1) ? "noticia.aspx?priv=" + CodValidacion(InfoID) + "&" : "urlNoticia.aspx?priv=" + CodValidacion(InfoID) + "&", InfoID);
            }
            else 
            {
                UrlPagina = string.Format("noticias/{0}InfoID={1}", (Tipo == 1) ? "noticia.aspx?" : "urlNoticia.aspx?", InfoID);
            }
            
            //1: Intra, 2 News
            string URL = "http://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath;
            return URL + UrlPagina;
        }

        protected void GVNoticias_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
                MVN.SetActiveView(VABM);

            if (e.CommandName == "Comentarios")
            {
                LinkButton InfoIDLinkButton = GVNoticias.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("InfoIDLinkButton") as LinkButton;
                Label TituloLabelAux = GVNoticias.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("TituloLabel") as Label;
                InfoIDHiddenField.Value = InfoIDLinkButton.CommandArgument;

                TituloLabel.Text = TituloLabelAux.Text;
                MVN.SetActiveView(VComentarios);

            }

            if (e.CommandName == "Clonar")
            {
                this.Session.Remove("NC");
                ControllerNoticias NCC = new ControllerNoticias();

                int NotiID;
                NotiID = int.Parse(e.CommandArgument.ToString());
                DSNoticias.infInfoDataTable NoticiaAClonar = new DSNoticias.infInfoDataTable();
                DSNoticias.infLinkDataTable NoticiaAClonarLink = new DSNoticias.infLinkDataTable();
                DSNoticias.infImagenDataTable NoticiaAClonarImg = new DSNoticias.infImagenDataTable();

                NoticiaAClonar = NCC.GetInfInfoByInfoID(NotiID);
                NoticiaAClonarLink = NCC.GetLinkByInfoID();
                NoticiaAClonarImg = NCC.GetImagenByInfoID();
                //NCC.Repositorio.AdapterDSNoticias.infLink 
                //NC.GetInfInfoByInfoID
                
                //NoticiaAClonarOriginal = NC.GetInfInfoByInfoID(NotiID);
                
                
                MVN.SetActiveView(VABM);
                FVNoticia.ChangeMode(FormViewMode.Insert);
                TextBox Titulo = FVNoticia.FindControl("TituloTextBox") as TextBox;
                TextBox Copete = FVNoticia.FindControl("CopeteTextBox") as TextBox;                
                TextBox Fuente = FVNoticia.FindControl("FuenteTextBox") as TextBox;
                FCKeditor Editor = FVNoticia.FindControl("Editor1") as FCKeditor;

                Titulo.Text = string.Empty;
                Copete.Text = string.Empty;
                Editor.Value = string.Empty;

                foreach (DSNoticias.infInfoRow NotiRow in NoticiaAClonar.Rows)
                {
                   
                    Titulo.Text += (NotiRow.IsTituloNull() ? "" : NotiRow.Titulo.ToString());
                    Copete.Text += (NotiRow.IsCopeteNull() ? "" : NotiRow.Copete.ToString());
                    Editor.Value += (NotiRow.IsTextoNull() ? "" : NotiRow.Texto.ToString());
                    Fuente.Text += (NotiRow.IsFuenteNull() ? "" : NotiRow.Fuente.ToString());
                    
                }

                foreach(DSNoticias.infLinkRow NoticiaLinkRow in NoticiaAClonarLink.Rows)
                {
                    NC.InsertLink(-1, (NoticiaLinkRow.IsDescripNull() ? "" : NoticiaLinkRow.Descrip.ToString()), NoticiaLinkRow.URL.ToString()); //NoticiaAClonarLink.Site.ToString()
                }

                GridView GVLinks = FVNoticia.FindControl("GVLinks") as GridView;
                GVLinks.DataBind();
                foreach(DSNoticias.infImagenRow NoticiaImgRow in NoticiaAClonarImg.Rows)
                {
                    NC.InsertImagen(-1, (NoticiaImgRow.IsDescripNull() ? "" : NoticiaImgRow.Descrip.ToString()), NoticiaImgRow.Path.ToString(), NoticiaImgRow.flyer);
                }

                GridView GVImagen = FVNoticia.FindControl("GVImagen") as GridView;
                GVImagen.DataBind();
                
                Button BotonInsertClone = FVNoticia.FindControl("InsertButton") as Button;
                BotonInsertClone.Text = "Clonar";



                

            }
        }       

        protected void FormView1_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            e.NewValues["FHMod"] = DateTime.Now;
            e.NewValues["UsrMod"] = this.ObjectUsuario.usuarioid;
            CheckBoxList ClusterCBL = FVNoticia.FindControl("ClusterCBL") as CheckBoxList;
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;

            FCKeditor editor = FVNoticia.FindControl("Editor1") as FCKeditor;
            e.NewValues["Texto"] = RemoveFONT(editor.Value);

            foreach (ListItem li in ClusterCBL.Items)
            {
                if (li.Selected)
                {
                    NC.InsertCluster(int.Parse(InfoIDLabel1.Value), int.Parse(li.Value));
                }
                else
                {
                    NC.DeleteCluster(int.Parse(InfoIDLabel1.Value),int.Parse(li.Value));
                }
            }
            
            NC.DeleteInfoTags(int.Parse(InfoIDLabel1.Value));

            Repeater rptTags = FVNoticia.FindControl("rptTags") as Repeater;
            foreach (RepeaterItem item in rptTags.Items)
            {
                CheckBox chk = item.FindControl("chkTag") as CheckBox;
                if (chk.Checked)
                {
                    HiddenField TagId = item.FindControl("hdTagId") as HiddenField;
                    NC.AddInfoTags(int.Parse(InfoIDLabel1.Value), int.Parse(TagId.Value));
                }
            }

            
        }

        protected void ODSLinksNoticias_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSLinksNoticias_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSNoticia_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSNoticia_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ObjectDataSourceLinkNoticias_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ObjectDataSourceLinkNoticias_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }       

        protected void FVLink_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GridView GVLinks = FVNoticia.FindControl("GVLinks") as GridView;
            GVLinks.DataBind();
            FormView FVLink = sender as FormView;
            FVLink.ChangeMode(FormViewMode.Insert);
        }

        protected void GVLinks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                FormView FVLink = FVNoticia.FindControl("FVLink") as FormView;
                FVLink.ChangeMode(FormViewMode.Edit);
            }

        }

        protected void FVLink_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GridView GVLinks = FVNoticia.FindControl("GVLinks") as GridView;
            GVLinks.DataBind();
        }

        protected void FVLink_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
            e.Values["InfoID"] = InfoIDLabel1.Value;
        }

        protected void FVImagen_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
            e.Values["InfoID"] = InfoIDLabel1.Value;
        }

        protected void GVImagen_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                FormView FVImagen = FVNoticia.FindControl("FVImagen") as FormView;
                FVImagen.ChangeMode(FormViewMode.Edit);
            }
        }

        protected void ODSImagenNoticias_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSImagenNoticias_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSImagenesNoticias_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSImagenesNoticias_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void FVImagen_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GridView GVImagen = FVNoticia.FindControl("GVImagen") as GridView;
            GVImagen.DataBind();
        }

        protected void FVImagen_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GridView GVImagen = FVNoticia.FindControl("GVImagen") as GridView;
            GVImagen.DataBind();
            FormView FVImagen = sender as FormView;
            FVImagen.ChangeMode(FormViewMode.Insert);
        }

        protected void ButtonNuevaNoticia_Click(object sender, EventArgs e)
        {
           MVN.SetActiveView(VABM);
           FVNoticia.ChangeMode(FormViewMode.Insert);
        }

        protected void ODSLinkNoticiasInsert_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSLinkNoticiasInsert_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSLinksNoticiasInsert_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSLinksNoticiasInsert_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSImagenNoticiasInsert_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSImagenNoticiasInsert_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSImagenesNoticiasInsert_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSImagenesNoticiasInsert_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void FVNoticia_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            if (NC.noticiaGuardada())
            {
                e.Cancel = true;
                MVN.SetActiveView(VErrorAtras);

            }
            e.Values["FHAlta"] = DateTime.Now;
            e.Values["UsrAlta"] = this.ObjectUsuario.usuarioid;
            e.Values["TipoInfoID"] = 1;
            CheckBoxList ClusterCBL = FVNoticia.FindControl("ClusterCBL") as CheckBoxList;
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
            
            FCKeditor editor = FVNoticia.FindControl("Editor1") as FCKeditor;

            e.Values["Texto"] = RemoveFONT(editor.Value);

            foreach (ListItem li in ClusterCBL.Items)
            {
                if (li.Selected)
                {
                    NC.InsertCluster(-1, int.Parse(li.Value));
                }
                else
                {
                    NC.DeleteCluster(-1, int.Parse(li.Value));
                }
            }


            Repeater rptTags = FVNoticia.FindControl("rptTags") as Repeater;
            foreach (RepeaterItem item in rptTags.Items)
            {
                CheckBox chk = item.FindControl("chkTag") as CheckBox;
                if (chk.Checked)
                {
                    HiddenField TagId = item.FindControl("hdTagId") as HiddenField;
                    NC.AddInfoTags(-1, int.Parse(TagId.Value));
                }
            }

        }

        protected void FVNoticia_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {

           // MVN.SetActiveView(VBuc);

            MVN.SetActiveView(VResultado);
            GVNoticias.DataBind();
            GVNoticias.SelectedIndex = -1;
        }

        protected void FVNoticia_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            //MVN.SetActiveView(VBuc);
            MVN.SetActiveView(VResultado);
            GVNoticias.DataBind();
        }

        protected void FVNoticia_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            //FormView elsender = (FormView)sender;
            //NC.DeleteInfo(int.Parse(elsender.SelectedValue.ToString()));
            ControllerNewsletter cn = new ControllerNewsletter();
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
            if (!cn.TieneNewsletter(int.Parse(InfoIDLabel1.Value)))
            {
                MVN.SetActiveView(VBuc);
                GVNoticias.DataBind();
            }
            else
            {
                EjecutarScript("alert('No puede eliminar la noticia, pertenece a un Newsletter');");
            }
        }

        protected void FVNoticia_DataBound(object sender, EventArgs e)
        {
            //NC = null;
            if (FVNoticia.CurrentMode == FormViewMode.Edit)
            {
                com.paginar.johnson.DAL.DSNoticias.RelInfoClusterDataTable DTTemp = NC.GetClusterByInfoID();
                CheckBoxList ClusterCBL = FVNoticia.FindControl("ClusterCBL") as CheckBoxList;
                HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
                HiddenField HiddenFieldUsuario = FVNoticia.FindControl("HiddenFieldUsuario") as HiddenField;

                if (ClusterCBL != null)
                {
                    foreach (com.paginar.johnson.DAL.DSNoticias.RelInfoClusterRow item in DTTemp.Rows)
                    {

                        ListItem Li = ClusterCBL.Items.FindByValue(item.ClusterID.ToString());
                        if (Li != null)
                            Li.Selected = true;
                    }
                }
                Button BotonBorrar = (Button)FVNoticia.FindControl("Button1");
                Button UpdateButton = (Button)FVNoticia.FindControl("UpdateButton");
                int InfoID = int.Parse(FVNoticia.SelectedValue.ToString());
                ControllerNoticias CNB = new ControllerNoticias();

                ControllerUsuarios CUVerificacion = new ControllerUsuarios();

                DSNoticias.InfoUsuarioComentariosDataTable LosComentarios = new DSNoticias.InfoUsuarioComentariosDataTable();
                int? LosVotos;
                CNB.GetInfInfoByInfoID(InfoID);

                int ClusterUsuarioCreador = 1;

                DSUsuarios.UsuarioDataTable UsrCreador = CUVerificacion.GetUsuarioByUsuarioID(int.Parse(HiddenFieldUsuario.Value));
                foreach(DSUsuarios.UsuarioRow UCRow in UsrCreador.Rows)
                {
                    ClusterUsuarioCreador = UCRow.Cluster; 
                }

                if ((this.ObjectUsuario.clusteriddefecto != ClusterUsuarioCreador) && this.ObjectUsuario.clusteriddefecto != 1)
                {
                    BotonBorrar.Enabled = false;
                    UpdateButton.Enabled = false;
                    BotonBorrar.ToolTip = "Esta noticia solo puede ser borrada por el administrador del Cluster que la creo";
                    UpdateButton.ToolTip = "Esta noticia solo puede ser actualizada por el administrador del Cluster que la creo";
                }

                LosComentarios = CNB.getComments(InfoID);
                LosVotos = CNB.getVotos(InfoID, 1);
                string confirmacion = string.Empty;
                bool bandera = false;

                int contComentarios = LosComentarios.Count;

                if (contComentarios > 0)
                {
                    bandera = true;
                    confirmacion += " comentarios ";
                }
              

                if (LosVotos > 0)
                {
                    if (bandera)
                        confirmacion += "y";
                    confirmacion += " votos ";
                    bandera = true;
                }

                if(bandera)
                    BotonBorrar.OnClientClick = "return confirm('Esa Noticia contiene" + confirmacion + ". ¿Realmente desea eliminarla?');";

            }
            
        }

        
        protected void BtnAgregarImagenes_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool agrega = true;
                DataView dv = (DataView)((ObjectDataSource)FVNoticia.FindControl("ODSImagenesNoticias")).Select();
                foreach (DataRow dr in dv.ToTable().Rows)
                {
                    if (dr["Flyer"].ToString() == "True") agrega = false;
                }

                if (((CheckBox)FVNoticia.FindControl("chkFlyer")).Checked && dv.ToTable().Rows.Count > 0)
                    agrega = false;

                if (agrega)
                {

                    FileUpload FUImagenes = FVNoticia.FindControl("FUImagenes") as FileUpload;
                    HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
                    TextBox DescripTextBox = FVNoticia.FindControl("DescripTextBox") as TextBox;
                    CheckBox chkFlyer = FVNoticia.FindControl("chkFlyer") as CheckBox;
                    if (FUImagenes.HasFile)
                    {
                        string filename = Path.GetFileNameWithoutExtension(FUImagenes.FileName).Replace("&", "").Replace("=", "") + DateTime.Now.Ticks.ToString() + Path.GetExtension(FUImagenes.FileName).ToLower();
                        string strPath = MapPath("~/noticias/Imagenes/") + filename;
                        FUImagenes.SaveAs(@strPath);
                        NC.InsertImagen(int.Parse(InfoIDLabel1.Value), DescripTextBox.Text, filename, chkFlyer.Checked);
                        DescripTextBox.Text = string.Empty;
                        GridView GVImagen = FVNoticia.FindControl("GVImagen") as GridView;
                        GVImagen.DataBind();

                        ((Label)FVNoticia.FindControl("LabelMensajeFlyer")).Visible = false;
                    }
                }
                else
                    ((Label)FVNoticia.FindControl("LabelMensajeFlyer")).Visible = true;
            }

        }

        protected void BtnAgregarImagenesINS_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool agrega = true;
                DataView dv = (DataView)((ObjectDataSource)FVNoticia.FindControl("ODSImagenesNoticiasInsert")).Select();
                foreach (DataRow dr in dv.ToTable().Rows)
                {
                    if (dr["Flyer"].ToString() == "True") agrega = false;
                }

                if (((CheckBox)FVNoticia.FindControl("chkFlyer")).Checked && dv.ToTable().Rows.Count > 0)
                    agrega = false;

                if (agrega)
                {
                    FileUpload FUImagenes = FVNoticia.FindControl("FUImagenes") as FileUpload;
                    


                    TextBox DescripTextBox = FVNoticia.FindControl("DescripTextBox") as TextBox;
                    CheckBox chkFlyer = FVNoticia.FindControl("chkFlyer") as CheckBox;
                    
                 

                    if (FUImagenes.HasFile)
                    {
                        string filename = Path.GetFileNameWithoutExtension(FUImagenes.FileName).Replace("&", "").Replace("=", "") + DateTime.Now.Ticks.ToString() + Path.GetExtension(FUImagenes.FileName).ToLower();
                        string strPath = MapPath("~/noticias/Imagenes/") + filename;
                        FUImagenes.SaveAs(@strPath);
                        NC.InsertImagen(-1, DescripTextBox.Text, filename, chkFlyer.Checked);
                        DescripTextBox.Text = string.Empty;
                        GridView GVImagen = FVNoticia.FindControl("GVImagen") as GridView;
                        GVImagen.DataBind();
                        ((Label)FVNoticia.FindControl("LabelMensajeFlyer")).Visible = false;
                    }
                }
                else
                    ((Label)FVNoticia.FindControl("LabelMensajeFlyer")).Visible = true;
            }
        }

        protected void ODSRecordatorio_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSRecordatorio_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void FVRecordatorios_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GridView GVRecordatorios = FVNoticia.FindControl("GVRecordatorios") as GridView;
            GVRecordatorios.DataBind();
        }

        protected void ODSRecordatorios_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSRecordatorios_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void FVRecordatorios_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            HiddenField InfoIDLabel1 = FVNoticia.FindControl("InfoIDLabel1") as HiddenField;
            e.Values["InfoID"] = int.Parse(InfoIDLabel1.Value);
        }

        protected void ODSRecordatoriosINS_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSRecordatoriosINS_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSRecordatorioINS_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSRecordatorioINS_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void FVRecordatoriosINS_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            GridView GVRecordatorios = FVNoticia.FindControl("GVRecordatoriosINS") as GridView;
            GVRecordatorios.DataBind();
        }

        protected void VBuc_Activate(object sender, EventArgs e)
        {
            //NC = null;
        }

        protected void VABM_Activate(object sender, EventArgs e)
        {
           
        }

        protected void GVComentarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                FVComentario.DataBind();
            }
        }

        protected void ODSComentarios_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSComentarios_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ODSComment_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = NC;
        }

        protected void ODSComment_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void CancelarBtn_Click(object sender, EventArgs e)
        {
            GVComentarios.SelectedIndex = -1;
            FVComentario.DataBind();
        }

        protected void FVComentario_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {

            GVComentarios.SelectedIndex = -1;
            FVComentario.DataBind();
            GVComentarios.DataBind();
        }

        protected void FHRecordatorioCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FormView FVRecordatorios = FVNoticia.FindControl("FVRecordatorios") as FormView;
            CustomValidator FHRecordatorioCustomValidator = FVRecordatorios.FindControl("FHRecordatorioCustomValidator") as CustomValidator;
            args.IsValid = false;
            DateTime FRecordatorio = DateTime.Parse(args.Value);
            TextBox FechaAsocTextBox = FVNoticia.FindControl("FechaAsocTextBox") as TextBox;
            if (!((!string.IsNullOrEmpty(FechaAsocTextBox.Text))&&(FRecordatorio.Validar(FechaAsocTextBox.Text))))
            {
                FHRecordatorioCustomValidator.ErrorMessage = "Antes de agregar un recordatorio debe ingresar la fecha de publicación de la noticia.";
                args.IsValid = false;
                return;
            }
            DateTime FechaAsoc = DateTime.Parse(FechaAsocTextBox.Text);
            
            if (FRecordatorio <= FechaAsoc)
            {
                FHRecordatorioCustomValidator.ErrorMessage = "La fecha del Recordatorio debe ser Mayor a la fecha de Publicacion de la Noticia";
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            CheckBoxList ClusterCBL = FVNoticia.FindControl("ClusterCBL") as CheckBoxList;
            bool selecciono = false;
            for (int i = 0; i < ClusterCBL.Items.Count; i++)
            {
                selecciono = ClusterCBL.Items[i].Selected;
                if (selecciono) { args.IsValid = true; return; }                
            }           
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {            
            if(!DateTime.Now.Validar(args.Value))
            {
                args.IsValid=false;
                return;
            }
            args.IsValid=NC.ValidarFechaPublicacion(DateTime.Parse(args.Value));
        }

        protected void CustomValidator1_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;
            CheckBoxList ClusterCBL = FVNoticia.FindControl("ClusterCBL") as CheckBoxList;
            bool selecciono = false;
            for (int i = 0; i < ClusterCBL.Items.Count; i++)
            {
                selecciono = ClusterCBL.Items[i].Selected;
                if (selecciono) { args.IsValid = true; return; }
            }    
        }

        protected void FHRecordatorioCustomValidatorINS_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FormView FVRecordatorios = FVNoticia.FindControl("FVRecordatoriosINS") as FormView;
            CustomValidator FHRecordatorioCustomValidator = FVRecordatorios.FindControl("FHRecordatorioCustomValidatorINS") as CustomValidator;
            args.IsValid = false;
            DateTime FRecordatorio = DateTime.Parse(args.Value);
            TextBox FechaAsocTextBox = FVNoticia.FindControl("FechaAsocTextBox") as TextBox;
            if (!((!string.IsNullOrEmpty(FechaAsocTextBox.Text)) && (FRecordatorio.Validar(FechaAsocTextBox.Text))))
            {
                FHRecordatorioCustomValidator.ErrorMessage = "Antes de agregar un recordatorio debe ingresar la fecha de publicación de la noticia.";
                args.IsValid = false;
                return;
            }
            DateTime FechaAsoc = DateTime.Parse(FechaAsocTextBox.Text);

            if (FRecordatorio <= FechaAsoc)
            {
                FHRecordatorioCustomValidator.ErrorMessage = "La fecha del Recordatorio debe ser Nayor a la fecha de Publicacion de la Noticia";
                args.IsValid = false;
                return;
            }
            args.IsValid = true;
        }       

        protected void CustomValidator3_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            DateTime FechaAsoc = DateTime.Parse(args.Value);
            args.IsValid = (FechaAsoc.Date >= DateTime.Now.Date);
        }

        protected void UpdateCancelButton_Click(object sender, EventArgs e)
        {
            MVN.SetActiveView(VBuc);
        }

        protected void InsertCancelButton_Click(object sender, EventArgs e)
        {
            MVN.SetActiveView(VBuc);
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            MVN.SetActiveView(VBuc);
            NC.noticiaGuardada();
            Timer1.Enabled = false;
        }

        protected void VResultado_Activate(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
        }

        protected void ODSNoticia_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            NC = null;
        }




        public string RemoveFONT(string strText)
        {
            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<[/]?(font|span|xml|del|pre|ins|[ovwxp]:\\w+)[^>]*?>");
            return regEx.Replace(strText, "");
        }



        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }
        protected string CodValidacion(int infoid)
        {
            int codigo;
            string retorno = string.Empty;
            codigo = infoid % 15;
            switch (codigo)
            { 
                case 0:
                    retorno = "2A55S";
                    break;
                case 1:
                    retorno = "2EW85";
                    break;
                case 2:
                    retorno = "DKE55";
                    break;
                case 3:
                    retorno = "SAP2Z";
                    break;
                case 4:
                    retorno = "LS5S2";
                    break;
                case 5:
                    retorno = "6SDRS";
                    break;
                case 6:
                    retorno = "ASE3Q";
                    break;
                case 7:
                    retorno = "WY7T4";
                    break;
                case 8:
                    retorno = "8ZW8S";
                    break;
                case 9:
                    retorno = "8ERSA";
                    break;
                case 10:
                    retorno = "3B843";
                    break;
                case 11:
                    retorno = "YYW5S";
                    break;
                case 12:
                    retorno = "P8SD9";
                    break;
                case 13:
                    retorno = "W2S3E";
                    break;
                case 14:
                    retorno = "Q31RT";
                    break;

            }
            return retorno;
        }

        public class NoticiaPreview
        {
            public int   InfoID { get; set; }
            public string CategoriaDescrip { get; set; }
            public string Titulo { get; set; }
            public string Copete { get; set; }
            public string FechaAsoc { get; set; }
            public string FHRecordatorio { get; set; }
            public bool Recordatorio { get; set; }
            public string Texto { get; set; }
            public string Fuente { get; set; }
        }


        protected void PreviewButton_Click(object sender, EventArgs e)
        {
            //lblCategoria  lbltitulo imgnoticia lblFecha lblCopete lblCuerpo lblAttach lblFuente
            //(DropDownList)FVNoticia.FindControl("CategoriaDropDownList").

            /*
             CategoriaDropDownList
             TituloTextBox CopeteTextBox FechaAsocTextBox FuenteTextBox DescripTextBox FUImagenes Editor1
             */

            List<NoticiaPreview> NoticiaBind = new List<NoticiaPreview>();
            NoticiaPreview np = new NoticiaPreview();
            np.InfoID = -1;
            np.CategoriaDescrip = ((DropDownList)FVNoticia.FindControl("CategoriaDropDownList")).SelectedItem.ToString();
            np.Titulo = ((TextBox)FVNoticia.FindControl("TituloTextBox")).Text.ToString();
            np.FechaAsoc = ((TextBox)FVNoticia.FindControl("FechaAsocTextBox")).Text.ToString();
            np.Copete = ((TextBox)FVNoticia.FindControl("CopeteTextBox")).Text.ToString();
            np.Texto =  RemoveFONT( ((FCKeditor)FVNoticia.FindControl("Editor1")).Value.ToString());
            np.Fuente = ((TextBox)FVNoticia.FindControl("FuenteTextBox")).Text.ToString();
            NoticiaBind.Add(np);


            //((Label)FVNoticia.FindControl("lblCategoria")).Text = ((DropDownList)FVNoticia.FindControl("CategoriaDropDownList")).SelectedItem.ToString();
            //((Label)FVNoticia.FindControl("lbltitulo")).Text = ((TextBox)FVNoticia.FindControl("TituloTextBox")).Text.ToString();
            //((Label)FVNoticia.FindControl("lblFecha")).Text = ((TextBox)FVNoticia.FindControl("FechaAsocTextBox")).Text.ToString();
            //((Label)FVNoticia.FindControl("lblCopete")).Text = ((TextBox)FVNoticia.FindControl("CopeteTextBox")).Text.ToString();
            //((Label)FVNoticia.FindControl("lblCopeteFlyer")).Text = ((TextBox)FVNoticia.FindControl("CopeteTextBox")).Text.ToString();
            //((Label)FVNoticia.FindControl("lblCuerpo")).Text = ((FCKeditor)FVNoticia.FindControl("Editor1")).Value.ToString();
            //((Label)FVNoticia.FindControl("lblFuente")).Text = ((TextBox)FVNoticia.FindControl("FuenteTextBox")).Text.ToString();

            DataList RNoticias = ((DataList)FVNoticia.FindControl("RNoticias"));
            RNoticias.DataSource = NoticiaBind;
            RNoticias.DataBind();

            //DAL.DSNoticias.infImagenDataTable imagenes = new DAL.DSNoticias.infImagenDataTable();
            //imagenes = NC.Get_ImagenesByInfoID();
            //if (imagenes.Count > 0)
            //{
            //    if (bool.Parse(imagenes.Rows[0]["flyer"].ToString()))
            //    {

            //        //flyer                                               
            //        HtmlControl divNoFlyer = FVNoticia.FindControl("divNoFlyer") as HtmlControl;
            //        HtmlControl divFlyer = FVNoticia.FindControl("divFlyer") as HtmlControl;
            //        Image imgFlyer = FVNoticia.FindControl("imgFlyer") as Image;
            //        imgFlyer.ImageUrl = "~/noticias/imagenes/" + imagenes.Rows[0]["path"].ToString();
            //        imgFlyer.Attributes.Add("onclick", "flyer('~/noticias/Imagenes/" + imagenes.Rows[0]["path"] + "');");
            //        imgFlyer.Attributes.Add("style", "cursor: pointer");
            //        divNoFlyer.Visible = false;
            //        divFlyer.Visible = true;
            //        //DivImagen.Visible = false;
            //    }
            //    else
            //    {
            //        ((Image)FVNoticia.FindControl("ImagenNoticia")).Visible = true;
            //        string ruta = " ";
            //        foreach (DAL.DSNoticias.infImagenRow imgs in imagenes.Rows)
            //        {
            //            ruta = imgs.Path.ToString();
            //        }

            //        string strPath = "~/ResizeImage.aspx?Image=~/noticias/Imagenes/" + ruta + "&Size=170";
            //        int cantidad = ruta.Length;
            //        string extencion = ruta.Substring(cantidad - 3, 3);
            //        if (ruta != " " && (extencion == "jpg" || extencion == "png"))
            //        {

            //            ((Image)FVNoticia.FindControl("ImagenNoticia")).ImageUrl = strPath.ToString();
            //            ((Image)FVNoticia.FindControl("ImagenNoticia")).Attributes.Add("onclick", "flyer('~/noticias/Imagenes/" + ruta + "');");
            //            ((Image)FVNoticia.FindControl("ImagenNoticia")).Attributes.Add("style", "cursor: pointer");
            //        }
            //    }
            //}
            //else
            //{
            //    ((Image)FVNoticia.FindControl("ImagenNoticia")).Visible = false;
            //}


            ((Button)FVNoticia.FindControl("InsertButton")).Enabled = true;
            ((Button)FVNoticia.FindControl("InsertButton")).ToolTip = "";

            EjecutarScript("$(document).ready(function () { abrirModal(); });");

        }

        protected void RNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            HiddenField InfoIDHiddenField = e.Item.FindControl("InfoIDHiddenField") as HiddenField;
            Repeater RepeaterRecordatorios = e.Item.FindControl("RepeaterRecordatorios") as Repeater;
            DataList RepeaterLink = e.Item.FindControl("RTLinks") as DataList;
            Image ImagenNoticia = e.Item.FindControl("ImagenNoticia") as Image;

            HtmlGenericControl DivImagen = e.Item.FindControl("DivImagen") as HtmlGenericControl;
            HiddenField RecordatorioHiddenField = e.Item.FindControl("RecordatorioHiddenField") as HiddenField;
            Label RecordatorioLiteral = e.Item.FindControl("RecordatorioLiteral") as Label;
            Literal LiteralTexto = e.Item.FindControl("LiteralTexto") as Literal;

            string STR_RemoveHtmlAttributeRegex = @"(?<=<)([^/>]+)(\s{0}=['""][^'""]+?['""])([^/>]*)(?=/?>|\s)";

            Regex reg = new Regex(string.Format(STR_RemoveHtmlAttributeRegex, "style"),
                RegexOptions.IgnoreCase);

            LiteralTexto.Text = reg.Replace(LiteralTexto.Text, item => item.Groups[1].Value + item.Groups[3].Value);
            LiteralTexto.Text = Regex.Replace(LiteralTexto.Text, @"<div.*?>(.*?)</div>", "&#09;$1&nbsp;<br>");

            int InfoID = int.Parse(InfoIDHiddenField.Value);
            ControllerNoticias CN = new ControllerNoticias();

            //->
            DSNoticias.infRecordatoriosDataTable dsinftab = new DSNoticias.infRecordatoriosDataTable();
            if (InfoID == -1)                        
                dsinftab = NC.GetRecordatoriosByInfoID();
            else
                dsinftab = NC.getRecordatoriosInfByID(int.Parse(InfoIDHiddenField.Value));
            //->

            Control RecordatorioItem = e.Item.FindControl("RecordatorioItem") as Control;
            RecordatorioItem.Visible = dsinftab.Count() > 0 ? true : false;
            RecordatorioHiddenField.Value = dsinftab.Count() > 0 ? "True" : "False";

         

         
            DAL.DSNoticias.infImagenDataTable ImagenDT = new DAL.DSNoticias.infImagenDataTable();
            DAL.DSNoticias.infImagenDataTable ArchivosDT = new DAL.DSNoticias.infImagenDataTable();
            DAL.DSNoticias.infLinkDataTable linkDT = new DAL.DSNoticias.infLinkDataTable();

            if (InfoID != -1)
            {
                CN.GetInfInfoByInfoID(InfoID);
                ImagenDT = CN.Get_ImagenesByInfoID();
            }
            else
            {
                ImagenDT = NC.Get_ImagenesByInfoID();
                ArchivosDT = NC.Get_ArchivosByInfoID();
                linkDT = NC.GetLinkByInfoID();
            }

            //Label FechaLiteral = (Label)e.Item.FindControl("LabelFechaAsoc");

            RecordatorioItem.Visible = Boolean.Parse(RecordatorioHiddenField.Value);


            if (Boolean.Parse(RecordatorioHiddenField.Value))
            {
             
                RepeaterRecordatorios.DataSource = dsinftab;
                RepeaterRecordatorios.DataBind();


            }

            if (linkDT != null)
            {
                RepeaterLink.DataSource = linkDT;
                RepeaterLink.DataBind();
            }
               


            SlideShowExtender ImagenNoticia_SlideShowExtender = e.Item.FindControl("ImagenNoticia_SlideShowExtender") as SlideShowExtender;
            if (ImagenDT.Rows.Count == 0)
            {
                DivImagen.Visible = false;
                ImagenNoticia_SlideShowExtender.Enabled = false;
                ImagenNoticia.Visible = false;

            }
            else
            {
                if (bool.Parse(ImagenDT.Rows[0]["flyer"].ToString()))
                {
                    //flyer                                               
                    HtmlControl divNoFlyer = e.Item.FindControl("divNoFlyer") as HtmlControl;
                    HtmlControl divFlyer = e.Item.FindControl("divFlyer") as HtmlControl;
                    Image imgFlyer = e.Item.FindControl("imgFlyer") as Image;
                    imgFlyer.ImageUrl = "~/noticias/imagenes/" + ImagenDT.Rows[0]["path"].ToString();


                    string url = ImagenDT.Rows[0]["Descrip"].ToString();
                    HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImgflyer"));
                    if (url != "")
                    {
                        if (!url.Contains("http://") && (!url.Contains("https://")))
                            url = "http://" + url;
                        lnkImg.HRef = url;
                    }
                    else
                    {
                        lnkImg.Attributes.Remove("href");
                        imgFlyer.Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + ImagenDT.Rows[0]["path"] + "'," + ImagenDT.Rows[0]["InfoID"].ToString() + ");");
                        imgFlyer.Attributes.Add("style", "cursor: pointer");
                    }


                    divNoFlyer.Visible = false;
                    divFlyer.Visible = true;
                    DivImagen.Visible = false;

                }
                else//imagen comun o video
                {
                  
                    string rutaimg = " ";
                    string rutavid = " ";
                    string url = ""; // si tiene para linkear
                    bool video = false;
                    bool imagen = false;

                    foreach (DAL.DSNoticias.infImagenRow imgs in ImagenDT.Rows)
                    {
                        string ruta = imgs.Path.ToString();
                        url = imgs.Descrip;

                        if (ruta.Contains(".mp4")) { video = true; rutavid = ruta; }
                        if (ruta.Contains(".jpg") || ruta.Contains("png")) { imagen = true; rutaimg = ruta; }
                    }

                 

                    //es solo Imagen 
                    if (imagen && !video)
                    {
                        url = setImagen(e, rutaimg, url,ImagenDT.Rows[0]["infoId"].ToString());


                    }


                    //es solo video
                    if (video && !imagen)
                    {
                        ((Image)e.Item.FindControl("ImagenNoticia")).Visible = false;
                        setVideo(e,rutavid);
                    }

                    //ambos
                    if (video && imagen)
                    {
                        url = setImagen(e, rutaimg, url, ImagenDT.Rows[0]["infoId"].ToString());
                        setVideo(e,rutavid);
                    }

                  


                }//imagen comun o video
            }



            Repeater RepeaterArchivos = e.Item.FindControl("RepeaterArchivos") as Repeater;
            RepeaterArchivos.DataSource = ArchivosDT;
            RepeaterArchivos.DataBind();

         


        }

        private void setVideo(DataListItemEventArgs e,string rutavid)
        {
          
            string site = GetUrlSite().Replace("/", "%2F").Replace(":", "%3A");
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;
            ltvideo.Text = " <embed src='../noticias/imagenes/gddflvplayer.swf' flashvars='?&autoplay=false&sound=70&buffer=2&vdo=" + site + "%2Fnoticias%2Fimagenes%2F" + rutavid.Replace(".", "%2E") + "' width='420' height='315' allowFullScreen='false' quality='best' wmode='transparent' allowScriptAccess='always'  pluginspage='http://www.macromedia.com/go/getflashplayer'  type='application/x-shockwave-flash'></embed>";

        }

        private static string setImagen(DataListItemEventArgs e, string rutaimg, string url,string InfoId)
        {
            ((Image)e.Item.FindControl("ImagenNoticia")).Visible = true;
            string strPath = "~/ResizeImage.aspx?Image=~/noticias/Imagenes/" + rutaimg + "&Size=170";
            int cantidad = rutaimg.Length;
            string extencion = rutaimg.Substring(cantidad - 3, 3);

            ((Image)e.Item.FindControl("ImagenNoticia")).ImageUrl = strPath.ToString();
            HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImg"));


            if (rutaimg != " " && (extencion == "jpg" || extencion == "png") && (url != ""))
            {

                if (!url.Contains("http://"))
                    url = "http://" + url;
                lnkImg.HRef = url;
            }
            else
            {
                lnkImg.Attributes.Remove("href");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + rutaimg + "'," + InfoId + ");");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("style", "cursor: pointer");
            }
            return url;
        }


        protected string GetUrlSite()
        {
            string url = this.ASP_SITE.ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return this.ASP_SITE.ToString().Replace(":8080", "");//produccion


        }

        protected void RTLinks_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;
                

            HyperLink link = e.Item.FindControl("HyperLink1") as HyperLink;

            if (link.NavigateUrl.Contains("www.youtube.com") || link.NavigateUrl.Contains("http://youtu.be"))
            {
                string url;
                link.Visible = false;                 
                url = link.NavigateUrl.Replace("http://youtu.be/","https://www.youtube.com/v/");

                url = url.Replace("watch?v=", "v/");
                ltvideo.Text = " <embed  width='420' height='315' src='"+url+"' > ";
            }

            

            if (link.NavigateUrl.Contains("vimeo.com"))
            {
                 link.Visible = false;
                 string url = link.NavigateUrl.Replace("https://vimeo.com/", "").Replace("channels/staffpicks/", "");
                         
                 ltvideo.Text = "<iframe src='//player.vimeo.com/video/"+url+"' width='420' height='315' frameborder='0' ></iframe>";
            }

        }

        protected void RBLFiltroNotiReporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList RBLTipoBusqueda = (RadioButtonList)sender;
            if (RBLTipoBusqueda.SelectedIndex != 2)
            {
                TextBox1.Enabled = false;
                TextBox2.Enabled = false;
            }
            else
            {
                TextBox1.Enabled = true;
                TextBox2.Enabled = true;
            }
        }



        protected void btnFlitroNotiReporte_Click(object sender, EventArgs e)
        {
            TextBox1.Text = TextBox1.Text.Trim();
            TextBox2.Text = TextBox2.Text.Trim();
            RadioButtonList RBL = (RadioButtonList)RBLFiltroNotiReporte;
            if (RBL.SelectedIndex == 0 || RBL.SelectedIndex == 1)
            {
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            //GVReporteNoticia.DataSourceObject.Equals(ODSComentarioRepNoticia);
            GVReporteNoticia.DataBind();
            

        }


        protected void Export_Click(object sender, EventArgs e)
        {


            StringWriter sw = new StringWriter();

            string attachment = "attachment; filename=Reporte-Noticias-" + DateTime.Now.ToShortDateString() + ".xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            
            //DSClasificados.clasiAvisoDataTable CADT = new DSClasificados.clasiAvisoDataTable();
            //DSClasificados.claAvisoComentariosDataTable CACDT = new DSClasificados.claAvisoComentariosDataTable();

            DateTime? Desde = new DateTime();
            DateTime? Hasta = new DateTime();
            if (TextBox1.Text.Trim() != "")
                Desde = Convert.ToDateTime(TextBox1.Text);
            else
                Desde = null;
            if (TextBox2.Text.Trim() != "")
                Hasta = Convert.ToDateTime(TextBox2.Text);
            else
                Hasta = null;

            DSNoticias.infInfoDataTable DTReporteNoticia = new DSNoticias.infInfoDataTable();
            //
            DTReporteNoticia = NC.Repositorio.getInfoByReporte(Desde, Hasta, RBLFiltroNotiReporte.SelectedValue);

            sw.WriteLine("<table VALIGN=\"top\" ALING=\"left\" BORDER=\"5\"><thead><tr><th bgcolor=\"#CCCCCC\">ID</th><th bgcolor=\"#CCCCCC\">TITULO</th><th bgcolor=\"#CCCCCC\">CATEGORIA</th><th bgcolor=\"#CCCCCC\">FECHA DE ALTA</th><th bgcolor=\"#CCCCCC\">ME GUSTA</th><th bgcolor=\"#CCCCCC\">PRIVACIDAD</th></tr></thead>");

            foreach (DSNoticias.infInfoRow NotiRRow in DTReporteNoticia.Rows)
            {
                sw.WriteLine("<tr>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\"><strong> " + NotiRRow.InfoID.ToString() + "</strong></td>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + NotiRRow.Titulo.ToString() + "</td>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + NotiRRow.CategoriaDescrip.ToString() + "</td>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + NotiRRow.FHAlta.ToShortDateString() + "</td>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + NotiRRow.Votos + "</td>");
                sw.WriteLine("<td VALIGN=\"top\" bgcolor=\"E3E3E3\">" + ((NotiRRow.Privacidad.ToString() == "0") ? "No" : "Si") + "</td>");
                sw.WriteLine("</tr>");
               
            }
            sw.WriteLine("</table>");
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }

        protected void GVReporteNoticia_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Comentarios")
            {
             
                Label TituloLabelID = GVReporteNoticia.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("lblIDReporte") as Label;
                Label TituloLabelAux = GVReporteNoticia.Rows[int.Parse(e.CommandArgument.ToString())].FindControl("lblTituloReporteNotica") as Label;
                HFRepComent.Value = TituloLabelID.Text;
                lblTituComentReport.Text = TituloLabelAux.Text;//" Titulo ";
                
                MVReporteNoticias.SetActiveView(VComentarioReporte);

            }

        }


        protected void lbAtrasReport_Click(object sender, EventArgs e)
        {
            MVReporteNoticias.SetActiveView(VReporteNoticias);
        }

        protected void LBimpresion_Click(object sender, EventArgs e)
        {

        }

        protected void GVNoticias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           if (e.Row.RowType == DataControlRowType.DataRow)
           {
            Repeater RepeaterRecordatorios = e.Row.FindControl("RepeaterRecordatorios") as Repeater;
            HiddenField HiddenFieldInfoID = (HiddenField)e.Row.FindControl("HiddenFieldInfoID");
            int InfoID = int.Parse(HiddenFieldInfoID.Value);
            DSNoticias.infRecordatoriosDataTable dsinftab = new DSNoticias.infRecordatoriosDataTable();
            dsinftab = NC.getRecordatoriosInfByID(InfoID);
            RepeaterRecordatorios.DataSource = dsinftab;
            RepeaterRecordatorios.DataBind();

                Repeater rptTags = e.Row.FindControl("rptTags") as Repeater; 

                rptTags.DataSource = NC.getTagsByInfoIDgrv(InfoID);
                rptTags.DataBind();
        }
        }

        protected void ClusterCBL_DataBound(object sender, EventArgs e)
        {
            
            if(this.ObjectUsuario.clusterID != 1)
            {
                FVNoticia.Enabled = false;
                foreach (ListItem item in ((CheckBoxList)sender).Items)
                {
                    if (item.Value == this.ObjectUsuario.clusterID.ToString())
                    {
                        item.Enabled = true;
                        //if (FVNoticia.CurrentMode == FormViewMode.Edit && item.Selected == true)
                            FVNoticia.Enabled = true;
                    }
                    else
                    {
                        item.Enabled = false;
                    }
                }


                if (FVNoticia.CurrentMode == FormViewMode.Insert)
                {
                    FVNoticia.Enabled = true;
                }
            }

        }

        protected void FVNoticia_PreRender(object sender, EventArgs e)
        {
            //EjecutarScript("$( document ).ready(function() { function check_extension(filename,id) {var re = /\\..+$/; var ext = filename.match(re); alert(ext); if (ext == '.jpg' || ext == '.gif' || ext == '.jpeg') { ValidatorEnable(id,false); } else { ValidatorEnable(id,true);} } });");
        }

        protected void FUImagenes_DataBinding(object sender, EventArgs e)
        {
            int a = 0;
        }

        bool flag = true;
        bool valid = true;
        protected void CustomValidator4_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool isvalid = args.IsValid;

           // Panel div = (Panel)FVNoticia.FindControl("divErrorImg");

            if (flag)
            {
                string filename = ((FileUpload)FVNoticia.FindControl("FUImagenes")).FileName;
                string extencion = Path.GetExtension(((FileUpload)FVNoticia.FindControl("FUImagenes")).FileName);
                if (extencion == ".jpg" || extencion == ".jpeg" || extencion == ".gif" || extencion == ".png" || extencion == ".mp4" )
                {

                    //
                    HttpPostedFile file = (HttpPostedFile) ( (FileUpload)FVNoticia.FindControl("FUImagenes")).PostedFile;
                    int iFileSize = file.ContentLength;
                    if (iFileSize > 15000000)  
                    {
                 
                        ((CustomValidator)source).ErrorMessage = "El tamaño de archivo es muy grande";
                        isvalid = false;
                    }
                    else //
                    isvalid = true;
                }
                else
                {
                    if (((TextBox)FVNoticia.FindControl("DescripTextBox")).Text.Length > 0)
                    {
                        isvalid = true;
                    }
                    else
                    {
                        isvalid = false;
                        ((TextBox)FVNoticia.FindControl("DescripTextBox")).Focus();

                    }
                }

                //div.Visible=!isvalid;
                args.IsValid = isvalid;
                flag = false;
                valid = args.IsValid;
            }
            args.IsValid = valid;
        }

       

    }
}

