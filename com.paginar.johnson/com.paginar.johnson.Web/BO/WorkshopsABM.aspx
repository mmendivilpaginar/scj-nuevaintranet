﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="WorkshopsABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.WorkshopsABM" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="../UserControl_Home/ucWorkShop.ascx" tagname="ucWorkShop" tagprefix="uc1" %>
<%@ Register assembly="FredCK.FCKeditorV2" namespace="FredCK.FCKeditorV2" tagprefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.AspNet-GridView {
    overflow: auto;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">

    <h2>WorkShops</h2>
    <asp:Label ID="AvisoCluster" runat="server" Text="Sección no habilitada" 
        Visible="False"></asp:Label>
    <asp:Panel ID="pnlContenido" runat="server">
    

    <asp:GridView ID="GVCursos" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="IdWorkshop" 
        DataSourceID="ODSGVWorkshop" 
        onselectedindexchanged="GVCursos_SelectedIndexChanged" PageSize="5">
        <Columns>
            <asp:CommandField SelectText="Ver/Editar" ShowSelectButton="True" />
            <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Objetivo" SortExpression="Objetivo">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Objetivo") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# cortarCadena(Eval("Objetivo").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Metodología" SortExpression="Metodologia">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Metodologia") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# cortarCadena(Eval("Metodologia").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Requisitos" SortExpression="Requisitos">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Requisitos") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# cortarCadena(Eval("Requisitos").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Duracion" HeaderText="Duración" 
                SortExpression="Duracion" />
            <asp:BoundField DataField="FechaInicioWorkshop" HeaderText="Período de Inicio" 
                SortExpression="FechaInicioWorkshop" />
            <asp:TemplateField HeaderText="Estado" SortExpression="IdEstado">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("IdEstado") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Estado(Eval("IdEstado").ToString()) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suscriptos" SortExpression="Cantidad">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Cantidad") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <a href="WorkshopsInscriptos.aspx?idws=<%# Eval("IdWorkshop").ToString() %>&Nombre=<%# Server.UrlEncode(Eval("Nombre").ToString()) %>" target="_blank"><asp:Label ID="Label6" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label></a>
                    <%--<asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl="~/BO/WorkshopsInscriptos.aspx?idws=<%# Eval("IdWorkshop").ToString() %>&Nombre=<%# Eval("Nombre").ToString() %>"><%# Eval("Cantidad").ToString() %></asp:HyperLink>--%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No hay datos disponibles en este momento.
        </EmptyDataTemplate>
    </asp:GridView>





    <asp:ObjectDataSource ID="ODSGVWorkshop" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="WorkshopSelectAll" 
        TypeName="com.paginar.johnson.BL.ControllerWorkShop" 
        ConvertNullToDBNull="True" DeleteMethod="WorkshopDelete" 
        InsertMethod="WorkshopInsert" UpdateMethod="WorkshopUpdate">
        <DeleteParameters>
            <asp:Parameter Name="IdWorkshop" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="Objetivo" Type="String" />
            <asp:Parameter Name="Metodologia" Type="String" />
            <asp:Parameter Name="Requisitos" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
            <asp:Parameter Name="IdEstado" Type="Int32" />
            <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
            <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="IdWorkshop" Type="Int32" />
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="Objetivo" Type="String" />
            <asp:Parameter Name="Metodologia" Type="String" />
            <asp:Parameter Name="Requisitos" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
            <asp:Parameter Name="IdEstado" Type="Int32" />
            <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
            <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
        </UpdateParameters>
    </asp:ObjectDataSource>





    &nbsp;<asp:Button ID="BTNAgregarCurso" runat="server" Text="Agregar Curso" 
        onclick="BTNAgregarCurso_Click" />
    &nbsp;&nbsp;
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="~/BO/ImprimirWorkshops.aspx" Target="_blank" 
        ToolTip="Imprimir / Exportar cursos activos">Imprimir / Exportar</asp:HyperLink>
    <br />
    <br />
    <%--<asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl="~/BO/WorkshopsInscriptos.aspx?idws=<%# Eval("IdWorkshop").ToString() %>&Nombre=<%# Eval("Nombre").ToString() %>"><%# Eval("Cantidad").ToString() %></asp:HyperLink>--%>
    <asp:FormView ID="FVWorkshop" runat="server" DataKeyNames="IdWorkshop" 
        DataSourceID="ODSFWShops" onitemdeleted="FVWorkshop_ItemDeleted" 
        oniteminserted="FVWorkshop_ItemInserted" 
        onitemupdated="FVWorkshop_ItemUpdated">
        <EditItemTemplate>
        <asp:Panel ID="Panel2" runat="server" DefaultButton="UpdateButton">
            <strong>Id:</strong>
            <br />
            &nbsp;<asp:Label ID="IdWorkshopLabel1" runat="server"
                Text='<%# Eval("IdWorkshop") %>' />
            <br />
            <strong>
            <br />
            Nombre:</strong><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
            <br />
            <strong>
            <br />
            Objetivo:</strong><br />
<%--            <asp:TextBox ID="ObjetivoTextBox" runat="server" 
                Text='<%# Bind("Objetivo") %>' />--%>
            <FCKeditorV2:FCKeditor ID="ObjetivoTextBox" runat="server" Value='<%# Bind("Objetivo") %>'>
            </FCKeditorV2:FCKeditor>
            <br />
            <strong>
            <br />
            Metodologia:</strong><br />
            <asp:TextBox ID="MetodologiaTextBox" runat="server" 
                Text='<%# Bind("Metodologia") %>' />
            <br />
            <strong>
            <br />
            Requisitos:</strong><br />
            <asp:TextBox ID="RequisitosTextBox" runat="server" 
                Text='<%# Bind("Requisitos") %>' />
            <br />
            <strong>
            <br />
            Duracion:</strong><br />
            <asp:TextBox ID="DuracionTextBox" runat="server" 
                Text='<%# Bind("Duracion") %>' />
            <br />
            <strong>
            <br />
            Fecha de Inicio:</strong><br />
            <asp:TextBox ID="FechaInicioWorkshopTextBox" runat="server" 
                Text='<%# Bind("FechaInicioWorkshop") %>' 
                SkinID="form-date" />
            <asp:ImageButton CausesValidation="false" ID="imgCalendar" runat="server" ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="FechaInicioWorkshopTextBox_CalendarExtender" 
                runat="server" Enabled="True" TargetControlID="FechaInicioWorkshopTextBox" 
                PopupButtonID="imgCalendar" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="FechaInicioWorkshopTextBox">*</asp:RequiredFieldValidator>
            <br />
            <br />
            <strong>Estado:</strong>
            <%--<asp:TextBox ID="IdEstadoTextBox" runat="server" 
                Text='<%# Bind("IdEstado") %>' />--%>
            <%--<asp:HiddenField ID="IdEstadoTextBox" runat="server" Value='<%# Bind("IdEstado") %>'/>--%>
            <asp:DropDownList ID="IdEstadoTextBox" runat="server"  
                Text='<%# Bind("IdEstado") %>'>
                <asp:ListItem Value="1">Pendiente</asp:ListItem>
                <asp:ListItem Value="2">Abierto</asp:ListItem>
                <asp:ListItem Value="3">Cerrado</asp:ListItem>
            </asp:DropDownList>
<%--            <br />
            PeriodoInscripcionInicio:
            <asp:TextBox ID="PeriodoInscripcionInicioTextBox" runat="server" 
                Text='<%# Bind("PeriodoInscripcionInicio") %>' />
            <br />
            PeriodoInscripcionFin:
            <asp:TextBox ID="PeriodoInscripcionFinTextBox" runat="server" 
                Text='<%# Bind("PeriodoInscripcionFin") %>' />
            <br />--%>
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Actualizar" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            
            </asp:Panel>
        </EditItemTemplate>
        <InsertItemTemplate>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="InsertButton">
            <strong>Nombre:</strong><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="NombreTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            <br />
            Objetivo:</strong><br />
<%--            <asp:TextBox ID="ObjetivoTextBox" runat="server" 
                Text='<%# Bind("Objetivo") %>' />--%>
                <FCKeditorV2:FCKeditor ID="ObjetivoTextBox" runat="server" Value='<%# Bind("Objetivo") %>'>
                </FCKeditorV2:FCKeditor>
<%--            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="ObjetivoTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>--%>
            <br />
            <strong>
            <br />
            Metodologia:</strong><br />
            <asp:TextBox ID="MetodologiaTextBox" runat="server" 
                Text='<%# Bind("Metodologia") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="MetodologiaTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            <br />
            Requisitos:</strong><br />
            <asp:TextBox ID="RequisitosTextBox" runat="server" 
                Text='<%# Bind("Requisitos") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ControlToValidate="RequisitosTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            <br />
            Duracion:</strong><br />
            <asp:TextBox ID="DuracionTextBox" runat="server" 
                Text='<%# Bind("Duracion") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                ControlToValidate="DuracionTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>
            <br />
            <strong>
            <br />
            Fecha de Inicio:</strong><br />
            <asp:TextBox ID="FechaInicioWorkshopTextBox" runat="server" 
                Text='<%# Bind("FechaInicioWorkshop") %>' 
                ReadOnly="False" SkinID="form-date" />
            <asp:ImageButton CausesValidation="false" ID="imgCalendar" runat="server" ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="FechaInicioWorkshopTextBox_CalendarExtender" 
                runat="server" Enabled="True" TargetControlID="FechaInicioWorkshopTextBox" 
                PopupButtonID="imgCalendar" Format="dd/MM/yyyy">
            </asp:CalendarExtender>

               
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                ControlToValidate="FechaInicioWorkshopTextBox" ErrorMessage="*">*</asp:RequiredFieldValidator>
            
            <br />
            <strong>
            <br />
            Estado:</strong><br />
            <%--<asp:TextBox ID="IdEstadoTextBox" runat="server" 
                Text='<%# Bind("IdEstado") %>' />--%>
            <asp:DropDownList ID="IdEstadoTextBox" runat="server"  
                Text='<%# Bind("IdEstado") %>'>
                <asp:ListItem Value="1">Pendiente</asp:ListItem>
                <asp:ListItem Value="2">Abierto</asp:ListItem>
                <asp:ListItem Value="3">Cerrado</asp:ListItem>
            </asp:DropDownList>
            <br />
<%--            PeriodoInscripcionInicio:
            <asp:TextBox ID="PeriodoInscripcionInicioTextBox" runat="server" 
                Text='<%# Bind("PeriodoInscripcionInicio") %>' />
            <br />
            PeriodoInscripcionFin:
            <asp:TextBox ID="PeriodoInscripcionFinTextBox" runat="server" 
                Text='<%# Bind("PeriodoInscripcionFin") %>' />
            <br />--%>
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="Agregar" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
            
            </asp:Panel>
        </InsertItemTemplate>
        <ItemTemplate>
            <strong>Id:</strong><br />
            <asp:Label ID="IdWorkshopLabel" runat="server" 
                Text='<%# Eval("IdWorkshop") %>' />
            <br />
            <br />
            <strong>Nombre:</strong><br />
            <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
            <br />
            <br />
            <strong>Objetivo:</strong><br />
            <asp:Label ID="ObjetivoLabel" runat="server" Text='<%# Bind("Objetivo") %>' />
            <br />
            <br />
            <strong>Metodologia:</strong><br />
            <asp:Label ID="MetodologiaLabel" runat="server" 
                Text='<%# Bind("Metodologia") %>' />
            <br />
            <br />
            <strong>Requisitos:</strong><br />
            <asp:Label ID="RequisitosLabel" runat="server" 
                Text='<%# Bind("Requisitos") %>' />
            <br />
            <br />
            <strong>Duración:</strong><br />
            <asp:Label ID="DuracionLabel" runat="server" Text='<%# Bind("Duracion") %>' />
            <br />
            <br />
            <strong>Fecha de inicio:</strong><br />
            <asp:Label ID="FechaInicioWorkshopLabel" runat="server" 
                Text='<%# Bind("FechaInicioWorkshop") %>' />
            <br />
            <br />
            <strong>Estado:</strong><br />
            <asp:Label ID="IdEstadoLabel" runat="server" Text='<%# Estado(Eval("IdEstado").ToString()) %>' />

            <br />

            <br />
<%--            PeriodoInscripcionInicio:
            <asp:Label ID="PeriodoInscripcionInicioLabel" runat="server" 
                Text='<%# Bind("PeriodoInscripcionInicio") %>' />
            <br />
            PeriodoInscripcionFin:
            <asp:Label ID="PeriodoInscripcionFinLabel" runat="server" 
                Text='<%# Bind("PeriodoInscripcionFin") %>' />
            <br />--%>
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                CommandName="Edit" Text="Editar" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" 
                CommandName="Delete" Text="Borrar" />
  <%--          &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                CommandName="New" Text="New" />--%>
        </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSFWShops" runat="server" 
        DeleteMethod="WorkshopDelete" InsertMethod="WorkshopInsert" 
        SelectMethod="WorkshopGetSpecific" 
        TypeName="com.paginar.johnson.BL.ControllerWorkShop" 
        UpdateMethod="WorkshopUpdate">
        <DeleteParameters>
            <asp:Parameter Name="IdWorkshop" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="Objetivo" Type="String" />
            <asp:Parameter Name="Metodologia" Type="String" />
            <asp:Parameter Name="Requisitos" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
            <asp:Parameter Name="IdEstado" Type="Int32" />
            <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
            <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVCursos" Name="IdWorkshop" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="IdWorkshop" Type="Int32" />
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="Objetivo" Type="String" />
            <asp:Parameter Name="Metodologia" Type="String" />
            <asp:Parameter Name="Requisitos" Type="String" />
            <asp:Parameter Name="Duracion" Type="String" />
            <asp:Parameter Name="FechaInicioWorkshop" Type="String" />
            <asp:Parameter Name="IdEstado" Type="Int32" />
            <asp:Parameter Name="PeriodoInscripcionInicio" Type="DateTime" />
            <asp:Parameter Name="PeriodoInscripcionFin" Type="DateTime" />
        </UpdateParameters>
    </asp:ObjectDataSource>

    <br />
    </asp:Panel>
    </asp:Content>
