﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;


namespace com.paginar.johnson.Web.BO
{
    public partial class VoluntariadoInscriptos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblNombreVoluntariado.Text = Server.UrlDecode(Request.QueryString[1].ToString());
        }

        protected void exportar_Click(object sender, EventArgs e)
        {
            string style = @"<style> .text { mso-number-format:\@; } </script> ";
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Inscriptos en " + lblNombreVoluntariado.Text + " " + DateTime.Now.ToShortDateString() + ".xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVInscriptos.RenderControl(htw);
            Response.Write(style);
            Response.Write(sw.ToString());
            Response.End();

        }

        protected void GVInscriptos_DataBound(object sender, EventArgs e)
        {

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        protected void GVInscriptos_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }


    }
}