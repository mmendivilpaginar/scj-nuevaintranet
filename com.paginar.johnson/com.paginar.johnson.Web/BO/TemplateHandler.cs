﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web
{
    public class TemplateHandler : ITemplate
    {
        int PasoID {get;set;} 
        int PeriodoID {get;set;}
        int TipoFormularioID { get; set; }

        public TemplateHandler(int PasoID, int PeriodoID, int TipoFormularioID)
        {
            this.PasoID = PasoID;
            this.PeriodoID = PeriodoID;
            this.TipoFormularioID = TipoFormularioID;
        }
        void ITemplate.InstantiateIn(Control container)
        {
             
            DropDownList cmd = new DropDownList();
            cmd.ID = string.Format( "cmd{0}",DateTime.Now.Ticks);
            cmd.DataSource = FachadaDA.Singleton.vwUsuarios.GetDataByPaso(this.PasoID, this.PeriodoID, this.TipoFormularioID);
            cmd.DataTextField = "ApellidoNombre";
            cmd.DataValueField = "Legajo";
          
            //cmd.Click += new EventHandler(Dynamic_Method);
            container.Controls.Add(cmd);

        }

        protected void Dynamic_Method(object sender, EventArgs e)
        {
            ((Button)sender).Text = "Hellooooo";
        }
    }
}