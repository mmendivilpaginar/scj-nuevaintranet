﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="ComunidadABM.aspx.cs" Inherits="com.paginar.johnson.Web.BO.ComunidadABM" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" language="javascript">
    function UserDeleteConfirmation() {
        return confirm("¿Esta seguro que desea eliminar este registro?");
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
    <div id="wrapperIndumentaria">
<h2>Relaciones con la Comunidad - Aliados</h2>
    <asp:GridView ID="GVAliados" runat="server" DataSourceID="ODSAliadosListado" 
        AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="idAliado">
        <Columns>
            <asp:BoundField DataField="idAliado" HeaderText="id" InsertVisible="False" 
                ReadOnly="True" SortExpression="idAliado" />
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                SortExpression="Nombre" />
            <asp:TemplateField HeaderText="Estado" SortExpression="Activo">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Activo") %>' />
                </EditItemTemplate>
                <ItemTemplate>
<%--                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Activo") %>' 
                        Enabled="false" />--%>
                    <asp:Literal ID="Literal1" runat="server" Text='<%# retornaEstado(Eval("Activo")) %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButtonModificar" runat="server"  ToolTip="Ver más" CommandName="Select"
                        ImageUrl="~/images/verMas.png" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <EmptyDataTemplate>
            En estos momentos no hay Aliados
        </EmptyDataTemplate>
        
    </asp:GridView>
    <asp:ObjectDataSource ID="ODSAliadosListado" runat="server" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="com_aliadosSelectAll" 
        TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad">
    </asp:ObjectDataSource>
    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onclick="btnNuevo_Click" />
    <asp:FormView ID="FVAliado" runat="server" DataKeyNames="idAliado" 
        DataSourceID="ODSAliadoSeleccionado" onitemcreated="FVAliado_ItemCreated" 
            onitemdeleted="FVAliado_ItemDeleted" oniteminserted="FVAliado_ItemInserted" 
            onitemupdated="FVAliado_ItemUpdated">
        <EditItemTemplate>
            <strong>id:</strong><br />
            <asp:Label ID="idAliadoLabel1" runat="server" Text='<%# Eval("idAliado") %>' />
            <br />
            <strong>Nombre:</strong><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="*" ValidationGroup="a" ControlToValidate="NombreTextBox">*</asp:RequiredFieldValidator>
            <br />
            <strong>Que Hace:</strong><br />
            <asp:TextBox ID="QueHaceTextBox" runat="server" Text='<%# Bind("QueHace") %>' 
                Rows="4" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ErrorMessage="*" ValidationGroup="a" ControlToValidate="QueHaceTextBox">*</asp:RequiredFieldValidator>
            <br />
            <strong>Que vamos a hacer:</strong><br />
            <asp:TextBox ID="QueHaraTextBox" runat="server" Text='<%# Bind("QueHara") %>' 
                Rows="4" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ErrorMessage="*" ValidationGroup="a" ControlToValidate="QueHaraTextBox">*</asp:RequiredFieldValidator>
            <br />
            <strong>web:</strong><br />
            <asp:TextBox ID="webTextBox" runat="server" Text='<%# Bind("web") %>' />
            <br />
            <strong>Activo:</strong><br />
            <asp:CheckBox ID="ActivoCheckBox" runat="server" 
                Checked='<%# Bind("Activo") %>' />
            <br />
            <strong>Logo:</strong><br />&nbsp;<asp:TextBox ID="imgLogoTextBox" runat="server" Text='<%# Bind("imgLogo") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ErrorMessage="*" ValidationGroup="a" ControlToValidate="imgLogoTextBox">*</asp:RequiredFieldValidator>
            <br />
            <asp:FileUpload ID="FUImagen" runat="server" /><br />
            <asp:Button ID="Button1" runat="server" Text="Subir Imagen" onclick="Button1_Click" 
                CausesValidation="False" />
            <br />
            <strong>Tipo</strong>
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("tipo") %>'>
                <asp:ListItem Value="1">Prevención de enfermedades infecciosas</asp:ListItem>
                <asp:ListItem Value="2">Cuidado Medio Hambiental</asp:ListItem>
                <asp:ListItem Value="3">Apoyo a la Educación</asp:ListItem>
                <asp:ListItem Value="4">Formación para el trabajo</asp:ListItem>
                <asp:ListItem Value="5">Mejora de la calidad de vida</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="Actualizar" ValidationGroup="a"/>
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="Cancelar" />
        </EditItemTemplate>
        <InsertItemTemplate>
            <strong>Nombre:</strong><br />
            <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                ErrorMessage="*" ValidationGroup="a" ControlToValidate="NombreTextBox">*</asp:RequiredFieldValidator>
            <br />
            <strong>Que Hace:</strong><br />
            <asp:TextBox ID="QueHaceTextBoxInsert" runat="server" 
                Text='<%# Bind("QueHace") %>' Rows="4" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                ErrorMessage="*" ValidationGroup="a" 
                ControlToValidate="QueHaceTextBoxInsert">*</asp:RequiredFieldValidator>
            <br />
            <strong>Que vamos a hacer:</strong><br />
            <asp:TextBox ID="QueHaraTextBoxInsert" runat="server" 
                Text='<%# Bind("QueHara") %>' Rows="4" TextMode="MultiLine" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                ErrorMessage="*" ValidationGroup="a" 
                ControlToValidate="QueHaraTextBoxInsert">*</asp:RequiredFieldValidator>
            <br />
            <strong>web:</strong><br />
            <asp:TextBox ID="webTextBox" runat="server" Text='<%# Bind("web") %>' />
            <br />
            <strong>Activo:</strong><br />
            <asp:CheckBox ID="ActivoCheckBoxInsert" runat="server" 
                Checked='<%# Bind("Activo") %>' />
            <br />
            <strong>Logo:</strong><br />
            <asp:TextBox ID="imgLogoTextBoxInsert" runat="server" 
                Text='<%# Bind("imgLogo") %>' />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                ErrorMessage="*" ValidationGroup="a" 
                ControlToValidate="imgLogoTextBoxInsert">*</asp:RequiredFieldValidator>
            <br />
            <asp:FileUpload ID="FUImagen" runat="server" /><br />
            <asp:Button ID="Button1" runat="server" Text="Subir Imagen" onclick="Button1_Click" 
                CausesValidation="False" />
            <br />
            <strong>Tipo</strong>
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("tipo") %>'>
                <asp:ListItem Value="1">Prevención de enfermedades infecciosas</asp:ListItem>
                <asp:ListItem Value="2">Cuidado Medio Hambiental</asp:ListItem>
                <asp:ListItem Value="3">Apoyo a la Educación</asp:ListItem>
                <asp:ListItem Value="4">Formación para el trabajo</asp:ListItem>
                <asp:ListItem Value="5">Mejora de la calidad de vida</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" ValidationGroup="a"
                CausesValidation="True" CommandName="Insert" Text="Aceptar" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" 
                CommandName="Cancel" Text="Cancelar" />
        </InsertItemTemplate>
        <ItemTemplate>
            <strong>id:</strong><br />
            <asp:Label ID="idAliadoLabel" runat="server" Text='<%# Eval("idAliado") %>' />
            <br />
            <strong>Nombre:</strong><br />
            <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
            <br />
            <strong>Que hace:</strong><br />
            <asp:Label ID="QueHaceLabel" runat="server" Text='<%# Bind("QueHace") %>' />
            <br />
            <strong>Que vamos a hacer:</strong><br />
            <asp:Label ID="QueHaraLabel" runat="server" Text='<%# Bind("QueHara") %>' />
            <br />
            <strong>web:</strong><br />
            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank" NavigateUrl='<%# Bind("web") %>' Text='<%# Bind("web") %>'> </asp:HyperLink>
                        
            <br />
            <strong>Activo:</strong><br />
            <asp:CheckBox ID="ActivoCheckBox" runat="server" 
                Checked='<%# Bind("Activo") %>' Enabled="false" />
            <br />
            <strong>Logo:</strong><br />
            <asp:Image ID="Image1" runat="server" ImageUrl='<%# RutaImagen(Eval("imgLogo")) %>' Width="60px" />
            <%--<asp:Label ID="imgLogoLabel" runat="server" Text='<%# Bind("imgLogo") %>' />--%>
            <br />
            <strong>Tipo</strong>
            <br />
            <asp:DropDownList ID="DropDownList1" runat="server" 
                SelectedValue='<%# Eval("tipo") %>' Enabled="False">
                <asp:ListItem Value="1">Prevención de enfermedades infecciosas</asp:ListItem>
                <asp:ListItem Value="2">Cuidado Medio Hambiental</asp:ListItem>
                <asp:ListItem Value="3">Apoyo a la Educación</asp:ListItem>
                <asp:ListItem Value="4">Formación para el trabajo</asp:ListItem>
                <asp:ListItem Value="5">Mejora de la calidad de vida</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" 
                CommandName="Edit" Text="Editar" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" OnClientClick="if(!UserDeleteConfirmation()) return false;"
                CommandName="Delete" Text="Borrar" />
<%--            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" 
                CommandName="New" Text="New" />
--%>
     </ItemTemplate>
    </asp:FormView>
    <asp:ObjectDataSource ID="ODSAliadoSeleccionado" runat="server" 
        DeleteMethod="com_aliadosDelete" InsertMethod="com_aliadosInsert" 
        OldValuesParameterFormatString="{0}" 
        SelectMethod="com_aliadosSelectById" 
        TypeName="com.paginar.johnson.BL.ControllerAliadosComunidad" 
        UpdateMethod="com_aliadosUpdate">
        <DeleteParameters>
            <asp:Parameter Name="idAliado" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="QueHace" Type="String" />
            <asp:Parameter Name="QueHara" Type="String" />
            <asp:Parameter Name="web" Type="String" />
            <asp:Parameter Name="Activo" Type="Boolean" />
            <asp:Parameter Name="imgLogo" Type="String" />
            <asp:Parameter Name="tipo" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GVAliados" Name="idAliado" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="idAliado" Type="Int32" />
            <asp:Parameter Name="Nombre" Type="String" />
            <asp:Parameter Name="QueHace" Type="String" />
            <asp:Parameter Name="QueHara" Type="String" />
            <asp:Parameter Name="web" Type="String" />
            <asp:Parameter Name="Activo" Type="Boolean" />
            <asp:Parameter Name="imgLogo" Type="String" />
            <asp:Parameter Name="tipo" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
</div>
</asp:Content>
