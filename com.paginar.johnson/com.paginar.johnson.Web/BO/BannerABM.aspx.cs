﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using AjaxControlToolkit.HTMLEditor;

namespace com.paginar.johnson.Web.BO
{
    public partial class BannerABM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        

        protected void gvBanners_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Editar")
            {
                hdBannerID.Value = e.CommandArgument.ToString();
                frmBanner.ChangeMode(FormViewMode.Edit);
                frmBanner.Visible = true;
                frmBanner.DataBind(); 
            }

            if (e.CommandName == "Eliminar")
            {
                hdBannerID.Value = e.CommandArgument.ToString();
                ControllerNoticias cn = new ControllerNoticias();
                cn.BannerClusterDelete(int.Parse(hdBannerID.Value));
                cn.BannerDelete(int.Parse(hdBannerID.Value));
                gvBanners.DataBind();
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            frmBanner.ChangeMode(FormViewMode.Insert);            
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {

            FileUpload fuImage = frmBanner.FindControl("fuImage") as FileUpload;
            Image Image = frmBanner.FindControl("Image") as Image;
            HiddenField HdImagen = frmBanner.FindControl("HdImagen") as HiddenField;

            if ((fuImage.HasFile) && (verificar_contenttype(fuImage.PostedFile)))
            {
                string[] filename = fuImage.FileName.Split('.');
                string file = DateTime.Now.Ticks.ToString() + "." + filename[1];
                fuImage.SaveAs(Server.MapPath("../noticias/Imagenes/" + file));
                Image.Visible = true;
                Image.ImageUrl = "../noticias/Imagenes/" + file;
                HdImagen.Value = file;
            }
           
        }
        

        #region Imagen
 
        public Boolean verificar_contenttype(HttpPostedFile file)
        {
            bool aux = false;
            switch (file.ContentType)
            {
                case "image/jpeg": aux = true;
                    break;
                case "image/jpg": aux = true;
                    break;
                case "image/jp_": aux = true;
                    break;
                case "image/pjpeg": aux = true;
                    break;
                case "image/pipeg": aux = true;
                    break;
                case "image/gif": aux = true;
                    break;
                default: aux = false;
                    break;
            }
            return aux;
        }
        protected void cvImg_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FileUpload fuImage = frmBanner.FindControl("fuImage") as FileUpload;

            if (verificar_contenttype(fuImage.PostedFile))
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        #endregion




        protected void odsBanner_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            int bannerid = (int)e.ReturnValue;

             GuardarBannerCluster(bannerid);

            frmBanner.Visible = false;
            gvBanners.DataBind();
        }

        protected void odsBanner_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            int bannerid = int.Parse(hdBannerID.Value);

            GuardarBannerCluster(bannerid);

            frmBanner.Visible = false;
            gvBanners.DataBind();
        }

        protected void GuardarBannerCluster(int bannerid)
        {
           Repeater rp = frmBanner.FindControl("rpCluster") as Repeater;
           foreach (RepeaterItem item in rp.Items)
           {
               HiddenField hd = item.FindControl("hdCluster") as HiddenField;
               CheckBox chk = item.FindControl("chkCluster") as CheckBox;
               ControllerNoticias cn = new ControllerNoticias();
               if (chk.Checked)
               {
                   cn.BannerClusterAdd(bannerid, int.Parse(hd.Value));
               }
               else
               {
                   cn.BannerClusterDelete(bannerid, int.Parse(hd.Value));
               }
           }
        }


        protected void frmBanner_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["FHAlta"] = DateTime.Now;
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FileUpload fuImage = frmBanner.FindControl("fuImage") as FileUpload;
            Editor  edTitulo = frmBanner.FindControl("edTitulo") as Editor;
            Editor  edCuerpo = frmBanner.FindControl("edCuerpo") as Editor;
            args.IsValid = fuImage.HasFile || (!string.IsNullOrEmpty(edTitulo.Content)) || (!string.IsNullOrEmpty(edCuerpo.Content));
        }


    }
}