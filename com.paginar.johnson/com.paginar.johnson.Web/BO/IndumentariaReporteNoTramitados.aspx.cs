﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing;

namespace com.paginar.johnson.Web.BO
{
    public partial class IndumentariaReporteNoTramitados : System.Web.UI.Page
    {
        ControllerIndumentaria CI = new ControllerIndumentaria();
        protected void Page_Load(object sender, EventArgs e)
        {
            DDLPeriodo_DataBound(null, null);
        }

        private string procesarPeriodo(DateTime Desde, DateTime Hasta)
        {
            string fecha = String.Format("{0:dd}-{0:MM}-{0:yyyy}", Desde, Desde, Desde);
            fecha = "Desde  " + fecha + " hasta " + String.Format("{0:dd}-{0:MM}-{0:yyyy}", Hasta, Hasta, Hasta);
            return fecha;

        }


        protected void DDLPeriodo_DataBound(object sender, EventArgs e)
        {
            //DDLPeriodo.Items.Add(new ListItem("Seleccione",""));
            foreach (DSIndumentaria.Indumentaria_PeriodoRow item in ((DSIndumentaria.Indumentaria_PeriodoDataTable)CI.SeleccionarPeriodosTodos()).Rows)
            {
                DDLPeriodo.Items.Add(new ListItem(procesarPeriodo(item.Desde, item.Hasta), item.PeriodoId.ToString()));
            }
        }

        private void ExportAExcel(string nombreDelReporte, GridView wControl)
        {


            wControl.AllowPaging = false;

            wControl.HeaderStyle.BackColor.Equals(Color.Red);
            wControl.HeaderStyle.ForeColor.Equals(Color.Red);
            wControl.HeaderStyle.BorderColor.Equals(Color.Red);



            wControl.DataBind();


            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            Literal litFechaActual = new Literal();
            litFechaActual.ID = "litFchs";
            litFechaActual.Text = "<br /><strong>Fecha de Generación:</strong> " + DateTime.Now.ToShortDateString() + "<br />";
            Label LF1 = new Label();
            LF1.ID = "LBF1";

            Literal litArea = new Literal();

            if (DDLPeriodo.SelectedItem.ToString() != "Seleccione")
            {
                LF1.Text = "<strong>Período:</strong> " + DDLPeriodo.SelectedItem.ToString();
            }
            else
            {
                LF1.Text = "<strong>Período:</strong> Todos";
            }


            litFechaActual.RenderControl(htw);
            LF1.RenderControl(htw);

            litArea.RenderControl(htw);

            form.Controls.Add(litFechaActual);
            form.Controls.Add(LF1);
            
            form.Controls.Add(litArea);
            //form.Controls.Add();
            form.Controls.Add(wControl);

            pageToRender.Controls.Add(new LiteralControl(
                             @"<html>
                              <head>
<style>                                    

body 
{
    text-decoration: none;
	line-height: 1.3;
    color: #000000 !Important;
    font-family: Arial;
}
a {
    color:#000000;
}

h1 {
    background-color:#CFCFCF;
    layer-background-color:#CFCFCF;
}

h2 {
    background-color:#CFCFCF;
    color: #558EC6;
    margin-bottom: 20px;
}
h2 {
    color:#000000;
    font-size:45px;
}
h3 {
    color:#000000;
    margin-bottom: 10px;    
    clear:both;
}
h4 {
    color:#000000;
    font-size:13px;/*antes 14px*/
    margin-bottom: 10px;
}
p 
{
    color:#000000;
    margin-bottom: 15px;
}

.AspNet-GridView 
{
    text-decoration: none;
    margin-bottom:10px;
    clear:both;
}
.AspNet-GridView table{
    width:100%;
}
.AspNet-GridView table th,
.AspNet-GridView table td 
{
    text-decoration: none;
    padding:5px;
    border:1px solid #D9D9D9;
    color: #000000 !important;
    font-size:12px;
}

.AspNet-GridView th
{
    text-decoration: none;
    background-color:#E6E6E6;
    color:#000000;
    text-transform:uppercase;
    font-weight:normal;
}

</style>
</head><body>"));

            pageToRender.Controls.Add(new LiteralControl(
                @"<div id='header'><div><h1>" + nombreDelReporte + " al " + DateTime.Now.ToShortDateString() + "</h1></div></div>"));

            pageToRender.Controls.Add(form);

            pageToRender.Controls.Add(new LiteralControl(@"
                                  
                              </body>
                              </html>"));

            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment;filename=" + nombreDelReporte + " al " + DateTime.Now.ToShortDateString() + ".xls");
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);

            response.Write(sw.ToString());

            response.End();
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            GVReporteNoTramitados.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ExportAExcel("Ropa de Trabajo - Tramites no iniciados", GVReporteNoTramitados);
        }
    }
}