﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.BO
{
    public partial class ImprimirNewsletter :  System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                string restado = Request["Estado"].ToString();
                string rnombre = Request["Nombre"].ToString();
                string rfhdesde = Request["FHDesde"].ToString();
                string rfhhasta = Request["FHHasta"].ToString();

                ControllerNewsletter cn = new ControllerNewsletter();

                int? estado = null;
                if (restado != "-1" && restado!="")
                    estado = int.Parse(restado);

                DateTime? fhdesde = null;
                if (rfhdesde != "")
                    fhdesde = DateTime.Parse(rfhdesde.Trim());

                DateTime? fhhasta = null;
                if (rfhhasta != "")
                    fhhasta = DateTime.Parse(rfhhasta.Trim());

                gvNewsletter.DataSource = cn.Buscar(estado, fhdesde, fhhasta, rnombre.Trim());
                gvNewsletter.DataBind();
                    

            }
        }
    }
}