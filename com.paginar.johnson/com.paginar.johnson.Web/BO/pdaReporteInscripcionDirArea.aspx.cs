﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.BO
{
    public partial class pdaReporteInscripcionDirArea : System.Web.UI.Page
    {
        ControllerPDA CPDA = new ControllerPDA();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Curso.Value = Request.QueryString["idCurso"].ToString();
                HFArea.Value = Request.QueryString["areaid"].ToString();
                HFDireccion.Value = Request.QueryString["dirid"].ToString();
                lblCurso.Text = curso(int.Parse(Curso.Value));
                if (Request.QueryString["direccion"] != null && Request.QueryString["direccion"].ToString() != "")
                {
                    LabelTituloDireccion.Visible = true;
                    LabelDireccion.Visible = true;
                    LabelDireccion.Text = Request.QueryString["direccion"].ToString();
                }
                else
                {
                    LabelTituloDireccion.Visible = false;
                    LabelDireccion.Visible = false;                
                }

                if (Request.QueryString["area"] != null && Request.QueryString["area"].ToString() != "")
                {
                    lblAreaTit.Visible = true;
                    lblArea.Text = Request.QueryString["area"].ToString();
                
                }
                else
                {
                    lblAreaTit.Visible = false;
                    lblArea.Visible = false;
                
                }

            }
        }


        protected string area(string areaid)
        {
            string valorRetorno = string.Empty;

            DAL.DSPda.AreasDataTable ADT = CPDA.getAreasByDireccionId(int.Parse(areaid));
            DAL.DSPda.AreasRow ARWAux;
            ARWAux = ADT.FindByAreaID(int.Parse(areaid));
            valorRetorno = ARWAux.AreaDESC;
            return valorRetorno;
        }

        protected string curso(int curso)
        {
            string valorRetorno = string.Empty;

            DAL.DSPda.pda_cursoDataTable ADT = CPDA.pda_cursoSelectByID(curso);
            DAL.DSPda.pda_cursoRow ARWAux;
            ARWAux = ADT.FindByidCurso(curso);
            valorRetorno = ARWAux.Nombre;
            return valorRetorno;
        }

       

        protected string direccion(string dirid)
        {
            string valorRetorno = string.Empty;

            //DAL.DSPda.AreasDataTable
            DAL.DSPda.DireccionDataTable DDT = CPDA.getDirecciones();
            DAL.DSPda.DireccionRow DDTAux;
            DDTAux = DDT.FindByDireccionID(int.Parse(dirid));

            valorRetorno = DDTAux.DireccionDET;
            return valorRetorno;
        }
    }
}