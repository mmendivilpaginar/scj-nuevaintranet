﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;

namespace com.paginar.johnson.Web.BO
{
    public partial class UsuariosAdmCompensatorios : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                AvisoCluster.Visible = false;
                if (this.ObjectUsuario.clusteriddefecto != 1)
                {
                    AvisoCluster.Visible = true;
                    pnlContenido.Visible = false;
                }
                else
                {
                    pnlContenido.Visible = true;
                }
            }

        }

        private void ExportAExcel(string nombreDelReporte, GridView wControl)
        {

            wControl.AllowPaging = false;
            wControl.DataBind();
            HttpResponse response = Response;
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pageToRender = new Page();
            HtmlForm form = new HtmlForm();
            Label L = new Label();
            if (DDLArea.SelectedIndex != 0)
                L.Text = "Usuarios con Compensatorios por Área: " + DDLArea.SelectedItem.ToString();
            else
                L.Text = "Usuarios con Compensatorios";
            form.Controls.Add(L);
            form.Controls.Add(wControl);
            pageToRender.Controls.Add(new LiteralControl(
                                        @"<html>
                                <head>
            <style>                                    

            body 
            {
            text-decoration: none;
            line-height: 1.3;
            color: #000000 !Important;
            font-family: Arial;
            }
            a {
            color:#000000;
            }

            h1 {
            background-color:#CFCFCF;
            layer-background-color:#CFCFCF;
            }

            h2 {
            background-color:#CFCFCF;
            color: #558EC6;
            margin-bottom: 20px;
            }
            h2 {
            color:#000000;
            font-size:45px;
            }
            h3 {
            color:#000000;
            margin-bottom: 10px;    
            clear:both;
            }
            h4 {
            color:#000000;
            font-size:13px;/*antes 14px*/
            margin-bottom: 10px;
            }
            p 
            {
            color:#000000;
            margin-bottom: 15px;
            }

            .AspNet-GridView 
            {
            text-decoration: none;
            margin-bottom:10px;
            clear:both;
            }
            .AspNet-GridView table{
            width:100%;
            }
            .AspNet-GridView table th,
            .AspNet-GridView table td 
            {
            text-decoration: none;
            padding:5px;
            border:1px solid #D9D9D9;
            color: #000000 !important;
            font-size:12px;
            }

            .AspNet-GridView th
            {
            text-decoration: none;
            background-color:#E6E6E6;
            color:#000000;
            text-transform:uppercase;
            font-weight:normal;
            }

            </style>
            </head><body>"));
            pageToRender.Controls.Add(form);
            pageToRender.Controls.Add(new LiteralControl(@"
                                  
                              </body>
                              </html>"));
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.ms-excel";
            if (DDLArea.SelectedIndex != 0)
                response.AddHeader("Content-Disposition", "attachment;filename=" + nombreDelReporte + " al " + DateTime.Now.ToShortDateString() + " filtrado por " + DDLArea.SelectedItem.ToString() + ".xls");
            else
                response.AddHeader("Content-Disposition", "attachment;filename=" + nombreDelReporte + " al " + DateTime.Now.ToShortDateString() + ".xls");
            response.Charset = "UTF-8";
            response.ContentEncoding = Encoding.Default;
            pageToRender.RenderControl(htw);
            response.Write(sw.ToString());
            response.End();
        }

        protected void BuscarButton_Click(object sender, EventArgs e)
        {
            GVUsuariosCompensatorios.DataBind();
        }

        protected void ButtonDescarga_Click(object sender, EventArgs e)
        {
            ExportAExcel("Listado Usuarios con Compensatorios", GVUsuariosCompensatorios);
        }

        protected string retornaSiNo(object valor)
        {
            string retorno = string.Empty;
            if (valor.ToString().ToLower() == "true")
                retorno = "Si";
            else
                retorno = "No";
            return retorno;
        }
    }
}