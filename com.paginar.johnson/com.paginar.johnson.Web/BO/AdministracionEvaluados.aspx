﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BO/MasterBO.Master" AutoEventWireup="true" CodeBehind="AdministracionEvaluados.aspx.cs" Inherits="com.paginar.johnson.Web.BO.AdministracionEvaluados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

  <script type="text/javascript" src="../js/jquery.sb.js"></script>
  <link href="../css/sb.css" rel="stylesheet" type="text/css" />
  

<script type="text/javascript" >
    $(document).ready(function () {

        SetearDropDown("<%= GridViewUsuarios.ClientID %>");
        
    });

    function SetearDropDown(grillaAuxi) {
        var DropDownListLT;
        var DropDownListJP;
        var DropDownListSectores;
        var DropDownListCargos;

        // var grillaAuxi =
        //esto recorre cada fila de la tabla  y por cada una de ellas busca los selects
        $('#' + grillaAuxi + ' table tbody tr').each(function () {
            var $DropDownListLT = $(this).find("select[id*='DropDownListLT']");
            var $DropDownListJP = $(this).find("select[id*='DropDownListJP']");
            var $DropDownListSectores = $(this).find("select[id*='DropDownListSectores']");
            var $DropDownListCargos = $(this).find("select[id*='DropDownListCargos']");

            if ($DropDownListLT != null)
                $DropDownListLT.sb({
                    fixedWidth: true
                });

            if ($DropDownListJP != null)
                $DropDownListJP.sb({
                    fixedWidth: true
                });

            if ($DropDownListSectores != null)
                $DropDownListSectores.sb({
                    fixedWidth: true
                });

            if ($DropDownListCargos != null)
                $DropDownListCargos.sb({
                    fixedWidth: true
                });

        }); //fin each

            

    }

   // });

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPMAIN" runat="server">
   

    <script type="text/javascript">
        var __modificado = 0;

        function CheckEdit(sender, args) {
            if ($("#<%= GridViewUsuarios.ClientID %> AspNet-GridView-Edit").length == 0) return;
       
            args.IsValid = false;
        }

        function VerificarCambios() {
            if (__modificado == 0) return true;
            if (confirm('Cuenta con cambios realizados. ¿Desea grabar los mismos antes de continuar con la nueva búsqueda?')) {
                var DropDownListTipoFormularioClienteID = '<%=DropDownListTipoFormulario.ClientID %>';
                var DropDownListTipoFormulario = document.getElementById(DropDownListTipoFormularioClienteID);
                var HiddenFieldTipoFormularioClienteID = '<%=HiddenFieldTipoFormularioID.ClientID %>';
                var HiddenFieldTipoFormulario = document.getElementById(HiddenFieldTipoFormularioClienteID);
                DropDownListTipoFormulario.value = HiddenFieldTipoFormulario.value;
                return false;
            }
            return true; //(!confirm('Cuenta con cambios realizados. ¿Desea grabar los mismos antes de continuar con la nueva búsqueda?'));
        }
    </script>

    
    <asp:HiddenField ID="HiddenFieldMODO" Value="BUSCAR" runat="server" />
    <asp:HiddenField ID="HiddenFieldClusterID" Value="" runat="server" />
   
    <h2>
        Módulo de Importación de Evaluados</h2>
<%--    <asp:UpdatePanel ID="UpdatePanelFiltros" runat="server">
        <ContentTemplate>--%>
              <div class="form-item leftthird"><label>Tipo Período</label>
              <asp:DropDownList ID="DropDownListTipoPeriodo" runat="server" AutoPostBack="True" 
                      onselectedindexchanged="DropDownListTipoPeriodo_SelectedIndexChanged">
                                        <asp:ListItem Value="1">Full Year Operarios</asp:ListItem>
                                        <asp:ListItem Value="3">Full Year Administrativos</asp:ListItem>
                                        <asp:ListItem Value="2">Mid Year</asp:ListItem>                                        
              </asp:DropDownList> 
    </div>
    <div class="form-item middlethird">

            <label>Período</label><asp:DropDownList ID="DropDownListPeriodo"
        runat="server" DataSourceID="ObjectDataSourcePeriodos"
            DataTextField="Descripcion" DataValueField="PeriodoID" 
        OnDataBound="DropDownListPeriodo_DataBound" AutoPostBack="True" 
                onselectedindexchanged="DropDownListPeriodo_SelectedIndexChanged" >
        </asp:DropDownList>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodos" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetDataByTipoPeriodoID" 
                  
                  TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
                  DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
            <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
            <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownListTipoPeriodo" Name="TipoPeriodoID" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
            <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
            <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:UpdatePanel RenderMode="Inline" ID="UpdatePanelPeriodo" runat="server" 
            UpdateMode="Conditional">
        <ContentTemplate>           
            <asp:Repeater ID="FormView1" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
                <ItemTemplate>
                    <strong class="destacados">
                    <span>
                    (<asp:Label ID="CargaDesdeLabel" runat="server" 
                Text='<%# Bind("CargaDesde","{0:dd/MM/yyyy}") %>' />
                    -
                    <asp:Label ID="CargaHastaLabel" runat="server" 
                Text='<%# Bind("CargaHasta","{0:dd/MM/yyyy}") %>' />)
                </span>
                    </strong>
                    
                </ItemTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetPeriodoByID" 
                TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
           
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="DropDownListPeriodo" 
                EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="DropDownListTipoFormulario" 
                EventName="SelectedIndexChanged" />
        </Triggers>
</asp:UpdatePanel>
      </div>

    <div class="form-item rightthird">
    <label>Formulario</label>
    <asp:DropDownList ID="DropDownListTipoFormulario" runat="server" DataSourceID="ObjectDataSourceTiposFormularios"
        DataTextField="Descripcion" DataValueField="TipoFormularioID" 
        onselectedindexchanged="DropDownListTipoFormulario_SelectedIndexChanged">
    </asp:DropDownList>
     <asp:ObjectDataSource ID="ObjectDataSourceTiposFormularios" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetTiposFormulariosByTipoPeriodoID" 
            TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj">
         <SelectParameters>
             <asp:ControlParameter ControlID="DropDownListTipoPeriodo" Name="TipoPeriodoID" 
                 PropertyName="SelectedValue" Type="Int32" />
         </SelectParameters>
    </asp:ObjectDataSource>
    </div>
       <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value="1" />
    <asp:HiddenField ID="HiddenFieldTipoPeriodoID" runat="server" />
   
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>


 
    <div class="controls">
    <asp:Button SkinID="BotonUsuarioBo" ID="ButtonBuscar" runat="server" Text="Buscar"
        OnClick="ButtonBuscar_Click" />
        </div>
    <asp:MultiView ID="MultiViewSincro" runat="server">
        <asp:View ID="ViewGrilla" runat="server">
            <asp:GridView ID="GridViewUsuarios" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceUsuarios"
                OnRowDataBound="GridViewUsuarios_RowDataBound" OnRowCommand="GridViewUsuarios_RowCommand"
                OnDataBound="GridViewUsuarios_DataBound" 
                OnDataBinding="GridViewUsuarios_DataBinding" 
                onrowediting="GridViewUsuarios_RowEditing">
                <Columns>
                    <asp:TemplateField HeaderText="Legajo" SortExpression="LEGAJO">
                        <ItemTemplate>
                            <asp:Label ID="LegajoLabel" runat="server" Text='<%# Bind("LEGAJO") %>'></asp:Label>
                             <asp:HiddenField ID="HiddenFieldJobGrade" runat="server"  Value='<%# Eval("JobGrade") %>'/>
                            <asp:HiddenField ID="HiddenFieldLinea" runat="server"  Value='<%# Eval("Linea")  %>'/>
                            <asp:HiddenField ID="HiddenFieldDireccion" runat="server" Value='<%# Eval("Direccion")  %>' />
                            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server" Value='<%# Eval("DireccionID")  %>' />
                           <asp:HiddenField ID="HiddenFieldLegajoReal" runat="server" Value='<%# Eval("LegajoReal")  %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="LegajoLabel" runat="server" Text='<%# Bind("LEGAJO") %>'></asp:Label>
                        <asp:HiddenField ID="HiddenFieldJobGrade" runat="server"  Value='<%# Eval("JobGrade") %>'/>
                        <asp:HiddenField ID="HiddenFieldLinea" runat="server"  Value='<%# Eval("Linea")  %>'/>
                        <asp:HiddenField ID="HiddenFieldDireccion" runat="server" Value='<%# Eval("Direccion")  %>' />
                        <asp:HiddenField ID="HiddenFieldDireccionID" runat="server" Value='<%# Eval("DireccionID")  %>' />
                        <asp:HiddenField ID="HiddenFieldLegajoReal" runat="server" Value='<%# Eval("LegajoReal")  %>' />
                        </EditItemTemplate>
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Apellido">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBoxApellido" runat="server" Text='<%# Bind("Apellido") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBoxNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Estado" HeaderText="Estado" SortExpression="Estado" ReadOnly="True">
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Evaluador">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenFieldEstado" runat="server" Value='<%# Eval("EstadoID") %>' />
                            <asp:HiddenField ID="HiddenFieldInterno" runat="server" Value='<%# Eval("Interno") %>' />
                            <asp:HiddenField ID="HiddenFieldFoto" runat="server" Value='<%# Eval("Foto") %>' />
                            <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("LEGAJO") %>' />
                            <asp:HiddenField ID="HiddenFieldNombre" runat="server" Value='<%# Eval("Nombre") %>' />
                            <asp:HiddenField ID="HiddenFieldEvaluador" runat="server" Value='<%# Eval("LT") %>' />
                            <asp:DropDownList AppendDataBoundItems="True" ID="DropDownListLT" runat="server"
                                 DataTextField="ApellidoNombre"
                                DataValueField="Legajo" >
                                <asp:ListItem Value="">- Seleccionar -</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLT" runat="server" ErrorMessage="*" ValidationGroup="Grabar"
                            ControlToValidate="DropDownListLT" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <%--<asp:CompareValidator ID="CompareValidatorLT" runat="server" ControlToValidate="DropDownListLT"
                                Display="Dynamic" Operator="NotEqual" ValidationGroup="Grabar" ValueToCompare="-1">*</asp:CompareValidator>--%>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="HiddenFieldEstado" runat="server" Value='<%# Eval("EstadoID") %>' />
                            <asp:HiddenField ID="HiddenFieldInterno" runat="server" Value='<%# Eval("Interno") %>' />
                            <asp:HiddenField ID="HiddenFieldFoto" runat="server" Value='<%# Eval("Foto") %>' />
                            <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("LEGAJO") %>' />
                            <asp:HiddenField ID="HiddenFieldNombre" runat="server" Value='<%# Eval("Nombre") %>' />
                            <asp:Label ID="LTApellidoNombreLabel" Text='<%# Eval("LTApellidoNombre") %>' runat="server"></asp:Label></ItemTemplate>
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Auditor">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenFieldAuditor" runat="server" Value='<%# Bind("JP") %>' />
                            <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListJP" runat="server"
                                 DataTextField="ApellidoNombre"
                                DataValueField="Legajo"  >
                                <asp:ListItem Value="-1">- Seleccionar -</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator Enabled='<%# (HiddenFieldTipoPeriodoID.Value=="1") %>' ID="CompareValidatorJP" runat="server" ControlToValidate="DropDownListJP"
                                Display="Dynamic" Operator="NotEqual" ValidationGroup="Grabar" ValueToCompare="-1">*</asp:CompareValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="JPApellidoNombreLabel" Text='<%# Eval("JPApellidoNombre") %>' runat="server"></asp:Label></ItemTemplate>
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Area">
                        <EditItemTemplate>
                           <asp:DropDownList AppendDataBoundItems="True" ID="DropDownListSectores" runat="server"
                                DataSourceID="ObjectDataSourceSectores" DataTextField="AreaDESC" DataValueField="AreaID"
                                SelectedValue='<%# Bind("AreaID") %>' CssClass="fixed_width">
                                <asp:ListItem Value="-1">- Seleccionar -</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidatorSectores" runat="server" ControlToValidate="DropDownListSectores"
                                Display="Dynamic" Operator="NotEqual" ValidationGroup="Grabar" ValueToCompare="-1">*</asp:CompareValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="SectorLabel" Text='<%# Eval("Sector") %>' runat="server"></asp:Label></ItemTemplate>
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cargo">
                        <EditItemTemplate>
                            <asp:HiddenField ID="HiddenFieldCargoID" runat="server" Value='<%# Bind("CargoID") %>' />
                            <asp:DropDownList AppendDataBoundItems="true" ID="DropDownListCargos" runat="server"
                                DataSourceID="ObjectDataSourceCargos" DataTextField="CargoDET" DataValueField="CargoID" CssClass="fixed_width2"   >                             
                                <asp:ListItem Value="-1">- Seleccionar -</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidatorCargos" runat="server" ControlToValidate="DropDownListCargos"
                                Display="Dynamic" Operator="NotEqual" ValidationGroup="Grabar" ValueToCompare="-1">*</asp:CompareValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="CargoLabel" Text='<%# Eval("Cargo") %>' runat="server"></asp:Label></ItemTemplate>
                        <HeaderStyle CssClass="tabla_titulo" />
                        <ItemStyle CssClass="texto" />
                    </asp:TemplateField>
                   
                    <asp:CommandField ShowEditButton="True" ButtonType="Image" EditImageUrl="~/images/page_edit.png"
                        CancelImageUrl="~/images/cancel.png" ValidationGroup="Grabar" UpdateImageUrl="~/images/page_save.png" />
                    
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonBorrar" runat="server" 
                                CommandArgument='<%# Eval("LEGAJO") %>' CommandName="Eliminar" 
                                ImageUrl="~/images/delete.png"
                                OnClientClick='<%# Eval("LEGAJO", "return confirm(\"Desea Borrar la Evaluación del legajo {0}?\")") %>' 
                                 />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label ID="Label4" runat="server" Text="No se encontraron registros para la búsqueda realizada"
                        CssClass="destacados"></asp:Label>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceUsuarios" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetUsuariosImportacion" TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj"
                OnObjectCreating="ObjectDataSourceUsuarios_ObjectCreating" 
                OnObjectDisposing="ObjectDataSourceUsuarios_ObjectDisposing" 
                onupdating="ObjectDataSourceUsuarios_Updating" UpdateMethod="GrabarCambios">
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceSectores" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetAreas" TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj">
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceCargos" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetCargos" TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj">
            </asp:ObjectDataSource>
            <asp:ObjectDataSource ID="ObjectDataSourceUsuariosEvaluadores1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetEvaluadoresByPeriodoID" TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:Parameter DefaultValue="0" Name="Legajo" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

<%--           <asp:ObjectDataSource ID="ObjectDataSourceUsuariosEvaluadores" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="getUsuariosAuditores" TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj"
                OnObjectCreating="ObjectDataSourceUsuariosEvaluadores_ObjectCreating" OnObjectDisposing="ObjectDataSourceUsuariosEvaluadores_ObjectDisposing">
            </asp:ObjectDataSource>--%>
            <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
            <div class="form-item leftHalf">
            <asp:Panel ID="PanelAgregarUsuarios" runat="server">
            <%--<div class="form-item leftHalf">--%>
                <asp:Label Text="Evaluado:" ID="Literal3" runat="server" CssClass="descripcion" Font-Bold="True"></asp:Label><asp:DropDownList
                    ID="DropDownListTodosUsuarios" runat="server" DataSourceID="ObjectDataSourceEvaluados"
                    DataTextField="ApellidoNombre" DataValueField="Legajo">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetVwUsuariosDisponibles" TypeName="com.paginar.formularios.businesslogiclayer.SincronizadorScj"
                    OnObjectCreating="ObjectDataSourceEvaluados_ObjectCreating" OnObjectDisposing="ObjectDataSourceEvaluados_ObjectDisposing">
                </asp:ObjectDataSource>
            <%--  </div>--%>
                <div class="controls">
                <asp:Button SkinID="BotonUsuarioBo" ID="ButtonAgregar" runat="server" Text="Agregar"
                    OnClick="ButtonAgregar_Click" />
                    </div>
                
            </asp:Panel>
             </div>
        </asp:View>
        <asp:View ID="ViewResultado" runat="server">
            <asp:Label ID="LabelResultado" CssClass="destacados" runat="server" Text="Label"></asp:Label>
            <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="5000" OnTick="Timer1_Tick">
            </asp:Timer>
            <asp:Label ID="LabelRefresh" runat="server" Text="Label"></asp:Label>
        </asp:View>
    </asp:MultiView>
    
</asp:Content>
