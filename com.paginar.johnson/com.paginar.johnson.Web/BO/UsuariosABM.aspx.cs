﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;
using com.paginar.johnson.DAL;
using com.paginar.johnson.validators;

namespace com.paginar.johnson.Web.BO
{
    public partial class UsuariosABM : PageBase
    {
        private ControllerUsuarios _UC;
        public ControllerUsuarios UC
        {
            get
            {
                _UC = (ControllerUsuarios)this.Session["UC"];
                if (_UC == null)
                {
                    _UC = new ControllerUsuarios();
                    this.Session["UC"] = _UC;
                }
                return _UC;
            }
            set
            {
                _UC = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                UC = new ControllerUsuarios(); 
                MVUsuarios.SetActiveView(VGrilla);
                GridUsuarios.Visible = false;
            }
        }

        protected void BuscarButton_Click(object sender, EventArgs e)
        {
            GridUsuarios.Visible = true;
            GridUsuarios.DataBind();
        }

        protected void GridUsuarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                //UC = null;
                UC.GetUsuarioByUsuarioID(int.Parse(e.CommandArgument.ToString()));
                
                FVUsuario.DataBind();
                MVUsuarios.SetActiveView(VABM);
                

            }

            if (e.CommandName == "Clonar")
            {
                MVUsuarios.SetActiveView(VABM);
                UC.GetUsuarioByUsuarioID(int.Parse(e.CommandArgument.ToString()));
                FVUsuario.ChangeMode(FormViewMode.ReadOnly);
            }
        }

        protected void FVUsuario_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            e.NewValues["FHMod"] = DateTime.Now;
            e.NewValues["UsrMod"] = this.ObjectUsuario.usuarioid;
            FileUpload FUImagenes = FVUsuario.FindControl("UsuarioFotoFileUpload") as FileUpload;
            if (FUImagenes.HasFile)
            {
                string filename = Path.GetFileNameWithoutExtension(FUImagenes.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(FUImagenes.FileName);
                string strPath = MapPath("~/images/personas/") + filename;
                FUImagenes.SaveAs(@strPath);
                e.NewValues["UsuarioFoto"] = filename;

            }
            else
            {
                HiddenField ImagenHiddenField = FVUsuario.FindControl("ImagenHiddenField") as HiddenField;
                e.NewValues["UsuarioFoto"] = ImagenHiddenField.Value;
            }
            HiddenField ClaveHiddenField = FVUsuario.FindControl("ClaveHiddenField") as HiddenField;
            if(!((e.NewValues["Clave"]!=null)&&(e.NewValues["Clave"].ToString()!=string.Empty)))
                e.NewValues["Clave"]  = ClaveHiddenField.Value;

            HiddenField UsuarioIDHiddenField = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            CheckBoxList ClustersCheckBoxList = FVUsuario.FindControl("ClustersCheckBoxList") as CheckBoxList;
            foreach (ListItem item in ClustersCheckBoxList.Items)
            {
                UC.AddCluster(int.Parse(UsuarioIDHiddenField.Value), int.Parse(item.Value), item.Selected);
            }

            CheckBoxList FormulariosCheckBoxList = FVUsuario.FindControl("FormulariosCheckBoxList") as CheckBoxList;
            foreach (ListItem item in FormulariosCheckBoxList.Items)
            {
                UC.AddFormularios(int.Parse(UsuarioIDHiddenField.Value), int.Parse(item.Value), item.Selected);
            }

            CheckBoxList GruposCheckBoxList = FVUsuario.FindControl("GruposCheckBoxList") as CheckBoxList;
            foreach (ListItem item in GruposCheckBoxList.Items)
            {
                UC.AddGrupos(int.Parse(UsuarioIDHiddenField.Value), int.Parse(item.Value), item.Selected);
            }

            TextBox elMailU;
            elMailU = (TextBox)FVUsuario.FindControl("EmailTextBox") as TextBox;
            if (elMailU.Text == null || elMailU.Text == "" || elMailU.Text == " ")
            {
                e.NewValues["Email"] = " ";
            }
            DropDownList acargo = FVUsuario.FindControl("ACargoDeDropDownList") as DropDownList;
            DropDownList cargo = FVUsuario.FindControl("CargoDropDownList") as DropDownList;
            if(!string.IsNullOrEmpty( acargo.SelectedValue))
            e.NewValues["ACargoDe"] = int.Parse(acargo.SelectedValue.ToString());

            if (!string.IsNullOrEmpty(cargo.SelectedValue))
                e.NewValues["CargoID"] = int.Parse(cargo.SelectedValue.ToString());



            DropDownList ClusterDropDownList = FVUsuario.FindControl("ClusterDropDownList") as DropDownList;
            if (ClusterDropDownList.SelectedIndex == 0)
            {
                e.NewValues["ClusterID"] = 9;
            }

            DropDownList UbicacionDropDownList = FVUsuario.FindControl("UbicacionDropDownList") as DropDownList;
            if (UbicacionDropDownList.SelectedIndex == 0)
            {
                e.NewValues["UbicacionID"] = 0;
            }

            DropDownList SectorIDDropDownList = FVUsuario.FindControl("SectorIDDropDownList") as DropDownList;
            if (SectorIDDropDownList.SelectedIndex == 0)
            {
                e.NewValues["SectorID"] = 29;// Sin sector
            }


            DropDownList AreaDropDownList = FVUsuario.FindControl("AreaDropDownList") as DropDownList;
            if (SectorIDDropDownList.SelectedIndex == 0)
            {
                e.NewValues["AreaID"] = 138;// sin area
            }



            DropDownList DireccionDropDownList = FVUsuario.FindControl("DireccionDropDownList") as DropDownList;
            if (DireccionDropDownList.SelectedIndex == 0)
            {
                e.NewValues["DireccionID"] = 11;// sin direccion 
            }


            DropDownList EscalaSalarialDropDownList = FVUsuario.FindControl("EscalaSalarialDropDownList") as DropDownList;
            if (EscalaSalarialDropDownList.SelectedIndex == 0)
            {
                e.NewValues["EscalaSalarial"] = 26;// sin informacion
            }



            TextBox FHNacimientoTextBox = FVUsuario.FindControl("FHNacimientoTextBox") as TextBox;
            if(FHNacimientoTextBox.Text.Trim()=="")
            {
                DateTime date = new DateTime(1900, 1, 1);
                e.NewValues["FHNacimiento"] = date;
            }

            


            MVUsuarios.SetActiveView(VResultado);
        }

        protected void ODSUsuario_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = UC;
        }

        protected void ODSUsuario_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected void GruposCheckBoxList_DataBound(object sender, EventArgs e)
        {
            CheckBoxList GruposCheckBoxList  = sender as CheckBoxList ;
            DSUsuarios.RelLoginGrupoDataTable RelLoginGrupDt = UC.GetGrupos();
            foreach (DSUsuarios.RelLoginGrupoRow RelLoginGrupoR in RelLoginGrupDt.Rows)
            {

                ListItem item = GruposCheckBoxList.Items.FindByValue(RelLoginGrupoR.GrupoNro.ToString());
                if (item != null)
                    item.Selected = true;
            }
            
        }

        protected void FormulariosCheckBoxList_DataBound(object sender, EventArgs e)
        {
            CheckBoxList FormulariosCheckBoxList = sender as CheckBoxList;
            DSUsuarios.frmRelUsuarioFormularioDataTable frmRelUsuarioFormularioDt = UC.GetFormularios();
            foreach (DSUsuarios.frmRelUsuarioFormularioRow frmRelUsuarioFormularioR in frmRelUsuarioFormularioDt.Rows)
            {

                ListItem item = FormulariosCheckBoxList.Items.FindByValue(frmRelUsuarioFormularioR.FormularioID.ToString());
                if (item != null)
                    item.Selected = true;
            }
        }

        protected void ClustersCheckBoxList_DataBound(object sender, EventArgs e)
        {
            CheckBoxList ClustersCheckBoxList = sender as CheckBoxList;
            DSUsuarios.RelUsuarioAccesoClusterDataTable RelUsuarioAccesoClusterDt = UC.GetClustersAcceso();
            foreach (DSUsuarios.RelUsuarioAccesoClusterRow RelUsuarioAccesoClusterR in RelUsuarioAccesoClusterDt.Rows)
            {

                ListItem item = ClustersCheckBoxList.Items.FindByValue(RelUsuarioAccesoClusterR.ClusterID.ToString());
                if (item != null)
                    item.Selected = true;
            }
        }

        protected void ButtonNuevo_Click(object sender, EventArgs e)
        {
            FVUsuario.ChangeMode(FormViewMode.Insert);                        
            UC.UsuarioClear();
            MVUsuarios.SetActiveView(VABM);
        }

        protected void FVUsuario_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            e.Values["FHAlta"] = DateTime.Now;
            e.Values["UsrAlta"] = this.ObjectUsuario.usuarioid;
            FileUpload FUImagenes = FVUsuario.FindControl("UsuarioFotoFileUpload") as FileUpload;
            if (FUImagenes.HasFile)
            {
                string filename = Path.GetFileNameWithoutExtension(FUImagenes.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(FUImagenes.FileName);
                string strPath = MapPath("~/images/personas/") + filename;
                FUImagenes.SaveAs(@strPath);
                e.Values["UsuarioFoto"] = filename;

            }
            TextBox ClaveTextBox = FVUsuario.FindControl("ClaveTextBox") as TextBox;
            e.Values["Clave"] = ClaveTextBox.Text;
            //if (!(( != null) && (e.Values["Clave"].ToString() != string.Empty)))
            //    e.Values["Clave"] = ClaveHiddenField.Value;

            HiddenField UsuarioIDHiddenField = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            CheckBoxList ClustersCheckBoxList = FVUsuario.FindControl("ClustersCheckBoxList") as CheckBoxList;
            foreach (ListItem item in ClustersCheckBoxList.Items)
            {
                UC.AddCluster(-1, int.Parse(item.Value), item.Selected);
            }

            CheckBoxList FormulariosCheckBoxList = FVUsuario.FindControl("FormulariosCheckBoxList") as CheckBoxList;
            foreach (ListItem item in FormulariosCheckBoxList.Items)
            {
                UC.AddFormularios(-1, int.Parse(item.Value), item.Selected);
            }

            CheckBoxList GruposCheckBoxList = FVUsuario.FindControl("GruposCheckBoxList") as CheckBoxList;
            foreach (ListItem item in GruposCheckBoxList.Items)
            {
                UC.AddGrupos(-1, int.Parse(item.Value), item.Selected);
            }
            TextBox elMail;
            elMail = (TextBox)FVUsuario.FindControl("EmailTextBox") as TextBox;
            if (elMail.Text == null || elMail.Text == "" || elMail.Text == " ")
            {
                e.Values["Email"] = " ";
            }


            DropDownList acargo = FVUsuario.FindControl("ACargoDeDropDownList") as DropDownList;            
            if (!string.IsNullOrEmpty(acargo.SelectedValue))
                e.Values["ACargoDe"] = int.Parse(acargo.SelectedValue.ToString());


            MVUsuarios.SetActiveView(VResultado);
        }

        protected void ODSUsuarios_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            MVUsuarios.SetActiveView(VGrilla);
            Timer1.Enabled = false;
        }

        protected void VResultado_Activate(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
        }

        protected void FVUsuario_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if(e.CommandName=="Cancel")
                MVUsuarios.SetActiveView(VGrilla);
        }
        
        protected void ClonarButton_Click(object sender, EventArgs e)
        {
            TextBox ApellidoTextBox = FVUsuario.FindControl("ApellidoTextBox") as TextBox;
            TextBox NombreTextBox = FVUsuario.FindControl("NombreTextBox") as TextBox;
            TextBox EmailTextBox = FVUsuario.FindControl("EmailTextBox") as TextBox;
            DropDownList EstadoDropDownList =FVUsuario.FindControl("EstadoDropDownList") as DropDownList;
            TextBox FHNacimientoTextBox = FVUsuario.FindControl("FHNacimientoTextBox") as TextBox;
            TextBox LegajoTextBox = FVUsuario.FindControl("LegajoTextBox") as TextBox;
            TextBox InternoTextBox = FVUsuario.FindControl("InternoTextBox") as TextBox;
            TextBox InternoIpTextBox = FVUsuario.FindControl("InternoIPTextBox") as TextBox;
            DropDownList UbicacionDropDownList = FVUsuario.FindControl("UbicacionDropDownList") as DropDownList;
            DropDownList SectorIDDropDownList = FVUsuario.FindControl("SectorIDDropDownList") as DropDownList;
            DropDownList EsPayRollDropDownList = FVUsuario.FindControl("EsPayRollDropDownList") as DropDownList;
            DropDownList ClusterDropDownList = FVUsuario.FindControl("ClusterDropDownList") as DropDownList;
            DropDownList EscalaSalarialDropDownList = FVUsuario.FindControl("EscalaSalarialDropDownList") as DropDownList;
            FileUpload  UsuarioFotoFileUpload = FVUsuario.FindControl("UsuarioFotoFileUpload") as FileUpload;
            string filename = null;
            if (UsuarioFotoFileUpload.HasFile)
            {
                filename = Path.GetFileNameWithoutExtension(UsuarioFotoFileUpload.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(UsuarioFotoFileUpload.FileName);
                string strPath = MapPath("~/images/personas/") + filename;
                UsuarioFotoFileUpload.SaveAs(@strPath);
                //e.Values["UsuarioFoto"] = filename;

            }
            DropDownList ACargoDeDropDownList= FVUsuario.FindControl("ACargoDeDropDownList") as DropDownList;
            DropDownList AreaDropDownList = FVUsuario.FindControl("AreaDropDownList") as DropDownList;
            DropDownList DireccionDropDownList = FVUsuario.FindControl("DireccionDropDownList") as DropDownList;
            DropDownList CargoDropDownList = FVUsuario.FindControl("CargoDropDownList") as DropDownList;
            DropDownList EstaEnQeQDropDownList = FVUsuario.FindControl("EstaEnQeQDropDownList") as DropDownList;
            TextBox IdentifTextBox = FVUsuario.FindControl("IdentifTextBox") as TextBox;
            TextBox ClaveTextBox = FVUsuario.FindControl("ClaveTextBox") as TextBox;


            //Grupos de login

            CheckBoxList GruposCheckBoxList = FVUsuario.FindControl("GruposCheckBoxList") as CheckBoxList;
            foreach (ListItem item in GruposCheckBoxList.Items)
            {

                UC.AddGrupos(-1, int.Parse(item.Value), item.Selected);
            }

            CheckBoxList ClustersCheckBoxList = FVUsuario.FindControl("ClustersCheckBoxList") as CheckBoxList;
            foreach (ListItem item in ClustersCheckBoxList.Items)
            {
                UC.AddCluster(-1, int.Parse(item.Value), item.Selected);
            }

            CheckBoxList FormulariosCheckBoxList = FVUsuario.FindControl("FormulariosCheckBoxList") as CheckBoxList;
            foreach (ListItem item in FormulariosCheckBoxList.Items)
            {
                UC.AddFormularios(-1, int.Parse(item.Value), item.Selected);
            }


            //Valido el identif
            ControllerUsuarios cux = new ControllerUsuarios();
            DSUsuarios.LoginDataTable DUL = new DSUsuarios.LoginDataTable();
            int contergLog;
            DUL = cux.Repositorio.AdapterDSUsuarios.Login.GetDataByUsuarioChekIdentif(IdentifTextBox.Text);
            contergLog = DUL.Count();




            if (contergLog == 0 && Page.IsValid)
            {
                int? AACargoDe = null;
                if (ACargoDeDropDownList.SelectedValue != "")
                    AACargoDe = int.Parse(ACargoDeDropDownList.SelectedValue);

                UC.InsertUsuario(ApellidoTextBox.Text, NombreTextBox.Text, EmailTextBox.Text, int.Parse(EstadoDropDownList.SelectedValue), DateTime.Parse(FHNacimientoTextBox.Text), int.Parse(LegajoTextBox.Text), InternoTextBox.Text, InternoIpTextBox.Text, (UbicacionDropDownList.SelectedValue), int.Parse(SectorIDDropDownList.SelectedValue), int.Parse(EsPayRollDropDownList.SelectedValue), int.Parse(ClusterDropDownList.SelectedValue), int.Parse(EscalaSalarialDropDownList.SelectedValue), filename, int.Parse(AreaDropDownList.SelectedValue), int.Parse(DireccionDropDownList.SelectedValue), int.Parse(CargoDropDownList.SelectedValue), AACargoDe, int.Parse(EstaEnQeQDropDownList.SelectedValue), DateTime.Now, this.ObjectUsuario.usuarioid, IdentifTextBox.Text, ClaveTextBox.Text, int.Parse(LegajoTextBox.Text));
                
                MVUsuarios.SetActiveView(VResultado);
            }
        }

        bool notcheck = false;

        protected void ValidadorIdentif_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (notcheck)
            {
                
                //DSUsuarios.LoginDataTable datoValidar =  new DSUsuarios.LoginDataTable();
                ControllerUsuarios cux = new ControllerUsuarios();
                DSUsuarios.LoginDataTable DUL = new DSUsuarios.LoginDataTable();
                int contergLog;
                DUL = cux.Repositorio.AdapterDSUsuarios.Login.GetDataByUsuarioChekIdentif(args.Value);
                contergLog = DUL.Count();


                

                if (contergLog >= 1)
                    args.IsValid = false;
                else
                    args.IsValid = true;

            }
            notcheck = true;
        }

        protected void ValidadorIdentifUpdate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ControllerUsuarios cu = new ControllerUsuarios();

            DSUsuarios.LoginDataTable DUL = new DSUsuarios.LoginDataTable();
            DSUsuarios.UsuarioDataTable USR = new DSUsuarios.UsuarioDataTable();

            int contergLog;
            string identificarodUser = null;
            
            DUL = cu.Repositorio.AdapterDSUsuarios.Login.GetDataByUsuarioChekIdentif(args.Value);
            contergLog = DUL.Count();

            HiddenField userID     = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            TextBox comparaIdentif = FVUsuario.FindControl("IdentifTextBox") as TextBox;
            USR = cu.GetUsuarioByUsuarioID(int.Parse(userID.Value));

            foreach (DSUsuarios.UsuarioRow UserRowIdentif in USR.Rows) {

                identificarodUser = UserRowIdentif.Identif.ToString();
            }


            //if(USR.Count())   

            if (contergLog >= 1 && identificarodUser == comparaIdentif.Text)
                args.IsValid = true;
            else if (contergLog >= 1)
                args.IsValid = false;
            else
                args.IsValid = true;


        }

        protected string CheckDBCargoValue(object SelectVal)
        {
            string retorno = "";

            if ((SelectVal == System.DBNull.Value) || (SelectVal == null))
                return retorno;

            ControllerUsuarios CU = new ControllerUsuarios();
            DSUsuarios.CargoDataTable CT = CU.getCargosByCargoID(int.Parse(SelectVal.ToString()));
            if (CT.Rows.Count > 0)
                retorno = SelectVal.ToString();
            else
                retorno = "";

            return retorno;
        }

        protected string ChekDBValue(object selectadVal)
        {

            if ((selectadVal == System.DBNull.Value)||(selectadVal==null))
                return "";

            string Estado = null;
            string NombreJF = null;
            string elId = null;
            ControllerUsuarios cuRA = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable USRaCargo = new DSUsuarios.UsuarioDataTable();


            USRaCargo = cuRA.GetUsuarioByUsuarioID(int.Parse(selectadVal.ToString()));

            foreach (DSUsuarios.UsuarioRow UserAcargo in USRaCargo.Rows)
            {

                Estado = UserAcargo.Estado.ToString();
                NombreJF = UserAcargo.Apellido.ToString() + ' ' + UserAcargo.Nombre.ToString();
                elId = UserAcargo.UsuarioID.ToString();
            }


            int estadoUsuarioCargo = int.Parse(Estado);
            if (estadoUsuarioCargo != 2)
            {
                ((Label)FVUsuario.FindControl("lblErrorAcargoDe")).Text = "El jefe seleccionado no se encuentra habilitado. Por Favor indique su jefe correspondiente"; //"Reporta a " + NombreJF + " usuario deshabilitado del sistema.";
                ((Label)FVUsuario.FindControl("lblErrorAcargoDe")).Visible = true;
                return "";
            }
            else
            {
                return selectadVal.ToString();
            }
                  
        }

        protected void EstadoDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControllerUsuarios cub = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable USRB = new DSUsuarios.UsuarioDataTable();
            HiddenField userId = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            DropDownList estado = (DropDownList)FVUsuario.FindControl("EstadoDropDownList");
            bool habilitado;

            if (int.Parse(estado.SelectedValue) == 1)
            {
                string usuariosDependientes = "<br/>";
                USRB = cub.Repositorio.AdapterDSUsuarios.Usuario.GetDependientesByUsuarioID(int.Parse(userId.Value));
                if (USRB.Count > 0)
                {
                    foreach (DSUsuarios.UsuarioRow urw in USRB.Rows)
                    {
                        usuariosDependientes += urw.LegajoWD + " - " + urw.Apellido + ", " + urw.Nombre + "<br/>";
                    }
                    ((Label)FVUsuario.FindControl("lblListadoUsuariosDependientes")).Text = usuariosDependientes;
                    //EjecutarScript("  $('.window .close').click(function (e) { e.preventDefault(); $('#mask').hide(); $('.window').hide(); }); $('#mask').click(function () { $(this).hide(); $('.window').hide(); }); function abrirModal() { var id = $('#dialog'); var maskHeight = $(document).height(); var maskWidth = $(window).width(); $('#mask').css({ 'width': maskWidth, 'height': maskHeight }); $('#mask').fadeIn(1000); $('#mask').fadeTo('slow', 0.8); var winH = $(window).height(); var winW = $(window).width(); $(id).css('top', winH / 2 - $(id).height() / 2); $(id).css('left', winW / 2 - $(id).width() / 2); $(id).fadeIn(2000); }");
                    EjecutarScript("$(document).ready(function () { abrirModal(); });");
                }

                habilitado = false;
            }
            else
            {
                habilitado = true;
            }

            HabilitaUsuario(habilitado);

        }

        private void HabilitaUsuario(bool habilitado)
        {

            RequiredFieldValidator RequiredFieldValidator11 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator11");
            RequiredFieldValidatorForCheckBoxLists checkboxListvalidator = (RequiredFieldValidatorForCheckBoxLists)FVUsuario.FindControl("checkboxListvalidator");
            CustomValidator CustomValidatorClusterPertenenciaClusterAcceso = (CustomValidator)FVUsuario.FindControl("CustomValidatorClusterPertenenciaClusterAcceso");
            RequiredFieldValidator RequiredFieldValidator8 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator8");
            CustomValidator CustomValidator2 = (CustomValidator)FVUsuario.FindControl("CustomValidator2");
            RequiredFieldValidator RequiredFieldValidator9 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator9");
            RequiredFieldValidator RequiredFieldValidator7 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator7");
            RequiredFieldValidator RequiredFieldValidator10 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator10");
            RequiredFieldValidator RequiredFieldValidator15 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator15");
            RequiredFieldValidator RequiredFieldValidator14 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator14");
           // RequiredFieldValidator RequiredFieldValidator13 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator13");
            RequiredFieldValidator RequiredFieldValidator12 = (RequiredFieldValidator)FVUsuario.FindControl("RequiredFieldValidator12");
            //RequiredFieldValidatorForCheckBoxLists RequiredFieldValidatorForCheckBoxLists2 = (RequiredFieldValidatorForCheckBoxLists)FVUsuario.FindControl("RequiredFieldValidatorForCheckBoxLists2");

            RequiredFieldValidator11.Enabled = habilitado;
            checkboxListvalidator.Enabled = habilitado;
            CustomValidatorClusterPertenenciaClusterAcceso.Enabled = habilitado;
            //RequiredFieldValidator8.Enabled=habilitado; legajo
            //CustomValidator2.Enabled=habilitado; legajo
            RequiredFieldValidator7.Enabled = habilitado;
            RequiredFieldValidator9.Enabled = habilitado;
            RequiredFieldValidator10.Enabled = habilitado;
            RequiredFieldValidator15.Enabled = habilitado;
            RequiredFieldValidator14.Enabled = habilitado;
            //RequiredFieldValidator13.Enabled = habilitado;
            RequiredFieldValidator12.Enabled = habilitado;
            //RequiredFieldValidatorForCheckBoxLists2.Enabled = habilitado;

            Label lblErrorAcargoDe =  (Label)FVUsuario.FindControl("lblErrorAcargoDe");
            ///lblErrorAcargoDe.Visible = habilitado;
          
        }

        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }

        protected void CustomValidatorLegajo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ControllerUsuarios CUL = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable UDTCUL = new DSUsuarios.UsuarioDataTable();
            if (args.Value != "")
            {
                DropDownList Clusters = FVUsuario.FindControl("ClusterDropDownList") as DropDownList;
                int cpertenecia = int.Parse(Clusters.SelectedIndex.ToString());

                //UDTCUL = CUL.GetUsuariosByLegajo(int.Parse(args.Value));
                UDTCUL = CUL.GetUsuariosByLegajoyCluster(int.Parse(args.Value),cpertenecia);
                if (UDTCUL.Count >= 1)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;            
            }
        }

        protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            ControllerUsuarios CUL = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable UDTCUL = new DSUsuarios.UsuarioDataTable();
            DropDownList Clusters = FVUsuario.FindControl("ClusterDropDownList") as DropDownList;
            int cpertenecia = int.Parse(Clusters.SelectedIndex.ToString());

            //UDTCUL = CUL.GetUsuariosByLegajo(int.Parse(args.Value));
            UDTCUL = CUL.GetUsuariosByLegajoyCluster(int.Parse(args.Value), cpertenecia);



            ControllerUsuarios cu = new ControllerUsuarios();

            DSUsuarios.LoginDataTable DUL = new DSUsuarios.LoginDataTable();
            DSUsuarios.UsuarioDataTable USR = new DSUsuarios.UsuarioDataTable();

            int contergLog;
            string LegajoUser = null;


            contergLog = DUL.Count();

            HiddenField userID = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            TextBox elLegajo = FVUsuario.FindControl("LegajoTextBox") as TextBox;
            USR = cu.GetUsuarioByUsuarioID(int.Parse(userID.Value));

            foreach (DSUsuarios.UsuarioRow UserRowIdentif in USR.Rows)
            {
                if( ! UserRowIdentif.IsLegajoWDNull())
                LegajoUser = UserRowIdentif.LegajoWD.ToString();
            }

            if (args.Value != "")
            {
                if (LegajoUser == elLegajo.Text)
                {
                    args.IsValid = true;
                }
                else if (UDTCUL.Count >= 1)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;
            }


        }

        protected void FVUsuario_DataBound(object sender, EventArgs e)
        {
            ControllerUsuarios CUVerificacion = new ControllerUsuarios();
            HiddenField UsuarioIDHiddenField = (HiddenField)FVUsuario.FindControl("UsuarioIDHiddenField");


            DropDownList ClusterDropDownList = (DropDownList)FVUsuario.FindControl("ClusterDropDownList");
            Button UpdateButton = (Button)FVUsuario.FindControl("UpdateButton");
            string modoFV = FVUsuario.CurrentMode.ToString();
            if (modoFV == "Edit")
            {
                int ClusterUsuarioCreador = 1;

                DSUsuarios.UsuarioDataTable UsrCreador = CUVerificacion.GetUsuarioByUsuarioID(int.Parse(UsuarioIDHiddenField.Value));
                foreach (DSUsuarios.UsuarioRow UCRow in UsrCreador.Rows)
                {
                    ClusterUsuarioCreador = UCRow.Cluster;
                }

                if ((this.ObjectUsuario.clusteriddefecto != ClusterUsuarioCreador) && this.ObjectUsuario.clusteriddefecto != 1)
                {
                    ClusterDropDownList.Enabled = false;
                    UpdateButton.Enabled = false;
                }
                CustomValidator CustomValidatorClusterPertenenciaClusterAcceso = (CustomValidator)FVUsuario.FindControl("CustomValidatorClusterPertenenciaClusterAcceso");
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClusterDropDownList", "#" + ClusterDropDownList.ClientID);
                CheckBoxList ClustersCheckBoxList= (CheckBoxList)FVUsuario.FindControl("ClustersCheckBoxList");
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClusterDropDownList", "#" + ClusterDropDownList.ClientID);
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClustersCheckBoxList", "#" + ClustersCheckBoxList.ClientID);


                HabilitaUsuario(UsrCreador.Rows[0]["Estado"].ToString()=="1"?false:true);

                DateTime dtemp= new DateTime(1900,1,1);
                if (DateTime.Compare(dtemp, DateTime.Parse(UsrCreador.Rows[0]["FHNacimiento"].ToString())) == 0) 
                {
                    TextBox FHNacimientoTextBox = (TextBox)FVUsuario.FindControl("FHNacimientoTextBox");
                    FHNacimientoTextBox.Text = "";
                }

            }
            else if (modoFV == "Insert" || modoFV == "ReadOnly")
            {
                if (this.ObjectUsuario.clusteriddefecto != 1)
                {
                    ClusterDropDownList.SelectedValue = this.ObjectUsuario.clusteriddefecto.ToString();
                    ClusterDropDownList.Enabled = false;                    
                }
            }

            if (modoFV == "Insert")
            {
                CustomValidator CustomValidatorClusterPertenenciaClusterAcceso = (CustomValidator)FVUsuario.FindControl("CustomValidatorClusterPertenenciaClusterAcceso");
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClusterDropDownList", "#" + ClusterDropDownList.ClientID);
                CheckBoxList ClustersCheckBoxList = (CheckBoxList)FVUsuario.FindControl("ClustersCheckBoxList");
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClusterDropDownList", "#" + ClusterDropDownList.ClientID);
                CustomValidatorClusterPertenenciaClusterAcceso.Attributes.Add("ClustersCheckBoxList", "#" + ClustersCheckBoxList.ClientID);
            }

        }

        protected void GridUsuarios_DataBound(object sender, EventArgs e)
        {

            foreach (GridViewRow row in GridUsuarios.Rows)
            {
                LinkButton LinkButton1 = (LinkButton)row.FindControl("LinkButton1");
                string LblCluster = ((Label)row.FindControl("LblCluster")).Text;
                string Pais = string.Empty;
                if (this.ObjectUsuario.clusteriddefecto != 1)
                {
                    switch (this.ObjectUsuario.clusteriddefecto.ToString())
                    {
                        case "2":
                            Pais = "Chile";
                            break;
                        case "3":
                            Pais = "Uruguay";
                            break;
                        case "4":
                            Pais = "Paraguay";
                            break;
                    }
                    if (Pais != LblCluster)
                    {
                        LinkButton1.Enabled = false;
                    }

                }
            }
        }





/*
             ControllerUsuarios CUL = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable UDTCUL = new DSUsuarios.UsuarioDataTable();
            UDTCUL = CUL.GetUsuariosByLegajo(int.Parse(args.Value));


            ControllerUsuarios cu = new ControllerUsuarios();

            DSUsuarios.LoginDataTable DUL = new DSUsuarios.LoginDataTable();
            DSUsuarios.UsuarioDataTable USR = new DSUsuarios.UsuarioDataTable();

            int contergLog;
            string LegajoUser = null;


            contergLog = DUL.Count();

            HiddenField userID = FVUsuario.FindControl("UsuarioIDHiddenField") as HiddenField;
            TextBox elLegajo = FVUsuario.FindControl("LegajoTextBox") as TextBox;
            USR = cu.GetUsuarioByUsuarioID(int.Parse(userID.Value));

            foreach (DSUsuarios.UsuarioRow UserRowIdentif in USR.Rows)
            {

                LegajoUser = UserRowIdentif.Legajo.ToString();
            }

            if (args.Value != "")
            {
                if (LegajoUser == elLegajo.Text)
                {
                    args.IsValid = true;
                }
                else if (UDTCUL.Count >= 1)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;
            }


 */
    }
}