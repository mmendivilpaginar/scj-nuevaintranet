﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprimirWorkshops.aspx.cs" Inherits="com.paginar.johnson.Web.BO.ImprimirWorkshops" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Workshop</title>
    <link href="../css/print.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }
    </script>
</head>
<body>
    <form id="FormImprimirWKTodos" runat="server">
        <div id="wrapper">

            <div id="header">
                <div class="bg">
                    
                </div>
                <h1>Workshops</h1>
                <img id="logo" src="../images/logoprint.gif" />
            </div>
     
        <div id="noprint">

             <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" /><asp:Button
                 ID="btnExportarSuscritos" runat="server" Text="Exportar" 
                 onclick="btnExportarSuscritos_Click" />
              <br /><br />
        </div>

            <asp:Repeater ID="RCursosActivos" runat="server" 
                DataSourceID="ODSCursosActivos">
                <ItemTemplate>
                    <asp:HiddenField ID="HFIDCurso" runat="server" Value='<%# Eval("IdWorkshop")%>' />
                    <br />
                    <h3><%# Eval("Nombre")%></h3>
                        <asp:GridView ID="GVIScriptos" runat="server" 
                AutoGenerateColumns="False" DataKeyNames="IdInscripcion" 
                DataSourceID="ODSInscriptos">
                            <Columns>
                                <asp:BoundField DataField="Usuario" HeaderText="Usuario" ReadOnly="True" 
                                    SortExpression="Usuario" />
                                <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha de Suscripción" 
                                    SortExpression="FechaInscripcion" />
                                <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                                    SortExpression="Legajo" />
                                <asp:CheckBoxField DataField="Notificada" HeaderText="Notificado" 
                                    SortExpression="Notificada" />
                            </Columns>

                        </asp:GridView>

            <asp:ObjectDataSource ID="ODSInscriptos" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="WorkshopGetInscriptos" 
                TypeName="com.paginar.johnson.BL.ControllerWorkShop">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HFIDCurso" Name="IdWorkshop" 
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

                </ItemTemplate>

            </asp:Repeater>

            <asp:ObjectDataSource ID="ODSCursosActivos" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="WorkshopDisponiblesActivos" 
                TypeName="com.paginar.johnson.BL.ControllerWorkShop"></asp:ObjectDataSource>

        </div>

    </form>
</body>
</html>
