﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.BO
{
    public partial class pdaImprimirReportes : System.Web.UI.Page
    {
        int totalInscriptos;
        int totalReporte2;
        ControllerPDA CPDA = new ControllerPDA();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Areaid"] != null)
                HFAreaid.Value = Request.QueryString["Areaid"].ToString();
                if (Request.QueryString["DireccionId"] != null)
                HFDireccionId.Value = Request.QueryString["DireccionId"].ToString();
                if (Request.QueryString["Cursoid"] != null)
                HFCursoid.Value = Request.QueryString["Cursoid"].ToString();
                if (Request.QueryString["Anio"] != null)
                    HFAnio.Value = Request.QueryString["Anio"].ToString();


                switch (Request.QueryString["Reporteid"].ToString())
                {
                    case "1":
                        //ltrlTitulo.Text = "Solicitudes de inscripción por Cursos";
                        MVReportesPDA.SetActiveView(VInscPorCurso);
                        break;
                    case "2":
                        //ltrlTitulo.Text = "Solicitudes de inscripción por Dirección / Área";
                        MVReportesPDA.SetActiveView(VInscAreaDir);
                        break;
                }
            }
        }

        protected string devolverEstado(string estadoid)
        {
            string valorRetorno = string.Empty;
            /*
            idEstado	Descripcion
            1	Pendiente
            2	Abierto
            3	Cerrado 
             */

            switch (estadoid)
            {
                case "1":
                    valorRetorno = "Pendiente";
                    break;
                case "2":
                    valorRetorno = "Abierto";
                    break;
                case "3":
                    valorRetorno = "Cerrado";
                    break;
            }
            return valorRetorno;
        }

        protected string cantidadInscriptos(object idCurso)
        {
            int? anio = null;
            if (HFAnio.Value.Length > 0)
                anio = int.Parse(HFAnio.Value);
            DAL.DSPda.pda_ReporteInscripcionPorCursoDataTable curso = CPDA.getReporteInscripcionPorCurso(int.Parse(idCurso.ToString()), null, anio);
            totalInscriptos = totalInscriptos + curso.Count;
            return curso.Count.ToString();
        }

        protected int devolverTotalInscriptosReporte1()
        {
            return totalInscriptos;
        }

        protected string traerTotalReporte2()
        {
            return "Total: " + totalReporte2;
        }

        protected string procesarCantidadReporte2(object cant)
        {
            totalReporte2 = totalReporte2 + int.Parse(cant.ToString());
            return cant.ToString();
        }


        protected void RptInscPorCursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            lblTotalInscriptos.Text = "<div class=\"AspNet-GridView\"><table width=\"100%\"><thead><tr class=\"AspNet-GridView-Header\"><td colspan=\"6\"><span style=\"float:right\"><b>Total de inscripciones en los cursos de capacitación PDA:</b> " + totalInscriptos.ToString() + "</span></td></tr></thead></table></div>";
            if (e.Item.ItemType == ListItemType.Header)
            {
                ((Literal)e.Item.FindControl("ltrlFecha")).Text = DateTime.Now.ToShortDateString();

            }
        }

        protected void RptAreaDirecion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            lblTotalReporte2.Text = "<div class=\"AspNet-GridView\"><table width=\"100%\"><thead><tr class=\"AspNet-GridView-Header\"><td colspan=\"6\"><span style=\"float:right\"><b>Total de inscripciones en los cursos de capacitación PDA:</b> " + totalReporte2.ToString() + "</span></td></tr></thead></table></div>";
            if (e.Item.ItemType == ListItemType.Header)
            {
                ((Literal)e.Item.FindControl("ltrlFecha")).Text = DateTime.Now.ToShortDateString();
 
            }
        }


    }
}