﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.BO
{
    public partial class pdaABM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Text = "";

        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            GVpda.SelectedIndex = -1;
            FVpda.ChangeMode(FormViewMode.Insert);
        }

        protected void FVpda_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            
        }

        protected void FVpda_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            lblmsg.Text = "<div id=\"msgOk\" class=\"messages msg-exito\" runat=\"server\" visible=\"false\"><span lang=\"ES-TRAD\">Los cambios fueron guardados de forma correcta.</span></div>";
            GVpda.DataBind();
        }

        protected void FVpda_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            GVpda.DataBind();
        }

        protected void FVpda_DataBinding(object sender, EventArgs e)
        {
            int a = 0;
        }

        protected void FVpda_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {


        }

        protected void FVpda_DataBound(object sender, EventArgs e)
        {
            if (FVpda.CurrentMode == FormViewMode.Edit && ((TextBox)FVpda.FindControl("PeriodoTentativoTextBox")) != null)
            {
                //((TextBox)FVpda.FindControl("PeriodoTentativoTextBox")).Text = procesarCadenaMesAno(((TextBox)FVpda.FindControl("PeriodoTentativoTextBox")).Text);
            }
            if (FVpda.CurrentMode == FormViewMode.ReadOnly && ((Label)FVpda.FindControl("PeriodoTentativoLabel")) != null)
            {
                //((Label)FVpda.FindControl("PeriodoTentativoLabel")).Text = procesarCadenaMesAno(((Label)FVpda.FindControl("PeriodoTentativoLabel")).Text);
            }
            
        }

        protected string dameEstado(object e)
        {
            string valorRetorno = string.Empty;
            switch (e.ToString())
            {
                case "1":
                    valorRetorno = "Pendiente";
                    break;
                case "2":
                    valorRetorno = "Abierto";
                    break;
                case "3":
                    valorRetorno = "Cerrado";
                    break;

            }

            return valorRetorno;
        }

        protected string procesarCadenaMesAno(string p)
        {
            string valorRetorno = string.Empty;
           
            return p;
        }

        protected void FVpda_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName == "Cancel")
            {
                GVpda.SelectedIndex = -1;
                FVpda.DataBind();
            }
        }
    }
}