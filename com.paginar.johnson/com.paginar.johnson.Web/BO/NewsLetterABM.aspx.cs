﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;
using System.Data.SqlClient;
using System.Text;
using com.paginar.formularios.businesslogiclayer;
using System.Configuration;
using System.Net.Mail;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace com.paginar.johnson.Web.BO
{



    public partial class NewsLetterABM : PageBase
    {



        private DataTable _dtDestinatarios;
        public DataTable dtDestinatarios
        {
            get
            {
                if (Session["dtDestinatarios"] == null)
                {
                    _dtDestinatarios = new DataTable();                    
                    _dtDestinatarios.Columns.Add("email");
                    Session["dtDestinatarios"]=_dtDestinatarios;
                }
                return (DataTable) Session["dtDestinatarios"];
            }

            set
            {
                Session["dtDestinatarios"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request["newsletterid"] != null)
                    Editar(int.Parse(Request["newsletterid"].ToString()));
                else
                    Nuevo();
            }
        }


        protected void Nuevo()
        {
            Session["dtDestinatarios"] = null;

            ControllerNewsletter cn = new ControllerNewsletter();
            int id = cn.getMaxId();
            id.ToString("00000");

            DateTime fecha= DateTime.Today;
            txtTitulo.Text= id.ToString("00000")+"_Newsletter_"+fecha.Year.ToString()+fecha.Month.ToString("00")+fecha.Day.ToString("00");
            drpEstado.SelectedIndex = 1;
            drpEnvio.SelectedIndex = 2;
            drpEnvio.Enabled = false;

            FechaTextBox.Text = fecha.Day.ToString() + "/" + fecha.Month.ToString() + "/" + fecha.Year.ToString();


            btnCrear.Enabled = false;
            btnCrear.ToolTip = "Debe realizar una Vista previa";
        }

        protected void Editar(int newsletterid)
        {

            lblModo.Text = "Editar";

            hdNewsletterID.Value = newsletterid.ToString();

            ControllerNewsletter cn = new ControllerNewsletter();

            DSNewsLetter.news_NewsLetterDataTable dt=  cn.getNewsletterByID(newsletterid);


            drpEstado.SelectedValue = dt.Rows[0]["EstadoID"].ToString();
            drpEnvio.SelectedValue = dt.Rows[0]["EstadoEnvio"].ToString()=="True"? "1":"0";
            txtTitulo.Text = dt.Rows[0]["Titulo"].ToString();
            FechaTextBox.Text = string.Format("{0: dd/MM/yyyy}",dt.Rows[0]["FechaEnvio"]);

            dtDestinatarios = (DataTable)cn.getDestinatarioByID(newsletterid);
            gvDestinatarios.DataSource = dtDestinatarios;
            gvDestinatarios.DataBind();

            //EjecutarScript("$(document).ready(function () {  $('#sel_noticias').remove(); });");

            rpNoticasSel.DataSource=  cn.getNoticiasByID(newsletterid);
            rpNoticasSel.DataBind();




            DSNewsLetter.news_RInfInfoNewsLetterDataTable dtnoticias = cn.getNoticiasByID(newsletterid);
            foreach (DSNewsLetter.news_RInfInfoNewsLetterRow dr in dtnoticias.Rows)
            {
                if (hdNoticiasIdsFilter.Value != "") hdNoticiasIdsFilter.Value += ",";

                hdNoticiasIdsFilter.Value += dr["infoId"].ToString();
            }

            DSNewsLetter.news_getNoticiasDataTable dtn = cn.getNoticias("100", 0, "", "", newsletterid);
            foreach (DSNewsLetter.news_getNoticiasRow dr in dtn)
            {
                hdPrivada.Value = dr["Privacidad"].ToString();
            }



            hdNoticiasIds.Value = hdNoticiasIdsFilter.Value;

            btnCrear.Visible = false;
            btnModificar.Visible = true;
            btnModificar.Enabled = false;
            btnModificar.ToolTip = "Debe realizar una Vista previa";
        }


        #region Validations

        protected void cvNoticias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (hdNoticiasIds.Value == "")
                args.IsValid = false;
            else
                args.IsValid = true;
        }




        protected void cvCantidadNew_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (hdNoticiasIds.Value != "")
            {
                ControllerNewsletter cn = new ControllerNewsletter();
                DSNewsLetter.news_getNoticiasDataTable dt = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);

                int novedadesmax = 4;
                int familycompanymax = 3;
                int viviscjmax = 2;
                int acordatemax = 4;

                args.IsValid = true;

                if (dt.Select("CategoriaDescripcion='Novedades'").Count() > novedadesmax) args.IsValid = false;

                if (dt.Select("CategoriaDescripcion='A family Company'").Count() > familycompanymax) args.IsValid = false;

                if (dt.Select("CategoriaDescripcion='Vivi SCJ'").Count() > viviscjmax) args.IsValid = false;

                if (dt.Select("CategoriaDescripcion='Acordate'").Count() > acordatemax) args.IsValid = false;


                rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);
                rpNoticasSel.DataBind();

                hdNoticiasIdsFilter.Value = hdNoticiasIds.Value;
            }
            else
            args.IsValid = false;

        }

        #endregion



        #region Destinatarios

        protected void imgAgregarDestinatario_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {
                DataRow dr = dtDestinatarios.NewRow();
                dr["email"] = txtDestinatarios.Text;
                if(lblModo.Text=="Editar")dr["newsletterid"] = 0;

                dtDestinatarios.Rows.Add(dr);

                gvDestinatarios.DataSource = dtDestinatarios;
                gvDestinatarios.DataBind();

                txtDestinatarios.Text = "";
            }
        }

        protected void gvDestinatarios_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Quitar")
            {
                string email = e.CommandArgument.ToString();
                DataRow[] dtDestinatarioaEliminar = dtDestinatarios.Select("email='" + email.ToString()+"'");
                if (dtDestinatarioaEliminar.Length > 0)
                   dtDestinatarioaEliminar[0].Delete();
                    

                gvDestinatarios.DataSource = dtDestinatarios;
                gvDestinatarios.DataBind();

                
            }
        }


        protected void cvExisteDestinatario_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string email = txtDestinatarios.Text.Trim();
            bool agrega = true;

            foreach (DataRow dr in dtDestinatarios.Rows)
            {
                if (dr.RowState !=  DataRowState.Deleted)
                {
                    if (dr["email"].ToString() == email)
                        agrega = false;
                }
            }

            args.IsValid = agrega;
        }

        protected void cvDestinatarios_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (gvDestinatarios.Rows.Count == 0)
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        #endregion


        #region CommandButons

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string noticiasids = hdNoticiasIds.Value;
                ControllerNewsletter cn = new ControllerNewsletter();
                bool envio = drpEnvio.SelectedValue == "1" ? true : false;

                cn.AgregarNewsletter(int.Parse(drpEstado.SelectedValue),
                    envio,
                    txtTitulo.Text, 
                    DateTime.Today, 
                    DateTime.Parse(FechaTextBox.Text), 
                    this.ObjectUsuario.usuarioid,
                    dtDestinatarios, noticiasids);

                Vista.ActiveViewIndex = 1;
                Timer1.Enabled = true;

                Response.Write("<script>setTimeout(\"location.href='" + "NewsLetterBuscador.aspx" + "'\",1000);</script>");

            }

        }


        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int newsletterid = int.Parse(hdNewsletterID.Value);

                string noticiasids = hdNoticiasIds.Value;

                ControllerNewsletter cn = new ControllerNewsletter();

                bool envio = drpEnvio.SelectedValue == "1" ? true : false;

                cn.ModificarNewsletter(newsletterid,
                int.Parse(drpEstado.SelectedValue),
                    envio,
                    txtTitulo.Text,
                    DateTime.Today,
                    DateTime.Parse(FechaTextBox.Text),
                    this.ObjectUsuario.usuarioid,
                    dtDestinatarios, noticiasids);

                Vista.ActiveViewIndex = 1;
                Timer1.Enabled = true;


                Response.Write("<script>setTimeout(\"location.href='" + "NewsLetterBuscador.aspx" + "'\",1000);</script>");
   
            }

        }


        #region VistaPrevia

        protected string GetUrlSite()
        {
            string url = this.ASP_SITE.ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return this.ASP_SITE.ToString().Replace(":8080", "");//produccion

       
        }

        protected void btnVistaPrevia_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                lblEnvio.Text = "";
                ControllerNewsletter cn = new ControllerNewsletter();
                if (hdNoticiasIds.Value != "")
                {
                    string nro = int.Parse(txtTitulo.Text.Substring(0, 5)).ToString();

                    hdPreview.Text = "<iframe src='NewsLetterVistaPrevia.aspx?NotidiasIDs=" + hdNoticiasIds.Value + "&Fecha=" + FechaTextBox.Text + "&Nro=" + nro + "  ' frameBorder='0' width='835' height='600' style='background: #FFF'/>  ";

                    //   cn.getBodyNewsletter(GetUrlSite(), cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null), FechaTextBox.Text).ToString();

                    btnCrear.Enabled = true;
                    btnModificar.Enabled = true;
                    btnCrear.ToolTip = "";
                    btnModificar.ToolTip = "";

                    rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);
                    rpNoticasSel.DataBind();

                    hdNoticiasIdsFilter.Value = hdNoticiasIds.Value;

                    EjecutarScript("$(document).ready(function () { NewsletterPreview(); });");
                }
                else
                {
                    rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", "-1", null); // -1 para q no traiga nada
                    rpNoticasSel.DataBind();
                }
            }
        }



        #endregion


        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewsLetterBuscador.aspx", false);
        }


        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            NotificacionesController n = new NotificacionesController();
            ControllerNewsletter cn = new ControllerNewsletter();


            string path = Server.MapPath("~");
            AlternateView htmlView = null;
            string nro = int.Parse(txtTitulo.Text.Substring(0, 5)).ToString();

            string preview = cn.getBodyNewsletter(GetUrlSite(), cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null), FechaTextBox.Text, nro, path, false, ref htmlView).ToString();

            btnCrear.Enabled = true;
            btnCrear.ToolTip = "";

            rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);
            rpNoticasSel.DataBind();

            hdNoticiasIdsFilter.Value = hdNoticiasIds.Value;

            

            if (ConfigurationManager.AppSettings["DEBUG_MODE"] == "false")
            {
                n.EnvioMailNewsletter("G057040@scj.com", this.ObjectUsuario.email, "SCJ News", htmlView);
            }
            else
            {
                n.EnvioMailNewsletter("G057040@scj.com", "evaluador@mom,evaluado@mom", "SCJ News", htmlView);


            }

            lblEnvio.Text = "Envio realizado";

        }


        #endregion

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Response.Redirect("NewsLetterBuscador.aspx", false);
        }


        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }



        protected void btnRecortar_Click(object sender, EventArgs e)
        {
            ControllerNewsletter cn = new ControllerNewsletter();
            int x = int.Parse(hdx1.Value);
            int y = int.Parse(hdy1.Value);
            int w = int.Parse(hdw.Text);
            int h = int.Parse(hdh.Text);
            string InfoID= hdInfoID.Value;

            System.Drawing.Image img = Crop(Server.MapPath("~") + "/noticias/imagenes/" + hdPath.Value, new Rectangle(x, y, w, h));

            saveJpeg(Server.MapPath("~") + "/noticias/imagenes/" + InfoID + "_flyercrop.jpg", (Bitmap)img, 85L);


            string ids = hdNoticiasIds.Value;

            if (!ids.Contains(InfoID))
            {
                if (ids != "") ids += ",";
                ids = ids + InfoID;
            }

            hdNoticiasIds.Value = ids;

            rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);
            rpNoticasSel.DataBind();

            hdNoticiasIdsFilter.Value = hdNoticiasIds.Value;
        }


        protected void btnMantener_Click(object sender, EventArgs e)
        {
            ControllerNewsletter cn = new ControllerNewsletter();
            string InfoID = hdInfoID.Value;

            string ids = hdNoticiasIds.Value;

            if (!ids.Contains(InfoID))
            {
                if (ids != "") ids += ",";
                ids = ids + InfoID;
            }

            if (File.Exists(Server.MapPath("~") + "/noticias/Imagenes/" + InfoID + "_flyercrop.jpg"))
                File.Delete(Server.MapPath("~") + "/noticias/Imagenes/" + InfoID + "_flyercrop.jpg");

            hdNoticiasIds.Value = ids;

            rpNoticasSel.DataSource = cn.getNoticias("100", 0, "", hdNoticiasIds.Value, null);
            rpNoticasSel.DataBind();

            hdNoticiasIdsFilter.Value = hdNoticiasIds.Value;
        }


        public static System.Drawing.Image Crop(string file, Rectangle selection)
        {
            Bitmap bmp =  (Bitmap)  System.Drawing.Image.FromFile(file);


            // Check if it is a bitmap:
            if (bmp == null)
                throw new ArgumentException("No valid bitmap");

            // Crop the image:
            Bitmap cropBmp = bmp.Clone(selection, bmp.PixelFormat);

            // Release the resources:
            //image.Dispose();

            return cropBmp;
        }



        private void saveJpeg(string path, Bitmap img, long quality)
        {
            // Encoder parameter for image quality
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = this.getEncoderInfo("image/jpeg");

            if (jpegCodec == null)
                return;

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            img.Save(path, jpegCodec, encoderParams);
        }


        private ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
    }
}