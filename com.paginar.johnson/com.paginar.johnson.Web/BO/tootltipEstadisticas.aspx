﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tootltipEstadisticas.aspx.cs" Inherits="com.paginar.johnson.Web.BO.tootltipEstadisticas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Usuarios</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
<script  type="text/javascript">
    function popup(url, ancho, alto) {
        var posicion_x;
        var posicion_y;
        posicion_x = (screen.width / 2) - (ancho / 2);
        posicion_y = (screen.height / 2) - (alto / 2);
        window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

    }
</script>
</head>
<body>

    <form id="form1" runat="server">

    <table width="100%" style="font-size: 14px;">
        <tr>
            <td class="form-item ">
                <asp:Label ID="LblInformation" runat="server" CssClass="descripcion" ></asp:Label>

            </td>
            <td align="right">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" />
            </td>
        </tr>

    </table>    
    <div style="font-size: 14px;">

        <asp:MultiView ID="MVtooltip" runat="server">
            <asp:View ID="Vuser_access_site" runat="server">
                <asp:GridView ID="GridView2" runat="server" 
                    AutoGenerateColumns="False" DataSourceID="ODSUsuarios_acceso" 
                    AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ODSUsuarios_acceso" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_user_access_site_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas" 
                    InsertMethod="InsertRegistroEstadistico">
                    <InsertParameters>
                        <asp:Parameter Name="UsuarioID" Type="Int32" />
                        <asp:Parameter Name="paginaVisitada" Type="String" />
                    </InsertParameters>
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:View>
            <asp:View ID="Vsec_mas_cons" runat="server">

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="ODSSec_mas_consult" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                    </Columns>
                </asp:GridView>

                <asp:ObjectDataSource ID="ODSSec_mas_consult" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_secc_mas_consult_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="titulo" QueryStringField="titulo" 
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
            </asp:View>

            <asp:View ID="banner_mas_consultado" runat="server">
            <asp:GridView ID="GridView7" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" DataSourceID="ODSbanner_mas_consultado"   >
            
                <Columns>
                  
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                  
                </Columns>
                <EmptyDataTemplate>
                    No se encontro información con los parametros de búsqueda ingresados
                </EmptyDataTemplate>
            </asp:GridView>

            <asp:ObjectDataSource ID="ODSbanner_mas_consultado" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="Get_banner_mas_consultado_detalle" 
                TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                <SelectParameters>
                      <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="titulo" QueryStringField="titulo" 
                            Type="String" />
                       
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>


            <asp:View ID="VTop_noticia" runat="server">
            <asp:GridView ID="GridView3" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" DataSourceID="ODSTopNoticias">
                <Columns>
                    <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                        SortExpression="Legajo" />
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                        SortExpression="Apellido" />
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                        SortExpression="Nombre" />
                    <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                        SortExpression="Cluster" />
                </Columns>
             </asp:GridView>
                <asp:ObjectDataSource ID="ODSTopNoticias" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_noticias_mas_visitadas_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter DefaultValue="" Name="notid" 
                            QueryStringField="notidi" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:View>
            <asp:View ID="Vhorario_concurrencia" runat="server">
                <asp:GridView ID="GridView4" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" DataSourceID="ODSHorario_concurrencia">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ODSHorario_concurrencia" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_horario_concurrencia_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="rango" QueryStringField="rango" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:View>
            <asp:View ID="VGrupo_Etario" runat="server">
                <asp:Label ID="lblRango" runat="server"></asp:Label>
                <asp:GridView ID="GridView5" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" DataSourceID="ODSgrupos_etarios">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ODSgrupos_etarios" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_grupo_etario_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="rango" QueryStringField="rango" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:View>
            <asp:View ID="Vhombres_mujeres" runat="server">
                <asp:GridView ID="GridView6" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" DataSourceID="ODSHombres_mujeres">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" 
                            SortExpression="Apellido" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Cluster" HeaderText="Cluster" ReadOnly="True" 
                            SortExpression="Cluster" />
                    </Columns>
                </asp:GridView>
                <asp:ObjectDataSource ID="ODSHombres_mujeres" runat="server" 
                    OldValuesParameterFormatString="original_{0}" 
                    SelectMethod="Get_Estadisticas_hombres_mujeres_detalle" 
                    TypeName="com.paginar.johnson.BL.ControllerEstadisticas">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="modo" QueryStringField="modo" Type="String" />
                        <asp:QueryStringParameter DefaultValue="" Name="desde" QueryStringField="desde" 
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="hasta" QueryStringField="hasta" 
                            Type="DateTime" />
                        <asp:QueryStringParameter DefaultValue="" Name="genero" 
                            QueryStringField="genero" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:View>
        </asp:MultiView>


        
    
    </div>
    </form>
</body>
</html>
