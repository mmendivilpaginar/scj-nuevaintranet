﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.formularios.businesslogiclayer;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class cursos : PageBase
    {

       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // TreeViewCursos.Attributes.Add("onclick", "return OnTreeClick(event)");
                int ClusterExternoPrivilegio;
                ControllerContenido cc = new ControllerContenido();
                if (this.ObjectUsuario.clusteridactual == 9)
                    ClusterExternoPrivilegio = 1;
                else
                    ClusterExternoPrivilegio = this.ObjectUsuario.clusteridactual;

                RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Gestión de Talentos", ClusterExternoPrivilegio).ToString();
                RelationalSystemDataSource1.DataBind();
                TreeViewCursos.DataBind();

                if (TreeViewCursos.Nodes.Count == 1)
                {
                    TreeViewCursos.Visible = false;
                    HiddenFieldItemID.Value = TreeViewCursos.Nodes[0].DataPath;
                    odsContenido.DataBind();
                    RelationalSystemDataSource1.DataBind();
                    TreeViewCursos.DataBind();


                }

                foreach (TreeNode TN in TreeViewCursos.Nodes)
                {
                    if (TN.ChildNodes.Count > 0)
                        PrintRecursive(TN);                    
                }

                TreeViewCursos.CollapseAll();

             }           

        }

        private void PrintRecursive(TreeNode treeNode)
        {
            
            foreach (TreeNode tn in treeNode.ChildNodes)
            {
                if (tn.Text == "Evaluación de Desempeño")
                {

                    //INICIADOS
                    string link = string.Empty;
                    int Legajo = int.Parse(this.ObjectUsuario.legajo.ToString());
                    FormulariosDS.FormularioPmpOperariosDataTable formulariosUsuario = new FormulariosDS.FormularioPmpOperariosDataTable();
                    FachadaDA.Singleton.FormularioPmpOperarios.FillFormulariosByLegajoTipoPeriodo(formulariosUsuario, Legajo, 0, "INICIADOs");

                    foreach (FormulariosDS.FormularioPmpOperariosRow formularioRow in formulariosUsuario)
                    {
                        TreeNode NuevoNodo = new TreeNode();
                       
                       // NuevoNodo.Text = "PMP " +  formularioRow["PeriodoDesc"].ToString().Substring(2);
                        NuevoNodo.Text = formularioRow["PeriodoDesc"].ToString();
                        int LegajoForm = int.Parse(formularioRow["Legajo"].ToString());
                        int PeriodoID = int.Parse(formularioRow["PeriodoID"].ToString());
                        int FormularioID = int.Parse(formularioRow["TipoFormularioID"].ToString());
                        int EstadoID = int.Parse(formularioRow["EstadoID"].ToString());
                        int TipoPeriodoID = int.Parse(formularioRow["TipoPeriodoID"].ToString());

                           string carga = "false";
                            //bool impresion = false;
                            FormulariosController f = new FormulariosController();
                            carga = f.ValidarPeriodoCarga(PeriodoID, DateTime.Now).ToString();

                            string UrlForm = DameURLForm(PeriodoID,FormularioID);

                            //popupPmpO
                            if (carga=="True" && EstadoID==2)
                            {
                                if (FormularioID == 11 || FormularioID == 10)
                                    link = string.Format("popupPmpO('" + "/PMPFY/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                                else                              
                                     link = string.Format("popupPmpO('" + "/PMP/" + "" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                                
                            }
                            //Sino no esta en el periodo de carga, tengo q abrir impresion
                            if ((carga == "False" && EstadoID == 2) || EstadoID == 4)
                            {
                               switch(TipoPeriodoID)
                               {

                                   case 2:
                                       link = string.Format("popupPmpO('" + "/PMP/" + "ImpresionMY.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                                       break;
                                   case 1:
                                       link = string.Format("popupPmpO('" + "/PMP/" + "ExportarWordPMPO.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                                       break;
                                   case 3:
                                       link = string.Format("popupPmpO('" + "/PMPFY/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                                      break;
                               }
                             
                               }


                            
                            
                        //}

                        NuevoNodo.NavigateUrl = "javascript:" + link;


                        tn.ChildNodes.Add(NuevoNodo);
                    }

                    //FachadaDA.Singleton.FormularioPmpOperarios.FillFormulariosByLegajoTipoPeriodo(formulariosUsuario, Legajo, 2, "INICIADOSHISTORICOS");

                    //foreach (FormulariosDS.FormularioPmpOperariosRow formularioRow in formulariosUsuario)
                    //{
                    //    TreeNode NuevoNodo = new TreeNode();

                    //   // NuevoNodo.Text = formularioRow["PeriodoDesc"].ToString();
                    //    NuevoNodo.Text = "MidYear " + formularioRow["PeriodoDesc"].ToString().Substring(2);
                    //    int LegajoForm = int.Parse(formularioRow["Legajo"].ToString());
                    //    int PeriodoID = int.Parse(formularioRow["PeriodoID"].ToString());
                    //    int FormularioID = int.Parse(formularioRow["TipoFormularioID"].ToString());
                    //    int EstadoID = int.Parse(formularioRow["EstadoID"].ToString());
                    //    int TipoPeriodoID = int.Parse(formularioRow["TipoPeriodoID"].ToString());


                    //    //string link = string.Format("popupPmpO('" + "/PMP/Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2},1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                    //    string link = string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);", LegajoForm.ToString(), PeriodoID.ToString(), FormularioID.ToString());
                    //    NuevoNodo.NavigateUrl = "javascript:" + link;
                    //    tn.ChildNodes.Add(NuevoNodo);


                    //}
                }
               // PrintRecursive(tn);
            }
        }

        private string DameURLForm(int PeriodoID, int FormularioID)
        {
          string  UrlForm= string.Empty;

            FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();
            FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 2, PeriodoID, FormularioID);
            if (RelPasosTipoFormulariosDt.Rows.Count > 0)
            {
                FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                UrlForm = RelPasosTipoFormulariosRow.FormAspx;
            }

            return UrlForm;
        }

      protected void TreeViewCursos_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewCursos.SelectedNode.DataPath;
            //if (TreeViewCursos.SelectedNode.Expanded == true)
               // TreeViewCursos.SelectedNode.Collapse();
        }

        protected void TreeViewCursos_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            HiddenFieldItemID.Value = e.Node.DataPath;
            odsContenido.DataBind();
            frmContenido.DataBind();
            string contenidoStr;
            Literal Contenido = (Literal)frmContenido.FindControl("ltContenido");
            contenidoStr = Contenido.Text;
            contenidoStr.Substring(contenidoStr.Length - 3);
            if (contenidoStr.Substring(contenidoStr.Length - 3) == "xls" || contenidoStr.Substring(contenidoStr.Length - 3) == "ppt" || contenidoStr.Substring(contenidoStr.Length - 3) == "doc" || contenidoStr.Substring(contenidoStr.Length - 3) == "htm" || contenidoStr.Substring(0, 4) == "http" || contenidoStr.Substring(contenidoStr.Length - 4) == "xlsx" || contenidoStr.Substring(contenidoStr.Length - 4) == "ppsx")
            {
                e.Node.NavigateUrl = Contenido.Text;
                e.Node.Target = "_blank";
                
               // e.Node.SelectAction = TreeNodeSelectAction.Expand;

            }




            if (contenidoStr.Substring(contenidoStr.Length - 4) == "aspx")
            {
                e.Node.NavigateUrl = Contenido.Text;
                e.Node.Target = "_self";
            }

            if (contenidoStr.IndexOf("sPDA.aspx") > 0)
            {
                e.Node.NavigateUrl = contenidoStr;
                e.Node.Target = "_blank";
            }

            if (e.Node.Text == "SCJ E-Learning")
                e.Node.ToolTip = "En usuario, colocar el Global ID personal de SCJ y en contraseña, la palabra 'welcome'";

            e.Node.SelectAction = TreeNodeSelectAction.Select;

            HiddenFieldItemID.Value = "0";

        }

        protected void TreeViewCursos_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {

        }

        protected void TreeViewCursos_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {

        }

        protected void TreeViewCursos_Load(object sender, EventArgs e)
        {

            if (TreeViewCursos.SelectedNode != null)
            {
                if (TreeViewCursos.SelectedNode.Selected)
                {
                    if (TreeViewCursos.SelectedNode.Expanded == true)
                        TreeViewCursos.SelectedNode.Collapse();
                    else
                        TreeViewCursos.SelectedNode.Expand();
                }

            }
        }

         

    }
}
