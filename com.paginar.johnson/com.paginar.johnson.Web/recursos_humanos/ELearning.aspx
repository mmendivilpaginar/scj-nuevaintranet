﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="ELearning.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.ELearning" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderhead" runat="server">
  <meta http-equiv="Cache-Control" content="no-store,no-cache,must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="-1"/> 
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">

        function popupEL(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "Elearning", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }



       function getDocHeight(doc) {
           var docHt = 0, sh, oh;
           if (doc.height) {
               docHt = doc.height;
           }
           else if (doc.body) {
               if (doc.body.scrollHeight) docHt = sh = doc.body.scrollHeight;
               if (doc.body.offsetHeight) docHt = oh = doc.body.offsetHeight;
               if (sh && oh) docHt = Math.max(sh, oh);
           }
           return docHt;
       }
       function getReSize() {
           var iframeWin = window.frames['IAsp'];
           var iframeEl = window.document.getElementById ? window.document.getElementById('IAsp') : document.all ? document.all['IAsp'] : null;
           if (iframeEl && iframeWin) {
               var docHt = getDocHeight(iframeWin.document);
               if (docHt != iframeEl.style.height) iframeEl.style.height = docHt + 'px';
           }
           else { // FireFox
               var docHt = window.document.getElementById('IAsp').contentDocument.height;
               window.document.getElementById('IAsp').style.height = docHt + 'px';
           }
       }
       function getRetry() {
           getReSize();
           setTimeout('getRetry()', 500);
       }

       $(document).ready(function () {
           // getRetry();
          // document.getElementById('IAsp').src = 'http://desa-t018a:1000/default.aspx';
          // document.getElementById('DivFrame').innerHTML = "<iframe src='http://www.scjlearning.com' width='100%' height='100%'></iframe>";
       });

     
</script> 

 
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="1" >
    <asp:View ID="ViewAdministrativos"  runat="Server">
    <h2>Bienvenido a la sección E-Learning</h2>


   <%-- <iframe name="IAsp" runat="server"  clientidmode="Static" id="IAsp"  width="100%"  marginheight="0" marginwidth="0" scrolling="auto"
       frameborder="0" allowtransparency="true"   height="700">    
      <p>Your browser does not support iframes.</p>
    </iframe>--%>

  <%--  <div id="DivFrame">
    
    </div>--%>

<a id="LinkAdministrativos" href="/Recursos_humanos/EL_Iframe.aspx?DeepLink=http://www.scjlearning.com" target="_blank">Haga clic aquí para ingresar al curso</a>

    </asp:View>
    <asp:View ID="ViewOperarios" runat="Server">

    <h2>Bienvenido a la sección E-Learning</h2>
    <p>
    <label>A continuación se detallan los cursos habilitados a los que usted puede acceder.</label>
    </p>
    <div class="AspNet-GridView">
    
    
        <asp:Repeater runat="server" ID="RepeaterCursosDisponibles" onitemcommand="RepeaterCursosDisponibles_ItemCommand">
        <HeaderTemplate>
        <table>
        <thead>
        <tr>
        <%--<th>Código</th>--%>
        <th>Descripción</th>
        <th></th>
        </tr>
        </thead>
        <tbody>
        </HeaderTemplate>
        <ItemTemplate>
        <tr>
  <%--      <td>
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Codigo") %>'></asp:Label>
        </td>--%>
         <td>
            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
        </td>
        
        
         <td align="center">
         <%--    <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Iniciar curso" CommandArgument='<%# Eval("Link") %>' ImageUrl="~/css/images/play.png" CommandName="Select" Width="20px"  />--%>
         <%--<asp:LinkButton runat="server" ID="LinkButtonCurso" CommandName="Select" CommandArgument='<%# Eval("Link") %>'  Text='<%# Eval("Descripcion") %>'>
         </asp:LinkButton> --%>        

           <a href='<%#  Eval("Link")  %>'    target="_blank"  >
              <asp:Image ID="Image1" ImageUrl="~/css/images/play.png" runat="server" Width="22px" />
           </a>

         </td>   
         </tr>     
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
         </table>
        </FooterTemplate>
        </asp:Repeater>

</div>
        <%--<asp:ObjectDataSource runat="server"  OldValuesParameterFormatString="original_{0}" SelectMethod="getAllCursos" 
            TypeName="com.paginar.johnson.BL.ControllerE_Learning" 
            ID="ObjectDataSourceCursos"></asp:ObjectDataSource>--%>

            <br />
            <br />
            
    
    </asp:View>
    </asp:MultiView>

    
</asp:Content>
