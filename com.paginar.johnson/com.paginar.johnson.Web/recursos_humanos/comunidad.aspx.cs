﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;


namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class comunidad : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //seteo el texto fijo según el país del cluster
/*                if (this.ObjectUsuario.clusteridactual < 4)
                {
                    mvClusterStaticText.ActiveViewIndex = this.ObjectUsuario.clusteridactual - 1;
                }
                else {
                    mvClusterStaticText.ActiveViewIndex = 0;
                }
 
 */

                if (this.ObjectUsuario.clusteridactual > 4)
                {
                    switch (this.ObjectUsuario.clusteridactual)
                    {
                        case 5:
                        case 9:
                            mvClusterStaticText.ActiveViewIndex = 0;
                            break;
                        case 6:
                            mvClusterStaticText.ActiveViewIndex = 1;
                            break;
                        case 7:
                            mvClusterStaticText.ActiveViewIndex = 2;
                            break;
                        case 8:
                            mvClusterStaticText.ActiveViewIndex = 3;
                            break;


                    }
                }
                else
                {

                    mvClusterStaticText.ActiveViewIndex = this.ObjectUsuario.clusteridactual - 1;
                }

                if (this.ObjectUsuario.clusteridactual != 3 && this.ObjectUsuario.clusteridactual != 4 && this.ObjectUsuario.clusteridactual != 7 && this.ObjectUsuario.clusteridactual != 8 & this.ObjectUsuario.clusteridactual != 5 & this.ObjectUsuario.clusteridactual != 6 & this.ObjectUsuario.clusteridactual != 9)  //Sin URU y PAR
                //if (this.ObjectUsuario.clusteridactual != 3 && this.ObjectUsuario.clusteridactual != 4 && this.ObjectUsuario.clusteridactual != 7 && this.ObjectUsuario.clusteridactual != 8)  //Sin URU y PAR
                {

                    pnlTreeview.Visible = true;

                    TreeViewComunidad.Attributes.Add("onclick", "pageScroll()");

                    ControllerContenido cc = new ControllerContenido();
                    RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Comunidad - Responsabilidad Social Empresaria", this.ObjectUsuario.clusteridactual).ToString();
                }


            }
        }

        protected void TreeViewComunidad_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewComunidad.SelectedNode.DataPath;
        }

    }
}