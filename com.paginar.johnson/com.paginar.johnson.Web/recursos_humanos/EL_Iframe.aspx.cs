﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class EL_Iframe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["deeplink"]))
            {
                string url = Request.QueryString["deeplink"].ToString();
                HiddenFieldURL.Value = url;
               // IAsp.Attributes.Add("src", url);
            }

        }
    }
}