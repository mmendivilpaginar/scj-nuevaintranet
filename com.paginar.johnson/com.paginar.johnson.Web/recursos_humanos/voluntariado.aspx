﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="voluntariado.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.voluntariado" %>

<%@ Register Src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia"
    TagPrefix="uc1" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="nobg">
    </div>
    <%--Multiview para los textos estáticos, debe estar en este orden para que el seteo desde el código funcione correctamente.--%>
    <asp:MultiView ID="mvClusterStaticText" runat="server">
        <asp:View ID="vArgentina" runat="server">
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                <asp:TabPanel HeaderText="Voluntariado" ID="Panel1" runat="server">
                    <ContentTemplate>
                        <p>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/voluntariado2.jpg" /><br />
                        </p>
                        <p class="destacado">
                            Nuestro objetivo es brindar un espacio de acción social para que los empleados y
                            jubilados de la compañía, que así lo deseen, puedan involucrarse genuinamente en
                            la construcción de una sociedad mejor, a través del compromiso y el trabajo responsable
                            con la comunidad.
                        </p>
                        <p>
                            <img src="../images/voluntariado-gente1.jpg" class="floatRight" />
                            Para su desarrollo, estamos trabajando con la Fundación Compromiso, especialista
                            en Voluntariados Corporativos. El Programa también pretende potenciar el vínculo
                            con las comunidades cercanas a nuestras plantas, como un modo de expresión de los
                            valores corporativos.
                        </p>
                        <p>
                            <img src="../images/voluntariado-gente2.jpg" class="floatRight" />
                            Es por ello que las acciones de los voluntarios se canalizan en la ayuda a organizaciones
                            sociales, con las cuales mantenemos un vínculo muy cercano desde hace varios años,
                            ya que la compañía colabora con ellos constantemente, con donaciones y acciones
                            de distinta índole.</p>
                        <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" runat="server" Categoriaid="58" />
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel HeaderText="Manual del Voluntario" ID="TabPanel1" runat="server">
                    <ContentTemplate>
                        <asp:TreeView ID="TreeViewVoluntariado" runat="server" DataSourceID="RelationalSystemDataSource1"
                            OnSelectedNodeChanged="TreeViewVoluntariado_SelectedNodeChanged">
                            <DataBindings>
                                <asp:TreeNodeBinding TextField="Name" />
                            </DataBindings>
                        </asp:TreeView>
                        <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
                        <a name="contenido-abajo"></a>
                        <br />
                        <asp:UpdatePanel ID="updContenido" runat="server">
                            <ContentTemplate>
                                <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
                                <div id="divContenido">
                                    <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="TreeViewVoluntariado" EventName="SelectedNodeChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </asp:View>
        <asp:View ID="vChile" runat="server">
            <p>
                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/voluntariado2.jpg" /><br />
            </p>
            <p class="destacado">
                Nuestro objetivo es brindar un espacio de acción social para que los empleados de
                la compañía, que así lo deseen, puedan involucrarse genuinamente en la construcción
                de una sociedad mejor, a través del compromiso y el trabajo responsable con la comunidad.
            </p>
            <p>
                El Programa también pretende potenciar el vínculo con las comunidades donde están
                insertas nuestras instalaciones, como un modo de expresión de los valores corporativos.
                Es por ello que las acciones de los voluntarios se canalizan en la ayuda a organizaciones
                sociales, con las cuales mantenemos un vínculo muy cercano desde hace varios años,
                ya que la compañía colabora con ellos constantemente, con donaciones y acciones
                de distinta índole.</p>
            <p>
                <ul>
                    <li><a href="documents/Manual_del_Voluntario_CHI.doc" target="_blank">Manual del voluntario</a>
                    </li>
                    <li><a href="http://www.prohumana.cl/" target="_blank">Fundación Prohumana </a></li>
                </ul>
            </p>
            <p>
                <h4>
                    Instituciones participantes del programa:</h4>
                <ul>
                    <li>Escuela 1193 “Carlos Berríos” (ex La Alianza) de Cerro Navia - Santiago:<br />
                        Directora: María Eliana Román
                        <br />
                        Contacto: Dora Pizarro
                        <br />
                        Dirección: Calle La Capilla 8514 Comuna de Cerro Navia
                        <br />
                        Teléfono: 6431687
                        <br />
                        E-mail: <a href="mailto:ali1193@gmail.com">ali1193@gmail.com</a></li>
                    <%--<li>Escuela Santa Julia de Viña del Mar:<br />
                        Directora: Graciela Cárdenas
                        <br />
                        Contacto: Petronila Carrasco (Pety)
                        <br />
                        Dirección: Calle Dionisio González S/N Santa Julia, Viña del Mar
                        <br />
                        Teléfono: 2393582
                        <br />
                        E-mail: <a href="mailto:gracafi@yahoo.es">gracafi@yahoo.es</a></li>--%>
                </ul>
            </p>
            <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia2" runat="server" Categoriaid="58" />
        </asp:View>
        <asp:View ID="vUruguay" runat="server">
            <p>
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/voluntariado2.jpg" /><br />
            </p>
            <p class="destacado">
                El objetivo del programa de voluntariado corporativo es brindar a los empleados
                de SCJ la oportunidad de involucrarse en la construcción de una sociedad mejor.
            </p>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p>
                            Para ello, anualmente, el equipo de SCJ Uruguay participa de una jornada de voluntariado
                            con el fin de colaborar con la comunidad.
                        </p>
                        <p>
                            A su vez, desde 2008 funciona el programa de Padrinazgo, a partir del cual los empleados
                            de la compañía apadrinan 4 instituciones educativas. Ellos son responsables de relevar
                            las necesidades de dichas organizaciones, preparar el plan anual, administrar el
                            presupuesto, desarrollar y monitorear las actividades y evaluar la performance del
                            año.
                            <ul>
                                <li>Criterios del Programa de Padrinazgo: <a href="documents/SCJ_Uy-Criterios_Programa_Padrinazgo_2010_2011.doc">
                                    Click aquí</a></li>
                                <li>Presentación de acciones realizadas a partir de Padrinazgo 2010: <a href="documents/Padrinazgos_10.ppt">
                                    Click aquí</a></li>
                            </ul>
                        </p>
                    </td>
                    <td valign="top" width="20%">
                        <img src="../images/voluntariado-gente1_URU.jpg" class="floatRight" /><br />
                        <img src="../images/voluntariado-gente2_URU.jpg" class="floatRight" />
                    </td>
                </tr>
            </table>
            <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia3" runat="server" Categoriaid="58" />
        </asp:View>
        <asp:View ID="vParaguay" runat="server">
            <p>
                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/voluntariado2.jpg" /><br />
            </p>
            <p class="destacado">
                El objetivo del programa de voluntariado corporativo es brindar a los empleados
                de SCJ la oportunidad de involucrarse en la construcción de una sociedad mejor.
            </p>
            <p>
                Para ello, anualmente, el equipo de SCJ Paraguay participa de una jornada de voluntariado
                con el fin de colaborar con la comunidad.</p>
            <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia4" runat="server" Categoriaid="58" />
        </asp:View>
    </asp:MultiView>
</asp:Content>
