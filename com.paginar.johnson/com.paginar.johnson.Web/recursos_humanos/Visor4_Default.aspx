﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Visor4_Default.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.Visor4_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">


    <script type="text/javascript">
function ImprimirReporte() {
            $('#PanelFiltros').hide();
            $('#PanelBotonesReporteGeneral').hide();
            $('#PanelBotones').hide();
            //MostrarOcultarColumna('gvReporteGeneral', 12, 'hide');
            window.print();
            $('#PanelFiltros').show();
            $('#PanelBotonesReporteGeneral').show();
            $('#PanelBotones').show();
            //MostrarOcultarColumna('gvReporteGeneral', 12, 'show');
        }

        function MostrarOcultarColumna(Grilla, columna, modo) {

            var aux = document.getElementById(Grilla);
            var grid;
            if (aux.nodeName == "TABLE")
                grid = aux;
            else
                grid = aux.children[0];

                      
            var cell1;                       
            var row;
            if (grid.rows.length > 0) {                
                for (i = 0; i < grid.rows.length; i++) {
                    //get the reference of first column 
                    row = grid.rows[i];
                    //var ImageVer = grid.rows[i].getElementById('ImageCommand');
                    cell1 = grid.rows[i].cells[columna];
                    if (cell1) {
                        if (modo == 'hide')
                            $(cell1).hide();
                        else
                            $(cell1).show();
                    }
                    else {
                        cell1 = grid.rows[i].cells[columna - 1];
                        if (cell1) {
                            var ImagVer = cell1.childNodes[0];
                            if (ImagVer) {
                                if (modo == 'hide')
                                    $(cell1).hide();
                                else
                                    $(cell1).show();
                            } 
                        }
                    }
                }
            }            
        }

    </script>

<h2>Listado Detallado de Ganancias</h2>
    <asp:Label ID="Label1" runat="server" Text="Debido a la sensibilidad de los datos mostrados, por favor, ingrese nuevamente su contrase&ntilde;a de Intranet antes de realizar la b&uacute;squeda."></asp:Label>
    <br />
    <br />
    <asp:Panel ID="pnlClave" runat="server" DefaultButton="btnBuscar">
    <div class="form-item leftHalf">
        <asp:Label ID="lblClave" runat="server" Text="Contrase&ntilde;a: "></asp:Label>
        <asp:TextBox ID="txtClave" runat="server" TextMode="Password" 
            ValidationGroup="ValidarClave"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validatorClave" runat="server" 
            ControlToValidate="txtClave" ErrorMessage="Por favor, ingrese su contrase&ntilde;a." 
            Font-Bold="False" ForeColor="Red" ValidationGroup="ValidarClave"></asp:RequiredFieldValidator>
        <br /><br />
    
        <asp:Button ID="btnBuscar" runat="server" onclick="btnBuscar_Click" 
        Text="Buscar" ValidationGroup="ValidarClave" />
    </div>
    </asp:Panel>
    <%--<asp:Button ID="btnImprimir" runat="server" Text="Imprimir" OnClientClick="ImprimirReporte()"/>--%>

    <div class="form-item rightHalf">
        <asp:Label ID="lblAnio" runat="server" Text="A&ntilde;o:"></asp:Label>
        <asp:DropDownList ID="lstAnios" runat="server" AppendDataBoundItems="True">
        </asp:DropDownList>
        <br /><br /><br />

        <asp:Image ID="imgPrinter" runat="server" ImageUrl="~/images/printer.png" />
        <asp:HyperLink ID="hlkImprimir" runat="server">Imprimir</asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Image ID="imgInstructivo" runat="server" ImageUrl="~/images/page_acrobat.png" />
        <asp:HyperLink ID="hlkInstructivo" runat="server">Instructivo de interpretación del visor</asp:HyperLink>
    </div>

      
      <div id="ocultarlista" style="visibility:hidden">
        <asp:Label ID="lblMes" runat="server" Text="Mes:"></asp:Label>
        <asp:DropDownList ID="lstMeses" runat="server">
            <asp:ListItem Value="1">Enero</asp:ListItem>
            <asp:ListItem Value="2">Febrero</asp:ListItem>
            <asp:ListItem Value="3">Marzo</asp:ListItem>
            <asp:ListItem Value="4">Abril</asp:ListItem>
            <asp:ListItem Value="5">Mayo</asp:ListItem>
            <asp:ListItem Value="6">Junio</asp:ListItem>
            <asp:ListItem Value="7">Julio</asp:ListItem>
            <asp:ListItem Value="8">Agosto</asp:ListItem>
            <asp:ListItem Value="9">Setiembre</asp:ListItem>
            <asp:ListItem Value="10">Octubre</asp:ListItem>
            <asp:ListItem Value="11">Noviembre</asp:ListItem>
            <asp:ListItem Value="12">Diciembre</asp:ListItem>
        </asp:DropDownList>
    </div>    
  
    <asp:HiddenField ID="hfLegajo" runat="server" />
    
    <asp:Label ID="lblMensaje" runat="server" Text="" CssClass="mensaje error"></asp:Label><br />
    <h3 class="MesLiquidado"><asp:Label ID="lblMesLiquidado" runat="server" Text="Mes cierre de liquidaci&oacute;n: "></asp:Label><asp:Label ID="lblUltimoMesLiquidado" runat="server" Text=""></asp:Label></h3>
    

    <div id="mygrilla" style="visibility:hidden">
    <asp:GridView runat="server" ID ="myGrilla" AutoGenerateColumns="False" 
    SkinID="gridMain_Visor4"  >
        <Columns >
            <asp:BoundField DataField="SOB_LINEA" HeaderText="Listado"   />
        </Columns>
    </asp:GridView>
    </div>
    
    <div class="recuadroPre">
    <pre runat="server" class="textoPlano" ID="TextoFormateado">
    </pre>
    </div>

    <asp:Literal ID="ltrMje" runat="server"></asp:Literal>

    <asp:ObjectDataSource ID="ObjectDataSourceAnios" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="com.paginar.johnson.DAL.Visor4_DataSetTableAdapters.Visor4_GetAniosLiquidadosTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="hfLegajo" Name="Legajo" PropertyName="Value" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="ObjectDataSourceGridView" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="com.paginar.johnson.DAL.Visor4_DataSetTableAdapters.Visor4_GetRetencionesTableAdapter">
        <SelectParameters>
            <asp:ControlParameter ControlID="hfLegajo" Name="Legajo" PropertyName="Value" 
                Type="Int32" />
            <asp:ControlParameter ControlID="lstMeses" Name="Mes" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="lstAnios" Name="Anio" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
</asp:Content>
