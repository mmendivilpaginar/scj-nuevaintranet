﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.membership;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class ELearning : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                int LegajoLog = 0;
                User Usuario = null;
                Usuarios usercontroller = new Usuarios();
                ControllerUsuarios uc = new ControllerUsuarios();

                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                LegajoLog = Usuario.GetLegajo();

                ControllerE_Learning ELC = new ControllerE_Learning();

                DSE_Learning.EL_CursosDataTable DTC = new DSE_Learning.EL_CursosDataTable();

                if (uc.DameTipoEvaluado(LegajoLog, null) == "Administrativo")
                {
                    DTC= ELC.getCursosAdministrativos();
                    
                   
                 //   string url = "http://www.scjlearning.com";
                   // IAsp.Attributes.Add("src", url);
                    
                }
                else
                {
                    DTC = ELC.getCursosOperarios();
                  
                }

                RepeaterCursosDisponibles.DataSource = DTC;
                RepeaterCursosDisponibles.DataBind();

            }

        }

       protected void RepeaterCursosDisponibles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName =="Select")
            {
                string url = e.CommandArgument.ToString();
               // IAsp.Attributes.Add("src", url);
               // MultiView1.ActiveViewIndex = 0;
               // LinkButtonVolver.Visible = true;

                ClientScript.RegisterStartupScript(typeof(Page), "OpenCurso", "<script type='text/javascript'>$(function() { popupEL('/Recursos_humanos/EL_Iframe.aspx?DeepLink=" + url + "',1000,600);return false;});</script>");    
             
            
            }
        }


       private void EjecutarScript(string js)
       {
           ScriptManager sm = ScriptManager.GetCurrent(this);
           if (sm.IsInAsyncPostBack)
               System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
           else
               this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


       }

       protected void LinkButtonVolver_Click(object sender, EventArgs e)
       {
           MultiView1.ActiveViewIndex = 1;
       }
    }
}