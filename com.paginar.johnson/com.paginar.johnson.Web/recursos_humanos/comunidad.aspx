﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="comunidad.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.comunidad" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script src="../js/init.js" type="text/javascript"></script>
    <%--Multiview para los textos estáticos, debe estar en este orden para que el seteo desde el código funcione correctamente.--%>
    <asp:MultiView ID="mvClusterStaticText" runat="server">
        <asp:View ID="vArgentina" runat="server">
            <h2>
                Relaciones con la Comunidad</h2>
            <p>
                Porque creemos en una comunidad mejor, en SC Johnson & Son Argentina, auspiciamos,
                promovemos y desarrollamos distintas acciones, que se focalizan, fundamentalmente,
                en las comunidades cercanas a nuestras plantas. En la Argentina, el programa de
                Relaciones con la Comunidad cuenta con cuatro pilares fundamentales: Educación,
                Salud, Promoción Social y Medio Ambiente, que se integran en el Voluntariado Corporativo.</p>
        </asp:View>
        <asp:View ID="vChile" runat="server">
            <h2>
                Relaciones con la Comunidad</h2>
            <p>
                Porque creemos en una comunidad mejor, nuestro Programa de Relaciones con la Comunidad
                cuenta con cuatro pilares fundamentales: Educación, Salud, Promoción Social y Medio
                Ambiente.
            </p>
            <p>
                En SC Johnson & Son Chile, auspiciamos, promovemos y desarrollamos distintas acciones
                que se focalizan, fundamentalmente, en las comunidades cercanas a nuestras instalaciones.
            </p>
            <p>
                <ul>
                    <li>Sumar Voluntariado: <a href="voluntariado.aspx">click aquí</a></li>
                </ul>
            </p>
        </asp:View>
        <asp:View ID="vUruguay" runat="server">
            <h2>
                Relaciones con la Comunidad</h2>
            <p class="destacado">
                Porque creemos en una comunidad mejor, en SC Johnson & Son auspiciamos, promovemos
                y desarrollamos distintas acciones que se focalizan, fundamentalmente, en las comunidades
                en las cuales operamos.
            </p>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <p>
                            En caso de SCJ Uruguay, nuestro Programa de Relaciones con la Comunidad está compuesto
                            de la siguiente manera:</p>
                        <ul>
                            <li>
                                <h5>
                                    Fundación Logros</h5>
                            </li>
                        </ul>
                        <p>
                            Colaboramos con Fundación Logros: creación de huertas orgánicas y montes frutales
                            en escuelas.
                            <br />
                            Desde 2004: 22 huertas y 10 plantaciones de árboles frutales.
                            <br />
                            <a href="http://www.fundacionlogros.org.uy/" target="_blank">http://www.fundacionlogros.org.uy</a>
                        </p>
                        <ul>
                            <li>
                                <h5>
                                    Unicef – MandalaVos</h5>
                            </li>
                        </ul>
                        <p>
                            Contribuimos con Unicef y Fundación El Abrojo, en el proyecto MandalaVos: integración
                            y desarrollo de jóvenes de diferentes realidades socioculturales.<br />
                            Número de beneficiarios por año: más de 120.
                            <br />
                            <a href="http://www.unicef.org/uruguay/spanish/overview.html" target="_blank">http://www.unicef.org/uruguay/spa...</a>
                        </p>
                        <ul>
                            <li><a href="voluntariado.aspx">
                                <h5>
                                    Sumar Voluntariado</h5>
                            </a></li>
                        </ul>
                    </td>
                    <td width="20%" valign="top">
                        <img src="../images/comunidad_URU.JPG" />
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="vParaguay" runat="server">
            <h2>
                Relaciones con la Comunidad</h2>
            <p class="destacado">
                Porque creemos en una comunidad mejor, en SC Johnson & Son auspiciamos, promovemos
                y desarrollamos distintas acciones que se focalizan, fundamentalmente, en las comunidades
                en las cuales operamos.
            </p>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-top: 10px">
                        <p>
                            En caso de SCJ Paraguay, nuestro Programa de Relaciones con la Comunidad está compuesto
                            de la siguiente manera:</p>
                        <h4>
                            Fundación Dequení</h4>
                        <p>
                            Apadrinamos a Fundación Dequení a partir de la inversión social en la Casa de Acogida.
                            SCJ colabora con dos programas:
                        </p>
                    </td>
                    <td width="20%" valign="top" align="center" style="padding-left: 10px">
                        <img src="../images/Logo_DEQUENI.JPG" /><br />
                    </td>
                </tr>
            </table>
            <p>
                <ul>
                    <li>Jardín y Preescolar: becas para la atención integral de 30 niños de entre 4 y 5
                        años.</li>
                    <li>Segundo Ciclo Escolar: becas para la atención integral de 12 niños</li>
                </ul>
            </p>
            <p>
                A su vez, anualmente el equipo de SCJ Paraguay realiza una jornada de Voluntariado
                en Dequení (ver <a href="voluntariado.aspx">Sumar Voluntariado</a>)
            </p>
            <p>
                Para más información sobre Fundación Dequení, <a href="documents/Alianza_Dequeni_SCJ.ppt">
                    clic aquí</a>.
            </p>
            <a href="http://www.dequeni.org.py/" target="_blank">http://www.dequeni.org.py</a>
        </asp:View>
    </asp:MultiView>
    <asp:Panel id="pnlTreeview" runat="server" Visible="false">
    <asp:TreeView ID="TreeViewComunidad" runat="server" DataSourceID="RelationalSystemDataSource1"
        CssClass="TreeView" OnSelectedNodeChanged="TreeViewComunidad_SelectedNodeChanged"
        NoExpandImageUrl="~/css/images/bullet-azul.png">
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
    <br />
    <a name="contenido-abajo"></a>
    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
            <div id="divContenido">
                <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                    <ItemTemplate>
                        <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewComunidad" EventName="SelectedNodeChanged" />
        </Triggers>
    </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
