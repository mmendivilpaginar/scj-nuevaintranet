﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="cursos.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.cursos" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

   <script type="text/javascript">



       function popupPmpO(url, ancho, alto) {
           var posicion_x;
           var posicion_y;
           posicion_x = (screen.width / 2) - (ancho / 2);
           posicion_y = (screen.height / 2) - (alto / 2);
           window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

       }



       function abrirPMP(gstrASP, FormularioID, gstrgrabadolink) {
           //alert(gstrASP + ' ' + FormularioID + ' ' + gstrgrabadolink);
           gstrgrabadolink = escape(gstrgrabadolink);
           var url = "/SessionTransfer.aspx?dir=2asp&url=" + ('servicios/' + gstrASP + '?FormularioID=' + FormularioID + '%26ASP=' + gstrASP + gstrgrabadolink);
           //alert(url);
           var win = window.open(url, "mywindow", "location=0,status=1,scrollbars=0,width=950,height=700,menubar=0,top=10,left=10");
           win.focus();
       }
</script> 
    <h2>Gestión de Talento</h2>
    

    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>

         <asp:TreeView ID="TreeViewCursos" runat="server" DataSourceID="RelationalSystemDataSource1"
        OnSelectedNodeChanged="TreeViewCursos_SelectedNodeChanged"         
        ontreenodedatabound="TreeViewCursos_TreeNodeDataBound" 
                onload="TreeViewCursos_Load" 
                ontreenodecollapsed="TreeViewCursos_TreeNodeCollapsed" 
        PopulateNodesFromClient="False"  >
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
    <br />

           
    <a name="contenido-abajo"></a>
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
            <div id="divContenido">
                <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                    <ItemTemplate>                        
                        <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>                        
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
          <br />
         </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewCursos" EventName="SelectedNodeChanged" />
            <asp:AsyncPostBackTrigger ControlID="TreeViewCursos" EventName="Load" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
