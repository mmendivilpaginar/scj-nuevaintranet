﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class voluntariado : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                /*
                 
                    0	Todos	Todos	NULL
                    1	Argentina	Arg	ar.png
                    2	Chile	Chi	cl.png
                    3	Uruguay	Uru	uy.png
                    4	Paraguay	Par	py.png
                    5	Externos Arg.	Ext	ar.png
                    6	Externos Cl.	ExtCl	cl.png
                    7	Externos Ur.	ExtUr	uy.png
                    8	Externos Py.	ExtPy	py.png                 
                 
                 */
                //seteo el texto fijo según el país del cluster
                if (this.ObjectUsuario.clusteridactual > 4)
                {
                    switch (this.ObjectUsuario.clusteridactual)
                    {
                        case 5:
                        case 9:
                            mvClusterStaticText.ActiveViewIndex = 0;
                            break;
                        case 6:
                            mvClusterStaticText.ActiveViewIndex = 1;
                            break;
                        case 7:
                            mvClusterStaticText.ActiveViewIndex = 2;
                            break;
                        case 8:
                            mvClusterStaticText.ActiveViewIndex = 3;
                            break;


                    }
                }
                else
                {

                    mvClusterStaticText.ActiveViewIndex = this.ObjectUsuario.clusteridactual - 1;
                }

                TreeViewVoluntariado.Attributes.Add("onclick", "pageScroll()");
                ControllerContenido cc = new ControllerContenido();
                RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Voluntariado Corporativo", this.ObjectUsuario.clusteridactual).ToString();
            }
        }

        protected void TreeViewVoluntariado_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewVoluntariado.SelectedNode.DataPath;
        }
    }
}