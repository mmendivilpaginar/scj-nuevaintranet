﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EL_Iframe.aspx.cs" Inherits="com.paginar.johnson.Web.recursos_humanos.EL_Iframe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-Learning</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/backoffice.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>

    <style type="text/css">
    .modalBackground {
	background-color:#000;
	filter:alpha(opacity=70);
	opacity:0.7;
}

.precarga {
	color: #aaa;
	font-weight: bold;
	text-align:center;
}

  


    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            url = $('input[id$=HiddenFieldURL]').val()
            if (url != 'http://www.scjlearning.com') {
                $('#DivCargando').show();
                $('#iframe').hide();
            }
            else {
                $('#DivCargando').hide();
                $('#iframe').show();
            }

            $('#IAsp').attr("src", url);

            if (url != 'http://www.scjlearning.com')
                WaitForIFrame();
        });

        iframe = document.getElementById("IAsp");

        

        function WaitForIFrame() {
            if (document.getElementById("IAsp").readyState != "complete") {
                setTimeout("WaitForIFrame();", 200);
            } else {
                $('#DivCargando').hide();
                $('#iframe').show();
            }
        }



    </script>
</head>
<body>
    <form id="form1" runat="server">
         <asp:HiddenField ID="HiddenFieldURL" runat="server" Value="" />
        <div id="wrapper">
            <div id="wrapper-inner">

                <div id="header">
                    <div id="header-inner">
                        <div id="logo">                   
                            <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server" ImageUrl="~/images/logo.png"   AlternateText="Johnson"/> 
                         </div>
                        <h1>SCJ <span>Cluster Cono Sur</span></h1>  
                                                                
                    </div>
                </div><!--/header-->

                <div id="main">
                    <div id="main-inner">

                        <div id="content">
                            <div id="content-inner">
                             <p id="FechaHoraLabel" style="text-align:right">
                               <asp:LinkButton ID="LinkButtonVolver" runat="server" PostBackUrl="/recursos_humanos/Elearning.aspx"  
                                     OnClientClick="javascript:window.close();">Cerrar</asp:LinkButton> 
                        </p>

                               <div class="precarga" id="DivCargando">
                                <div>
                                    <asp:Image AlternateText="Procesando" ID="ImgProc" ImageUrl="~/pmp/img/ajax-loader.gif"
                                        runat="server" />
                                    <p>
                                        Cargando curso. Espere por favor....</p>
                                </div>
                
                             <div style="POSITION: fixed; WIDTH: 983px; HEIGHT: 2057px; TOP: 0px; LEFT: 0px; " id="backgroundElement" class="modalBackground"></div>
                             </div>
                               <div id="iframe">
                                 <iframe name="IAsp" runat="server"  clientidmode="Static" id="IAsp"  width="100%"  marginheight="0" marginwidth="0" scrolling="auto"
       frameborder="0" allowtransparency="true"   height="700">    
      <p>Your browser does not support iframes.</p>
    </iframe>
    </div>

    
                            </div>
                        </div>

                    </div>

     

          
                </div>

                <div id="footer">
                </div>
        
            </div><!--/wrapper-inner-->
        </div><!--/wrapper-->                
    </form>
</body>
</html>
