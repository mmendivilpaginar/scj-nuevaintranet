﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Text;

namespace com.paginar.johnson.Web.recursos_humanos
{
    public partial class Visor4_Default : PageBase
    {

        private Visor4_BL MyObjectBL
        {

            get
            {

                //if (this.Session["ObjBL"] == null)
                //{
                //    Visor4_BL myBl = new Visor4_BL();
                //    this.Session["ObjBL"] = myBl;
                //}

                //return this.Session["ObjBL"] as Visor4_BL;

                Visor4_BL myBl = new Visor4_BL();

                //myBl.unValor = 2;
                return myBl;
            }

        }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMensaje.Visible = false;
            lblMesLiquidado.Visible = false;
            lblUltimoMesLiquidado.Visible = false;

            //btnImprimir.Attributes.Add("onclick", "window.open('../servicios/imprimirVisor4.aspx" + qstring + "'); return false;");
            string qstring = "?Legajo=0&Mes=0&Anio=0";
            hlkImprimir.NavigateUrl = "../servicios/imprimirVisor4.aspx" + qstring;
            hlkInstructivo.NavigateUrl = "documents/Instructivo_Visor_4ta_Categoria.pdf";

            if (!Page.IsPostBack)
            {
                Session["ReintentosClave"] = 0;

                //Obtener el nro. de legajo del usuario conectado
                hfLegajo.Value = this.ObjectUsuario.legajo.ToString();

                //Obtener años liquidados del usuario conectado y bindearlos en la lista de años
                Visor4_BL ControllerVisor = new Visor4_BL();
                lstAnios.DataSource = ControllerVisor.Repositorio.GetAniosLiquidados(int.Parse(hfLegajo.Value));
                lstAnios.DataValueField = "Anios";
                lstAnios.DataTextField = "Anios";
                lstAnios.DataBind();
                
                //Desactivar botones y listados si el usuario no tiene años liquidados
                if (lstAnios.Items.Count == 0)
                {
                    lstAnios.Items.Add(new ListItem("Ninguno", "0"));
                    lstAnios.SelectedValue = "0";

                    lstMeses.Items.Clear();
                    lstMeses.Items.Add(new ListItem("Ninguno", "0"));
                    lstMeses.SelectedValue = "0";

                    btnBuscar.Enabled = false;

                    txtClave.Enabled = false;

                    //validatorClave.Visible = false;
                }

                hlkImprimir.Enabled = false;
                hlkImprimir.Visible = false;
                imgPrinter.Visible = false;
                hlkInstructivo.Enabled = false;
                hlkInstructivo.Visible = false;
                imgInstructivo.Visible = false;

                SeleccionarMesLiquidado(ControllerVisor);
                //lstMeses.SelectedValue = Mes;
                
            }
        }

        private void SeleccionarMesLiquidado(Visor4_BL ControllerVisor)
        {
            //Traer ultimo mes liquidado y seleccionarlo en la lista de meses
            DAL.Visor4_DataSet.Visor4_GetMesUltimaLiquidacionDataTable tab = ControllerVisor.GetMesUltimaLiquidacion(int.Parse(hfLegajo.Value));
            if (tab.Count > 0)
            {
                string Mes = tab[0].Mes.ToString();
                if (!String.IsNullOrEmpty(Mes))
                {
                    lstMeses.SelectedValue = Mes;
                }
                else lstMeses.SelectedValue = "0";
            }

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Visor4_BL ControllerVisor = new Visor4_BL();

            DAL.Visor4_DataSet.Visor4_GetMesUltimaLiquidacionDataTable tab = ControllerVisor.GetMesUltimaLiquidacion(int.Parse(hfLegajo.Value));
            string Mes = tab[0].Mes.ToString();
            if (String.IsNullOrEmpty(Mes)) Mes = "1";
            
            //myGrilla.DataSource = ControllerVisor.GetRetenciones(int.Parse(hfLegajo.Value), int.Parse(lstMeses.SelectedValue.ToString()), int.Parse(lstAnios.SelectedValue.ToString()));
            //myGrilla.DataBind();

            //Obtener hfLegajo.Value de la combinacion de Legajo y Clave
            string value = "";
            if (this.ObjectUsuario.Clave == txtClave.Text)
            {
                //value = ControllerVisor.GetRetencionesInText(ControllerVisor.GetRetenciones(int.Parse(hfLegajo.Value), int.Parse(lstMeses.SelectedValue.ToString()), int.Parse(lstAnios.SelectedValue.ToString())));
                //value = value.Replace("\n", "<br/>");
                Session["ReintentosClave"] = 0;
                value = ControllerVisor.GetRetencionesInText(ControllerVisor.GetRetenciones(int.Parse(hfLegajo.Value), int.Parse(Mes), int.Parse(lstAnios.SelectedValue.ToString())));
            }
            else
            {
                int ReintentosClave = int.Parse(Session["ReintentosClave"].ToString());
                if (ReintentosClave < 1)
                {
                    Alert.Show("Ingresó una clave incorrecta. Inténtelo nuevamente.");
                    Session["ReintentosClave"] = ReintentosClave + 1;
                    lblMensaje.Visible = false;
                }
                else
                {
                    Session["ReintentosClave"] = 0;
                    Desloguear();
                }
            }
            
            this.TextoFormateado.InnerText = value;
            if (value.Trim() == "")
            {
                if (Session["ReintentosClave"] != null)
                {
                    if (int.Parse(Session["ReintentosClave"].ToString()) < 1)
                    {
                        lblMensaje.Visible = true;
                        lblMensaje.Text = "<br/>No existen resultados para la b&uacute;squeda.";
                    }
                }
                hlkImprimir.Enabled = false;
                hlkImprimir.Visible = false;
                imgPrinter.Visible = false;
                hlkInstructivo.Enabled = false;
                hlkInstructivo.Visible = false;
                imgInstructivo.Visible = false;
            }
            else 
            {
                hlkImprimir.Enabled = true;
                string qstring = "?Legajo=" + int.Parse(hfLegajo.Value) + "&Mes=" + int.Parse(lstMeses.SelectedValue.ToString()) + "&Anio=" + int.Parse(lstAnios.SelectedValue.ToString());
                hlkImprimir.NavigateUrl = "../servicios/imprimirVisor4.aspx" + qstring;
                hlkImprimir.Target = "_new";
                hlkImprimir.Visible = true;
                imgPrinter.Visible = true;
                hlkInstructivo.Enabled = true;
                hlkInstructivo.Visible = true;
                hlkInstructivo.Target = "_new";
                imgInstructivo.Visible = true;

                lblMesLiquidado.Visible = true;
                lblUltimoMesLiquidado.Visible = true;
                lblUltimoMesLiquidado.Text = DarMes(int.Parse(Mes));
            }
        }

        private void Desloguear()
        {
            StringBuilder sbScript = new StringBuilder();

            sbScript.Append("alert('Ingresó una clave errónea. \\nSerá redireccionado a la pantalla de login.');");
            sbScript.Append("this.window.location='/Logout.aspx';");
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "_VolverLogin", sbScript.ToString(), true);
            //Response.Redirect("~/Logout.aspx", false);
        }


        private string DarMes(int nro_mes)
        {
            string Mes;
            switch (nro_mes)
            {
                case 1: Mes = "Enero";
                    break;
                case 2: Mes = "Febrero";
                    break;
                case 3: Mes = "Marzo";
                    break;
                case 4: Mes = "Abril";
                    break;
                case 5: Mes = "Mayo";
                    break;
                case 6: Mes = "Junio";
                    break;
                case 7: Mes = "Julio";
                    break;
                case 8: Mes = "Agosto";
                    break;
                case 9: Mes = "Setiembre";
                    break;
                case 10: Mes = "Octubre";
                    break;
                case 11: Mes = "Noviembre";
                    break;
                case 12: Mes = "Diciembre";
                    break;
                default: Mes = "No especificado";
                    break;
            }
            return Mes;
        }

        public static class Alert
        {

            /// <summary> 
            /// Shows a client-side JavaScript alert in the browser. 
            /// </summary> 
            /// <param name="message">The message to appear in the alert.</param> 
            public static void Show(string message)
            {
                // Cleans the message to allow single quotation marks 
                string cleanMessage = message.Replace("'", "\\'");
                string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

                // Gets the executing web page 
                Page page = HttpContext.Current.CurrentHandler as Page;

                // Checks if the handler is a Page and that the script isn't allready on the Page 
                if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
                {
                    page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
                }
            }
        }

    }
}