﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web
{
    /// <summary>
    /// Descripción breve de wsNewsletter
    /// </summary>
    /// 



    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
     [System.Web.Script.Services.ScriptService]
    public class wsNewsletter : System.Web.Services.WebService
    {

        [WebMethod]
        public string[] getDestinatarios(string prefixText, int count) 
        {

            ControllerNewsletter cn = new ControllerNewsletter();

            DSNewsLetter.news_getDestinatariosDataTable dt = cn.getDestinatarios(prefixText,"BUSCAR");

            string[] destinatarios = new string[dt.Rows.Count];
            int i = 0;
            foreach (DSNewsLetter.news_getDestinatariosRow dr in dt.Rows)
            {
                destinatarios.SetValue(dr.email.ToString().Trim(),i);
                i++;
            }


            return destinatarios;
        }



    }
}
