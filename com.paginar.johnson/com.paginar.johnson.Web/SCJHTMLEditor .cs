﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AjaxControlToolkit.HTMLEditor;
using System.Security.Permissions;
using System.ComponentModel;
using System.Web.UI;
namespace com.paginar.johnson.Web
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal),
   ToolboxData("<{0}:SCJHTMLEditor runat=\"server\" />")]
    public class SCJHTMLEditor : Editor
    {
        /// <summary>
        /// Método en el que se colocarán los elementos de la barra superior del control
        /// </summary>
        protected override void FillTopToolbar()
        {
            // Botón de negrita
            this.TopToolbar.Buttons.Add(
                new AjaxControlToolkit.HTMLEditor.ToolbarButton.Bold());
            // Botón de cursiva
            this.TopToolbar.Buttons.Add(
                new AjaxControlToolkit.HTMLEditor.ToolbarButton.Italic());
            // Botón de subrayado
            this.TopToolbar.Buttons.Add(
                new AjaxControlToolkit.HTMLEditor.ToolbarButton.Underline());
        }

        /// <summary>
        /// Método en el que se colocarán los elementos de la barra inferior del control
        /// </summary>
        protected override void FillBottomToolbar()
        {
            // Vista Diseño
            this.BottomToolbar.Buttons.Add(
                new AjaxControlToolkit.HTMLEditor.ToolbarButton.DesignMode());
            // Vista Previa
            this.BottomToolbar.Buttons.Add(
                new AjaxControlToolkit.HTMLEditor.ToolbarButton.PreviewMode());

            // base.FillBottomToolbar();
        }

    }
}