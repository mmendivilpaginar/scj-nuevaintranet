﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Collections.Specialized;
namespace com.paginar.johnson.Web
{
    /// <summary>
    /// Descripción breve de WsNoticias
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService()]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WsNoticias : System.Web.Services.WebService
    {

        //[System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]

        public AjaxControlToolkit.Slide[] GetSlides(string contextKey)
        {

            ControllerNoticias CN = new ControllerNoticias();
            int InfoID;
            if (contextKey != null)
                InfoID = int.Parse(contextKey);
            else
                InfoID = 0;

            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.Get_ImagenesByInfoID();
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[ImagenDT.Rows.Count];
            int i = 0;
           
            string url = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "");
            foreach (DSNoticias.infImagenRow imagen in ImagenDT.Rows)
            {
                Imagenes[i] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/noticias/Imagenes/" + imagen.Path + "&Size=110", "", imagen.Descrip);
                
                i++;
            }
            return Imagenes;
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public AjaxControlToolkit.Slide[] GetSlidesHome(string contextKey)
        {

            
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[5];
            string url = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "");
            if (contextKey == "1")
            {
                Imagenes[0] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/1.jpg" + "&Size=500", "", "");
                Imagenes[1] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/2.jpg" + "&Size=500", "", "");
                Imagenes[2] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/3.jpg" + "&Size=500", "", "");
                Imagenes[3] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/4.jpg" + "&Size=500", "", "");
            }
            else if (contextKey == "2")
            {
                Imagenes = new AjaxControlToolkit.Slide[3];
                Imagenes[0] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/01-b.jpg" + "&Size=500", "", "");
                Imagenes[1] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/02-b.jpg" + "&Size=500", "", "");
                Imagenes[2] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/03-b.jpg" + "&Size=500", "", "");


            }
            else if (contextKey == "0")
            {
                Imagenes[0] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/all/01.jpg" + "&Size=500", "", "");
                Imagenes[1] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/all/02.jpg" + "&Size=500", "", "");
                Imagenes[2] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/all/03.jpg" + "&Size=500", "", "");
                Imagenes[3] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/all/04.jpg" + "&Size=500", "", "");
               // Imagenes[4] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/home/all/05.jpg" + "&Size=500", "", "");
            }

            return Imagenes;
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public AjaxControlToolkit.Slide[] GetSlideVoluntariado()
        {
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[9];
            string url = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "");

                    Imagenes[0] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/1.jpg" + "&Size=500", "", "");
                    Imagenes[1] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/2.jpg" + "&Size=500", "", "");
                    Imagenes[2] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/3.jpg" + "&Size=500", "", "");
                    Imagenes[3] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/4.jpg" + "&Size=500", "", "");
                    Imagenes[4] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/5.jpg" + "&Size=500", "", "");
                    Imagenes[5] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/6.jpg" + "&Size=500", "", "");
                    Imagenes[6] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/7.jpg" + "&Size=500", "", "");
                    Imagenes[7] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/8.jpg" + "&Size=500", "", "");
                    Imagenes[8] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/images/voluntariado/9.jpg" + "&Size=500", "", "");

            return Imagenes;
        
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public AjaxControlToolkit.Slide[] GetSlidesDetalles(string contextKey)
        {

            ControllerNoticias CN = new ControllerNoticias();
            int InfoID;
            if (contextKey != null)
                InfoID = int.Parse(contextKey);
            else
                InfoID = 0;

            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.Get_ImagenesByInfoID();
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[ImagenDT.Rows.Count];
            int i = 0;
            //NameValueCollection variables = HttpContext.Current.Request.UrlReferrer.AbsoluteUri.ServerVariables; 
            //string protocol = (variables["SERVER _ PORT _ SECURE"] == "1") ? "https://" : "http://"; 
            //string domain = variables["SERVER _ NAME"]; 
            //string port = variables["SERVER _ PORT"];
            string url = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "");
            foreach (DSNoticias.infImagenRow imagen in ImagenDT.Rows)
            {
                //Imagenes[i] = new AjaxControlToolkit.Slide("../ResizeImage.aspx?Image=" + "~/noticias/Imagenes/" + imagen.Path + "&Size=132", "", "");
                Imagenes[i] = new AjaxControlToolkit.Slide(url + "/ResizeImage.aspx?Image=" + "~/noticias/Imagenes/" + imagen.Path + "&Size=170", "",imagen.Descrip);
                //Imagenes[i] = new AjaxControlToolkit.Slide(Server.MapPath(  "~/noticias/Imagenes/" + imagen.Path), "", "");
                i++;
            }
            return Imagenes;
        }
    }
}
