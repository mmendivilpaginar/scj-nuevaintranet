﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" ResponseEncoding="iso-8859-1"
    AutoEventWireup="true" CodeBehind="Buscador.aspx.cs" Inherits="com.paginar.johnson.Web.noticias.Buscador" %>

<%@ Register Src="~/UserControl_Home/ucNoticias.ascx" TagName="ucNoticias" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <h2>
        Buscador de noticias</h2>
    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Buscar" runat="server" />
    <asp:Panel ID="pnlBuscar" runat="server" DefaultButton="BtnBuscar">
        <div class="box formulario">
            <div class="form-item full">
                <label>
                    Buscar:</label>
                <asp:TextBox ID="PalabraTextBox" runat="server"></asp:TextBox>
            </div>
            <div class="form-item full">
                <label>
                    Categoría:</label>
                <asp:DropDownList ID="CategoriaDropDownList" runat="server" DataSourceID="ODSCategorias"
                    DataTextField="Descrip" DataValueField="CategoriaID" AppendDataBoundItems="true">
                    <asp:ListItem Value="" Text="Seleccione una categoría"></asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ODSCategorias" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter">
                </asp:ObjectDataSource>
            </div>
            <div class="form-item leftHalf">
                <label>
                    Desde:</label>
                <asp:TextBox ID="DesdeTextBox" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                    PopupButtonID="IMGCalen1" TargetControlID="DesdeTextBox" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="DesdeTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="DesdeTextBox" SetFocusOnError="True">*</asp:RangeValidator>

                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="DesdeTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>

            </div>
            <div class="form-item rightHalf">
                <label>
                    Hasta:</label>
                <asp:TextBox ID="HastaTextBox" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:ImageButton ID="IMGCalen2" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                    PopupButtonID="IMGCalen2" TargetControlID="HastaTextBox" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="HastaTextBox"
                    ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="HastaTextBox"
                    ControlToValidate="DesdeTextBox" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
                    Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="HastaTextBox" SetFocusOnError="True">*</asp:RangeValidator>
            </div>
            <div class="controls">
                <asp:Button ID="BtnBuscar" runat="server" Text="Filtrar" OnClick="BtnBuscar_Click"
                    ValidationGroup="Buscar" />
            </div>
        </div>
    </asp:Panel>
    <uc1:ucNoticias ID="ucNot" runat="server" displayHeader="false" />
</asp:Content>
