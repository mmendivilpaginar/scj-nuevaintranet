﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using AjaxControlToolkit;
using System.Text;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.noticias
{
    public partial class urlNoticia : System.Web.UI.Page
    {
        protected string privadoCod = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PanelNoticia.Visible = true;
                PanelError.Visible = false;
                if (Request["infoID"] != null)
                {

                    hdInfoID.Value = Request["infoID"].ToString();
                    CargarDatos(int.Parse(Request["infoID"]));

                }




            }


        }
        private ControllerNoticias cn;
        
        public void CargarDatos(int infoID)
        {
            cn = new ControllerNoticias();
            RNoticias.DataSource = cn.Repositorio.AdapterDSNoticias.infInfo.GetDataByID(infoID);
            RNoticias.DataBind();
            this.DataBind();
            /*
            ControllerUCHome CU = new ControllerUCHome();
            DsUCHome.UsuarioDataTable du = CU.getDataUsuario(this.ObjectUsuario.usuarioid);
            DsUCHome.UsuarioRow ru = (DsUCHome.UsuarioRow)du.Rows[0];
            imgUsuario.ImageUrl = "/images/personas/" + ru.UsuarioFoto;

            //votos
            lblUnLikeCount.Text = cn.getVotos(infoID, -1).ToString();
            lblLikeCount.Text = cn.getVotos(infoID, 1).ToString();
             * */
        }
        protected void RNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            HiddenField InfoIDHiddenField = e.Item.FindControl("InfoIDHiddenField") as HiddenField;
            HiddenField PrivacidadHidden = e.Item.FindControl("PrivacidadHidden") as HiddenField;
            int InfoID = int.Parse(InfoIDHiddenField.Value);

            if (Request["priv"] != null)
            {

                privadoCod = Request["priv"].ToString();

            }
            else
            {
                privadoCod = "00000";
            }

            if (PrivacidadHidden.Value == "1")
            {
                if (checkAcceso(InfoID, privadoCod))
                {
                    PanelNoticia.Visible = true;
                    PanelError.Visible = false;
                }
                else
                {
                    PanelNoticia.Visible = false;
                    PanelError.Visible = true;
                    lblMsgError.Text = "No tiene Autorizacion para ver esta Noticia";
                }
            }


            Image ImagenNoticia = e.Item.FindControl("ImagenNoticia") as Image;
            HiddenField RecordatorioHiddenField = e.Item.FindControl("RecordatorioHiddenField") as HiddenField;
            Label RecordatorioLiteral = e.Item.FindControl("RecordatorioLiteral") as Label;

            Control RecordatorioItem = e.Item.FindControl("RecordatorioItem") as Control;
            RecordatorioItem.Visible = Boolean.Parse(RecordatorioHiddenField.Value);
            ControllerNoticias CN = new ControllerNoticias();
            
            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.Get_ImagenesByInfoID();
            ///ResizeImage.aspx?Image=~/noticias/Imagenes/Superman634393323807211336.jpg&Size=170
            SlideShowExtender ImagenNoticia_SlideShowExtender = e.Item.FindControl("ImagenNoticia_SlideShowExtender") as SlideShowExtender;
            if (ImagenDT.Rows.Count == 0)
            {
                ImagenNoticia_SlideShowExtender.Enabled = false;
                ImagenNoticia.Visible = false;

            }
            else 
            {
                if (bool.Parse(ImagenDT.Rows[0]["flyer"].ToString()))
                {
                    //flyer                                               
                    HtmlControl divNoFlyer = e.Item.FindControl("divNoFlyer") as HtmlControl;
                    HtmlControl divFlyer = e.Item.FindControl("divFlyer") as HtmlControl;
                    Image imgFlyer = e.Item.FindControl("imgFlyer") as Image;
                    imgFlyer.ImageUrl = "~/noticias/imagenes/" + ImagenDT.Rows[0]["path"].ToString();
                    imgFlyer.Attributes.Add("onclick", "flyer('~/noticias/Imagenes/" + ImagenDT.Rows[0]["path"] + "');");
                    imgFlyer.Attributes.Add("style", "cursor: pointer");
                    divNoFlyer.Visible = false;
                    divFlyer.Visible = true;                    
                }
                else//imagen comun
                {
                    string Imagen_a_mostrar = string.Empty;
                    foreach (DSNoticias.infImagenRow ImgRow in ImagenDT.Rows)
                    {
                        Imagen_a_mostrar = ImgRow.Path.ToString();
                    }
                    ImagenNoticia.ImageUrl = ("/ResizeImage.aspx?Image=~/noticias/Imagenes/" + Imagen_a_mostrar + "&Size=170").ToString();

                    ImagenNoticia.Attributes.Add("onclick", "flyer('~/noticias/Imagenes/" + Imagen_a_mostrar + "');");
                    ImagenNoticia.Attributes.Add("style", "cursor: pointer");

                    ImagenNoticia_SlideShowExtender.Enabled = true;
                    ImagenNoticia.Visible = true;
                }
            }
            Repeater RepeaterArchivos = e.Item.FindControl("RepeaterArchivos") as Repeater;
            RepeaterArchivos.DataSource = CN.Get_ArchivosByInfoID();
            RepeaterArchivos.DataBind();

            ImagenNoticia_SlideShowExtender.ContextKey = InfoIDHiddenField.Value;
            


        }

        protected bool checkAcceso(int infoid, string CodPriv)
        { 
        string codComparar = CodValidacion(infoid);
        
        if (CodPriv == codComparar)
            {
                return true;
            }
        return false;
        }

        protected string CodValidacion(int infoid)
        {
            int codigo;
            string retorno = string.Empty;
            codigo = infoid % 15;
            switch (codigo)
            { 
                case 0:
                    retorno = "2A55S";
                    break;
                case 1:
                    retorno = "2EW85";
                    break;
                case 2:
                    retorno = "DKE55";
                    break;
                case 3:
                    retorno = "SAP2Z";
                    break;
                case 4:
                    retorno = "LS5S2";
                    break;
                case 5:
                    retorno = "6SDRS";
                    break;
                case 6:
                    retorno = "ASE3Q";
                    break;
                case 7:
                    retorno = "WY7T4";
                    break;
                case 8:
                    retorno = "8ZW8S";
                    break;
                case 9:
                    retorno = "8ERSA";
                    break;
                case 10:
                    retorno = "3B843";
                    break;
                case 11:
                    retorno = "YYW5S";
                    break;
                case 12:
                    retorno = "P8SD9";
                    break;
                case 13:
                    retorno = "W2S3E";
                    break;
                case 14:
                    retorno = "Q31RT";
                    break;

            }
            return retorno;
        }
        


    }
}