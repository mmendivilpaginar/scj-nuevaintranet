﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using AjaxControlToolkit;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.noticias
{
    public partial class noticia : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string script = "$(function () {	$('a.load-votantes').cluetip({local: true, hideLocal: true, sticky: true, arrows: true, cursor: 'pointer',width: 300, mouseOutClose: true  }); });";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "ELscripT32", script, true);
            if (!Page.IsPostBack)
            {
                HFInfoIDToVotos.Value = hdInfoID.Value;
                if (Request["infoID"] != null)
                {
                    hdInfoID.Value = Request["infoID"].ToString();
                    CargarDatos(int.Parse(Request["infoID"]));
                    
                }
            }
        }


        private ControllerNoticias cnr = new ControllerNoticias();
        private DSNoticias.infInfoDataTable _infInfoDt;
        public DSNoticias.infInfoDataTable infInfoDt
        {
            get { return _infInfoDt; }
            set { _infInfoDt = value; }
        }

        private ControllerNoticias cn;



        public void CargarDatos(int infoID)
        {
            cn = new ControllerNoticias();
            RNoticias.DataSource = cn.Repositorio.AdapterDSNoticias.infInfo.GetDataByID(infoID);
            RNoticias.DataBind();
            this.DataBind();

            ControllerUCHome CU = new ControllerUCHome();
            DsUCHome.UsuarioDataTable du = CU.getDataUsuario(this.ObjectUsuario.usuarioid);
            DsUCHome.UsuarioRow ru = (DsUCHome.UsuarioRow)du.Rows[0];
            imgUsuario.ImageUrl = "/images/personas/" + ru.UsuarioFoto;

            //votos
            //lblUnLikeCount.Text = cn.getVotos(infoID, -1).ToString();
            lblLikeCount.Text = cn.getVotos(infoID, 1).ToString();
        }

        
        protected void RNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            HiddenField InfoIDHiddenField = e.Item.FindControl("InfoIDHiddenField") as HiddenField;
            Repeater RepeaterRecordatorios = e.Item.FindControl("RepeaterRecordatorios") as Repeater;

            Image ImagenNoticia = e.Item.FindControl("ImagenNoticia") as Image;

            HtmlGenericControl DivImagen= e.Item.FindControl("DivImagen") as HtmlGenericControl;
            HiddenField RecordatorioHiddenField = e.Item.FindControl("RecordatorioHiddenField") as HiddenField;            
            Label RecordatorioLiteral = e.Item.FindControl("RecordatorioLiteral") as Label;
            Literal LiteralTexto = e.Item.FindControl("LiteralTexto") as Literal;

            string STR_RemoveHtmlAttributeRegex = @"(?<=<)([^/>]+)(\s{0}=['""][^'""]+?['""])([^/>]*)(?=/?>|\s)";

            Regex reg = new Regex(string.Format(STR_RemoveHtmlAttributeRegex, "style"),
                RegexOptions.IgnoreCase);

            LiteralTexto.Text = reg.Replace(LiteralTexto.Text, item => item.Groups[1].Value + item.Groups[3].Value);
            LiteralTexto.Text = Regex.Replace(LiteralTexto.Text, @"<div.*?>(.*?)</div>", "&#09;$1&nbsp;<br>");
           
           

            Control RecordatorioItem = e.Item.FindControl("RecordatorioItem") as Control;
            RecordatorioItem.Visible = Boolean.Parse(RecordatorioHiddenField.Value);
            ControllerNoticias CN = new ControllerNoticias();
            int InfoID = int.Parse(InfoIDHiddenField.Value);
            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.Get_ImagenesByInfoID();

            //Label FechaLiteral = (Label)e.Item.FindControl("LabelFechaAsoc");

            RecordatorioItem.Visible = Boolean.Parse(RecordatorioHiddenField.Value);

            //DateTime FechaPublicacion= DateTime.Parse(FechaLiteral.Text);
            if (Boolean.Parse(RecordatorioHiddenField.Value))
            {
                DSNoticias.infRecordatoriosDataTable dsinftab = new DSNoticias.infRecordatoriosDataTable();

                dsinftab = cnr.getRecordatoriosInfByID(int.Parse(InfoIDHiddenField.Value));
                RepeaterRecordatorios.DataSource = dsinftab;
                RepeaterRecordatorios.DataBind();

               //DateTime fechaRecordatorio = new DateTime();
                //bool flag = false;
                //foreach (DSNoticias.infRecordatoriosRow infR in dsinftab.Rows)
                //{
                   
                //    if ((DateTime.Compare(infR.FHRecordatorio, FechaPublicacion) == 1) && (DateTime.Compare(infR.FHRecordatorio,System.DateTime.Now) <= 0))
                //    { 
                //        fechaRecordatorio = infR.FHRecordatorio;
                //        flag = true;
                //   }
                  

                //}

                //if(flag)
                //   FechaLiteral.Text = fechaRecordatorio.ToShortDateString();
                
            }


            SlideShowExtender ImagenNoticia_SlideShowExtender = e.Item.FindControl("ImagenNoticia_SlideShowExtender") as SlideShowExtender;
            if (ImagenDT.Rows.Count == 0)
            {
                DivImagen.Visible = false;
                ImagenNoticia_SlideShowExtender.Enabled = false;
                ImagenNoticia.Visible = false;

            }
            else
            {
                if (bool.Parse(ImagenDT.Rows[0]["flyer"].ToString()))
                {
                    //flyer                                               
                    HtmlControl divNoFlyer = e.Item.FindControl("divNoFlyer") as HtmlControl;
                    HtmlControl divFlyer = e.Item.FindControl("divFlyer") as HtmlControl;
                    Image imgFlyer = e.Item.FindControl("imgFlyer") as Image;
                    imgFlyer.ImageUrl = "~/noticias/imagenes/" + ImagenDT.Rows[0]["path"].ToString();

                    string url = ImagenDT.Rows[0]["Descrip"].ToString();
                     HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImgflyer"));
                     if (url != "")
                     {
                         if (!url.Contains("http://") && (!url.Contains("https://")))
                             url = "http://" + url;
                         lnkImg.HRef = url;
                     }
                     else
                     {
                         lnkImg.Attributes.Remove("href");
                         imgFlyer.Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + ImagenDT.Rows[0]["path"] + "'," + ImagenDT.Rows[0]["InfoID"].ToString() + ");");
                         imgFlyer.Attributes.Add("style", "cursor: pointer");
                     }
                    divNoFlyer.Visible = false;
                    divFlyer.Visible = true;
                    DivImagen.Visible = false;
                }
                else//imagen comun o video
                {
                    string rutaimg = " ";
                    string rutavid = " ";
                    string url = ""; // si tiene para linkear
                    bool video = false;
                    bool imagen = false;

                    foreach (DAL.DSNoticias.infImagenRow imgs in ImagenDT.Rows)
                    {
                        string ruta = imgs.Path.ToString();
                        url = imgs.Descrip;

                        if (ruta.Contains(".mp4")) { video = true; rutavid = ruta; }
                        if (ruta.Contains(".jpg") || ruta.Contains("png")) { imagen = true; rutaimg = ruta; }
                    }

                 
                     //es solo Imagen 
                    if (imagen && !video)
                    {
                        url = setImagen(e, rutaimg, url, ImagenDT.Rows[0]["InfoId"].ToString());
                    }


                    if (video && !imagen)
                    {
                        ImagenNoticia.Visible = false;
                        setVideo(e, rutavid);
                    }

                     //ambos
                    if (video && imagen)
                    {
                        url = setImagen(e, rutaimg, url, ImagenDT.Rows[0]["InfoId"].ToString());
                        setVideo(e, rutavid);
                    }

                }
            }



            Repeater RepeaterArchivos = e.Item.FindControl("RepeaterArchivos") as Repeater;
            RepeaterArchivos.DataSource = CN.Get_ArchivosByInfoID();
            RepeaterArchivos.DataBind();

            ImagenNoticia_SlideShowExtender.ContextKey = InfoIDHiddenField.Value;

            //
            DAL.DSNoticias.infLinkDataTable linkDT = new DAL.DSNoticias.infLinkDataTable();
            DataList RepeaterLink = e.Item.FindControl("RTLinks") as DataList;
            linkDT = cn.GetLinkByInfoID(InfoID);
            RepeaterLink.DataSource = linkDT;
            RepeaterLink.DataBind();
            //
          
        }

        private void setVideo(DataListItemEventArgs e, string ruta)
        {
            string site = GetUrlSite().Replace("/", "%2F").Replace(":", "%3A");
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;
            ltvideo.Text = " <embed src='../noticias/imagenes/gddflvplayer.swf' flashvars='?&autoplay=false&sound=70&buffer=2&vdo=" + site + "%2Fnoticias%2Fimagenes%2F" + ruta.Replace(".", "%2E") + "' width='420' height='315' allowFullScreen='false' quality='best' wmode='transparent' allowScriptAccess='always'  pluginspage='http://www.macromedia.com/go/getflashplayer'  type='application/x-shockwave-flash'></embed>";

        }

        private static string setImagen(DataListItemEventArgs e, string ruta, string url,string InfoId)
        {
            int cantidad = ruta.Length;
            string extencion = ruta.Substring(cantidad - 3, 3);
            HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImg"));

            if (ruta != " " && (extencion == "jpg" || extencion == "png") && (url != ""))
            {
                if (!url.Contains("http://") && (!url.Contains("https://")))
                    url = "http://" + url;
                lnkImg.HRef = url;
            }
            else
            {
                lnkImg.Attributes.Remove("href");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + ruta + "'," + InfoId + ");");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("style", "cursor: pointer");
            }
            return url;
        }

        protected string GetUrlSite()
        {
            string url = this.ASP_SITE.ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return this.ASP_SITE.ToString().Replace(":8080", "");//produccion


        }

        protected void RTLinks_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;


            HyperLink link = e.Item.FindControl("HyperLink1") as HyperLink;
           //solo mostrara videos no links

            if (link.NavigateUrl.Contains("www.youtube.com") || link.NavigateUrl.Contains("http://youtu.be"))
            {
                string url;
                link.Visible = false;
                url = link.NavigateUrl.Replace("http://youtu.be/", "https://www.youtube.com/v/");

                url = url.Replace("watch?v=", "v/");
                ltvideo.Text = " <embed  width='420' height='315' src='" + url + "' > ";
            }



            if (link.NavigateUrl.Contains("vimeo.com"))
            {
                link.Visible = false;
                string url = link.NavigateUrl.Replace("https://vimeo.com/", "");
                ltvideo.Text = "<iframe src='//player.vimeo.com/video/" + url + "' width='420' height='315' frameborder='0' ></iframe>";
            }

        }


        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static AjaxControlToolkit.Slide[] GetSlides(string contextKey)
        {

            ControllerNoticias CN = new ControllerNoticias();
            int InfoID = int.Parse(HttpContext.Current.Request.QueryString["InfoID"]);
            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.GetImagenByInfoID();
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[ImagenDT.Rows.Count];
            int i = 0;
            foreach (DSNoticias.infImagenRow imagen in ImagenDT.Rows)
            {
                Imagenes[i] = new AjaxControlToolkit.Slide(imagen.Path, "", "");
                i++;

            }
            return Imagenes;
        }

        protected void btnComentar_Click(object sender, EventArgs e)
        {

            ControllerNoticias CN = new ControllerNoticias();
            string userText = txtComentario.Text.ToString();
            userText = userText.Replace("\r\n", "<br/>").Replace("\r", "<br/>");
            CN.InsertUSuarioComentario(this.ObjectUsuario.usuarioid, int.Parse(hdInfoID.Value), userText);
            txtComentario.Text = "";
            dtComments.DataBind();

           
        }

        protected void imgUnLike_Click(object sender, ImageClickEventArgs e)
        {
            ControllerNoticias CN = new ControllerNoticias();
            int InfoID = int.Parse(hdInfoID.Value);
            if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID))
            {
                CN.InsertInfoUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID, -1);
                //lblUnLikeCount.Text = CN.getVotos(InfoID, -1).ToString();
                EjecutarScript("alert('Gracias por Votar');");
            }
            else
                EjecutarScript("alert('Esta Noticia ya cuenta con su voto');");
        }

        protected void imgLike_Click(object sender, ImageClickEventArgs e)
        {
            ControllerNoticias CN = new ControllerNoticias();
            int InfoID = int.Parse(hdInfoID.Value);
            if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID))
            {
                CN.InsertInfoUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID, 1);
                lblLikeCount.Text = CN.getVotos(InfoID, 1).ToString();                
                EjecutarScript("alert('Gracias por Votar');");
            }
            else
                EjecutarScript("alert('Esta Noticia ya cuenta con su voto');");
               
        }




        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;
        }


    }


}