﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.noticias
{
    public partial class ucCategoriaNoticia : System.Web.UI.UserControl
    {

        public int Categoriaid { get; set; }
        public int? TagID { get; set; }
        public bool displayHeader = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtFHDesde.Text = txtFHDesde.Text.Trim();
            txtFHHasta.Text = txtFHHasta.Text.Trim();
            if (!Page.IsPostBack)
            {
                int clusterid = int.Parse(((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString());
                ControllerNoticias CN = new ControllerNoticias();
                ucNoticias1.infInfoDt = CN.GetBusquedaFE(null, this.Categoriaid, null, null,null,clusterid,TagID);
                ucNoticias1.displayHeader = displayHeader;
                ucNoticias1.CargarDatos();
            }
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            int clusterid = int.Parse(((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString());
         
            DateTime? fhdesde= null;
            DateTime? fhhasta = null;
            
            if (txtFHDesde.Text != "") fhdesde = DateTime.Parse(txtFHDesde.Text);
            if (txtFHHasta.Text != "") fhhasta = DateTime.Parse(txtFHHasta.Text);

             ControllerNoticias CN = new ControllerNoticias(); 
             ucNoticias1.infInfoDt = CN.GetBusquedaFE(PalabraTextBox.Text.Replace("'","%"), this.Categoriaid, fhdesde, fhhasta,null,clusterid,null);
             ucNoticias1.CargarDatos();

        }

    }
}