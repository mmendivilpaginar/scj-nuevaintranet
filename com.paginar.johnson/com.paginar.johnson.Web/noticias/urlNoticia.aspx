﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="urlNoticia.aspx.cs" Inherits="com.paginar.johnson.Web.noticias.urlNoticia" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>Johnson Intranet - Noticias</title>
    <link href="../js/jQueryUI/themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../js/jquery.corner.js" type="text/javascript"></script>
    <script src="../js/jQueryUI/jquery-ui-1.8.5.min.js" type="text/javascript"></script>
    <script src="../js/jquery.cluetip.min.js" type="text/javascript"></script>
    <script src="../js/init.js" type="text/javascript"></script>
    <script src="../js/jquery.tooltip.js" type="text/javascript"></script>
    <script src="../js/jquery.rotate.1-1.js" type="text/javascript"></script>
    <script src="../js/clock.js" type="text/javascript"></script>
    <script src="../js/jquery.shorten.min.js" type="text/javascript"></script>
    <link href="../css/clock.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">


        $(document).ready(function () {
            $('#BusquedaTextBox').keypress(function (e) {
                if (e.which == 13) {
                    Buscar();
                    return false;
                }
            });

            setTimeout(checkexpira, 3600000);

        });
        function closerW() {

            window.location = "/Login.aspx?Forced=TRUE";
        }
        function checkexpira() {
            var input_box;
            input_box = confirm("La sesión va a expirar si no refresca la pantalla\r\n Aceptar refrescará la pantalla y Cancelar la cerrará, regresando al loguin.");
            if (input_box == true) {


                location.reload();
            }

            else {


                window.location = "/Logout.aspx";

            }
        }

        $(function () {
            $('#BusquedaButton').click(Buscar);
        })


        $(function () {
            $('#BusquedaTextBox').blur(Ocultar);
        })



        function Ocultar() {
            $("#BuscadorContent").fadeOut();
        }



        function Buscar() {
            var input = $('#BusquedaTextBox').val();
            var numberOfWords = input.length;



            if ((numberOfWords >= 3) && (input != "Buscar...")) {

                $("#BuscadorContent").fadeIn();

                $.ajax({
                    type: "POST",
                    url: "../wsBuscador.asmx/Buscador",
                    data: "{'busqueda':'" + $('#BusquedaTextBox').val().replace(new RegExp("'", "g"), "%") + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        ServiceOK(msg);
                    }
                        , error: ServiceERROR
                });

                return false;
            }
            else
                return false;

        }




        function ServiceOK(result) {
            var html = '';
            var resultados = (typeof result.d) == 'string' ? eval('(' + result.d + ')') : result.d;
            $('#BuscadorContent').empty();

            for (var i = 0; i < resultados.length; i++) {
                html += resultados[i].row;
            }


            $('#BuscadorContent').append(html);

        }



        function ServiceERROR(result) {
            alert("ERROR " + result.status + ' ' + result.statusText);
        }


        function flyer(path) {
            if (window.screen) {
                w = window.screen.availWidth;
                h = window.screen.availHeight;
            }
            winpopup = window.open("../Flyer.aspx?path=" + path, "flyer", "'menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + w + ",height=" + h + "'");

        }
    </script>
</head>
<body id="NewsletterPage">

<form id="form1" runat="server">
    <div id="wrapper">
        <div id="wrapper-inner">
            
            <!--<div id="header-inner">
                <div id="logo">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/logo.png" AlternateText="Johnson" PostBackUrl="~/Default.aspx" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                </div>
                <h1>SCJ <span>Cluster Cono Sur</span></h1>
            </div>-->

            <div id="header">
                <img src="../images/headernl.jpg" />
                <img src="../images/header_bajonl.jpg" />
            </div>
            
            <div id="main">
                <div id="main-inner">
                    <div id="content">
                        <div id="content-inner">
                            <asp:Panel ID="PanelNoticia" runat="server">
                                <div class="noticia">
                                    <h2>Noticias</h2>
                                    <asp:HiddenField ID="hdInfoID" runat="server" />
                                    <asp:DataList ID="RNoticias" runat="server" OnItemDataBound="RNoticias_ItemDataBound">
                                        <ItemTemplate>
                                            <ul class="tags">
                                                <li><span>
                                                    <%# Eval("CategoriaDescrip")%></span></li>
                                                <li runat="server" id="RecordatorioItem" class="recordatorio">
                                                    <asp:Label ID="RecordatorioLiteral" Text="Recordatorio" runat="server"></asp:Label></li>
                                            </ul>
                                            <h3>
                                                <asp:HiddenField ID="PrivacidadHidden" runat="server" Value='<%# Eval("Privacidad") %>' />
                                                <asp:HiddenField ID="RecordatorioHiddenField" runat="server" Value='<%# Eval("Recordatorio") %>' />
                                                <%# Eval("Titulo")%>
                                            </h3>
                                            <div class="nota news">
                                             <div runat="server" id="divNoFlyer">  
                                                <asp:Image ID="ImagenNoticia" runat="server" />
                                                <%--<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString() +" "+ Eval("Copete")%>--%>
                                                <i>
                                                    <asp:Label Font-Italic="true" runat="server" ID="LabelFechaAsoc" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                                                </i>-
                                                <asp:Label Font-Italic="true" runat="server" ID="LiteralCopete" Text='<%#Eval("Copete")%>'></asp:Label>
                                                <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                                                <%# Eval("texto").ToString()%>
                                                <ul class="attachnew">
                                                    <asp:Repeater ID="RepeaterArchivos" runat="server">
                                                        <ItemTemplate>
                                                            <li>
                                                                <%--<img src="../css/images/attach.png" title="Ver Adjunto" border="0" />--%>
                                                                <span onclick='<%# "flyer('~/noticias/Imagenes/"+Eval("Path") %>'>'
                                                                <asp:HyperLink ID="HyperLinkArchivoAdjunto" Text='<%# Eval("Descrip") %>' runat="server"
                                                                    NavigateUrl='<%# "~/noticias/Imagenes/"+Eval("Path") %>' Target="_blank" Visible='<%# Eval("Path").ToString()!="" %>'>  ss</asp:HyperLink>
                                                                </span>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                                </div>
                                                <div runat="server" id="divFlyer" visible="false">
                                                  <asp:Image ID="imgFlyer" runat="server" Width="450px" Height="450px"/>
                                                 <br />
                                                </div>
                                            </div>


                                            <%# Eval("Fuente")%>
                                            <asp:HiddenField ID="InfoIDHiddenField" runat="server" Value='<%# Eval("InfoID")%>' />
                                            <asp:SlideShowExtender SlideShowServicePath="~/WsNoticias.asmx" ID="ImagenNoticia_SlideShowExtender"
                                                runat="server" SlideShowServiceMethod="GetSlidesDetalles" TargetControlID="ImagenNoticia"
                                                UseContextKey="True" NextButtonID="" PlayButtonID="" PlayButtonText="play" StopButtonText="stop"
                                                PreviousButtonID="" AutoPlay="true" Loop="True" PlayInterval="3000">
                                            </asp:SlideShowExtender>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    <asp:Repeater ID="rpLinks" runat="server" DataSourceID="odsLinks">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="HyperLink1" NavigateUrl='<%# Eval("url")%>' Text='<%# Eval("descrip")%>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:ObjectDataSource ID="odsLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetLinkByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdInfoID" Name="InfoID" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                            </asp:Panel>
                            
                            <asp:Panel ID="PanelError" runat="server">
                    <asp:Label ID="lblMsgError" runat="server"></asp:Label>
                </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>

            <div id="footer">
                <img src="../images/footernlConEnjoy.jpg" />
            </div>

        </div><!--/wrapper-inner-->
    </div><!--/wrapper-->

</form>
</body>
</html>
