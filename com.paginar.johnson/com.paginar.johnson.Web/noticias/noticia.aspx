﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="noticia.aspx.cs" Inherits="com.paginar.johnson.Web.noticias.noticia" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <script type="text/javascript">
        $(function () {

            $('a.linkToolTip').cluetip({
                local: true,
                hideLocal: true,
                sticky: false,
                cursor: 'pointer',
                showTitle: false,
                width: '100px'

            });

            $('a.votantes-link').cluetip({
                local: true,
                hideLocal: true,
                sticky: true,
                arrows: true,
                cursor: 'pointer',
                width: 300,
                mouseOutClose: true

            });
                        
        });
  </script>

    
    <asp:HiddenField ID="hdInfoID" runat="server" />
    <!--BUSCADOR-->
    <div class="box">
    </div>
    <h2>
        Noticias</h2>
    <!--NOTICIA-->
    <div class="noticia">
        <asp:DataList ID="RNoticias" runat="server" OnItemDataBound="RNoticias_ItemDataBound">
            <ItemTemplate>
                <ul class="tags">
                    <li><span>
                        <%# Eval("CategoriaDescrip")%></span></li>
                    <li runat="server" id="RecordatorioItem" class="recordatorio">
                        <span>
                        <asp:Label ID="RecordatorioLiteral" Text="Recordatorio" runat="server" Visible="false"></asp:Label>
                         <a class="linkToolTip" href="#Recordatorios" rel="#Recordatorios">Recordatorio</a>
                         </span>
                        </li>
                </ul>
                 <div id="Recordatorios">
                     <asp:Repeater ID="RepeaterRecordatorios" runat="server">
                     <HeaderTemplate>
                      <table style=" border: 1px;">
                     </HeaderTemplate>
                     <ItemTemplate>
                      <tr><td><asp:Label ID="Label1" runat="server" Text='<%# Bind("FHRecordatorio","{0:dd/MM/yyyy}") %>'></asp:Label></td></tr>
                     </ItemTemplate>
                     <FooterTemplate>
                     </table>
                     </FooterTemplate>
                     </asp:Repeater>
                       
                 </div>
                <h3>
                    <asp:HiddenField ID="RecordatorioHiddenField" runat="server" Value='<%# Eval("Recordatorio") %>' />
                    <%# Eval("Titulo")%>
                </h3>
                <div class="nota">
                <div class="ImgDes" runat="server" id="DivImagen">
                 <a href="#"  runat="server" ID="lnkImg"  target="_blank"> 
                   <asp:Image ID="ImagenNoticia" runat="server"  CssClass="ImgDes1"/>
                 </a>
                    <br />
                  <b><asp:Label ID="ImagenNoticiaDescripcion" runat="server" Text=""></asp:Label></b>
                </div>
                   <div runat="server" id="divNoFlyer">  
                    <%--<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString() +" "+ Eval("Copete")%>--%>
                    <i> <label>Fecha Publicación:</label>
                        <asp:Label Font-Italic="true" runat="server" ID="LabelFechaAsoc" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                    </i><br />
                    <asp:Label Font-Italic="true" runat="server" ID="LiteralCopete" Text='<%#Eval("Copete")%>'></asp:Label>
                    <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                    <asp:Literal ID="LiteralTexto" runat="server" Text='<%# Eval("texto").ToString()%>'></asp:Literal>                    
                    <ul class="attachnew">
                        <asp:Repeater ID="RepeaterArchivos" runat="server">
                            <ItemTemplate>
                                <li>
                                    <%--<img src="../css/images/attach.png" title="Ver Adjunto" border="0" />--%>
                                    <asp:HyperLink ID="HyperLinkArchivoAdjunto" Text='<%# Eval("Descrip") %>' runat="server"
                                        NavigateUrl='<%# "~/noticias/Imagenes/"+Eval("Path") %>' Target="_blank" Visible='<%# Eval("Path").ToString()!="" %>'>  ss</asp:HyperLink>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                   </div>
                   <div runat="server" id="divFlyer" visible="false">
                   <i> <label>Fecha Publicación:</label>
                        <asp:Label Font-Italic="true" runat="server" ID="Label2" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                    </i><br /> <br />
                          <asp:Label Font-Italic="true" runat="server" ID="Label3" Text='<%#Eval("Copete")%>'></asp:Label>
                    <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                        <br />
                        <a href="#"  runat="server" ID="lnkImgflyer"  target="_blank"> 
                          
                             <asp:Image ID="imgFlyer" runat="server" Width="450px" />
                              
                        </a>
                        <br />
                   </div>
                    <div align="center">
                        <asp:Literal ID="ltvideo" runat="server"></asp:Literal>
                    </div>
                </div>
                <%# Eval("Fuente")%>
                <asp:HiddenField ID="InfoIDHiddenField" runat="server" Value='<%# Eval("InfoID")%>' />
                <asp:SlideShowExtender SlideShowServicePath="~/WsNoticias.asmx" ID="ImagenNoticia_SlideShowExtender"
                    runat="server" SlideShowServiceMethod="GetSlidesDetalles" TargetControlID="ImagenNoticia"
                    UseContextKey="True" NextButtonID="" PlayButtonID="" PlayButtonText="play" StopButtonText="stop"
                    PreviousButtonID="" AutoPlay="true" ImageDescriptionLabelID="ImagenNoticiaDescripcion" Loop="True" PlayInterval="3000">
                </asp:SlideShowExtender>

                <br /><br />
                                       <div  id="divlinks" style="margin-left:20px"

                                           <asp:DataList ID="RTLinks" runat="server" OnItemDataBound="RTLinks_ItemDataBound">
                                               <ItemTemplate>
                                                    <br />
                                                   <asp:Literal ID="ltvideo" runat="server"></asp:Literal>
                                                   <asp:HyperLink Target="_blank" ID="HyperLink1" NavigateUrl='<%# Eval("url")%>' Text='<%# Eval("descrip")%>' runat="server" />

                                               </ItemTemplate>
                                           </asp:DataList>
                                       </div>
                                     

            </ItemTemplate>
        </asp:DataList>
        <br />
<%--        <asp:Repeater ID="rpLinks" runat="server" DataSourceID="odsLinks"   >
            <ItemTemplate>
                <asp:HyperLink Target="_blank" ID="HyperLink1" NavigateUrl='<%# Eval("url")%>' Text='<%# Eval("descrip")%>'
                    runat="server" />
            </ItemTemplate>
        </asp:Repeater>
        <asp:ObjectDataSource ID="odsLinks" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetLinkByInfoID" TypeName="com.paginar.johnson.BL.ControllerNoticias">
            <SelectParameters>
                <asp:ControlParameter ControlID="hdInfoID" Name="InfoID" PropertyName="Value" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>--%>
        <br />
        <div style="border-bottom: 1px solid #D9D9D9; width:100%; margin-bottom:10px;padding-bottom:10px;" ></div>
        <ul class="action-list">
            <%--<li>
                <asp:UpdatePanel ID="updUnLike" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblUnLikeCount" runat="server"></asp:Label>
                        <asp:ImageButton ID="imgUnLike" runat="server" ImageUrl="~/css/images/ico-unlike.gif"
                            CommandName="unlike" OnClick="imgUnLike_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </li>--%>
            <li>
                <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="votantes"  class="userCard">

                                <asp:HiddenField ID="HFInfoIDToVotos" runat="server" 
                                    Value="" />
                                <asp:GridView ID="GVVotantes" runat="server" AutoGenerateColumns="False" 
                                    DataKeyNames="UsuarioID" DataSourceID="ODSUsuariosVotantesInfo" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblvotHeader" runat="server" Text="Esta Noticia fue votada por:" CssClass="izquierda"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/images/personas/"+Eval("usuariofoto")%>' />                               
                                                </div>
                                                <div class="informacion">
                                                <b><%# Eval("Apellido") + " " + Eval("Nombre") %></b>
                                                <br />
                                                <%# Eval("ubicaciondescripcion")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmpty" runat="server" Text="Esta Noticia aún no fue votada"></asp:Label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:ObjectDataSource ID="ODSUsuariosVotantesInfo" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="getUsuariosVotantesByInfoID" 
                                TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hdInfoID" Name="infoID" 
                                            PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                            </asp:ObjectDataSource>
<%--                                <table border="1">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("InfoIDAUX")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>--%>
                            </div> 
                           <a class="votantes-link" href="#votantes" rel="#votantes"><asp:Label ID="lblLikeCount" runat="server"></asp:Label></a>
                        <%--<asp:Label ID="lblLikeCountB" runat="server"></asp:Label>--%>
                        <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif"
                            CommandName="like" OnClick="imgLike_Click" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </li>
        </ul>
        <asp:UpdateProgress ID="upgNoticiaslike" runat="server" AssociatedUpdatePanelID="updLike">
            <ProgressTemplate>
                <img src="../images/preloader.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
<%--        <asp:UpdateProgress ID="upgNoticiaslUnlike" runat="server" AssociatedUpdatePanelID="updUnLike">
            <ProgressTemplate>
                <img src="../images/preloader.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
    </div>
    <!--COMENTARIOS-->
    <div class="box comentarios" >
        <asp:UpdatePanel ID="updComments" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <a name="comentarios-abajo"></a>
                <div class="usuario-comentar" style="display:none">
                    <asp:Image ID="imgUsuario" runat="server" Height="50px" Width="50px" CssClass="hide" />
                    <div class="form-item">
                        <label>
                            Agregar comentario:</label>
                        <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComentario"
                            ValidationGroup="Comentar"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controls">
                        <asp:Button ID="btnComentar" runat="server" Text="Comentar" OnClick="btnComentar_Click"
                            ValidationGroup="Comentar" />
                    </div>
                </div>
                <div class="lista-comentarios">
                    <asp:DataList ID="dtComments" runat="server" DataKeyField="comentarioID" DataSourceID="odsComments"
                        RepeatLayout="Flow">
                        <ItemTemplate>
                            <div class="comentario">
                                <div class="pic">
                                    <asp:Image ID="Image2" runat="server" ToolTip='<%# Eval("nombrecompleto") %>' ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif"  %>' />
                                </div>
                                <div class="content">
                                    <p>
                                        <asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                    <p class="fecha">
                                        Fecha:
                                        <asp:Label ID="FechaLabel" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy - hh:MM tt}") %>' /></p>
                                    <p>
                                        Comentario:
                                        <asp:Label ID="comentariosLabel" runat="server" Text='<%# Eval("comentarios") %>' /></p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <asp:ObjectDataSource ID="odsComments" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getComments" TypeName="com.paginar.johnson.BL.ControllerNoticias">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdInfoID" Name="infoid" PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upgComments" runat="server" AssociatedUpdatePanelID="updComments">
            <ProgressTemplate>
                <img src="../images/preloader.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
