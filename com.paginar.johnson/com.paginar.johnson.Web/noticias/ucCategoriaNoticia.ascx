﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCategoriaNoticia.ascx.cs"
    Inherits="com.paginar.johnson.Web.noticias.ucCategoriaNoticia" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControl_Home/ucNoticias.ascx" TagName="ucNoticias" TagPrefix="uc1" %>
<script type="text/javascript">

    $(function () {
        $('#imgBuscar').click(Toogle);
    })


    function Toogle() {
        $("#divBuscar").toggle("slow");

    }

</script>
<p class="alignRight">
    <img id="imgBuscar" alt="Buscar" src="../css/images/ico-buscar.png" style="cursor: pointer;"
        title="Desplegar/Ocultar Buscador" /></p>
<asp:Panel runat="server" ID="pnlBuscar" DefaultButton="BtnBuscar">
    <div class="box formulario" id="divBuscar" style="display: none">
    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Buscar" runat="server" />
        <div class="form-item full">
            <label>
                Buscar:</label>
            <asp:TextBox ID="PalabraTextBox" runat="server"></asp:TextBox>
            <asp:ObjectDataSource ID="ODSCategorias" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetData" TypeName="com.paginar.johnson.DAL.DSNoticiasTableAdapters.infCategoriaTableAdapter">
            </asp:ObjectDataSource>
        </div>
        <div class="form-item leftHalf">
            <label>
                Desde:</label>
            <asp:TextBox ID="txtFHDesde" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="Fecha Desde fuera de rango, coloque otra."  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="txtFHDesde" SetFocusOnError="True">*</asp:RangeValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFHDesde"
                ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
            <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="DesdeTextBox_CalendarExtender" runat="server" Enabled="True"
                PopupButtonID="IMGCalen1" TargetControlID="txtFHDesde" Format="dd/MM/yyyy">
            </asp:CalendarExtender>

        </div>
        <div class="form-item rightHalf">
            <label>
                Hasta:</label>
            <asp:TextBox ID="txtFHHasta" runat="server" SkinID="form-date"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="Fecha Hasta fuera de rango, coloque otra."  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="txtFHHasta" SetFocusOnError="True">*</asp:RangeValidator>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFHHasta"
                ErrorMessage="El formato de la fecha ingresada en Hasta es incorrecto. El formato de fecha es dd/mm/yyyy."
                Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="txtFHHasta"
            ControlToValidate="txtFHDesde" ErrorMessage="La fecha ingresada en el campo Desde debe ser menor a la ingresada en el campo Hasta."
            Operator="LessThanEqual" Type="Date" ValidationGroup="Buscar" Display="Dynamic">*</asp:CompareValidator>
            <asp:ImageButton ID="IMGCalen2" runat="server" ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="HastaTextBox_CalendarExtender" runat="server" Enabled="True"
                PopupButtonID="IMGCalen2" TargetControlID="txtFHHasta" Format="dd/MM/yyyy">
            </asp:CalendarExtender>

        </div>
        <div class="controls">
            <asp:Button ID="BtnBuscar" runat="server" Text="Filtrar" OnClick="BtnBuscar_Click"
                ValidationGroup="Buscar" />
        </div>
    </div>
</asp:Panel>
<uc1:ucNoticias ID="ucNoticias1" runat="server"  />
