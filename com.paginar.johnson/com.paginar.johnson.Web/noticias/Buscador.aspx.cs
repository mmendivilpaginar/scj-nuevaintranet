﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Text;

namespace com.paginar.johnson.Web.noticias
{
    public partial class Buscador : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {

                int clusterid = this.ObjectUsuario.clusteridactual;
                string BuscarTexto = (Request["busqueda"] != null) ? Request["busqueda"].ToString() : null;
                ControllerNoticias CN = new ControllerNoticias();
                ucNot.CantFilas = 20;
                ucNot.infInfoDt = CN.GetBusquedaFE(BuscarTexto, null, null, null,20,clusterid,null);
                ucNot.CargarDatos();                
            }
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            int clusterid = this.ObjectUsuario.clusteridactual;
            ucNot.CantFilas = 10;
            ControllerNoticias CN = new ControllerNoticias();
            int? CategoriaID = (CategoriaDropDownList.SelectedIndex == 0) ? null : (int.Parse(CategoriaDropDownList.SelectedValue) as int?);
            ucNot.infInfoDt = CN.GetBusquedaFE((PalabraTextBox.Text == string.Empty) ? null : PalabraTextBox.Text.Replace("'","%") as string, CategoriaID, (DesdeTextBox.Text.Trim() == string.Empty) ? null : DateTime.Parse(DesdeTextBox.Text) as DateTime?, (HastaTextBox.Text.Trim() == string.Empty) ? null : DateTime.Parse(HastaTextBox.Text) as DateTime?,null,clusterid,null);
            ucNot.CargarDatos();

        }
    }
}