﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;

namespace com.paginar.johnson.Web
{
    public partial class ASPForm : PageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            string[] urlPrimaria = Request.QueryString["url"].ToString().Split('?');
            switch (urlPrimaria[0])
            {
                case "servicios/SolicitudIndumentaria.aspx":
                    string tramiteid;
                    if (Request.QueryString[0].Split('&').Length > 2)
                    {
                        tramiteid = Request.QueryString[0].Split('&')[2].Split('=')[1];
                        Response.Redirect("~/" + urlPrimaria[0] + "?TramiteID=" + tramiteid);
                    }
                    else
                        Response.Redirect("~/" + urlPrimaria[0]);
                    break;
            }

            string url = "SessionTransfer.aspx?dir=2asp&url=" + Server.UrlEncode(Request.QueryString["url"]);            
            IAsp.Attributes.Add("src", url);
            
		}
	}
}