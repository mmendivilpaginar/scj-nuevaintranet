﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="com.paginar.johnson.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Intranet Johnson</title>
<link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
<link rel="icon" href="/favicon.ico" type="image/ico"/>
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../js/jquery.corner.js" type="text/javascript"></script>
    <script src="../js/jQueryUI/jquery-ui-1.8.5.min.js" type="text/javascript"></script>
    <script src="../js/jquery.cluetip.min.js" type="text/javascript"></script>
    <style type="text/css">
        
        #wrapper
        {
            width: 960px;
            margin: 0 auto;
        }
        .AspNet-Login
        {
            width: 300px;
            margin: 0 auto;
            font-family: Arial;
            font-size: 12px;
        }
        .AspNet-Login-TitlePanel, .AspNet-Login-RememberMePanel
        {
            display: none;
        }
        .AspNet-Login label
        {
            display: block;
        }
        .AspNet-Login label em
        {
            font-style: normal;
        }
        .AspNet-Login-UserPanel, .AspNet-Login-PasswordPanel
        {
            margin-bottom: 10px;
        }
        #Login2_UserName, #Login2_Password
        {
            width: 270px;
        }

    /* simple css-based tooltip */
    .tooltip {
	    background-color:#000;
	    border:1px solid #fff;
	    padding:10px 15px;
	    width:200px;
	    display:none;
	    color:#fff;
	    text-align:left;
	    font-size:12px;

	    /* outline radius for mozilla/firefox only */
	    -moz-box-shadow:0 0 10px #000;
	    -webkit-box-shadow:0 0 10px #000;
    }
    </style>



    <script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#Login2_UserName").attr("title", " Ejemplos:\n\r Usuario Operador: 1234 \n\r Usuario Administrativo: A071234")

    });
        function Abrir_ventana(pagina) {
            var opciones = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=508, height=365, top=85, left=140";
            window.open(pagina, "", opciones);
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
        <div id="wrapper-inner">
            <!--<div id="header">
                    <div id="header-inner">
                        <div id="logo">                   
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/logo.png"   AlternateText="Johnson"  PostBackUrl="~/Default.aspx"/> 
                         </div>
                        <h1>SCJ <span>Cluster Cono Sur</span></h1>                                                 
                    </div>
                </div>-->
            <div style="font-size: 23px; color: #2A3C6D; font-family: Arial; text-align: center;">
                SCJ Cluster Cono Sur</div>
            <br />
            <br />
            <div id="main">
                <div id="main-inner">
                    <div id="content">
                        <div id="content-inner">
                            <div style="font-size: 12px; color: #2A3C6D; font-family: Arial; text-align: center;">
                                Ingresar el Número de Legajo (solo Operarios) o ID (solo Administrativos) y la clave personal que ya utilizaban en la intranet.<br /> 
                                Si Usted es usuario Operario y aún no cuenta con acceso a la Intranet seleccione <a href="javascript:void(0)" onclick="Abrir_ventana('solicitarContrasena.aspx?tipo=solicitar')" >aquí</a>.

                            </div>
                            <br />
                            <asp:Login ID="Login2" runat="server" FailureText="Los datos ingresados no parecen ser correctos. Por favor vuelva a intentarlo."
                                LoginButtonText="Entrar" UserNameLabelText="Usuario:" PasswordLabelText="Contraseña:"
                                OnLoggedIn="Login2_LoggedIn" onloggingin="Login2_LoggingIn">
                            </asp:Login>
                            <div style="font-size: 12px; color: #2A3C6D; font-family: Arial; text-align: center;">

                                <a href="javascript:void(0)" onclick="Abrir_ventana('solicitarContrasena.aspx?tipo=recuperar')" >¿Olvidaste la contrase&ntilde;a?</a>

                            </div>
                        </div>
                    </div>
                </div>
                <!--/main-inner-->
            </div>
            <!--/main-->
            <div id="footer">
            </div>
            <div style=" font-family:Arial; font-size:11px; border: 1px solid; margin-left: 220px; margin-right: 220px; margin-top: 50px;
                padding: 15px 50px 15px 50px; background-repeat: no-repeat; background-position: 10px center;
                display: block; color: #9F6000; background-color: #FEEFB3; background-image: url(images/alerta.png);">
                Para consultas sobre esta sección o su contenido, y para agregar tu foto, por favor
                contactar a <b>María del Carmen Aranguren</b> </div>
        </div>
        <!--/wrapper-inner-->
    </div>
    <!--/wrapper-->
    </form>
</body>
</html>
