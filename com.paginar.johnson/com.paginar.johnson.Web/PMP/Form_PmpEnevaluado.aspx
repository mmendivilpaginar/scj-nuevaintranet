﻿<%@ Page Theme="PMP" Title="" Language="C#" MasterPageFile="~/PMP/MasterPages/MP.Master" AutoEventWireup="true" CodeBehind="Form_PmpEnevaluado.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Form_PmpEnevaluado" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="OKMessageBox.ascx" tagname="OKMessageBox" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            /* Semaforos */
            $('#t1').addClass('oKgreen');
            $('#tabs-1').addClass('oKgreen');

            $('#t2').addClass('oKgreen');
            $('#tabs-2').addClass('oKgreen');


            revisarSemaforoComentarioEvaluado();
            $('textarea').blur(function () {
                revisarSemaforoComentarioEvaluado();
            });
            
            /* fin Semaforos */

        });

        //Cieerre de X (2), Cierre Guardar sin enviar (1), Cierre por envío a evaluador (3)

        var flagCierre = 2;

        function confirmOnClose() {
            if (flagCierre == 1) {
                //flagCierre = 2;
                //return "Usted retornará al Panel de Control."; //Cierre Guardar sin enviar 1
            }
            else {
                if (flagCierre == 2) {
                    return 'Si el formulario no fue guardado es probable que pierda los datos'; // Cieerre de X
                }
            }
        }

        window.onbeforeunload = confirmOnClose;

        function GuardarSinEnviar() {
            flagCierre = 1;            
            window.close();
        }
</script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="CPH1" runat="Server"><asp:HiddenField ID="HiddenFieldPeriodoID" Value="1" runat="server" />
    <script type="text/javascript">
       
        var ModalProgress = '<%= ModalProgress.ClientID %>';
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

        function beginReq(sender, args) {

            // muestra el popup 

            $find(ModalProgress).show();

        }



        function endReq(sender, args) {

            //  esconde el popup 

            $find(ModalProgress).hide();

        }

        function ValidarTextBoxFundamentacion(sender, args) {

            var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
            var TextOpcional = "Puede fundamentar la fortaleza indicada";
            args.IsValid = true;

            var RBMB = sender.attributes["RBMB"].value;
            var RBB = sender.attributes["RBB"].value;
            var RBR = sender.attributes["RBR"].value;
            var RBNS = sender.attributes["RBNS"].value;
            var TextBoxFundamentacion = sender.attributes["TextBoxFundamentacion"].value;

            if ($(RBR).is(':checked') || $(RBNS).is(':checked')) {
                if (($(TextBoxFundamentacion).val() == "") || ($(TextBoxFundamentacion).val() == TextRequerido) || ($(TextBoxFundamentacion).val() == TextOpcional)) {
                    args.IsValid = false;
                }

            }
        }


        function ValidarTextBoxObservacionAreasDeOperaciones(sender, args) {


            args.IsValid = true;

            var CheckBoxAreaDeOperacion = sender.attributes["CheckBoxAreaDeOperacion"].value;
            var TextBoxObservacion = sender.attributes["TextBoxObservacion"].value;

            if ($(CheckBoxAreaDeOperacion).is(':checked'))
                if ($.trim($(TextBoxObservacion).val()) == "")
                    args.IsValid = false;



        }

        function ValidarRadiosEnGrillaCompetencias(sender, args) {
            args.IsValid = ValidarRadiosEnGrilla("<%= GridViewCompetencias.ClientID %>");
        }
        function ValidarRadiosEnGrillaAspectos(sender, args) {
            args.IsValid = ValidarRadiosEnGrilla("<%= GridViewAspectos.ClientID %>");
        }
        function SetearPagina() {
            if (!Page_ClientValidate('EnviarEvaluado')) {
                SetearGrilla("<%= GridViewCompetencias.ClientID %>");
                SetearGrilla("<%= GridViewAspectos.ClientID %>");
                //code that need to be executed if page is validated

            }
        }


        function ValidaComentariosEvaluado(oSrc, args) {

            var comentario = document.getElementById('<%= ((TextBox) FormViewFormulario.FindControl("TextBoxComentarioEv") ).ClientID %>').value;
            if (comentario == "") {
                args.IsValid = confirm('No agregó comentarios. ¿Desea continuar?');
            }
            else {
                args.IsValid = true;
            }
        }


        function ValidaAreasdeInteres(oSrc, args) {

            var cont = 0;

            var grilla = document.getElementById('<%= GVAO.ClientID %>');
            grilla = grilla.all[0];
            var celdas = grilla.cells;

            for (i = 1; i < celdas.length; i++) {
                if (celdas[i].firstChild.type == "checkbox" && celdas[i].firstChild.checked == true) {
                    cont++;
                }
            }

            if (cont == 0) {
                args.IsValid = confirm('No cuenta con Áreas de Interés Seleccionadas. ¿Desea continuar?');
            }
            else {
                args.IsValid = true;
            }
        }



        function CheckAreasdeInteres() {

//            var cont = 0;

//            var grilla = document.getElementById('<%= GVAO.ClientID %>');
//            grilla = grilla.all[0];
//            var celdas = grilla.cells;

//            for (i = 1; i < celdas.length; i++) {
//                if (celdas[i].firstChild.type == "checkbox" && celdas[i].firstChild.checked == true) {
//                    cont++;
//                }
//            }

//            if (cont == 0) {
//                return confirm('No cuenta con Áreas de Interés Seleccionadas. ¿Desea continuar?');
//            }
//            else {
//                return false;
            //            }
            
                var seleccionado = false;

                $("#CPH1_GVAO input[type=checkbox]").each(function () {
                    if (this.checked)
                       seleccionado = true;
                });

            
              if(!seleccionado)
                  return confirm('No cuenta con Áreas de Interés Seleccionadas. ¿Desea continuar?');
              else 
                 return true;
                 
        }


        function ClickEnvio() {


            if (confirm('¿La Evaluación será Enviada. Es correcto?')) {
                if (Page_ClientValidate('EnviarJP')) {
                    var VarCheckAreasdeInteres = CheckAreasdeInteres();
                    if (!VarCheckAreasdeInteres) return VarCheckAreasdeInteres;
                    var VarChequeaComentario = CheckComentario('ctl00_CPH1_FormViewFormulario_TextBoxComentarioEv');
                    if (!VarChequeaComentario) return VarChequeaComentario;
                    return (VarChequeaComentario && VarCheckAreasdeInteres);
                }
            }
            else
                return false;
        
        }

        function ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
            popup('Impresion.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
        }
    </script>
    <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldPasoID" Value="" runat="server" />
    
    <asp:Panel Visible="false" CssClass="mensaje exito" ID="PanelExito" runat="server">
        El Formulario fue enviado exitosamente
    </asp:Panel>



    <asp:ValidationSummary ID="ValidationSummaryFormulario" runat="server" 
        ValidationGroup="NOHABILITADO" CssClass="mensaje error"/>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="GrupoImprimir"/>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="EnviarJP" CssClass="mensaje error"/>
    <br />
    <h2 class="hide">Evaluación</h2>
   <div id="tabs">
      <ul>
         <li id="t1"><a href="#tabs-1">Competencias</a></li>
         <li id="t2"><a href="#tabs-2">Objetivos</a></li>
         <li id="t3"><a href="#tabs-3">Áreas de interés</a></li>
         <li id="t4"><a href="#tabs-4">Comentarios</a></li>
      </ul>
      
      <div id="tabs-1">
         <h3>Competencias</h3>
         
            
         
         <div class="definicion-grado">
            <h4>Definición del Grado</h4>
            <br />
            <ul>
               <li><strong>Muy Bueno:</strong> El desempeño excede claramente los requerimientos para la tarea.</li>
               <li><strong>Bueno:</strong> El desempeño es acorde a los requerimientos de la tarea.</li>
               <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Posee un buen desempeño, pero sin alcanzar el nivel esperado.</li>
               <li><strong>No Satisfactorio:</strong> El desempeño no satisface los requerimientos mínimos para las tareas.</li>
            </ul>                  
         </div>
         <asp:GridView ID="GridViewCompetencias" runat="server" 
              AutoGenerateColumns="False" DataKeyNames="ItemEvaluacionID" 
              DataSourceID="ObjectDataSourceCompetencias" 
              onrowdatabound="GridViewCompetencias_RowDataBound" 
              >
              <Columns>
                  <asp:TemplateField HeaderText="Competencias">
                      <ItemTemplate>
                          <div class="titulo">
                              <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                         <asp:HyperLink ID="HyperLink1" CssClass="masInfo" Visible='<%# Eval("Descripcion").ToString()!= "" %>' runat="server" ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>
                          </div>
                          <div class="detalle">
<%--                              <asp:Label ID="Label2" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>
                              <asp:HiddenField ID="HFID" runat="server" 
                                  Value='<%# Eval("ItemEvaluacionID") %>' />--%>
                          </div>                          
                      </ItemTemplate>
                      <ItemStyle CssClass="columnEmphasis" /> 
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Muy Bueno">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBMB" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),4) %>' />
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Bueno">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBB" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),3) %>' />
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Regular">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBR" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),2) %>'/>
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                      <HeaderTemplate>
                      <asp:Label runat="server" ID="LableHeaderRegular" Text="Regularmente <br/> Cumple/ <br/> Necesita <br/>desarrollarse"></asp:Label>
                      </HeaderTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="No Satisfactorio">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBNS" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>'  Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),1) %>'/>
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                      <ItemTemplate>
                          
                          <asp:Label ID="lblFundamentacion" runat="server"   Width="80%"    Text='<%# Bind("Fundamentacion") %>' > </asp:Label>
                          
                                          
                          
                          
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Left" />
                  </asp:TemplateField>
              </Columns>
          </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                      Name="PeriodoID" PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>
      </div>
      
      <div id="tabs-2">
         <h3>Objetivos</h3> 
             
         <div class="definicion-grado">
            <h4>Definición del Grado</h4>
            <br />
            <ul>
               <li><strong>Muy Bueno:</strong> Los objetivos son cumplidos con naturalidad y exceden constantemente las expectativas. Las acciones de esta persona aparecen ante los demás como un modelo para la organización.  </li>
               <li><strong>Bueno:</strong> Conoce y domina sus objetivos. La mayoría de las veces se cumplen los objetivos y los aportes individuales son positivos y evidentes. </li>
               <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Algunos objetivos no son cumplidos. Conoce lo que se espera de su contribución pero, a pesar de su esfuerzo, no cumple totalmente con su rol requerido.  </li>
               <li><strong>No Satisfactorio:</strong> No conoce o no domina correctamente lo que se espera de su contribución. Hay diferencias notables entre objetivos y desempeño. </li>
            </ul>                  
         </div>
         <asp:GridView ID="GridViewAspectos" runat="server" AutoGenerateColumns="False" 
              DataKeyNames="ItemEvaluacionID" DataSourceID="ObjectDataSourceAspectos" 
              onrowdatabound="GridViewAspectos_RowDataBound">
              <Columns>
                  <asp:TemplateField HeaderText="Objetivos">
                      <ItemTemplate>
                          <div class="titulo">
                              
                              <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                         <asp:HyperLink ID="HyperLink1" CssClass="masInfo" Visible='<%# Eval("Descripcion").ToString()!= "" %>' runat="server" ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>
                          </div>
                          <div class="detalle">
<%--                              <asp:Label ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label><asp:HiddenField ID="HFID" runat="server" 
                                  Value='<%# Eval("ItemEvaluacionID") %>' />--%>
                          </div>                          
                      </ItemTemplate>
                      <ItemStyle CssClass="columnEmphasis" /> 
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Muy Bueno">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBMB" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),4) %>'/>
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Bueno">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBB" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),3) %>'/>
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Regular">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBR" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),2) %>'/>
                      </ItemTemplate>
                      <HeaderTemplate>
                      <asp:Label runat="server" ID="LableHeaderRegular" Text="Regularmente <br/> Cumple/ <br/> Necesita <br/>desarrollarse"></asp:Label>
                      </HeaderTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="No Satisfactorio">
                      <ItemTemplate>
                          <asp:RadioButton ID="RBNS" runat="server" 
                              GroupName='<%# Eval("ItemEvaluacionID") %>' Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),1) %>'/>
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="85px" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                      <ItemTemplate>
                          <asp:Label ID="lblFundamentacion"       runat="server" Text='<%# Bind("Fundamentacion") %>' Width="80%"></asp:Label>                         
</ItemTemplate>
                      <ItemStyle HorizontalAlign="Left" />
                  </asp:TemplateField>
              </Columns>
          </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceAspectos" runat="server" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                      Name="PeriodoID" PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>
      </div>
      
      <div id="tabs-3">
         <h3>Áreas de interés del evaluado <span>(área a ser completada por el evaluado, opcional)</span></h3>
         <p class="descripcion">Seleccione, tildando sobre el casillero ubicado en la primera columna, el o las áreas de interés y describa brevemente cual es el rol o las funciones que le interesaría desempeñar allí.</p>
         
         <asp:GridView ID="GVAO" runat="server" AutoGenerateColumns="False" 
              DataKeyNames="AreaDeOperacionID" 
              DataSourceID="ObjectDataSourceAreasDeOperaciones" 
              onrowdatabound="GVAO_RowDataBound">
              <Columns>
                  <asp:TemplateField>
                      <ItemTemplate>
                          <asp:CheckBox ID="CheckBoxAreaDeOperacion" runat="server" 
                              Checked='<%# Bind("Chequeado") %>' />
                          <asp:HiddenField ID="HiddenFieldAreaDeOperacionID" runat="server" 
                              Value='<%# Eval("AreaDeOperacionID") %>' />
                      </ItemTemplate>
                      <ItemStyle HorizontalAlign="Center" Width="45px" /> 
                  </asp:TemplateField>
                  
                 <%-- <asp:BoundField DataField="Descripcion" HeaderText="Área  de operaciones" SortExpression="Descripcion">
                     <ItemStyle Width="140px" Font-Bold="true" />
                  </asp:BoundField>--%>
                  
                  <asp:TemplateField HeaderText="Área  de Operaciones" SortExpression="Descripcion">
                      <ItemTemplate>
                          
                              
                              <asp:Label Font-Bold="true" ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                              <%--<div class="titulo">
                          </div>--%>
                          </ItemTemplate>
                          <ItemStyle Width="140px" />
                          </asp:TemplateField>
                  
                  <asp:TemplateField HeaderText="Observaciones - Roles y Funciones">
                      <ItemTemplate>
                          <asp:TextBox Text='<%# Bind("Observacion") %>' ID="TextBoxObservacion" runat="server" TextMode="MultiLine" Width="93%"></asp:TextBox>
                          <asp:CustomValidator ID="CustomValidatorTextBoxAreasDeOperaciones" runat="server" Text="*" ClientValidationFunction="ValidarTextBoxObservacionAreasDeOperaciones" ValidationGroup="EnviarJP"></asp:CustomValidator>
                      </ItemTemplate>
                  </asp:TemplateField>
              </Columns>
          </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceAreasDeOperaciones" runat="server" 
              OldValuesParameterFormatString="original_{0}" 
              SelectMethod="GetAreasOperaciones" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>
          <%--<asp:CustomValidator ID="cvAreaInteres" ErrorMessage="sss" runat="server" 
              ClientValidationFunction="ValidaAreasdeInteres" ValidationGroup="EnviarJP"></asp:CustomValidator>--%>
      </div>
      
      <div id="tabs-4">
         <h3>Comentarios</h3>
         <br />
          <asp:FormView ID="FormViewFormulario" runat="server" 
              DataKeyNames="Legajo,PeriodoID,TipoFormularioID" 
              DataSourceID="ObjectDataSourceFormulario" Width="100%" 
              ondatabound="FormViewFormulario_DataBound" 
              >
              <ItemTemplate>
              <table class="GridView">
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Evaluador
                    &nbsp;
                        <asp:HyperLink ID="HyperLink1" CssClass="masInfo" runat="server" ToolTip="Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación recomendada, etc.">
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>     
                  </div> 
<%--                  <div class="detalle">
                     Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación recomendada, etc.
                  </div>--%>
               </td>
               <td>
               <asp:Label ID="LabelComentarioJT" runat="server" Text='<%# DefaultValComentarios(Eval("ComentarioJefeTurno"),"S/C") %>'></asp:Label>
                  <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioJT" runat="server" TextMode="MultiLine" Text='<%# Bind("ComentarioJefeTurno") %>' Width="93%"></asp:TextBox>--%>
               </td>
               <%-- <td style="width:1%">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentarioJT" runat="server" 
                        Display="Dynamic" ControlToValidate="TextBoxComentarioJT" ErrorMessage="Agregar Comentario de Líder de Turno" 
                        ValidationGroup="EnviarEvaluado">*</asp:RequiredFieldValidator>
                </td>--%>
            </tr>
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Evaluado  &nbsp;
                        <asp:HyperLink ID="HyperLink2" CssClass="masInfo" runat="server" ToolTip="Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su rol actual, su relación con sus compañeros y supervisores. Como también, observaciones útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la Compañía.">
                            <asp:Image ID="Image2" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>  
                  </div>
<%--                  <div class="detalle">
                     Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su rol actual, su relación con sus compañeros y 
                     supervisores. Como también, observaciones útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la Compañía.
                  </div>--%>
               </td>
               <td>
               
                 <asp:TextBox ID="TextBoxComentarioEv" runat="server" onChange="revisarSemaforoComentarioEvaluado()" rel = "comenEvaluado" TextMode="MultiLine" Width="93%" Text='<%# Bind("ComentarioEvaluado") %>'></asp:TextBox>               
               <asp:RequiredFieldValidator ControlToValidate="TextBoxComentarioEv" ID="RequiredFieldValidatorTextBoxComentarioEv"
                               runat="server" Text="*" ValidationGroup="EnviarJP" ErrorMessage="Ingresar los comentarios de la evaluacíon para continuar con la misma."></asp:RequiredFieldValidator>
                        
               </td>
                <%--<td style="width:1%">
                    &nbsp;</td>--%>
            </tr>
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Auditor  &nbsp;
                        <asp:HyperLink ID="HyperLink3" CssClass="masInfo" runat="server" ToolTip="Observaciones adicionales.">
                            <asp:Image ID="Image3" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>  
                  </div>
<%--                  <div class="detalle">Observaciones adicionales</div>--%>
               </td>
               <td>
               <asp:Label ID="LabelComentarioJP" runat="server" Text='<%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"),"S/C") %>'></asp:Label>
                  <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# Bind("ComentarioJefeDePlanta") %>' runat="server" TextMode="MultiLine" Width="93%"></asp:TextBox>--%>
               </td>
               <%-- <td style="width:1%">
                    &nbsp;</td>--%>
            </tr>                        
         </table>                  
              </ItemTemplate>
              <InsertItemTemplate>
              <table class="GridView">
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Evaluador</div> 
                  <div class="detalle">
                     Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación recomendada, etc.
                  </div>
               </td>
               <td>
               <asp:Label ID="LabelComentarioJT" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeTurno"),"S/C") %>'></asp:Label>
                 <%-- <asp:TextBox ID="TextBoxComentarioJT" Enabled="false" runat="server" TextMode="MultiLine" Text='<%# Bind("ComentarioJefeTurno") %>' Width="93%"></asp:TextBox>--%>
               </td>
              <%-- <td style="width:1%">
                    
                </td>--%>
            </tr>
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Evaluado</div>
                  <div class="detalle">
                     Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su rol actual, su relación con sus compañeros y 
                     supervisores. Como también, observaciones útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la Compañía.
                  </div>
               </td>
               <td>
                  <asp:TextBox ID="TextBoxComentarioEv" runat="server" TextMode="MultiLine" Width="93%" Text='<%# Bind("ComentarioEvaluado") %>'></asp:TextBox>               
               <asp:RequiredFieldValidator ControlToValidate="TextBoxComentarioEv" ID="RequiredFieldValidatorTextBoxComentarioEv"
                               runat="server" Text="*" ValidationGroup="EnviarJP" ErrorMessage="Ingresar los comentarios de la evaluacíon para continuar con la misma."></asp:RequiredFieldValidator>
               </td>
               <%--<td style="width:1%">
               </td>--%>
            </tr>
            <tr>
               <td class="columnEmphasis">
                  <div class="titulo">Auditor</div>
                  <div class="detalle">Observaciones adicionales</div>
               </td>
               <td>
               <asp:Label ID="LabelComentarioJP" runat="server" Text='<%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"),"N/C") %>'></asp:Label>
                  <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# Bind("ComentarioJefeDePlanta") %>' runat="server" TextMode="MultiLine" Width="93%"></asp:TextBox>--%>
               </td>
              <%-- <td style="width:1%">
               </td>--%>
            </tr>                        
         </table>                  
              </InsertItemTemplate>
          </asp:FormView>
          <asp:ObjectDataSource ID="ObjectDataSourceFormulario" runat="server" 
              DeleteMethod="Delete" InsertMethod="Insert" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
              TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter" 
              UpdateMethod="Update" >
              <DeleteParameters>
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
              </DeleteParameters>
              <UpdateParameters>
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="FAlta" Type="DateTime" />
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
              </UpdateParameters>
              <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
              <InsertParameters>
                  <asp:Parameter Name="Legajo" Type="Int32" />
                  <asp:Parameter Name="PeriodoID" Type="Int32" />
                  <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
              </InsertParameters>
          </asp:ObjectDataSource>
         

      </div>      
   </div><!--/tabs-->
    <asp:CustomValidator ID="CustomValidatorGrillaCompetencias" ClientValidationFunction="ValidarRadiosEnGrillaCompetencias" runat="server"  ErrorMessage="Faltan Seleccionar Items en la Grilla de Competencias" ValidationGroup="EnviarEvaluado">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidatorGrillaAspectos" ClientValidationFunction="ValidarRadiosEnGrillaAspectos" runat="server"  ErrorMessage="Faltan Seleccionar Items en la Grilla de Aspectos" ValidationGroup="EnviarEvaluado">&nbsp;</asp:CustomValidator>
    <br />
    
    
   <div id="acciones">
       <span class="button">
          <asp:UpdatePanel ID="upPnlPage" runat="server">
        <ContentTemplate>
        <asp:Button ValidationGroup="EnviarJP" ID="ButtonEnviarJP" runat="server" OnClientClick="javascript:document.body.onbeforeunload='';" CssClass="formSubmitAsync"
           Text="Enviar al Auditor" onclick="ButtonEnviarJP_Click" ToolTip="Guarda la Evaluación dejando la misma disponible para el Auditor. El Evaluado no puede realizar más cambios" 
           EnableTheming="false"  />
                      
        </ContentTemplate>
    </asp:UpdatePanel>
    </span>  
       
<%--      <span class="button">
          <asp:UpdatePanel ID="UpdatePanelExportar" runat="server">
            <ContentTemplate>
                <asp:Button ID="ButtonExportar" 
                    ToolTip="Visualiza el formulario en modo de Exportación con los datos volcados en la base de datos"  
                    runat="server" Text="Exportar" CssClass="formSubmitAsync" EnableTheming="false" 
                    onclick="ButtonExportar_Click" />
            </ContentTemplate>           
          </asp:UpdatePanel>
      </span>--%>
          <span class="button">
         <asp:UpdatePanel ID="UpdatePanelDraft" runat="server">
            <ContentTemplate>
           
                  <asp:Button ID="ButtonGSinEnviar" runat="server" Text="Guardar sin enviar" 
                     OnClick="ButtonGSinEnviar_Click" CssClass="formSubmitAsync" 
                     EnableTheming="false" 
                     ToolTip="Guarda la evaluación sin enviar la misma al Evaluado" OnClientClick="flagCierre = 1; SetearPagina();" />
               
           </ContentTemplate>
          </asp:UpdatePanel>
    </span>
    <span class="button">
         <asp:UpdatePanel ID="UpdatePanelImprimir" runat="server">
            <ContentTemplate>
               <asp:Button ID="ButtonImprimir" runat="server" Text="Imprimir/Exportar" 
                    ValidationGroup="GrupoImprimir" CssClass="formSubmitAsync"           
                        ToolTip="Visualiza el formulario en modo de Impresión con los datos volcados en la base de datos" 
                        onclick="ButtonImprimir_Click" EnableTheming="false" />
            </ContentTemplate>
         </asp:UpdatePanel>
      </span>     
   </div>
    <asp:UpdatePanel ID="UpdatePanelExito" runat="server">
           <ContentTemplate>
           <uc1:OkMessageBox ID="omb" runat="server" />  
           </ContentTemplate>
           <Triggers>
               <asp:AsyncPostBackTrigger ControlID="ButtonEnviarJP" EventName="Click" />
           </Triggers>
           </asp:UpdatePanel>

  <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
      <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="upPnlPage">
         <ProgressTemplate>
            
               <div class="precarga">
               <asp:Image AlternateText="Procesando" ID="ImgProc" ImageUrl="~/pmp/img/ajax-loader.gif" runat="server" />
               <p>Operación en progreso...</p>
            </div>   
         </ProgressTemplate>
      </asp:UpdateProgress>
   </asp:Panel>
   <cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
      BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:HiddenField ID="HiddenFieldJS" runat="server" />
</asp:Content>
