﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PMP/MasterPages/MP_PMP.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

   <style type="text/css">
      a:hover
      {
         background: #ffffff;
         text-decoration: none;
      }
      .tooltip {  font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666666; text-decoration: none}
      a.tooltip:hover { color: #DA421A; text-decoration: none }
      /*BG color is a must for IE6*/a.tooltip span
      {
         display: none;
         padding: 2px 3px;
         margin-left: 8px;
         
      }
      a.tooltip:hover span
      {
         display: inline;
         position: absolute;
         background: #ffffff;
         border: 1px solid #cccccc;
         color: #6c6c6c;
      }
      
   </style>

   <script type="text/javascript">
       function popup(url, ancho, alto) {
           var posicion_x;
           var posicion_y;
           posicion_x = (screen.width / 2) - (ancho / 2);
           posicion_y = (screen.height / 2) - (alto / 2);
           window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

       }

       function Toggle(TextBoxID, lnkID, chkbID) {
           var param;
           var chk = document.getElementById(chkbID);
           var txt = document.getElementById(TextBoxID);
           var lnk = document.getElementById(lnkID);

           if (chk.checked == true)
               param = confirm("¿Esta seguro de ocultar la evaluación? El evaluado seleccionado quedará sin evaluación.");
           else
               param = true;
           if (param) {

               if (txt.style.display == 'block') {
                   txt.disabled = true;
                   txt.style.display = 'none';
                   lnk.style.display = 'block';
                   //$('#' + TextBoxID).addClass("disabled");
               }
               else {
                   txt.disabled = false;
                   txt.style.display = 'block';
                   lnk.style.display = 'none';
                   //$('#' + TextBoxID).removeClass("disabled");
                   txt.focus();

               }

           }
           else
               chk.checked = false;

           return false;
       }

       function CountSeleccionados() {
           var expr1 = '#<%=GridViewFormularios.ClientID %> >tbody >tr >td >input:checkbox:checked';
           var CantSeleccionados = $(expr1).length;
           return CantSeleccionados;
       }

       function Confirmacion() {
           if (CountSeleccionados() == 0) {
               alert("Debe tener seleccionada por lo menos una Evaluación.");
               return false;
           } else {
               return true;
           }
       }        
     
   </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <script type="text/javascript">
        function refreshParent() {
            __doPostBack('ActGrilla', '')
        }

        function ocultargrilla(panel) {
            $("'#" + panel + "'").hide();
        }
    </script>
    <asp:Label ID="Label6" runat="server" Text="Panel Evaluador" 
        CssClass="tituloN1"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="LabelNombreUsuarioActual" runat="server" Text="Label" CssClass="tituloN3"></asp:Label>
   <asp:HiddenField Value="6666" ID="HiddenFieldLegajoEvaluador" runat="server" />
   
   <p>
      
   
    
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Período de Evaluación: " CssClass="descripcion"
                    Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPeriodos" runat="server" DataSourceID="ObjectDataSourcePeriodos"
                    DataTextField="Descripcion" DataValueField="PeriodoID" AutoPostBack="True" OnSelectedIndexChanged="DropDownListPeriodos_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePeriodos" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetPeriodos" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Estado:" CssClass="descripcion" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListPasos" runat="server" DataSourceID="ObjectDataSourcePasos"
                    DataTextField="Descripcion" DataValueField="PasoID" 
                    AppendDataBoundItems="True">
                    <asp:ListItem Value="0">TODOS</asp:ListItem>
                    <asp:ListItem Value="-1">No iniciado</asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourcePasos" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetPasosTodos" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar" OnClick="BtnBuscar_Click" />
            </td>
            <%--<td>
                <asp:UpdateProgress ID="UpdateProgressBusqueda" runat="server">
                    <ProgressTemplate>
                        <img alt="" src="img/progress.gif" style="width: 16px; height: 16px" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>--%>
        </tr>
    </table>
    
   </p>
   <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value="1" />
   <asp:HiddenField ID="HiddenFieldPasoID" runat="server" Value="1" />
             
   <p>
   
   
  <%--    
   <asp:UpdatePanel ID="UpdatePanelBusqueda" runat="server">
   
   
           <ContentTemplate>--%>
       <asp:GridView ID="GridViewFormularios" CssClass="dashboard" runat="server" AutoGenerateColumns="False"
           DataKeyNames="Legajo,PeriodoID,TipoFormularioID" DataSourceID="ObjectDataSourceFormularios"
           OnRowDataBound="GridViewFormularios_RowDataBound" AllowSorting="True" 
           ondatabound="GridViewFormularios_DataBound" >
           <Columns>
  <%--             <asp:BoundField HeaderText="Formulario" SortExpression="TipoFormularioDesc">
               <HeaderStyle CssClass="destacados" />
               </asp:BoundField>--%>
               <asp:TemplateField>
                   <ItemTemplate>
                       <asp:ImageButton ID="ImageCommand" runat="server" />                       
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Legajo">
                   <ItemTemplate>
                       <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkLegajo" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="Legajo" Text="Legajo">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Apellido y Nombre">
                   <ItemTemplate>
                       <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" CommandName="Sort"
                               CssClass="destacados" CommandArgument="EvaluadoNombreyApellido" Text="Apellido y Nombre">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Formulario">
               <HeaderTemplate>
                       <strong>
                           <asp:Label ID="lnkFormulario" runat="server"  CssClass="destacados"
                                Text="Formulario">
                           </asp:Label>
                       </strong>
                   </HeaderTemplate>
                   <ItemTemplate>
                       <asp:Label ID="Label7" runat="server" Text='<%# Eval("TipoFormularioDesc") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Sector">
                   <ItemTemplate>
                       <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("EvaluadoSector") %>'></asp:Literal>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkEvaluadoSector" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="EvaluadoSector" Text="Sector">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Cargo">
                   <ItemTemplate>
                       <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkCargo" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="EvaluadoCargo" Text="Cargo">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Inicio Evaluación" SortExpression="FAlta">
                   <ItemTemplate>
                       <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkFAlta" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="FAlta" Text="Inicio Evaluación">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                   <ItemTemplate>
                       <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                       <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>' />
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkEstado" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="Estado" Text="Estado">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Resultado" SortExpression="Calificacion">
                   <ItemTemplate>
                       <asp:Label ID="LabelCalificacion" runat="server" Text='<%# DefaultVal(Eval("Calificacion")) %>' Visible='<%# !((Eval("EstadoID").ToString()=="1" && Eval("ComentarioEvaluado").ToString().Trim()=="")|| (Eval("EstadoID").ToString()=="5") ) %>'></asp:Label>
                   </ItemTemplate>
                   <HeaderTemplate>
                       <strong>
                           <asp:LinkButton ID="lnkCalificacion" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="Calificacion" Text="Resultado">
                           </asp:LinkButton>
                       </strong>
                   </HeaderTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Comentarios Evaluador">
                   <ItemTemplate>
                       <asp:TextBox ID="TextBoxComentarioJefe" runat="server" Text='<%# Eval("ComentarioJefeTurno")%>' Width='65px' ></asp:TextBox>
                       <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                       <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                       <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                     <span class="tooltip" runat="server" id="LinkSinComentarioJefeTurno" Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length ==0 || (Eval("EstadoID").ToString()=="1" && Eval("ComentarioEvaluado").ToString().Trim()=="") ) ? true : false %>'> <%# "S/C" %></span>  
                     <span runat="server" id="LinkComentarioJefeTurno" Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length >0 && (Eval("EstadoID").ToString()!="1" || Eval("ComentarioEvaluado").ToString().Trim()!="")) ? true : false %>'>
                       <a onclick="javascript:void(0)" id='tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" rel="#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                           class="tooltip">
                           <%# DefaultValComentarios(Eval("ComentarioJefeTurno")) %></a>
                           </span>
                       <div id='loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                           <asp:Repeater ID="FVJT" runat="server" DataSourceID="ObjectDataSourceCJT">
                               <ItemTemplate>
                                   - <strong>
                                       <asp:Literal ID="EstadoRolLabel" runat="server" Text='<%# Bind("EstadoRol") %>' /></strong>:
                                   <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' /><br />
                                   - <strong>Evaluado</strong>:
                                   <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                   - <strong>Fecha</strong>:
                                   <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                   <br />
                                   - <strong>Comentario</strong>:</ItemTemplate>
                           </asp:Repeater>
                           <asp:ObjectDataSource ID="ObjectDataSourceCJT" runat="server" OldValuesParameterFormatString="original_{0}"
                               SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                               <SelectParameters>
                                   <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                       PropertyName="Value" Type="Int32" />
                                   <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                               </SelectParameters>
                           </asp:ObjectDataSource>
                           <span id="Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" >
                               <%# (Eval("EstadoID").ToString() != "1" || Eval("ComentarioEvaluado").ToString().Trim() != "") ? Eval("ComentarioJefeTurno") : ""%>
                           </span>
                       </div>

                       <script type="text/javascript">
                           $(document).ready(function () {

                               if (jQuery.trim($('#Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                   $('#tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                               }
                           })
                       </script>

                   </ItemTemplate>
                   <HeaderStyle CssClass="destacados" />                   
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Comentarios Evaluado">
                   <ItemTemplate>
                   <span class="tooltip" runat="server" id="LinkSinComentarioEvaluado" Visible='<%# (Eval("ComentarioEvaluado").ToString().Length ==0) ? true : false %>'><%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></span>  
                   <span class="tooltip" runat="server" id="LinkComentarioEvaluado" Visible='<%# (Eval("ComentarioEvaluado").ToString().Length >0) ? true : false %>'>  
                       <a onclick="javascript:void(0)" id='tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                           rel="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" class="tooltip">
                           <%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></a>
                           </span>
                       <div id='loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                           <asp:Repeater ID="FVEVAL" runat="server" DataSourceID="ObjectDataSourceEval">
                               <ItemTemplate>
                                   - <strong>Evaluado</strong>:
                                   <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                   - <strong>Fecha</strong>:
                                   <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                   <br />
                                   - <strong>Comentario</strong>:
                               </ItemTemplate>
                           </asp:Repeater>
                           <span id="SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" />
                           <%# Eval("ComentarioEvaluado")%>
                           </span>
                           <asp:ObjectDataSource ID="ObjectDataSourceEval" runat="server" OldValuesParameterFormatString="original_{0}"
                               SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                               <SelectParameters>
                                   <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                       PropertyName="Value" Type="Int32" />
                                   <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                               </SelectParameters>
                           </asp:ObjectDataSource>
                       </div>
                       </a>

                       <script type="text/javascript">
                           $(document).ready(function () {
                               if (jQuery.trim($('#SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                   $('#tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                               }
                           })
                       </script>

                   </ItemTemplate>
                   <HeaderStyle CssClass="destacados" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Comentarios Auditor">
                   <ItemTemplate>
                   <span class="tooltip" runat="server" id="LinkSinComentarioJefeDePlanta" Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length ==0) ? true : false %>'><%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></span>  
                   <span class="tooltip" runat="server" id="LinkComentarioJefeDePlanta" Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length >0) ? true : false %>'>
                       <a onclick="javascript:void(0)" id='tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" rel="#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                           class="tooltip">
                           <%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></a>
                           </span>
                       <div id='loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                           <asp:Repeater ID="FVJP" runat="server" DataSourceID="ObjectDataSourceJP">
                               <ItemTemplate>
                                   - <strong>
                                       <asp:Literal ID="EstadoRolLabel" runat="server" Text='<%# Bind("EstadoRol") %>' /></strong>:
                                   <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' /><br />
                                   - <strong>Evaluado</strong>:
                                   <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                   - <strong>Fecha</strong>:
                                   <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                   <br />
                                   - <strong>Comentario</strong>:
                               </ItemTemplate>
                           </asp:Repeater>
                           <span id="SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>">
                               <%# Eval("ComentarioJefeDePlanta")%>
                           </span>
                           <asp:ObjectDataSource ID="ObjectDataSourceJP" runat="server" OldValuesParameterFormatString="original_{0}"
                               SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                               <SelectParameters>
                                   <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                       Type="Int32" />
                                   <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                       PropertyName="Value" Type="Int32" />
                                   <asp:Parameter DefaultValue="3" Name="PasoID" Type="Int32" />
                               </SelectParameters>
                           </asp:ObjectDataSource>
                       </div>

                       <script type="text/javascript">
                           $(document).ready(function () {

                               if (jQuery.trim($('#SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                   $('#tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       hoverIntent: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                               }
                           })
                       </script>

                   </ItemTemplate>
                   <HeaderStyle CssClass="destacados" />
               </asp:TemplateField>
                <asp:TemplateField HeaderText="">
                   <ItemTemplate>
                        <asp:CheckBox ID="CheckBoxSinEvaluacion" runat="server" 
                            Checked='<%# (Eval("EstadoID").ToString()=="5")? true : false %>' />
                    </ItemTemplate>
                     <HeaderStyle CssClass="destacados" />
                </asp:TemplateField>
           </Columns>
           <EmptyDataTemplate>
               <asp:Label ID="Label4" runat="server" Text="No se encontraron registros para la búsqueda realizada" CssClass="destacados"></asp:Label>
           </EmptyDataTemplate>
       </asp:GridView>
               <asp:ObjectDataSource ID="ObjectDataSourceFormularios" runat="server" OldValuesParameterFormatString="original_{0}"
                   SelectMethod="GetFormulariosByEvaluadorEstadoTodos" 
           TypeName="com.paginar.formularios.businesslogiclayer.DashboardController" 
           onselecting="ObjectDataSourceFormularios_Selecting">
                   <SelectParameters>
                       <asp:ControlParameter ControlID="HiddenFieldLegajoEvaluador" Name="LegajoEvaluador"
                           PropertyName="Value" Type="Int32" />
                       <asp:ControlParameter ControlID="DropDownListPasos" Name="PasoID" PropertyName="SelectedValue"
                           Type="Int32" />
                       <asp:ControlParameter ControlID="DropDownListPeriodos" Name="PeriodoID" PropertyName="SelectedValue"
                           Type="Int32" />
                       <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                           PropertyName="Value" Type="Int32" />
                   </SelectParameters>
               </asp:ObjectDataSource>
           <%--</ContentTemplate>
           <Triggers>
               <asp:AsyncPostBackTrigger ControlID="BtnBuscar" EventName="Click" />
           </Triggers>
       </asp:UpdatePanel>--%>
    </p>
    <p style="text-align:right;">
       <asp:Button ID="btnGrabar" runat="server" Text="Grabar" OnClick="BtnGrabar_Click" OnClientClick="return Confirmacion();"/>
    </p>    

    
                       
                    <asp:Panel ID="PanelIniciarEvaluaciones" runat="server">
                    <p>      <asp:Label ID="Label2" CssClass="destacados" Font-Bold="true" runat="server" Text="En caso de no contar en la grilla con el operario buscado agregarlo desde acá"></asp:Label></p>
    <table>
        <tr>
            <td>
                
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                    <ContentTemplate>         
                     <asp:DropDownList ID="DropDownListUsuarios" runat="server" 
                             
                            >
                        </asp:DropDownList>          
                          <%-- <asp:DropDownList ID="DropDownListUsuarios" runat="server" DataSourceID="ObjectDataSourceOtrosEvalaudos"
                            DataTextField="ApellidoNombre" DataValueField="Legajo" 
                                onprerender="DropDownListUsuarios_PreRender">
                        </asp:DropDownList>
                         <asp:ObjectDataSource ID="ObjectDataSourceOtrosEvalaudos" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="GetDataAllOtrosEvaluadosByPaso" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="DropDownListPeriodos" Name="PeriodoID" PropertyName="SelectedValue"
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" DefaultValue="1" Name="TipoFormularioID"
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldLegajoEvaluador" DefaultValue="" Name="legajo"
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldPasoID" DefaultValue="1" Name="PasoID"
                                    PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>--%>
</ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DropDownListPeriodos" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                        
                <asp:HiddenField ID="HiddenFieldLegajoEvaluacion" runat="server" />
                             <asp:ObjectDataSource ID="ObjectDataSource1" runat="server">
                             </asp:ObjectDataSource>
                <asp:HiddenField ID="HiddenFieldTipoFormularioIDEvaluacion" runat="server" />
                <asp:HiddenField ID="HiddenFieldPeriodoIDEvaluacion" runat="server" />
            </td>
            <td>
                <asp:Button ID="ButtonEvaluar" runat="server" Text="Iniciar Evaluación" OnClick="ButtonEvaluar_Click"
                    ValidationGroup="TieneEvaluacion" />
                <asp:CustomValidator ID="cvPeriodo" runat="server" ErrorMessage="No puede cargar fuera del rango habilitado"
                    OnServerValidate="cvPeriodo_ServerValidate" 
                    ValidationGroup="TieneEvaluacion" Display="Dynamic"></asp:CustomValidator>
            </td>
            <td>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <img alt="" src="img/progress.gif" style="width: 16px; height: 16px" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    </asp:Panel>
                    
       <asp:UpdatePanel ID="UpdatePanelValidaComienzoEval" runat="server">
        <ContentTemplate>
            <asp:Repeater ID="RepeaterTieneEvaluacion" runat="server" 
                DataSourceID="ObjectDataSourceTieneEvaluacion" Visible="False">
      <ItemTemplate>
         <span class="destacados">El operario ya cuenta con una Evaluación en curso. El Evaluador a cargo es
            <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' /></span>
      </ItemTemplate>
      <AlternatingItemTemplate>
      <br />
      </AlternatingItemTemplate>
   </asp:Repeater>
   <asp:ObjectDataSource ID="ObjectDataSourceTieneEvaluacion" runat="server" OldValuesParameterFormatString="original_{0}"
      SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
      <SelectParameters>
         <asp:ControlParameter ControlID="HiddenFieldLegajoEvaluacion" Name="Legajo" PropertyName="Value"
            Type="Int32" />
         <asp:ControlParameter ControlID="HiddenFieldPeriodoIDEvaluacion" Name="PeriodoID"
            PropertyName="Value" Type="Int32" />
         <asp:ControlParameter ControlID="HiddenFieldTipoFormularioIDEvaluacion" DefaultValue=""
            Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
         <asp:ControlParameter ControlID="HiddenFieldPasoID" DefaultValue="1" Name="PasoID"
            PropertyName="Value" Type="Int32" />
      </SelectParameters>
   </asp:ObjectDataSource>

        </ContentTemplate>
           <Triggers>
               <asp:AsyncPostBackTrigger ControlID="DropDownListPeriodos" 
                   EventName="SelectedIndexChanged" />
           </Triggers>
    </asp:UpdatePanel>
   <p>&nbsp; &nbsp; &nbsp; &nbsp; </p>
 
</asp:Content>
