﻿<%@ Page Language="C#" Theme="PMP" AutoEventWireup="true" CodeBehind="ImpresionScrapper.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.ImpresionScrapper" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <style type="text/css">
/****styles********/
/*------------------------------------------------------------------
[Estilos generales]
*/
body {
	font-family: Arial;
	font-size: 12px;
	color: #558DC6;
}

h3 {
	text-transform: uppercase;
	margin-bottom: 10px;
}

h3 span {
	text-transform: none;
	font-weight: normal;
} 
p {
	margin-bottom: 15px;
}

p.descripcion {
	color:#2B3C6E;
	font-size: 11px;
}

.hide {
	display: none;
}
.alignCenter {
    text-align:center;
}

/* @group enlaces */
#page a {
    color:#558DC6;
}

a.btnHistorial {
	position: absolute;
	right: 240px;
	bottom: 14px;
}
a.btnEvalAnt {
	position: absolute;
	right: 90px;
	bottom: 14px;
}
a.btnObjetivos {
	position: absolute;
	right: 14px;
	bottom: 14px;
}

a.button  {
	background: url(../img/bg-btn-left.gif) no-repeat scroll 0 0;
	display:inline-block;
	white-space:nowrap;
	cursor:pointer;
	padding: 4px 0 5px 10px;
	text-decoration: none;
}

a.button .inner {
	background: url(../img/bg-btn-right.gif) no-repeat scroll 100% 0;
	font-size: 12px;
	color:#558DC6;
	font-size: 10px;
	font-weight: bold;

	height: 19px;
	padding: 3px 10px 6px 0;
}
/* @end */



/* @group tooltip */

/*contenedor*/
.cluetip-default{
    background-color:#2A3C6D;
}
.cluetip-default #cluetip-outer {
    background-color:#2A3C6D;
    font-size:11px;
    color:#FFFFFF;
}
.cluetip-default #cluetip-outer{
    padding: 10px;
}

/*titulo*/
.cluetip-default h3#cluetip-title {
    background-color:#2A3C6D;
    margin:0;
    padding:0;
    text-transform:none;
    font-weight:normal;
    font-size:11px;
    line-height:1.1;
}

/*cerrar*/
.cluetip-default div#cluetip-close {
    display:none;
}

/*contenido*/
.cluetip-default #cluetip-inner {
    margin:0;
    padding:0;
    text-transform:lowercase!important;
}
.cluetip-default #cluetip-inner .split-body {
    word-wrap:break-word;
}

/*flechas*/
#cluetip-arrows {

}


/* @end */




/* @group formularios */

.form-textarea {
	font-size: 11px!important;
	font-family: Arial!important;
	padding: 5px 0;
	color: #808080;
	border: 1px solid #999999;
	overflow: auto;
    width:100%;
	
	word-wrap: break-word;	
}

/*Button: CSS Sliding Doors*/
span.button  {
	background: url(../img/btn-l.gif) no-repeat scroll 0 0;
	display:inline-block;
	white-space:nowrap;
	cursor:pointer;
	padding-left: 5px;
	/*margin-right: 10px;*/
	margin-left: 10px;
}

.formSubmit,
.formSubmitAsync {
	background: url(../img/btn-r.gif) no-repeat scroll 100% 0;
	font-size: 12px;
	color:#FFFFFF;
	cursor:pointer;


	border:0;
	height: 18px;
	padding: 0 5px 3px 0;
	*padding-bottom: 0;/*IE6-7*/
	
	position: relative;
	white-space: nowrap;
	margin: 0;
	overflow: visible;
	white-space:normal;
	
}
/* @end */


/* @group tablas */

/* @group mensajes de usuario */
.mensaje {
	border: 1px solid;
	margin: 10px 0;
	padding:15px 10px 15px 50px;
	background-repeat: no-repeat;
	background-position: 10px center;
}
.mensaje ul{
	padding-left: 20px;
	list-style-type:none;
}
.info {
	color: #00529B;
	background-color: #BDE5F8;
	background-image: url(../img/info.png);
}
.exito {
	color: #4F8A10;
	background-color: #DFF2BF;
	background-image:url(../img/exito.png);
}
.alerta {
	color: #9F6000;
	background-color: #FEEFB3;
	background-image: url(../img/alerta.png);
}
.error {
	color: #D8000C;
	background-color: #FFBABA;
	background-image: url(../img/error.gif);
}
/* @end */

/*control gridview*/
.GridView {
	width:100%;
	font-family: Arial;	
}
.GridView tr.HeaderStyle th{
	padding: 15px 0;
	color: #2B3C6E;
	font-weight: normal;
	border-top: 1px solid #999999;
	border-right: 1px solid #ffffff;
}
.GridView td {
	padding: 5px;
	border: 1px solid #999999;
}
.GridView td.columnEmphasis {
	width: 260px;
}
.GridView tr.resaltado td { /*'resaltado' generado con jQuery*/
	background-color: #fdffda;
}
.GridView td .titulo {
	font-weight: bold;
}
.GridView td .detalle {
	font-size: 11px;
	margin-bottom: 15px;
}
.GridView td textarea {
	font-size: 11px;
	font-family: Arial;
	height: 80px;
	padding: 5px;
	color: #808080;
	border: 1px solid #999999;
	overflow: auto;
	
	word-wrap: break-word;	
}
.GridView td textarea.requerido{
	color:#808080;
	background-color:#F2F2F2;
}
.GridView td textarea.disabled {
	background-color: #C0C0C0;
}

.GridView td .status {/*'status' generado con jQuery*/
	display: inline;
	padding-left: 8px;
}
/* @end */




#page{
	width: 960px;
	margin: 0 auto;
}

/*------------------------------------------------------------------
[Cabecera]
*/
#header {
	position: relative;
	background-color:#FFFFFF;
	background-image: url(../img/bg-header.jpg);
	background-repeat: repeat-x;
	background-position: 0 0;
	margin-bottom: 15px;
}
#header-inner {
	padding: 25px 10px 0 10px;	
}

/* @group logo - titulo */
#logo-title {
	margin-bottom: 12px;
}

#site-name h1{
	color:#A6C5E1;
	font-size: 28px;
}
#site-name h1 span{
	font-size: 22px;
	display: block;
}
#logo {
	position: absolute;
	right: 10px;
	top: 25px;
}
/* @end */


/* @group datos de evaluacion */

/*fechas*/
#fechas {
	color: #FFFFFF;
	overflow: hidden;
	margin-bottom: 25px;
	zoom: 1; /*IE6*/
}
#fechas li{
	float: left;
	margin-right: 15px;
}

/*personas*/
#personas {
	background-color: #FFFFFF;	
	margin-bottom: 15px;
	width :854px;
	float: left;
}
#personas-inner {
	padding: 10px;
	overflow: hidden;
}
#personas-inner .inner-decoration {/*'inner-decoration' generado con jQuery*/
	background-image:url(../img/separator.gif);
	background-repeat: repeat-y;
	background-position: 405px top;
	overflow: hidden;
	zoom: 1; /*IE6*/
}
#personas h2 {
	text-transform: uppercase;
	font-size: 17px;
	margin-bottom: 10px;
}
#personas ul {
	padding: 0 0 2px 0;
	overflow: hidden;
}
#personas ul li{
	float: left;
	width: 200px;
	margin-bottom: 2px;
}

#personas .evaluador,
#personas .evaluado {
	width: 410px;
}
#personas .evaluador {
	float: left;
	color:#2B3C6E;
}

#personas .evaluado {
	float: right;
}

/*foto*/
#foto {
	width: 70px;
	height: 70px;
	float: right;
	border: 2px solid #FFFFFF;
}
#foto img{
	width: 70px;
	height: 70px;
	_display: block; /* soluciona Image breaks up in IE6 only*/
}

/*calificacion*/
#calificacion {
	clear: both;
	background-color: #FFFFFF;
	margin-left: 10px;
	margin-right: 10px;
	padding:15px 10px;
    min-height:16px;
	*height: 17px; /*IE6-7*/
}
#calificacion h2{
	color: #FFFFFF;
	font-weight: normal;
	font-size: 16px;
}
#calificacion h2 span{
	font-weight: bold;
	text-transform: uppercase;
}
/* @end */


#main {
	margin-bottom: 30px;
}



.definicion-grado {
	color: #2B3C6E;
	font-size: 11px;
	margin-bottom: 15px;
}


.definicion-grado h4 {
font-weight:normal;
margin-bottom:2px;
text-decoration:underline;
}


.definicion-grado li {
margin-bottom:10px;
}


/* @group Modal Popup */
.modalBackground {
	background-color: #000;
	filter: alpha(opacity=75);
	opacity: 0.75;
}
.contentPopup { 
	background-image:url(../img/bg-content-tab.jpg);
	background-repeat: repeat-x;
    background-color: #FFFFFF; 
    position: absolute; 
    width: 300px; 
    *width: 350px;/*IE7-6*/
    height: 300px; 
    z-index: 999;
    padding: 5px;
    text-align: center;
} 
.contentPopup ul{ 
	padding-left: 20px;
	list-style-type: none;
	margin-bottom: 10px;
	text-align: left;
}
.contentPopup ul a{ 
	color: #558DC6;
}


.titleModalPopup {
	overflow: hidden;
	zoom: 1;/*IE6*/
	margin-bottom: 20px;
}
.titleModalPopup h2{
	float: left;
	color: #2B3C6E;
	font-size: 14px;
}
.titleModalPopup .closeModalPopup{
	float: right;
	cursor: pointer;
	font-weight: bold;
}

.contentModalPopup {
    overflow: auto;
	overflow-x:hidden;/*IE7-6*/    
    height: 250px; 
}


/* @end */

.loading {            
	background-image: url('../img/loading.gif');
	background-repeat: no-repeat;
}

.loadingAutoComplete
{
    background-image: url('../images/preloader.gif');
    background-position: right;
    background-repeat: no-repeat;
}
.OkMessage {
	width: 300px;
	height: 100px;
	background-color: #ffffff;
	padding: 15px;
	text-align: center;
}

.precarga {
	color: #aaa;
	font-weight: bold;
}


/****fin styles*****/
/****print*********/
@media print {
	.hidebutton {
		display:none;
	}
	
	#page {
		width: auto;
	}
	#header {
		width: auto;
	}
	#personas {
		width: 98%;
	}
	.bloque {
		zoom: 1;
	}
}

body {
	font-family: Trebuchet MS;
}
.bloque {
	/*border: 1px solid #999;*/
	padding: 10px;
}
.separador {
	border: 10px solid #000;
	margin-top: 5px;
	margin-bottom: 5px;
}
body,
#site-name h1,
#fechas,
#personas .evaluador,
h3,
p.descripcion,
div.definicion-grado,
table.GridView tr.HeaderStyle th{
	color: #000;
}

.toUpperCase{
	text-transform: uppercase;
}
.definicion-grado li {
	margin-bottom: 5px;
}



/*cabecera*/
#header {
	_height: 250px;
	background: none;	
}
#headerBG {
	border-top: 215px solid #ccc;
	border-bottom: 46px solid #999;
}
#personas .evaluador, 
#personas .evaluado {
	width: 98%;
}	
#foto {
	border:1px solid #999;
}
#calificacion {
	background: none;
}
#calificacion h2 {
	color: #000;
}
#acciones {
	position: absolute;
	bottom: 10px;
	right: 10px;
}


/*grilla*/
.GridView {
	font-family: Trebuchet MS;	
}
.GridView tr.HeaderStyle th {
	font-size: 14px;
}
.GridView td {
	padding: 15px;
}
.GridView p {
	margin: 0;
	font-size: 11px;
	line-height: 1.2;
}

/*comentarios*/
#comentarios .item {
	margin-bottom: 20px;
}
#comentarios .descripcion{
	margin-bottom: 10px;
}
#comentarios .textwrapper {
	border: 1px solid #999;
}
#comentarios .textwrapper .textwrapperInner{
	padding: 5px;
}
/****fin de print*********/
/*******reset*****************/
/* http://meyerweb.com/eric/tools/css/reset/ */
/* v1.0 | 20080212 */

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, font, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
	margin: 0;
	padding: 0;
	border: 0;
	outline: 0;
	/*vertical-align: baseline;*/
	background: transparent;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

/* remember to define focus styles! */
:focus {
	outline: 0;
}

/* remember to highlight inserts somehow! */
ins {
	text-decoration: none;
}
del {
	text-decoration: line-through;
}

/* tables still need 'cellspacing="0"' in the markup */
table {
	border-collapse: collapse;
	border-spacing: 0;
}

/*******reset*****************/






    </style>

   <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           //quitar bordes laterales
           $('.GridView tr th:first').css('border-left', '1px solid #fff');
           $('.GridView tr th:last').css('border-right', '1px solid #fff');
           $('.GridView tr').each(function () {
               $(this).find('td:first').css('border-left', '1px solid #fff');
               $(this).find('td:last').css('border-right', '1px solid #fff');
           });
       });
   </script>   
</head>
<body>



   <div id="page">
      <div id="page-inner">

         <table width="100%"><tr><td>
         <form id="form1" runat="server">
         <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         
         <br />
         <table width="100%" style=" border: 1pt solid #999999"><tr><td>
         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" 
                 ondatabinding="FVC_DataBinding" ondatabound="FVC_DataBound">
            <ItemTemplate>
               <div id="header">
                  <!--<div id="headerBG"></div>-->
                  <div id="header-inner">
                     
                     <div id="logo-title">
                        <table>
                           <td>
                        <div id="site-name">
                           <h1>Evaluación de Desempeño <span><asp:Literal ID="TipoFormularioLabel" runat="server" Text='<%# Bind("TipoFormulario") %>' /></span></h1>
                        </div>
                        </td>
                        <td>
                        <div id="logo" style=" text-align:right;">
                           <img id="logo-image" alt="Logo Johnson " src="http://Word/logoprint.jpg"  />
                          
                           </div>
                           </td>
                        </table>
                     </div>
                     
                     <div id="datos">
                        
                        <ul id="fechas">
                           <li>Período de Evaluación: <asp:Literal ID="Label1" runat="server" Text='<%# Bind("Periodo") %>' /></li>
                           <li>Fecha de Inicio del PMP: <asp:Literal ID="EvaluacionFechaLabel" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>' /></li>
                        </ul>
                        
                        <!--datos de Evaluador y Evaluado-->
                        <table width="100%">
                           <tr>
                              <td valign="top">
                                 <div id="personas">
                                    <div id="personas-inner">
                                       <table width="100%">
                                          <tr>
                                             <td>
                                                <div class="evaluador">
                                                   <h2>Evaluador</h2>
                                                   <table>
                                                      <tr>
                                                         <td><strong>Nombre y Apellido: </strong><asp:Literal ID="EvaluadorNombreLabel" runat="server" Text='<%# Bind("EvaluadorNombre") %>' /></td>
                                                         <td><strong>Legajo: </strong><asp:Literal ID="EvaluadorLegajoLabel" runat="server" Text='<%# Bind("EvaluadorLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Sector: </strong><asp:Literal ID="Literal1" runat="server" Text='<%# Bind("EvaluadorArea") %>' /></td>
                                                         <td><strong>Cargo: </strong><asp:Literal ID="Literal2" runat="server" Text='<%# Bind("EvaluadorCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                             
                                             <td>
                                                <div class="evaluado">
                                                   <h2>Evaluado</h2>
                                                   <table>
                                                      <tr>
                                                         <td><strong>Nombre y Apellido: </strong><asp:Literal ID="Label2" runat="server" Text='<%# Bind("EvaluadoNombre") %>' /></td>
                                                         <td><strong>Legajo: </strong><asp:Literal ID="EvaluadoLegajoLabel" runat="server" Text='<%# Bind("EvaluadoLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Sector: </strong><asp:Literal ID="EvaluadoSectorLabel" runat="server" Text='<%# Bind("EvaluadoArea") %>' /></td>
                                                         <td><strong>Cargo: </strong><asp:Literal ID="Literal3" runat="server" Text='<%# Bind("EvaluadoCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>                              
                              </td>
                              <td valign="top">
                              <div id="DivFotoUser">
                               
                                   <asp:Literal ID="LiteralFoto" runat="server" Text='<%# Bind("EvaluadoFoto") %>'></asp:Literal>                                    
                                    
                              </div>                         
                              </td>	
                           </tr>
                        </table>
                        
                     </div><!--/datos-->
                     
                    
                     
                  </div><!--/header-inner-->
               </div><!--/header-->
               

            </ItemTemplate>
         </asp:FormView>         
         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         </td></tr></table>
         <br />

         <table width="100%" style=" border: 1pt solid #999999"><tr><td>
         <div class="bloque">
            <h3>Competencias</h3>
            <asp:FormView ID="FormViewPeriodo" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
               Width="100%">
               <ItemTemplate>
                  <p class="descripcion" style="margin-bottom: 15px">
                     Seleccionar en que Grado considera que posee cada competencia definida, basándose
                     en el desempeño del evaluado en el período
                     <asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                     -
                     <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />
                  </p>
               </ItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetPeriodo" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
            <div class="definicion-grado">
               <h4>Definición del Grado</h4>
               <br />
               <ul>
                  <li><strong>Muy Bueno:</strong> El desempeño excede claramente los requerimientos para
                     la tarea.</li>
                  <li><strong>Bueno:</strong> El desempeño es acorde a los requerimientos de la tarea.</li>
                  <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Posee un buen desempeño, pero sin alcanzar el nivel esperado.</li>
                  <li><strong>No Satisfactorio:</strong> El desempeño no satisface los requerimientos
                     mínimos para las tareas.</li>
               </ul>
            </div>
            <asp:GridView ID="GridViewCompetencias" runat="server" AutoGenerateColumns="False"
               DataKeyNames="ItemEvaluacionID" DataSourceID="ObjectDataSourceCompetencias" BorderColor="#999999">
               <Columns>
                  <asp:TemplateField HeaderText="Competencias">
                     <ItemTemplate>
                        <div class="titulo">
                           <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        </div>
                        <div class="detalle">
                           <asp:Label ID="Label2" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>
                           <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </div>
                     </ItemTemplate>
                     <ItemStyle Width="40%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Calificación">
                     <ItemTemplate>
                     <asp:Label ID="LabelPonderacion" runat="server" Text='<%# GetResultado(Eval("Ponderacion")) %>'></asp:Label>
                        <%--<asp:Label ID="LabelResultado" runat="server" Text="Muy Bueno" />--%>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Center" CssClass="toUpperCase" Width="20%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                     <ItemTemplate>
                        <p>
                           <asp:Label ID="TextBoxFundamentacion" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion").ToString().Replace(Environment.NewLine,"<br />"),"N/C") %>'></asp:Label></p>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Left" Width="40%" />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                 DeleteMethod="Delete" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                     PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <br />
         </td></tr></table>
         <br />

         <table width="100%" style=" border: 1pt solid #999999"><tr><td>
         <div class="bloque">
            <h3>Objetivos</h3>
            <asp:FormView ID="FormView1" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
               Width="100%">
               <ItemTemplate>
                  <p class="descripcion" style="margin-bottom: 15px">
                     Seleccionar en que Grado considera que alcanzó los niveles requeridos, basándose
                     en el desempeño del evaluado en el período
                     <asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                     -
                     <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />
                  </p>
               </ItemTemplate>
            </asp:FormView>
            <div class="definicion-grado">
               <h4>Definición del Grado</h4>
               <br />
               <ul>
                  <li><strong>Muy Bueno:</strong> Los objetivos son cumplidos con naturalidad. Genera
                     una contribución individual que se enfoca en la mejora continua de las tareas y
                     prácticas. Propicia la autogestión. </li>
                  <li><strong>Bueno:</strong> Conoce y domina sus objetivos. Los trasmite a los demás
                     colaboradores y cada quien sabe que se espera de él y se esfuerza por lograrlo.</li>
                  <li><strong>Regular:</strong> Conoce y domina, pero no puede, a pesar de su esfuerzo,
                     transmitir a los colaboradores del equipo el requerimiento de manera cabal. Algunos
                     objetivos no son cumplidos. </li>
                  <li><strong>No Satisfactorio:</strong> No conoce o no domina íntegramente tanto lo
                     que se espera de su contribución como de su equipo. Hay diferencias notables entre
                     objetivos y desempeño. </li>
               </ul>
            </div>
            <asp:GridView ID="GridViewAspectos" runat="server" AutoGenerateColumns="False" DataKeyNames="ItemEvaluacionID"
               DataSourceID="ObjectDataSourceAspectos">
               <Columns>
                  <asp:TemplateField HeaderText="Objetivos">
                     <ItemTemplate>
                        <div class="titulo">
                           <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        </div>
                        <div class="detalle">
                           <asp:Label ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label><asp:HiddenField
                              ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </div>
                     </ItemTemplate>
                     <ItemStyle Width="40%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Calificación">
                     <ItemTemplate>
                        <%--<asp:Label ID="Label1" runat="server" Text="Muy Bueno" />--%>
                        <asp:Label ID="Label2" runat="server" Text='<%# GetResultado(Eval("Ponderacion")) %>'></asp:Label>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Center" CssClass="toUpperCase" Width="20%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                     <ItemTemplate>
                       <p>
                           <asp:Label ID="TextBoxFundamentacion" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion").ToString().Replace(Environment.NewLine,"<br />"),"N/C") %>'></asp:Label></p>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Left" Width="40%" />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAspectos" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                 DeleteMethod="Delete" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                     PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <br />
         </td></tr></table>
         <br />

         <table width="100%" style=" border: 1pt solid #999999"><tr><td>
         <div class="bloque" runat="server" id="AreasDeInteres">
            <h3>Áreas de interés del evaluado <span>(área a ser completada por el evaluado, opcional)</span></h3>
            <p class="descripcion" style="margin-bottom: 15px">
               Seleccione, tildando sobre el casillero ubicado en la primera columna, el o las áreas
               de interés y describa brevemente cual es el rol o las funciones que le interesaría
               desempeñar allí.</p>
            
            <asp:GridView ID="GVAO" runat="server" AutoGenerateColumns="False" DataKeyNames="AreaDeOperacionID"
               DataSourceID="ObjectDataSourceAreasDeOperaciones">
               <Columns>
                  <asp:BoundField DataField="Descripcion" HeaderText="Área  de Operaciones" SortExpression="Descripcion">
                     <ItemStyle Width="40%" Font-Bold="true" />
                  </asp:BoundField>
                  <asp:TemplateField HeaderText="Observaciones - Roles y Funciones">
                     <ItemTemplate>
                        <asp:Label ID="TextBoxObservacion" runat="server" Text='<%# Bind("Observacion") %>'></asp:Label>
                     </ItemTemplate>
                  </asp:TemplateField>
               </Columns>
                <EmptyDataTemplate>
                    Sin áreas de interés seleccionadas
                </EmptyDataTemplate>
            </asp:GridView>
            
            <asp:ObjectDataSource ID="ObjectDataSourceAreasDeOperaciones" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter" 
                 DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Activo" Type="Boolean" />
                    <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Activo" Type="Boolean" />
                    <asp:Parameter Name="PorDefecto" Type="Boolean" />
                    <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Activo" Type="Boolean" />
                    <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Activo" Type="Boolean" />
                    <asp:Parameter Name="PorDefecto" Type="Boolean" />
                </InsertParameters>
            </asp:ObjectDataSource>
         </div>
         <br />
         </td></tr></table>
         <br />

         <table width="100%" style=" border: 1pt solid #999999"><tr><td>
         <div class="bloque">
         <br />
            <asp:Repeater ID="RepeaterCalificacion" runat="server" DataSourceID="ObjectDataSourceCabecera">
                 <ItemTemplate>
                  <div id="calificacion">
                      <h2>Calificación General: <span id="SpanCalificacion"><asp:Literal ID="TextCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' /></span></h2>
                  </div>
                 </ItemTemplate>
            </asp:Repeater>
         </div>
         <br />
         </td></tr></table>
         <br />

         <table width="100%" style=" border: 1pt solid #999999"><tr><td>       
         <div class="bloque">
            <h3>Comentarios</h3>
            <asp:FormView ID="FormViewFormulario" runat="server" DataKeyNames="Legajo,PeriodoID,TipoFormularioID"
               DataSourceID="ObjectDataSourceFormulario" Width="100%">
               <ItemTemplate>
                  <div id="comentarios">
                     <div class="item">
                        <p class="descripcion">
                           Evaluador(Aspectos acerca del evaluado que a su juicio merezcan ser destacados,
                           capacitación recomendada, etc. – No obligatorio.)</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioJT" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeTurno"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <p class="descripcion">
                           Evaluado(Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada
                           en su rol actual, su relación con sus compañeros y supervisores. Como también, observaciones
                           útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la
                           Compañía.)</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioEv" runat="server" Text='<%# DefaultVal(Eval("ComentarioEvaluado"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <p class="descripcion">
                           Auditor - Observaciones adicionales</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioJP" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </ItemTemplate>
               <InsertItemTemplate>
                  <table class="GridView">
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Evaluador</div>
                           <div class="detalle">
                              Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación
                              recomendada, etc. – No obligatorio.
                           </div>
                        </td>
                        <td>
                           <asp:TextBox ID="TextBoxComentarioJT" runat="server" TextMode="MultiLine" Text='<%# DefaultVal(Eval("ComentarioJefeTurno"),"S/C") %>'
                              Width="93%"></asp:TextBox>
                        </td>
                        <%-- <td style="width:1%">
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentarioJT" runat="server" 
                                       Display="Dynamic" ControlToValidate="TextBoxComentarioJT" ErrorMessage="Agregar Comentario de Jefe de Turno" 
                                       ValidationGroup="EnviarEvaluado">*</asp:RequiredFieldValidator>
                               </td>--%>
                     </tr>
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Evaluado</div>
                           <div class="detalle" >
                              Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su
                              rol actual, su relación con sus compañeros y supervisores. Como también, observaciones
                              útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la
                              Compañía.
                           </div>
                        </td>
                        <td>
                           <asp:TextBox Enabled="false" ID="TextBoxComentarioEv" runat="server" TextMode="MultiLine"
                              Width="93%" Text='<%# DefaultVal(Eval("ComentarioEvaluado"),"S/C") %>'></asp:TextBox>
                        </td>
                        <%--<td style="width:1%">
                              </td>--%>
                     </tr>
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Auditor</div>
                           <div class="detalle">
                              Observaciones adicionales</div>
                        </td>
                        <td>
                           <asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta"),"S/C") %>'
                              runat="server" TextMode="MultiLine" Width="93%"></asp:TextBox>
                        </td>                       
                     </tr>
                  </table>
               </InsertItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ObjectDataSourceFormulario" runat="server" DeleteMethod="Delete"
               InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID"
               TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter"
               UpdateMethod="Update">
               <DeleteParameters>
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
               </DeleteParameters>
               <UpdateParameters>
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="FAlta" Type="DateTime" />
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
               </UpdateParameters>
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
               <InsertParameters>
                  <asp:Parameter Name="Legajo" Type="Int32" />
                  <asp:Parameter Name="PeriodoID" Type="Int32" />
                  <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
               </InsertParameters>
            </asp:ObjectDataSource>
         </div>
         <br />
         </td></tr></table>
         <br />
         </form>
         </td></tr></table>
      
      </div><!--/page-inner-->
   </div><!--/page-->
   
   

   
</body>
</html>

