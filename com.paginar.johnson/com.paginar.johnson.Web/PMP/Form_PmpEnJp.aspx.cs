﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.membership;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;


namespace com.paginar.johnson.Web.PMP
{
    public partial class Form_PmpEnJp : PageBase
    {
        string ScriptJs = string.Empty;
        private bool SoloLectura
        {
            get { return (bool)ViewState["SoloLectura"]; }
            set { ViewState["SoloLectura"] = value; }
        }
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }

        protected string DefaultVal(object val, string Default)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }
        void omb_OkButtonPressed(object sender, EventArgs args)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }
        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();

            return ((bool)f.ValidarPeriodoCarga(periodoid, DateTime.Now));
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);
            Usuarios usercontroller = new Usuarios();
            User Usuario = null;
            if (!Page.IsPostBack)
            {
                HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
                HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
                HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;



            }
            DisableControls(GridViewAspectos, false);
            DisableControls(GridViewCompetencias, false);
            DisableControls(GVAO, false);
            try
            {
                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                int LegajoLog = Usuario.GetLegajo();
                int? LegajoAuditor = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 3);
                int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
                SoloLectura = false;// (!(PasoActualID == 3)) || (!IsPeriodoAbierto);
                bool PuedeEditar = (LegajoLog == LegajoAuditor && PasoActualID == 3);
                if (SoloLectura)
                {


                    DisableControls(FormViewFormulario, false);
                    //ButtonAprobarEvaluacion.Visible = false;
                    //ButtonDevolverJT.Visible = false;
                    upPnlPage.Visible = false;
                    ButtonImprimir.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldlegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));

                }//si no es de solo lectura actualizo
                else
                {
                    if (!PuedeEditar && !Page.IsPostBack)
                        Response.Redirect("~/UnauthorizedAccess.aspx");

                    HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
                    BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx');");
                   
                        
                }
            }
            catch (Exception)
            {

                //throw;
            }
            ;
        }
        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);
            //return true;

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void GridViewCompetencias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblFundametnacion = e.Row.FindControl("lblFundamentacion") as Label;
                if (lblFundametnacion.Text == string.Empty) lblFundametnacion.Text = "N/C";
            }
        }


        private void GrabarAreasOperaciones()
        {
            foreach (GridViewRow fila in GVAO.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int AreaDeOperacionID = 0;
                    string Observacion = string.Empty;
                    CheckBox CheckBoxAreaDeOperacion = fila.FindControl("CheckBoxAreaDeOperacion") as CheckBox;
                    if (CheckBoxAreaDeOperacion.Checked)
                    {
                        HiddenField HiddenFieldAreaDeOperacionID = fila.FindControl("HiddenFieldAreaDeOperacionID") as HiddenField;
                        AreaDeOperacionID = int.Parse(HiddenFieldAreaDeOperacionID.Value);
                        TextBox TextBoxObservacion = fila.FindControl("TextBoxObservacion") as TextBox;
                        Observacion = TextBoxObservacion.Text;
                        FC.SaveAreaOperacion(AreaDeOperacionID, Observacion);
                    }
                }

            }
        }
        private void GuardarItems(GridView GridItems)
        {
            foreach (GridViewRow fila in GridItems.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    HiddenField HFID = fila.FindControl("HFID") as HiddenField;
                    ItemEvaluacionID = int.Parse(HFID.Value);

                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if (RBMB.Checked)
                        Ponderacion = 4;
                    if (RBB.Checked)
                        Ponderacion = 3;
                    if (RBR.Checked)
                        Ponderacion = 2;
                    if (RBNS.Checked)
                        Ponderacion = 1;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }


        protected void FormViewFormulario_DataBound(object sender, EventArgs e)
        {
            if (FormViewFormulario.DataItemCount == 0)
                FormViewFormulario.ChangeMode(FormViewMode.Insert);
            else
                FormViewFormulario.ChangeMode(FormViewMode.ReadOnly);
            ButtonAprobarEvaluacion.OnClientClick += "return ClickEnvio();";
            //string.Format("return CheckComentario('{0}');", "ctl00_CPH1_FormViewFormulario_TextBoxComentarioJP");
            ButtonDevolverJT.OnClientClick += string.Format("return CheckComentario('{0}');", "ctl00_CPH1_FormViewFormulario_TextBoxComentarioJP");
        }
       
        protected void GridViewAspectos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblFundametnacion = e.Row.FindControl("lblFundamentacion") as Label;
                if (lblFundametnacion.Text == string.Empty) lblFundametnacion.Text = "N/C";


            }
        }

        protected void CustomValidatorCompetencias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewCompetencias);
        }


        protected void CustomValidatorAspectos_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewAspectos);
        }

        private bool ValidarGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                    {
                        resultado = false;
                        break;
                    }

                }

            }
            return resultado;
        }

        private void DisableControls(Control c, bool habilita)
        {

            if ((c is WebControl) && ((c.GetType().Name == "RadioButton") || (c.GetType().Name == "CheckBox") || (c.GetType().Name == "TextBox")) && (c.GetType().Name != "GridView"))
                ((WebControl)c).Enabled = habilita;

            foreach (Control child in c.Controls)
                DisableControls(child, habilita);
        }


        protected void ButtonAprobarEvaluacion_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
                LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
                Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
                LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
                TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioJefeDePlanta(TextBoxComentarioJP.Text);
                try
                {
                    FC.GrabarJP();
                    FC.SaveHistorial(4, PeriodoID, legajo, TipoFormularioID);
                    NotificacionesController nc = new NotificacionesController();
                    FormulariosDS.CabeceraFormularioDataTable Cabe = FC.GetCabecera(int.Parse(PeriodoID.ToString()), int.Parse(legajo.ToString()), int.Parse(TipoFormularioID.ToString()), null);
                    ControllerUsuarios CU = new ControllerUsuarios();
                    DSUsuarios.UsuarioDataTable DTU = CU.GetUsuariosByLegajo(legajo);
                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    int legajojp = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 3);
                    int legajoEvaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);
                    string mailEvaluador = UE.GetMail(legajoEvaluador);
                    
                    string NombreEvaluador = Cabe.Rows[0].ItemArray[12].ToString();
                    string MailOperario = DTU.Rows[0].ItemArray[4].ToString();
                    string URLPMP;
                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSIMP&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];
                    nc.AprobadaPMPOEvaluado(MailOperario, UE.GetUsuarioNombre(legajojp), URLPMP);
                    nc.AprobadaPMPOEvaluador(UE.GetUsuarioNombre(legajojp), UE.GetUsuarioNombre(legajo), mailEvaluador, URLPMP);


                    EjecutarScript(string.Format("document.body.onbeforeunload=''; PreguntaImprimirEvaluacionV2({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    omb.ShowMessage("La Evaluación fue Aprobada");
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        protected void ButtonDevolverJT_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
                LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
                Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
                LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
                TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioJefeDePlanta(TextBoxComentarioJP.Text);
                try
                {
                    FC.GrabarJP();
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UELegajoEvaluador = new UsuariosEvaluacion(Page.User.Identity.Name);
                    UsuariosEvaluacion UELegajoEvaluacion = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UELegajoEvaluador.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);
                    int legajojefe = UELegajoEvaluador.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 3);

                    string URLPMP;
                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSE2&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];


                    string envio = n.devuelveJefeTurno(UELegajoEvaluador.GetMail(legajoevaluador), UELegajoEvaluador.GetUsuarioNombre(legajojefe), UELegajoEvaluacion.GetUsuarioNombre(legajo), URLPMP);


                    EjecutarScript(string.Format("document.body.onbeforeunload=''; PreguntaImprimirEvaluacionV2({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        //protected void ButtonImprimir_Click(object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        int legajo = 0;
        //        int PeriodoID = 0;
        //        int TipoFormularioID = 0;
        //        if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
        //        {
        //            legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
        //        }
        //        if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
        //        {
        //            PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
        //        }
        //        if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
        //        {
        //            TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
        //        }

        //        Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
        //        LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
        //        Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
        //        LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
        //        TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
        //        FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
        //        FC.SetComentarioJefeDePlanta(TextBoxComentarioJP.Text);
        //        try
        //        {                  
        //            FC.GrabarJP();
        //            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
                   
        //        }
        //        catch (Exception)
        //        {

        //            throw;
        //        }

        //    }
        //}

        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
            LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
            Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
            LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
            TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

              

                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionV2({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            
        }

        protected void ButtonExportar_Click(object sender, EventArgs e)
        {
            ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ExportarEvaluacionPMPO({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            //EjecutarScript();

        }


        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {


                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
                LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
                Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
                LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
                TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioJefeDePlanta(TextBoxComentarioJP.Text);
                FC.GrabarJP();
                FC.SaveHistorial(3, PeriodoID, legajo, TipoFormularioID);
                GuardadoPrevio();
                omb.ShowMessage("La Evaluación fue guardada satisfactoriamente");

        }

        protected void GuardadoPrevio()
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            FC.GrabarJP();
            FC.SaveHistorial(3, PeriodoID, legajo, TipoFormularioID);
        }

    }
}