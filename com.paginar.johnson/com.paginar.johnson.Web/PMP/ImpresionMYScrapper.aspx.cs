﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.PMP
{
    public partial class ImpresionMYScrapper : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }
        }
        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);

        }

        protected string GetResultado(object Ponderacion)
        {
            string PonderacionAux = Ponderacion.ToString();

            switch (PonderacionAux)
            {
                case "1":
                    return "Fortaleza";
                    break;
                case "2":
                    return "Area de Desarrollo";
                    break;
                case "3":
                    return "Bueno";
                    break;
                case "4":
                    return "Muy Bueno";
                    break;
                default:
                    return string.Empty;
                    break;
            }


        }

        protected string DefaultVal(object val, string DefaultValue)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return DefaultValue;
            return (val.ToString());

        }
    }
}