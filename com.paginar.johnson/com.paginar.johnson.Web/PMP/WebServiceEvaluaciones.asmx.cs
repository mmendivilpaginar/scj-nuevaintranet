﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP
{
    /// <summary>
    /// Summary description for WebServiceEvaluaciones
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class WebServiceEvaluaciones : System.Web.Services.WebService
    {

        public WebServiceEvaluaciones()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        public class Curso
        {

            public int CursoID;
            public string CursoDescripcion;
            public string DebilidadID;

            public Curso(int CursoID, string CursoDescripcion, string DebilidadID)
                {
                this.CursoID = CursoID;
                this.CursoDescripcion = CursoDescripcion;
                this.DebilidadID = DebilidadID;
                }
           
        }

        [WebMethod]
        public string GetCalificacionByPuntos(int PuntosCompetencias, int PuntosObjetivos, int TipoFormularioID)
        {
            FormulariosController FC = new FormulariosController();
            return FC.GetCalificacionByPuntos(PuntosCompetencias,PuntosObjetivos, TipoFormularioID);
            //return "Hello World";
        }


        [WebMethod]
        public List <Curso> GetCursosByDebilidad(int TipoFormularioID, int AreaID, string Debilidad)
        {

            FormulariosFYController FFYC = new FormulariosFYController();
            List <Curso> Cursos = new List<Curso>();

            FormulariosPMPFYDS.frmPMPCursosDataTable CursosDT = new FormulariosPMPFYDS.frmPMPCursosDataTable();
            CursosDT = FFYC.GetCursosByDebilidadID(TipoFormularioID, AreaID, Debilidad);

            foreach ( FormulariosPMPFYDS.frmPMPCursosRow CursoRow in CursosDT.Rows)
            {

                Cursos.Add(new Curso(int.Parse(CursoRow["CursoID"].ToString()), CursoRow["CursoDescripcion"].ToString(), CursoRow["DebilidadID"].ToString()));
            }
            
            return Cursos;
           
        }

        
    }
}
