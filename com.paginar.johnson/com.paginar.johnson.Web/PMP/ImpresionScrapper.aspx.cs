﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace com.paginar.johnson.Web.PMP
{
    public partial class ImpresionScrapper : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }

            if (!string.IsNullOrEmpty(Request.QueryString["ImprimeAreas"]))
            {
                if (Request.QueryString["ImprimeAreas"].ToString() == "True")
                    AreasDeInteres.Visible = true;
                else
                    AreasDeInteres.Visible = false;
            }

           string imageUrlLogo= Request.ServerVariables.Get("SERVERNAME");

           
        }

        public string ImageToBase64(MemoryStream ms,
          System.Drawing.Imaging.ImageFormat format)
        {

            byte[] imageBytes = ms.ToArray();


            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;

        }

        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);

        }

        protected string GetResultado(object Ponderacion)
        {
            string PonderacionAux = Ponderacion.ToString();

            switch (PonderacionAux)
            {
                case "1":
                    return "No Satisfactorio";
                    break;
                case "2":
                    return "Regular";
                    break;
                case "3":
                    return "Bueno";
                    break;
                case "4":
                    return "Muy Bueno";
                    break;
                default:
                    return string.Empty;
                    break;
            }


        }

        protected string DefaultVal(object val, string DefaultValue)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return DefaultValue;
            return (val.ToString());

        }

        /// <summary>
        /// Convierte un recurso de imagen en formato de stream.
        /// </summary>
        /// <param name="Image_Url">La url completa que lleva a la imagen</param>
        /// <returns></returns>
        private MemoryStream ConvertImage_FromUrlToStream(string Image_Url)
        {

            string fileName = Image_Url.Substring(Image_Url.LastIndexOf("/") + 1);
            string folderBaseImage = "/images/personas/";
            string FullFileNameRelative = folderBaseImage + fileName;
            string FullFileNameAbsolute = this.Server.MapPath(FullFileNameRelative);

            if (!File.Exists(FullFileNameAbsolute))
            {
                return new MemoryStream();
            }
            else
            {
                byte[] imageInBytesFormat = File.ReadAllBytes(FullFileNameAbsolute);

                using (MemoryStream imageInStreamFormat = new MemoryStream(imageInBytesFormat, 0, imageInBytesFormat.Length))
                {
                    imageInStreamFormat.Write(imageInBytesFormat, 0, imageInBytesFormat.Length);
                    return imageInStreamFormat;
                }
            }


        }

        protected void FVC_DataBinding(object sender, EventArgs e)
        {
            ///imagen en base64
          
            
        }

        protected void FVC_DataBound(object sender, EventArgs e)
        {
            //HtmlContainerControl divImgUsr = (HtmlContainerControl)FVC.FindControl("foto");
            // System.Web.UI.WebControls.Image imgUser = (System.Web.UI.WebControls.Image)FVC.FindControl("LiteralFoto");
            Literal ImgUser = (Literal)FVC.FindControl("LiteralFoto");
            MemoryStream imageInStreamFormat = ConvertImage_FromUrlToStream(ImgUser.Text);
            string imagUserBase64 = ImageToBase64(imageInStreamFormat, System.Drawing.Imaging.ImageFormat.Gif);
            //ImgUser.Text = "<DIVFOTO" + imagUserBase64 + "/>";
            ImgUser.Text = "<img id=\"imgFoto\"  src=\"data:image/gif;base64," + imagUserBase64 + "\" />";
            //divImgUsr.InnerHtml = "<img width=70 height=70 ID=\"imagen-usr\" style=\"width:70pt;height:70pt;visibility:visible;mso-wrap-styl=e:square\" src=\"http://Word/user.gif\">";

            ///*****/////
            ///

            

        }
    }
}