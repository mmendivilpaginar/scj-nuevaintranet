﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EvaluacionError.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.EvaluacionError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Página no disponible</title>
       <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/backoffice.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="error404">
        <div id="logo-error"></div>

    <form id="form1" class="form-error" runat="server">
          <div id="main-error">
                <div id="main-inner">
				    <div id="content-error">
					    <div id="content-inner-error">
                           <!--<div id="header-error"></div>-->
					       <div>
                            <div id="icon-error"></div>
					           <h1 style="font-size: large">
					                <asp:Label ID="Label1" runat="server" Text="No existe la evaluación a la que intenta acceder."></asp:Label>
					           </h1>
					       </div>
					    </div>
				    </div>

                </div><!--/main-inner-->
            </div><!--/main-->

            <div id="footer-error">
			    <div style="text-align:center">
			        <asp:Button ID="btnVolver" runat="server" Text="Aceptar" onclick="btnVolver_Click" />
			    </div>
		    </div>

		</form>
    </div>
</body>
</html>

