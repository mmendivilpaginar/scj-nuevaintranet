﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="ExportarWordPMPO.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.ExportarWordPMPO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <link rel="stylesheet" type="text/css" href="css/styles.css" />
   <link rel="stylesheet" type="text/css" href="css/print.css" />
   <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           //quitar bordes laterales
           $('.GridView tr th:first').css('border-left', '1px solid #fff');
           $('.GridView tr th:last').css('border-right', '1px solid #fff');
           $('.GridView tr').each(function () {
               $(this).find('td:first').css('border-left', '1px solid #fff');
               $(this).find('td:last').css('border-right', '1px solid #fff');
           });
       });
   </script>   
 
 <script type="text/javascript">
     function Imprimir() {
         var bot = document.getElementById('print');
         bot.style.display = 'none';
         window.print();
         bot.style.display = 'block';
     }

    </script>
   
</head>
<body>


<div id="export" runat="server">
   <div id="page">
      <div id="page-inner">

         <table width="100%">
         <tr>
         <td>
         <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="false" runat="server">
         </asp:ScriptManager>
         <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         
         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" BorderStyle="Dotted">
            <ItemTemplate>
               <div id="header1">
                  <!--<div id="headerBG"></div>-->
                  <div id="header-inner">
                     <div id="logo-title">
                      <table width="100%"><tr>
                      <td>
                      <div id="site-name">
                           <h1>Evaluación de Desempeño <br /><span><asp:Literal ID="TipoFormularioLabel" runat="server" Text='<%# Bind("TipoFormulario") %>' /></span></h1>
                        </div>
                      </td>
                      <td>
                      <div id="logo" runat="server">
                           <img id="logo-image" alt="Logo Johnson " src="img/logoprint.jpg"/>
                         </div>
                      </td>
                      </tr>
                      </table>                                                
                     </div>
                     
                     <div id="datos">
                        <!--Fechas-->
                        <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td><strong>Período de Evaluación:</strong> <asp:Literal ID="Label1" runat="server" Text='<%# Bind("Periodo") %>' /></td>
                                <td>&nbsp;</td>
                                <td><strong>Fecha de Inicio del PMP:</strong> <asp:Literal ID="EvaluacionFechaLabel" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>' /></td>
                            </tr>
                        </table>
                        <br /><br />
                        <!--datos de Evaluador y Evaluado-->
                        
                        <table width="100%" style="border:1px solid #ccc;">
                           <tr>
                              <td valign="top">
                                 <div id="personas1">
                                    <div id="personas-inner1">
                                       <table width="100%" cellpadding="1" cellspacing="1">
                                          <tr>
                                             <td>
                                                <div class="evaluador1">
                                                   <h2>Evaluador</h2>
                                                   <table >
                                                      <tr>
                                                         <td valign="top"><strong>Nombre y Apellido: </strong><asp:Literal ID="EvaluadorNombreLabel" runat="server" Text='<%# Bind("EvaluadorNombre") %>' /></td>
                                                         <td valign="top"><strong>Legajo: </strong><asp:Literal ID="EvaluadorLegajoLabel" runat="server" Text='<%# Bind("EvaluadorLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top"><strong>Área: </strong><asp:Literal ID="Literal1" runat="server" Text='<%# Bind("EvaluadorArea") %>' /></td>
                                                         <td valign="top"><strong>Cargo: </strong><asp:Literal ID="Literal2" runat="server" Text='<%# Bind("EvaluadorCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                             
                                             <td>
                                                <div class="evaluado1">
                                                   <h2>Evaluado</h2>
                                                   <table>
                                                      <tr>
                                                         <td valign="top"><strong>Nombre y Apellido: </strong><asp:Literal ID="Label2" runat="server" Text='<%# Bind("EvaluadoNombre") %>' /></td>
                                                         <td valign="top"><strong>Legajo: </strong><asp:Literal ID="EvaluadoLegajoLabel" runat="server" Text='<%# Bind("EvaluadoLegajoReal") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top"><strong>Área: </strong><asp:Literal ID="EvaluadoSectorLabel" runat="server" Text='<%# Bind("EvaluadoArea") %>' /></td>
                                                         <td valign="top"><strong>Cargo: </strong><asp:Literal ID="Literal3" runat="server" Text='<%# Bind("EvaluadoCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>                              
                              </td>
                              <td valign="top">
                                 <div id="foto" runat="server" style="width:70px;height:70px">
                                    <asp:Image ID="Image1" Width="70px" Height="70px" ToolTip='<%# Bind("EvaluadoNombre") %>' AlternateText='<%# Eval("EvaluadoNombre") %>' ImageUrl='<%# (Eval("EvaluadoFoto", GetUrl("{0}")).Replace("~/", "")).Replace("PMP/","") %>' runat="server" />
                                 </div>                              
                              </td>
                           </tr>
                        </table>

                        <div id="calificacion1">
                           <%--<h2><span id="SpanCalificacion"><asp:Literal ID="TextCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' /></span></h2>--%>
                        </div>
                     </div><!--/datos-->
                     <div style="height:45px">
                     <div id="print" style="text-align: right; padding-top:15px; ">
                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                         <ContentTemplate>
                         <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                             Text="Exportar" />
                             &nbsp;<asp:Button ID="btnImprimir" runat="server" 
                             OnClientClick="Imprimir();" Text="Imprimir" onclick="btnImprimir_Click" />
                             <%--<input type="button" value="Imprimir" onclick="Imprimir()" class="hidebutton" />--%>
                         &nbsp;<input type="button" value="Cerrar" onclick="window.close()" class="hidebutton" />

                         </ContentTemplate>
                             <Triggers>
                                 <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                                 <asp:AsyncPostBackTrigger ControlID="btnImprimir" EventName="Click" />
                             </Triggers>
                         </asp:UpdatePanel>

                     </div>
                     </div>
                  </div><!--/header-inner-->
               </div><!--/header-->
               
               <!--INICIO - ModalPopup Evaluaciones anteriores-->
               <asp:Panel ID="PanelFA" CssClass="contentPopup" Style="display: none" runat="server">
                  <asp:Panel CssClass="titleModalPopup" ID="titleBarEvalAnt" runat="server">
                     <h2>Evaluaciones anteriores</h2>
                     <span id="btnOkay" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <ul>
                        <asp:Repeater ID="RFA" runat="server" DataSourceID="ObjectDataSourceFormulariosAnteriores">
                           <ItemTemplate>
                              <li>
                                 <asp:LinkButton Text='<%# Convert.ToString(Eval("Fecha","{0:dd/MM/yyyy}"))+" "+Convert.ToString(Eval("Calificacion")) %>'
                                    ID="LinkButtonFormu" runat="server">LinkButton</asp:LinkButton></li>
                           </ItemTemplate>
                           <AlternatingItemTemplate>
                           </AlternatingItemTemplate>
                           <FooterTemplate>
                              <asp:Label ID="lblEmpty" Text="El usuario no cuenta con evaluaciones anteriores" runat="server"
                                 Visible='<%# bool.Parse((((Repeater)FVC.FindControl("RFA")).Items.Count==0).ToString())%>'>
                              </asp:Label>
                           </FooterTemplate>
                        </asp:Repeater>
                     </ul>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceFormulariosAnteriores" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorialByLegajo" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" PropertyName="Value"
                           Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Evaluaciones anteriores-->
               <asp:Panel CssClass="contentPopup" ID="PanelH" Style="display: none" runat="server">
                  <asp:Panel ID="Panel1" CssClass="titleModalPopup" runat="server">
                     <h2>
                        Historial</h2>
                     <span id="btnOkayHistorial" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <asp:GridView ID="GridViewHistorial" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceHistorial">
                        <Columns>
                           <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" SortExpression="Estado" />
                           <asp:BoundField DataField="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha"
                              SortExpression="Fecha" />
                           <asp:BoundField DataField="Usuario" HeaderText="Usuario" ReadOnly="True" SortExpression="Usuario" />
                        </Columns>
                        <EmptyDataTemplate>
                           <asp:Label ID="Label3" runat="server" Text="El formulario actual no tiene registros en el historial."></asp:Label>
                        </EmptyDataTemplate>
                     </asp:GridView>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceHistorial" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorial" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                           PropertyName="Value" Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Historial-->
            </ItemTemplate>
         </asp:FormView>
         
         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         
         <div class="bloque">
            <h3>Competencias</h3>
                     <asp:FormView ID="FormViewPeriodo" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
               Width="100%">
               <ItemTemplate>
                  <p class="descripcion">
                     Seleccionar en que Grado considera que posee las habilidades definidas, 
                     de acuerdo al desempeño del evaluado en el período
                     (<asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                     -
                     <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />)
                  </p>
               </ItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetPeriodo" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
            <div id="defGrado" class="definicion-grado" runat="server">
               <h4>Definición del Grado</h4>
               <br />
               <ul>
                  <li><strong>Muy Bueno:</strong> El desempeño excede claramente los requerimientos para
                     la tarea.</li>
                  <li><strong>Bueno:</strong> El desempeño es acorde a los requerimientos de la tarea.</li>
                  <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Posee un buen desempeño, pero sin alcanzar el nivel esperado.</li>
                  <li><strong>No Satisfactorio:</strong> El desempeño no satisface los requerimientos
                     mínimos para las tareas.</li>
               </ul>
            </div>
            <asp:GridView ID="GridViewCompetencias" runat="server" AutoGenerateColumns="False"
               DataKeyNames="ItemEvaluacionID" DataSourceID="ObjectDataSourceCompetencias" 
                 CssClass="GridView">
               <Columns>
                  <asp:TemplateField HeaderText="Competencias">
                     <ItemTemplate>
                        <div class="titulo">
                           <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        </div>
                        <div class="detalle">
                           <asp:Label ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>
                           <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </div>
                     </ItemTemplate>
                     <ItemStyle Width="40%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Calificación">
                     <ItemTemplate>
                     <asp:Label ID="Label2" runat="server" Text='<%# GetResultado(Eval("Ponderacion")) %>'></asp:Label>
                        <%--<asp:Label ID="LabelResultado" runat="server" Text="Muy Bueno" />--%>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Center" CssClass="toUpperCase" Width="20%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                     <ItemTemplate>
                        <p>
                           <asp:Label ID="TextBoxFundamentacion" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion").ToString().Replace(Environment.NewLine,"<br />"),"N/C") %>'></asp:Label></p>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Left" Width="40%" />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                 DeleteMethod="Delete" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                     PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <div class="separador">
         </div>
         <div class="bloque">
            <h3>Objetivos</h3>
            <asp:FormView ID="FormView1" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
               Width="100%">
               <ItemTemplate>
                  <p class="descripcion">
                     Seleccionar en que Grado considera que alcanzó los objetivos requeridos, 
                     en función del desempeño del evaluado en el período
                     (<asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                     -
                     <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />)
                  </p>
               </ItemTemplate>
            </asp:FormView>
            <div class="definicion-grado">
               <h4>Definición del Grado</h4>
               <br />
               <ul>
                  <li><strong>Muy Bueno:</strong> Los objetivos son cumplidos con naturalidad. Genera
                     una contribución individual que se enfoca en la mejora continua de las tareas y
                     prácticas. Propicia la autogestión. </li>
                  <li><strong>Bueno:</strong> Conoce y domina sus objetivos. Los trasmite a los demás
                     colaboradores y cada quien sabe que se espera de él y se esfuerza por lograrlo.</li>
                  <li><strong>Regular:</strong> Conoce y domina, pero no puede, a pesar de su esfuerzo,
                     transmitir a los colaboradores del equipo el requerimiento de manera cabal. Algunos
                     objetivos no son cumplidos. </li>
                  <li><strong>No Satisfactorio:</strong> No conoce o no domina íntegramente tanto lo
                     que se espera de su contribución como de su equipo. Hay diferencias notables entre
                     objetivos y desempeño. </li>
               </ul>
            </div>
            <asp:GridView ID="GridViewAspectos" runat="server" AutoGenerateColumns="False" DataKeyNames="ItemEvaluacionID"
               DataSourceID="ObjectDataSourceAspectos" CssClass="GridView">
               <Columns>
                  <asp:TemplateField HeaderText="Objetivos">
                     <ItemTemplate>
                        <div class="titulo">
                           <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        </div>
                        <div class="detalle">
                           <asp:Label ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label><asp:HiddenField
                              ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </div>
                     </ItemTemplate>
                     <ItemStyle Width="40%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Calificación">
                     <ItemTemplate>
                        <%--<asp:Label ID="Label1" runat="server" Text="Muy Bueno" />--%>
                        <asp:Label ID="Label2" runat="server" Text='<%# GetResultado(Eval("Ponderacion")) %>'></asp:Label>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Center" CssClass="toUpperCase" Width="20%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Fundamentación">
                     <ItemTemplate>
                       <p>
                           <asp:Label ID="TextBoxFundamentacion" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion").ToString().Replace(Environment.NewLine,"<br />"),"N/C") %>'></asp:Label></p>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Left" Width="40%" />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceAspectos" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                 DeleteMethod="Delete" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                     PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <div class="separador">
         </div>
         <div class="bloque">
            <h3>Áreas de interés del evaluado <span>(área a ser completada por el evaluado, opcional)</span></h3>
            <p class="descripcion">
               Seleccione, tildando sobre el casillero ubicado en la primera columna, el o las áreas
               de interés y describa brevemente cual es el rol o las funciones que le interesaría
               desempeñar allí.</p>
            
            <asp:GridView ID="GVAO" runat="server" AutoGenerateColumns="False" DataKeyNames="AreaDeOperacionID"
               DataSourceID="ObjectDataSourceAreasDeOperaciones" CssClass="GridView">
               <Columns>
                  <asp:BoundField DataField="Descripcion" HeaderText="Área  de Operaciones" SortExpression="Descripcion">
                     <ItemStyle Width="40%" Font-Bold="true" />
                  </asp:BoundField>
                  <asp:TemplateField HeaderText="Observaciones - Roles y Funciones">
                     <ItemTemplate>
                        <asp:Label ID="TextBoxObservacion" runat="server" Text='<%# Bind("Observacion") %>'></asp:Label>
                     </ItemTemplate>
                  </asp:TemplateField>
               </Columns>
                <EmptyDataTemplate>
                    Sin áreas de interés seleccionadas
                </EmptyDataTemplate>
            </asp:GridView>
            
            <asp:ObjectDataSource ID="ObjectDataSourceAreasDeOperaciones" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter" 
                 DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Activo" Type="Boolean" />
                    <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Activo" Type="Boolean" />
                    <asp:Parameter Name="PorDefecto" Type="Boolean" />
                    <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Activo" Type="Boolean" />
                    <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Activo" Type="Boolean" />
                    <asp:Parameter Name="PorDefecto" Type="Boolean" />
                </InsertParameters>
            </asp:ObjectDataSource>
         </div>
         <div class="separador">
         </div>
         <div class="bloque">
            <asp:Repeater ID="RepeaterCalificacion" runat="server" DataSourceID="ObjectDataSourceCabecera">
                 <ItemTemplate>
                  <div id="calificacion">
                      <h2>Calificación General: <span id="SpanCalificacion"><asp:Literal ID="TextCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' /></span></h2>
                  </div>
                 </ItemTemplate>
            </asp:Repeater>
         </div>
   
         <div class="separador">
         </div>
         <div id="bloComentarios" class="bloque" runat="server">
            <h3>
               Comentarios</h3>
            <asp:FormView ID="FormViewFormulario" runat="server" DataKeyNames="Legajo,PeriodoID,TipoFormularioID"
               DataSourceID="ObjectDataSourceFormulario" Width="100%">
               <ItemTemplate>
                  <div id="comentarios">
                     <div class="item">
                        <p class="descripcion">
                           Evaluador(Aspectos acerca del evaluado que a su juicio merezcan ser destacados,
                           capacitación recomendada, etc. – No obligatorio.)</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioJT" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeTurno"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <p class="descripcion">
                           Evaluado(Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada
                           en su rol actual, su relación con sus compañeros y supervisores. Como también, observaciones
                           útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la
                           Compañía.)</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioEv" runat="server" Text='<%# DefaultVal(Eval("ComentarioEvaluado"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <p class="descripcion">
                           Auditor - Observaciones adicionales</p>
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentarioJP" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </ItemTemplate>
               <InsertItemTemplate>
                  <table class="GridView">
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Evaluador</div>
                           <div class="detalle">
                              Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación
                              recomendada, etc. – No obligatorio.
                           </div>
                        </td>
                        <td>
                           <asp:TextBox ID="TextBoxComentarioJT" runat="server" TextMode="MultiLine" Text='<%# DefaultVal(Eval("ComentarioJefeTurno"),"S/C") %>'
                              Width="93%"></asp:TextBox>
                        </td>
                        <%-- <td style="width:1%">
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentarioJT" runat="server" 
                                       Display="Dynamic" ControlToValidate="TextBoxComentarioJT" ErrorMessage="Agregar Comentario de Jefe de Turno" 
                                       ValidationGroup="EnviarEvaluado">*</asp:RequiredFieldValidator>
                               </td>--%>
                     </tr>
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Evaluado</div>
                           <div class="detalle">
                              Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su
                              rol actual, su relación con sus compañeros y supervisores. Como también, observaciones
                              útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la
                              Compañía.
                           </div>
                        </td>
                        <td>
                           <asp:TextBox Enabled="false" ID="TextBoxComentarioEv" runat="server" TextMode="MultiLine"
                              Width="93%" Text='<%# DefaultVal(Eval("ComentarioEvaluado"),"S/C") %>'></asp:TextBox>
                        </td>
                        <%--<td style="width:1%">
                              </td>--%>
                     </tr>
                     <tr>
                        <td class="columnEmphasis">
                           <div class="titulo">
                              Auditor</div>
                           <div class="detalle">
                              Observaciones adicionales</div>
                        </td>
                        <td>
                           <asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta"),"S/C") %>'
                              runat="server" TextMode="MultiLine" Width="93%"></asp:TextBox>
                        </td>                       
                     </tr>
                  </table>
               </InsertItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="ObjectDataSourceFormulario" runat="server" DeleteMethod="Delete"
               InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID"
               TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter"
               UpdateMethod="Update">
               <DeleteParameters>
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
               </DeleteParameters>
               <UpdateParameters>
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="FAlta" Type="DateTime" />
                  <asp:Parameter Name="Original_Legajo" Type="Int32" />
                  <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
                  <asp:Parameter Name="Original_FAlta" Type="DateTime" />
               </UpdateParameters>
               <SelectParameters>
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
               <InsertParameters>
                  <asp:Parameter Name="Legajo" Type="Int32" />
                  <asp:Parameter Name="PeriodoID" Type="Int32" />
                  <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                  <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
                  <asp:Parameter Name="ComentarioEvaluado" Type="String" />
                  <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
               </InsertParameters>
            </asp:ObjectDataSource>
            <div style="float:right">
                <asp:Label ID="LabelRegistro" runat="server" Text=""></asp:Label>
            </div>
         </div>
         </form>
         </td></tr></table>
      
      </div><!--/page-inner-->
   </div><!--/page-->
   
</div>

   
</body>
</html>
