﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using System.Web.UI.HtmlControls;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP
{
	public partial class MP : System.Web.UI.MasterPage
	{
        int legajo = 0;
        int PeriodoID = 0;
        int TipoFormularioID = 0;
        protected FormulariosFYController FFY = new FormulariosFYController(); 
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            CargarValoresQueryString();

            //TextBox TBBacio = (TextBox)FVC.FindControl("lblEmpty");
            


            //FC.get
            //TBBacio.Visible = true;

        }

        private void CargarValoresQueryString()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"]))
            {
                HiddenFieldLegajoEvaluador.Value = Request.QueryString["LegajoEvaluador"];
            }

            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
        }

        protected void GridViewHistorial_DataBound(object sender, EventArgs e)
        {
            //Label lblPeridodoID = (Label)GridViewHistorial.Rows[GridViewHistorial.SelectedIndex].Cells[1].FindControl("lblPeridodoID"); 
        }


        protected string DameEstado(object val)
        {
            string Value = string.Empty;

            switch (val.ToString())
            {
                case "-1":
                    Value = "No iniciada";
                    break;
                case "1":
                case "2":
                case "3":
                    Value = "Abierta";
                    break;

                case "4":
                    Value = "Aprobada";
                    break;
                case "6":
                    Value = "No concluida";
                    break;
            }

            return Value;

        }

        public string Periodo(object PeriodoID)
        {
            string retorno = FC.GetPeriodoPMPMY(int.Parse(PeriodoID.ToString()));
            return retorno;

        }

        public string ImprimirEvaluado(object legajo, object PeriodoID, object TipoFormularioID)
        {
            return string.Format("ImprimirEvaluacionV2({0},{1},{2}); return false;", int.Parse(legajo.ToString()), int.Parse(PeriodoID.ToString()), int.Parse(TipoFormularioID.ToString()));
        }

        protected void FVC_DataBound(object sender, EventArgs e)
        {

            Repeater RepeaterObjetivosAnt = (Repeater)FVC.FindControl("RepeaterObjetivosAnt");
            Label lblEmpty = (Label)FVC.FindControl("lblEmpty");


            if (RepeaterObjetivosAnt == null) return;

            if (RepeaterObjetivosAnt.Items.Count > 0)
            {
                HtmlTableRow Obj1 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj1");
                HtmlTableRow Obj2 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj2");
                HtmlTableRow Obj3 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj3");
                HtmlTableRow Obj4 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj4");
                HtmlTableRow Obj5 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj5");
                // HtmlTableRow Obj6 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj6");
                //HtmlTableRow Obj7 = (HtmlTableRow)RepeaterObjetivosAnt.Items[0].FindControl("Obj7");

                if (Obj1.Visible == false && Obj2.Visible == false && Obj3.Visible == false && Obj4.Visible == false && Obj5.Visible == false)
                {
                    RepeaterObjetivosAnt.Visible = false;
                    lblEmpty.Visible = true;

                }
            }
            if (RepeaterObjetivosAnt.Items.Count == 0)
            {
                RepeaterObjetivosAnt.Visible = false;
                lblEmpty.Visible = true;
            }

        }

	}
}