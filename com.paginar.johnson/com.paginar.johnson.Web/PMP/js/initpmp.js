$(document).ready(function () {
    //SoportePNG();	

    ActivarTabs();
    GridView();
    TextArea();
    Buttons();
    RoundedCorners();
    toolTip();
    $('#personas-inner').wrapInner('<div class="inner-decoration" />');

    var total = 0;
    var TotalCompetencias = 0;
    var TotalObjetivos = 0;

    function calcTotal() {
        $("input:checked").each(function () {
            //This happens for each checked input field
            var value = GetValueByText($(this).attr("value"));
            total += parseInt(value);
        });

    }

    function CalcPuntos() {

        TotalCompetencias = 0;
        $('#tabs-1').find("input:checked").each(function () {
            //$("#" + "<%= GridViewCompetencias.ClientID %>"+ " input:checked").each(function () {
            var value = GetValueByText($(this).attr("value"));
            TotalCompetencias += parseInt(value);
        });

        TotalObjetivos = 0;
        $('#tabs-2').find("input:checked").each(function () {
            //$("#" + "<%= GridViewCompetencias.ClientID %>"+ " input:checked").each(function () {
            var value = GetValueByText($(this).attr("value"));
            TotalObjetivos += parseInt(value);
        });
    }

    //This happens when the page loads
    calcTotal();

    $("input:checkbox, input:radio").click(function () {
        total = 0;
        //calcTotal();
        CalcPuntos()
        //$("p.total").html("Total: <strong>" + total + "</strong>");

        CallServiceGetCalificacion(TotalCompetencias, TotalObjetivos, $('#HiddenFieldTipoFormularioID').val());
        //$("#SpanCalificacion").text(GetCalificacion(total));

        //alert(total);
    });


});

function PreguntaImprimirEvaluacion(Legajo,PeriodoID,TipoFormularioID) {
    if (confirm(String.fromCharCode(191) + 'Desea Imprimir la Evaluaci\u00f3n?'))
        ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID);
}

function PreguntaImprimirEvaluacionV2(Legajo, PeriodoID, TipoFormularioID) {
    if (confirm(String.fromCharCode(191) + 'Desea Imprimir la Evaluaci\u00f3n?'))
        ImprimirEvaluacionV2(Legajo, PeriodoID, TipoFormularioID);
}



function ImprimirEvaluacionV2(Legajo, PeriodoID, TipoFormularioID) {
    popupImpresion('ExportarWordPMPO.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}

function popupImpresion(url, ancho, alto) {
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    window.open(url, "ImprimirV2PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

}


function ExportarEvaluacionPMPO(Legajo, PeriodoID, TipoFormularioID) {
    popupExportacion('ExportarWordPMPO.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID + '&VieneDeImpresion=1', 1000, 600);
}

function popupExportacion(url, ancho, alto) {
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    window.open(url, "ExportarOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

}

function ExportarEvaluacionPMPOImpresion(Legajo, PeriodoID, TipoFormularioID) {
    popup('ExportarWordPMPO.aspx?VieneDeImpresion=1&Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}


function ImprimirEvaluacionMY(Legajo, PeriodoID, TipoFormularioID) {
    popup('ImpresionMY.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}

function popup(url, ancho, alto) {
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

}
function GetCalificacion(puntos) {
    if ((puntos >= 11) && (puntos <= 20))
        return "No Satisfactorio";
    if ((puntos >= 21) && (puntos <= 28))
        return "Regular";
    if ((puntos >= 29) && (puntos <= 36))
        return "Bueno";
    if ((puntos >= 37) && (puntos <= 44))
        return "Muy Bueno";
    return "";
}

function CallServiceGetCalificacion(puntosCompetencias,puntosObjetivos, TipoFormularioID) {
    
    $.ajax({
        type: "POST",
        url: "WebServiceEvaluaciones.asmx/GetCalificacionByPuntos",
        data: "{'PuntosCompetencias': " + puntosCompetencias + ", 'PuntosObjetivos': " + puntosObjetivos + ", 'TipoFormularioID': " + TipoFormularioID + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        error: OnError
    });

    function OnSuccess(data, status) {
        $("#SpanCalificacion").removeClass("loading");
        //alert("OnSuccess");
        // $("#SpanCalificacion").html(data);
        $("#SpanCalificacion").text(data.d);
    }

    function OnError(request, status, error) {
        //alert("OnError");
        $("#SpanCalificacion").removeClass("loading");
        $("#SpanCalificacion").html(request.statusText);

    }
}

function GetValueByText(Text) {
    var valor = 0;
    switch (Text) {
        case "RBMB":
            valor = 4;
            break;
        case "RBB":
            valor = 3;
            break;
        case "RBR":
            valor = 2;
            break;
        case "RBNS":
            valor = 1;
            break;
    }
    return valor;
}

function ActivarTabs() {
    $("#tabs").tabs();
    $('.ui-tabs-panel').wrapInner('<div class="ui-tabs-panel-inner" />');
}

function GridView() {
    //quitar bordes laterales
    $('.GridView tr th:first').css('border-left', '1px solid #fff');
    $('.GridView tr th:last').css('border-right', '1px solid #fff');
    $('.GridView tr').each(function() {
        $(this).find('td:first').css('border-left', '1px solid #fff'); /*borde izquierdo de la tabla*/
        $(this).find('td:last').css('border-right', '1px solid #fff'); /*borde derecho de la tabla*/
    });

    //efecto rollover
    $('.GridView tr').mouseover(function() {
        $(this).addClass('resaltado');
    });
    $('.GridView tr').mouseout(function() {
        $(this).removeClass('resaltado');
    });    
}

function Buttons() {
    $('input.formSubmit').wrap('<span class="button"></span>');
    $('a.button').wrapInner('<span class="inner" />');
}

function CheckComentario(TextBoxID) {
    if ($("#" + TextBoxID).val() == '') {
        return confirm('No agreg\u00f3 comentarios. '+String.fromCharCode(191)+'Desea continuar?');
    }
    return true;
}

function TextArea() {
    //tamano maximio de caracteres en textarea
    $('textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 500, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    //In-Field Label
    $('textarea').each(function() {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val($this.attr('title'));
        }
        $this.focus(function() {
            if ($this.val() == $this.attr('title')) {
                $this.val('');
            }
        });
        $this.blur(function() {
            if ($this.val() == '') {
                $this.val($this.attr('title'));
            }
        });
    });

}

function RoundedCorners() {
    DD_roundies.addRule('#personas', '5px', true);
    DD_roundies.addRule('#foto', '5px', true);
}

function SoportePNG() {
    if ($.browser.msie) {
        if ($.browser.version >= 7) {
            //codigo para ie7 y superior
        } else {
            //codigo para ie6
            DD_belatedPNG.fix('img');
        }
    }
}
function SetearGrilla(grillaAux) {

    var grid = document.getElementById(grillaAux);
    grid = grid.all[0];
    //variable to contain the cell of the grid
    var cell1;
    var cell2;
    var cell3;
    var cell4;
    var cell5;
    var RBMB;
    var RBB;
    var RBR;
    var RBNS;
    var TextBoxFundamentacion;
    var row;
    var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
    var TextOpcional = "Puede fundamentar la fortaleza indicada";

    if (grid != null) 
    if(grid.rows.length > 0)
    {
        //loop starts from 1. rows[0] points to the header.
        for (i = 1; i < grid.rows.length; i++) {
            cell1 = grid.rows[i].cells[1];
            RBMB = cell1.childNodes[0]
            cell2 = grid.rows[i].cells[2];
            RBB = cell2.childNodes[0]
            cell3 = grid.rows[i].cells[3];
            RBR = cell3.childNodes[0]
            cell4 = grid.rows[i].cells[4];
            RBNS = cell4.childNodes[0]
            cell5 = grid.rows[i].cells[5];
            TextBoxFundamentacion = cell5.childNodes[0];
            if ((RBMB.checked || RBB.checked) && ((TextBoxFundamentacion.value == "") || (TextBoxFundamentacion.value == TextRequerido) || (TextBoxFundamentacion.value == TextOpcional)))
                SeteoValores('#' + TextBoxFundamentacion.id, "opcional");
            if ((RBR.checked || RBNS.checked) && ((TextBoxFundamentacion.value == "") || (TextBoxFundamentacion.value == TextRequerido) || (TextBoxFundamentacion.value == TextOpcional)))
                SeteoValores('#' + TextBoxFundamentacion.id, "requerido");

        }
    }
}

function ValidarRadiosEnGrilla(grillaAuxi) {
    //get reference of GridView control
    var IsValid = true;
    var grid = document.getElementById(grillaAuxi);
    grid = grid.all[0];
    //variable to contain the cell of the grid
    var cell1;
    var cell2;
    var cell3;
    var cell4;
    var cell5;
    var RBMB;
    var RBB;
    var RBR;
    var RBNS;
    var row;    
    if (grid.rows.length > 0) {
        //loop starts from 1. rows[0] points to the header.
       
        for (i = 1; i < grid.rows.length; i++) {
            //get the reference of first column            
        
            row = grid.rows[i];
            var checkeado = false;
            var tieneradios = false;
            //loop according to the number of childNodes in the cell
            //grid.rows[i].cells[1].childNodes[0].childNodes[0]
            cell1 = grid.rows[i].cells[1];
            if (typeof cell1.childNodes[0].childNodes[0] !== "undefined")
                RBMB = cell1.childNodes[0].childNodes[0];
            else
                RBMB = cell1.childNodes[1].childNodes[0];

            cell2 = grid.rows[i].cells[2];
            if (typeof cell2.childNodes[0].childNodes[0] !== "undefined")
                RBB = cell2.childNodes[0].childNodes[0];
            else
                RBB = cell2.childNodes[1].childNodes[0];

            cell3 = grid.rows[i].cells[3];
            if (typeof cell3.childNodes[0].childNodes[0] !== "undefined")
                RBR = cell3.childNodes[0].childNodes[0];
            else
                RBR = cell3.childNodes[1].childNodes[0];

            cell4 = grid.rows[i].cells[4];
            if (typeof cell4.childNodes[0].childNodes[0] !== "undefined")
                RBNS = cell4.childNodes[0].childNodes[0];
            else
                RBNS = cell4.childNodes[1].childNodes[0];


            if ((!RBMB.checked) && (!RBB.checked) && (!RBR.checked) && (!RBNS.checked))
                return false;         
            
        }
    }
    return true;
}

function toolTip() {

    //personalizando el tooltip
    $('.masInfo').cluetip({
        splitTitle: '|',
        sticky: true,
        arrows: true,
        cursor: 'pointer',
        mouseOutClose: true
    });

}

function revisarSemaforoComentarioAuditor() {
    //div[id$=TextBoxComentarioJT]
    if ($('[rel = "comenAuditor"]').val().length > 3) {
        $('#t4').addClass('oKgreen');
        $('#tabs-4').addClass('oKgreen');

    }
    else {
        $('#t4').removeClass('oKgreen');
        $('#tabs-4').removeClass('oKgreen');
    }
}

function revisarSemaforoComentarioEvaluado() {
    var NoEsValido3;
    var NoEsValido4;
    NoEsValido = false;
    if ($('[rel = "comenEvaluado"]').val().length < 3) {
        NoEsValido4 = true;
    }


    $('div[id$=GVAO]').find('table tr').each(function () {

        $(this).find('textarea:enabled').each(function () {
            
            if($.trim($(this).html()) == "")
                NoEsValido3 = true;
            
        });

    });

    if ($('[type=checkbox]:checked').length < 1) {
        NoEsValido3 = true;
    
    }

    if (NoEsValido4) {

        $('#t4').removeClass('oKgreen');
        $('#tabs-4').removeClass('oKgreen');
    }
    else {

        $('#t4').addClass('oKgreen');
        $('#tabs-4').addClass('oKgreen');
    }

    if (NoEsValido3) {

        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {

        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }


}


function revisarSemaforoComentario() {
    var NoEsValido;
    NoEsValido = false;
   
    if ($('[id$=TextBoxComentarioJT]').val().length < 3) {
        NoEsValido = true;
    }

    if (NoEsValido) {

        $('#t4').removeClass('oKgreen');
        $('#tabs-4').removeClass('oKgreen');
    }
    else {

        $('#t4').addClass('oKgreen');
        $('#tabs-4').addClass('oKgreen');
    }
}




function revisarSemaforo() {

    var cuerpo;
    var NoEsValido;
    cuerpo = false;
    NoEsValido = false;

    $('div[id$=GridViewCompetencias]').find('table tr').each(function () {

        if (cuerpo) {

            if ($(this).find('input[type=radio]:checked').length == 0)
                NoEsValido = true;

        }
        cuerpo = true;
    });
    $('div[id$=GridViewCompetencias]').find('.requerido').each(function () {
        if ($.trim($(this).html()) == 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...' || $(this).html().tr == '') {
            NoEsValido = true;
        }

    });
    if (NoEsValido) {

        $('#t1').removeClass('oKgreen');
        $('#tabs-1').removeClass('oKgreen');
    }
    else {

        $('#t1').addClass('oKgreen');
        $('#tabs-1').addClass('oKgreen');
    }




}

function revisarSemaforoObjetivos() {
    var cuerpo;
    var NoEsValido;
    cuerpo = false;
    NoEsValido = false;

    $('div[id$=GridViewAspectos]').find('table tr').each(function () {

        if (cuerpo) {

            if ($(this).find('input[type=radio]:checked').length == 0)
                NoEsValido = true;

        }
        cuerpo = true;
    });
    $('div[id$=GridViewAspectos]').find('.requerido').each(function () {
        if ($.trim($(this).html()) == 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...' || $(this).html().tr == '') {
            NoEsValido = true;
        }

    });
    if (NoEsValido) {

        $('#t2').removeClass('oKgreen');
        $('#tabs-2').removeClass('oKgreen');
    }
    else {

        $('#t2').addClass('oKgreen');
        $('#tabs-2').addClass('oKgreen');
    }



}

function revisarSemaforosChecks() { 
//$('div[type=checks]')

}

function CambiarClase(ObjID, Clase) {

    //Habilito el control
    $(ObjID).removeAttr('disabled');
    var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
    var TextOpcional = "Puede fundamentar la fortaleza indicada";
	//($(ObjID).attr('class').split(' ').length == 1) &&
    var IsEscribio = ( ($(ObjID).val().length > 0)) && ($(ObjID).val() != TextRequerido) && ($(ObjID).val() != TextOpcional);

    //var IsCambioCondicion = ($(ObjID).attr('class').split(' ')[0] != Clase);
	var IsCambioCondicion = !($(ObjID).hasClass( Clase ));


    if ((IsEscribio) && (IsCambioCondicion)) {
        if (confirm(String.fromCharCode(191)+'Desea eliminar los comentarios de esta competencia?')) {
            $(ObjID).val("");
            SeteoValores(ObjID, Clase)
        }
    } else {
        if (!IsEscribio) {
            SeteoValores(ObjID, Clase)
        }
    }


    $(ObjID).removeClass("opcional");
    $(ObjID).removeClass("requerido");
    $(ObjID).addClass(Clase);

    $('.requerido').click(function () {
        if ($.trim($(this).html()) == 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...' || $(this).html().tr == '') {
            $(this).html('');

        }
        revisarSemaforo();
        revisarSemaforoObjetivos();
    });

    $('.requerido').blur(function () {        
        if ($.trim($(this).html()) == 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...' || $(this).html().tr == '') {
            $(this).html('Fundamente la oportunidad de mejora indicada (campo obligatorio)...');

        }
        revisarSemaforo();
        revisarSemaforoObjetivos();
    });

   
}
function SeteoValores(ObjID, Clase) {
    $(ObjID).val("");
    if (Clase == "opcional") {

        $(ObjID).example(function() {
            var text = $(ObjID).attr('title');

            $(ObjID).attr('title', 'Puede fundamentar la fortaleza indicada');
            return text;
        });
    }
    if (Clase == "requerido") {
        $(ObjID).example(function() {
            var text = $(ObjID).attr('title');

            $(ObjID).attr('title', 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...');
            return text;
        });
    }
    $(ObjID).keyup();
}
function HabDeshab(chk, txt) {

    if ($(chk).is(":checked")) {
        $(txt).removeClass("disabled");
        $(txt).attr('disabled', false);
    } else {
        $(txt).addClass("disabled");
        $(txt).attr('disabled', true);
    }
    revisarSemaforoComentarioEvaluado();
}

function checkRadioslogros() {
    var NoEsValido;
    NoEsValido = false;
    var text = "";

    $('[rel = "desarrollo"]').each(function () {
        var rbDesarrollos;
        rbDesarrollos = $(this);
        var rbfortaleza;
        rbfortaleza = $(this).parent().parent().parent().find('[rel = "rbfortaleza"]');

        if (rbDesarrollos.html().search('CHECKED') < 0 && rbfortaleza.html().search('CHECKED') < 0) {
            NoEsValido = true;
        }


    });

    if (NoEsValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }

}
