﻿$(document).ready(function () {
    //SoportePNG();	
    ActivarTabs();
    GridView();
    //TextArea();
    Buttons();
    RoundedCorners();
    toolTip();

    $('#personas-inner').wrapInner('<div class="inner-decoration" />');



    var total = 0;

    function calcTotal() {
        $("input:checked").each(function () {
            //This happens for each checked input field
            var value = GetValueByText($(this).attr("value"));
            total += parseInt(value);
        });
    }

    //This happens when the page loads
    calcTotal();

    $("input:checkbox, input:radio").click(function () {
        total = 0;
        calcTotal();
        //$("p.total").html("Total: <strong>" + total + "</strong>");

        CallServiceGetCalificacion(total, $('#ctl00_HiddenFieldTipoFormularioID').val());
        //$("#SpanCalificacion").text(GetCalificacion(total));

        //alert(total);
    });

});


function toolTip() {

    //personalizando el tooltip
    $('.masInfo').cluetip({
        splitTitle: '|',
        sticky:false,
        arrows:true
    }); 

}

function PreguntaImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
    if (confirm(String.fromCharCode(191) + 'Desea Imprimir/Exportar la Evaluaci\u00f3n?'))
        ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID);
    return 3;
}
function ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
    popup('ImpresionMY.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}
function popup(url, ancho, alto) {
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

}
function GetCalificacion(puntos) {
    if ((puntos >= 11) && (puntos <= 20))
        return "No Satisfactorio";
    if ((puntos >= 21) && (puntos <= 28))
        return "Regular";
    if ((puntos >= 29) && (puntos <= 36))
        return "Bueno";
    if ((puntos >= 37) && (puntos <= 44))
        return "Muy Bueno";
    return "";
}

function CallServiceGetCalificacion(puntos, TipoFormularioID) {

    $.ajax({
        type: "POST",
        url: "WebServiceEvaluaciones.asmx/GetCalificacionByPuntos",
        data: "{'Puntos': " + puntos + ", 'TipoFormularioID': " + TipoFormularioID + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        error: OnError
    });

    function OnSuccess(data, status) {
        $("#SpanCalificacion").removeClass("loading");
        //alert("OnSuccess");
        $("#SpanCalificacion").html(data);
    }

    function OnError(request, status, error) {
        //alert("OnError");
        $("#SpanCalificacion").removeClass("loading");
        $("#SpanCalificacion").html(request.statusText);

    }
}

function GetValueByText(Text) {
    var valor = 0;
    switch (Text) {
        case "RBMB":
            valor = 4;
            break;
        case "RBB":
            valor = 3;
            break;
        case "RBR":
            valor = 2;
            break;
        case "RBNS":
            valor = 1;
            break;
    }
    return valor;
}

function ActivarTabs() {
    $("#tabs").tabs();
    $('.ui-tabs-panel').wrapInner('<div class="ui-tabs-panel-inner" />');
}

function GridView() {
    //quitar bordes laterales
  $('.GridView tr th:first').css('border-left', '1px solid #fff');
    $('.GridView tr th:last').css('border-right', '1px solid #fff');
    $('.GridView tr').each(function () {
        $(this).find('td:first').css('border-left', '1px solid #fff'); //borde izquierdo de la tabla
        $(this).find('td:last').css('border-right', '1px solid #fff'); //borde derecho de la tabla
    });

    //efecto rollover
    $('.GridView tr').mouseover(function () {
        $(this).addClass('resaltado');
    });
    $('.GridView tr').mouseout(function () {
        $(this).removeClass('resaltado');
    });
}

function Buttons() {
    $('input.formSubmit').wrap('<span class="button"></span>');
    $('a.button').wrapInner('<span class="inner" />');
}

function CheckComentario(TextBoxID) {
    if ($("#" + TextBoxID).val() == '') {
        return confirm('No agreg\u00f3 comentarios. ' + String.fromCharCode(191) + 'Desea continuar?');
    }
    return true;
}

function TextArea() {
    //tamano maximio de caracteres en textarea
    $('textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 500, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    //In-Field Label
    $('textarea').each(function () {
        var $this = $(this);
        if ($this.val() == '') {
            $this.val($this.attr('title'));
        }
        $this.focus(function () {
            if ($this.val() == $this.attr('title')) {
                $this.val('');
            }
        });
        $this.blur(function () {
            if ($this.val() == '') {
                $this.val($this.attr('title'));
            }
        });
    });

}

function RoundedCorners() {
    DD_roundies.addRule('#personas', '5px', true);
    DD_roundies.addRule('#foto', '5px', true);
}

function SoportePNG() {
    if ($.browser.msie) {
        if ($.browser.version >= 7) {
            //codigo para ie7 y superior
        } else {
            //codigo para ie6
            DD_belatedPNG.fix('img');
        }
    }
}
function SetearGrilla(grillaAux) {

    var div = document.getElementById(grillaAux);
    //variable to contain the cell of the grid
    var cell1;
    var cell2;
    var cell3;
    var cell4;
    var cell5;
    var RBFortaleza;
    var RBAreaDesarrollo;
   // var RBR;
   // var RBNS;
    var TextBoxFundamentacion;
    var row;
    var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
    var TextOpcional = "Puede fundamentar la fortaleza indicada";

    var grid;
    if (div.nodeName == "TABLE")
        grid = div;
    else
        grid = div.children[0];

    if (grid != null)
        if (grid.rows.length > 0) {
            //loop starts from 1. rows[0] points to the header.
            for (i = 1; i < grid.rows.length; i++) {
                cell1 = grid.rows[i].cells[1];
                RBFortaleza = cell1.childNodes[0]
                cell2 = grid.rows[i].cells[2];
                RBAreaDesarrolo = cell2.childNodes[0]
               // cell3 = grid.rows[i].cells[3];
               // RBR = cell3.childNodes[0]
               // cell4 = grid.rows[i].cells[4];
               // RBNS = cell4.childNodes[0]
                cell5 = grid.rows[i].cells[3];
                TextBoxFundamentacion = cell5.childNodes[0];
                if ((RBFortaleza.checked ) && ((TextBoxFundamentacion.value == "") || (TextBoxFundamentacion.value == TextRequerido) || (TextBoxFundamentacion.value == TextOpcional)))
                    SeteoValores('#' + TextBoxFundamentacion.id, "opcional");
                if ((RBAreaDesarrollo.checked ) && ((TextBoxFundamentacion.value == "") || (TextBoxFundamentacion.value == TextRequerido) || (TextBoxFundamentacion.value == TextOpcional)))
                    SeteoValores('#' + TextBoxFundamentacion.id, "requerido");

            }
        }
}

function ValidarRadiosEnGrilla(grillaAuxi) {

    var IsValid = true;
    $('#' + grillaAuxi + ' table tbody tr').each(function () {
        var $RBFortaleza = $(this).find("input:radio[id*='RBFortaleza']");
        var $RBAreaDesarrollo = $(this).find("input:radio[id*='RBAreaDesarrollo']");

        if ($RBFortaleza != null && $RBFortaleza != null) {

            if (!$RBFortaleza.is(':checked') && !$RBAreaDesarrollo.is(':checked')) {

                IsValid = false;
            } 
        }

    });   
    return IsValid;
}

function CambiarClase(ObjID, Clase) {

    //Habilito el control
    $(ObjID).removeAttr('disabled');

    var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
    var TextOpcional = "Puede fundamentar la fortaleza indicada";
    //($(ObjID).attr('class').split(' ').length == 1) &&
    var IsEscribio = (($(ObjID).val().length > 0)) && ($(ObjID).val() != TextRequerido) && ($(ObjID).val() != TextOpcional);

    //var IsCambioCondicion = ($(ObjID).attr('class').split(' ')[0] != Clase);
    var IsCambioCondicion = !($(ObjID).hasClass(Clase));


    if ((IsEscribio) && (IsCambioCondicion)) {
        if (confirm(String.fromCharCode(191) + 'Desea eliminar los comentarios de esta competencia?')) {
            $(ObjID).val("");
            SeteoValores(ObjID, Clase)
        }
    } else {
        if (!IsEscribio) {
            SeteoValores(ObjID, Clase)
        }
    }


    $(ObjID).removeClass("opcional");
    $(ObjID).removeClass("requerido");
    $(ObjID).addClass(Clase);
    /*checkRadiosFODE();*/
    checkLeadershipImperatives();


}
function SeteoValores(ObjID, Clase) {
    $(ObjID).val("");
    if (Clase == "opcional") {

        $(ObjID).example(function () {
            var text = $(ObjID).attr('title');

            $(ObjID).attr('title', 'Puede fundamentar la fortaleza indicada');
            return text;
        });
    }
    if (Clase == "requerido") {
        $(ObjID).example(function () {
            var text = $(ObjID).attr('title');

            $(ObjID).attr('title', 'Fundamente la oportunidad de mejora indicada (campo obligatorio)...');
            return text;
        });
    }
    $(ObjID).keyup();
}
function HabDeshab(chk, txt) {

    if ($(chk).is(":checked")) {
        $(txt).removeClass("disabled");
        $(txt).attr('disabled', false);
    } else {
        $(txt).addClass("disabled");
        $(txt).attr('disabled', true);
    }

}




function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}




function ProcesarComentarios(idText, IdCheck, IdCheckContrario) {
    var cantidad;
    //alert(IdCheckContrario);
    cantidad = validarDesarrollo();
  
    var NoEsValido;
    NoEsValido = false;
    var i = 0;
    for (i = 0; i <= 4; i++) {

        $('#' + idText).val($.trim($('#' + idText).val()));
//        alert('#' + idText).val($.trim($('#' + idText).val()))
/*        if ($('#' + IdCheck.substring(0, IdCheck.length - 1) + i).attr("checked") && ($('#' + idText.substring(0, idText.length - 1) + i).val() == "" || $('#' + idText.substring(0, idText.length - 1) + i).val() == "Fundamente la oportunidad de mejora indicada (campo obligatorio)...")) 
        {
            
                NoEsValido = true;
            
        }*/
       // alert($('#' + IdCheck.substring(0, IdCheck.length - 1) + i).attr("checked") + "   " + $('#' + IdCheckContrario.substring(0, IdCheckContrario.length - 1) + i).attr("checked") + "  " + i);

        if (!$('#' + IdCheck.substring(0, IdCheck.length - 1) + i).attr("checked") && !$('#' + IdCheckContrario.substring(0, IdCheckContrario.length - 1) + i).attr("checked")) {
            NoEsValido = true;
        }

    }

   

    if (NoEsValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }


}

/*rel="desarrollo"*/
function validarDesarrollo() {
    
    var cant;
    cant = 0;
    $('[rel = "desarrollo"]').each(function () {
        cant = cant + 1;
    });
    return cant;
}

function checkRadiosFODE() {
    var NoEsValido;
    NoEsValido = false;
    var text = "";

    $('[rel = "desarrollo"]').each(function () {
        var rbDesarrollos;
        rbDesarrollos = $(this);
        var rbfortaleza;
        rbfortaleza = $(this).parent().parent().parent().find('[rel = "rbfortaleza"]');

        if (rbDesarrollos.html().search('CHECKED') < 0 && rbfortaleza.html().search('CHECKED') < 0)
        {
            NoEsValido = true;
        }


    });

    if (NoEsValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }

}


function checkLeadershipImperatives() {
    var NoEsValido;
    NoEsValido = false;
    var text = "";
    $('[rel = "desarrollo"]').each(function () {


        texto = $.trim($(this).parent().parent().find("textarea").val());
        //        $(this).attr("value", texto);   
        $(this).parent().parent().find("textarea").attr("value", texto);

        var rb = $(this).find(':first');
        var ta;



        if (rb.attr("checked")) {
            ta = $(this).parent().parent().find("textarea").val();


            if ((ta == "" || ta == "Fundamente la oportunidad de mejora indicada (campo obligatorio)..."))
                NoEsValido = true;

        }
        /*
        alert($(this).parent().parent().parent().html().search('CHECKED'));
        alert($(this).parent().parent().parent().html());
        var rb2 = $(this).parent().parent().parent();
        if ($(this).html().search("CHECKED") < 0 && $(this).parent().parent().find("td:eq(1)").html().search("CHECKED") < 0) {
            NoEsValido = true;
        }*/

    });
    

    if (NoEsValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }

    
}





function checklogros() {
    var NoValido;
    NoValido = false;
    var texto = "";
    $('[rel = "logros"]').each(function () {

        texto = $.trim($(this).val());
        $(this).attr("value", texto);        
        if ($(this).val().length == 0) {
            NoValido = true;
        }
    });

   

    if (NoValido) {
        //aqui cambiamos el class para cambiar el color 
        $('#t1').removeClass('oKgreen');
        $('#tabs-1').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t1').addClass('oKgreen');
        $('#tabs-1').addClass('oKgreenBody');
    }

}

function checkcomportamientos() {
    var NoValido;
    NoValido = false;
    var texto = "";
    $('[rel = "comportamiento"]').each(function () {

        texto = $.trim($(this).val());
        $(this).attr("value", texto);        
        if ($(this).val().length == 0) {
            NoValido = true;
        }
    });

    
    if (NoValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t2').removeClass('oKgreen');
        $('#tabs-2').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t2').addClass('oKgreen');
        $('#tabs-2').addClass('oKgreen');
    }

}





/*rel="oportunidades"*/
function validarOportunidades() {
    var NoValido;
    NoValido = false;
    var texto = "";
    $('[rel = "oportunidades"]').each(function () {

        texto = $.trim($(this).val());
        $(this).attr("value", texto);
        /*
        alert($(this).attr("value", texto).val());
        alert($(this).val().length);
        */
        if ($(this).val().length == 0) {
            NoValido = true;
        }
    });
    if (NoValido) {
        //aqui cambiamos el class para cuando no es valido
        $('#t4').removeClass('oKgreen');
        $('#tabs-4').removeClass('oKgreen');

    }
    else {
        // aqui el class para el valido
        $('#t4').addClass('oKgreen');
        $('#tabs-4').addClass('oKgreen');
        

    }
}

function validarpestanas() {
    /*checkRadiosFODE();*/
    /*checkLeadershipImperatives();*/
    checklogros();
    checkcomportamientos();
    validarOportunidades();
    
    
}

$(document).ready(function () {
    validarpestanas();
});
