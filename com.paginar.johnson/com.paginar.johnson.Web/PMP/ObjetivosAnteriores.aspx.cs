﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.PMP
{
    public partial class ObjetivosAnteriores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string DamePeriodoByFecha(object val)
        {
            DateTime Fecha = DateTime.Parse(val.ToString());
            DateTime FechaInicioPeriodo = DateTime.Parse("01/07/" + Fecha.Year);
            string Periodo;

            if (Fecha < FechaInicioPeriodo)
            {
                Periodo = (Fecha.Year - 1) + "-" + Fecha.Year;
            }
            else
                Periodo = Fecha.Year + "-" + (Fecha.Year + 1);

            return Periodo;




        }
    }
}