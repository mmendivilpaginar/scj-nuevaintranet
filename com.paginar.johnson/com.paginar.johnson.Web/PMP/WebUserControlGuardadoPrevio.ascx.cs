﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.PMP
{
    public partial class pmp_WebUserControlGuardadoPrevio : System.Web.UI.UserControl
    {
        public Boolean EnabledTimer
        {
            get { return TimerPMP.Enabled; }
            set { TimerPMP.Enabled = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public delegate void GuardadoPrevioHandler();
        public event GuardadoPrevioHandler GuardadoPrevio;
        protected virtual void OnGuardadoPrevio()
        {
            if (GuardadoPrevio != null)
                GuardadoPrevio();
        }
        protected void TimerPMP_Tick(object sender, EventArgs e)
        {
            try
            {
                OnGuardadoPrevio();

                UltimoBackup.Text = string.Format("Borrador Guardado con &eacute;xito a las {0:T} ", DateTime.UtcNow.ToLocalTime().ToUniversalTime().AddHours(-3));

            }
            catch (Exception)
            {
                UltimoBackup.Text = "Error al guardar borrador, ejecute el guardado manual.";
                // throw;
            }
            if (!UltimoBackup.Visible)
            {
                UltimoBackup.Visible = true;
            }
        }
    }
}