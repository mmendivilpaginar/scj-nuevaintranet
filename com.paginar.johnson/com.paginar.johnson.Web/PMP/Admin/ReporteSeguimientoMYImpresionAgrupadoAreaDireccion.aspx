﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoMYImpresionAgrupadoAreaDireccion.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimientoMYImpresionAgrupadoAreaDireccion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Johnson Intranet</title>
    <link href="../css/printReporteSeguimiento2.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
           // window.close();
        }

        
        
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
    
       <div id="header">
            <div class="bg"></div>
            <h1><asp:Label ID="LabelTitulo" runat="server" Text="Reporte de Seguimiento"></asp:Label></h1>
            <img id="logo" src="../../images/logoprint.gif" />
        </div>

        <div id="noprint">           
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />           
            <%--<asp:Button ID="ButtonExportarWord" runat="server" Text="Exportar Word" 
                onclick="ButtonExportarWord_Click"/>--%>
            <asp:Button ID="ButtonExportarExcel" runat="server" Text="Exportar excel" 
                onclick="ButtonExportarExcel_Click"/>
            <br /><br />
        </div>


        <div id="PanelCompleto" runat="server">
            <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" />
            <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" />
            <asp:HiddenField ID="HiddenFieldPasoID" runat="server" />
            <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" />
            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server" />
            <asp:HiddenField ID="HiddenFieldAreaID" runat="server" />
            <asp:HiddenField ID="HiddenFieldCluster" runat="server" />
            <asp:Repeater ID="RepeaterEvaluaciones" runat="server"  DataSourceID="ObjectDataSourceEvaluacionesAgrupadoPorDireccionArea"
                onitemdatabound="RepeaterEvaluaciones_ItemDataBound" 
                onprerender="RepeaterEvaluaciones_PreRender">
            <HeaderTemplate>
                <table>
                      
                        <tr style="height:60px;" >
                            <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg1"></div>
                                <asp:Label ID="LabelTitulo" runat="server" Text="Área/ Dirección"></asp:Label>
                                </div></th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                No <br  style="mso-data-placement: same-cell;"/>Iniciadas</div></th>

                             <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                % No <br  style="mso-data-placement: same-cell;"/>Iniciadas</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                En <br  style="mso-data-placement: same-cell;"/>Evaluado</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                % En <br  style="mso-data-placement: same-cell;"/>Evaluado</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1" style=" top:-15"></div>
                                En <br  style="mso-data-placement: same-cell;"/>Evaluador</div></th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                % En <br  style="mso-data-placement: same-cell;"/>Evaluador</div> </th>

                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg"></div>
                                Aprobadas</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                % <br  style="mso-data-placement: same-cell;"/>Aprobadas</div> </th>

                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;" id="COL_TO_HIDE_HEADER1" runat="server">
                                <div style="position:relative;"><div class="bg1"></div>
                                No <br  style="mso-data-placement: same-cell;"/>Concluidas</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;" id="COL_TO_HIDE_HEADER2" runat="server">
                                <div style="position:relative;"><div class="bg1"></div>
                                % No <br  style="mso-data-placement: same-cell;"/>Concluidas</div> </th>

                           <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg"></div>
                                Total</div> </th>
                            <th align="center" style="border:1px solid #D9D9D9;padding:0px;background-color: #ccc;">
                                <div style="position:relative;"><div class="bg1"></div>
                                % Total</div> </th>
                            
                        </tr>   
                              
            </HeaderTemplate>
            <ItemTemplate>
                       <tr style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "height:60px;": "" %>' >

                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <asp:HiddenField ID="HiddenFieldAreaID" runat="server"  Value='<%# (Request.QueryString["AreaID"].ToString() != "0" ? Request.QueryString["AreaID"].ToString() : Eval("AreaID"))%>' />
                       <asp:HiddenField ID="HiddenFieldDireccionID" runat="server"  Value='<%# Bind("DireccionID") %>'/>
                       <asp:HiddenField ID="HiddenFieldAreaID1" runat="server"  Value='<%# Bind("AreaID") %>'/>


                       <asp:Literal ID="LiteralColorCelda" runat="server" Text='<%# DameColorCelda(Eval("AreaID"),Eval("DireccionID")) %>'>'></asp:Literal>
                       <%--<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>--%>
                       <asp:Label ID="LabelDireccion" runat="server" Text='<%# DameNombreColumna(Eval("Area"),Eval("Direccion")) %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>
                       </td>

                       
                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>' >
                            
                       
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                       
                            <asp:Label ID="LabelCantNoIniciadas" runat="server" Text='<%# Eval("CantNoIniciadas")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>

                       <asp:Repeater ID="RepeaterEnNoIniciados" runat="server" DataSourceID="ObjectDataSourceNoIniciados"
                            onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                 <br runat="server" id="brNoIniciados"/> 
                                <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br/> 
                            </ItemTemplate>
                            <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server"  ></asp:Label>

                            </FooterTemplate>
                            </asp:Repeater>
                       <asp:ObjectDataSource ID="ObjectDataSourceNoIniciados" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="-1" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>

                       </td>

                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="Label1" runat="server" Text='<%# Eval("PorcNoIniciadas"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                       </td>
                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                  
                           
                            <asp:Label ID="LabelCantEnEvaluado" runat="server" Text='<%# Eval("CantEnEvaluado")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>

                        <asp:Repeater ID="RepeaterEnEvaluado" runat="server" DataSourceID="ObjectDataSourceEnEvaluado"
                                      onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                <br  runat="server" id="brEnEvaluado"/>                                            
                                <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br  /> 
                                </ItemTemplate>
                                <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server" ></asp:Label>

                            </FooterTemplate>
                            </asp:Repeater>
                           
                       <asp:ObjectDataSource ID="ObjectDataSourceEnEvaluado" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="2" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>
                       
                       </td>

                        <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="Label2" runat="server" Text='<%# Eval("PorcEnEvaluado"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                       </td>

                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>  
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                             
                            
                            <asp:Label ID="LabelCantEnLT" runat="server" Text='<%# Eval("CantEnEvaluador")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>
                       
                       <asp:Repeater ID="RepeaterEnEvaluador" runat="server" DataSourceID="ObjectDataSourceEnevaluador"
                                    onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                <br runat="server" id="brEnEvaluador" /> 
                                    <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br  /> 
                                </ItemTemplate>
                                 <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server" ></asp:Label>

                            </FooterTemplate>
                            </asp:Repeater>
                       <asp:ObjectDataSource ID="ObjectDataSourceEnEvaluador" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="1" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>
                                                              
                       </td>

                        <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="Label3" runat="server" Text='<%# Eval("PorcEnEvaluador"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                       </td>


                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                         
                           <asp:Label ID="LabelCantAprobadas" runat="server" Text='<%# Eval("CantAprobadas")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>
                            <asp:Repeater ID="RepeaterAprobadas" runat="server" DataSourceID="ObjectDataSourceAprobadas"
                                          onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                <br runat="server" id="brAprobadas"/> 
                                <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br /> 
                                </ItemTemplate>
                                <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server" ></asp:Label>

                            </FooterTemplate>
                                </asp:Repeater>
                      <asp:ObjectDataSource ID="ObjectDataSourceAprobadas" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="4" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>
                       
                       </td>
                       
                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="LabelPorcAprobadas" runat="server" Text='<%# Eval("PorcAprobadas"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                          </td>

                       <td id="COL_TO_HIDE1" runat="server" align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                         
                           <asp:Label ID="LabelNoConcluidas" runat="server" Text='<%# Eval("CantNoConcluidas")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>
                            <asp:Repeater ID="RepeaterNoConcluidas" runat="server" DataSourceID="ObjectDataSourceNoConcluidas"
                                           onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                <br runat="server" id="brNoConcluidas" /> 
                                <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br /> 
                                </ItemTemplate>
                                 <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server"  ></asp:Label>

                            </FooterTemplate>
                                </asp:Repeater>
                      <asp:ObjectDataSource ID="ObjectDataSourceNoConcluidas" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="6" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>
                       
                       </td>

                       <td id="COL_TO_HIDE2" runat="server"  align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="Label4" runat="server" Text='<%# Eval("PorcNoConcluidas"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                       </td>

                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                         
                           <asp:Label ID="LabelCantTotal" runat="server" Text='<%# Eval("CantTotal")%>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>
                            <asp:Repeater ID="RepeaterTotal" runat="server" DataSourceID="ObjectDataSourceTotal"
                                          onitemdatabound="RepeaterIN_ItemDataBound">
                                <ItemTemplate>
                                <br runat="server" id="brAprobadas"/> 
                                <%# Eval("Evaluado").ToString() + Eval("Legajo", " - {0}")%>
                                <br /> 
                                </ItemTemplate>
                                <FooterTemplate>
                            <asp:Label ID="lblEmpty" Text="0" runat="server" ></asp:Label>

                            </FooterTemplate>
                                </asp:Repeater>
                      <asp:ObjectDataSource ID="ObjectDataSourceTotal" runat="server" 
                                              OldValuesParameterFormatString="original_{0}" 
                                              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
                                              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                                              <SelectParameters>
                                                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                                      Type="Int32" />
                                                 <asp:Parameter DefaultValue="0" Name="LegajoEvaluador" Type="Int32" /> 
                                                  <asp:Parameter DefaultValue="0" Name="Pasoid" Type="Int32" />
                                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:ControlParameter ControlID="HiddenFieldAreaID" Name="AreaID" 
                                                    PropertyName="Value" Type="Int32" />
                                                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                                                      QueryStringField="Calificacion" Type="String" />
                                                  <asp:ControlParameter ControlID="HiddenFieldCluster" Name="Cluster" 
                                                      PropertyName="Value" Type="String" />
                                              </SelectParameters>
                                          </asp:ObjectDataSource>
                       
                       </td>
                       
                       <td align="center" style='<%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6;" : "border:1px solid #D9D9D9;"%>'>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                                                                                                                  
                          <asp:Label ID="Label6" runat="server" Text='<%# Eval("PorcTotal"," {0} %") %>'></asp:Label>
                       <%# (string.IsNullOrEmpty(Eval("AreaID").ToString()) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(Eval("DireccionID").ToString()))) ? "</div>" : ""%>                          
                          </td>

                       </tr>            
            </ItemTemplate>

            <FooterTemplate>
                            
                </table>

             </FooterTemplate>
            </asp:Repeater>

            <asp:Label ID="lblEmpty" Visible='false'  Text="No se encontrarón resultados para la búsqueda" runat="server" CssClass="messages msg-info"></asp:Label>
            <asp:ObjectDataSource ID="ObjectDataSourceEvaluacionesAgrupadoPorDireccionArea" runat="server" 
                  OldValuesParameterFormatString="original_{0}" SelectMethod="Eval_GetReporteSeguimientoAgrupadoAreaDireccion" 
                  TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                <SelectParameters>
                    <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                        Type="Int32" />
                    <asp:QueryStringParameter Name="LegajoEvaluador" 
                        QueryStringField="LegajoEvaluador" Type="Int32" />
                    <asp:QueryStringParameter Name="Pasoid" QueryStringField="PasoID" 
                        Type="Int32" />
                    <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                    <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                        QueryStringField="DireccionID" Type="Int32" />
                    <asp:QueryStringParameter DefaultValue="0" Name="AreaID" 
                        QueryStringField="AreaID" Type="Int32" />                                                
                    <asp:QueryStringParameter DefaultValue="" Name="Cluster" 
                        QueryStringField="Cluster" Type="String" />
                    <asp:QueryStringParameter DefaultValue="" Name="MostrarAreas" 
                        QueryStringField="MostrarAreas" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>  
            <br />
 
       </div>
    
    </div>
    </form>
</body>
</html>
