﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoAdmImpresionOrg.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimientoAdmImpresionOrg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Johnson Intranet</title>
    <link href="../css/printReporteSeguimiento2.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            //window.close();
        }

        
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
    
       <div id="header">
            <div class="bg"></div>
            <h1>Reporte de Seguimiento PMP Administrativos</h1>
            <img id="logo" src="../../images/logoprint.gif" />
        </div>

        <div id="noprint">           
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />           
            
            <asp:Button ID="ButtonExportarExcel" runat="server" Text="Exportar excel" 
                onclick="ButtonExportarExcel_Click"/>
            <br /><br />
        </div>


        <div id="PanelCompleto" runat="server">
            <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" />
            <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" />
            <asp:HiddenField ID="HiddenFieldPasoID" runat="server" />            
            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server" /> 
            <asp:HiddenField ID="HiddenFieldPasoIDAbiertas" runat="server" /> 
            <asp:HiddenField ID="HiddenFieldPasoIDCerradas" runat="server" /> 
                       
            <asp:Repeater ID="RepeaterEvaluaciones" runat="server"  
                DataSourceID="ObjectDataSourceEvaluacionesConSubtotalesPorArea" onitemdatabound="RepeaterEvaluaciones_ItemDataBound"
                >
            <HeaderTemplate>
                <table>
                        <tr style="height:60px;" >
                            <th align="center" style="padding:0px; background-color:#E6E6E6;width: 200px;">
                            <div style="position:relative;"><div class="bg"></div>
                                Jefe</div></th>
                            <th align="center" style="padding:0px; background-color:#E6E6E6;">
                            <div style="position:relative;"><div class="bg1"></div>
                                Dirección <br  style="mso-data-placement: same-cell;"/>Jefe</div></th>
                            <th align="center" style="padding:0px; background-color:#E6E6E6;">
                            <div style="position:relative;"><div class="bg"></div>
                                Abiertas</div> </th>
                            <th align="center" style="padding:0px; background-color:#E6E6E6;">
                            <div style="position:relative;"><div class="bg" style=" top:-15"></div>
                                Cerradas</div> </th>                               
                            <th align="center" style="padding:0px; background-color:#E6E6E6;">
                            <div style="position:relative;"><div class="bg1"></div>
                                Porcentaje <br  style="mso-data-placement: same-cell;"/>Cerradas</div> </th>
                        </tr>            
            </HeaderTemplate>
            <ItemTemplate>
                       <tr style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "height:60px;": "" %>' >
                       <td align="center" style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "border:1px solid #D9D9D9; padding:0px; background-color: #E6E6E6" : "border:1px solid #D9D9D9;"%>'>
                        <%# Eval("LegajoEvaluador").ToString() == "-1" ? "<div style='position:relative;'><div class='bgSubT'></div>" : ""%>
                       <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Bind("Evaluador") %>'></asp:Label>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "</div>" : ""%> </td>
                       <td align="center" style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6" : "border:1px solid #D9D9D9;"%>'>
                        <%# Eval("LegajoEvaluador").ToString() == "-1" ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>
                       <asp:Label ID="LabelDireccionEvaluador" runat="server" Text='<%# Eval("DireccionEvaluador").ToString()!="" ? Eval("DireccionEvaluador") : "-" %>'></asp:Label>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "</div>" : ""%> </td>
                       <td align="center" style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6" : "border:1px solid #D9D9D9;"%>'>
                            <asp:HiddenField ID="HiddenFieldEvaluador" runat="server"  Value='<%# Bind("LegajoEvaluador")%>'/> 
                            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server"  Value='<%# Bind("DireccionID")%>'/>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>                                        
                            <asp:Repeater ID="RepeaterAbiertas" runat="server" DataSourceID="ObjectDataSourceAbiertas" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                            </ItemTemplate>
                            </asp:Repeater>
                         <asp:Label ID="LabelCantAbiertas" runat="server" Text='<%# Eval("CantAbiertas")%>' Visible="false"></asp:Label>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "</div>" : ""%> 
                        <asp:ObjectDataSource ID="ObjectDataSourceAbiertas" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorOrigTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldEvaluador" Name="LegajoEvaluador" 
                                        PropertyName="Value" Type="Int32" />                                
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDAbiertas" Name="EstadoID" 
                                        PropertyName="Value" Type="Int32" />                                     
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                       </td>
                       <td align="center" style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6" : "border:1px solid #D9D9D9;"%>'> 
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>
                            <asp:Repeater ID="RepeaterCerradas" runat="server" DataSourceID="ObjectDataSourceCerradas" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                    <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                                </ItemTemplate>
                            </asp:Repeater>

                           <asp:Label ID="LabelCantAprobadas" runat="server" Text='<%# Eval("CantAprobadas")%>' Visible="false"></asp:Label>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "</div>" : ""%> 
                        <asp:ObjectDataSource ID="ObjectDataSourceCerradas" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorOrigTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldEvaluador" Name="LegajoEvaluador" 
                                        PropertyName="Value" Type="Int32" />                                
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDCerradas" Name="EstadoID" 
                                        PropertyName="Value" Type="Int32" />                                    
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>                                            
                       </td>
                      
                       
                       <td align="center" style='<%# Eval("LegajoEvaluador").ToString() == "-1" ? "border:1px solid #D9D9D9; padding:0px;background-color: #E6E6E6" : "border:1px solid #D9D9D9;"%>'>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "<div style='position:relative;'><div class='bgSubT1'></div>" : ""%>  
                       <asp:Label ID="LabelPorcAprobadas" runat="server" Text='<%# Eval("PorcAprobadas"," {0} %") %>'></asp:Label>
                       <%# Eval("LegajoEvaluador").ToString() == "-1" ? "</div>" : ""%>   
                       </td>

                       </tr>            
            </ItemTemplate>

            <FooterTemplate>
             <asp:Repeater ID="RepeaterFooter" runat="server" DataSourceID="ObjectDataSourceReporteSeguimientoTotales">
             <ItemTemplate>
              <tr style="height: 40px; " >
                    <td align="center" style=" padding:0px;background-color:gray;"><div style="position:relative;"><div class="bg"></div>TOTAL </div></td>
                    <td align="center" style=" padding:0px;background-color:gray;"><div style="position:relative;"><div class="bg"></div>-</div></td>                   
                    <td align="center" style=" padding:0px;background-color:gray;">
                    <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="LabelTotalAbiertas" runat="server" Text='<%#Eval("CantAbiertas") %>'></asp:Label></div></td>                    
                    <td align="center" style=" padding:0px;background-color:gray;" >
                       <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="LabelTotalCerradas" runat="server" Text='<%#Eval("CantCerradas") %>'></asp:Label></div></td>
                   
                   <td align="center" style=" padding:0px;background-color:gray;">
                        <div style="position:relative;"><div class="bg"></div>
                        <asp:Label ID="LabelPorcentajeAprobadas" runat="server" Text='<%#Eval("PorCerradas"," {0} %") %>'></asp:Label></div></td>                          
                      
                </tr>
             </ItemTemplate>
             </asp:Repeater>
               

                <asp:ObjectDataSource ID="ObjectDataSourceReporteSeguimientoTotales" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesOrigTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                            Type="Int32" />
                        <asp:QueryStringParameter Name="LegajoEvaluador" 
                            QueryStringField="LegajoEvaluador" Type="Int32" />
                        <asp:QueryStringParameter Name="Pasoid" QueryStringField="PasoID" 
                            Type="Int32" />                        
                        <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                            QueryStringField="DireccionID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </table>

             </FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceEvaluacionesConSubtotalesPorArea" runat="server" 
                 OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"                                            
                TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosOrigTableAdapter">
                <SelectParameters>
                    <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                        Type="Int32" />
                    <asp:QueryStringParameter Name="LegajoEvaluador" 
                        QueryStringField="LegajoEvaluador" Type="Int32" />
                    <asp:QueryStringParameter Name="Pasoid" QueryStringField="PasoID" 
                        Type="Int32" />
                    <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                    <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                        QueryStringField="DireccionID" Type="Int32" />
                    <asp:Parameter DefaultValue="" Name="Cluster" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>  
       </div>
    
    </div>
    </form>
</body>
</html>
