﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteByAreaDeOperacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(Request.QueryString["PasoID"].ToString()) || Request.QueryString["PasoID"].ToString() == "0")
                 Label3.Visible = false;

            if (string.IsNullOrEmpty(Request.QueryString["AreaDeOperacionID"].ToString()) || Request.QueryString["AreaDeOperacionID"].ToString() == "0")
            {
                LabelAreaInteres.Visible = false;
                GridViewEvaluados.Columns[3].Visible = true;
            }

            if (string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"].ToString()) || Request.QueryString["TipoFormularioID"].ToString() != "0")
                GridViewEvaluados.Columns[5].Visible = false;
           

        }

        protected string DefaultValComentarios(object val)
        {

            if (((val == System.DBNull.Value) || (val == null)))
                return "-";
            if (val == string.Empty)
                return "-";
            else
            {
                int Maximalongitud = 10;
                string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

                return Texto;
            }

        }

        protected void GridViewEvaluados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                LinkButton lnkBtnApellidoNombre = (LinkButton)e.Row.FindControl("lnkBtnApellidoNombre");
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
               // Label lblApellidoNombre = (Label)e.Row.FindControl("lblApellidoNombre");
                int Legajo;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "Legajo").ToString(), out Legajo);

                int EstadoID;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "EstadoID").ToString(), out EstadoID);

                string UrlForm = string.Empty;
                FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();

                FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
                if (RelPasosTipoFormulariosDt.Rows.Count > 0)
                {
                    FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                    UrlForm = RelPasosTipoFormulariosRow.FormAspx;
                }

                bool carga = false;

                int LegajoEvaluador;
                int.TryParse(Request.QueryString["LegajoEvaluador"], out LegajoEvaluador);

                if (ViewState["HabilitaPeriodo"] != null)
                    carga = (bool)ViewState["HabilitaPeriodo"];
                else
                    carga = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));

                if (carga)
                {
                    if (EstadoID == 4 || EstadoID == 5)
                        lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('../Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                        lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('" + "../" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}&LegajoEvaluador={3}&SoloLectura=1',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, LegajoEvaluador));
                }
                else//Sino no esta en el periodo de carga, tengo q abrir impresion

                    lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('../Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                //if (Page.User.IsInRole("Visualizar PMPS-Reporte de Seguimiento PMPO"))
                //    lblApellidoNombre.Visible = false;
                //else
                //    lnkBtnApellidoNombre.Visible = false;
            }
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }

         
    }
}