﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.membership;



namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class CambiarEstadoNoIniciadoEvaluador : System.Web.UI.Page
    {

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

       

        protected void ButtonInicarPMP_Click(object sender, EventArgs e)
        {

                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                int LegajoEvaluador = 0;
                int estadoID = -1;

               
                
               foreach (GridViewRow item in  GridView1.Rows)
               {
                
                    legajo = Convert.ToInt32(((Label)item.FindControl("LabelLegajo")).Text);                   
                    PeriodoID = Convert.ToInt32(((Label)item.FindControl("LabelPeriodoID")).Text);                
                    TipoFormularioID = Convert.ToInt32(((Label)item.FindControl("LabelTipoFormularioID")).Text);               
                    LegajoEvaluador = Convert.ToInt32(((Label)item.FindControl("LabelLegajoEvaluador")).Text);
                    estadoID = Convert.ToInt32(((Label)item.FindControl("LabelEstadoID")).Text); 
                //esto es Paral asociar else evaluador
                //if ()
                //{
                //    FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
                //    //Limpio Hiddden para q no lo tome despues en el envio y lo asociie nuevamente
                //    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                //    NotificacionesController n = new NotificacionesController();
                //    string mensaje = n.inicioEvaluacion(UE.GetUsuarioNombre(LegajoEvaluador), UE.GetUsuarioNombre(legajo));

                //   // HiddenFieldLegajoEvaluador.Value = string.Empty;
                //   // EjecutarScript(string.Format("document.getElementById('{0}').value ='';", HiddenFieldLegajoEvaluador.ClientID));

                //}
                    if (estadoID == -1)
                    {
                        FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                        FC.SetComentarioJefeTurno(string.Empty);
                        try
                        {
                            GuardarItems(TipoFormularioID);
                            FC.GrabarEvaluador();
                            FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);
                            //omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
            }

               ObjectDataSource1.DataBind();

        }

       

        private void GuardarItems( int TipoFormularioID)
        {
            FormulariosDS.ItemEvaluacionDataTable DataTableItems = new FormulariosDS.ItemEvaluacionDataTable();
            FachadaDA.Singleton.ItemEvaluacion.FillByTipoFormularioID(DataTableItems, TipoFormularioID);

            foreach (FormulariosDS.ItemEvaluacionRow IER in DataTableItems)
            {
                if(bool.Parse(IER.Activo.ToString()))
                {
                
                    int ItemEvaluacionID = int.Parse(IER.ItemEvaluacionID.ToString());
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;                    
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }
       
    
  }
}