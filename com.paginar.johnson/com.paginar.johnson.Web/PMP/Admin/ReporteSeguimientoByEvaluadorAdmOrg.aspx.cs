﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteSeguimientoByEvaluadorAdmOrg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["LegajoEvaluador"].ToString() == "-1")
                PanelEvaluador.Visible = false;
            if (Request.QueryString["LegajoEvaluador"].ToString() == "0")
                LabelSinEvaluador.Visible = true;
            if (Request.QueryString["EstadoID"].ToString() == "0")
                LabelEstado.Text = "Cerradas";
            if (Request.QueryString["EstadoID"].ToString() == "1")
            {
                LabelEstado.Text = "Abiertas";
                GridViewEvaluados.Columns[7].Visible = false;
            }
            if (Request.QueryString["EstadoID"].ToString() == "-1")
                PanelEstado.Visible = false;
            if (Request.QueryString["DireccionID"].ToString() == "0")
                PanelDireccion.Visible = false;



        }

        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("--");
            else
                return (val.ToString());

        }
    }
}