﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.formularios.businesslogiclayer;
using System.Data;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;




namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class PMPCalibracion :  PageBase
    {

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }
        private DataTable _dtEvaluaciones;
        private DataTable dtEvaluaciones
        {
            get
            {
                _dtEvaluaciones = (DataTable)Session["dtEvaluaciones"];
                if (_dtEvaluaciones == null)
                {
                   // _dtEvaluaciones = new FormulariosReporteGeneralDS.Eval_GetReporteGeneralDataTable();
                    FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionDataTable DataTableCalibracion = new FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionDataTable();
                    FachadaDA.Singleton.GetReporteParaCalibracion.Fill(DataTableCalibracion, Par_PeriodoID, Par_TipoFormularioID,
                        Par_Estado, Evaluado, EvaluadoLegajo, Evaluador, Sector,Area, Cargo, Calificacion);
                    _dtEvaluaciones = LLenarTablaParaCalibracion(DataTableCalibracion);
                    //FachadaDA.Singleton.GetReporteGeneral.Fill(_dtReporteGeneral, int.Parse(ddlPeriodo.SelectedValue), Formularios, Estado,
                    //    Evaluado, Evaluador, Auditor, Sector, Area,
                    //    Resultado, ResultadoComp, Competencia);


                    Session["dtEvaluaciones"] = _dtEvaluaciones;
                }
                return _dtEvaluaciones;
            }
            set
            {
                Session["dtEvaluaciones"] = value;
                _dtEvaluaciones = value;
            }
        }
        private int _Par_PeriodoID;
        private int Par_PeriodoID
        { 
        get
            {

                if (Session["PeriodoID"] != null)
                    //return (int)Session["idLegal"];
                    _Par_PeriodoID = (int)Session["PeriodoID"];
                else
                    _Par_PeriodoID = 0;
            return _Par_PeriodoID;
           }
        set{
            this.Session["PeriodoID"] = value;
            _Par_PeriodoID = value;
            }
        }
        private int _Par_TipoFormularioID;
        private int Par_TipoFormularioID
        {
            get
            {

                if (Session["TipoFormularioID"] != null)
                    //return (int)Session["idLegal"];
                    _Par_TipoFormularioID = (int)Session["TipoFormularioID"];
                else
                    _Par_TipoFormularioID = 0;
                return _Par_TipoFormularioID;
            }
            set
            {
                this.Session["TipoFormularioID"] = value;
                _Par_PeriodoID = value;
            }
        }
        private int _Par_Estado;
        private int Par_Estado
        {
            get
            {

                if (Session["Estado"] != null)
                    //return (int)Session["idLegal"];
                    _Par_Estado = (int)Session["Estado"];
                else
                    _Par_Estado = 0;
                return _Par_Estado;
            }
            set
            {
                this.Session["Estado"] = value;
                _Par_Estado = value;
            }
        }
        private string _Evaluado;
        private string Evaluado
        {
            get
            {

                if (Session["Evaluado"] != null)
                    //return (int)Session["idLegal"];
                    _Evaluado = Session["Evaluado"].ToString();
                else
                    _Evaluado = string.Empty;
                return _Evaluado;
            }
            set
            {
                this.Session["Evaluado"] = value;
                _Evaluado = value;
            }
        }
        private string _EvaluadoLegajo;

        private string EvaluadoLegajo
        {
            get
            {

                if (Session["EvaluadoLegajo"] != null)
                    //return (int)Session["idLegal"];
                    _EvaluadoLegajo = Session["EvaluadoLegajo"].ToString();
                else
                    _EvaluadoLegajo = string.Empty;
                return _EvaluadoLegajo;
            }
            set
            {
                this.Session["EvaluadoLegajo"] = value;
                _EvaluadoLegajo = value;
            }
        }
        private string _Evaluador;
        private string Evaluador
        {
            get
            {

                if (Session["Evaluador"] != null)
                    //return (int)Session["idLegal"];
                    _Evaluador = Session["Evaluador"].ToString();
                else
                    _Evaluador = string.Empty;
                return _Evaluador;
            }
            set
            {
                this.Session["Evaluador"] = value;
                _Evaluador = value;
            }
        }
        private string _Sector;
        private string Sector
        {
            get
            {

                if (Session["Sector"] != null)
                    //return (int)Session["idLegal"];
                    _Sector = Session["Sector"].ToString();
                else
                    _Sector = string.Empty;
                return _Sector;
            }
            set
            {
                this.Session["Sector"] = value;
                _Sector = value;
            }
        }
        private string _Area;

        private string Area
        {
            get
            {

                if (Session["Area"] != null)
                    //return (int)Session["idLegal"];
                    _Area = Session["Area"].ToString();
                else
                    _Area = string.Empty;
                return _Area;
            }
            set
            {
                this.Session["Area"] = value;
                _Area = value;
            }
        }
        private string _Cargo;
        private string Cargo
        {
            get
            {

                if (Session["Cargo"] != null)
                    //return (int)Session["idLegal"];
                    _Cargo = Session["Cargo"].ToString();
                else
                    _Cargo = string.Empty;
                return _Cargo;
            }
            set
            {
                this.Session["Cargo"] = value;
                _Cargo = value;
            }
        }
        private string _Calificacion;

        private string Calificacion
        {
            get
            {

                if (Session["Calificacion"] != null)
                    //return (int)Session["idLegal"];
                    _Calificacion = Session["Calificacion"].ToString();
                else
                    _Calificacion = string.Empty;
                return _Calificacion;
            }
            set
            {
                this.Session["Calificacion"] = value;
                _Calificacion = value;
            }
        }

        private bool _isEditMode;

        private bool isEditMode
        {

            get 
            {
                if (Session["IsInEditMode"] != null)
                    _isEditMode = (bool)Session["IsInEditMode"];
                else
                    _isEditMode = false;

                return _isEditMode;
            }

            set 
            {
                this.Session["IsInEditMode"] = value;
                _isEditMode = value;
            }

        }

    private string getPostBackControlName()
    {
        Control control = null;
        //first we will check the "__EVENTTARGET" because if post back made by       the controls
        //which used "_doPostBack" function also available in Request.Form collection.
        string ctrlname = Page.Request.Params["__EVENTTARGET"];
        if (ctrlname != null && ctrlname != String.Empty)
        {
            control = Page.FindControl(ctrlname);
        }
        // if __EVENTTARGET is null, the control is a button type and we need to
        // iterate over the form collection to find it
        else
        {
            string ctrlStr = String.Empty;
            Control c = null;
            foreach (string ctl in Page.Request.Form)
            {
                //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                //mouse x and y coordinates
                if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                {
                    ctrlStr = ctl.Substring(0, ctl.Length - 2);
                    c = Page.FindControl(ctrlStr);
                }
                else
                {
                    c = Page.FindControl(ctl);
                }
                if (c is System.Web.UI.WebControls.Button ||
                         c is System.Web.UI.WebControls.ImageButton)
                {
                    control = c;
                    break;
                }
            }
        }
        //return control.ID;
        return control == null ? String.Empty : control.ID;
 
    }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
              {
                if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
                    Session["PeriodoID"] = int.Parse(Request.QueryString["PeriodoID"].ToString());
                if (!string.IsNullOrEmpty(Request.QueryString["FormularioID"]))
                     Session["TipoFormularioID"] = int.Parse(Request.QueryString["FormularioID"].ToString());
                if (!string.IsNullOrEmpty(Request.QueryString["Estado"]))
                    Session["Estado"] = int.Parse(Request.QueryString["Estado"].ToString());
                if (!string.IsNullOrEmpty(Request.QueryString["Evaluado"]))
                    Session["Evaluado"] = Request.QueryString["Evaluado"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["EvaluadoLegajo"]))
                   Session["EvaluadoLegajo"] = Request.QueryString["EvaluadoLegajo"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["Evaluador"]))
                    Session["Evaluador"] = Request.QueryString["Evaluador"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["Sector"]))
                    Session["Sector"] = Request.QueryString["Sector"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["Area"]))
                    Session["Area"] = Request.QueryString["Area"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["Cargo"]))
                    Session["Cargo"] = Request.QueryString["Cargo"].ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["Calificacion"]))
                    Session["Calificacion"] = Request.QueryString["Calificacion"].ToString();
                dtEvaluaciones = null;
                isEditMode = false;
                
            }

            string controlCallPostback = getPostBackControlName();
            isEditMode = false;
            if (controlCallPostback == "btnEditarTodas" || controlCallPostback == "btnGuardarTodas")
            {
                isEditMode = true;

                dtEvaluaciones = SelectDataTable(dtEvaluaciones, "[EstadoId] = 1", "");
                if (dtEvaluaciones.Rows.Count == 0)
                {
                    isEditMode = false;
                    dtEvaluaciones = null;
                }
            }

            //Visibilidad de los botones
            EnviarTodoDos.Visible = !isEditMode;
            //EnviarTodo.Visible = !isEditMode;
            btnEditarTodas.Visible = !isEditMode;
            btnCancelarTodas.Visible = isEditMode;
            btnGuardarTodas.Visible = isEditMode;
            CreateTemplatedGridView();

             if (IsPostBack)
                ActualizardtEvaluaciones();
            //MultiViewReportes.SetActiveView(vwReporteParaCalibracion);
            GridViewEvaluados.DataSource = dtEvaluaciones;
            GridViewEvaluados.DataBind();
           //EjecutarScript("CreateGridHeader('DataDiv', '"+GridViewEvaluados.ClientID+"', 'HeaderDiv'");
            // ClientScript.RegisterStartupScript(this.GetType(), "CreateGridHeader", "<script>CreateGridHeader('DataDiv', '"+GridViewEvaluados.ClientID+"', 'HeaderDiv');</script>");
             
           // ClientScript.RegisterStartupScript(typeof(Page),"scroller", "<script>scrollToVal();</script>");
            HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
            ////BodyPmp.Attributes.Add("onunload", "window.opener.location.reload();");
            //String js = "if (!window.opener.closed)window.opener.refreshParent();" + Environment.NewLine;
            //  BodyPmp.Attributes.Add("onunload", js);


            BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx');");
                   
            
        }

        void ActualizardtEvaluaciones()
        {
           // string ItemEvaluacionID;
            int index = GridViewEvaluados.EditIndex;
            if (index > -1)
            {
                DataRow dtRow = dtEvaluaciones.Rows[index];
                Session["linea"] = ((TextBox)GridViewEvaluados.Rows[index].FindControl("TextBoxLinea")).Text;
                
              //  for (int i = 24; i < dtEvaluaciones.Columns.Count; i++)
               // {
                    //ItemEvaluacionID= dtEvaluaciones.Columns[i].ColumnName;
                   // HtmlTextArea TextArea = (HtmlTextArea)Page.FindControl("textarea1");
                   // Label LabelItemEvaluacion = (Label)GridViewEvaluados.Rows[index].FindControl("LabelItemEvaluacion" + ItemEvaluacionID);
                  //  Session["Fundamentacion" + ItemEvaluacionID] = ((HtmlTextArea)GridViewEvaluados.Rows[index].FindControl("TextBoxLinea")).Value;

              //  }
            }
           // GridViewEvaluados.Rows[e.RowIndex]
        
        }

        protected string DefaultValComentarios(object val)
        {

            if (((val == System.DBNull.Value) || (val == null)))
                return "-";
            if (val == string.Empty)
                return "-";
            else
            {
                int Maximalongitud = 10;
                string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

                return Texto;
            }

        }

        

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }



 


        private DataTable LLenarTablaParaCalibracion(FormulariosReporteGeneralDS.eval_GetReporteParaCalibracionDataTable OrigenDataTable)
        {
            var r01 = DistinctRows(OrigenDataTable, "Legajo");
            DataTable dtnew = new DataTable();
            dtnew = OrigenDataTable.Clone();
                      

            //-- Agregar filas como columnas (las que no son pivot)
            FormulariosDS.ItemEvaluacionDataTable DataTableItems = new FormulariosDS.ItemEvaluacionDataTable();
            FachadaDA.Singleton.ItemEvaluacion.FillByTipoFormularioID(DataTableItems, Par_TipoFormularioID);

            foreach (FormulariosDS.ItemEvaluacionRow IER in DataTableItems)
            {

                dtnew.Columns.Add(IER.ItemEvaluacionID.ToString());
                r01.Columns.Add(IER.ItemEvaluacionID.ToString());

                dtnew.Columns.Add("Fundamentacion" + IER.ItemEvaluacionID.ToString());
                r01.Columns.Add("Fundamentacion" + IER.ItemEvaluacionID.ToString());
            }

            foreach (DataRow dt in r01.Rows)
            {
                DataTable datosEvaluado = SelectDataTable(OrigenDataTable, "Legajo=" + dt["Legajo"], "");
                DataRow rownueva = dtnew.NewRow();
                rownueva["ItemEvaluacionID"] = dt["PeriodoID"];
                rownueva["Ponderacion"] = dt["Ponderacion"];                
                rownueva["PeriodoID"] = dt["PeriodoID"];
                rownueva["TipoFormularioID"] = dt["TipoFormularioID"];
                rownueva["Legajo"] = dt["Legajo"];
                rownueva["TipoFormulario"] = dt["TipoFormulario"];
                rownueva["linea"] = dt["linea"];
                rownueva["fIngreso"] = dt["fIngreso"];
                rownueva["ApellidoNombre"] = dt["ApellidoNombre"];
                rownueva["LegajoEvaluador"] = dt["LegajoEvaluador"];
                rownueva["Evaluador"] = dt["Evaluador"];
                rownueva["LegajoAudior"] = dt["LegajoAudior"];
                rownueva["Auditor"] = dt["Auditor"];
                rownueva["EstadoID"] = dt["EstadoID"];
                rownueva["Estado"] = dt["Estado"];
                rownueva["Calificacion"] = dt["Calificacion"];
                rownueva["CalificacionPuntos"] = dt["CalificacionPuntos"];
                rownueva["Sector"] = dt["Sector"];
                rownueva["Area"] = dt["Area"];
                rownueva["Cargo"] = dt["Cargo"];
                rownueva["fCalibracion"] = dt["fCalibracion"];
                rownueva["usuarioCalibracion"] = dt["usuarioCalibracion"];

                foreach (DataRow RowEvaluado in datosEvaluado.Rows)
                {
                    string nombreCalumna = RowEvaluado["ItemEvaluacionID"].ToString();//itemEvaluado.Table.Columns["ItemevaluacionID"].ToString();
                    rownueva[nombreCalumna] = RowEvaluado["Ponderacion"];


                    nombreCalumna = "Fundamentacion" + RowEvaluado["ItemEvaluacionID"].ToString();//itemEvaluado.Table.Columns["ItemevaluacionID"].ToString();
                    rownueva[nombreCalumna] = RowEvaluado["Fundamentacion"];
                    
                }


                //rownueva[nombreColumnaPivot] = dt;
                dtnew.Rows.Add(rownueva);
            }



            return dtnew;

            }

        protected DataTable DistinctRows(DataTable dt, string keyfield)
        {
            DataTable newTable = dt.Clone();
            if (newTable.Columns["rowcount"] == null)
                newTable.Columns.Add("rowcount", typeof(int));
            int keyval = 0;
            DataView dv = dt.DefaultView;
            dv.Sort = keyfield;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr1 in dt.Rows)
                {
                    bool existe = false;
                    foreach (DataRow dr2 in newTable.Rows)
                    {
                        if (dr1[keyfield].ToString() == dr2[keyfield].ToString())
                        {
                            existe = true;
                            dr2["rowcount"] = ((int)dr2["rowcount"]) + 1;
                        }
                    }

                    if (!existe)
                    {
                        newTable.ImportRow(dr1);
                        newTable.Rows[newTable.Rows.Count - 1]["rowcount"] = 1;
                    }
                }
            }
            else
                newTable = dt.Clone();
            return newTable;
        }

        public DataTable SelectDataTable(DataTable dt, string filter, string sort)
        {
            DataRow[] rows = null;
            DataTable dtNew = default(DataTable);

            // copy table structure 
            dtNew = dt.Clone();

            // sort and filter data 
            rows = dt.Select(filter, sort);

            // fill dtNew with selected rows 

            foreach (DataRow dr in rows)
            {
                dtNew.ImportRow(dr);
            }

            // return filtered dt 
            return dtNew;
        }

         protected void GridViewEvaluados_PreRender(object sender, EventArgs e)
        {

            //GridView grdv = (GridView)sender;
            //if (grdv.Rows.Count == 0)
            //{
            //    MultiViewReportes.SetActiveView(ViewSinResultados);
            //}

        }

        void CreateTemplatedGridView()
        {
            String NombreColumna="";
            DataTable aux;
            //int LegajoEvaluado;
            string ToolTip = "";
            for (int i = 24; i < dtEvaluaciones.Columns.Count; i=i + 2)
            {

                string where = "["+dtEvaluaciones.Columns[i].ColumnName+"] is not null";
                    //dtEvaluaciones.Select("isnull([99],0)<>0","").Length
                   
                aux = SelectDataTable(dtEvaluaciones, where, "");

                if(aux.Rows.Count>0)
                {
                        FormulariosDS.ItemEvaluacionDataTable itemEvaluacionDatatable = new FormulariosDS.ItemEvaluacionDataTable();
                        FachadaDA.Singleton.ItemEvaluacion.FillByItemEvaluacionID(itemEvaluacionDatatable, int.Parse(dtEvaluaciones.Columns[i].ColumnName));

                        if (itemEvaluacionDatatable.Rows.Count > 0)
                        {
                            FormulariosDS.ItemEvaluacionRow itemEvaluacionRow = itemEvaluacionDatatable.Rows[0] as FormulariosDS.ItemEvaluacionRow;
                            NombreColumna = itemEvaluacionRow.Titulo;
                        }

                   

                TemplateField ItemTmpField = new TemplateField();
                    //fundamentacion
                //int LegajoEvaluado = int.Parse(dtEvaluaciones.Columns["Legajo"].ToString());
                //FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable RelitemEvaluacionDT = new FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable();
                //FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow RelitemEvaluacionRow;
                //FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.FillByID(RelitemEvaluacionDT, LegajoEvaluado, Par_PeriodoID, Par_TipoFormularioID);
                //if (RelitemEvaluacionDT.Rows.Count > 0)
                //{
                //    RelitemEvaluacionRow = (FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow)RelitemEvaluacionDT.Rows[0];
                //    ToolTip = RelitemEvaluacionRow["Fundamentacion"].ToString();
                //}
                ItemTmpField.HeaderTemplate = new DynamicallyTemplatedGridViewHandler(ListItemType.Header,
                                                              NombreColumna,
                                                              dtEvaluaciones.Columns[i].DataType.Name, ToolTip);
                // create ItemTemplate
                ItemTmpField.ItemTemplate = new DynamicallyTemplatedGridViewHandler(ListItemType.Item,
                                                              dtEvaluaciones.Columns[i].ColumnName,
                                                              dtEvaluaciones.Columns[i].DataType.Name, ToolTip, isEditMode);
               

                 //create EditItemTemplate
                ItemTmpField.EditItemTemplate = new DynamicallyTemplatedGridViewHandler(ListItemType.EditItem,
                                                              dtEvaluaciones.Columns[i].ColumnName,
                                                              dtEvaluaciones.Columns[i].DataType.Name, ToolTip);
                // then add to the GridView
                GridViewEvaluados.Columns.Add(ItemTmpField);
               }

           }

            TemplateField ComentariosTemplate = new TemplateField();
            ComentariosTemplate.ItemTemplate =
                new DynamicallyTemplatedGridViewHandler(ListItemType.Item, "Comentario", "Comentario", "", isEditMode);
            ComentariosTemplate.HeaderTemplate =
                new DynamicallyTemplatedGridViewHandler(ListItemType.Header, "Comentario", "Comentario", "", isEditMode);
            ComentariosTemplate.EditItemTemplate =
                new DynamicallyTemplatedGridViewHandler(ListItemType.EditItem, "Comentario", "Comentario", "", isEditMode);
            GridViewEvaluados.Columns.Add(ComentariosTemplate);


                TemplateField BtnTmpField = new TemplateField();
                BtnTmpField.ItemTemplate =
                    new DynamicallyTemplatedGridViewHandler(ListItemType.Item, "...", "Command", ToolTip, isEditMode);
                BtnTmpField.HeaderTemplate =
                    new DynamicallyTemplatedGridViewHandler(ListItemType.Header, "...", "Command", ToolTip, isEditMode);
                BtnTmpField.EditItemTemplate =
                    new DynamicallyTemplatedGridViewHandler(ListItemType.EditItem, "...", "Command", ToolTip, isEditMode);
                GridViewEvaluados.Columns.Add(BtnTmpField);

            // bind and display the data
            //GridViewEvaluados.DataSource = TableCalibracion;// LLenarTablaParaCalibracion(DataTableCalibracion);
            //GridViewEvaluados.DataBind();
        }


        protected void enviar_evaluacion(int legajoIn, bool bindear)
        {

            dtEvaluaciones = null;

            DataTable DTEnviar = SelectDataTable(dtEvaluaciones, "Legajo=" + legajoIn.ToString(), "");
            if (DTEnviar.Rows.Count > 0)
            {
                int legajo = legajoIn;
                int PeriodoID = int.Parse((DTEnviar.Rows[0]).ItemArray[1].ToString());
                int TipoFormularioID = int.Parse((DTEnviar.Rows[0]).ItemArray[2].ToString()); ;
                int LegajoEvaluador = int.Parse((DTEnviar.Rows[0]).ItemArray[8].ToString()); ;

                FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                NotificacionesController n = new NotificacionesController();
                string mensajex = string.Empty;
                try
                {
                    mensajex = n.inicioEvaluacion(UE.GetUsuarioNombre(LegajoEvaluador), UE.GetUsuarioNombre(legajo));
                }
                catch (Exception xy)
                {

                    mensajex = "No se envio";
                }
                
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                //esto lo utilizaré para setear comentario
                //                FC.SetComentarioJefeTurno(TextBoxComentarioJT.Text);
                try
                {
                    //FC.GrabarEvaluador();
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);

                    NotificacionesController nc = new NotificacionesController();
                    FormulariosDS.CabeceraFormularioDataTable Cabe = FC.GetCabecera(int.Parse(PeriodoID.ToString()), int.Parse(legajo.ToString()), int.Parse(TipoFormularioID.ToString()), null);
                    ControllerUsuarios CU = new ControllerUsuarios();
                    DSUsuarios.UsuarioDataTable DTU = CU.GetUsuariosByLegajo(legajo);


                    string NombreEvaluador = Cabe.Rows[0].ItemArray[12].ToString();
                    string MailOperario = DTU.Rows[0].ItemArray[4].ToString();
                    string URLPMP;

                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSE1&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];
                    string mensaje = string.Empty;
                    try
                    {
                        mensaje = nc.inicioEvaluacionOperario(MailOperario, NombreEvaluador, URLPMP);
                    }
                    catch (Exception xx)
                    {

                        mensaje = "No se envio";
                    }
                     
                    //omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    //EjecutarScript(string.Format("PreguntaImprimirEvaluacionV2({0},{1},{2})", legajo, PeriodoID, TipoFormularioID));
                    //Response.Redirect("~/UnauthorizedAccess.aspx");

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            if (bindear)
            {

                dtEvaluaciones = null;
                if (IsPostBack)
                ActualizardtEvaluaciones();
                //MultiViewReportes.SetActiveView(vwReporteParaCalibracion);
                GridViewEvaluados.DataSource = dtEvaluaciones;
                GridViewEvaluados.DataBind();
                EjecutarScript("$( document ).ready(function() { alert('Evaluación enviada exitosamente'); });");
            }
        }
       

       protected void GridViewEvaluados_RowCommand(object sender, GridViewCommandEventArgs e)
        {
                btnEditarTodas.Enabled = true;
                btnEditarTodas.ToolTip = "Editar todas";
                EnviarTodoDos.Enabled = true;
                EnviarTodoDos.ToolTip = "Enviar";                    
            //CreateTemplatedGridView();
            if (e.CommandName == "Edit")
            {
                string jsScrollHorizontalToController = " $( document ).ready(function() { $('div.fht-tbody').scrollLeft( 1300 ); });";
                EjecutarScript("__modificado = 1;" + jsScrollHorizontalToController);
               // DropDownListTodosUsuarios.Enabled = false;
                //ButtonAgregar.Enabled = false;
                btnEditarTodas.Enabled = false;
                btnEditarTodas.ToolTip = "Debe finalizar la edición de la evaluación en curso para poder realizar esta acción";
                EnviarTodoDos.Enabled = false;
                EnviarTodoDos.ToolTip = "Debe finalizar la edición de la evaluación en curso para poder realizar esta acción";
            }

            if (e.CommandName == "Enviar")
           {
               int a = 0;
               enviar_evaluacion(int.Parse(e.CommandArgument.ToString()), true);
            //aqui enviamos la pmp
           }

            if (e.CommandName == "Update")
            {

                if (!Page.IsValid) return;
                int index;
                index = GridViewEvaluados.EditIndex;                

                //index=int.parse((e.CommandArgument.ToString());
                int TipoFormularioID = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldTipoFormularioID") as HiddenField).Value);
                int PeriodoID = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldPeriodoID") as HiddenField).Value);
                int Legajo = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldLegajo") as HiddenField).Value);
                string Linea = Session["linea"].ToString();//(GridViewEvaluados.Rows[index].FindControl("TextBoxLinea") as TextBox).Text;
                string Evaluador = (GridViewEvaluados.Rows[index].FindControl("LabelEvaluador") as Label).Text;
                string ComentarioEvaluador = (GridViewEvaluados.Rows[index].FindControl("editar_comentario") as TextBox).Text;
                //(((GridView)sender).Rows[index].FindControl("TextBoxLinea") as TextBox).Text
                
                FC = new FormulariosController(PeriodoID, Legajo, TipoFormularioID);
                FC.SetComentarioJefeTurno(ComentarioEvaluador);
                string NombreDropDownList;
                string NombreLabel;
                int ItemEvaluacionID;
                String Fundamentacion = string.Empty;
                FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable DataTableItemEvaluacion = new FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable();
                FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.FillByID(DataTableItemEvaluacion, Legajo, PeriodoID, TipoFormularioID);

               try{       
                for (int i = 24; i < dtEvaluaciones.Columns.Count; i=i+2)
                {
                    ItemEvaluacionID = int.Parse(dtEvaluaciones.Columns[i].ColumnName);
                    DataRow[] rows = DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID);
                    NombreDropDownList = "DropDownListItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
                    DropDownList DropDownListItemsEvaluacion = (DropDownList)GridViewEvaluados.Rows[index].FindControl(NombreDropDownList);

                    HiddenField HiddenFieldItemEvaluacion = (HiddenField)GridViewEvaluados.Rows[index].FindControl("HiddenFieldItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName);

                    NombreLabel = "LabelItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
                    //Label LabelItemEvaluacion = (Label)GridViewEvaluados.Rows[index].FindControl(NombreLabel);
                    HiddenField LabelItemEvaluacion = (HiddenField)GridViewEvaluados.Rows[index].FindControl(NombreLabel);


                    
                    
                    
                    if (rows.Count() > 0 && DropDownListItemsEvaluacion!=null)
                    {
                        FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow PmpOperariosItemsEvaluacionRow = DataTableItemEvaluacion.Select("ItemEvaluacionID="+ ItemEvaluacionID)[0]  as FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow;
                        Fundamentacion = HiddenFieldItemEvaluacion.Value;// PmpOperariosItemsEvaluacionRow.Fundamentacion;                        
                        //GridViewEvaluados.Rows[0].
                        //GridViewEvaluados.Rows[index].FindControl(NombreDropDownList)
                        int Ponderacion = int.Parse((GridViewEvaluados.Rows[index].FindControl(NombreDropDownList) as DropDownList).SelectedValue);
                        FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                    }
                    
                }
                FC.SaveHistorial(1, PeriodoID, Legajo, TipoFormularioID);
                FC.SetDatosCalibracion(ObjectUsuario.usuarioid);
                
                FC.SetLineaEvaluado(Legajo,PeriodoID,TipoFormularioID,Linea);

                FC.GrabarEvaluador();

                  }
                     catch (Exception exc)
                        {}

                
                dtEvaluaciones = null;
                //CreateTemplatedGridView();                
                //GridViewEvaluados.DataSource = dtEvaluaciones;
                //GridViewEvaluados.DataBind();
                GridViewEvaluados.DataSource = dtEvaluaciones;
                GridViewEvaluados.DataBind();

                EjecutarScript("__modificado = 0; $( document ).ready(function() { alert('Evaluación guardada'); });");
            }

            if (e.CommandName == "Cancel")
            {
                //DropDownListTodosUsuarios.Enabled = true;
                //ButtonAgregar.Enabled = true;
            }
            //ClientScript.RegisterStartupScript(this.GetType(), "CreateGridHeader", "<script>CreateGridHeader('DataDiv', '" + GridViewEvaluados.ClientID + "', 'HeaderDiv');</script>");
            
           
           this.MaintainScrollPositionOnPostBack = true;  
        }

    

       private void EjecutarScript(string js)
       {
           ScriptManager sm = ScriptManager.GetCurrent(this);
           if (sm.IsInAsyncPostBack)
               System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
           else
               this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


       }

       protected void GridViewEvaluados_RowEditing(object sender, GridViewEditEventArgs e)
       {
           GridViewEvaluados.EditRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#D8ECF2");
           GridViewEvaluados.EditIndex = e.NewEditIndex;
           GridViewEvaluados.DataBind();
           this.MaintainScrollPositionOnPostBack = true;
           HiddenFieldEditRow.Value = GridViewEvaluados.EditIndex.ToString();
            //GridView1.Rows[e.NewEditIndex].FindControl("txtEdit").Focus();
           GridViewEvaluados.Rows[e.NewEditIndex].FindControl("DropDownListItemEvaluacion1").Focus();


       }

       protected void GridViewEvaluados_RowUpdating(object sender, GridViewUpdateEventArgs e)
       {
           GridViewEvaluados.EditIndex = -1;
           HiddenFieldEditRow.Value = "-1";
           dtEvaluaciones = null;
           GridViewEvaluados.DataSource = dtEvaluaciones;
           GridViewEvaluados.DataBind();
          
           this.MaintainScrollPositionOnPostBack = true;

       }

       protected void GridViewEvaluados_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
       {
           GridViewEvaluados.EditIndex = -1;
           GridViewEvaluados.DataBind();
           this.MaintainScrollPositionOnPostBack = true;
       }

       protected void GridViewEvaluados_RowDataBound(object sender, GridViewRowEventArgs e)
       {

           if (e.Row.RowType == DataControlRowType.DataRow)
           {
               ImageButton edit_button= (ImageButton)e.Row.FindControl("edit_button");
               ImageButton send_button = (ImageButton)e.Row.FindControl("send_button");
               ImageButton print_button = (ImageButton)e.Row.FindControl("print_button");
               
               
               HiddenField HiddenFieldEstadoID = (HiddenField)e.Row.FindControl("HiddenFieldEstadoID");
               if (send_button != null)
               {
                   send_button.Visible = false;
                   if (HiddenFieldEstadoID.Value == "1")
                       send_button.Visible = true;
               }

               if (print_button != null)
               {
                   print_button.Visible = false;
                   if (HiddenFieldEstadoID.Value == "1")
                       print_button.Visible = true;
               }

               if (edit_button != null && isEditMode)
               {
                   edit_button.Enabled = false;
                   edit_button.ToolTip = "En modo \"Editar todas\" esta opción no esta disponible";
               }

               if (send_button != null && isEditMode)
               {
                   send_button.Enabled = false;
                   send_button.ToolTip = "En modo \"Editar todas\" esta opción no esta disponible";
               }

               if (print_button != null && isEditMode)
               {
                   print_button.Enabled = false;
                   print_button.ToolTip = "En modo \"Editar todas\" esta opción no esta disponible";
               }


               string NombreLabel;
               string NombreDropDownList;
               string NombreLiteral;
               string NombreHiddenField;
               string toolTip="Sin Comentario";
               string IDLink;
               string ID;
               string Fundamentacion = string.Empty; ;
               
               int ItemEvaluacionID;
               if (edit_button != null)
               {
                   if (int.Parse(HiddenFieldEstadoID.Value) != 1)
                       edit_button.Visible = false;
               }


               HiddenField Legajo = (HiddenField)e.Row.FindControl("HiddenFieldLegajo");
               HiddenField HiddenFieldTipoFormularioID = (HiddenField)e.Row.FindControl("HiddenFieldTipoFormularioID");
               HiddenField HiddenFieldPeriodoID = (HiddenField)e.Row.FindControl("HiddenFieldPeriodoID");
               Label mostrar_comentario = (Label)e.Row.FindControl("mostrar_comentario");
               TextBox editar_comentario = (TextBox)e.Row.FindControl("editar_comentario");
               FormulariosController FCX = new FormulariosController(int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(Legajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));

               if (mostrar_comentario != null)
               {
                   mostrar_comentario.Text = FCX.GetComentarioJefeTurno() == null ? "" : FCX.GetComentarioJefeTurno();
                   mostrar_comentario.ToolTip = FCX.GetComentarioJefeTurno() == null ? "" : FCX.GetComentarioJefeTurno();
               }

               if (editar_comentario != null)
               {
                   editar_comentario.Text = FCX.GetComentarioJefeTurno() == null ? "" : FCX.GetComentarioJefeTurno();
                   editar_comentario.ToolTip = FCX.GetComentarioJefeTurno() == null ? "" : FCX.GetComentarioJefeTurno();
               }
               
              //HiddenField HiddenFieldLegajo = (HiddenField)e.Row.FindControl("HiddenFieldLegajo");
              // FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable DataTableItemEvaluacion = new FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable();
              // FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.FillByID(DataTableItemEvaluacion, int.Parse(Legajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value), int.Parse(HiddenFieldPeriodoID.Value));
                           
                   
               
               for (int i = 24; i < dtEvaluaciones.Columns.Count; i=i+2)
               {

               toolTip = "S/C";

               ItemEvaluacionID = int.Parse(dtEvaluaciones.Columns[i].ColumnName);
               IDLink = Legajo.Value.ToString() + HiddenFieldTipoFormularioID.Value.ToString() + HiddenFieldPeriodoID.Value.ToString() + ItemEvaluacionID.ToString();
               ID = ItemEvaluacionID.ToString();
              // DataRow[] rows = DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID);

               NombreLabel = "LabelItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
               NombreDropDownList = "DropDownListItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
               NombreLiteral="LiteralItemEvaluacion"+ dtEvaluaciones.Columns[i].ColumnName;
               NombreHiddenField = "HiddenFieldItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;

                //Label LabelItemEvaluacion = (Label)e.Row.FindControl(NombreLabel);
               HiddenField HiddenFieldItemEvaluacion = (HiddenField)e.Row.FindControl(NombreHiddenField);
                DropDownList DropDownListItemEvaluacion = (DropDownList)e.Row.FindControl(NombreDropDownList);
                Label LabelItemEvaluacion = (Label)e.Row.FindControl(NombreLabel);
                Label LabelValidacion = (Label)e.Row.FindControl("LabelValidacion");
                Literal LiteralItemEvaluacion = (Literal)e.Row.FindControl(NombreLiteral);
                CustomValidator CustomValidatorTextBoxFundamentacion = e.Row.FindControl("CustomValidatorTextBoxFundamentacion" + dtEvaluaciones.Columns[i].ColumnName) as CustomValidator;

                if (CustomValidatorTextBoxFundamentacion != null)
                {
                    CustomValidatorTextBoxFundamentacion.Attributes.Add("DropDownListItemEvaluacion", "#" + DropDownListItemEvaluacion.ClientID);
                    CustomValidatorTextBoxFundamentacion.Attributes.Add("HiddenFieldItemEvaluacion", "#" + HiddenFieldItemEvaluacion.ClientID);
                    CustomValidatorTextBoxFundamentacion.Attributes.Add("LabelValidacion", "#" + LabelValidacion.ClientID);
                    //CustomValidatorTextBoxFundamentacion.ErrorMessage = "Debe completar la fundamentación de la Competencia: " + dtEvaluaciones.Columns[i].ColumnName;
                    //CustomValidatorTextBoxFundamentacion.BorderColor = System.Drawing.Color.OrangeRed; "red";

                }
                       //if (DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID).Length > 0)
                //{
                  //  FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow PmpOperariosItemsEvaluacionRow = DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID)[0] as FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow;
                  //  if (PmpOperariosItemsEvaluacionRow.Fundamentacion != "")
                  //  {
                if (HiddenFieldItemEvaluacion != null)
                   {
                       toolTip = HiddenFieldItemEvaluacion.Value;
                       Fundamentacion = HiddenFieldItemEvaluacion.Value;
                       if (LabelItemEvaluacion != null)
                       {
                           if(string.IsNullOrEmpty(HiddenFieldItemEvaluacion.Value))
                                LabelItemEvaluacion.ToolTip="S/C";
                           else
                               LabelItemEvaluacion.ToolTip = HiddenFieldItemEvaluacion.Value;
                       }
                   }
                        
                  //  }
              //  }




               // if (LabelItemEvaluacion != null && DropDownListItemEvaluacion!=null)
                //    LabelItemEvaluacion.Style.Add("display", "none");

                   
                if (DropDownListItemEvaluacion != null && LiteralItemEvaluacion != null)
                {
                    //LabelItemEvaluacion.Attributes.CssStyle[ Visible = false;
                    string span = "<span class='tooltip' runat='server' id='LinkFundamentacion" + IDLink + "'> <a onclick='javascript:void(0)' id='tooltipFundamentacion" + IDLink + "' href='#loadmeFundamentacion" + IDLink + "'  rel='#loadmeFundamentacion" + IDLink + "' class='tooltip'>.</a></span>";
                    string div = "<div id='loadmeFundamentacion" + IDLink + "'> <span id='SpanFundamentacion" + IDLink + "' runat='server' /> <textarea cols='40' rows='2' class='defaultText'  runat='server' ID='textarea" + ID + "' name='textarea" + ID + "' onkeyup='javascript:CopyText( \"" + HiddenFieldItemEvaluacion.ClientID.ToString() + "\", this.value,"+ IDLink +"," + ID + ")'>" + toolTip + " </textarea> </span> <a href='javascript:ComprobarComentario(" + IDLink + ");'>OK</a>  </div>";
                    //string Script = "<script type='text/javascript'>$(document).ready(function () {if (jQuery.trim($('#SpanFundamentacion" + IDLink + "').html()) != '') {$('#tooltipFundamentacion" + IDLink + "').cluetip({local: true, sticky: true, closePosition: 'title', arrows: true, activation:'click', ajaxSettings :{ type: 'Post', data: " + IDLink + "},ajaxProcess :function(data){ var TextArea = document.getElementById('textarea' + data); TextArea = $(data);}, });} else { $('#loadmeFundamentacion" + IDLink + "').hide();} })</script>";
                    string Script = "";
                    LiteralItemEvaluacion.Text = span + div + Script;
                    //LiteralItemEvaluacion.Visible = false;                    
                    //LabelItemEvaluacion.ToolTip = toolTip;

                }
               if (DropDownListItemEvaluacion != null)
               {
                   DropDownListItemEvaluacion.Attributes["onclick"] = "javascript:AbrirToolTip(" + IDLink + "," + ID + ")";
                   if (DropDownListItemEvaluacion.SelectedValue == "")
                       DropDownListItemEvaluacion.Enabled = false;
                   else
                      // DropDownListItemEvaluacion.Items.Remove("");
                       DropDownListItemEvaluacion.Items.RemoveAt(0);
                   
                   
                  // DropDownListItemEvaluacion.ToolTip = toolTip;
               }


           }

            //validamos si esta en condiciones de ser enviada

            bool ValidarRow = validarRowActual(((DataRowView)e.Row.DataItem).Row);




            if (send_button != null)
                send_button.Visible = (ValidarRow && int.Parse(HiddenFieldEstadoID.Value) == 1);

            if (send_button != null)
                send_button.CommandArgument = ((DataRowView)e.Row.DataItem).Row.ItemArray[4].ToString();

            if (print_button != null)
            {
                print_button.Visible = (int.Parse(HiddenFieldEstadoID.Value) == 1);
                print_button.OnClientClick = string.Format("popup('/PMP/Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value);
                    //.Attributes.Add("onclick", string.Format("popup('/PMP/Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
            }

           }
       }


       protected bool validarRowActual(DataRow DR)
       {
           //Elementos que pueden ser null
           ArrayList ElementosPermitidosNull = new ArrayList();

           ElementosPermitidosNull.Add(0);
           ElementosPermitidosNull.Add(16);
           ElementosPermitidosNull.Add(19);
           ElementosPermitidosNull.Add(23);
           ElementosPermitidosNull.Add(40);
           ElementosPermitidosNull.Add(41);
           ElementosPermitidosNull.Add(44);
           ElementosPermitidosNull.Add(45);
           ElementosPermitidosNull.Add(46);
           ElementosPermitidosNull.Add(47);

           //Elementos que van valuados
           ArrayList ElementosObligatorios = new ArrayList();
           ElementosObligatorios.Add(24);
           ElementosObligatorios.Add(26);
           ElementosObligatorios.Add(28);
           ElementosObligatorios.Add(30);
           ElementosObligatorios.Add(32);
           ElementosObligatorios.Add(34);
           ElementosObligatorios.Add(36);
           ElementosObligatorios.Add(38);
           ElementosObligatorios.Add(42);
           ElementosObligatorios.Add(48);

           bool retorno = true;
           for (int i = 0; i < dtEvaluaciones.Columns.Count; i = i + 1)
           {
               if (ElementosObligatorios.Contains(i) && !ElementosPermitidosNull.Contains(i))
               {
                   if (DR.ItemArray[i].ToString() == "" || DR.ItemArray[i].ToString() == "0")
                   {
                       retorno = false;
                       break;
                   }
                   else
                   {
                       if(int.Parse(DR.ItemArray[i].ToString()) < 3 && (DR.ItemArray[i+1].ToString() == null || DR.ItemArray[i+1].ToString() == ""))
                       {
                           retorno = false;
                           break;
                       }
                   }
               }

           }

           return retorno;
       }

        protected void Button1_Click(object sender, EventArgs e)
       {



       }

       protected void EnviarTodo_Click(object sender, ImageClickEventArgs e)
       {

           //foreach (GridViewRow elRow in GridViewEvaluados.Rows)
           string evaluacionesEnviadas = string.Empty;
           ArrayList ElementosPermitidosNull = new ArrayList();

           ElementosPermitidosNull.Add(0);
           ElementosPermitidosNull.Add(16);
           ElementosPermitidosNull.Add(19);
           ElementosPermitidosNull.Add(23);
           ElementosPermitidosNull.Add(40);
           ElementosPermitidosNull.Add(41);
           ElementosPermitidosNull.Add(44);
           ElementosPermitidosNull.Add(45);
           ElementosPermitidosNull.Add(46);
           ElementosPermitidosNull.Add(47);

           for(int o = 0; o < GridViewEvaluados.Rows.Count; o = o + 1)
           {

               
               if (int.Parse((dtEvaluaciones.Rows[o]).ItemArray[12].ToString()) == 1)
               {
                   //validamos si esta en condiciones de ser enviada
                   bool seEnvia;

                   seEnvia = validarRowActual(dtEvaluaciones.Rows[o]);



                   if (seEnvia)
                   {
                       enviar_evaluacion(int.Parse((dtEvaluaciones.Rows[o]).ItemArray[4].ToString()), false);
                       evaluacionesEnviadas += (dtEvaluaciones.Rows[o]).ItemArray[4].ToString();
                   }
               }
           }

           if (evaluacionesEnviadas != string.Empty)
               EjecutarScript("$( document ).ready(function() { alert('Evaluaciones enviadas exitosamente'); });" );
           else
               EjecutarScript("$( document ).ready(function() { alert('En este momento no existen evaluaciones para ser enviadas'); });");


            dtEvaluaciones = null;

            GridViewEvaluados.DataSource = dtEvaluaciones;
            GridViewEvaluados.DataBind();


       }

       protected void GridViewEvaluados_RowCreated(object sender, GridViewRowEventArgs e)
       {



       }

       protected void btnEditarTodas_Click(object sender, EventArgs e)
       {

           dtEvaluaciones = null;

           DataTable dtEvaluacionesAux = SelectDataTable(dtEvaluaciones, "[EstadoId] = 1", "");

           if (dtEvaluacionesAux.Rows.Count == 0)
           {
               EjecutarScript("$( document ).ready(function() { alert('En este momento no hay evaluaciones para editar');});");
           }
           else
           {

               isEditMode = true;
               GridViewEvaluados.DataSource = dtEvaluacionesAux;
               GridViewEvaluados.DataBind();
           }
       }

       protected void btncancelarTodas_Click(object sender, ImageClickEventArgs e)
       {
           dtEvaluaciones = null;
           GridViewEvaluados.DataSource = dtEvaluaciones;
           GridViewEvaluados.DataBind();
       }

       protected void btnGuardarTodas_Click(object sender, ImageClickEventArgs e)
       {

           dtEvaluaciones = SelectDataTable(dtEvaluaciones, "[EstadoId] = 1", "");
           isEditMode = true;

         
           for (int index = 0; index < GridViewEvaluados.Rows.Count; index++)
           {

               if (!Page.IsValid) return;

               int TipoFormularioID = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldTipoFormularioID") as HiddenField).Value);
               int PeriodoID = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldPeriodoID") as HiddenField).Value);
               int Legajo = int.Parse((GridViewEvaluados.Rows[index].FindControl("HiddenFieldLegajo") as HiddenField).Value);
               string Linea = "";
               string Evaluador = (GridViewEvaluados.Rows[index].FindControl("LabelEvaluador5") as Label).Text;
               string ComentarioEvaluador = string.Empty;

                   ComentarioEvaluador = (GridViewEvaluados.Rows[index].FindControl("editar_comentario") as TextBox).Text;
                    

               FC = new FormulariosController(PeriodoID, Legajo, TipoFormularioID);
               FC.SetComentarioJefeTurno(ComentarioEvaluador);
               string NombreDropDownList;
               string NombreLabel;
               int ItemEvaluacionID;
               String Fundamentacion = string.Empty;
               FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable DataTableItemEvaluacion = new FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionDataTable();
               FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.FillByID(DataTableItemEvaluacion, Legajo, PeriodoID, TipoFormularioID);

               try
               {
                   for (int i = 24; i < dtEvaluaciones.Columns.Count; i = i + 2)
                   {
                       ItemEvaluacionID = int.Parse(dtEvaluaciones.Columns[i].ColumnName);
                       DataRow[] rows = DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID);
                       NombreDropDownList = "DropDownListItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
                       DropDownList DropDownListItemsEvaluacion = (DropDownList)GridViewEvaluados.Rows[index].FindControl(NombreDropDownList);

                       HiddenField HiddenFieldItemEvaluacion = (HiddenField)GridViewEvaluados.Rows[index].FindControl("HiddenFieldItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName);

                       NombreLabel = "LabelItemEvaluacion" + dtEvaluaciones.Columns[i].ColumnName;
                       Label LabelItemEvaluacion = (Label)GridViewEvaluados.Rows[index].FindControl(NombreLabel);
                       //HiddenField LabelItemEvaluacion = (HiddenField)GridViewEvaluados.Rows[index].FindControl(NombreLabel);

                       if (rows.Count() > 0 && DropDownListItemsEvaluacion != null)
                       {
                           FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow PmpOperariosItemsEvaluacionRow = DataTableItemEvaluacion.Select("ItemEvaluacionID=" + ItemEvaluacionID)[0] as FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow;
                           Fundamentacion = HiddenFieldItemEvaluacion.Value;                        
                           int Ponderacion = int.Parse((GridViewEvaluados.Rows[index].FindControl(NombreDropDownList) as DropDownList).SelectedValue);
                           FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                       }

                   }
                   FC.SaveHistorial(1, PeriodoID, Legajo, TipoFormularioID);
                   FC.SetDatosCalibracion(ObjectUsuario.usuarioid);

                   FC.SetLineaEvaluado(Legajo, PeriodoID, TipoFormularioID, Linea);

                   FC.GrabarEvaluador();

               }
               catch (Exception exc)
               { }

           }
           isEditMode = false;
           EjecutarScript("__modificado = 0; $( document ).ready(function() { alert('Evaluaciones guardadas');});");
           Response.Redirect(Request.RawUrl);
                
       }
     
    
    }

}