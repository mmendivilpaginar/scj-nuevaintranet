﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteSeguimienotByEvaluadorAdm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["LegajoEvaluador"].ToString() == "-1")
                PanelEvaluador.Visible = false;
            if (Request.QueryString["LegajoEvaluador"].ToString() == "0")
                LabelSinEvaluador.Visible = true;
           
           
            if (Request.QueryString["DireccionID"].ToString() == "0")
                PanelDireccion.Visible = false;
            else
                GridViewEvaluados.Columns[3].Visible = false;

            switch (Request.QueryString["EstadoID"].ToString()) {
                case "NI":
                     LabelEstado.Text = "No Iniciadas";
                  GridViewEvaluados.Columns[7].Visible = false;
                  GridViewEvaluados.Columns[6].Visible = false;
                  GridViewEvaluados.Columns[5].Visible = false;
                  break;
                case "G":
                  LabelEstado.Text = "Grabado Previo";
                  GridViewEvaluados.Columns[7].Visible = false;
                  break;
                case "E":
                  LabelEstado.Text = "Enviadas";
                  GridViewEvaluados.Columns[7].Visible = true;
                  break;
                case "A":
                  LabelEstado.Text = "Aprobadas";
                  GridViewEvaluados.Columns[7].Visible = true;
                  break;

            }



            if (Request.QueryString["EstadoID"].ToString() == "-1" )
                PanelEstado.Visible = false;
           


        }

        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("--");
            else
                return (val.ToString());

        }

    }
}