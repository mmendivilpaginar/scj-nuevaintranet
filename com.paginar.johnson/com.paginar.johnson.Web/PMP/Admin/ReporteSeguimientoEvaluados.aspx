﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoEvaluados.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimientoEvaluados" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Evaluados</title>
    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style=" padding:5px !important;">
       <table width="100%" style=" padding:5px !important;" >
        <tr>
            <td>
    
    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
    <br />
    </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label3" runat="server" Text="Estado:" Font-Size="14px"></asp:Label> &nbsp;<asp:Repeater ID="RepeaterPaso" runat="server" 
        DataSourceID="ObjectDataSourceEstado">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralEstado" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
     <asp:ObjectDataSource ID="ObjectDataSourceEstado" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPasoID" 
        
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PasoID" QueryStringField="PasoID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </span>
       
               
   <span runat="server" ID="PanelArea">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelDirec" runat="server" Text="Área:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterArea" runat="server" 
        DataSourceID="ObjectDataSourceArea">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LabelArea" Text='<%# Eval("AreaDesc") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByAreaID" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="AreaID" QueryStringField="AreaID" 
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
    <br />
    </span>

    <span runat="server" ID="PanelDireccion">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelDireccion" runat="server" Text="Dirección:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterDireccion" runat="server" 
        DataSourceID="ObjectDataSourceDireccion">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LabelArea" Text='<%# Eval("DireccionDet") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <asp:ObjectDataSource ID="ObjectDataSourceDireccion" runat="server" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataByDireccionID" 
        TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="DireccionID" QueryStringField="DireccionID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    </span>

               
            </td>
            <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="../img/logoprint.jpg" /><br /><br /></td>
        </tr>
    </table>

      <div class="AspNet-GridViewRepPMP">
          <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
              DataSourceID="ObjectDataSourceEvaluados">
              <Columns>
                  <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                      SortExpression="Legajo" />
                  <asp:BoundField DataField="Evaluado" HeaderText="Evaluado" ReadOnly="True" 
                      SortExpression="Evaluado" />                  
                  <asp:BoundField DataField="direccion" HeaderText="direccion" ReadOnly="True" 
                      SortExpression="direccion" />
                  <asp:BoundField DataField="area" HeaderText="area" SortExpression="area" />
                  <asp:TemplateField HeaderText="Inicio Eval." SortExpression="FAlta" >
                        <ItemTemplate>
                        <div style=" text-align:center">
                            <asp:Label ID="LabelFAlta" runat="server" Text='<%# DefaultVal(Eval("FAlta","{0:dd/MM/yyyy}")) %>'></asp:Label>
                      </div>
                      </ItemTemplate>
                    </asp:TemplateField>
                  <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" 
                      SortExpression="PasoID" />
                  
              </Columns>
          </asp:GridView>
          <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" 
              OldValuesParameterFormatString="original_{0}" 
              SelectMethod="Eval_GetReporteSeguimientoEvaluados" 
              TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
              <SelectParameters>
                  <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                      Type="Int32" />
                  <asp:QueryStringParameter DefaultValue="0" Name="LegajoEvaluador" 
                      QueryStringField="LegajoEvaluador" Type="Int32" />
                  <asp:QueryStringParameter DefaultValue="" Name="Pasoid" 
                      QueryStringField="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                  <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                      QueryStringField="DireccionID" Type="Int32" />
                  <asp:QueryStringParameter Name="AreaID" QueryStringField="AreaID" 
                      Type="Int32" />
                  <asp:QueryStringParameter DefaultValue="0" Name="Calificacion" 
                      QueryStringField="Calificacion" Type="String" />
                  <asp:QueryStringParameter DefaultValue="" Name="Cluster" 
                      QueryStringField="Cluster" Type="String" />
              </SelectParameters>
          </asp:ObjectDataSource>
      </div>
    
    </div>
    </form>
</body>
</html>
