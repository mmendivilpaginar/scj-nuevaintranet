﻿<%@ Page Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master"AutoEventWireup="true" Inherits="com.paginar.johnson.Web.PMP.Admin.PMPCalibracion" Codebehind="PMPCalibracion.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="../OKMessageBox.ascx" tagname="OKMessageBox" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>--%>

    <script src="../../js/jquery.cluetip.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>--%>    
    <script type="text/javascript" src="../js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../js/jquery.fixedheadertable.js"></script>


    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../css/defaultTheme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
           

        function Onscrollfnction() {
            $('#tooltipFundamentacion' + document.getElementById('<%=HiddenFieldToopTipID.ClientID %>').value).trigger('hideCluetip');
            var HiddenFieldScrollValue = document.getElementById('<%=HiddenFieldScrollValue.ClientID %>');
            HiddenFieldScrollValue.value = $('div.fht-fixed-column').find('.fht-tbody').scrollTop();
            return false;
        }


        function ComprobarComentario(ItemEvaluacionID) {
            var TextArea = document.getElementById('textarea' + ItemEvaluacionID);
            // var Loadme = document.getElementById('ItemEvaluacionID');
            $('#tooltipFundamentacion' + ItemEvaluacionID).trigger('hideCluetip');
        }

        function AbrirToolTip(ItemEvaluacionID, ID) {
            var div = document.getElementById('ItemEvaluacionID');
            // object.value = LabelItemEvaluacion.value;
            var HiddenFieldScrollValue = document.getElementById('<%=HiddenFieldToopTipID.ClientID %>');
            HiddenFieldScrollValue.value = ItemEvaluacionID;
            var value = jQuery.trim(document.getElementById('textarea' + ID).value);
            if (value == "") {
                            document.getElementById('textarea' + ID).value = "Comentarios...";
                
            }
            $('#tooltipFundamentacion' + ItemEvaluacionID).trigger('hideCluetip');
            $('#tooltipFundamentacion' + ItemEvaluacionID).trigger('click.cluetip');
        }
        function CopyText(HiddenFielItemEvaluacion, e, ItemEvaluacionID, ID) {


            document.getElementById(HiddenFielItemEvaluacion).value = e.substring(0, 499);
            document.getElementById('textarea' + ID).value = e.substring(0, 499);

            if (e.length > 500) {
                $('#tooltipFundamentacion' + ItemEvaluacionID).trigger('hideCluetip');
                alert("Excedio la cantidad maxima de caracteres permitidos");
            }

        }

        function ValidarTextBoxFundamentacion(sender, args) {

            var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
            var TextOpcional = "Puede fundamentar la fortaleza indicada";
            args.IsValid = true;

            var DropDownListItemEvaluacion = sender.attributes["DropDownListItemEvaluacion"].value;

            var HiddenFieldItemEvaluacion = sender.attributes["HiddenFieldItemEvaluacion"].value;

            var LabelValidacion = sender.attributes["LabelValidacion"].value;

            if (($(DropDownListItemEvaluacion).val() == 1 || $(DropDownListItemEvaluacion).val() == 2) && ($(HiddenFieldItemEvaluacion).val() == "" || $(HiddenFieldItemEvaluacion).val() == TextRequerido || $(HiddenFieldItemEvaluacion).val() == ""))
             {
                args.IsValid = false;
                //$(LabelValidacion).css({ 'display': 'block' });
                $(LabelValidacion).css({ 'display': 'none' });
                $(DropDownListItemEvaluacion).attr("style", "border: 2px solid #FC3838;");
                var lock = document.getElementById('divPreloader');
                lock.className = 'LockOff';
                errorDetected();
            }

            
               
         
             
        }

        var __modificado = 0;
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperariosRepGeneral", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }


        function popupPmpO(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function Imprimir() {
            $('#PanelFiltros').hide();
            $('#PanelBotones').hide();
            window.print();
            $('#PanelFiltros').show();
            $('#PanelBotones').show();
        }
        $(document).ready(function () {
            $('table.tableHeaderFixed').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: true });
            $('a.tooltip').cluetip({ local: true, sticky: true, closePosition: 'title', arrows: true, activation: 'click' });
            $('div.fht-tbody').scroll(function () { Onscrollfnction();});            
            $('table.fht-table-init tr').each( function () { $(this).find('td:eq(0)').remove(); });
            $('tr.AspNet-GridView-Header th:eq(0)').remove();            
            $('table.fht-table-init tr')
                                       .each(
                                             function () {
                                                 var numrow = $(this).parent().children().index($(this)) + 1 ;
                                                 //if (numrow>1)
                                                 $(this).find('td:eq(0)').css({ 'height': $('table.fht-table tr:eq(' + numrow + ')').find('td:eq(0)').css('height') });

                     
                                             }
                                             );


            $(".defaultText").focus(function (srcc) {
                                                        if ($(this).val() == "Comentarios...") {
                                                            $(this).removeClass("defaultTextActive");
                                                            $(this).val("");
                                                        }
                                                    });

            $(".defaultText").blur(function () {
                                                if ($(this).val() == "") {
                                                    $(this).addClass("defaultTextActive");
                                                    $(this).val("Comentarios...");
                                                   }
                                              });

            $(".defaultText").blur();
            var HiddenFieldScrollValue = document.getElementById('<%=HiddenFieldScrollValue.ClientID %>');

            if (HiddenFieldScrollValue.value != "")
                $('div.fht-fixed-column').find('.fht-tbody').scrollTop(parseInt(HiddenFieldScrollValue.value));

        });


        $(window).unload(function () {
            var HiddenFieldScrollValue = document.getElementById('<%=HiddenFieldScrollValue.ClientID %>');
            HiddenFieldScrollValue.value = $('div.fht-fixed-column').find('.fht-tbody').scrollTop();
        });


        function Preloader() {
            var lock = document.getElementById('divPreloader');
            if (lock)
                lock.className = 'LockOn';
        }

        function PreloaderCancel() {
            if (confirm('¿Está seguro que desea cancelar los cambios realizados?')) {
                var lock = document.getElementById('divPreloader');
                if (lock)
                    lock.className = 'LockOn';
                return true;
            }
            else {
                return false;
            }
        }

        function PreloaderConfirm() {

            if (confirm('¿Confirma que desea enviar la/s evaluación/es?')) {
                var lock = document.getElementById('divPreloader');
                if (lock)
                    lock.className = 'LockOn';
                return true;
            }
            else {
                return false;
            }
        }

        function errorDetected() {
            var lock = document.getElementById('divError');
            if (lock)
                lock.className = 'errLockOn';
        }

        function errorClose() {
            var lock = document.getElementById('divError');
            if (lock)
                lock.className = 'errLockOff';

        }
        /*
        window.onunload = refreshParent;
        function refreshParent() {
            window.opener.location.reload();
        }*/
    </script>
     <style type="text/css">
 .cluetip-default #cluetip-inner a {
    font-size:12px;
    color:#FFFFFF;
    font-weight: bold;
    text-decoration:none;
}

.cluetip-default #cluetip-inner a:hover {text-decoration: underline; color: #FFFFFF; background-color:#2a3c6d;}


.bottDerecho
{
    padding-right: 48px !important;    
}

.enviarTop  
{
    
    float: right;
    padding-right: 31px;
    padding-bottom: 15px;
    vertical-align: middle;    
}
.enviarBott
{

    float: right;
    padding-top: 10px;    
    padding-right: 31px;
    vertical-align: middle;        
}
        
a:hover {
    background: none repeat scroll 0 0 #FFFFFF;
    text-decoration: none;
}
.tooltip {
    color: #666666;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    text-decoration: none;
}
a.tooltip:hover {
    color: #DA421A;
    text-decoration: none;
}
a.tooltip span {
    display: none;
    margin-left: 8px;
    padding: 2px 3px;
}
a.tooltip:hover span {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #CCCCCC;
    color: #6C6C6C;
    display: inline;
    position: absolute;
}     
 
.AspNet-GridView-Header{            
    border:1px solid #D9D9D9;
    font-size:12px;
    background-color:#E6E6E6;
    color:#666666 !important;
    text-transform:uppercase;
    font-weight:normal;  
    padding-left:5px !important;
    padding-right:5px !important; 

} 
          
tr.AspNet-GridView-Header th div{            
   
    width:180px!important;
}           

.AspNet-GridView table th
{    
	/*background: url(../../css/images/bg-content.gif") repeat-x scroll 0 0 rgba(0, 0, 0, 0) !important;  */
    background-image: url("/css/images/bg-content.gif")!important;
    Background-repeat: repeat-x !important;
    Background-position: scroll 0 0 !important;
    Background-color: rgba(0, 0, 0, 0) !important;	
	border: 1px; /*solid #D9D9D9;*/
    color: #558EC8;
    font: bold 12px Arial;	
}

.IcnCalEdit
{
    display: block;
    width: 23px;
    height: 23px;
    float:left;
    background: url("/images/calibracion/CalibracionEditar.png") no-repeat 0 0;    
    
}
.IcnCalEdit:hover
{
    background: url("/images/calibracion/CalibracionEditarHover.png") no-repeat 0 0;     
}

.IcnCalGrab
{
    display: block;
    width: 23px;
    height: 23px;
    float:left;
    background: url("/images/calibracion/CalibracionGuardar.png") no-repeat 0 0;    
    
}

.IcnCalGrab:hover
{
    background: url("/images/calibracion/CalibracionGuardarHover.png") no-repeat 0 0;         
}

.IcnCalCancel
{
    display: block;
    width: 23px;
    height: 23px;
    float:left;
    background: url("/images/calibracion/CalibracionCancel.png") no-repeat 0 0;     
}
.IcnCalCancel:hover
{
  background: url("/images/calibracion/CalibracionCancelHover.png") no-repeat 0 0;    
}

.IcnCalEnviar
{
    display: block;
    width: 23px;
    height: 23px;
    float:left;
    background: url("/images/calibracion/CalibracionEnviar.png") no-repeat 0 0;     
}

.IcnCalEnviar:hover
{

    background: url("/images/calibracion/CalibracionEnviarHover.png") no-repeat 0 0;           
}

.IcnCalImprimir
{
    display: block;
    width: 23px;
    height: 23px;
    float:left;
    background: url("/images/calibracion/CalibracionPrint.png") no-repeat 0 0;         
}

.IcnCalImprimir:hover
{
    background: url("/images/calibracion/CalibracionPrintHover.png") no-repeat 0 0;    
}

.AspNet-GridView-Edit  td {
    vertical-align:middle;
    }        
                        
.AspNet-GridView-Header table td {
    padding:5px !important;
    border:1px solid #D9D9D9;
    font-size:12px;
    border:1px  !important;
    padding-left:5px;
    padding-right:5px; 
    vertical-align:middle;
}
 .modalBackground {
	background-color: #000;
	filter: alpha(opacity=75);
	opacity: 0.75;
 }
 
.fht-thead{
    width:800px;
    overflow:hidden;
    height: 58px;
}
.fht-tbody{
    width:800px!important;
    height:400px;
}

.fht-table th{
    width:300px!important;
    font-weight:bold;
    text-align:center;
}
table.fht-table{
}
.fht-fixed-column .fht-thead{
    width:300px;
}

.fht-fixed-column .fht-tbody{
    width:300px!important;
}


.fht-fixed-column{
    float:left;
    overflow:hidden!important;
    width:299px!important;
}

.fht-fixed-column .fht-tbody{
    overflow:hidden!important;


}

.fht-fixed-body{
    float:left;
    width:600px!important;    
}

.IconosEdit 
{
   padding-left:10px;
   padding-right:10px;
    }

tr.AspNet-GridView-Header th{
    /*border:1px solid red!important;    */
    padding:5px!important;
    
}

.defaultText {  }
.defaultTextActive{
    color: #a1a1a1; 
    font-style: italic;
}

.fht-tbody{
    /*width: 392px !important;*/
}

/*.fht-thead .fht-table{
    margin-left:-3221px!important;

}
.fht-thead .fht-table tbody{
    margin-left:-3221px!important;

}


/*.fht-fixed-column tr{
    height:45px;
}

.fht-fixed-body tr{
    height:45px;

}

*/
body{
    scrollbar-base-color: #663366;
    scrollbar-face-color: #808080!important;
    scrollbar-track-color: #CCCCCC!important;
    scrollbar-arrow-color: #ffffff;
    scrollbar-highlight-color: #808080;
    scrollbar-3dlight-color: #808080;
    scrollbar-shadow-color: #808080;
    scrollbar-darkshadow-color: #808080;
}

        .LockOff {
            display: none;
            visibility: hidden;
        }

        .LockOn {
            display: block; 
            visibility: visible;
            position: absolute;
            z-index: 999;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            background-color: #fff;
            text-align: center;
            padding-top: 20%;
            filter: alpha(opacity=75);
            opacity: 0.75;
        }
        .PrProgress{
            width:300px;
            margin:0 auto;
        }
        .PrContainer{
            border: solid 1px #808080;
            border-width: 1px 0px;
        }
        .PrHeader{
            background: url(../../img/sprite.png) repeat-x 0px 0px;
            border-color: #808080 #808080 #ccc;
            border-style: solid;
            border-width: 0px 1px 1px;
            padding: 0px 10px;
            color: #000000;
            font-size: 9pt;
            font-weight: bold;
            line-height: 1.9;  
            white-space:nowrap;
            font-family: arial,helvetica,clean,sans-serif;
        }
        .PrBody{
            background-color: #f2f2f2;
            border-color: #808080;
            border-style: solid;
            border-width: 0px 1px;
            padding: 10px;
        }    

        .errLockOff {
            display: none;
            visibility: hidden;
        }

        .errLockOn {
            display: block; 
            visibility: visible;
            position: absolute;
            z-index: 999;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            background-color: #000;
            text-align: center;
            padding-top: 20%;
            filter: alpha(opacity=75);
            opacity: 0.90;
        }
        .errPrProgress{
            width:300px;
            margin:0 auto;
        }
        .errPrContainer{
            /*border: solid 1px #808080;
            border-width: 1px 0px;*/
        }
        .errPrHeader{
            background-color: #fff;
            border-width: 0px;
        }
        .errPrBody{
            background-color: #fff;
            border-width: 0px;            
        }   
        .errClose
        {
            margin-left: 289px;
        }    
        #Div2.errPrContainer{ width: 297px; height: 142px; 
        /* cambiar por la url de la imagen */ background-image: url(/images/calibracion/Calibrarmodal-bg.png); }

        .errClose{ position: absolute; top: -5px; right: -5px; width: 28px; height: 28px;
        /* cambiar por la url de la imagen */ background-image: url(/images/calibracion/Calibrarmodal-cerrar.png); }

        .errClose a img, .errClose a img:hover{ opacity: 0 }
        .errClose a:hover{background: none!important;}
        .errPrProgress{ position: relative; }
        .errPrHeader { display: none; }
        .errPrBody { background: none!important; padding-top: 74px; }
             
</style>
       
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<uc1:OKMessageBox ID="OKMessageBox1" runat="server" />
       <div id="divPreloader" class="LockOff">
            <div class="PrProgress">
                <div id="innerPopup2" class="PrContainer">
                    <div class="PrHeader">
                        Procesando Datos...                        
                    </div>
                    <div class="PrBody">
                        <img src="/images/activity.gif" alt="Cargando.." />
                    </div>           
                </div>
            </div>
        </div>  
   
       <div id="divError" class="errLockOff">
       
            <div class="errPrProgress">
            <div class="errClose">
                <a href="javascript:void(0);" onclick="errorClose();">
                        <img src="/images/calibracion/CalibracionClose.png" alt="Cerrar" />       
                </a>
            </div>
                <div id="Div2" class="errPrContainer">
                    <div class="errPrHeader">
                        <img src="/images/calibracion/CalibracionExclamation.png" alt="Atencion" />
                    </div>
                    <div class="errPrBody">
                        Falta ingresar comentarios
                    </div>           
                </div>
            </div>
        </div> 
      
    <div class="buttons" id="PanelTitulo">
    <br />
     <b>Módulo de Calibración  PMP PLANTA</b>
        
     <br />
    <br />
    </div> 
     <asp:HiddenField ID="HiddenFieldScrollValue" runat="server"  Value="0"/>
     <asp:HiddenField ID="HiddenFieldToopTipID" runat="server" />
     <asp:HiddenField ID="HiddenFieldEditRow" runat="server"  Value="-1"/>    



     
     
            <div id="HeaderDiv"></div>
            <div class="headerTable"></div>
             
              
            <div id="DataDiv" onscroll="Onscrollfnction();">

            <%--<asp:ImageButton ID="EnviarTodo" runat="server" CssClass="enviarTop" 
                    ImageUrl="~/images/calibracion/CalibracionEnviarTodo.png" OnClientClick="PreloaderConfirm();" 
                    onclick="EnviarTodo_Click">
            </asp:ImageButton>--%>            
                <asp:GridView ID="GridViewEvaluados" runat="server" CssClass="tableHeaderFixed"
                    onprerender="GridViewEvaluados_PreRender" AutoGenerateColumns="False" 
                    onrowcommand="GridViewEvaluados_RowCommand" 
                    onrowediting="GridViewEvaluados_RowEditing"                    
                    onrowupdating="GridViewEvaluados_RowUpdating" 
        onrowcancelingedit="GridViewEvaluados_RowCancelingEdit" 
                    onrowdatabound="GridViewEvaluados_RowDataBound" 
                    onrowcreated="GridViewEvaluados_RowCreated" >
                   <Columns>
                      <%-- <asp:CommandField ShowEditButton="True" ButtonType="Image" EditImageUrl="~/images/page_edit.png"
                        CancelImageUrl="~/images/cancel.png" ValidationGroup="Grabar" UpdateImageUrl="~/images/page_save.png" />--%>
                       <asp:TemplateField HeaderText="Apellido y Nombre" SortExpression="Legajo">
                           <ItemTemplate>
                              <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Bind("Legajo") %>' />
                               <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Bind("TipoFormularioID") %>' />
                               <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("PeriodoID") %>' />
                               <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" Value='<%# Bind("EstadoID") %>' />
                               <asp:Label ID="LabelLegajo" runat="server" Text='<%# Eval("ApellidoNombre")%>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                              <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Bind("Legajo") %>' />
                               <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Bind("TipoFormularioID") %>' />
                               <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("PeriodoID") %>' />
                               <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" Value='<%# Bind("EstadoID") %>' />
                               <asp:Label ID="Labellegajo" runat="server" Text='<%# Eval("ApellidoNombre") %>'></asp:Label>                               
                           </EditItemTemplate>
                           <HeaderStyle  Width="66px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Apellido y Nombre" SortExpression="ApellidoNombre" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="LabelApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="LabelApellidoNombre1" runat="server" Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                            </EditItemTemplate>
                            <HeaderStyle  Width="217px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Cargo" SortExpression="Cargo">
                           <ItemTemplate>
                               <asp:Label ID="LabelCargo" runat="server" Text='<%# Bind("Cargo") %>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                               <asp:Label ID="LabelCargo1" runat="server" Text='<%# Bind("Cargo") %>'></asp:Label>
                           </EditItemTemplate>
                           <HeaderStyle  Width="271px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Área" SortExpression="Area">
                           <ItemTemplate>
                               <asp:Label ID="LabelArea" runat="server" Text='<%# Bind("Area") %>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                               <asp:Label ID="LabelArea2" runat="server" Text='<%# Bind("Area") %>'></asp:Label>
                           </EditItemTemplate>
                           <HeaderStyle  Width="198px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Línea" SortExpression="Linea" ShowHeader="False" 
                           Visible="False">
                           <ItemTemplate>
                               <asp:Label ID="LabelLinea" runat="server" Text='<%# Bind("Linea") %>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                               <asp:TextBox ID="TextBoxLinea" runat="server" Text='<%# Bind("Linea") %>' MaxLength="200"></asp:TextBox>
                           </EditItemTemplate>
                           <HeaderStyle  Width="52px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Líder" SortExpression="Evaluador">
                           <ItemTemplate>
                               <asp:Label ID="LabelEvaluador5" runat="server" Text='<%# Bind("Evaluador") %>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                               <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label>
                           </EditItemTemplate>
                           <HeaderStyle  Width="207px"/>
                       </asp:TemplateField>                        
                       <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                            <ItemTemplate>
                                <asp:Label ID="LabelEstado1" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="LabelEstado1" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                            </EditItemTemplate>
                            <HeaderStyle  Width="97px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Calificación" SortExpression="Calificacion">
                            <ItemTemplate>
                                <asp:Label ID="LabelCalificacion2" runat="server" Text='<%# Bind("Calificacion") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="LabelCalificacion2" runat="server" Text='<%# Eval("Calificacion") %>'></asp:Label>
                            </EditItemTemplate>
                            <HeaderStyle  Width="312px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Sector" SortExpression="Sector" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="LabelSector3" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="LabelSector" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                            </EditItemTemplate>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Fecha Calibración" 
                           SortExpression="fcalibracion">
                            <ItemTemplate>
                                <asp:Label ID="LabelFCalibracion1" runat="server" Text='<%# Eval("fcalibracion","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="LabelFCalibracion2" runat="server" Text='<%# Eval("fcalibracion","{0:dd/MM/yyyy}") %>'></asp:Label>
                            </EditItemTemplate>
                            <HeaderStyle  Width="60px"/>
                       </asp:TemplateField>
                       <asp:TemplateField HeaderText="Usuario que Modificó" 
                           SortExpression="usuariocalibracion">
                           <ItemTemplate>
                               <asp:Label ID="Labelusuariocalibracion1" runat="server" Text='<%# Bind("usuariocalibracion") %>'></asp:Label>
                           </ItemTemplate>
                           <EditItemTemplate>
                               <asp:Label ID="Labelusuariocalibracion2" runat="server" Text='<%# Eval("usuariocalibracion") %>'></asp:Label>
                           </EditItemTemplate>
                           <HeaderStyle  Width="312px"/>
                       </asp:TemplateField>
                       
                    </Columns>
                    <EmptyDataTemplate>
                        En este momento no hay evaluaciones para mostrar.
                    </EmptyDataTemplate>
                </asp:GridView>            
 
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" 
                    Visible="False" />
 
            <asp:ImageButton ID="EnviarTodoDos" runat="server" CssClass="enviarBott bottDerecho" 
                    ImageUrl="~/images/calibracion/CalibracionEnviarTodo.png" OnClientClick="return PreloaderConfirm();"
                    onclick="EnviarTodo_Click">
            </asp:ImageButton> 

            <asp:ImageButton ID="btnCancelarTodas" runat="server" CssClass="enviarBott bottDerecho" 
                ImageUrl="~/images/calibracion/CalibracionCancelarTodas.png" OnClientClick="return PreloaderCancel();" 
                onclick="btncancelarTodas_Click">
            </asp:ImageButton>
            <asp:ImageButton ID="btnGuardarTodas" runat="server" CssClass="enviarBott" 
                ImageUrl="~/images/calibracion/CalibracionGuardarTodas.png" OnClientClick="Preloader();" 
                onclick="btnGuardarTodas_Click" ValidationGroup="Grabar">
            </asp:ImageButton>
            
            <asp:ImageButton ID="btnEditarTodas" runat="server" CssClass="enviarBott" 
                ImageUrl="~/images/calibracion/CalibracionEditarTodas.png" OnClientClick="Preloader();"
                onclick="btnEditarTodas_Click">
            </asp:ImageButton>              


                
             
                
           </div>
  
             
    <%-- <asp:CommandField ShowEditButton="True" ButtonType="Image" EditImageUrl="~/images/page_edit.png"
                        CancelImageUrl="~/images/cancel.png" ValidationGroup="Grabar" UpdateImageUrl="~/images/page_save.png" />--%><%-- <asp:CommandField ShowEditButton="True" ButtonType="Image" EditImageUrl="~/images/page_edit.png"
                        CancelImageUrl="~/images/cancel.png" ValidationGroup="Grabar" UpdateImageUrl="~/images/page_save.png" />--%><%--<asp:Button ID="btnEditarTodasx" runat="server" Text="Editar Todas" 
                    onclick="btnEditarTodas_Click"  />--%>
    
         
</asp:Content>


