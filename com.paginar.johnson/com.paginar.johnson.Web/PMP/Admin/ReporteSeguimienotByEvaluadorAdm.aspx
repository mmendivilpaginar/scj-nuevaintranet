﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimienotByEvaluadorAdm.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimienotByEvaluadorAdm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
<script  type="text/javascript">
    function popup(url, ancho, alto) {
        var posicion_x;
        var posicion_y;
        posicion_x = (screen.width / 2) - (ancho / 2);
        posicion_y = (screen.height / 2) - (alto / 2);
        window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

    }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%">
        <tr>
            <td>
   <span runat="server" ID="PanelEvaluador">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="LabelEvaluador" runat="server" Text="Jefe:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="LabelSinEvaluador" CssClass="destacados"  Text="Sin Evaluador" runat="server" Font-Size="12px" Visible="false"></asp:Label>
    <asp:Repeater ID="RepeaterEvaluador" runat="server" 
        DataSourceID="ObjectDataSourceEvaluador">
        <ItemTemplate>
            <asp:Label ID="LiteralEvaluador" CssClass="destacados"  Text='<%# Eval("ApellidoNombre") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />         
    </span>
    
    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label> &nbsp;<asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelEstadoTitulo" runat="server" Text="Estado:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label ID="LabelEstado" CssClass="destacados"  Text="" runat="server" Font-Size="12px"></asp:Label>
    <br />
    </span>
               
   <span runat="server" ID="PanelDireccion">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelDirec" runat="server" Text="Dirección:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="Repeater1" runat="server" 
        DataSourceID="ObjectDataSourceDireccion">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LabelDireccion" Text='<%# Eval("DireccionDet") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>

                &nbsp;
            </td>
            <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" /><br /></td>
        </tr>
    </table>
   <div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones">
        <Columns>
            <asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" 
                Visible="False" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
           <%-- <asp:BoundField DataField="tra_id" HeaderText="ID" 
                SortExpression="tra_id" />--%>
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="direccionevaluado" HeaderText="Dirección" SortExpression="Area" />
            <asp:BoundField DataField="Cargoevaluado" HeaderText="Cargo" SortExpression="Cargo" />
            <asp:BoundField DataField="tra_Fhalta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Inicio Evaluación" SortExpression="Falta" />  
            <asp:BoundField DataField="ultimaaccion" HeaderText="Ultima Acción" 
                SortExpression="ultimaaccion" /> 
            <asp:TemplateField HeaderText="Calificación" SortExpression="CalificacionTotal">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server"  Text='<%# DefaultVal(Eval("CalificacionTotal")) %>'></asp:Label>
                </ItemTemplate>                
            </asp:TemplateField>            

        </Columns>
    </asp:GridView>
   </div>
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
        TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="LegajoEvaluador" 
                QueryStringField="LegajoEvaluador" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="Estadoid" 
                QueryStringField="EstadoID" Type="String" />
                <asp:QueryStringParameter DefaultValue="0" Name="DireccionID" 
                QueryStringField="DireccionID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluador" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBylegajo" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="legajo" QueryStringField="LegajoEvaluador" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="ObjectDataSourceDireccion" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.DireccionTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_DireccionID" Type="Int32" />
            <asp:Parameter Name="Original_DireccionDET" Type="String" />
            <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="DireccionDET" Type="String" />
            <asp:Parameter Name="CodigoPXXi" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="0" Name="DireccionID" 
                QueryStringField="DireccionID" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="DireccionDET" Type="String" />
            <asp:Parameter Name="CodigoPXXi" Type="Int32" />
            <asp:Parameter Name="Original_DireccionID" Type="Int32" />
            <asp:Parameter Name="Original_DireccionDET" Type="String" />
            <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <br />

    </form>
</body>
</html>
