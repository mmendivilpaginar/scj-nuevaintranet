﻿<%@ Page Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master"AutoEventWireup="true" Inherits="PMP_Admin_ReporteGeneral" Codebehind="ReporteGeneral.aspx.cs" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <%-- <link rel="stylesheet" href="../../css/jquery.cluetip.css" type="text/css" />--%>
    <script src="../../js/jquery.cluetip.min.js" type="text/javascript"></script>

    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#header').hide();           

        });
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperariosRepGeneral", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }
                 

        function popupPmpO(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function Imprimir() {
            $('#PanelFiltros').hide();
           // $('#PanelBotones').hide();
           // $('#PanelTitulo').hide();
            $('.buttons').hide();
            $('#header').show();                 
            window.print();
            $('#header').hide();
            $('.buttons').show();
           // $('#PanelTitulo').show();      
            $('#PanelFiltros').show();
           // $('#PanelBotones').show();
        }

        function ImprimirPMPOcultas() {
            $('#PanelFiltros').hide();
            $('.buttons').hide();
            $('#header').show();
            MostrarOcultarColumna('<%= GridViewFormularios.ClientID %>', 0, 'hide');
            window.print();
            $('#header').hide();
            $('.buttons').show();          
            $('#PanelFiltros').show();
          
        }

        function ImprimirCalificaciones() {
            $('#PanelFiltros').hide();
           // $('#PanelBotonesCalificaciones').hide();
            // $('#PanelTitulo').hide(); 
            $('.buttons').hide();           
            $('#header').show();
            MostrarOcultarColumna('<%= GridViewReportCalificaciones.ClientID %>',0, 'hide');
            window.print();
            $('#header').hide();
            $('.buttons').show();
            $('#PanelFiltros').show();
           // $('#PanelTitulo').show();
           // $('#PanelBotonesCalificaciones').show();
            MostrarOcultarColumna('<%= GridViewReportCalificaciones.ClientID %>', 0, 'show');
        }

        function ImprimirReporteGeneral() {
            $('#PanelFiltros').hide();
            // $('#PanelTitulo').hide();     
            $('.buttons').hide();            
            MostrarOcultarColumna('gvReporteGeneral', 12, 'hide');
            $('#header').show();
            window.print();
            $('#header').hide();
            $('#PanelTitulo').show();      
            // $('#PanelFiltros').show();
            $('.buttons').show();           
            MostrarOcultarColumna('gvReporteGeneral', 12, 'show');
        }

        function MostrarOcultarColumna(Grilla, columna, modo) {

            var aux = document.getElementById(Grilla);
            var grid;
            if (aux.nodeName == "TABLE")
                grid = aux;
            else
                grid = aux.children[0];

                      
            var cell1;                       
            var row;
            if (grid.rows.length > 0) {                
                for (i = 0; i < grid.rows.length; i++) {
                    //get the reference of first column 
                    row = grid.rows[i];
                    //var ImageVer = grid.rows[i].getElementById('ImageCommand');
                    cell1 = grid.rows[i].cells[columna];
                    if (cell1) {
                        if (modo == 'hide')
                            $(cell1).hide();
                        else
                            $(cell1).show();
                    }
                    else {
                        cell1 = grid.rows[i].cells[columna - 1];
                        if (cell1) {
                            var ImagVer = cell1.childNodes[0];
                            if (ImagVer) {
                                if (modo == 'hide')
                                    $(cell1).hide();
                                else
                                    $(cell1).show();
                            } 
                        }
                    }
                }
            }            
        }

    </script>
     <style type="text/css">
        .AspNet-GridViewReportes {
            margin-bottom:10px;
            overflow-x:auto;
            overflow-y:hidden;
            clear:both;
        }
        .AspNet-GridViewReportes table{
            width:100%;
            border: 1px solid rgb(204, 204, 204);
        }
        .AspNet-GridViewReportes table th,
        .AspNet-GridViewReportes table td {
            padding:2px;
            border:1px solid #FFFFFF;
            font-size:12px;
            vertical-align:top;
        }

        .AspNet-GridViewReportes th{
            background-color:#E6E6E6;
            color:#666666;
            text-transform:uppercase;
            font-weight:normal;
     }  
     
     
      a:hover
      {
         background: #ffffff;
         text-decoration: none;
      }
      .tooltip {  font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666666; text-decoration: none}
      a.tooltip:hover { color: #DA421A; text-decoration: none }
      /*BG color is a must for IE6*/a.tooltip span
      {
         display: none;
         padding: 2px 3px;
         margin-left: 8px;
         
      }
      a.tooltip:hover span
      {
         display: inline;
         position: absolute;
         background: #ffffff;
         border: 1px solid #cccccc;
         color: #6c6c6c;
      }
      
    .buttons{
    margin-bottom:15px;
    overflow:hidden;
    clear:both;
    padding-left:2%;
     }
     
     .labelTituloPrint
    {
    font-size:25px; 
    font-weight:normal; 
    color:#333; 
    background:none; 
    float:left; margin:0; 
    text-indent: 0px;
    }
      
   </style>
       
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="buttons" id="PanelTitulo">
    <br />  
    <u><b>Reportes PMP Full Year - Operarios</b></u>
    <br />
    <br />
   </div>
<div class="box resultados" id="PanelFiltros">
   
    
<div class="form-item leftthird">
                <label class="descripcion"> Período <br /></label>                 
               
                 <asp:DropDownList ID="ddlPeriodo" runat="server" DataSourceID="ObjectDataSourcePeriodos"
                  DataTextField="Descripcion" DataValueField="PeriodoID" CssClass="formselect">  </asp:DropDownList> 
                  <asp:ObjectDataSource ID="ObjectDataSourcePeriodos" runat="server" DeleteMethod="Delete"
                            InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTipoPeriodoID"
                            TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter"
                            UpdateMethod="Update">
                            <DeleteParameters>
                                <asp:Parameter Name="PeriodoID" Type="Int32" />
                            </DeleteParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Descripcion" Type="String" />
                                <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                <asp:Parameter Name="PeriodoID" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Descripcion" Type="String" />
                                <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                <asp:Parameter Name="PeriodoID" Type="Int32" />
                            </InsertParameters>
                        </asp:ObjectDataSource> 
  </div>
  <div class="form-item middlethird">
             
                 <label>Tipo Reporte<br /></label>
                 <asp:DropDownList ID="ddlTipoReporte" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlTipoReporte_SelectedIndexChanged" CssClass="formselect">
                        
                        <asp:ListItem Value="2">PMP Operarios</asp:ListItem>
                        <asp:ListItem Value="1">Reporte por competencias</asp:ListItem>                        
                        <asp:ListItem Value="3">Reporte por objetivos</asp:ListItem>                        
                        <asp:ListItem Value="8">Reporte por Resultado/competencias</asp:ListItem>
                        <asp:ListItem Value="4">Reporte por Calificaciones</asp:ListItem>
                        <asp:ListItem Value ="5">Reporte por status</asp:ListItem>
                        <asp:ListItem Value="6">Reporte por PMP Ocultas</asp:ListItem>
                        <asp:ListItem Value="7">Reporte por Áreas de Interés</asp:ListItem>
                        <asp:ListItem Value="9">Reporte por Comentarios</asp:ListItem>
                        </asp:DropDownList> 
</div>
<div class="form-item rightthird">
                 <label>Formularios<br /></label>
             
                 <asp:ListBox ID="ListBoxFormularios" runat="server" CssClass="form-select"
                      DataSourceID="ObjectDataSourceFormularios" DataTextField="Descripcion" 
                     DataValueField="TipoFormularioID" SelectionMode="Multiple" Rows="3" 
                     ondatabound="ListBoxFormularios_DataBound"  >
                              <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                          </asp:ListBox>
                 <asp:ObjectDataSource ID="ObjectDataSourceFormularios" runat="server" 
                     DeleteMethod="Delete" InsertMethod="Insert" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTipoPeriodoID" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.TipoFormularioTableAdapter" 
                     UpdateMethod="Update">
                              <DeleteParameters>
                                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                                  <asp:Parameter Name="Original_Descripcion" Type="String" />
                              </DeleteParameters>
                              <SelectParameters>
                                  <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                              </SelectParameters>
                              <UpdateParameters>
                                  <asp:Parameter Name="Descripcion" Type="String" />
                                  <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                                  <asp:Parameter Name="Original_Descripcion" Type="String" />
                              </UpdateParameters>
                              <InsertParameters>
                                  <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                                  <asp:Parameter Name="Descripcion" Type="String" />
                              </InsertParameters>
                          </asp:ObjectDataSource> 
</div>


<div class="form-item leftthird">
                <label>Evaluado<br /></label>
              
               <asp:ListBox ID="ListBoxEvaluado" runat="server" 
                      DataSourceID="ObjectDataSourceEvaluados" DataTextField="ApellidoNombre" 
                      DataValueField="Legajo" SelectionMode="Multiple"  CssClass="form-select" 
                    Rows="3" ondatabound="ListBoxEvaluado_DataBound">
                       <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                          </asp:ListBox>
               
               
                 <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" OldValuesParameterFormatString="original_{0}"
                       SelectMethod="GetDataByPaso"     
                              TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                              <SelectParameters>
                                  <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID"
                        PropertyName="SelectedValue" Type="Int32" />
                                  <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                              </SelectParameters>
                          </asp:ObjectDataSource> 
</div>       
<div class="form-item middlethird">
                 <label>Sector<br /></label> 

                 <asp:ListBox ID="ListBoxArea" runat="server" 
                      DataSourceID="ObjectDataSourceAreas" DataTextField="AreaDESC" 
                     DataValueField="AreaID" SelectionMode="Multiple" CssClass="form-select" 
                     Rows="3" ondatabound="ListBoxArea_DataBound">
                              <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                          </asp:ListBox>
                <asp:ObjectDataSource ID="ObjectDataSourceAreas" runat="server" 
                    DeleteMethod="Delete" InsertMethod="Insert" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasTableAdapter" 
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_AreaID" Type="Int32" />
                        <asp:Parameter Name="Original_AreaDESC" Type="String" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="AreaDESC" Type="String" />
                        <asp:Parameter Name="Original_AreaID" Type="Int32" />
                        <asp:Parameter Name="Original_AreaDESC" Type="String" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="AreaDESC" Type="String" />
                    </InsertParameters>
                </asp:ObjectDataSource> 
</div>
<div class="form-item rightthird">           
                  
                  
                  <asp:ListBox ID="ListBoxSector" runat="server" visible="false" st
                      DataSourceID="ObjectDataSourceSector" DataTextField="Descripcion" 
                      DataValueField="SectorID" SelectionMode="Multiple" 
                      AppendDataBoundItems="True" CssClass="form-select" Rows="3" 
                      ondatabound="ListBoxSector_DataBound">
                       <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                          </asp:ListBox>
                  <asp:ObjectDataSource ID="ObjectDataSourceSector" runat="server" 
                      DeleteMethod="Delete" InsertMethod="Insert" 
                      OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                      TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.SectorTableAdapter" 
                      UpdateMethod="Update">
                      <DeleteParameters>
                          <asp:Parameter Name="Original_SectorID" Type="Int32" />
                          <asp:Parameter Name="Original_Descripcion" Type="String" />
                          <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                      </DeleteParameters>
                      <InsertParameters>
                          <asp:Parameter Name="SectorID" Type="Int32" />
                          <asp:Parameter Name="Descripcion" Type="String" />
                          <asp:Parameter Name="SectorIDPXXI" Type="String" />
                      </InsertParameters>
                      <UpdateParameters>
                          <asp:Parameter Name="Descripcion" Type="String" />
                          <asp:Parameter Name="SectorIDPXXI" Type="String" />
                          <asp:Parameter Name="Original_SectorID" Type="Int32" />
                          <asp:Parameter Name="Original_Descripcion" Type="String" />
                          <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                      </UpdateParameters>
                  </asp:ObjectDataSource>
</div> 

<div class="form-item leftthird">  
                 <label>Auditor<br /></label>
                <asp:ObjectDataSource ID="ObjectDataSourceAuditores" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="3" Name="PasoID" Type="Int32" />
                        <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ListBox ID="ListBoxAuditor" runat="server" 
                   DataSourceID="ObjectDataSourceAuditores" 
                    DataTextField="ApellidoNombre" DataValueField="Legajo" 
                    SelectionMode="Multiple" CssClass="form-select" Rows="3" 
                     ondatabound="ListBoxAuditor_DataBound" >
                    <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                </asp:ListBox> 
</div>     
<div class="form-item middlethird">
            
                <label>Estado<br /></label>
                 
                <asp:ListBox ID="ListBoxEstado" runat="server" 
                     DataSourceID="ObjectDataSourceEstados" 
                    DataTextField="Descripcion" DataValueField="PasoID" 
                    SelectionMode="Multiple" CssClass="form-select" Rows="3" 
                    ondatabound="ListBoxEstado_DataBound">
                    <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                    <asp:ListItem Value="-1">No iniciado</asp:ListItem>
                </asp:ListBox>
                        <asp:ObjectDataSource ID="ObjectDataSourceEstados" runat="server" 
             DeleteMethod="Delete" InsertMethod="Insert" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetTodos" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter" 
             UpdateMethod="Update">
             <DeleteParameters>
                 <asp:Parameter Name="Original_PasoID" Type="Int32" />
                 <asp:Parameter Name="Original_Descripcion" Type="String" />
             </DeleteParameters>
             <UpdateParameters>
                 <asp:Parameter Name="Descripcion" Type="String" />
                 <asp:Parameter Name="Original_PasoID" Type="Int32" />
                 <asp:Parameter Name="Original_Descripcion" Type="String" />
             </UpdateParameters>
             <InsertParameters>
                 <asp:Parameter Name="PasoID" Type="Int32" />
                 <asp:Parameter Name="Descripcion" Type="String" />
             </InsertParameters>
         </asp:ObjectDataSource>
</div>
<div class="form-item rightthird">
              
                 <label>Resultado<br /></label>    
                <asp:ListBox ID="ListBoxResultado" runat="server" SelectionMode="Multiple" 
                    CssClass="form-select" Rows="3">
                    <asp:ListItem Selected="True" Value="0" >Todos</asp:ListItem>
                    <asp:ListItem Value="No Satisfactorio">No Satisfactorio</asp:ListItem>
                    <asp:ListItem Value="Regularmente cumple / Necesita desarrollarse">Regularmente cumple / Necesita desarrollarse</asp:ListItem>
                    <asp:ListItem Value="Bueno">Bueno</asp:ListItem>
                    <asp:ListItem Value="Muy Bueno">Muy Bueno</asp:ListItem>
                    
                    
                    
                </asp:ListBox> 
</div>

<div class="form-item leftthird">
                 <label>Evaluador<br /></label> 
                <asp:ListBox ID="ListBoxEvaluador" runat="server" 
                    CssClass="form-select" DataSourceID="ObjectDataSourceEvaluadores" 
                    DataTextField="ApellidoNombre" DataValueField="Legajo" 
                    SelectionMode="Multiple" Rows="3" ondatabound="ListBoxEvaluador_DataBound">
                    <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                </asp:ListBox>
                <asp:ObjectDataSource ID="ObjectDataSourceEvaluadores" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                        <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource> 
</div>
<div class="form-item middlethird">
                 <label>Competencia<br /></label>
              <asp:ListBox ID="ListBoxCompetencia" runat="server" 
                    CssClass="form-select" DataSourceID="ObjectDataSourceCompetencias1" 
                    DataTextField="Titulo" DataValueField="ItemEvaluacionID"  
                    SelectionMode="Multiple" Rows="3" 
                     ondatabound="ListBoxCompetencia_DataBound">
                    <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                </asp:ListBox>
                 <asp:ObjectDataSource ID="ObjectDataSourceCompetencias1" runat="server" 
                     DeleteMethod="Delete" InsertMethod="Insert" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBySeccionID" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                     UpdateMethod="Update">
                     <DeleteParameters>
                         <asp:Parameter Name="ItemEvaluacionID" Type="Int32" />
                     </DeleteParameters>
                     <InsertParameters>
                         <asp:Parameter Name="Titulo" Type="String" />
                         <asp:Parameter Name="Descripcion" Type="String" />
                         <asp:Parameter Name="SeccionID" Type="Int32" />
                         <asp:Parameter Name="Activo" Type="Boolean" />
                         <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                     </InsertParameters>
                     <SelectParameters>
                         <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                     </SelectParameters>
                     <UpdateParameters>
                         <asp:Parameter Name="Titulo" Type="String" />
                         <asp:Parameter Name="Descripcion" Type="String" />
                         <asp:Parameter Name="SeccionID" Type="Int32" />
                         <asp:Parameter Name="Activo" Type="Boolean" />
                         <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                         <asp:Parameter Name="ItemEvaluacionID" Type="Int32" />
                     </UpdateParameters>
                 </asp:ObjectDataSource>
             
</div>
<div class="form-item rightthird">
                 <label>Resultado Comp<br /></label>
                 <asp:ListBox ID="ListBoxResultadoComp" runat="server" SelectionMode="Multiple" 
                    CssClass="form-select" Rows="3">
                    <asp:ListItem Selected="True" Value="0" >Todos</asp:ListItem>
                    <asp:ListItem Value="1">No Satisfactorio</asp:ListItem>
                    <asp:ListItem Value="2">Regularmente cumple / Necesita desarrollarse</asp:ListItem>
                    <asp:ListItem Value="3">Bueno</asp:ListItem>
                    <asp:ListItem Value="4">Muy Bueno</asp:ListItem>
                    
                    
                    
                </asp:ListBox>
</div>
<div class="form-item leftthird">
<asp:Button ID="ButtonBuscar" runat="server" OnClick="ButtonBuscar_Click" Text="Buscar" />
<asp:Button ID="ButtonImprimir" runat="server"  Text="Imprimir"  OnClientClick="ImprimirReporteGeneral()" Visible="false"/>
<asp:Button ID="ButtonExportar" runat="server"  Text="Exportar a Excel" onclick="ButtonExportarExcel_Click" Visible="false" />
</div>
<div class="form-item middlethird"></div>
<div class="form-item rightthird"> 
</div>
</div> <!-- cierre div panelfiltros-->
       <div id="header" style="overflow:hidden; zoom:1; padding:20px; margin-bottom: 20px;position:relative; background:none; height:70px;">
            <div style="position: absolute; top:0; left:0; width: 100%; height:30px; border-top:100px solid #ccc; z-index:-1;"></div>
            <asp:Label ID="LabelTituloPrint" runat="server" Text="Label" CssClass="labelTituloPrint"></asp:Label>            
            <img  src="../../images/logoprint.gif" style="float:right;" />
        </div>
   
     <asp:MultiView ID="MultiViewReportes" runat="server">
             <asp:View ID="vwReporteResultadosCompetencias" runat="server">
                 <asp:ObjectDataSource ID="ObjectDataSourceResultadosCompetencias" 
                     runat="server" OldValuesParameterFormatString="original_{0}" 
                     SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorResultadoCompetenciasTableAdapter">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="ReporteResultadoCompetencias" Name="Modo" 
                             Type="String" />
                         <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxArea" Name="AreaID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxResultado" Name="Ponderacion" 
                             PropertyName="SelectedIndex" Type="Int32" />
                     </SelectParameters>
                 </asp:ObjectDataSource>
                 <br />
            <div class="buttons">
             <asp:Label ID="LabelRepoteResultadosCompetencias" runat="server" Text="Resultados de Búsqueda - PMP Planta" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
             <br />
              <br />
              </div>
                 <asp:GridView ID="GridViewReasResultados" runat="server" AutoGenerateColumns="False" 
                     DataKeyNames="AreaID" 
                     DataSourceID="ObjectDataSourceResultadosCompetencias" 
                     onrowdatabound="GridViewReasResultados_RowDataBound" 
                     
                     EmptyDataText="La búsqueda no produjo resultados para los parámetros indicados." 
                     onprerender="GridViewReasResultados_PreRender">
                     <Columns>
                         <asp:BoundField DataField="AreaDESC" HeaderText="Area" SortExpression="AreaDESC" Visible="False" />
                         <asp:BoundField DataField="AreaDESC" HeaderText="Area" 
                             SortExpression="AreaDESC" />
                         <asp:TemplateField HeaderText="Resultado" SortExpression="Calificacion">                              
                             <ItemTemplate>
                                 <%--<asp:Label ID="lblCalificacion" runat="server" Text='<%# Bind("Calificacion") %>'></asp:Label>--%>
                                 <asp:LinkButton ID="lnkBtnCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' CssClass="link"  >LinkButton</asp:LinkButton>
                             </ItemTemplate>                         
                         </asp:TemplateField>
                         
                     </Columns>
                 </asp:GridView>
             </asp:View>
             <asp:View ID="vwReporteCompetencias" runat="server">
             <br />
             <asp:Label ID="lblTitulo2" runat="server" Text="Resultados de Búsqueda - Competencias" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
             <br />
                 <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter" 
                     onselected="ObjectDataSourceCompetencias_Selected">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="ReporteCompetencias" Name="Modo" Type="String" />
                         <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:Parameter Name="titulo" Type="String" DefaultValue="" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:Parameter Name="PasoID" Type="Int32" />
                         <asp:Parameter DefaultValue="" Name="Ponderacion" Type="Int32" />
                         <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                     </SelectParameters>
                 </asp:ObjectDataSource>
          
         <br />
         <%--<asp:Label ID="lblSinResultados" runat="server" Text="La búsqueda no produjo resultados para los parámetros indicados." 
                CssClass="destacados"></asp:Label> --%>
          <div class="box resultados">
          <div class="AspNet-GridView">
          <asp:Repeater ID="RepeaterEvaluaciones" runat="server" 
                     DataSourceID="ObjectDataSourceCompetencias" 
                     onprerender="RepeaterEvaluaciones_PreRender">
            <HeaderTemplate>
                <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0">
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th align="center" >
                            Competencias
                        </th>
                      
                        <th align="center" colspan="3">
                            En Evaluado
                        </td>
                        <th align="center" colspan="3">
                            En Auditor
                        </th>
                        <th align="center" colspan="3">
                            Finalizado
                        </th>
                        <th align="center" colspan="2">
                            TOTAL
                        </th>
                    </tr>
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th>&nbsp;
                            
                        </th>
                        
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="destacados" style="background-color: WhiteSmoke;">
                    <td rowspan="4">
                    
                        <asp:HiddenField ID="HiddenFieldCantTotal" runat="server" Value='<%# Eval("CantidadTotal") %>' />
                        <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='<%# Eval("Titulo") %>' />
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    </td> 
               <%--</tr>--%>
              <%--<tr class="destacados" style="background-color: WhiteSmoke;">--%>
              <asp:Repeater ID="RepeaterMB" DataSourceID="ObjectDataSourceMB"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBEnEvaluado" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantEnEvaluado"), 1)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcEnEvaluado"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBEnJP" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantEnJP"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcEnJP"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBArpobadas" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantAprobadas"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcAprobadas"), 1)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantidadTotal"), 1)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcentajeTotal"), 1)%>
                    </td>
                            
                        </ItemTemplate>
         </asp:Repeater> 
              </tr>
              <asp:ObjectDataSource ID="ObjectDataSourceMB" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="4" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater1" DataSourceID="ObjectDataSourceB"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionBEnEvaluado" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantEnEvaluado"), 1)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcEnEvaluado"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionBEnJP" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantEnJP"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcEnJP"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionBAprobadas" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantAprobadas"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcAprobadas"), 1)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantidadTotal"), 1)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcentajeTotal"), 1)%>
                    </td>
                          
                        </ItemTemplate>
         </asp:Repeater> 
              </tr>  
               <asp:ObjectDataSource ID="ObjectDataSourceB" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="3" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater2" DataSourceID="ObjectDataSourceR"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionREnEvaluado" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantEnEvaluado"), 1)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcEnEvaluado"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionREnJP" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantEnJP"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcEnJP"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionRAprobadas" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantAprobadas"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcAprobadas"), 1)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantidadTotal"), 1)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcentajeTotal"), 1)%>
                    </td>
                          
                        </ItemTemplate>
         </asp:Repeater>  
              </tr> 
          <asp:ObjectDataSource ID="ObjectDataSourceR" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater3" DataSourceID="ObjectDataSourceNS"
                        runat="server">
                       
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantEnEvaluado"), 1)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcEnEvaluado"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnJP" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantEnJP"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcEnJP"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSAprobadas" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantAprobadas"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcAprobadas"), 1)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantidadTotal"), 1)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcentajeTotal"), 1)%>
                    </td>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             </tr>
          <asp:ObjectDataSource ID="ObjectDataSourceNS" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
           </ItemTemplate>
           <FooterTemplate>
           <tr class="calendarioMes" style="font-weight: bold;">
                <td>
                   <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='0' />
                    TOTAL
                </td>
                 <asp:Repeater ID="RepeaterTotal" DataSourceID="ObjectDataSourceTotal"
                        runat="server">
                       
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionTotal" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantEnEvaluado"), 1)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcEnEvaluado"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnJP" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantEnJP"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcEnJP"), 1)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSAprobadas" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantAprobadas"), 1)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcAprobadas"), 1)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantidadTotal"), 1)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcentajeTotal"), 1)%>
                    </td>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             </tr>
          <asp:ObjectDataSource ID="ObjectDataSourceTotal" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
               
           </FooterTemplate>    
           </asp:Repeater>                                          
          </div></div>
             </asp:View>
             <asp:View ID="vwReporteAspectos" runat="server">
             <br />
             <asp:Label ID="Label13" runat="server" Text="Resultados de Búsqueda - Objetivos" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
             <br />
                 <asp:ObjectDataSource ID="ObjectDataSourceAspectos" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter" 
                     onselected="ObjectDataSourceCompetencias_Selected">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="ReporteCompetencias" Name="Modo" Type="String" />
                         <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:Parameter Name="Titulo" Type="String" DefaultValue="" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:Parameter Name="PasoID" Type="Int32" />
                         <asp:Parameter DefaultValue="" Name="Ponderacion" Type="Int32" />
                         <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
                     </SelectParameters>
                 </asp:ObjectDataSource>
          
         <br />
         <%--<asp:Label ID="lblSinResultados" runat="server" Text="La búsqueda no produjo resultados para los parámetros indicados." 
                CssClass="destacados"></asp:Label> --%>
          <div class="box resultados">
          <div class="AspNet-GridView">
          <asp:Repeater ID="RepeaterAspectos" runat="server" 
                     DataSourceID="ObjectDataSourceAspectos" 
                     onprerender="RepeaterEvaluaciones_PreRender">
            <HeaderTemplate>
                <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0">
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th align="center" >
                            Objetivos
                        </th>
                      
                        <th align="center" colspan="3">
                            En Evaluado
                        </td>
                        <th align="center" colspan="3">
                            En Auditor
                        </th>
                        <th align="center" colspan="3">
                            Finalizado
                        </th>
                        <th align="center" colspan="2">
                            TOTAL
                        </th>
                    </tr>
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th>&nbsp;
                            
                        </th>
                        
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>&nbsp;
                            
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                        <th>
                            Cant
                        </th>
                        <th>
                            %
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="destacados" style="background-color: WhiteSmoke;">
                    <td rowspan="4">
                    
                        <asp:HiddenField ID="HiddenFieldCantTotal" runat="server" Value='<%# Eval("CantidadTotal") %>' />
                        <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='<%# Eval("Titulo") %>' />
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    </td> 
               <%--</tr>--%>
              <%--<tr class="destacados" style="background-color: WhiteSmoke;">--%>
              <asp:Repeater ID="RepeaterMB" DataSourceID="ObjectDataSourceMB"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBEnEvaluado" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantEnEvaluado"), 2)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcEnEvaluado"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBEnJP" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantEnJP"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcEnJP"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionMBArpobadas" runat="server" Text='MB'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantAprobadas"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcAprobadas"), 2)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("CantidadTotal"), 2)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 4, Eval("PorcentajeTotal"), 2)%>
                    </td>
                            
                        </ItemTemplate>
         </asp:Repeater> 
              </tr>
              <asp:ObjectDataSource ID="ObjectDataSourceMB" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="Titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="4" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater1" DataSourceID="ObjectDataSourceB"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionBEnEvaluado" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantEnEvaluado"), 2)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcEnEvaluado"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionBEnJP" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantEnJP"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcEnJP"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionBAprobadas" runat="server" Text='B'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantAprobadas"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcAprobadas"), 2)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("CantidadTotal"), 2)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 3, Eval("PorcentajeTotal"), 2)%>
                    </td>
                          
                        </ItemTemplate>
         </asp:Repeater> 
              </tr>  
               <asp:ObjectDataSource ID="ObjectDataSourceB" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="Titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="3" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater2" DataSourceID="ObjectDataSourceR"
                        runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionREnEvaluado" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantEnEvaluado"),2)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcEnEvaluado"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionREnJP" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantEnJP"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcEnJP"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionRAprobadas" runat="server" Text='R'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantAprobadas"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcAprobadas"), 2)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("CantidadTotal"), 2)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 2, Eval("PorcentajeTotal"), 2)%>
                    </td>
                          
                        </ItemTemplate>
         </asp:Repeater>  
              </tr> 
          <asp:ObjectDataSource ID="ObjectDataSourceR" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="Titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
              <tr class="destacados" style="background-color: WhiteSmoke;">
              <asp:Repeater ID="Repeater3" DataSourceID="ObjectDataSourceNS"
                        runat="server">
                       
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantEnEvaluado"), 2)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcEnEvaluado"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnJP" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantEnJP"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcEnJP"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSAprobadas" runat="server" Text='NS'></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantAprobadas"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcAprobadas"), 2)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("CantidadTotal"), 2)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(Eval("Titulo"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 1, Eval("PorcentajeTotal"), 2)%>
                    </td>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             </tr>
          <asp:ObjectDataSource ID="ObjectDataSourceNS" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="Titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
           </ItemTemplate>
           <FooterTemplate>
           <tr class="calendarioMes" style="font-weight: bold;">
                <td>
                   <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='0' />
                    TOTAL
                </td>
                 <asp:Repeater ID="RepeaterTotal" DataSourceID="ObjectDataSourceTotal"
                        runat="server">
                       
                        <ItemTemplate>
                            <td align="center">
                                <asp:Label ID="lblCalificacionTotal" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantEnEvaluado"), 2)%>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcEnEvaluado"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSEnJP" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantEnJP"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcEnJP"), 2)%>
                            </td>
                        
                            <td align="center">
                                <asp:Label ID="lblCalificacionNSAprobadas" runat="server" Text=' '></asp:Label>
                            </td>
                            <td align="center">
                               
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantAprobadas"), 2)%>
                            </td>
                            <td align="center">
                                
                                <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcAprobadas"), 2)%>
                            </td>
                    <td align="center" >
                       
                        <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("CantidadTotal"), 2)%>
                    </td>
                    <td align="center">
                        
                        <%# CrearLink(0, int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), 0, Eval("PorcentajeTotal"), 2)%>
                    </td>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             </tr>
          <asp:ObjectDataSource ID="ObjectDataSourceTotal" runat="server" 
             OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
             TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorCompetenciasTableAdapter">
              <SelectParameters>
                  <asp:Parameter DefaultValue="ReporteCompetenciasByPonderacion" Name="Modo" 
                      Type="String" />
                  <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTitulo" DefaultValue="" 
                      Name="Titulo" PropertyName="value" Type="String" />
                  <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                      PropertyName="SelectedValue" Type="Int32" />
                  <asp:Parameter DefaultValue="" Name="PasoID" Type="Int32" />
                  <asp:Parameter DefaultValue="1" Name="Ponderacion" Type="Int32" />
                  <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
              </SelectParameters>
         </asp:ObjectDataSource>
               
           </FooterTemplate>    
           </asp:Repeater>                                          
          </div></div>
             </asp:View>
             <asp:View ID="vwReporteGeneral" runat="server">
           <div class="box resultados">
          <div class="AspNet-GridViewRepPMP">
                <asp:Repeater id="repeaterReporteGeneral" runat="server" 
                    onitemdatabound="repeaterReporteGeneral_ItemDataBound" 
                    onitemcommand="repeaterReporteGeneral_ItemCommand" 
                    onprerender="repeaterReporteGeneral_PreRender" >
                    <HeaderTemplate>
                    <table  id="gvReporteGeneral">
    	                <tr>                            
    		                <th><asp:LinkButton ID="LinkButtonFormulario" runat="server" CommandName="Sort0" CommandArgument="FormularioID">Formulario</asp:LinkButton>
                                <asp:Label ID="LabelFormulario" runat="server" Text="Formulario" Visible="false"></asp:Label>
                            </th>

    		                <th><asp:LinkButton ID="LinkButtonLegajo" runat="server" CommandName="Sort" CommandArgument="Legajo">Legajo</asp:LinkButton>
                                <asp:Label ID="LabelLegajo" runat="server" Text="Legajo" Visible="false"></asp:Label></th>
    		                <th><asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br /> Nombre</asp:LinkButton>
                               <asp:Label ID="LabelApellidoNombre" runat="server" Text="ApellidoNombre" Visible="false">Apellido y <br /> Nombre</asp:Label></th>
    		                <th>Inicio Eval.</th>
    		                <th> <asp:LinkButton ID="LinkButtonEvaluador" runat="server" CommandName="Sort" CommandArgument="Evaluador">Evaluador</asp:LinkButton>
                                <asp:Label ID="LabelEvaluador" runat="server" Text="Evaluador" Visible="false"></asp:Label></th>
    		                <th><asp:LinkButton ID="LinkButtonAuditor" runat="server" CommandName="Sort" CommandArgument="Auditor">Auditor</asp:LinkButton>
                               <asp:Label ID="LabelAuditor" runat="server" Text="Auditor" Visible="false"></asp:Label></th>
                            <th><asp:LinkButton ID="LinkButtonEstado" runat="server" CommandName="Sort" CommandArgument="Estado">Estado</asp:LinkButton>
                               <asp:Label ID="LabelEstado" runat="server" Text="Estado" Visible="false"></asp:Label></th>
                            <th><asp:LinkButton ID="LinkButtonCalificacion" runat="server" CommandName="Sort" CommandArgument="Calificacion">Resultado</asp:LinkButton>
                               <asp:Label ID="LabelCalificacion" runat="server" Text="Resultado" Visible="false"></asp:Label></th>
                            <th><asp:LinkButton ID="LinkButtonSector" runat="server" CommandName="Sort" CommandArgument="Area">Sector</asp:LinkButton>
                               <asp:Label ID="LabelSector" runat="server" Text="Sector" Visible="false"></asp:Label></th>
                            <th><asp:LinkButton ID="LinkButtonCargo" runat="server" CommandName="Sort" CommandArgument="Area">Cargo</asp:LinkButton>
                               <asp:Label ID="LabelCargo" runat="server" Text="Cargo" Visible="false"></asp:Label></th>
                            <%--<th><asp:LinkButton ID="LinkButton7" runat="server" CommandName="Sort" CommandArgument="Area">Area</asp:LinkButton></th>--%>
                            <th><asp:LinkButton ID="LinkButtonCompetencia" runat="server" CommandName="Sort2" CommandArgument="Titulo">Competencia</asp:LinkButton>
                               <asp:Label ID="LabelCompetencia" runat="server" Text="Competencia" Visible="false"></asp:Label></th>
                            <th><asp:LinkButton ID="LinkButtonCalificacionComp" runat="server" CommandName="Sort2" CommandArgument="calificacionComp">Result. Comp.</asp:LinkButton>
                              <asp:Label ID="LabelCalificacionComp" runat="server" Text="Result. Comp." Visible="false"></asp:Label></th>
                            <th><asp:Label ID="LabelVer" runat="server" Text="Ver"></asp:Label> </th>
    	                </tr>
                    </HeaderTemplate> 
                     
                    <ItemTemplate>
                        <tr>
    		                <td rowspan='<%# Eval("rowCount") %>'  >
    		                    <asp:Label ID="lblTipoFormulario" runat="server" Text='<%# Bind("TipoFormulario") %>'></asp:Label>
								<asp:HiddenField ID="hdFilterValue" runat="server" Value='<%# Eval("TipoFormularioID") %>'/>
    		                </td>
                            
                            <asp:Repeater id="rpNivel_0" runat="server" onitemdatabound="rpNivel_0_ItemDataBound">
       		                  <ItemTemplate>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label3" runat="server" Text='<%# Eval("Legajo") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label2" runat="server" Text='<%# Eval("ApellidoNombre") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label12" runat="server" Text='<%# Eval("Falta","{0:dd/MM/yyyy}") %>' ></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label4" runat="server" Text='<%# Eval("Evaluador") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label5" runat="server" Text='<%# Eval("Auditor") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label6" runat="server" Text='<%# Eval("Estado") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label8" runat="server" Text='<%# Eval("Calificacion") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label9" runat="server" Text='<%# Eval("Area") %>'></asp:Label></td>
                              <td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label10" runat="server" Text='<%# Eval("Cargo") %>'></asp:Label></td>
                              <%--<td rowspan='<%# Eval("rowCount") %>'><asp:Label ID="Label11" runat="server" Text='<%# Eval("Area") %>'></asp:Label>--%>
                              <asp:HiddenField ID="hdFilterValue" runat="server" Value='<%# Eval("TipoFormularioID").ToString() +","+Eval("Legajo").ToString()+ ","+ Eval("PeriodoID").ToString()%>'/>
                              <asp:HiddenField ID="HiddenFieldRowCount" runat="server" Value='<%# Eval("rowCount") %>' />
                              
                              </td>
                              
                              
                              <asp:Repeater id="rpNivel_1" runat="server" onitemdatabound="rpNivel_1_ItemDataBound" >
                                    <ItemTemplate>
                                     <td>
                                     <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>'/>
                                     <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("Legajo") %>'/>
                                     <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>'/>
                                     <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>'/>
                                     <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" Value='<%# Eval("LegajoEvaluador") %>'/>	
                                     <asp:Label ID="lblCompetencia" runat="server" Text='<%# Bind("titulo") %>'></asp:Label></td>
						             <td><asp:Label ID="lblcalificacionComp" runat="server" Text='<%# Bind("calificacionComp") %>'></asp:Label></td>

                                     <%#  ((RepeaterItem)Container).ItemIndex == 0  ? "<td rowspan='"+ Eval("rowCount")+"'>":"" %>
                                     <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="../img/ver.jpg" Visible='<%#  ((RepeaterItem)Container).ItemIndex == 0  %>' />
                                     <%#  ((RepeaterItem)Container).ItemIndex == 0  ? "</td>":"" %>
                                      </tr><tr>
                                   </ItemTemplate>
                              </asp:Repeater>                              
                                                                             
                              </ItemTemplate>
                            </asp:Repeater>
                            
                            </tr> 
                     
                    </ItemTemplate>

                    <FooterTemplate>
   	                </table>
	                </FooterTemplate>  
                                        
                    </asp:Repeater>
          </div>
          <div style='text-align:right;' id="PanelBotonesReporteGeneral">
          <%--<asp:Button ID="Button2" runat="server"  Text="Imprimir"  OnClientClick="ImprimirReporteGeneral()"/>
          <asp:Button ID="ButtonExportarExcel" runat="server" Text="Exportar a Excel" onclick="ButtonExportarExcel_Click"  />--%>
           
         </div>
           
          </div>
                

             </asp:View>
             <asp:View ID="vwReporteCalificaciones" runat="server">
             <div class="buttons">
             <br />
             <asp:Label ID="Label11" runat="server" Text="Resultados de Búsqueda - Calificaciones" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
             <br />
             </div>
             <div class="AspNet-GridView">
                 <asp:GridView ID="GridViewReportCalificaciones" runat="server" AutoGenerateColumns="False" 
                     onrowdatabound="GridViewReportCalificaciones_RowDataBound" 
                     
                     EmptyDataText="La búsqueda no produjo resultados para los parámetros indicados." 
                     onprerender="GridViewReportCalificaciones_PreRender">
                     <Columns>
                         <asp:TemplateField>
                             <ItemTemplate>
                                  <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>'/>
                                  <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("Legajo") %>'/>
                                  <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>'/>	
                                  <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" Value='<%# Eval("EstadoID") %>'/>
                                  <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" Value='<%# Eval("LegajoEvaluador") %>'/>
                                  <asp:ImageButton ID="ImageCommand" runat="server" ImageUrl="../img/ver.jpg" />
                             </ItemTemplate>
                             <EditItemTemplate>
                                 <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                             </EditItemTemplate>
                         </asp:TemplateField>
                         <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                             SortExpression="Legajo" />                         
                         <asp:BoundField DataField="ApellidoNombre" HeaderText="Apellido y Nombre" 
                             ReadOnly="True" SortExpression="ApellidoNombre" />
                         <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" 
                             ReadOnly="True" SortExpression="TipoFormulario" />
                         <asp:BoundField DataField="Area" HeaderText="Sector" 
                             SortExpression="Area" />
                         <asp:BoundField DataField="Estado" HeaderText="Estado" />
                         <asp:BoundField DataField="Calificacion" HeaderText="Resultado" ReadOnly="True" 
                             SortExpression="Calificacion" />
                         <asp:BoundField DataField="CalificacionPuntos" HeaderText="Puntaje" 
                             ReadOnly="True" SortExpression="CalificacionPuntos" />
                     </Columns>
                 </asp:GridView>
             </div>

         <div style='text-align:right;' id="PanelBotonesCalificaciones">
          <%--<asp:Button ID="Button1" runat="server"  Text="Imprimir"  OnClientClick="ImprimirCalificaciones()"/>
           <asp:Button ID="ButtonExportar1" runat="server" Text="Exportar a Excel" onclick="ButtonExportar_Click"  />--%>
           
         </div>
           <%--  <asp:ObjectDataSource ID="ObjectDataSourcePorCalificaciones" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorCalificacionesTableAdapter">
                 <SelectParameters>
                     <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormulario" 
                         PropertyName="SelectedValue" Type="Int32" />
                     <asp:ControlParameter ControlID="ListBoxEvaluado" Name="Evaluado" 
                         PropertyName="SelectedValue" Type="String" />
                     <asp:ControlParameter ControlID="ListBoxSector" Name="Sector" 
                         PropertyName="SelectedValue" Type="String" />
                     <asp:ControlParameter ControlID="ListBoxResultado" Name="Calificacion" 
                         PropertyName="SelectedValue" Type="String" />
                 </SelectParameters>
                 </asp:ObjectDataSource>--%>
             </asp:View>
             <asp:View ID="vwReporteStatus" runat="server">
              <div class="AspNet-GridView">
              <div class="buttons">
              <br />
              <asp:Label ID="LabelStatusPMPPlanta" runat="server" Text="Resultados de Búsqueda - Status PMP Planta" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
              <br />
              </div>
                 <asp:GridView ID="GridViewReporteStatus" runat="server" AutoGenerateColumns="False" 
                     DataSourceID="ObjectDataSourceReporteStatus" 
                      onprerender="GridViewReporteStatus_PreRender">
                     <Columns>
                         <asp:BoundField DataField="indice" HeaderText="indice" ReadOnly="True"  Visible="false"
                             SortExpression="indice" />
                         <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" 
                             SortExpression="Estado" />
                         <asp:BoundField DataField="cantidad" HeaderText="cantidad" ReadOnly="True" 
                             SortExpression="cantidad" />
                         <asp:BoundField DataField="porcentaje" HeaderText="porcentaje" ReadOnly="True" 
                             SortExpression="porcentaje" />
                     </Columns>
                 </asp:GridView>
                 <br />
               <asp:Label ID="Label15" runat="server" Text="Detalle de Status" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>  
                 <asp:GridView ID="GridViewReporteDetalleStatus" runat="server" AutoGenerateColumns="False" 
                     DataSourceID="ObjectDataSourceReporteDetalleStatus" 
                      onrowdatabound="GridViewReporteDetalleStatus_RowDataBound">
                     <Columns>
                         <asp:BoundField DataField="indice" HeaderText="indice" ReadOnly="True" Visible="false"
                             SortExpression="indice" />
                         <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" 
                             SortExpression="Estado" />
                         <asp:TemplateField HeaderText="cantidad" SortExpression="cantidad">                            
                             <ItemTemplate>
                                 <asp:Label ID="LabelCantidad" runat="server" Text='<%# Bind("cantidad") %>'  Visible="false"></asp:Label>
                                 <asp:HiddenField ID="HiddenFieldPasoID" runat="server" Value='<%# Bind("EstadoID") %>' />
                                 <asp:HiddenField ID="HiddenFieldTotalEvaluaciones" runat="server" Value='<%# Bind("TotalEvaluaciones") %>' />
                                 <asp:LinkButton ID="LinkButtonCatidad" runat="server" Text='<%# Bind("cantidad") %>' CssClass="link" ></asp:LinkButton>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="porcentaje" SortExpression="porcentaje">
                             <ItemTemplate>
                                 <asp:Label ID="LabelPorcentaje" runat="server" Text='<%# Bind("porcentaje") %>' Visible="false"></asp:Label>
                                 <asp:LinkButton ID="LinkButtonPorcentaje" runat="server" Text='<%# Bind("porcentaje") %>' CssClass="link" ></asp:LinkButton>
                             </ItemTemplate>
                         </asp:TemplateField>
                     </Columns>
                 </asp:GridView>
                 <br />
              </div>

         <div style='text-align:right;' id="PanelBotones">
          <%-- <asp:Button ID="ButtonImprimir1" runat="server"  Text="Imprimir"  OnClientClick="Imprimir()"/>
           <asp:Button ID="ButtonExportarStatus" runat="server" Text="Exportar a Excel" onclick="ButtonExportarStatus_Click"  />--%>
          
          </div>
                <asp:ObjectDataSource ID="ObjectDataSourceReporteStatus" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorStatusTableAdapter">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="STATUSPMP" Name="Modo" Type="String" />
                         <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                     </SelectParameters>
                 </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSourceReporteDetalleStatus" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorStatusTableAdapter">
                     <SelectParameters>
                         <asp:Parameter DefaultValue="DETALLESTATUSPMP" Name="Modo" Type="String" />
                         <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                     </SelectParameters>
                 </asp:ObjectDataSource>
                
            </asp:View>         
             <asp:View ID="vwReporteOcultas" runat="server">
             <div class="AspNet-GridView">

                    <asp:GridView ID="GridViewFormularios" runat="server" AllowSorting="True" 
                        AutoGenerateColumns="False" 
                        DataKeyNames="Legajo,PeriodoID,TipoFormularioID" 
                        DataSourceID="ObjectDataSourcePMPOcultas" 
                        onprerender="GridViewFormularios_PreRender" 
                        onrowdatabound="GridViewReportCalificaciones_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageCommand" runat="server"  ImageUrl="../img/ver.jpg"  />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Legajo">
                                <ItemTemplate>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Legajo") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkLegajo" runat="server" CommandArgument="Legajo" 
                                        CommandName="Sort" CssClass="destacados" Text="Legajo">
                                           </asp:LinkButton>
                                       
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Apellido y Nombre">
                                <ItemTemplate>
                                    <asp:Literal ID="Literal2" runat="server" 
                                        Text='<%# Eval("EvaluadoNombreyApellido") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkEvaluadoNombreyApellido" runat="server" 
                                        CommandArgument="EvaluadoNombreyApellido" CommandName="Sort" 
                                        CssClass="destacados" Text="Apellido y Nombre">
                                           </asp:LinkButton>
                                    
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sector">
                                <ItemTemplate>
                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Area") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkEvaluadoSector" runat="server" 
                                        CommandArgument="EvaluadoSector" CommandName="Sort" CssClass="destacados" 
                                        Text="Sector">
                                           </asp:LinkButton>
                                    
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cargo">
                                <ItemTemplate>
                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EvaluadoCargo") %>'></asp:Literal>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkCargo" runat="server" CommandArgument="EvaluadoCargo" 
                                        CommandName="Sort" CssClass="destacados" Text="Cargo">
                                           </asp:LinkButton>
                                
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inicio Evaluación" SortExpression="FAlta">
                                <ItemTemplate>
                                    <asp:Label ID="LabelFAlta" runat="server" 
                                        Text='<%# Eval("FAlta","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkFAlta" runat="server" CommandArgument="FAlta" 
                                        CommandName="Sort" CssClass="destacados" Text="Inicio Evaluación">
                                           </asp:LinkButton>
                                    
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="LinkButtonEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                                    <asp:HiddenField ID="HiddenFieldEstadoID" runat="server" 
                                        Value='<%# Eval("EstadoID") %>' />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <strong>
                                    <asp:LinkButton ID="lnkEstado" runat="server" CommandArgument="Estado" 
                                        CommandName="Sort" CssClass="destacados" Text="Estado">
                                           </asp:LinkButton>
                                   
                                    </strong>
                                </HeaderTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField HeaderText="Comentarios Evaluador">
                                <ItemTemplate>                                    
                                    <asp:HiddenField ID="HiddenFieldLegajo" runat="server" Value='<%# Eval("legajo") %>' />
                                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Eval("PeriodoID") %>' />
                                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value='<%# Eval("TipoFormularioID") %>' />
                                    <span class="tooltip" runat="server" id="LinkSinComentarioJefeTurno" Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length ==0 || (Eval("EstadoID").ToString()=="1" && Eval("ComentarioEvaluado").ToString().Trim()=="") ) ? true : false %>'> <%# "S/C" %></span>  
                                    <span runat="server" id="LinkComentarioJefeTurno" Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length >0 && (Eval("EstadoID").ToString()!="1" || Eval("ComentarioEvaluado").ToString().Trim()!="")) ? true : false %>'>
                                    <a onclick="javascript:void(0)" id='tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" rel="#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                                        class="tooltip">
                                        <%# DefaultValComentarios(Eval("ComentarioJefeTurno")) %></a>
                                        </span>
                                    <div id='loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                        <asp:Repeater ID="FVJT" runat="server" DataSourceID="ObjectDataSourceCJT">
                                            <ItemTemplate>
                                                - <strong>
                                                    <asp:Literal ID="EstadoRolLabel" runat="server" Text='<%# Bind("EstadoRol") %>' /></strong>:
                                                <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' /><br />
                                                - <strong>Evaluado</strong>:
                                                <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                                - <strong>Fecha</strong>:
                                                <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                                <br />
                                                - <strong>Comentario</strong>:</ItemTemplate>
                                        </asp:Repeater>
                                        <asp:ObjectDataSource ID="ObjectDataSourceCJT" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                                    PropertyName="Value" Type="Int32" />
                                                <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                        <span id="Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" >
                                            <%# (Eval("EstadoID").ToString() != "1" || Eval("ComentarioEvaluado").ToString().Trim() != "") ? Eval("ComentarioJefeTurno") : ""%>
                                        </span>
                                    </div>

                                    <script type="text/javascript">
                                        $(document).ready(function () {

                                            if (jQuery.trim($('#Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                                $('#tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                                    local: true,
                                                    showTitle: false,
                                                    stycky: true,
                                                    mouseOutClose: false
                                                });
                                            } else {
                                                $('#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                                            }
                                        })
                                    </script>

                                </ItemTemplate>
                                <HeaderStyle CssClass="destacados" />                   
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comentarios Evaluado">
                                <ItemTemplate>
                                <span class="tooltip" runat="server" id="LinkSinComentarioEvaluado" Visible='<%# (Eval("ComentarioEvaluado").ToString().Length ==0) ? true : false %>'><%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></span>  
                                <span class="tooltip" runat="server" id="LinkComentarioEvaluado" Visible='<%# (Eval("ComentarioEvaluado").ToString().Length >0) ? true : false %>'>  
                                    <a onclick="javascript:void(0)" id='tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                                        rel="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" class="tooltip">
                                        <%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></a>
                                        </span>
                                    <div id='loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                        <asp:Repeater ID="FVEVAL" runat="server" DataSourceID="ObjectDataSourceEval">
                                            <ItemTemplate>
                                                - <strong>Evaluado</strong>:
                                                <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                                - <strong>Fecha</strong>:
                                                <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                                <br />
                                                - <strong>Comentario</strong>:
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <span id="SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" />
                                        <%# Eval("ComentarioEvaluado")%>
                                        </span>
                                        <asp:ObjectDataSource ID="ObjectDataSourceEval" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                                    PropertyName="Value" Type="Int32" />
                                                <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                    </a>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            if (jQuery.trim($('#SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                                $('#tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                                    local: true,
                                                    showTitle: false,
                                                    stycky: true,
                                                    mouseOutClose: false
                                                });
                                            } else {
                                                $('#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                                            }
                                        })
                                    </script>

                                </ItemTemplate>
                                <HeaderStyle CssClass="destacados" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Comentarios Auditor">
                                <ItemTemplate>
                                <span class="tooltip" runat="server" id="LinkSinComentarioJefeDePlanta" Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length ==0) ? true : false %>'><%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></span>  
                                <span class="tooltip" runat="server" id="LinkComentarioJefeDePlanta" Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length >0) ? true : false %>'>
                                    <a onclick="javascript:void(0)" id='tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' href="#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" rel="#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>"
                                        class="tooltip">
                                        <%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></a>
                                        </span>
                                    <div id='loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                        <asp:Repeater ID="FVJP" runat="server" DataSourceID="ObjectDataSourceJP">
                                            <ItemTemplate>
                                                - <strong>
                                                    <asp:Literal ID="EstadoRolLabel" runat="server" Text='<%# Bind("EstadoRol") %>' /></strong>:
                                                <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' /><br />
                                                - <strong>Evaluado</strong>:
                                                <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" Text='<%# Bind("UsuarioEvaluado") %>' /><br />
                                                - <strong>Fecha</strong>:
                                                <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                                <br />
                                                - <strong>Comentario</strong>:
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <span id="SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>">
                                            <%# Eval("ComentarioJefeDePlanta")%>
                                        </span>
                                        <asp:ObjectDataSource ID="ObjectDataSourceJP" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="GetLastHistorialByPasoID" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                                                    Type="Int32" />
                                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                                    PropertyName="Value" Type="Int32" />
                                                <asp:Parameter DefaultValue="3" Name="PasoID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>

                                    <script type="text/javascript">
                                        $(document).ready(function () {

                                            if (jQuery.trim($('#SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                                $('#tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                                    local: true,
                                                    showTitle: false,
                                                    hoverIntent: false,
                                                    stycky: true,
                                                    mouseOutClose: false
                                                });
                                            } else {
                                                $('#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                                            }
                                        })
                                    </script>

                                </ItemTemplate>
                                <HeaderStyle CssClass="destacados" />
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="">
                                   <ItemTemplate>
                                        <asp:CheckBox ID="CheckBoxSinEvaluacion" runat="server" 
                                            Checked='<%# (Eval("EstadoID").ToString()=="5")? true : false %>' />
                                    </ItemTemplate>
                                     <HeaderStyle CssClass="destacados" />
                                </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                   
             </div>
                   <asp:ObjectDataSource ID="ObjectDataSourcePMPOcultas" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.eval_GetReportePorPMPOcultasTableAdapter" 
                     onselecting="ObjectDataSourcePMPOcultas_Selecting">
                       <SelectParameters>
                           <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormulario" 
                               PropertyName="SelectedValue" Type="Int32" />
                           <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                               PropertyName="SelectedValue" Type="Int32" />
                           <asp:ControlParameter ControlID="ListBoxEvaluado" Name="Evaluado" 
                               PropertyName="SelectedValue" Type="String" />
                       </SelectParameters>
                    </asp:ObjectDataSource>
             </asp:View>
             <asp:View ID="vwReporteAreasInteres" runat="server">
             <div class="AspNet-GridView">
             <br />
                <asp:Label ID="LabelReporteAreaDeInteres" runat="server" Text="Resultados de Búsqueda - Areas de interes" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
              <br />
             <asp:Repeater ID="RepeaterAreasInteres" runat="server" 
                     DataSourceID="ObjectDataSourceAreasDeInteres">
             <HeaderTemplate>
                <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0">
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th align="center" >
                            Area de interes
                        </th>
                      
                        <th align="center" colspan="">
                            En Evaluado
                        </td>
                        <th align="center" colspan="">
                            En Auditor
                        </th>
                        <th align="center" colspan="">
                            Finalizado
                        </th>
                        <th align="center" colspan="">
                            TOTAL
                        </th>
                    </tr>
                    <tr class="calendarioMes" style="font-weight: bold;">
                        <th>&nbsp;
                            
                        </th>                        
                        
                        <th>
                            Cant
                        </th>
                       <%-- <th>
                            %
                        </th>--%>
                       
                        <th>
                            Cant
                        </th>
                        <%--<th>
                            %
                        </th>--%>
                        
                        <th>
                            Cant
                        </th>
                        <%--<th>
                            %
                        </th>--%>
                        <th>
                            Cant
                        </th>
                        <%-- <th>
                            %
                        </th>--%>
                        
                    </tr>             
             </HeaderTemplate>
             <ItemTemplate>
                <tr class="destacados" style="background-color: WhiteSmoke;">
                    <td>                        
                        <asp:HiddenField ID="HiddenFieldAreaDeOperacionID" runat="server" Value='<%# Eval("AreaDeOperacionID") %>' />
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                    </td> 
                    
             <asp:Repeater ID="RepeaterAreaDeInteresDetalle" DataSourceID="ObjectDataSourceAreaDeInteresDetalle"
                        runat="server">
                       
                        <ItemTemplate>
                           
                            <td align="center">
                               <asp:HiddenField ID="HiddenFieldAreaDeOperacionID" runat="server" Value='<%# Eval("AreaDeOperacionID") %>' />
                                <%# CrearLinkAI(int.Parse(Eval("AreaDeOperacionID").ToString()), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue),  Eval("CantEnEvaluado"))%>
                            </td>
                          <%--  <td align="center">
                               
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcEnEvaluado"))%>
                            </td>   --%>                     
                           
                            <td align="center">
                                
                                <%# CrearLinkAI(int.Parse(Eval("AreaDeOperacionID").ToString()), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), Eval("CantEnAuditor"))%>
                            </td>
                           <%-- <td align="center">
                                
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcEnAuditor"))%>
                            </td>--%>
                        
                            
                            <td align="center">
                               
                                <%# CrearLinkAI(int.Parse(Eval("AreaDeOperacionID").ToString()), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue), Eval("CantAprobadas"))%>
                            </td>
                           <%-- <td align="center">
                                
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcAprobadas"))%>
                            </td>--%>
                    <td align="center" >
                       
                        <%# CrearLinkAI(int.Parse(Eval("AreaDeOperacionID").ToString()), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue), Eval("CantidadTotal"))%>
                    </td>
                    <%--<td align="center">
                        
                        <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcentajeTotal"))%>
                    </td>--%>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             <asp:ObjectDataSource ID="ObjectDataSourceAreaDeInteresDetalle" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorAreaDeOperacionTableAdapter">
                  <SelectParameters>
                      <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                          PropertyName="SelectedValue" Type="Int32" />
                      <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                          PropertyName="SelectedValue" Type="Int32" />
                      <asp:ControlParameter ControlID="HiddenFieldAreaDeOperacionID" DefaultValue="" 
                      Name="AreaDeOperacionesID" PropertyName="Value" Type="Int32" />
                  
                  </SelectParameters>
                 </asp:ObjectDataSource>                              
   
                    </tr>         
             </ItemTemplate>
             <FooterTemplate>
              <tr class="calendarioMes" style="font-weight: bold;">
              <td>TOTAL</td>
             <asp:Repeater ID="RepeaterAreaDeInteresDetalle" DataSourceID="ObjectDataSourceAreaDeInteresTotal"
                        runat="server">
                       
                        <ItemTemplate>
                           
                            <td align="center">
                                <%# CrearLinkAI(0, int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue),  Eval("CantEnEvaluado"))%>
                            </td>
                          <%--  <td align="center">
                               
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 2, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcEnEvaluado"))%>
                            </td>   --%>                     
                           
                            <td align="center">
                                
                                <%# CrearLinkAI(0, int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue), Eval("CantEnAuditor"))%>
                            </td>
                           <%-- <td align="center">
                                
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 3, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcEnAuditor"))%>
                            </td>--%>
                        
                            
                            <td align="center">
                               
                                <%# CrearLinkAI(0, int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue),  Eval("CantAprobadas"))%>
                            </td>
                           <%-- <td align="center">
                                
                                <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 4, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcAprobadas"))%>
                            </td>--%>
                    <td align="center" >
                       
                        <%# CrearLinkAI(0, int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue),  Eval("CantidadTotal"))%>
                    </td>
                    <%--<td align="center">
                        
                        <%# CrearLinkAI(Eval("areadeoperacionid"), int.Parse(ddlPeriodo.SelectedValue), 0, int.Parse(ListBoxFormularios.SelectedValue),  Eval("PorcentajeTotal"))%>
                    </td>--%>
                           
                        </ItemTemplate>
                        
         </asp:Repeater> 
             <asp:ObjectDataSource ID="ObjectDataSourceAreaDeInteresTotal" runat="server" 
                     OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReportePorAreaDeOperacionTableAdapter">
                  <SelectParameters>
                      <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                          PropertyName="SelectedValue" Type="Int32" />
                      <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                          PropertyName="SelectedValue" Type="Int32" />
                     <asp:Parameter DefaultValue="0" Name="AreaDeOperacionesID" Type="Int32" />                     
                  
                  </SelectParameters>
                 </asp:ObjectDataSource> 
              </tr>
             </table>
             </FooterTemplate>
             </asp:Repeater>

             </div>
              
                 <asp:ObjectDataSource ID="ObjectDataSourceAreasDeInteres" runat="server" 
                     DeleteMethod="Delete" InsertMethod="Insert" 
                     OldValuesParameterFormatString="original_{0}" 
                     SelectMethod="GetDataByFormulario" 
                     TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter" 
                     UpdateMethod="Update">
                     <DeleteParameters>
                         <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                         <asp:Parameter Name="Original_Descripcion" Type="String" />
                         <asp:Parameter Name="Original_Activo" Type="Boolean" />
                         <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                     </DeleteParameters>
                     <InsertParameters>
                         <asp:Parameter Name="Descripcion" Type="String" />
                         <asp:Parameter Name="Activo" Type="Boolean" />
                         <asp:Parameter Name="PorDefecto" Type="Boolean" />
                     </InsertParameters>
                     <SelectParameters>
                         <asp:Parameter Name="legajo" Type="Int32" />
                         <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormularioID" 
                             PropertyName="SelectedValue" Type="Int32" />
                         <asp:Parameter Name="PeriodoID" Type="Int32" />
                     </SelectParameters>
                     <UpdateParameters>
                         <asp:Parameter Name="Descripcion" Type="String" />
                         <asp:Parameter Name="Activo" Type="Boolean" />
                         <asp:Parameter Name="PorDefecto" Type="Boolean" />
                         <asp:Parameter Name="Original_AreaDeOperacionID" Type="Int32" />
                         <asp:Parameter Name="Original_Descripcion" Type="String" />
                         <asp:Parameter Name="Original_Activo" Type="Boolean" />
                         <asp:Parameter Name="Original_PorDefecto" Type="Boolean" />
                     </UpdateParameters>
                 </asp:ObjectDataSource>
             </asp:View>
            <asp:View ID="vwReportePorComentarios" runat="server">
                <br />
                <asp:Label ID="LabelReporteComentarios" runat="server" Text="Resultados de Búsqueda - Comentarios" CssClass="CalendarioBack" Font-Bold="true"></asp:Label>
                 <br />
             <div class="AspNet-GridView">
                <asp:GridView ID="GridViewReportePorComentarios" runat="server" AutoGenerateColumns="False" 
                   
                     onprerender="GridViewReportePorComentarios_PreRender">
                    <Columns>
                        <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                            SortExpression="Legajo" />
                        <asp:BoundField DataField="EvaluadoNombreyApellido" HeaderText="Evaluado" 
                            ReadOnly="True" SortExpression="EvaluadoNombreyApellido" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" 
                            SortExpression="Estado" />
                        <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" ReadOnly="True" SortExpression="TipoFormulario" />
                        <asp:TemplateField HeaderText="Comentarios del Evaluador">
                            <ItemTemplate>
                                <asp:HiddenField ID="HiddenFieldLegajo" runat="server" 
                                    Value='<%# Eval("legajo") %>' />
                                <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" 
                                    Value='<%# Eval("PeriodoID") %>' />
                                <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" 
                                    Value='<%# Eval("TipoFormularioID") %>' />
                                <span ID="LinkSinComentarioJefeTurno" runat="server" class="tooltip" 
                                    Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length ==0 || (Eval("EstadoID").ToString()=="1" && Eval("ComentarioEvaluado").ToString().Trim()=="") ) ? true : false %>'>
                                <%# "S/C" %></span><span ID="LinkComentarioJefeTurno" runat="server" 
                                    Visible='<%# (Eval("ComentarioJefeTurno").ToString().Length >0 && (Eval("EstadoID").ToString()!="1" || Eval("ComentarioEvaluado").ToString().Trim()!="")) ? true : false %>'>
                                <a ID='tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    class="tooltip" 
                                    href='#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    onclick="javascript:void(0)" 
                                    rel='#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                <%# DefaultValComentarios(Eval("ComentarioJefeTurno")) %></a></span>
                                <div ID='loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                    <asp:Repeater ID="FVJT" runat="server" DataSourceID="ObjectDataSourceCJT">
                                        <ItemTemplate>
                                            - <strong>
                                            <asp:Literal ID="EstadoRolLabel" runat="server" 
                                                Text='<%# Bind("EstadoRol") %>' />
                                            </strong>:
                                            <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' />
                                            <br />
                                            - <strong>Evaluado</strong>:
                                            <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" 
                                                Text='<%# Bind("UsuarioEvaluado") %>' />
                                            <br />
                                            - <strong>Fecha</strong>:
                                            <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                            <br />
                                            - <strong>Comentario</strong>:
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:ObjectDataSource ID="ObjectDataSourceCJT" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" 
                                        SelectMethod="GetLastHistorialByPasoID" 
                                        TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                                                Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                                            <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                    <span ID='Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                    <%# (Eval("EstadoID").ToString() != "1" || Eval("ComentarioEvaluado").ToString().Trim() != "") ? Eval("ComentarioJefeTurno") : ""%>
                                    </span>
                                </div>
                                <script type="text/javascript">


                                        $(document).ready(function () {

                                            if (jQuery.trim($('#Span<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                                $('#tooltip<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                                    local: true,
                                                    showTitle: false,
                                                    stycky: true,
                                                    mouseOutClose: false
                                                });
                                            } else {
                                                $('#loadme<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                                            }
                                        })
                                    </script>
                            </ItemTemplate>
                            <HeaderStyle CssClass="destacados" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comentarios del Evaluado">
                            <ItemTemplate>
                                <span ID="LinkSinComentarioEvaluado" runat="server" class="tooltip" 
                                    Visible='<%# (Eval("ComentarioEvaluado").ToString().Length ==0) ? true : false %>'>
                                <%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></span>
                                <span ID="LinkComentarioEvaluado" runat="server" class="tooltip" 
                                    Visible='<%# (Eval("ComentarioEvaluado").ToString().Length >0) ? true : false %>'>
                                <a ID='tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    class="tooltip" 
                                    href='#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    onclick="javascript:void(0)" 
                                    rel='#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                <%# DefaultValComentarios(Eval("ComentarioEvaluado"))%></a></span>
                                <div ID='loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                    <asp:Repeater ID="FVEVAL" runat="server" DataSourceID="ObjectDataSourceEval">
                                        <ItemTemplate>
                                            - <strong>Evaluado</strong>:
                                            <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" 
                                                Text='<%# Bind("UsuarioEvaluado") %>' />
                                            <br />
                                            - <strong>Fecha</strong>:
                                            <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                            <br />
                                            - <strong>Comentario</strong>:
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <span id="SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>" />
                                    <%# Eval("ComentarioEvaluado")%></span>
                                    <asp:ObjectDataSource ID="ObjectDataSourceEval" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" 
                                        SelectMethod="GetLastHistorialByPasoID" 
                                        TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                                                Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                                            <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                                </a>
                                <script type="text/javascript">


                           $(document).ready(function () {
                               if (jQuery.trim($('#SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                   $('#tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                               }
                           })
                       </script>
                            </ItemTemplate>
                            <HeaderStyle CssClass="destacados" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Comentarios del Auditor">
                            <ItemTemplate>
                                <span ID="LinkSinComentarioJefeDePlanta" runat="server" class="tooltip" 
                                    Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length ==0) ? true : false %>'>
                                <%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></span>
                                <span ID="LinkComentarioJefeDePlanta" runat="server" class="tooltip" 
                                    Visible='<%# (Eval("ComentarioJefeDePlanta").ToString().Length >0) ? true : false %>'>
                                <a ID='tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    class="tooltip" 
                                    href='#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>' 
                                    onclick="javascript:void(0)" 
                                    rel='#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                <%# DefaultValComentarios(Eval("ComentarioJefeDePlanta"))%></a></span>
                                <div ID='loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                    <asp:Repeater ID="FVJP" runat="server" DataSourceID="ObjectDataSourceJP">
                                        <ItemTemplate>
                                            - <strong>
                                            <asp:Literal ID="EstadoRolLabel" runat="server" 
                                                Text='<%# Bind("EstadoRol") %>' />
                                            </strong>:
                                            <asp:Literal ID="UsuarioLabel" runat="server" Text='<%# Bind("Usuario") %>' />
                                            <br />
                                            - <strong>Evaluado</strong>:
                                            <asp:Literal ID="UsuarioEvaluadoLabel" runat="server" 
                                                Text='<%# Bind("UsuarioEvaluado") %>' />
                                            <br />
                                            - <strong>Fecha</strong>:
                                            <asp:Literal ID="FechaLabel" runat="server" Text='<%# Bind("Fecha") %>' />
                                            <br />
                                            - <strong>Comentario</strong>:
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <span ID='SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>'>
                                    <%# Eval("ComentarioJefeDePlanta")%></span>
                                    <asp:ObjectDataSource ID="ObjectDataSourceJP" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" 
                                        SelectMethod="GetLastHistorialByPasoID" 
                                        TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                                                PropertyName="Value" Type="Int32" />
                                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                                                Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                                            <asp:Parameter DefaultValue="3" Name="PasoID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                                <script type="text/javascript">


                           $(document).ready(function () {

                               if (jQuery.trim($('#SpanJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').html()) != '') {
                                   $('#tooltipJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       hoverIntent: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadmeJP<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %>').hide();
                               }
                           })
                       </script>
                            </ItemTemplate>
                            <HeaderStyle CssClass="destacados" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </div>
            </asp:View>
         
             <asp:View ID="ViewSinResultados" runat="server">
                <br />
                <br />
                <br />
                    <asp:Label ID="LabelResultado" runat="server" Text="La búsqueda no produjo resultados para los parámetros indicados." 
                        CssClass="destacados"></asp:Label>            
                <br />
               <br />
                <br />
            </asp:View> 

 
            <asp:View ID="ViewVacio" runat="server"></asp:View>             
         </asp:MultiView>
     
    
    
         
</asp:Content>

