﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteByCursoID.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteByCursoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Detalle Usuarios</title>
     <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
    <tr>
    <td>
     <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>


    <span runat="server" ID="SpanCurso">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="LabelCurso" runat="server" Text="Curso:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterCurso" runat="server" 
        DataSourceID="ObjectDataSourceCurso">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("CursoDescripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    </td>

    <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" /><br /></td>
        </tr>
    </table>
    <br />

        <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="legajo" DataSourceID="ObjectDataSourceEvaluados">
            <Columns>
                <asp:TemplateField HeaderText="legajo" SortExpression="legajo">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("legajo") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Apellido y Nombre" SortExpression="apellidoNombre">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("apellidoNombre") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cluster" SortExpression="descripcion">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("descripcion") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cargo" SortExpression="cargodet">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("cargodet") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Direccion" SortExpression="direcciondet">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("direcciondet") %>'></asp:Label>
                    </ItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tipo de Formulario" SortExpression="TipoFormulario">
                    <ItemTemplate>
                      <asp:Label ID="LabeltipoFormulario" runat="server" Text='<%# Bind("TipoFormulario") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

     <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataCursosByCursoID" 
        
            
            TypeName="com.paginar.johnson.DAL.DSReporteAdminTableAdapters.Eval_Rep_Cursos_UserByCursoIDTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="Legajo" QueryStringField="UsuarioID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="DireccionID" QueryStringField="DireccionID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="CursoID" QueryStringField="CursoID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="Cluster" QueryStringField="Cluster" 
                Type="String" />
            <asp:QueryStringParameter Name="TipoFormualrioId" 
                QueryStringField="TipoFormularioID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
     <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceCurso" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByCursoID" 
        
            TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmPMPCursosTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="CursoID" QueryStringField="CursoID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
     
    </div>
    </form>
   
</body>
</html>
