﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.DAL;
using System.Data;


namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReportesAdm : System.Web.UI.Page
    {

        private RepositoryEvalDesemp _Repositorio = new RepositoryEvalDesemp();
        public RepositoryEvalDesemp Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryEvalDesemp();
                return _Repositorio;
            }
        }
        

        private DSEvalDesempReportes.frmGetReportePMPCursosDataTable _dtReportePMPCursos;
        private DSEvalDesempReportes.frmGetReportePMPCursosDataTable dtReportePMPCursos
        {
            get
            {
                _dtReportePMPCursos = (DSEvalDesempReportes.frmGetReportePMPCursosDataTable)Session["dtReportePMPCursos"];
                if (_dtReportePMPCursos == null)
                {
                    _dtReportePMPCursos = new DSEvalDesempReportes.frmGetReportePMPCursosDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPCursosTableAdapter.Fill(_dtReportePMPCursos, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        0, int.Parse(DropDownListDireccion.SelectedValue));


                    Session["dtReportePMPCursos"] = _dtReportePMPCursos;
                }
                return _dtReportePMPCursos;
            }
            set
            {
                Session["dtReportePMPCursos"] = value;
                _dtReportePMPCursos = value;
            }
        }

        private DSEvalDesempReportes.frmGetReporteCompetenciaComoFortalezasDataTable _dtReporteCompetenciasComoFortalezas;
        private DSEvalDesempReportes.frmGetReporteCompetenciaComoFortalezasDataTable dtReporteCompetenciasComoFortalezas
        {
            get
            {
                _dtReporteCompetenciasComoFortalezas = (DSEvalDesempReportes.frmGetReporteCompetenciaComoFortalezasDataTable)Session["dtReporteCompetenciasComoFortalezas"];
                if (_dtReporteCompetenciasComoFortalezas == null)
                {
                    _dtReporteCompetenciasComoFortalezas = new DSEvalDesempReportes.frmGetReporteCompetenciaComoFortalezasDataTable();

                    Repositorio.AdapterEvalDesemp.GetReporteCompetenciaComoFortalezasTableAdapter.Fill(_dtReporteCompetenciasComoFortalezas, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        DropDownListFortalezas.SelectedValue);


                    Session["dtReporteCompetenciasComoFortalezas"] = _dtReporteCompetenciasComoFortalezas;
                }
                return _dtReporteCompetenciasComoFortalezas;
            }
            set
            {
                Session["dtReporteCompetenciasComoFortalezas"] = value;
                _dtReporteCompetenciasComoFortalezas = value;
            }
        }

        private DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable _dtReporteCalificaciones;
        private DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable dtReporteCalificaciones
        {
            get
            {
                _dtReporteCalificaciones = (DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable)Session["dtReporteCalificaciones"];
                if (_dtReporteCalificaciones == null)
                {
                    _dtReporteCalificaciones = new DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPCalificacionesTableAdapter.Fill(_dtReporteCalificaciones, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        int.Parse(DropDownListDireccion.SelectedValue),int.Parse(DropDownListCalificacionTotal.SelectedValue),int.Parse(DropDownListCalificacionFactores.SelectedValue),int.Parse(DropDownListFactores.SelectedValue));


                    Session["dtReporteCalificaciones"] = _dtReporteCalificaciones;
                }
                return _dtReporteCalificaciones;
            }
            set
            {
                Session["dtReporteCalificaciones"] = value;
                _dtReporteCalificaciones = value;
            }
        }

        private DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable _dtReportePlanDeAccion;
        private DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable dtReportePlanDeAccion
        {
            get
            {
                _dtReportePlanDeAccion = (DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable)Session["dtReportePlanDeAccion"];
                if (_dtReportePlanDeAccion == null)
                {
                    _dtReportePlanDeAccion = new DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPPlanDeAccionTableAdapter.Fill(_dtReportePlanDeAccion, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        int.Parse(DropDownListDireccion.SelectedValue),null);


                    Session["dtReportePlanDeAccion"] = _dtReportePlanDeAccion;
                }
                return _dtReportePlanDeAccion;
            }
            set
            {
                Session["dtReportePlanDeAccion"] = value;
                _dtReportePlanDeAccion = value;
            }
        }

        private DSEvalDesempReportes.frmGetReportePMPCarrerPlanDataTable _dtReporteCarrerPlan;
        private DSEvalDesempReportes.frmGetReportePMPCarrerPlanDataTable dtReporteCarrerPlan
        {
            get
            {
                _dtReporteCarrerPlan = (DSEvalDesempReportes.frmGetReportePMPCarrerPlanDataTable)Session["dtReporteCarrerPlan"];
                if (_dtReporteCarrerPlan == null)
                {
                    _dtReporteCarrerPlan = new DSEvalDesempReportes.frmGetReportePMPCarrerPlanDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPCarrerPlanTableAdapter.Fill(_dtReporteCarrerPlan, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        int.Parse(DropDownListDireccion.SelectedValue), DropDownListPosicion.SelectedValue, int.Parse(DropDownListCalificacionTotal.SelectedValue));


                    Session["dtReporteCarrerPlan"] = _dtReporteCarrerPlan;
                }
                return _dtReporteCarrerPlan;
            }
            set
            {
                Session["dtReporteCarrerPlan"] = value;
                _dtReporteCarrerPlan = value;
            }
        }


        private DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable _dtReporteFortalezas;
        private DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable dtReporteFortalezas
        {
            get
            {
                _dtReporteFortalezas = (DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable)Session["_dtReporteFortalezas"];
                if (_dtReporteFortalezas == null)
                {
                    _dtReporteFortalezas = new DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPFortalezasPorc.Fill(_dtReporteFortalezas, int.Parse(DropDownListPeriodo.SelectedValue), 
                        int.Parse(DropDownListEvaluado.SelectedValue), DropDownListFortalezas.SelectedValue.ToString(), int.Parse(DropDownListDireccion.SelectedValue));


                    Session["_dtReporteFortalezas"] = _dtReporteFortalezas;
                }
                return _dtReporteFortalezas;
            }
            set
            {
                Session["_dtReporteFortalezas"] = value;
                _dtReporteFortalezas = value;
            }
        }

        private DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable _dtReporteNecesidades;
        private DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable dtReporteNecesidades
        {
            get
            {
                _dtReporteNecesidades = (DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable)Session["_dtReporteNecesidades"];
                if (_dtReporteNecesidades == null)
                {
                    _dtReporteNecesidades = new DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPNecesidadesPorc.Fill(_dtReporteNecesidades, int.Parse(DropDownListPeriodo.SelectedValue),
                        int.Parse(DropDownListEvaluado.SelectedValue), DropDownListFortalezas.SelectedValue.ToString(), int.Parse(DropDownListDireccion.SelectedValue));


                    Session["_dtReporteNecesidades"] = _dtReporteNecesidades;
                }
                return _dtReporteNecesidades;
            }
            set
            {
                Session["_dtReporteNecesidades"] = value;
                _dtReporteNecesidades = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonBuscarRepAdministrativos_Click(object sender, EventArgs e)
        {

            switch (DropDownListTipoReporte.SelectedValue.ToString())
            {
                case "0": //Competencias Como Fortalezas
                    dtReporteCompetenciasComoFortalezas = null;
                    //ObjectDataSourceReporteCompetenciasComoFortalezas.DataBind();
                    RepeaterReporteCompetenciasComoFortalezas.DataSource = dtReporteCompetenciasComoFortalezas;//ObjectDataSourceReporteCompetenciasComoFortalezas;
                    RepeaterReporteCompetenciasComoFortalezas.DataBind();
                    if (RepeaterReporteCompetenciasComoFortalezas.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCompetenciasComoFortalezas);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Competencia como Fortalezas";
                        LabelTituloPrint.Text = "PMP - Competencia como Fortalezas";
                    }
                    else
                    
                        MultiviewResultados.SetActiveView(ViewSinResultados);
                       
                    break;
                case "1": //Calificaciones PMP
                    dtReporteCalificaciones = null;
                    //ObjectDataSourceReporteCalificacionesPMP.DataBind();
                    RepeaterReporteCalificacionesPMP.DataSource = dtReporteCalificaciones;//ObjectDataSourceReporteCalificacionesPMP;
                    RepeaterReporteCalificacionesPMP.DataBind();
                    if (RepeaterReporteCalificacionesPMP.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCalificacionesPMP);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Calificaciones PMP";
                        LabelTituloPrint.Text = "PMP - Calificaciones ";
                    }
                    else
                      MultiviewResultados.SetActiveView(ViewSinResultados);
                    
                    break;
               
                case "2": //Summary
                    //ObjectDataSourceSummary.DataBind();
                    dtReporteCalificaciones = null;
                    RepeaterSummary.DataSource = dtReporteCalificaciones;//ObjectDataSourceSummary;
                    RepeaterSummary.DataBind();
                    if (RepeaterSummary.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteSummary);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Summary";
                        LabelTituloPrint.Text = "PMP - Summary";
                    }
                    else
                    
                        MultiviewResultados.SetActiveView(ViewSinResultados);
                    
                    break;

                case "3": //Planes de Accion
                    //ObjectDataSourcePlanesDeAccion.DataBind();
                    dtReportePlanDeAccion = null;
                    RepeaterPlanDeAccion.DataSource = dtReportePlanDeAccion;//ObjectDataSourcePlanesDeAccion;
                    RepeaterPlanDeAccion.DataBind();
                    if (RepeaterPlanDeAccion.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReportePlanDeAccion);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Plan de Acción";
                        LabelTituloPrint.Text = "PMP - Plan de Acción";
                    }
                    else
                    
                        MultiviewResultados.SetActiveView(ViewSinResultados);
                    
                    break;
                case "4": //Fortalezas Necesidades Gral
                   
                    //DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable DTFortalezas = new DSEvalDesempReportes.frmGetReportePMPFortalezasPorcDataTable();
                    //DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable DtNecesidades = new DSEvalDesempReportes.frmGetReportePMPNecesidadesPorcDataTable();
                    //Repositorio.AdapterEvalDesemp.GetReportePMPNecesidadesPorc.Fill(DtNecesidades, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue), DropDownListFortalezas.SelectedValue.ToString(), int.Parse(DropDownListDireccion.SelectedValue));
                    //Repositorio.AdapterEvalDesemp.GetReportePMPFortalezasPorc.Fill(DTFortalezas, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue), DropDownListFortalezas.SelectedValue.ToString(), int.Parse(DropDownListDireccion.SelectedValue));
                    dtReporteNecesidades = null;
                    dtReporteFortalezas = null;
                    RepeaterNecesidadesGral.DataSource = dtReporteNecesidades;
                    RepeaterFortalezasGral.DataSource = dtReporteFortalezas;
                    RepeaterNecesidadesGral.DataBind();
                    RepeaterFortalezasGral.DataBind();

                    string PorcentajeFortalezas = dtReporteFortalezas.Compute("sum(porcentaje)", "").ToString();
                    string PorcentajeNecesidades = dtReporteNecesidades.Compute("sum(porcentaje)", "").ToString();

                    string TotalFortalezas = dtReporteFortalezas.Compute("count(cantidad)", "").ToString();
                    string TotalNecesidades = dtReporteNecesidades.Compute("count(cantidad)", "").ToString();

                    LabelFortalezas.Text = "Estas " + TotalFortalezas + " Fortalezas concentran el " + PorcentajeFortalezas + "% de los resultados obtenidos";
                    LabelNecesidades.Text = "Estas " + TotalNecesidades + " Necesidades concentran el " + PorcentajeNecesidades + "% de los resultados obtenidos";

                    if (RepeaterNecesidadesGral.Items.Count > 0 || RepeaterFortalezasGral.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteFortalezasNecesidadesGral);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Fortalezas y Necesidades de Desarrollo Gral.";
                        LabelTituloPrint.Text = "PMP - Fortalezas y Necesidades <br/> de Desarrollo  Gral.";
                    }
                    else
                    
                        MultiviewResultados.SetActiveView(ViewSinResultados);
                    
                    break;


                case "5": //Carrer Plan
                   // ObjectDataSourceCarrerPlan.DataBind();
                    dtReporteCarrerPlan = null;
                    RepeaterCarrerPlan.DataSource = dtReporteCarrerPlan;//ObjectDataSourceCarrerPlan;
                    RepeaterCarrerPlan.DataBind();
                    if (RepeaterCarrerPlan.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCarrerPlan);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Carrer Plan";
                        LabelTituloPrint.Text = "PMP - Carrer Plan";
                    }
                    else                    
                        MultiviewResultados.SetActiveView(ViewSinResultados);
                    
                    break;

                case "6": //Cursos
                  
                    dtReportePMPCursos = null;
                    RepeaterCursos.DataSource = dtReportePMPCursos;
                    RepeaterCursos.DataBind();
                    if (RepeaterCursos.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCursos);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Cursos";
                        LabelTituloPrint.Text = "PMP - Cursos";
                    }
                    else

                        MultiviewResultados.SetActiveView(ViewSinResultados);

                    break;
            }

            if (MultiviewResultados.ActiveViewIndex == 0 || MultiviewResultados.ActiveViewIndex == 1)
            {
                ButtonExportarRepSeguimientoAdm.Visible = false;
                ButtonImprimir.Visible = false;
                LabelResultadobusqueda.Visible = false;
            }
            else
            {
                ButtonExportarRepSeguimientoAdm.Visible = true;
                ButtonImprimir.Visible = true;
                LabelResultadobusqueda.Visible = true;

            }

        }
        
        protected string DefaultVal(object val)
        {

            if (((val == System.DBNull.Value) || (val == null)))
                return "-";
            if (val == string.Empty)
                return "-";
            else
                return val.ToString();        

        }

        protected void DropDownListTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownListEvaluado.SelectedValue = "0";
            DropDownListFortalezas.SelectedValue = "0";
            DropDownListPosicion.SelectedValue = "0";
            DropDownListDireccion.SelectedValue = "0";
            DropDownListCalificacionFactores.SelectedValue = "0";
            DropDownListFactores.SelectedValue = "0";
            DropDownListCalificacionTotal.SelectedValue = "0";

            switch (((DropDownList)sender).SelectedValue.ToString())
            {
                    
                case "0"://foratalezas como debilidades
                    DropDownListEvaluado.Enabled=true;                    
                    DropDownListFortalezas.Enabled=true;
                    DropDownListPosicion.Enabled=false;
                    DropDownListDireccion.Enabled=false;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled=false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son las competencias que para el evaluado son fortalezas";
                    break;
                case "1"://calificaciones
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = true;
                    DropDownListFactores.Enabled = true;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cual es la calificación final de acuerdo a los FCE seleccionados";
                    break;
                case "2"://summary
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es visualizar la calificación final de cada evaluado";
                    break;
                case "3"://Planes De Accion
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son los objetivos planteados por los evaluados para el próximo año fiscal";
                    break;
                case "4"://Fortalezas Necesidades Gral
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = true;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son las fortalezas y necesidades más seleccionadas por los evaluados para poder planificar cursos y capacitaciones para cada caso";
                    break;
                case "5"://Carrer Plan
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = true;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son los aspectos que pretende el evaluado a corto y largo plazo";
                    break;
                case "6"://Cursos
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado, es identificar cuales son los cursos mas solicitados por los usuarios de Johnson";
                    break;

            }

            LabelResultadobusqueda.Visible = false;
            MultiviewResultados.SetActiveView(ViewVacio);
        }

        protected void Exportar(Repeater rpt, string filename, string titulo)
        {
            if (rpt.Items.Count > 0)
            {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write(generarExportarTodo(rpt, titulo));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
        }
        private string generarExportarTodo(Repeater rpt, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

           
            Page page = new Page();
            HtmlForm form = new HtmlForm();

           
                    Label LabelApellidoNombre = (Label)rpt.Controls[0].Controls[0].FindControl("LabelApellidoNombre");
                    LinkButton LinkButtonApellidoNombre = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonApellidoNombre");

                    if (LabelApellidoNombre != null && LinkButtonApellidoNombre != null)
                      {
                        LabelApellidoNombre.Visible = true;
                        LinkButtonApellidoNombre.Visible = false;   
                      }

                    Label LabelFortalezas = (Label)rpt.Controls[0].Controls[0].FindControl("LabelFortalezas");
                    LinkButton LinkButtonFortalezas = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonFortalezas");

                    if (LabelFortalezas != null && LinkButtonFortalezas != null)
                      {
                        LabelFortalezas.Visible = true;
                        LinkButtonFortalezas.Visible = false;   
                      }

                    Label LabelNecesidades = (Label)rpt.Controls[0].Controls[0].FindControl("LabelNecesidades");
                    LinkButton LinkButtonNecesidades = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonNecesidades");

                    if (LabelNecesidades != null && LinkButtonNecesidades != null)
                      {
                        LabelNecesidades.Visible = true;
                        LinkButtonNecesidades.Visible = false;   
                      }
                    
                       

                        Label LabelLegajo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelLegajo");
                        LinkButton LinkButtonLegajo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonLegajo");

                        if (LabelLegajo != null && LinkButtonLegajo != null)
                          {
                              LabelLegajo.Visible = true;
                              LinkButtonLegajo.Visible = false;   
                          }

                        Label LabelCargo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCargo");
                        LinkButton LinkButtonCargo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCargo");

                        if (LabelCargo != null && LinkButtonCargo != null)
                          {
                              LabelCargo.Visible = true;
                              LinkButtonCargo.Visible = false;   
                          }


                        Label LabelCalificacion = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCalificacion");
                        LinkButton LinkButtonCalificacion = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCalificacion");

                        if (LabelCalificacion != null && LinkButtonCalificacion != null)
                          {
                              LabelCalificacion.Visible = true;
                              LinkButtonCalificacion.Visible = false;   
                          }

                        Label LabelDireccion = (Label)rpt.Controls[0].Controls[0].FindControl("LabelDireccion");
                        LinkButton LinkButtonDireccion = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonDireccion");

                        if (LabelDireccion != null && LinkButtonDireccion != null)
                        {
                            LabelDireccion.Visible = true;
                            LinkButtonDireccion.Visible = false;
                        }

            

            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(rpt);
            page.RenderControl(htw);
            return  sb.ToString();
        }
        protected void Exportar(Repeater rpt1, Repeater rpt2, string filename, string titulo1, string titulo2)
        {
          //  if (rpt.Items.Count > 0)
           // {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write(generarExportarTodo(rpt1, rpt2, titulo1,titulo2));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
           // }
        }
        private string generarExportarTodo(Repeater rpt1, Repeater rpt2, string titulo1, string titulo2)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();

            Label LabelFortalezas = (Label)rpt1.Controls[0].Controls[0].FindControl("LabelFortaleza");
            LinkButton LinkButtonFortalezas = (LinkButton)rpt1.Controls[0].Controls[0].FindControl("LinkButtonFortaleza");

            if (LabelFortalezas != null && LinkButtonFortalezas != null)
            {
                LabelFortalezas.Visible = true;
                LinkButtonFortalezas.Visible = false;
            }

            Label LabelNecesidades = (Label)rpt2.Controls[0].Controls[0].FindControl("LabelNecesidad");
            LinkButton LinkButtonNecesidades = (LinkButton)rpt2.Controls[0].Controls[0].FindControl("LinkButtonNecesidad");

            if (LabelNecesidades != null && LinkButtonNecesidades != null)
            {
                LabelNecesidades.Visible = true;
                LinkButtonNecesidades.Visible = false;
            }



            Literal LiteralTitulo1 = new Literal();
            LiteralTitulo1.Text = "<BR><BR> <strong>" + titulo1 + "</strong><BR><BR>";

            Literal LiteralTitulo2 = new Literal();
            LiteralTitulo2.Text = "<BR><BR> <strong>" + titulo2 + "</strong><BR><BR>";

            // rpaux.EnableViewState = false;        

            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(LiteralTitulo1);
            form.Controls.Add(rpt1);
            form.Controls.Add(LiteralTitulo2);
            form.Controls.Add(rpt2);
            page.RenderControl(htw);
            return  sb.ToString() ;
        }

  

        protected void Exportar2(Repeater rpt, string filename, string titulo)
        {
            if (rpt.Items.Count > 0)
            {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write(generarExportarTodo2(rpt, titulo));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
        }
        private string generarExportarTodo2(Repeater rpt, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();

            Label LabelNombreCursoH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelNombreCurso");
            Label LabelCantidadH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCantidad");
            Label LabelPorcentajeH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelPorcentaje");

            LinkButton LinkButtonNombreCursoH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonNombreCurso");
            LinkButton LinkButtonCantidadH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCantidad");
            LinkButton LinkButtonPorcentajeH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonPorcentaje");

            if (LabelNombreCursoH != null && LinkButtonNombreCursoH != null)
            {
                LabelNombreCursoH.Visible = true;
                LinkButtonNombreCursoH.Visible = false;

            }

            if (LabelCantidadH != null && LinkButtonCantidadH != null)
            {
                LabelCantidadH.Visible = true;
                LinkButtonCantidadH.Visible = false;

            }

            if (LabelPorcentajeH != null && LinkButtonPorcentajeH != null)
            {
                LabelPorcentajeH.Visible = true;
                LinkButtonPorcentajeH.Visible = false;

            }


            foreach (RepeaterItem Ri in rpt.Items)
            {
                HiddenField HiddenFieldCursoID = (HiddenField)Ri.FindControl("HiddenFieldCursoID");

                LinkButton LinkButtonkPorcentaje = (LinkButton)Ri.FindControl("LinkButtonPorcentaje");
                LinkButton LinkButtonCantidad = (LinkButton)Ri.FindControl("LinkButtonCantidad");

                Label LabelPorcentaje = (Label)Ri.FindControl("LabelPorcentaje");
                Label LabelCantidad = (Label)Ri.FindControl("LabelCantidad");

                if (LinkButtonkPorcentaje != null)
                {
                    LinkButtonkPorcentaje.Visible = false;
                    LabelPorcentaje.Visible = true;
                }

                if (LinkButtonCantidad != null)
                {
                    LinkButtonCantidad.Visible = false;
                    LabelCantidad.Visible = true;
                }

            }

            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(rpt);
            page.RenderControl(htw);
            return sb.ToString();
        }
        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {
            switch(MultiviewResultados.ActiveViewIndex)
            {
                case 2:
                    Exportar(RepeaterReporteCompetenciasComoFortalezas, "Reporte Competencias Como Fortalezas", "Reporte Competencias Como Fortalezas");
                    break;
                case 3:
                    Exportar(RepeaterReporteCalificacionesPMP, "Reporte por Calificaciones", "Reporte por Calificaciones");
                    break;
                case 4:
                    Exportar(RepeaterSummary, "Reporte Summary", "Reporte Summary");
                    break;
                case 5:
                    Exportar(RepeaterPlanDeAccion, "Reporte Planes de Accion", "Reporte Planes de Accion");
                    break;
                case 6:
                    Exportar(RepeaterFortalezasGral, RepeaterNecesidadesGral, "Reporte Planes de Accion", LabelFortalezas.Text, LabelNecesidades.Text);
                    break;
                case 7:
                    Exportar(RepeaterCarrerPlan, "Reporte Carrer Plan", "Reporte Carrer Plan");
                    break;
                case 8:
                    Exportar2(RepeaterCursos, "Reporte Cursos", "Reporte Cursos");
                    break;

            }
        }

        

        protected void RepeaterCursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton LinkButtonCantidad = (LinkButton)e.Item.FindControl("LinkButtonCantidad");
                HiddenField HiddenFieldCursoID = (HiddenField)e.Item.FindControl("HiddenFieldCursoID");
                LinkButton LinkButtonPorcentaje = (LinkButton)e.Item.FindControl("LinkButtonPorcentaje");
                LinkButtonPorcentaje.OnClientClick = string.Format("javascript:popupPmpRepAdm('/PMP/Admin/ReporteByCursoID.aspx?PeriodoID={0}&DireccionID={1}&UsuarioID={2}&CursoID={3}',600,300)", DropDownListPeriodo.SelectedValue, DropDownListDireccion.SelectedValue, DropDownListEvaluado.SelectedValue, HiddenFieldCursoID.Value);
                LinkButtonCantidad.OnClientClick = string.Format("javascript:popupPmpRepAdm('/PMP/Admin/ReporteByCursoID.aspx?PeriodoID={0}&DireccionID={1}&UsuarioID={2}&CursoID={3}',600,300)", DropDownListPeriodo.SelectedValue, DropDownListDireccion.SelectedValue, DropDownListEvaluado.SelectedValue, HiddenFieldCursoID.Value);                
                
            }
        }

        protected void RepeaterCursos_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterCursos.DataSource = SelectDataTable(dtReportePMPCursos, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterCursos.DataBind();
                

            }

        }


        public DataTable SelectDataTable(DataTable dt, string filter, string sort)
        {
            DataRow[] rows = null;
            DataTable dtNew = default(DataTable);
            // copy table structure
            dtNew = dt.Clone();
            // sort and filter data
            rows = dt.Select(filter, sort, DataViewRowState.CurrentRows);
            // fill dtNew with selected rows
            foreach (DataRow dr in rows)
            {
                dtNew.ImportRow(dr);
            }
            // return filtered dt
            return dtNew;
        }


        void SortOrder(string SortExpression)
        {
            if (ViewState[SortExpression] == null)
            {
                ViewState[SortExpression] = " ASC";
            }
            else if (ViewState[SortExpression].ToString() == " ASC")
            {
                ViewState[SortExpression] = " DESC";
            }
            else
            {
                ViewState[SortExpression] = " ASC";
            }
            
        }

        protected void RepeaterReporteCompetenciasComoFortalezas_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterReporteCompetenciasComoFortalezas.DataSource = SelectDataTable(dtReporteCompetenciasComoFortalezas, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterReporteCompetenciasComoFortalezas.DataBind();
            }

        }

        protected void RepeaterReporteCalificacionesPMP_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterReporteCalificacionesPMP.DataSource = SelectDataTable(dtReporteCalificaciones, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterReporteCalificacionesPMP.DataBind();
            }

        }

        protected void RepeaterSummary_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterSummary.DataSource = SelectDataTable(dtReporteCalificaciones, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterSummary.DataBind();
            }

        }

        protected void RepeaterPlanDeAccion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterPlanDeAccion.DataSource = SelectDataTable(dtReportePlanDeAccion, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterPlanDeAccion.DataBind();
            }

        }

        protected void RepeaterCarrerPlan_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterCarrerPlan.DataSource = SelectDataTable(dtReporteCarrerPlan, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterCarrerPlan.DataBind();
            }

        }

        protected void RepeaterFortalezasGral_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterFortalezasGral.DataSource = SelectDataTable(dtReporteFortalezas, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterFortalezasGral.DataBind();
            }

        }

        protected void RepeaterNecesidadesGral_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterNecesidadesGral.DataSource = SelectDataTable(dtReporteNecesidades, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterNecesidadesGral.DataBind();
            }

        }
    }
}