﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteSeguimientoAdmPorAreaImpresion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
                    HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];

                if (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"]))
                    HiddenFieldLegajoEvaluador.Value = Request.QueryString["LegajoEvaluador"];

                if (!string.IsNullOrEmpty(Request.QueryString["Pasoid"]))
                    HiddenFieldPasoID.Value = Request.QueryString["Pasoid"];

                if (!string.IsNullOrEmpty(Request.QueryString["Pasoid"]))
                {
                    HiddenFieldPasoID.Value = Request.QueryString["Pasoid"];
                    switch (Request.QueryString["Pasoid"])
                    {
                        case "NI":
                            HiddenFieldPasoIDNI.Value = "NI";
                            HiddenFieldPasoIDG.Value = "NN";
                            HiddenFieldPasoIDE.Value = "NN";
                            HiddenFieldPasoIDA.Value = "NN";
                            break;
                        case "G":
                            HiddenFieldPasoIDNI.Value = "NN";
                            HiddenFieldPasoIDG.Value = "G";
                            HiddenFieldPasoIDE.Value = "NN";
                            HiddenFieldPasoIDA.Value = "NN";
                            break;
                        case "E":
                            HiddenFieldPasoIDNI.Value = "NN";
                            HiddenFieldPasoIDG.Value = "NN";
                            HiddenFieldPasoIDE.Value = "E";
                            HiddenFieldPasoIDA.Value = "NN";
                            break;
                        case "A":
                            HiddenFieldPasoIDNI.Value = "NN";
                            HiddenFieldPasoIDG.Value = "NN";
                            HiddenFieldPasoIDE.Value = "NN";
                            HiddenFieldPasoIDA.Value = "A";
                            break;

                    }

                }

                if (!string.IsNullOrEmpty(Request.QueryString["AreaID"]))
                    HiddenFieldAreaID.Value = Request.QueryString["AreaID"];
            }

        }

        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {
            if (RepeaterEvaluaciones.Visible == true)
            {
                if (RepeaterEvaluaciones.Items.Count != 0)
                {
                    DateTime fecha = DateTime.Now;
                    string filename = "";
                    filename = "Reporte_de_Seguimiento" + String.Format("{0: ddMMyyyy}", fecha);

                    Exportar(RepeaterEvaluaciones, filename, "Reporte de Seguimiento");
                    //ExportarPDF(RepeaterEvaluaciones, filename, "Reporte de Seguimeinto");
                }
            }
        }

        protected void Exportar(Repeater gv, string filename, string titulo)
        {

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Repeater gvaux = new Repeater();
            gvaux = gv;

            Page page = new Page();
            HtmlForm form = new HtmlForm();

            gvaux.EnableViewState = false;


            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);


            form.Controls.Add(gvaux);

            page.RenderControl(htw);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Regex.Replace(sb.ToString(), "<a[^>]+>([^<]+)</a>", "$1"));
            Response.End();



        }

        //protected void Exportar(Repeater gv, string filename, string titulo)
        //{
        //    Response.Clear();
        //    Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
        //    Response.Write("<head>");
        //    Response.Write("<!--[if gte mso 9]><xml>");
        //    Response.Write("<x:ExcelWorkbook>");
        //    Response.Write("<x:ExcelWorksheets>");
        //    Response.Write("<x:ExcelWorksheet>");
        //    Response.Write("<x:Name> Reportes PMP</x:Name>");
        //    Response.Write("<x:WorksheetOptions>");
        //    Response.Write("<x:Print>");
        //    Response.Write("<x:ValidPrinterInfo/>");
        //    Response.Write("</x:Print>");
        //    Response.Write("</x:WorksheetOptions>");
        //    Response.Write("</x:ExcelWorksheet>");
        //    Response.Write("</x:ExcelWorksheets>");
        //    Response.Write("</x:ExcelWorkbook>");
        //    Response.Write("</xml>");
        //    Response.Write("<![endif]--> ");
        //    Response.Write("</head>");
        //    Response.Write("<body>");
        //    Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
        //    Response.Charset = "";
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.ContentType = "application/vnd.xls";

        //    Response.Write(generarExportarTodo(gv, titulo));

        //    Response.Write("</body>");
        //    Response.Write("</html>");
        //    Response.End();
        //}


        private string generarExportarTodo(Repeater gv, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Repeater gvaux = new Repeater();
            gvaux = gv;





            Page page = new Page();
            HtmlForm form = new HtmlForm();

            gvaux.EnableViewState = false;


            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);


            form.Controls.Add(gvaux);

            page.RenderControl(htw);

            return sb.ToString();
        }

        protected void RepeaterEvaluaciones_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HiddenFieldEvaluador = (HiddenField)e.Item.FindControl("HiddenFieldEvaluador");
                Repeater RepeaterBorrador = (Repeater)e.Item.FindControl("RepeaterBorrador");
                Repeater RepeaterNoIniciadas = (Repeater)e.Item.FindControl("RepeaterNoIniciadas");
                Repeater RepeaterEnviadas = (Repeater)e.Item.FindControl("RepeaterEnviadas");
                Repeater RepeaterAprobadas = (Repeater)e.Item.FindControl("RepeaterAprobadas");

                Label LabelCantNoIniciadas = (Label)e.Item.FindControl("LabelCantNoIniciadas");
                Label LabelCantBorrador = (Label)e.Item.FindControl("LabelCantBorrador");
                Label LabelCantEnviadas = (Label)e.Item.FindControl("LabelCantEnviadas");
                Label LabelCantAprobadas = (Label)e.Item.FindControl("LabelCantAprobadas");

                //if (HiddenFieldEvaluador.Value == "-1")
                //{
                //    RepeaterBorrador.Visible = false;
                //    RepeaterNoIniciadas.Visible = false;
                //    RepeaterEnviadas.Visible = false;
                //    RepeaterAprobadas.Visible = false;


                //    LabelCantNoIniciadas.Visible = true;
                //    LabelCantBorrador.Visible = true;
                //    LabelCantEnviadas.Visible = true;
                //    LabelCantAprobadas.Visible = true;
                //}


            }
        }
    }
}