﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteObjetivos.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteObjetivos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
     <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
     <script type="text/javascript" src="../../js/jquery.fixedheadertable.js"></script>
     <link href="../../css/defaultTheme.css" rel="stylesheet" type="text/css" />
    

     <style type="text/css">
         
        .AspNet-GridViewReportes {
            margin-bottom:10px;
            overflow-x:auto;
            overflow-y:hidden;
            clear:both;
        }
        .AspNet-GridViewReportes table{
            width:100%;
            border: 1px solid rgb(204, 204, 204);
        }
        .AspNet-GridViewReportes table th,
        .AspNet-GridViewReportes table td {
            padding:2px;
            border:1px solid #FFFFFF;
            font-size:12px;
            vertical-align:top;
            color:#000;
        }
        
        .AspNet-GridViewReportes table span {
           color:#000;
        }

        .AspNet-GridViewReportes th{
            background-color:#E6E6E6;
            color:#666666;
            text-transform:uppercase;
            font-weight:normal;
            height:100px;
            padding: 0px;
            
     }   
     
         
   
    .leftthird {
    clear:left;
    float:left;
    width: 30%;
    font-size:12px;
    padding-left:2%;
    }
   .middlethird{
    float:left;
    width:34%;
    font-size:12px;
   }

   .rightthird{
    float:right;
    width:34%;
    font-size:12px;
    }
    
 .buttons{
    margin-bottom:10px;
    overflow:hidden;
    clear:both;
    padding-left:2%;
}

.LabelsFortalezasNecesidades{
    margin-bottom:10px;
    overflow:hidden;
    clear:both;
    padding-left:2%;
    font-size:12px;    
}

.labelTituloPrint
{
    font-size:25px; 
    font-weight:normal; 
    color:#333; 
    background:none; 
    float:left; margin:0; 
    text-indent: 0px;
    }
    
     th .bg{
    position:absolute;
    top:-16px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:48px solid #ccc;
    padding: 0px;
}     

 th .bg1{
    position:absolute;
    top:-8px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:48px solid #ccc;
    padding: 0px;
}     


 th .bg2{
    position:absolute;
    top:-20px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:57px solid #ccc;
    padding: 0px;
}     

 th .bg3{
    position:absolute;
    top:-12px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}     
 th .bg4{
    position:absolute;
    top:-2px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}  
   th .bg5{
    position:absolute;
    top:-13px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:40px solid #ccc;
    padding: 0px;
}   

th .bg6{
    position:absolute;
    top:-10px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}   

th .bg7{
    position:absolute;
    top:-10px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:64px solid #ccc;
    padding: 0px;
}   
.fht-thead{
    overflow:hidden;
 
}
.fht-tbody{   
    height:240px;
}

.fht-table th{
   font-weight:bold;
    text-align:center;
} 

.tableHeaderFixed th{
   font-weight:bold;
    text-align:center;
} 

thead {display: table-cell;}


 



         
     </style>
     <script type="text/javascript">
         function Imprimir() {

             $('#PanelFiltros').hide();
             $('#PanelTitulo').hide();
             $('#PanelTitulo2').hide();
             $('#header').show();
             $('table.tableHeaderFixed').fixedHeaderTable('destroy');
             var $thead = $('table.tableHeaderFixed').find('thead');
             $('table.tableHeaderFixed').each(function (index) { $(this).find('thead').remove(); });
             //$('table.tableHeaderFixed').each(function (index) { $(this).prepend($thead[index].innerHTML); });

             $('table.tableHeaderFixed1').fixedHeaderTable('destroy');
             var $thead1 = $('table.tableHeaderFixed1').find('thead');
             $('table.tableHeaderFixed1').each(function (index) { $(this).find('thead').remove(); });
             $('table.tableHeaderFixed1').each(function (index) { $(this).prepend($thead1[index].innerHTML); });

             window.print();
             $('table.tableHeaderFixed tr:first').each(function (index) { $(this).remove(); });
             //$('table.tableHeaderFixed > tbody').each(function (index) { $(this).prepend('<thead>' + $thead[index].innerHTML + '</thead>'); });

             $('table.tableHeaderFixed1 tr:first').each(function (index) { $(this).remove(); });
             $('table.tableHeaderFixed1 > tbody').each(function (index) { $(this).prepend('<thead>' + $thead1[index].innerHTML + '</thead>'); });

             $('#PanelFiltros').show();
             $('#PanelTitulo').show();
             $('#PanelTitulo2').show();
             $('#header').hide();
             $('table.tableHeaderFixed').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });


         }

         $(document).ready(function () {
             $('#header').hide();
             //$('table.tableHeaderFixed').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });
             //$('table.tableHeaderFixed1').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });

         });

         function popupPmpRepAdm(url, ancho, alto) {
             var posicion_x;
             var posicion_y;
             posicion_x = (screen.width / 2) - (ancho / 2);
             posicion_y = (screen.height / 2) - (alto / 2);
             window.open(url, "PmpRepAdm", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

         }
         
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="buttons" id="PanelTitulo">
    <br />
     <u><b>Reporte Control de carga de Objetivos</b></u>
    <br />
    </div>

    <div class="box formulario" id="PanelFiltros">
        <div class="form-item leftthird">
        <label class="descripcion"> Período de Evaluación</label>
        <asp:DropDownList ID="DropDownListPeriodo" runat="server" DataSourceID="ObjectDataSourcePeriodosAdministrativos"
            DataTextField="Descripcion" DataValueField="PeriodoID">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSourcePeriodosAdministrativos" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetData" 
                
                TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.PeriodoTableAdapter">
            </asp:ObjectDataSource>
                                               
        </div>


        
   
        
      
        
        <div class="form-item middlethird">
        <label >Dirección</label>
            <asp:DropDownList ID="DropDownListDireccion" runat="server" 
                        DataSourceID="ObjectDataSourceDirecciones" DataTextField="DireccionDET" 
                        DataValueField="DireccionID" AppendDataBoundItems="True">
              <asp:ListItem Value="0">TODOS</asp:ListItem>
                    </asp:DropDownList>          
            <asp:ObjectDataSource ID="ObjectDataSourceDirecciones" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="DireccionDET" Type="String" />
                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="DireccionDET" Type="String" />
                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>

        </div> 

        
 
             <div class="form-item middlethird">
                 <label>Área</label>
                 <asp:DropDownList ID="DropDownListAreaRS" runat="server" 
                     DataValueField="AreaID" DataTextField="AreaDesc"
                                     AppendDataBoundItems="True" >
                   <asp:ListItem Text="Todos" Value="0"></asp:ListItem>
                 
                 </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSourceAreaRS" runat="server" 
                     OldValuesParameterFormatString="original_{0}" 
                     SelectMethod="GetAreasByDireccionID" 
                     TypeName="com.paginar.johnson.BL.ControllerReportesDeSeguimiento">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownListDireccion" Name="DireccionID" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                 </asp:ObjectDataSource>
                 </div>
   

            <div class="form-item leftthird">
            <asp:Panel ID="PanelCluster" runat="server">
            <label>Cluster</label>

                <asp:ListBox ID="DropDownListCluster" runat="server" SelectionMode="Multiple" Rows="4" CssClass="form-select">
                     <asp:ListItem Text="Todos" Value="0" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="Argentina" Value="1"></asp:ListItem>
                      <asp:ListItem Text="Chile" Value="2"></asp:ListItem>
                      <asp:ListItem Text="Paraguay" Value="4"></asp:ListItem>
                      <asp:ListItem Text="Uruguay" Value="3"></asp:ListItem>
                </asp:ListBox>
               
                 </asp:Panel>
            </div>                  


               <div class="form-item middlethird">
                 <label> Incluir Detalle </label>
                 <asp:CheckBox ID="chkDetalle" runat="server" Checked="true" />
               </div>
            
                     
                                  
        <div class="buttons">
        <asp:Button ID="ButtonBuscarRepAdministrativos" runat="server" OnClick="ButtonBuscar" 
                    Text="Buscar" />
       <asp:Button ID="ButtonExportarRepSeguimientoAdm" runat="server" onclick="ButtonExportarExcel_Click" 
                    Text="Exportar"  Visible="true"/> 
      <asp:Button ID="ButtonImprimir" runat="server"  Text="Imprimir"  OnClientClick="Imprimir()"/>
        </div>
        </div>

    <div id="header" style="overflow:hidden; zoom:1; padding:10px; margin-bottom: 15px;position:relative; background:none; height:70px;">
            <div style="position: absolute; top:0; left:0; width: 100%; height:30px; border-top:100px solid #ccc; z-index:-1;"></div>
            <asp:Label ID="LabelTituloPrint" runat="server" Text="Reporte Control carga de Objetivos" CssClass="labelTituloPrint"></asp:Label>            
            <img  src="../../images/logoprint.gif" style="float:right;" />
        </div>


    <div class="buttons" id="PanelTitulo2">
       
           <asp:Label ID="LabelResultadobusqueda" runat="server" Text="Resultados de Búsqueda - Reporte de Objetivos"
           visible="false"></asp:Label>
        
        </div>        

     <div class="AspNet-GridViewRepPMP">

            <asp:Repeater ID="rptDireccion" runat="server" 
            onitemdatabound="rptDireccion_ItemDataBound" >
                            <HeaderTemplate>
                                <table width="100%"  class="tableHeaderFixed">
                                    <tr>
                                        <th  align="left">
                                         &nbsp 
                                         <asp:Label Text=" Dirección/Area"  Font-Size="Small" ID="lblCabecera1" runat="server" />
                                                                                   
                                        </th>
                                        <th  align="left">
                                         &nbsp 
                                         <asp:Label Text="Completo objetivos"  Font-Size="Small" ID="lblCabecera2" runat="server" />
                                        </th>
                                     
                                    </tr>

                                    <tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr  style="background-color: #ccc;">
                                    <td>
                                      &nbsp <asp:Label ID="Label1" runat="server"  Font-Size="Small" Font-Bold="true" Text='<%#  Eval("Direccion")  %>'></asp:Label> 
                                       <asp:HiddenField ID="hdDireccionID" runat="server" Value='<%#  Eval("DireccionID")  %>' />
                                    </td>
                                   
                                    <td>
                                 
                                    </td>
                               </tr>
                                <asp:Repeater ID="rptEvaluados" runat="server">
                                    <HeaderTemplate>

                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <tr>
                                            <td>
                                             &nbsp  <asp:Label ID="Label7" runat="server" Font-Size="Smaller"  Text='<%#  Eval("legajowd")  %>'></asp:Label>
                                             &nbsp  <asp:Label ID="Label1" runat="server" Font-Size="Smaller"  Text='<%#  Eval("Apellido")  %>'></asp:Label>
                                             &nbsp
                                                <asp:Label ID="Label2" runat="server" Font-Size="Smaller"   Text='<%#  Eval("Nombre")  %>'></asp:Label>
                                            </td>
                                            <td>
                                            <strong>
                                                    <asp:Label ID="Label3" runat="server"  Font-Size="Smaller" Text='<%#  Eval("objetivos").ToString()=="0"?"NO":"SI"  %>'></asp:Label>
                                                    <br />
                                                </strong>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                      
                                    

                                <tr>
                                    
                                    <td >                                     
                                     <asp:Label ID="Label4"  Font-Size ="Smaller" runat="server" Font-Italic="true"  Font-Bold="true" Text="SubTotal "></asp:Label> 
                                     <asp:Label ID="Label5"  Font-Size ="Smaller" runat="server"  Font-Italic="true" Font-Bold="true" Text="Completados "></asp:Label> 
                                     <asp:Label ID="lblCompletados"   Font-Size ="Smaller"  runat="server"  Font-Italic="true" Text=""></asp:Label> 
                                     <asp:Label ID="Label6"  Font-Size ="Smaller" runat="server"  Font-Bold="true"  Font-Italic="true" Text=" Sin Completar "></asp:Label>                                       
                                     <asp:Label ID="lblSincompletar"  Font-Size ="Smaller"  Font-Italic="true" runat="server" Text=""></asp:Label> 
                                    </td>
                                    <td></td>
                                    <td>
                                       
                                    </td>
                               </tr>

                            </ItemTemplate>
                            <FooterTemplate>
                            <tr height="20px"> <td></td><td></td><td></td>   </tr>
                                <tr>
                                    <td>
                                       <asp:Label ID="Label4"  Font-Size ="Smaller" runat="server" Font-Italic="true"  Font-Bold="true" Text="Total General"></asp:Label> 
                                       &nbsp
                                       <asp:Label ID="Label8"  Font-Size ="Smaller" runat="server" Font-Italic="true"  Font-Bold="true" Text="Completados"></asp:Label> 
                                       <asp:Label ID="lblTotalCompletos"    runat="server" Font-Size="Smaller" ></asp:Label>
                                       <asp:Label ID="Label9"  Font-Size ="Smaller" runat="server" Font-Italic="true"  Font-Bold="true" Text="Sin Completar"></asp:Label> 
                                        <asp:Label ID="lblTotalSinCompletar" runat="server" Font-Size="Smaller" ></asp:Label>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>

      </div>

    </div>
    </form>
</body>
</html>
