﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CambiarEstadoNoIniciadoEvaluador.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.CambiarEstadoNoIniciadoEvaluador" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>

                                         <div class="box formulario">
                                     <div class="form-item leftHalf">
                                     <label>Período:</label>
                                         <%--<asp:Label ID="lblPeriodo" runat="server" Text="Periodo: " CssClass="descripcion" Font-Bold="True"></asp:Label></td>--%>
                
                                     <asp:DropDownList ID="ddlPeriodo" runat="server" DataSourceID="ObjectDataSourcePeriodosCalibracion"
                                      DataTextField="Descripcion" DataValueField="PeriodoID" CssClass="formselect"                                             
                                            AutoPostBack="True">  </asp:DropDownList>
                                     <asp:ObjectDataSource ID="ObjectDataSourcePeriodosCalibracion" runat="server" DeleteMethod="Delete"
                                                InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTipoPeriodoID"
                                                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter"
                                                UpdateMethod="Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="PeriodoID" Type="Int32" />
                                                </DeleteParameters>
                                                <SelectParameters>
                                                    <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="Descripcion" Type="String" />
                                                    <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                                    <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                                    <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                                    <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                                    <asp:Parameter Name="PeriodoID" Type="Int32" />
                                                </UpdateParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="Descripcion" Type="String" />
                                                    <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                                    <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                                    <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                                    <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                                    <asp:Parameter Name="PeriodoID" Type="Int32" />
                                                </InsertParameters>
                                            </asp:ObjectDataSource> 
            
                                     <asp:Repeater ID="FormView1" runat="server" 
                                            DataSourceID="ObjectDataSourcePeriodo">
                                                    <ItemTemplate>
                                                        <strong class="destacados">
                                                        <span>
                                                        (<asp:Label ID="CargaDesdeLabel" runat="server" 
                                                    Text='<%# Bind("CargaDesde","{0:dd/MM/yyyy}") %>' />
                                                        -
                                                        <asp:Label ID="CargaHastaLabel" runat="server" 
                                                    Text='<%# Bind("CargaHasta","{0:dd/MM/yyyy}") %>' />)
                                                    </span>
                                                        </strong>
                    
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                     <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
                                                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
                                
                                            TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
                                            DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                                                        <asp:Parameter Name="Original_Descripcion" Type="String" />
                                                        <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                                                        <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                                                        <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                                                        <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                                                    </DeleteParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="PeriodoID" Type="Int32" />
                                                        <asp:Parameter Name="Descripcion" Type="String" />
                                                        <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                                        <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                                        <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                                        <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                                    </InsertParameters>
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                                                            PropertyName="SelectedValue" Type="Int32" />
                                                    </SelectParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="Descripcion" Type="String" />
                                                        <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                                                        <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                                                        <asp:Parameter Name="CargaDesde" Type="DateTime" />
                                                        <asp:Parameter Name="CargaHasta" Type="DateTime" />
                                                        <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                                                        <asp:Parameter Name="Original_Descripcion" Type="String" />
                                                        <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                                                        <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                                                        <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                                                        <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                                                    </UpdateParameters>
                                                </asp:ObjectDataSource>
                                     </div>
                                    <div class="form-item rightHalf">           
                                     <asp:Label ID="lblEvaluado" runat="server" CssClass="descripcion" Font-Bold="True" Text="Evaluado:"></asp:Label>
                                     <asp:ListBox ID="ListBoxEvaluado" runat="server" 
                                                      DataSourceID="ObjectDataSourceEvaluados" DataTextField="ApellidoNombre" 
                                                      DataValueField="Legajo" AppendDataBoundItems="True"  CssClass="form-select" Rows="1">
                                                     <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                                                 </asp:ListBox>
                                     <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" 
                                         OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
                                         TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                                             <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                                                 PropertyName="SelectedValue" Type="Int32" />
                                             <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                         </SelectParameters>
                                     </asp:ObjectDataSource>                                    
                         
                   
                                    </div>
              
             
                                   <div class="form-item leftHalf">
                                     <asp:Label ID="lblArea" runat="server" CssClass="descripcion" Font-Bold="True" Text="Área:"></asp:Label>            
              
                                    <asp:ListBox ID="ListBoxArea" runat="server" 
                                         DataSourceID="ObjectDataSourceAreas" DataTextField="AreaDESC" 
                                         DataValueField="AreaID" 
                                         AppendDataBoundItems="True" CssClass="form-select" Rows="1">
                                         <asp:ListItem Value="0" Selected="True">Todos</asp:ListItem>
                                    </asp:ListBox>
                                    <asp:ObjectDataSource ID="ObjectDataSourceAreas" runat="server" 
                                        DeleteMethod="Delete" InsertMethod="Insert" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                                        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasTableAdapter" 
                                        UpdateMethod="Update">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                            <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                        </DeleteParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="AreaDESC" Type="String" />
                                            <asp:Parameter Name="Original_AreaID" Type="Int32" />
                                            <asp:Parameter Name="Original_AreaDESC" Type="String" />
                                        </UpdateParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="AreaDESC" Type="String" />
                                        </InsertParameters>
                                    </asp:ObjectDataSource> 

                                    </div>

                                 <div class="form-item rightHalf"> 
                                     <asp:Label ID="lblEvaluador" runat="server" CssClass="descripcion" Font-Bold="True" Text="Evaluador: "></asp:Label> 
            
                                    <asp:ListBox ID="ListBoxEvaluador" runat="server"  
                                        CssClass="form-select" DataSourceID="ObjectDataSourceEvaluadores" AppendDataBoundItems="true"
                                        DataTextField="ApellidoNombre" DataValueField="Legajo" Rows="1">
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                    </asp:ListBox>
                                    <asp:ObjectDataSource ID="ObjectDataSourceEvaluadores" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPaso" 
                                        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="1" Name="PasoID" Type="Int32" />
                                            <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID" 
                                                PropertyName="SelectedValue" Type="Int32" />
                                            <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource> 
                                </div>
                                <div class="form-item leftHalf">
                                   <asp:Label ID="lblCargo" runat="server" Text="Cargo: " CssClass="descripcion" Font-Bold="True"></asp:Label>
                    
                                    <asp:ObjectDataSource ID="ObjectDataSourceCargos" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    
                                         TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.CargoTableAdapter" 
                                         DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                                        <DeleteParameters>
                                            <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                            <asp:Parameter Name="Original_CargoDET" Type="String" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="CargoDET" Type="String" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="CargoDET" Type="String" />
                                            <asp:Parameter Name="Original_CargoID" Type="Int32" />
                                            <asp:Parameter Name="Original_CargoDET" Type="String" />
                                        </UpdateParameters>
                                    </asp:ObjectDataSource>
                                    <asp:ListBox ID="ListBoxCargos" runat="server" AppendDataBoundItems="True" 
                                       DataSourceID="ObjectDataSourceCargos" CssClass="form-select"
                                        DataTextField="CargoDET" DataValueField="CargoID" Rows="1" >
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                    </asp:ListBox> 
                                </div>
                                <div class="form-item rightHalf">
                                     <asp:Label ID="lblResultado" runat="server" CssClass="descripcion" Font-Bold="True" Text="Calificación:"></asp:Label>
                
                                    <asp:ListBox ID="ListBoxResultado" runat="server" 
                                        CssClass="form-select" Rows="1">
                                        <asp:ListItem Selected="True" Value="0" >Todos</asp:ListItem>
                                        <asp:ListItem Value="No Satisfactorio">No Satisfactorio</asp:ListItem>
                                        <asp:ListItem Value="Regularmente cumple / Necesita desarrollarse">Regular</asp:ListItem>
                                        <asp:ListItem Value="Bueno">Bueno</asp:ListItem>
                                        <asp:ListItem Value="Muy Bueno">Muy Bueno</asp:ListItem>
                    
                    
                    
                                    </asp:ListBox>  
                               </div>
                                <div class="form-item leftHalf">            
                                   <asp:Label ID="lblEstado" runat="server" Text="Estado: " CssClass="descripcion" Font-Bold="True"></asp:Label>
                 
                                    <asp:ListBox ID="ListBoxEstado" runat="server" AppendDataBoundItems="True" 
                                         DataSourceID="ObjectDataSourceEstados"  CssClass="form-select"
                                        DataTextField="Descripcion" DataValueField="PasoID" Rows="1" >
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="-1">No iniciado</asp:ListItem>
                                    </asp:ListBox>
                                    <asp:ObjectDataSource ID="ObjectDataSourceEstados" runat="server" 
                                                 DeleteMethod="Delete" InsertMethod="Insert" 
                                                 OldValuesParameterFormatString="original_{0}" SelectMethod="GetTodos" 
                                                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter" 
                                                 UpdateMethod="Update">
                                                 <DeleteParameters>
                                                     <asp:Parameter Name="Original_PasoID" Type="Int32" />
                                                     <asp:Parameter Name="Original_Descripcion" Type="String" />
                                                 </DeleteParameters>
                                                 <UpdateParameters>
                                                     <asp:Parameter Name="Descripcion" Type="String" />
                                                     <asp:Parameter Name="Original_PasoID" Type="Int32" />
                                                     <asp:Parameter Name="Original_Descripcion" Type="String" />
                                                 </UpdateParameters>
                                                 <InsertParameters>
                                                     <asp:Parameter Name="PasoID" Type="Int32" />
                                                     <asp:Parameter Name="Descripcion" Type="String" />
                                                 </InsertParameters>
                                             </asp:ObjectDataSource>
                                   </div>
                                  <div class="form-item rightHalf">
                                    <asp:Label ID="lblLegajo" runat="server" CssClass="descripcion" 
                                         Font-Bold="True" Text="Legajo:"></asp:Label>
                          
                                   <asp:ListBox ID="ListBoxEvaluadoLegajo" runat="server" 
                                          DataSourceID="ObjectDataSourceEvaluadosLegajo" DataTextField="Legajo" 
                                          DataValueField="Legajo" 
                                          AppendDataBoundItems="True"  CssClass="form-select" Rows="1">
                                           <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                                              </asp:ListBox>               
                                   <asp:ObjectDataSource ID="ObjectDataSourceEvaluadosLegajo" runat="server" OldValuesParameterFormatString="original_{0}"
                                           SelectMethod="GetDataByPaso"     
                              
                                         TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
                                                  <SelectParameters>
                                                      <asp:Parameter DefaultValue="2" Name="PasoID" Type="Int32" />
                                                      <asp:ControlParameter ControlID="ddlPeriodo" DefaultValue="" Name="PeriodoID"
                                            PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                                                  </SelectParameters>
                                              </asp:ObjectDataSource> 
                                </div>
                               <div class="form-item leftHalf"> 
                                 <label>Formulario:</label>
              
                                     <asp:ListBox ID="ListBoxFormularios" runat="server" CssClass="form-select"
                                         DataSourceID="ObjectDataSourceFormularios" DataTextField="Descripcion" 
                                         DataValueField="TipoFormularioID" Rows="1" AppendDataBoundItems="True"> 
                                          <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>                    
                                     </asp:ListBox>
                                     <asp:ObjectDataSource ID="ObjectDataSourceFormularios" runat="server" 
                                         OldValuesParameterFormatString="original_{0}" 
                                         SelectMethod="GetDataByTipoPeriodoID" 
                                         TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.TipoFormularioTableAdapter">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="1" Name="TipoPeriodoID" Type="Int32" />
                                         </SelectParameters>
                                     </asp:ObjectDataSource>
                                 </div>
                                 <div class="form-item rightHalf">
                                 <label>Sector:</label>
                                  <asp:ListBox ID="ListBoxSector" runat="server" 
                                  DataSourceID="ObjectDataSourceSector" DataTextField="Descripcion" 
                                  DataValueField="SectorID" SelectionMode="Single" Rows="1"
                                  AppendDataBoundItems="True" CssClass="form-select">
                                   <asp:ListItem Value="0" Selected="True" >Todos</asp:ListItem>
                                      </asp:ListBox>
                                  <asp:ObjectDataSource ID="ObjectDataSourceSector" runat="server" 
                                      DeleteMethod="Delete" InsertMethod="Insert" 
                                      OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                                      TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.SectorTableAdapter" 
                                      UpdateMethod="Update">
                                      <DeleteParameters>
                                          <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                          <asp:Parameter Name="Original_Descripcion" Type="String" />
                                          <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                      </DeleteParameters>
                                      <InsertParameters>
                                          <asp:Parameter Name="SectorID" Type="Int32" />
                                          <asp:Parameter Name="Descripcion" Type="String" />
                                          <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                      </InsertParameters>
                                      <UpdateParameters>
                                          <asp:Parameter Name="Descripcion" Type="String" />
                                          <asp:Parameter Name="SectorIDPXXI" Type="String" />
                                          <asp:Parameter Name="Original_SectorID" Type="Int32" />
                                          <asp:Parameter Name="Original_Descripcion" Type="String" />
                                          <asp:Parameter Name="Original_SectorIDPXXI" Type="String" />
                                      </UpdateParameters>
                                  </asp:ObjectDataSource>          
                                 
                                 </div>
                                  <div class="controls"> 
                                  <table>
                                  <tr>
                                  <td><asp:Button ID="ButtonBuscarCalibracion" runat="server" Text="Buscar" /></td>
                                  
                                  <td> 
                                      <asp:Button ID="ButtonInicarPMP" runat="server" Text="IniciarPMPs" 
                                          onclick="ButtonInicarPMP_Click" />
                                  </td>
                                  </tr>
                                  <tr>
                                  <td colspan="2"><asp:Label ID="LabelResultado" runat="server" Text="La búsqueda no produjo resultados para los parámetros indicados." 
                                    CssClass="destacados" Visible="false"></asp:Label> </td>
                                  </tr>
                                  </table>       
                                    
                                    
                                     
                                </div>


                                 </div>
    
    </div>

    <div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSource1">
            <Columns>
                <asp:BoundField DataField="Indice" HeaderText="Indice" ReadOnly="True" 
                    SortExpression="Indice" />
                <asp:TemplateField HeaderText="periodoid" SortExpression="periodoid">
                    <ItemTemplate>
                        <asp:Label ID="LabelPeriodoID" runat="server" Text='<%# Bind("periodoid") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="tipoformularioid" 
                    SortExpression="tipoformularioid">
                    <ItemTemplate>
                        <asp:Label ID="LabelTipoFormularioID" runat="server" Text='<%# Bind("tipoformularioid") %>'></asp:Label>
                    </ItemTemplate>
                    
                </asp:TemplateField>
                <asp:BoundField DataField="TipoFormulario" HeaderText="TipoFormulario" 
                    SortExpression="TipoFormulario" />
                <asp:TemplateField HeaderText="Legajo" SortExpression="Legajo">
                    <ItemTemplate>
                        <asp:Label ID="LabelLegajo" runat="server" Text='<%# Bind("Legajo") %>'></asp:Label>
                    </ItemTemplate>                   
                </asp:TemplateField>
                <asp:BoundField DataField="ApellidoNombre" HeaderText="ApellidoNombre" 
                    SortExpression="ApellidoNombre" ReadOnly="True" />
                <asp:TemplateField HeaderText="LegajoEvaluador" 
                    SortExpression="LegajoEvaluador">
                    <ItemTemplate>
                        <asp:Label ID="LabelLegajoEvaluador" runat="server" Text='<%# Bind("LegajoEvaluador") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("LegajoEvaluador") %>'></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Evaluador" HeaderText="Evaluador" 
                    SortExpression="Evaluador" ReadOnly="True" />
                <asp:BoundField DataField="LegajoAudior" HeaderText="LegajoAudior" 
                    ReadOnly="True" SortExpression="LegajoAudior" />
                <asp:BoundField DataField="Auditor" HeaderText="Auditor" 
                    SortExpression="Auditor" ReadOnly="True" />
                <asp:TemplateField HeaderText="EstadoID" SortExpression="EstadoID">
                    <ItemTemplate>
                        <asp:Label ID="LabelEstadoID" runat="server" Text='<%# Bind("EstadoID") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("EstadoID") %>'></asp:Label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Estado" HeaderText="Estado" 
                    ReadOnly="True" SortExpression="Estado" />
                <asp:BoundField DataField="Calificacion" HeaderText="Calificacion" ReadOnly="True" 
                    SortExpression="Calificacion" />
                <asp:BoundField DataField="CalificacionPuntos" HeaderText="CalificacionPuntos" ReadOnly="True" 
                    SortExpression="CalificacionPuntos" />
                <asp:BoundField DataField="Sector" HeaderText="Sector" 
                    SortExpression="Sector" />
                <asp:BoundField DataField="Area" HeaderText="Area" SortExpression="Area" />
                <asp:BoundField DataField="Cargo" HeaderText="Cargo" SortExpression="Cargo" />
                <asp:BoundField DataField="linea" HeaderText="linea" 
                    SortExpression="linea" ReadOnly="True" />
                <asp:BoundField DataField="fIngreso" HeaderText="fIngreso" 
                    SortExpression="fIngreso" ReadOnly="True" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
            
            TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.eval_GetReportePMPOperariosNoIniciadosTableAdapter">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlPeriodo" Name="PeriodoID" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="ListBoxFormularios" Name="TipoFormulario" 
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="ListBoxEvaluado" Name="Evaluado" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxEvaluadoLegajo" Name="LegajoEvaluado" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxEvaluador" Name="Evaluador" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxSector" Name="Sector" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxArea" Name="Area" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxCargos" Name="Cargo" 
                    PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ListBoxResultado" Name="Calificacion" 
                    PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
