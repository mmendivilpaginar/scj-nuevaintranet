﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteSeguimientoOpImpresionAgrupadoAreaDireccionConDetalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
                    HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];

                if (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"]))
                    HiddenFieldLegajoEvaluador.Value = Request.QueryString["LegajoEvaluador"];

                if (!string.IsNullOrEmpty(Request.QueryString["Pasoid"]))
                    HiddenFieldPasoID.Value = Request.QueryString["Pasoid"];

                if (!string.IsNullOrEmpty(Request.QueryString["DireccionID"]))

                    HiddenFieldDireccionID.Value = Request.QueryString["DireccionID"];

                if (!string.IsNullOrEmpty(Request.QueryString["AreaID"]))

                    HiddenFieldAreaID.Value = Request.QueryString["AreaID"];

                if (!string.IsNullOrEmpty(Request.QueryString["Cluster"]))
                    HiddenFieldCluster.Value = Request.QueryString["Cluster"];

                FormulariosController FC = new FormulariosController();
                switch (FC.DameTipoPeriodoByPeriodoID(int.Parse(Request.QueryString["PeriodoID"].ToString())))
                {

                    case 1:
                        LabelTitulo.Text = "Reporte de Seguimiento Full Year";
                        break;
                    case 2:
                        LabelTitulo.Text = "Reporte de Seguimiento Mid Year";
                        break;
                    case 3:
                        LabelTitulo.Text = "Reporte de Seguimiento Full Year";
                        break;

                }


            }

        }

        protected void RepeaterIN_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                Label lblEmpty = (Label)e.Item.FindControl("lblEmpty");
                if (((Repeater)sender).Items.Count < 1)
                    lblEmpty.Visible = true;

                else
                    lblEmpty.Visible = false;
            }
        }

        protected void RepeaterEvaluaciones_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {

                Label LabelTitulo = (Label)e.Item.FindControl("LabelTitulo");
                if (Request.QueryString["MostrarAreas"].ToString() == "0")
                    LabelTitulo.Text = "<br  style='mso-data-placement: same-cell;'/>Dirección";
                else

                    LabelTitulo.Text = "Área/<br  style='mso-data-placement: same-cell;'/>Dirección ";

                if (Request.QueryString["IncluirNoConcluidas"].ToString() == "NO")
                {
                    HtmlTableCell COL_TO_HIDE_HEADER1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER1");
                    HtmlTableCell COL_TO_HIDE_HEADER2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER2");
                    HtmlTableCell COL_TO_HIDE_HEADER3 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE_HEADER3");
                    COL_TO_HIDE_HEADER1.Visible = false;
                    COL_TO_HIDE_HEADER2.Visible = false;
                    COL_TO_HIDE_HEADER3.Visible = false;
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (Request.QueryString["IncluirNoConcluidas"].ToString() == "NO")
                {
                    HtmlTableCell COL_TO_HIDE1 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE1");
                    COL_TO_HIDE1.Visible = false;

                    HtmlTableCell COL_TO_HIDE2 = (HtmlTableCell)e.Item.FindControl("COL_TO_HIDE2");
                    COL_TO_HIDE2.Visible = false;


                }

                HiddenField HiddenFieldAreaID = (HiddenField)e.Item.FindControl("HiddenFieldAreaID1");
                HiddenField HiddenFieldDireccionID = (HiddenField)e.Item.FindControl("HiddenFieldDireccionID");
                Label LabelCantNoIniciadas = (Label)e.Item.FindControl("LabelCantNoIniciadas");
                Label LabelCantEnEvaluado = (Label)e.Item.FindControl("LabelCantEnEvaluado");
                Label LabelCantEnLT = (Label)e.Item.FindControl("LabelCantEnLT");
                Label LabelCantAprobadas = (Label)e.Item.FindControl("LabelCantAprobadas");
                Label LabelNoConcluidas = (Label)e.Item.FindControl("LabelNoConcluidas");

                Label LabelCantTotal = (Label)e.Item.FindControl("LabelCantTotal");

                Label LabelCantEnAuditor = (Label)e.Item.FindControl("LabelCantEnAuditor");

                Repeater RepeaterEnNoIniciados = (Repeater)e.Item.FindControl("RepeaterEnNoIniciados");
                Repeater RepeaterEnEvaluado = (Repeater)e.Item.FindControl("RepeaterEnEvaluado");
                Repeater RepeaterEnEvaluador = (Repeater)e.Item.FindControl("RepeaterEnEvaluador");
                Repeater RepeaterAprobadas = (Repeater)e.Item.FindControl("RepeaterAprobadas");
                Repeater RepeaterNoConcluidas = (Repeater)e.Item.FindControl("RepeaterNoConcluidas");
                Repeater RepeaterEnAuditor = (Repeater)e.Item.FindControl("RepeaterEnAuditor");
                Repeater RepeaterTotal = (Repeater)e.Item.FindControl("RepeaterTotal");

                if (Request.QueryString["IncluirDetalle"].ToString() == "NO" || (string.IsNullOrEmpty(HiddenFieldAreaID.Value) && (Request.QueryString["MostrarAreas"].ToString() == "1" || string.IsNullOrEmpty(HiddenFieldDireccionID.Value))))
                {

                    RepeaterEnNoIniciados.Visible = RepeaterEnEvaluado.Visible = RepeaterEnEvaluador.Visible = RepeaterAprobadas.Visible = RepeaterNoConcluidas.Visible =  RepeaterEnAuditor.Visible= false;
                   // LabelCantNoIniciadas.Visible = LabelCantEnEvaluado.Visible = LabelCantEnLT.Visible = LabelCantAprobadas.Visible = LabelNoConcluidas.Visible =  LabelCantEnAuditor.Visible= true;


                }
                else
                {

                   // LabelCantNoIniciadas.Visible = LabelCantEnEvaluado.Visible = LabelCantEnLT.Visible = LabelCantAprobadas.Visible = LabelNoConcluidas.Visible = LabelCantEnAuditor.Visible = false;

                    if (HiddenFieldPasoID.Value == "0" || HiddenFieldPasoID.Value == "")
                        RepeaterEnNoIniciados.Visible = RepeaterEnEvaluado.Visible = RepeaterEnEvaluador.Visible = RepeaterAprobadas.Visible = RepeaterNoConcluidas.Visible = RepeaterEnAuditor.Visible = true;
                    else
                    {
                        RepeaterEnNoIniciados.Visible = (HiddenFieldPasoID.Value == "-1");
                        RepeaterEnEvaluado.Visible = (HiddenFieldPasoID.Value == "2");
                        RepeaterEnEvaluador.Visible = (HiddenFieldPasoID.Value == "1");
                        RepeaterAprobadas.Visible = (HiddenFieldPasoID.Value == "4");
                        RepeaterNoConcluidas.Visible = (HiddenFieldPasoID.Value == "6");
                        RepeaterEnAuditor.Visible = (HiddenFieldPasoID.Value == "3");

                    }


                }

            }




        }

        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {
            if (RepeaterEvaluaciones.Visible == true)
            {
                if (RepeaterEvaluaciones.Items.Count != 0)
                {
                    DateTime fecha = DateTime.Now;
                    string filename = "";
                    filename = "Reporte_de_Seguimiento" + String.Format("{0: ddMMyyyy}", fecha);

                    Exportar(RepeaterEvaluaciones, filename, "Reporte de Seguimiento");
                    //ExportarPDF(RepeaterEvaluaciones, filename, "Reporte de Seguimeinto");
                }
            }
        }

        protected void Exportar(Repeater gv, string filename, string titulo)
        {
            Response.Clear();
            Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name> Reportes PMP</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";

            Response.Write(generarExportarTodo(gv, titulo));

            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }

        private string generarExportarTodo(Repeater gv, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Repeater gvaux = new Repeater();
            gvaux = gv;

            foreach (RepeaterItem GVITEM in gvaux.Items)
            {
                Repeater RepeaterEnNoIniciados = (Repeater)GVITEM.FindControl("RepeaterEnNoIniciados");
                Repeater RepeaterEnEvaluado = (Repeater)GVITEM.FindControl("RepeaterEnEvaluado");
                Repeater RepeaterEnEvaluador = (Repeater)GVITEM.FindControl("RepeaterEnEvaluador");
                Repeater RepeaterNoConcluidas = (Repeater)GVITEM.FindControl("RepeaterNoConcluidas");
                Repeater RepeaterAprobadas = (Repeater)GVITEM.FindControl("RepeaterAprobadas");

                foreach (RepeaterItem ITEMIN in RepeaterEnNoIniciados.Items)
                {
                    HtmlGenericControl brNoIniciadas = (HtmlGenericControl)ITEMIN.FindControl("brNoIniciados");
                    brNoIniciadas.Visible = false;
                }

                foreach (RepeaterItem ITEMIN in RepeaterAprobadas.Items)
                {
                    HtmlGenericControl brAprobadas = (HtmlGenericControl)ITEMIN.FindControl("brAprobadas");
                    brAprobadas.Visible = false;
                }

                foreach (RepeaterItem ITEMIN in RepeaterEnEvaluado.Items)
                {
                    HtmlGenericControl brEnEvaluado = (HtmlGenericControl)ITEMIN.FindControl("brEnEvaluado");
                    brEnEvaluado.Visible = false;
                }

                foreach (RepeaterItem ITEMIN in RepeaterEnEvaluador.Items)
                {
                    HtmlGenericControl brEnEvaluador = (HtmlGenericControl)ITEMIN.FindControl("brEnEvaluador");
                    brEnEvaluador.Visible = false;
                }

                foreach (RepeaterItem ITEMIN in RepeaterNoConcluidas.Items)
                {
                    HtmlGenericControl brNoConcluidas = (HtmlGenericControl)ITEMIN.FindControl("brNoConcluidas");
                    brNoConcluidas.Visible = false;
                }
            }





            Page page = new Page();
            HtmlForm form = new HtmlForm();

            gvaux.EnableViewState = false;


            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);


            form.Controls.Add(gvaux);

            page.RenderControl(htw);

            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
            string HTML;
            HTML = regEx.Replace(sb.ToString(), "");


            return HTML;
        }

        protected string DameNombreColumna(object Nombre1, object Nombre2)
        {
            if (!string.IsNullOrEmpty(Nombre1.ToString()))
                return (Nombre1.ToString());
            else
            {
                if (!string.IsNullOrEmpty(Nombre2.ToString()))
                {
                    if (Request.QueryString["MostrarAreas"].ToString() == "0")
                        return Nombre2.ToString();
                    if (Nombre2.ToString().Length < 18)
                        return ("Dirección: <br  style='mso-data-placement: same-cell;'/>" + Nombre2.ToString());
                    else
                        return ("Direcciónn: " + Nombre2.ToString());
                }
                else
                    return "Total";
            }

        }

        protected void RepeaterEvaluaciones_PreRender(object sender, EventArgs e)
        {
            if (RepeaterEvaluaciones.Items.Count == 0)
            {
                RepeaterEvaluaciones.Visible = false;
                lblEmpty.Visible = true;
            }
        }
    }
}