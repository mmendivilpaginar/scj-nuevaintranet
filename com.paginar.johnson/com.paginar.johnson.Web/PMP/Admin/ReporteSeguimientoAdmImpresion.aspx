﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoAdmImpresion.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimientoAdmImpresion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Johnson Intranet</title>
    <link href="../css/printReporteSeguimiento.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            //window.close();
        }

        
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
    
       <div id="header">
            <div class="bg"></div>
            <h1>Reporte de Seguimiento PMP Administrativos</h1>
            <img id="logo" src="../../images/logoprint.gif" />
        </div>

        <div id="noprint">           
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />           
            
            <asp:Button ID="ButtonExportarExcel" runat="server" Text="Exportar excel" 
                onclick="ButtonExportarExcel_Click"/>
            <br /><br />
        </div>


        <div id="PanelCompleto" runat="server">
            <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" />
            <asp:HiddenField ID="HiddenFieldLegajoEvaluador" runat="server" />
            <asp:HiddenField ID="HiddenFieldPasoID" runat="server" />            
            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server" /> 
            <asp:HiddenField ID="HiddenFieldPasoIDNI" value="NI" runat="server" />   
            <asp:HiddenField ID="HiddenFieldPasoIDG" value="G" runat="server" /> 
            <asp:HiddenField ID="HiddenFieldPasoIDE" value="E" runat="server" />
            <asp:HiddenField ID="HiddenFieldPasoIDA" value="A" runat="server" />         
            <asp:Repeater ID="RepeaterEvaluaciones" runat="server"  
                DataSourceID="ObjectDataSourceEvaluaciones" onitemdatabound="RepeaterEvaluaciones_ItemDataBound"
                >
            <HeaderTemplate>
                <table>
                        <tr style="height:20px;" >
                            <th align="center" style="padding:0px; background-color:gray;width: 200px;" rowspan="3">
                            <div style="position:relative;"><div class="bg3"></div>
                                Direccion</div></th>
                            <th align="center" style="padding:0px; background-color:gray;" rowspan="2" colspan="2">
                            <div style="position:relative;"><div class="bg2"></div>
                                No Iniciadas</div> </th>
                            <th align="center" style="padding:0px; background-color:gray;" colspan="4">
                            <div style="position:relative;"><div class="bg"></div>
                                Iniciadas</div> </th>
                           <th align="center" style="padding:0px; background-color:gray;" rowspan="2" colspan="2">
                            <div style="position:relative;"><div class="bg2"></div>
                                Totales</div> </th>
                            
                        </tr>
                        <tr style="height:20px;" >
                            
                            <th align="center" style="padding:0px; background-color:gray;" colspan="2">
                            <div style="position:relative;"><div class="bg"></div>
                                Abiertas</div> </th>
                            <th align="center" style="padding:0px; background-color:gray;" colspan="2">
                            <div style="position:relative;"><div class="bg"></div>
                                Aprobadas</div> </th>
                           
                        </tr>
                        <tr style="height:30px;" >
                            
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;"><div class="bg4"></div>
                                Total</div> </th>
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative; width: 50px;"><div class="bg4"></div>
                                %</div> </th>
                           <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;"><div class="bg4"></div>
                                Total No Enviadas</div> </th>
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;"><div class="bg" style=" top:-15"></div>
                                Total  Enviadas</div> </th>                               
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;"><div class="bg1"></div>
                                Total</div> </th>
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;  width: 50px;"><div class="bg1"></div>
                                % </div> </th>
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;"><div class="bg1"></div>
                                Total </div> </th>
                            <th align="center" style="padding:0px; background-color:gray;">
                            <div style="position:relative;  width: 50px;"><div class="bg1"></div>
                                % </div> </th>
                        </tr>            
            </HeaderTemplate>
            <ItemTemplate>

                       <tr style="height: 40px;">
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="LabelEvaluador" runat="server" Text='<%# Bind("Evaluador") %>'></asp:Label> </div></td>                       
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label1" runat="server" Text='<%# Eval("CantNoIniciadas") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label8" runat="server" Text='<%# Eval("PorcNoIniciadas") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label2" runat="server" Text='<%# Eval("CantBorrador") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label5" runat="server" Text='<%# Eval("CantEnviadas") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label6" runat="server" Text='<%# Eval("CantAprobadas") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label7" runat="server" Text='<%# Eval("PorcAprobadas") %>'></asp:Label> </div></td>
                      <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label9" runat="server" Text='<%# Eval("CantTotalEvaluador") %>'></asp:Label> </div></td>
                       <td align="center" style=" padding:0px;background-color:#C0C0C0;"><div style="position:relative;"><div class="bgSubT2"></div>
                           <asp:Label ID="Label10" runat="server" Text='<%# Eval("PorcTotal") %>'></asp:Label> </div></td>
                       </tr>   
                       <tr>
                       <td align="center">
                        Detalle Usuarios...
                       
                        </td>
                       <td align="center">
                            <asp:HiddenField ID="HiddenFieldEvaluador" runat="server"  Value='<%# Bind("LegajoEvaluador")%>'/> 
                            <asp:HiddenField ID="HiddenFieldDireccionID" runat="server"  Value='<%# Bind("DireccionID")%>'/>
                                                               
                            <asp:Repeater ID="RepeaterNoIniciadas" runat="server" DataSourceID="ObjectDataSourceNoIniciadas" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                            </ItemTemplate>
                            </asp:Repeater>
                         
                       
                        <asp:ObjectDataSource ID="ObjectDataSourceNoIniciadas" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:QueryStringParameter Name="LegajoEvaluador" QueryStringField="LegajoEvaluador" Type="Int32" />                               
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDNI" Name="EstadoID" 
                                        PropertyName="Value" Type="String" />                                    
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                       </td>
                       <td></td>
                       <td align="center">
                            
                                                               
                            <asp:Repeater ID="RepeaterBorrador" runat="server" DataSourceID="ObjectDataSourceBorrador" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                            </ItemTemplate>
                            </asp:Repeater>
                         
                       
                        <asp:ObjectDataSource ID="ObjectDataSourceBorrador" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:QueryStringParameter Name="LegajoEvaluador" QueryStringField="LegajoEvaluador" Type="Int32" />                                
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDG" Name="EstadoID" 
                                        PropertyName="Value" Type="String" />                                    
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                       </td>
                       <td align="center">
                                                                  
                            <asp:Repeater ID="RepeaterEnviadas" runat="server" DataSourceID="ObjectDataSourceEnviadas" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                            </ItemTemplate>
                            </asp:Repeater>
                        
                       
                        <asp:ObjectDataSource ID="ObjectDataSourceEnviadas" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:QueryStringParameter Name="LegajoEvaluador" 
                            QueryStringField="LegajoEvaluador" Type="Int32" />                                
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDE" Name="EstadoID" 
                                        PropertyName="Value" Type="String" />                                    
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>

                       </td>
                       <td align="center" > 
                       
                            <asp:Repeater ID="RepeaterAprobadas" runat="server" DataSourceID="ObjectDataSourceAprobadas" >
                                <ItemTemplate>
                                <br  style="mso-data-placement: same-cell;"/> 
                                    <%# Eval("ApellidoNombre").ToString() + Eval("Legajo", " - {0}")%>
                                <br  style="mso-data-placement: same-cell;"/>
                                </ItemTemplate>
                            </asp:Repeater>

                           
                       
                        <asp:ObjectDataSource ID="ObjectDataSourceAprobadas" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
                            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosByEvaluadorTableAdapter">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                                    Type="Int32" />
                                <asp:QueryStringParameter Name="LegajoEvaluador" 
                            QueryStringField="LegajoEvaluador" Type="Int32" />                                
                                <asp:ControlParameter ControlID="HiddenFieldPasoIDA" Name="EstadoID" 
                                        PropertyName="Value" Type="String" />                                    
                                <asp:ControlParameter ControlID="HiddenFieldDireccionID" Name="DireccionID" 
                                        PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>                                            
                       </td>
                                             
                       <td align="center">                       
                       
                       
                       </td>
                       <td></td>
                       <td></td>
                       

                       </tr>   
                             
            </ItemTemplate>

            <FooterTemplate>
             <asp:Repeater ID="RepeaterFooter" runat="server" DataSourceID="ObjectDataSourceReporteSeguimientoTotales">
             <ItemTemplate>
              <tr style="height: 40px; " >
                    <td align="center" style=" padding:0px;background-color:gray;"><div style="position:relative;"><div class="bg"></div>TOTAL </div></td>
                    <td align="center" style=" padding:0px;background-color:gray;">
                    <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="LabelTotalEnBorrador" runat="server" Text='<%#Eval("CantNoIniciadas") %>'></asp:Label></div></td>
                   <td align="center" style=" padding:0px;background-color:gray;">
                   <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="Label11" runat="server" Text='<%#Eval("PorcNoIniciadas"," {0} %") %>'></asp:Label></div></td>  
                   <td align="center" style=" padding:0px;background-color:gray;">
                    <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="Label3" runat="server" Text='<%#Eval("CantBorrador") %>'></asp:Label></div></td>                                      
                    <td align="center" style=" padding:0px;background-color:gray;">
                    <div style="position:relative;"><div class="bg"></div>
                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("CantEnviadas") %>'></asp:Label></div></td>
                    <td align="center" style=" padding:0px;background-color:gray;" >
                       <div style="position:relative;"><div class="bg"></div>
                       <asp:Label ID="LabelTotalCerradas" runat="server" Text='<%#Eval("CantAprobadas") %>'></asp:Label></div></td>
                   
                   <td align="center" style=" padding:0px;background-color:gray;">
                        <div style="position:relative;"><div class="bg"></div>
                        <asp:Label ID="LabelPorcentajeAprobadas" runat="server" Text='<%#Eval("PorAprobadas"," {0} %") %>'></asp:Label></div></td> 
                  <td align="center" style=" padding:0px;background-color:gray;">
                        <div style="position:relative;"><div class="bg"></div>
                        <asp:Label ID="Label12" runat="server" Text='<%#Eval("CantTotal") %>'></asp:Label></div></td>                          
                 <td align="center" style=" padding:0px;background-color:gray;">
                        <div style="position:relative;"><div class="bg"></div>
                        <asp:Label ID="Label13" runat="server" Text='<%#Eval("PorTotal"," {0} %") %>'></asp:Label></div></td>                         
                      
                </tr>
             </ItemTemplate>
             </asp:Repeater>
               

                <asp:ObjectDataSource ID="ObjectDataSourceReporteSeguimientoTotales" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                    TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativosTotalesTableAdapter">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                            Type="Int32" />
                        <asp:QueryStringParameter Name="LegajoEvaluador" 
                            QueryStringField="LegajoEvaluador" Type="Int32" />
                        <asp:QueryStringParameter Name="Pasoid" QueryStringField="PasoID" 
                            Type="String" />                        
                        <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                            QueryStringField="DireccionID" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </table>

             </FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
                 OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"                                            
                TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.Eval_GetReporteSeguimientoAministrativos1TableAdapter">
                <SelectParameters>
                    <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                        Type="Int32" />
                    <asp:QueryStringParameter Name="LegajoEvaluador" 
                        QueryStringField="LegajoEvaluador" Type="Int32" />
                    <asp:QueryStringParameter Name="Pasoid" QueryStringField="PasoID" 
                        Type="String" />
                    <asp:Parameter DefaultValue="0" Name="TipoFormularioID" Type="Int32" />
                    <asp:QueryStringParameter DefaultValue="" Name="DireccionID" 
                        QueryStringField="DireccionID" Type="Int32" />
                    <asp:Parameter DefaultValue="" Name="Cluster" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>  
       </div>
    
    </div>
    </form>
</body>
</html>
