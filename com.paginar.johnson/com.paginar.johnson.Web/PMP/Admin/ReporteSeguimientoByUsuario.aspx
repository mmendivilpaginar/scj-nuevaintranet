﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoByUsuario.aspx.cs" Inherits="com.paginar.johnson.Web.servicios.ReporteSeguimientoByUsuario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <title></title>
<script  type="text/javascript">
    function popup(url, ancho, alto) {
        var posicion_x;
        var posicion_y;
        posicion_x = (screen.width / 2) - (ancho / 2);
        posicion_y = (screen.height / 2) - (alto / 2);
        window.open(url, "PmpOperarios", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

    }
</script>
</head>
<body>

    <form id="form1" runat="server">
    <table width="100%">
        <tr>
            <td>
                <span runat="server" ID="PanelEvaluador">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="Label1" runat="server" Text="Evaluador:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterLT" runat="server" 
        DataSourceID="ObjectDataSourceEvaluador">
        <ItemTemplate>
            <asp:Label ID="LiteralLT" CssClass="destacados"  Text='<%# Eval("ApellidoNombre") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
                
    </span>
    <br />
    <span>
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label> &nbsp;<asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label3" runat="server" Text="Estado:" Font-Size="14px"></asp:Label> &nbsp;<asp:Repeater ID="RepeaterPaso" runat="server" 
        DataSourceID="ObjectDataSourceEstado">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralEstado" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:label>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Label ID="LabelCalifiacion" CssClass="destacados" runat="server" Font-Size="12px"></asp:Label>
    <br />
    </span>
                <br />
                &nbsp;
            </td>
            <td align="right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/LogoJSC.jpg" /><br /></td>
        </tr>
    </table>
   <div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones" 
        onrowdatabound="GridViewEvaluados_RowDataBound">
        <Columns>
            <asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" 
                Visible="False" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("periodoid") %>'/>
                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value= '<%# Bind("tipoformularioid") %>' />
                    <asp:LinkButton ID="lnkBtnApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'  >LinkButton</asp:LinkButton>
                    <asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Area" HeaderText="Area" SortExpression="Area" />
            <asp:BoundField DataField="Cargo" HeaderText="Cargo" SortExpression="Cargo" />
            <asp:BoundField DataField="Falta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Fecha" SortExpression="Falta" />
            <asp:BoundField DataField="Calificacion" HeaderText="Resultado" 
                SortExpression="Calificacion" />
            <asp:BoundField DataField="Evaluador" HeaderText="Evaluador" 
                SortExpression="Evaluador" />
        </Columns>
    </asp:GridView>
   </div>
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteDSTableAdapters.GetReporteSegumientoByEvaluadorTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="LegajoEvaluador" 
                QueryStringField="LegajoEvaluador" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="" Name="Pasoid" 
                QueryStringField="Pasoid" Type="Int32" />
            <asp:QueryStringParameter Name="TipoFormularioID" 
                QueryStringField="TipoFormularioID" Type="Int32" />
            <asp:QueryStringParameter Name="Calificacion" QueryStringField="Calificacion" 
                Type="String" />
            <asp:QueryStringParameter DefaultValue="" Name="EstadoID2" 
                QueryStringField="EstadoID2" Type="Int32" />
                <asp:QueryStringParameter DefaultValue="0" Name="DireccionID" 
                QueryStringField="DireccionID" Type="Int32" />
            <asp:Parameter DefaultValue="0" Name="Cluster" Type="Int32" />
            <asp:QueryStringParameter DefaultValue="0" Name="AreaID" 
                QueryStringField="AreaID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluador" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBylegajo" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.vwUsuariosTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="legajo" QueryStringField="LegajoEvaluador" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceEstado" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPasoID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_PasoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Original_PasoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PasoID" QueryStringField="EstadoID2" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="PasoID" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    </form>
</body>
</html>
