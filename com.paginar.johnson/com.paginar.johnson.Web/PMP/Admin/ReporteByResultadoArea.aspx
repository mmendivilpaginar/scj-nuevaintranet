<%@ Page Title="" Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master" AutoEventWireup="true" Inherits="pmp_Admin_ReporteByResultadoArea" Codebehind="ReporteByResultadoArea.aspx.cs" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                

    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Per�odo:" Font-Size="14px"></asp:Label>
     &nbsp;<asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelResultado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label3" runat="server" Text="Resultado:" Font-Size="14px"></asp:Label> 
    <asp:Label CssClass="destacados"  ID="LiteralResultado" Text='' runat="server" Font-Size="12px"></asp:Label>      
    <asp:Label ID="LabelResultado" CssClass="destacados" runat="server" Font-Size="12px"></asp:Label>    
    </span>
    <br />
    <span runat="server" ID="PanelArea">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="Label1" runat="server" Text="Area:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterArea" runat="server" 
        DataSourceID="ObjectDataSourceArea">
        <ItemTemplate>
            <asp:Label ID="LiteralArea" CssClass="destacados"  Text='<%# Eval("AreaDesc") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
                
    </span>
    <br />
    &nbsp;</td>
            <td align="right">
                <img alt="" src="../img/LogoJSC.jpg" style="width: 195px; height: 74px" /><br /></td>
        </tr>
    </table>
<div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones" 
        onrowdatabound="GridViewEvaluados_RowDataBound">
        <Columns>
            <%--<asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" Visible="false" />--%>
            <%--<asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />--%>
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("periodoid") %>'/>
                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value= '<%# Bind("tipoformularioid") %>' />
                    <asp:LinkButton ID="lnkBtnApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'  >LinkButton</asp:LinkButton>
                    <%--<asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
           
           <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" SortExpression="TipoFormulario" />
            <asp:BoundField DataField="Competencia" 
                HeaderText="Competencia" SortExpression="Competencia" />
            <asp:TemplateField HeaderText="CalificacionCompetencia" 
                SortExpression="CalificacionCompetencia">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" 
                        Text='<%# Bind("CalificacionCompetencia") %>'></asp:Label>
                </ItemTemplate>
                <HeaderTemplate>
                  <asp:Label ID="Label1" runat="server" 
                        Text='Calificaci�n <br/> Competencia'></asp:Label>
                </HeaderTemplate>
                
            </asp:TemplateField>
            <asp:BoundField DataField="Area" HeaderText="Sector" SortExpression="Area" />
            <asp:BoundField DataField="Falta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Fecha" SortExpression="Falta" />
            <%--<asp:BoundField DataField="Calificacion" HeaderText="Resultado" 
                SortExpression="Calificacion" />
            <asp:BoundField DataField="Evaluador" HeaderText="Evaluador" 
                SortExpression="Evaluador" />--%>
            
        </Columns>
    </asp:GridView>
</div>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>    
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteByResultadoAreaTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="TipoFormularioID" 
                QueryStringField="TipoFormularioID" Type="Int32" />
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="Ponderacion" QueryStringField="Calificacion" 
                Type="String" />
            <asp:QueryStringParameter Name="AreaID" 
                QueryStringField="AreaID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceArea" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByAreaID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_AreaID" Type="Int32" />
            <asp:Parameter Name="Original_AreaDESC" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="AreaDESC" Type="String" />
            <asp:Parameter Name="Original_AreaID" Type="Int32" />
            <asp:Parameter Name="Original_AreaDESC" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="areaid" QueryStringField="AreaID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="AreaDESC" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>

