﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;
using System.Data;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteObjetivos : System.Web.UI.Page
    {


        private RepositoryReporteAdmin _Repistorio = new RepositoryReporteAdmin();
        public RepositoryReporteAdmin Repositorio
        {
            get
            {
                if (_Repistorio == null) _Repistorio = new RepositoryReporteAdmin();
                return _Repistorio;
            }
        }

        private DSReporteAdmin.Eval_GetReporteObjetivosDataTable _dt;
        public DSReporteAdmin.Eval_GetReporteObjetivosDataTable dt
        {
            get
            {
                if (_dt == null) _dt = buscar();
                return _dt;
            }
            set
            {
                _dt = value;
            }
        }





        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.IsPostBack)
            {

                

            }
        }



        protected DataTable DistinctRows(DataTable dt, string keyfield)
        {
            DataTable newTable = dt.Clone();
            int keyval = 0;
            DataView dv = dt.DefaultView;
            dv.Sort = keyfield;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr1 in dt.Rows)
                {
                    bool existe = false;
                    foreach (DataRow dr2 in newTable.Rows)
                    {
                        if (dr1[keyfield].ToString() == dr2[keyfield].ToString())
                            existe = true;
                    }

                    if (!existe)
                    {
                        newTable.ImportRow(dr1);
                    }
                }
            }
            else
                newTable = dt.Clone();
            return newTable;
        }


        protected DSReporteAdmin.Eval_GetReporteObjetivosDataTable buscar()
        {
            int Periodo = int.Parse(DropDownListPeriodo.SelectedValue);

            int direccion = int.Parse(DropDownListDireccion.SelectedValue);

            int area = int.Parse(DropDownListAreaRS.SelectedValue);


            int cluster = int.Parse(DropDownListCluster.SelectedValue);


            return Repositorio.GetObjetivosDireccion(Periodo, direccion, area, cluster);
        }


        protected void ButtonBuscar(object sender, EventArgs e)
        {
            totalcompleto = 0;
            totalsincompletar = 0;

            dt = buscar();

            rptDireccion.DataSource = DistinctRows((DataTable) dt, "DireccionID");
            rptDireccion.DataBind();
        }

 

        int totalcompleto, totalsincompletar;

        protected void rptDireccion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hd = (HiddenField)e.Item.FindControl("hdDireccionID");
                

                DataRow[] drcompletos    =  dt.Select("  direccionid = " + hd.Value.ToString() + " and objetivos = 1 ");
                DataRow[] drsincompletar =  dt.Select("  direccionid = " + hd.Value.ToString() + " and objetivos = 0 ");

                int  completos  = drcompletos.Count();
                int sincompletar = drsincompletar.Count();

                totalcompleto += completos;
                totalsincompletar += sincompletar;

                Label lblCompletados = (Label)e.Item.FindControl("lblCompletados");
                Label lblSincompletar = (Label)e.Item.FindControl("lblSincompletar");

                lblCompletados.Text = completos.ToString();
                lblSincompletar.Text = sincompletar.ToString();

                if (chkDetalle.Checked)
                {
                    Repeater rpt = (Repeater)e.Item.FindControl("rptEvaluados");
                    rpt.DataSource = dt.Select("  direccionid = " + hd.Value.ToString());
                    rpt.DataBind();
                }
            }

            if(e.Item.ItemType ==  ListItemType.Footer)
            {
                Label lblTotalCompletos = (Label)e.Item.FindControl("lblTotalCompletos");
                Label lblTotalSinCompletar = (Label)e.Item.FindControl("lblTotalSinCompletar");

                lblTotalCompletos.Text = totalcompleto.ToString();
                lblTotalSinCompletar.Text = totalsincompletar.ToString();
            }
               

          

        }



        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {

            Exportar(rptDireccion, "Reporte Control de carga de Objetivos", "Reporte Control de carga de Objetivos");

        }


        protected void Exportar(Repeater rpt, string filename, string titulo)
        {
            if (rpt.Items.Count > 0)
            {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write("<strong>" + LabelTituloPrint.Text + "</strong>");
                Response.Write(generarExportarTodo(rpt, titulo));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
        }



        private string generarExportarTodo(Repeater rpt, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();




            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(rpt);
            page.RenderControl(htw);
            return sb.ToString();
        }



    }
}