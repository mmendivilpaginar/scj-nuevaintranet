<%@ Page Title="" Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master" AutoEventWireup="true" Inherits="pmp_Admin_ReporteByItemEvaluacion" Codebehind="ReporteByItemEvaluacion.aspx.cs" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }
    </script>
<link href="../../css/styles.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                

    <span runat="server" ID="PanelPeriado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Per�odo:" Font-Size="14px"></asp:Label>
     &nbsp;<asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelResultado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label3" runat="server" Text="Resultado:" Font-Size="14px"></asp:Label> 
    <asp:Label CssClass="destacados"  ID="LiteralResultado" Text='' runat="server" Font-Size="12px"></asp:Label>      
    <asp:Label ID="LabelResultado" CssClass="destacados" runat="server" Font-Size="12px"></asp:Label>    
    </span>
    <br />
    <span runat="server" ID="PanelCompetencia">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="LabelSeccion" runat="server" Text="Competencia:" Font-Size="14px"></asp:Label> &nbsp;
   <%-- <asp:Repeater ID="RepeaterCompetencia" runat="server" 
        DataSourceID="ObjectDataSourceCompetencia">
        <ItemTemplate>--%>
            <asp:Label ID="LiteralCompetencia" CssClass="destacados"  Text='' runat="server" Font-Size="12px"></asp:Label>
       <%-- </ItemTemplate>
    </asp:Repeater>--%>
                
    </span>
    <br />
    &nbsp;</td>
            <td align="right">
                <img alt="" src="../img/LogoJSC.jpg" style="width: 195px; height: 74px" /><br /></td>
        </tr>
    </table>
<div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones" 
        onrowdatabound="GridViewEvaluados_RowDataBound">
        <Columns>
            <asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" Visible="false" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("periodoid") %>'/>
                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value= '<%# Bind("tipoformularioid") %>' />
                    <asp:LinkButton ID="lnkBtnApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'  >LinkButton</asp:LinkButton>
                   <%-- <asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" SortExpression="TipoFormulario" />
            <asp:BoundField DataField="Area" HeaderText="Sector" SortExpression="Area" />
            <asp:BoundField DataField="Cargo" HeaderText="Cargo" SortExpression="Cargo" />
            <asp:BoundField DataField="Falta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Fecha" SortExpression="Falta" />
            <%--<asp:BoundField DataField="Calificacion" HeaderText="Resultado" 
                SortExpression="Calificacion" />
            <asp:BoundField DataField="Evaluador" HeaderText="Evaluador" 
                SortExpression="Evaluador" />--%>
        </Columns>
    </asp:GridView>
</div>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>    
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteByItemEvaluacionTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PasoID" QueryStringField="PasoID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="TipoFormularioID" 
                QueryStringField="TipoFormularioID" Type="Int32" />
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
            <asp:QueryStringParameter Name="Ponderacion" QueryStringField="Calificacion" 
                Type="Int32" />
            <asp:QueryStringParameter Name="titulo" 
                QueryStringField="Titulo" Type="String" />
            <asp:QueryStringParameter Name="SeccionID" QueryStringField="SeccionID" 
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <%--<asp:ObjectDataSource ID="ObjectDataSourceCompetencia" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataByItemEvaluacionID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="ItemEvaluacionID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Titulo" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="SeccionID" Type="Int32" />
            <asp:Parameter Name="Activo" Type="Boolean" />
            <asp:Parameter Name="TipoFormularioID" Type="Int32" />
            <asp:Parameter Name="ItemEvaluacionID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="ItemEvaluacionID" 
                QueryStringField="ItemEvaluacionID" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Titulo" Type="String" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="SeccionID" Type="Int32" />
            <asp:Parameter Name="Activo" Type="Boolean" />
            <asp:Parameter Name="TipoFormularioID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>--%>
</asp:Content>

