﻿<%@ Page Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master"  AutoEventWireup="true" CodeBehind="ReporteByStatus.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteByStatus" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }
    </script>
<link href="../../css/styles.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                

    <span runat="server" ID="PanelPeriado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label>
     &nbsp;    
     <asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="14px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelTipoFormulario">
    <asp:Label ID="lblTipoFormulario" Font-Bold="true" CssClass="destacados"  Text='PMP Planta Total' runat="server" Font-Size="14px" Visible="false"></asp:Label>
    <asp:Repeater ID="RepeaterTipoFormulario" runat="server" 
        DataSourceID="ObjectDataSourceTipoFormulario">
        <ItemTemplate>
            <asp:Label ID="lblTipoFormulario" Font-Bold="true" CssClass="destacados"  Text='<%# Eval("Descripcion") + " Total:" %>' runat="server" Font-Size="14px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater> 
    <asp:Label  CssClass="destacados" ID="LabelTotalEvaluaciones" runat="server" Text="1" Font-Size="12px"></asp:Label>
    <asp:ObjectDataSource ID="ObjectDataSourceTipoFormulario" runat="server" 
                    DeleteMethod="Delete" InsertMethod="Insert" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
                    TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.TipoFormularioTableAdapter" 
                    UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                <asp:Parameter Name="Original_Descripcion" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TipoFormularioID" Type="Int32" />
                <asp:Parameter Name="Descripcion" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="TipoFormularioID" 
                    QueryStringField="TipoFormularioID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descripcion" Type="String" />
                <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
                <asp:Parameter Name="Original_Descripcion" Type="String" />
            </UpdateParameters>
      </asp:ObjectDataSource>
     
    </span>
    <br />
    
    <span runat="server" ID="PanelCompetencia">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="LabelEstado" runat="server" Text="PMP Finalizadas:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Label  CssClass="destacados" ID="LabelCantidad" runat="server" Text="1" Font-Size="12px"></asp:Label>
   
                
    </span>
    <br />
    &nbsp;</td>
            <td align="right">
                <img alt="" src="../img/LogoJSC.jpg" style="width: 195px; height: 74px" /><br /></td>
        </tr>
    </table>
<div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones" onrowdatabound="GridViewEvaluados_RowDataBound">
        <Columns>
            <asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" Visible="false" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("periodoid") %>'/>
                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value= '<%# Bind("tipoformularioid") %>' />                    
                    <%--<asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>--%>
                    <asp:LinkButton ID="lnkBtnApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'  >LinkButton</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" SortExpression="TipoFormulario" />
            <asp:BoundField DataField="Area" HeaderText="Sector" SortExpression="Area" />
            <asp:BoundField DataField="Cargo" HeaderText="Cargo" SortExpression="Cargo" />
            <asp:BoundField DataField="Falta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Fecha" SortExpression="Falta" />
           
        </Columns>
    </asp:GridView>
    
</div>
<asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteByStatusTableAdapter">
    <SelectParameters>
        <asp:QueryStringParameter Name="PasoID" QueryStringField="PasoID" 
            Type="Int32" />
        <asp:QueryStringParameter Name="TipoFormularioID" 
            QueryStringField="TipoFormularioID" Type="Int32" />
        <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
            Type="Int32" />
    </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>    
 
 
</asp:Content>