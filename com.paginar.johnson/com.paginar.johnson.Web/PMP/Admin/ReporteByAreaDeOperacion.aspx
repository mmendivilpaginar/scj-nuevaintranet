﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PMP/Admin/MasterBO.master" AutoEventWireup="true" CodeBehind="ReporteByAreaDeOperacion.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteByAreaDeOperacion" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery.cluetip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios1", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }
    </script>

    <style type="text/css">
          a:hover
      {
         background: #ffffff;
         text-decoration: none;
      }
      .tooltip {  font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666666; text-decoration: none}
      a.tooltip:hover { color: #DA421A; text-decoration: none }
      /*BG color is a must for IE6*/a.tooltip span
      {
         display: none;
         padding: 2px 3px;
         margin-left: 8px;
         
      }
      a.tooltip:hover span
      {
         display: inline;
         position: absolute;
         background: #ffffff;
         border: 1px solid #cccccc;
         color: #6c6c6c;
      }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
        <tr>
            <td>
                

    <span runat="server" ID="PanelPeriodo">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label2" runat="server" Text="Período:" Font-Size="14px"></asp:Label>
     &nbsp;<asp:Repeater ID="RepeaterPeriodo" runat="server" 
        DataSourceID="ObjectDataSourcePeriodo">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralPeriodo" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    </span>
    <span runat="server" ID="PanelEstado">
    <asp:Label  Font-Bold="true" CssClass="destacados" ID="Label3" runat="server" Text="Estado:" Font-Size="14px"></asp:Label> &nbsp;<asp:Repeater ID="RepeaterPaso" runat="server" 
        DataSourceID="ObjectDataSourceEstado">
        <ItemTemplate>
            <asp:Label CssClass="destacados"  ID="LiteralEstado" Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:label>
        </ItemTemplate>
    </asp:Repeater>
    
    <br />
    </span>
    
    <span runat="server" ID="PanelAreaDeOperacion">
    <asp:Label Font-Bold="true" CssClass="destacados" ID="LabelAreaInteres" runat="server" Text="Área de Interés:" Font-Size="14px"></asp:Label> &nbsp;
    <asp:Repeater ID="RepeaterAreaInteres" runat="server" 
        DataSourceID="ObjectDataSourceAreaInteres">
        <ItemTemplate>
            <asp:Label ID="LiteralAreaInteres" CssClass="destacados"  Text='<%# Eval("Descripcion") %>' runat="server" Font-Size="12px"></asp:Label>
        </ItemTemplate>
    </asp:Repeater>
                
    </span>
    <br />
    &nbsp;</td>
            <td align="right">
                <img alt="" src="../img/LogoJSC.jpg" style="width: 195px; height: 74px" /><br /></td>
        </tr>
    </table>
<div class="AspNet-GridViewRepPMP">
    <asp:GridView ID="GridViewEvaluados" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSourceEvaluaciones" 
        onrowdatabound="GridViewEvaluados_RowDataBound" >
        <Columns>
            <asp:BoundField DataField="Indice" ReadOnly="True" SortExpression="Indice" Visible="false" />
            <asp:BoundField DataField="Legajo" HeaderText="Legajo" 
                SortExpression="Legajo" />
            <asp:BoundField DataField="Area" HeaderText="Area Actual" SortExpression="Area" />
            <asp:BoundField DataField="Descripcion" HeaderText="Area de Interes" SortExpression="Descripcion" Visible="false" />
            <asp:TemplateField HeaderText="Apellido y Nombre" 
                SortExpression="ApellidoNombre">
                <ItemTemplate>
                    <asp:HiddenField ID="HiddenFieldPeriodoID" runat="server" Value='<%# Bind("periodoid") %>'/>
                    <asp:HiddenField ID="HiddenFieldTipoFormularioID" runat="server" Value= '<%# Bind("tipoformularioid") %>' />
                    <asp:LinkButton ID="lnkBtnApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'  >LinkButton</asp:LinkButton>
                   <%-- <asp:Label ID="lblApellidoNombre" runat="server" Text='<%# Bind("ApellidoNombre") %>'></asp:Label>--%>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="TipoFormulario" HeaderText="Formulario" SortExpression="TipoFormulario" />
            <asp:BoundField DataField="Cargo" HeaderText="Cargo" SortExpression="Cargo" />
            <asp:BoundField DataField="Falta" DataFormatString="{0:dd/MM/yyyy}" 
                HeaderText="Fecha" SortExpression="Falta" />
            <asp:BoundField DataField="Calificacion" HeaderText="Resultado" 
                SortExpression="Calificacion" />
            <%--<asp:BoundField DataField="Observacion" HeaderText="Comentarios Evaluado" 
                SortExpression="Observacion" />--%>
                <asp:TemplateField HeaderText="Comentarios Evaluado">
                   <ItemTemplate>
                   <span class="tooltip" runat="server" id="LinkSinComentarioEvaluado" Visible='<%# (Eval("Observacion").ToString().Length ==0) ? true : false %>'><%# DefaultValComentarios(Eval("Observacion"))%></span>  
                   <span class="tooltip" runat="server" id="LinkComentarioEvaluado" Visible='<%# (Eval("Observacion").ToString().Length >0) ? true : false %>'>  
                       <a onclick="javascript:void(0)" id='tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>' href="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>"
                           rel="#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>" class="tooltip">
                           <%# DefaultValComentarios(Eval("Observacion"))%></a>
                           </span>
                       <div id='loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>'>
                           <span id="SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>" />
                           <%# Eval("Observacion")%>
                           </span>
                          
                       </div>
                       </a>

                       <script type="text/javascript">
                           $(document).ready(function () {
                               if (jQuery.trim($('#SpanEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>').html()) != '') {
                                   $('#tooltipEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>').cluetip({
                                       local: true,
                                       showTitle: false,
                                       stycky: true,
                                       mouseOutClose: false
                                   });
                               } else {
                                   $('#loadmeEvaluado<%# Eval("Legajo") %><%# Eval("PeriodoID") %><%# Eval("TipoFormularioID") %><%# Eval("AreaDeOperacionID") %>').hide();
                               }
                           })
                       </script>

                   </ItemTemplate>
                   <HeaderStyle CssClass="destacados" />
               </asp:TemplateField>
          
        </Columns>
    </asp:GridView>
</div>
    <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PeriodoTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
            <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
            <asp:Parameter Name="CargaDesde" Type="DateTime" />
            <asp:Parameter Name="CargaHasta" Type="DateTime" />
            <asp:Parameter Name="PeriodoID" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>    
    <asp:ObjectDataSource ID="ObjectDataSourceEvaluaciones" runat="server" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
        
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosReporteGeneralDSTableAdapters.Eval_GetReporteByAreaDeOperacionTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PasoID" QueryStringField="PasoID" Type="Int32" />
            <asp:QueryStringParameter Name="TipoFormularioID" QueryStringField="TipoFormularioID" Type="Int32" />
            <asp:QueryStringParameter Name="PeriodoID" QueryStringField="PeriodoID" Type="Int32" />
            <asp:QueryStringParameter Name="AreaOperacionID" QueryStringField="AreaDeOperacionID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSourceAreaInteres" runat="server" 
        OldValuesParameterFormatString="original_{0}" 
        SelectMethod="GetDataByAreaDeOperacionID" 
        
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.AreasDeOperacionesTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="AreaDeOperacionID" 
                QueryStringField="AreaDeOperacionID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

   <asp:ObjectDataSource ID="ObjectDataSourceEstado" runat="server" 
        DeleteMethod="Delete" InsertMethod="Insert" 
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByPasoID" 
        TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.PasosTableAdapter" 
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_PasoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Original_PasoID" Type="Int32" />
            <asp:Parameter Name="Original_Descripcion" Type="String" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="PasoID" QueryStringField="PasoID" 
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="PasoID" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
</asp:Content>


