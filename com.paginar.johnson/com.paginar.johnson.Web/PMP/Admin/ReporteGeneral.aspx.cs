﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.formularios.businesslogiclayer;
using System.Text.RegularExpressions;

public partial class PMP_Admin_ReporteGeneral : System.Web.UI.Page
{
    private string Formularios;
    private string Evaluado;
    private string Evaluador;
    private string Auditor;
    private string Estado;
    private string Resultado;
    private string ResultadoComp;
    private string Competencia;
    private string Sector;
    private string Area;
    private string Sort1="Legajo";
    private string Sort2="Titulo";

    private FormulariosReporteGeneralDS.Eval_GetReporteGeneralDataTable _dtReporteGeneral;
    private FormulariosReporteGeneralDS.Eval_GetReporteGeneralDataTable dtReporteGeneral
    {
        get
        {
            _dtReporteGeneral = (FormulariosReporteGeneralDS.Eval_GetReporteGeneralDataTable)Session["dtReporteGeneral"];
            if (_dtReporteGeneral == null)
            {
                _dtReporteGeneral = new FormulariosReporteGeneralDS.Eval_GetReporteGeneralDataTable();

                FachadaDA.Singleton.GetReporteGeneral.Fill(_dtReporteGeneral, int.Parse(ddlPeriodo.SelectedValue), Formularios, Estado,
                    Evaluado, Evaluador, Auditor, Sector, Area,
                    Resultado, ResultadoComp, Competencia);   

                
                Session["dtReporteGeneral"] = _dtReporteGeneral;
            }
            return _dtReporteGeneral;
        }
        set
        {
            Session["dtReporteGeneral"] = value;
            _dtReporteGeneral = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }

        
    }

    protected void ButtonBuscar_Click(object sender, EventArgs e)
    {
        ButtonImprimir.Visible = false;
        ButtonExportar.Visible = false;
        switch (ddlTipoReporte.SelectedValue.ToString())
            {
                case "1"://Reporte por competencias
                    LabelTituloPrint.Text = "Resultados de Búsqueda - Competencias";
                    MultiViewReportes.SetActiveView(vwReporteCompetencias);
                    ObjectDataSourceCompetencias.DataBind();
                    break;

                case "3"://Reporte por objetivos
                    LabelTituloPrint.Text = "Resultados de Búsqueda - Objetivos";
                    MultiViewReportes.SetActiveView(vwReporteAspectos);
                    ObjectDataSourceCompetencias.DataBind();
                    break;

                case "8"://Reporte por Resultado/competencias
                    LabelTituloPrint.Text = "Reporte por Resultado/Competencias";
                    MultiViewReportes.SetActiveView(vwReporteResultadosCompetencias);
                    GridViewReasResultados.DataBind();
                    if (ListBoxFormularios.SelectedValue == "0")
                        LabelRepoteResultadosCompetencias.Text = "Resultados de búsqueda - Resultado/competencias";
                    else
                        LabelRepoteResultadosCompetencias.Text = "Resultados de búsqueda - " + ListBoxFormularios.SelectedItem.ToString();
                    break;

                case "2"://PMP Operarios
                    LabelTituloPrint.Text = "PMP Planta";
                    Session["dtReporteGeneral"] = null;

                    Formularios = DameElementosListBox(ListBoxFormularios);
                    Evaluado = DameElementosListBox(ListBoxEvaluado);
                    Evaluador = DameElementosListBox(ListBoxEvaluador);
                    Auditor = DameElementosListBox(ListBoxAuditor);
                    Estado = DameElementosListBox(ListBoxEstado);
                    Resultado = DameElementosListBox(ListBoxResultado);
                    Sector = DameElementosListBox(ListBoxSector);
                    Area = DameElementosListBox(ListBoxArea);
                    Competencia = DameElementosListBox(ListBoxCompetencia);
                    ResultadoComp = DameElementosListBox(ListBoxResultadoComp); 
                    repeaterReporteGeneral.DataSource = DistinctRows(dtReporteGeneral, "TipoFormularioID");
                    repeaterReporteGeneral.DataBind();
                    MultiViewReportes.SetActiveView(vwReporteGeneral);

                    break;
                case "4"://Reporte por Calificaciones
                    LabelTituloPrint.Text = "PMP Planta - Calificaciones";
                    Evaluado = DameElementosListBox(ListBoxEvaluado);
                    Sector = DameElementosListBox(ListBoxSector);
                    Resultado = DameElementosListBox(ListBoxResultado);
                    FormulariosReporteGeneralDS.eval_GetReportePorCalificacionesDataTable dtReportePorCalificaciones = new FormulariosReporteGeneralDS.eval_GetReportePorCalificacionesDataTable();
                    FachadaDA.Singleton.GetReportePorCalificaciones.Fill(dtReportePorCalificaciones,int.Parse(ddlPeriodo.SelectedValue), int.Parse(ListBoxFormularios.SelectedValue), Evaluado, Sector, Resultado);

                    MultiViewReportes.SetActiveView(vwReporteCalificaciones);
                    GridViewReportCalificaciones.DataSource = dtReportePorCalificaciones;
                    GridViewReportCalificaciones.DataBind();
                    break;


                case "5"://Reporte por status
                    LabelTituloPrint.Text = "Status PMP Planta";
                    MultiViewReportes.SetActiveView(vwReporteStatus);
                    ObjectDataSourceReporteStatus.DataBind();
                    ObjectDataSourceReporteDetalleStatus.DataBind();
                    if(ListBoxFormularios.SelectedValue=="0")
                    {
                       
                       LabelStatusPMPPlanta.Text = "Resultados de Búsqueda - Status PMP Planta" ;
                    }
                    else
                        LabelStatusPMPPlanta.Text = "Resultados de Búsqueda - Status" + ListBoxFormularios.SelectedItem.ToString();

                    ButtonExportar.Visible = true;
                    ButtonImprimir.Visible = true;
                    ButtonImprimir.OnClientClick = "Imprimir()";

                    break;

                case "6"://Reporte por PMP Ocultas
                    LabelTituloPrint.Text = "Reporte por PMP Ocultas";
                    MultiViewReportes.SetActiveView(vwReporteOcultas);
                    ObjectDataSourcePMPOcultas.DataBind();                   
                    break;

                case "7"://Reporte por Áreas de Interés
                    LabelTituloPrint.Text = "Resultados de Búsqueda - Areas de interes";
                    MultiViewReportes.SetActiveView(vwReporteAreasInteres);
                    ObjectDataSourceAreasDeInteres.DataBind();
                    break;

                case "9"://Reporte por comentarios
                    LabelTituloPrint.Text = "Resultados de Búsqueda - Comentarios";
                    Evaluado = DameElementosListBox(ListBoxEvaluado);
                    FormulariosReporteGeneralDS.eval_GetReportePorComentariosDataTable dtReportePorComentarios = new FormulariosReporteGeneralDS.eval_GetReportePorComentariosDataTable();
                    FachadaDA.Singleton.GetReportePorComentarios.Fill(dtReportePorComentarios, int.Parse(ListBoxFormularios.SelectedValue), int.Parse(ddlPeriodo.SelectedValue), Evaluado);

                    GridViewReportePorComentarios.DataSource = dtReportePorComentarios;
                    GridViewReportePorComentarios.DataBind();
                    MultiViewReportes.SetActiveView(vwReportePorComentarios);
                    
                    break;

            }

        
        
        DeshabilitarItems();
       // ddlTipoReporte_SelectedIndexChanged(null, null);
    }

    private string DameElementosListBox(ListBox lst)
    {
        int[] lstbItems = lst.GetSelectedIndices();
        String Elementos = "";
       
        foreach (int i in lstbItems)
        {
            if (lst.Items[i].Value.ToString() == "0")
            {
                Elementos = lst.Items[i].Value.ToString();
                break;
            }
            else
            {
                Elementos = Elementos + "," + lst.Items[i].Value.ToString();
            }
        }
        return Elementos.Trim(',');

    }

    protected DataTable DistinctRows(DataTable dt, string keyfield)
        {
            DataTable newTable = dt.Clone();
            if (newTable.Columns["rowcount"] == null)
                newTable.Columns.Add("rowcount", typeof(int));
            int keyval = 0;
            DataView dv = dt.DefaultView;
            dv.Sort = keyfield;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr1 in dt.Rows)
                {
                    bool existe = false;
                    foreach (DataRow dr2 in newTable.Rows)
                    {
                        if (dr1[keyfield].ToString() == dr2[keyfield].ToString())
                        {
                            existe = true;
                            dr2["rowcount"] = ((int)dr2["rowcount"]) + 1;
                        }
                    }

                    if (!existe)
                    {
                        newTable.ImportRow(dr1);
                        newTable.Rows[newTable.Rows.Count - 1]["rowcount"] = 1;
                    }
                }
            }
            else
                newTable = dt.Clone();
            return newTable;
        }

   protected string CrearLink(object Titulo, int PeriodoID, int Pasoid, int TipoFormularioID, int Calificacion, object Puntaje, int Seccion)
    {
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        decimal PuntajeAux = decimal.Parse(Puntaje.ToString());
        //switch(Puntaje.GetType().Name)

        Label LblCantidad = new Label();
        LblCantidad.ID = "LblCantidad" + Puntaje.GetType().Name + Titulo.ToString() + Pasoid.ToString() + Calificacion.ToString();
        HyperLink LnkCantidad = new HyperLink();
        LnkCantidad.ID = "LnkCantidad" + Puntaje.GetType().Name + Titulo.ToString() + Pasoid.ToString() + Calificacion.ToString();
        if (PuntajeAux > 0)
        {
            LnkCantidad.NavigateUrl = string.Format("javascript:popup('ReporteByItemEvaluacion.aspx?Titulo={0}&Pasoid={1}&TipoFormularioID={2}&Calificacion={3}&PeriodoID={4}&SeccionID={5}',500,300)", Titulo, Pasoid, TipoFormularioID, Calificacion, PeriodoID, Seccion);
            LnkCantidad.Text = Puntaje.ToString();
            LnkCantidad.CssClass = "link";

            LnkCantidad.RenderControl(htmlWrite);

            //return string.Format("popup('ReporteSeguimientoByUsuario.aspx?LegajoEvaluador={0}&Pasoid={1}&TipoFormularioID={2}&Calificacion={3}&PeriodoID={4}',600,600)", LegajoEvaluador, Pasoid, TipoFormularioID, Calificacion, PeriodoID);
        }
        else
        {
            LblCantidad.Text = Puntaje.ToString();
            LblCantidad.RenderControl(htmlWrite);

        }
        return stringWrite.ToString();
    }

   protected string CrearLinkAI(int AreaDeOperacionID, int PeriodoID, int Pasoid, int TipoFormularioID,  object Puntaje)
   {
       System.IO.StringWriter stringWrite = new System.IO.StringWriter();

       System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

       decimal PuntajeAux = decimal.Parse(Puntaje.ToString());
       //switch(Puntaje.GetType().Name)

       Label LblCantidad = new Label();
       LblCantidad.ID = "LblCantidad" + Puntaje.GetType().Name + AreaDeOperacionID.ToString() + Pasoid.ToString();
       HyperLink LnkCantidad = new HyperLink();
       LnkCantidad.ID = "LnkCantidad" + Puntaje.GetType().Name + AreaDeOperacionID.ToString() + Pasoid.ToString();
       if (PuntajeAux > 0)
       {
           LnkCantidad.NavigateUrl = string.Format("javascript:popup('ReporteByAreaDeOperacion.aspx?AreaDeOperacionID={0}&Pasoid={1}&TipoFormularioID={2}&PeriodoID={3}',650,400)", AreaDeOperacionID, Pasoid, TipoFormularioID, PeriodoID);
           LnkCantidad.Text = Puntaje.ToString();
           LnkCantidad.CssClass = "link";

           LnkCantidad.RenderControl(htmlWrite);

           //return string.Format("popup('ReporteSeguimientoByUsuario.aspx?LegajoEvaluador={0}&Pasoid={1}&TipoFormularioID={2}&Calificacion={3}&PeriodoID={4}',600,600)", LegajoEvaluador, Pasoid, TipoFormularioID, Calificacion, PeriodoID);
       }
       else
       {
           LblCantidad.Text = Puntaje.ToString();
           LblCantidad.RenderControl(htmlWrite);

       }
       return stringWrite.ToString();
      
   }
   protected void ObjectDataSourceCompetencias_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        int countRows;
        countRows = e.AffectedRows;
    }
   protected void RepeaterEvaluaciones_PreRender(object sender, EventArgs e)
    {
        Repeater rpt = (Repeater)sender;
        if (rpt.Items.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }


    }

    protected void ddlTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBoxFormularios.SelectedIndex = 0;
        ListItem item1;
        ButtonImprimir.Visible = false;
        ButtonExportar.Visible = false;

        switch (ddlTipoReporte.SelectedValue.ToString())
        {
            case "1": // competencias

 
                ListBoxEvaluado.Attributes.Add("disabled", "");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled","");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;
                //ListBoxResultado.SelectionMode = ListSelectionMode.Single;
                //ListBoxArea.SelectionMode = ListSelectionMode.Single;    

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                //item1 = ListBoxArea.Items.FindByValue("0");
                //item1.Attributes.Add("disabled", "");

                //item1 = ListBoxResultado.Items.FindByValue("0");
                //item1.Attributes.Add("disabled", "");


                ListBoxFormularios.SelectedIndex = 0;
                //ListBoxArea.SelectedIndex = 1;
                //ListBoxResultado.SelectedIndex = 1;                
                break;

            case "3"://aspectos

 
                ListBoxEvaluado.Attributes.Add("disabled", "");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled","");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;
                //ListBoxResultado.SelectionMode = ListSelectionMode.Single;
                //ListBoxArea.SelectionMode = ListSelectionMode.Single;    

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                //item1 = ListBoxArea.Items.FindByValue("0");
                //item1.Attributes.Add("disabled", "");

                //item1 = ListBoxResultado.Items.FindByValue("0");
                //item1.Attributes.Add("disabled", "");


                ListBoxFormularios.SelectedIndex = 0;
                //ListBoxArea.SelectedIndex = 1;
                //ListBoxResultado.SelectedIndex = 1;   
                break;

            case "8"://Reporte por Resultado/competencias



                ListBoxEvaluado.Attributes.Add("disabled", "");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled","");
                ListBoxResultado.Attributes.Remove("disabled");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Remove("disabled");

                ListBoxArea.SelectionMode = ListSelectionMode.Single;                
                ListBoxResultado.SelectionMode = ListSelectionMode.Single;
                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;


                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                ListBoxFormularios.SelectedIndex = 0;
                ListBoxArea.SelectedIndex = 0;
                ListBoxResultado.SelectedIndex = 0;

                break;

            case "2"://general


                ListBoxEvaluado.Attributes.Remove("disabled");
                ListBoxEvaluador.Attributes.Remove("disabled");
                ListBoxAuditor.Attributes.Remove("disabled");
                ListBoxSector.Attributes.Remove("disabled");
                ListBoxEstado.Attributes.Remove("disabled");
                ListBoxResultadoComp.Attributes.Remove("disabled");
                ListBoxResultado.Attributes.Remove("disabled");
                ListBoxCompetencia.Attributes.Remove("disabled");
                ListBoxArea.Attributes.Remove("disabled");

                ListBoxArea.SelectionMode = ListSelectionMode.Multiple;
                ListBoxResultado.SelectionMode = ListSelectionMode.Multiple;
                ListBoxFormularios.SelectionMode = ListSelectionMode.Multiple;
                
                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                ListBoxFormularios.SelectedIndex = 0;
                ListBoxArea.SelectedIndex = 0;
                ListBoxResultado.SelectedIndex = 0;    
  
                ListBoxEvaluado.SelectedIndex = 0;
                ListBoxEvaluador.SelectedIndex = 0;
                ListBoxAuditor.SelectedIndex = 0;
                ListBoxSector.SelectedIndex = 0;
                ListBoxEstado.SelectedIndex = 0;
                ListBoxResultadoComp.SelectedIndex = 0;
                ListBoxResultado.SelectedIndex = 0;
                ListBoxCompetencia.SelectedIndex = 0;
                ListBoxArea.SelectedIndex = 0;
                break;

            case "4"://Calificaiones
 

                ListBoxEvaluado.Attributes.Remove("disabled");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Remove("disabled");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled","");
                ListBoxResultado.Attributes.Remove("disabled");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");


                ListBoxArea.SelectionMode = ListSelectionMode.Multiple;
                ListBoxResultado.SelectionMode = ListSelectionMode.Multiple;
                ListBoxFormularios.SelectionMode = ListSelectionMode.Multiple;

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                ListBoxFormularios.SelectedIndex = 0;
                ListBoxArea.SelectedIndex = 0;
                ListBoxResultado.SelectedIndex = 0;
                ListBoxEvaluado.SelectedIndex = 0;
                ListBoxSector.SelectedIndex = 0;
                break;

            case "5"://Status


                ListBoxEvaluado.Attributes.Add("disabled", "");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled", "");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;
                

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

               

                ListBoxFormularios.SelectedIndex = 0;
                
                break;

            case "6"://ocultas


                ListBoxEvaluado.Attributes.Remove("disabled");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled", "");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;
               

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
                ListBoxFormularios.SelectedIndex = 0;
                ListBoxEvaluado.SelectedIndex = 0;
                
                break;

            case "7": // Areas de interes


                ListBoxEvaluado.Attributes.Add("disabled", "");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled", "");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;
                 

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");



                ListBoxFormularios.SelectedIndex = 0;
                               
                break;

            case "9"://Comentarios


                ListBoxEvaluado.Attributes.Remove("disabled");
                ListBoxEvaluador.Attributes.Add("disabled", "");
                ListBoxAuditor.Attributes.Add("disabled", "");
                ListBoxSector.Attributes.Add("disabled", "");
                ListBoxEstado.Attributes.Add("disabled", "");
                ListBoxResultadoComp.Attributes.Add("disabled", "");
                ListBoxCompetencia.Attributes.Add("disabled", "");
                ListBoxResultado.Attributes.Add("disabled", "");
                ListBoxArea.Attributes.Add("disabled", "");

                ListBoxFormularios.SelectionMode = ListSelectionMode.Single;


                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
                ListBoxFormularios.SelectedIndex = 0;
                ListBoxEvaluado.SelectedIndex = 0;

                break;


        }

 
      
        MultiViewReportes.SetActiveView(ViewVacio);
    }

    private void DeshabilitarItems()
    {
    
        ListItem item1;

        switch (ddlTipoReporte.SelectedValue.ToString())
        {
            case "1": // competencias
                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
                            
                break;

            case "3"://aspectos 
              
                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
              
                break;

            case "8"://Reporte por Resultado/competencias


                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
               

                break;

            case "2"://general
                
                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
                
                
                break;

            case "4"://Calificaiones               

               // item1 = ListBoxFormularios.Items.FindByValue("0");
               // item1.Attributes.Add("disabled", "");

                item1 = ListBoxArea.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                item1 = ListBoxResultado.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");

                break;

            case "5"://Status               

                item1 = ListBoxFormularios.Items.FindByValue("0");
                item1.Attributes.Remove("disabled");
                break;

            case "6"://ocultas               

               // item1 = ListBoxFormularios.Items.FindByValue("0");
               // item1.Attributes.Add("disabled", "");
               
                
                break;

            case "7": // Areas de interes

                item1 = ListBoxFormularios.Items.FindByValue("0");
               // item1.Attributes.Add("disabled", "");             
                               
                break;

            case "9"://Comentarios

                item1 = ListBoxFormularios.Items.FindByValue("0");
              //  item1.Attributes.Add("disabled", "");              

                break;


        }
    }
    protected void GridViewReasResultados_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int AreaID = int.Parse(DataBinder.Eval(e.Row.DataItem, "AreaID").ToString());
            string Calificacion = DataBinder.Eval(e.Row.DataItem, "Calificacion").ToString();
            int PeriodoID = int.Parse(DataBinder.Eval(e.Row.DataItem, "periodoid").ToString());
            int TipoFormularioID = int.Parse(DataBinder.Eval(e.Row.DataItem, "tipoformularioid").ToString());
            LinkButton lnkBtnCalificacion = (LinkButton)e.Row.FindControl("lnkBtnCalificacion");
            lnkBtnCalificacion.Attributes.Add("onclick", string.Format("javascript:popup('ReporteByResultadoArea.aspx?AreaID={0}&Calificacion={1}&TipoFormularioID={2}&PeriodoID={3}',500,300)", AreaID, Calificacion, TipoFormularioID, PeriodoID));

        }
    }

    protected void repeaterReporteGeneral_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

           
            HiddenField hdFilterValue = (HiddenField)e.Item.FindControl("hdFilterValue");
            
            String filter = String.Format("TipoFormularioID = {0}", hdFilterValue.Value);
            DataTable dtNivel_0 = SelectDataTable(dtReporteGeneral, filter, Sort1);

            Repeater rpNivel_0 = (Repeater)e.Item.FindControl("rpNivel_0");
            rpNivel_0.DataSource = DistinctRows(dtNivel_0, "Legajo");
            rpNivel_0.DataBind();

 
  

        }
    }

    public DataTable SelectDataTable(DataTable dt, string filter, string sort)
    {
        DataRow[] rows = null;
        DataTable dtNew = default(DataTable);
        // copy table structure
        dtNew = dt.Clone();
        // sort and filter data
        rows = dt.Select(filter, sort,DataViewRowState.CurrentRows);
        // fill dtNew with selected rows
        foreach (DataRow dr in rows)
        {
            dtNew.ImportRow(dr);
        }
        // return filtered dt
        return dtNew;
    }

    protected void rpNivel_0_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
           
            HiddenField hdFilterValue = (HiddenField)e.Item.FindControl("hdFilterValue");
            HiddenField HiddenFieldRowCount = (HiddenField)e.Item.FindControl("HiddenFieldRowCount");
            String filter = "";
            string[] filterValue;
            if (hdFilterValue != null)
            {
                filterValue = hdFilterValue.Value.Split(',');
                filter = String.Format("TipoFormularioID={0} and Legajo= {1} and PeriodoID={2}", filterValue[0], filterValue[1], filterValue[2]);
            }

            Repeater rpNivel_1 = (Repeater)e.Item.FindControl("rpNivel_1");
           

            if (rpNivel_1 != null)
            {
                DataTable dtNivel_1 = SelectDataTable(dtReporteGeneral, filter, Sort2);
                if (dtNivel_1.Columns["rowcount"] == null)
                    dtNivel_1.Columns.Add("rowcount", typeof(int));
                foreach (DataRow dr2 in dtNivel_1.Rows)
                { dr2["rowcount"] = int.Parse(HiddenFieldRowCount.Value.ToString()); }
                rpNivel_1.DataSource = dtNivel_1;
                rpNivel_1.DataBind();
                               

            }         



        }
    }

    protected void rpNivel_1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            ImageButton ImageCommand = e.Item.FindControl("ImageCommand") as ImageButton;
            HiddenField HiddenFieldLegajo = e.Item.FindControl("HiddenFieldLegajo") as HiddenField;
            HiddenField HiddenFieldPeriodoID = e.Item.FindControl("HiddenFieldPeriodoID") as HiddenField;
            HiddenField HiddenFieldTipoFormularioID = e.Item.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
            HiddenField HiddenFieldEstadoID = e.Item.FindControl("HiddenFieldEstadoID") as HiddenField;
            HiddenField HiddenFieldLegajoEvaluador = e.Item.FindControl("HiddenFieldLegajoEvaluador") as HiddenField;

            string UrlForm = string.Empty;
            FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();

            FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
            if (RelPasosTipoFormulariosDt.Rows.Count > 0)
            {
                FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                UrlForm = RelPasosTipoFormulariosRow.FormAspx;
            }

            FormulariosController f = new FormulariosController();
            bool carga = false;
            carga = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));

            if( int.Parse(HiddenFieldEstadoID.Value)==4 || int.Parse(HiddenFieldEstadoID.Value)==5 || !carga)
                ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
            else
                ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "../" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}&LegajoEvaluador={3}&SoloLectura=1',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, HiddenFieldLegajoEvaluador.Value));
        }
    }

    protected void GridViewReportCalificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
            HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
            HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
            HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
            HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
            HiddenField HiddenFieldLegajoEvaluador = e.Row.FindControl("HiddenFieldLegajoEvaluador") as HiddenField;

            string UrlForm = string.Empty;
            FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();

            FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
            if (RelPasosTipoFormulariosDt.Rows.Count > 0)
            {
                FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                UrlForm = RelPasosTipoFormulariosRow.FormAspx;
            }

            FormulariosController f = new FormulariosController();
            bool carga = false;
            carga = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));

            if (int.Parse(HiddenFieldEstadoID.Value) == 4 || int.Parse(HiddenFieldEstadoID.Value) == 5 || !carga)
                ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
            else
                ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "../" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}&LegajoEvaluador={3}&SoloLectura=1',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, HiddenFieldLegajoEvaluador.Value));
           // ImageCommand.Attributes.Add("onclick", string.Format("popupPmpO('" + "/PMP/" + "Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));

        }
    }

    protected void repeaterReporteGeneral_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        
        
        if (e.CommandName == "Sort")
        {

            Sort1 = e.CommandArgument.ToString();            
            repeaterReporteGeneral.DataSource = DistinctRows(dtReporteGeneral, "TipoFormularioID");
            repeaterReporteGeneral.DataBind();          
           
        }
        if (e.CommandName == "Sort2")
        {

            Sort2 = e.CommandArgument.ToString();
            repeaterReporteGeneral.DataSource = DistinctRows(dtReporteGeneral, "TipoFormularioID");
            repeaterReporteGeneral.DataBind();

        }

 

    }


    protected bool ValidaPeriodo(int periodoid)
    {
        FormulariosController f = new FormulariosController();
        ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
        return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
    }

    protected void repeaterReporteGeneral_PreRender(object sender, EventArgs e)
    {
        Repeater rpt = (Repeater)sender;
        if (rpt.Items.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);

        }
        else
        {
            ButtonImprimir.Visible = true;
            ButtonExportar.Visible = true;
            ButtonImprimir.OnClientClick = "ImprimirReporteGeneral()";

        }
    }

    protected void GridViewReasResultados_PreRender(object sender, EventArgs e)
    {
        GridView grdv = (GridView)sender;
        if (grdv.Rows.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }

        else
        {
            ButtonImprimir.Visible = true;
            ButtonExportar.Visible = true;
            ButtonImprimir.OnClientClick = "Imprimir()";
        }

    }

    protected void GridViewReportCalificaciones_PreRender(object sender, EventArgs e)
    {
        GridView grdv = (GridView)sender;
        if (grdv.Rows.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }
        else
        {
            ButtonImprimir.Visible = true;
            ButtonExportar.Visible = true;
            ButtonImprimir.OnClientClick = "ImprimirCalificaciones()";

        }


    }

    protected void Exportar(Repeater rpt, string filename, string titulo)
    {
        if (rpt.Items.Count > 0)
        {
            Response.Clear();
            Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name> Reportes PMP</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            Response.Write(generarExportarTodo(rpt, titulo));
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }
    }

   protected void Exportar(GridView gv, string filename, string titulo)
    { 
        Response.Clear();
        Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
        Response.Write("<head>");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name> Reportes PMP</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        Response.Write("</head>");
        Response.Write("<body>");       
        Response.AddHeader("content-disposition", "attachment;filename="+ filename + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";

       Response.Write(generarExportarTodo(gv, titulo));

       Response.Write("</body>");
       Response.Write("</html>");
       Response.End();
    }

    protected void Exportar(GridView gv1, GridView gv2, string filename, string titulo)
    {
        //if (gv.Rows.Count > 0)
        //{
        Response.Clear();
        Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
        Response.Write("<head>");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name> Reportes PMP</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        Response.Write("</head>");
        Response.Write("<body>");
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
        Response.Charset = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        Response.Write(generarExportarTodo(gv1,gv2, titulo));
        
        Response.Write("</body>");
        Response.Write("</html>");
        Response.End();
        //}
    }

    private string generarExportarTodo(GridView gv, string titulo)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        GridView gvaux = new GridView();

        foreach (GridViewRow gvr in gv.Rows)
        {
            HtmlControl LinkComentarioJefeTurno = (HtmlControl)gvr.FindControl("LinkComentarioJefeTurno");
            if (LinkComentarioJefeTurno != null)
                LinkComentarioJefeTurno.Visible = false;

            HtmlControl LinkComentarioEvaluado = (HtmlControl)gvr.FindControl("LinkComentarioEvaluado");
            if (LinkComentarioEvaluado != null)
                LinkComentarioEvaluado.Visible = false;   
        
            
        }

       
        gvaux = gv;

        GridViewRow grh = gvaux.HeaderRow;
       
        gvaux.Columns[0].Visible = false;

        



        Page page = new Page();
        HtmlForm form = new HtmlForm();

        gvaux.EnableViewState = false;
        gvaux.AllowPaging = false;
        gvaux.GridLines = GridLines.Both;
        gvaux.BorderStyle = BorderStyle.None;
        gvaux.BorderColor = System.Drawing.Color.Black;
        gvaux.BorderWidth = 1; 

        page.EnableEventValidation = false;

        page.DesignerInitialize();

        page.Controls.Add(form);


        form.Controls.Add(gvaux);

        

        page.RenderControl(htw);

        System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
        string HTML;
       // HTML = regEx.Replace("<html><body>" + sb.ToString() + "</body></html>", "");
        HTML = regEx.Replace("<html><body>" + Regex.Replace(sb.ToString(), "<a[^>]+>([^<]+)</a>", "$1")  + "</body></html>", "");
       
        return Encabezado(titulo) + HTML;

       // return Encabezado(titulo) + sb.ToString();
    }



    private string generarExportarTodo(Repeater rpt, string titulo)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        ImageButton ImageButton1;
        HtmlTableCell tdVer;
        // HtmlTableCell tdEliminarRol;

        LinkButton LinkButtonFormulario = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonFormulario");
        Label LabelFormulario = (Label)rpt.Controls[0].Controls[0].FindControl("LabelFormulario");
        LinkButtonFormulario.Visible = false;
        LabelFormulario.Visible = true;


        LinkButton LinkButtonLegajo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonLegajo");
        Label LabelLegajo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelLegajo");
        LinkButtonLegajo.Visible = false;
        LabelLegajo.Visible = true;

        LinkButton LinkButtonApellidoNombre = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonApellidoNombre");
        Label LabelApellidoNombre = (Label)rpt.Controls[0].Controls[0].FindControl("LabelApellidoNombre");
        LinkButtonApellidoNombre.Visible = false;
        LabelApellidoNombre.Visible = true;

        LinkButton LinkButtonEvaluador = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonEvaluador");
        Label LabelEvaluador = (Label)rpt.Controls[0].Controls[0].FindControl("LabelEvaluador");
        LinkButtonEvaluador.Visible = false;
        LabelEvaluador.Visible = true;

        LinkButton LinkButtonAuditor = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonAuditor");
        Label LabelAuditor = (Label)rpt.Controls[0].Controls[0].FindControl("LabelAuditor");
        LinkButtonAuditor.Visible = false;
        LabelAuditor.Visible = true;

        LinkButton LinkButtonEstado = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonEstado");
        Label LabelEstado = (Label)rpt.Controls[0].Controls[0].FindControl("LabelEstado");
        LinkButtonEstado.Visible = false;
        LabelEstado.Visible = true;

        LinkButton LinkButtonCalificacion = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCalificacion");
        Label LabelCalificacion = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCalificacion");
        LinkButtonCalificacion.Visible = false;
        LabelCalificacion.Visible = true;

        LinkButton LinkButtonSector = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonSector");
        Label LabelSector = (Label)rpt.Controls[0].Controls[0].FindControl("LabelSector");
        LinkButtonSector.Visible = false;
        LabelSector.Visible = true;

        LinkButton LinkButtonCargo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCargo");
        Label LabelCargo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCargo");
        LinkButtonCargo.Visible = false;
        LabelCargo.Visible = true;

        LinkButton LinkButtonCompetencia = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCompetencia");
        Label LabelCompetencia = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCompetencia");
        LinkButtonCompetencia.Visible = false;
        LabelCompetencia.Visible = true;

        LinkButton LinkButtonCalificacionComp = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCalificacionComp");
        Label LabelCalificacionComp = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCalificacionComp");
        LinkButtonCalificacionComp.Visible = false;
        LabelCalificacionComp.Visible = true;


         Label LabelVer = (Label)rpt.Controls[0].Controls[0].FindControl("LabelVer");
         LabelVer.Visible = false;

        foreach (RepeaterItem item in rpt.Items)
        {
            Repeater rpNivel_0 = (Repeater)item.FindControl("rpNivel_0");
            // rpNivel_0.Items[0].Visible = false;
            foreach (RepeaterItem item_0 in rpNivel_0.Items)
            {

                Repeater rpNivel_1 = (Repeater)item_0.FindControl("rpNivel_1");
                foreach (RepeaterItem item_1 in rpNivel_1.Items)
                {
                    ImageButton1 = (ImageButton)item_1.FindControl("ImageCommand");
                    if (ImageButton1 != null)
                        ImageButton1.Visible = false;
                }
            }
        }



        Page page = new Page();
        HtmlForm form = new HtmlForm();

        // rpaux.EnableViewState = false;        

        page.EnableEventValidation = false;

        page.DesignerInitialize();

        page.Controls.Add(form);
        form.Controls.Add(rpt);
        page.RenderControl(htw);

        System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
        string HTML;
        HTML = regEx.Replace("<html><body>" + sb.ToString() + "</body></html>", "");

        return Encabezado(titulo) + HTML;
    }

    private string generarExportarTodo(GridView gv1, GridView gv2, string titulo)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        Label LabelTitulo2= new Label();
        LabelTitulo2.Text = "<br/><br/>  Detalle de Status<br/><br/> ";

        GridView gvaux1 = new GridView();
        gvaux1 = gv1;

        GridView gvaux2 = new GridView();
        gvaux2 = gv2;

        //GridViewRow grh = gvaux.HeaderRow;       
        //gvaux.Columns[0].Visible = false;
        Page page = new Page();
        HtmlForm form = new HtmlForm();

        gvaux1.EnableViewState = false;
        gvaux1.AllowPaging = false;

        foreach (GridViewRow dt in gvaux2.Rows)
        {
            Label LabelCantidad = (Label)dt.FindControl("LabelCantidad");
            LinkButton LinkButtonCatidad = (LinkButton)dt.FindControl("LinkButtonCatidad");
            LabelCantidad.Visible = true;
            LinkButtonCatidad.Visible = false;

            Label LabelPorcentaje = (Label)dt.FindControl("LabelPorcentaje");
            LinkButton LinkButtonPorcentaje = (LinkButton)dt.FindControl("LinkButtonPorcentaje");
            LabelPorcentaje.Visible = true;
            LinkButtonPorcentaje.Visible = false;


        }
        gvaux2.EnableViewState = false;
        gvaux2.AllowPaging = false;

        page.EnableEventValidation = false;

        page.DesignerInitialize();

        page.Controls.Add(form);
        form.Controls.Add(gvaux1);
        form.Controls.Add(LabelTitulo2);
        form.Controls.Add(gvaux2);
        page.RenderControl(htw);

        System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
        string HTML;
        HTML = regEx.Replace("<html><body>" + sb.ToString() + "</body></html>", "");

        return Encabezado(titulo) + HTML;
       // return Encabezado(titulo) + sb.ToString();
    }
    public string Encabezado(string titulo)
    {
        DateTime dt1 = DateTime.Now;
        string Header = "";

        Header = Header + "Modulo de Reportes - SCJohnson";
        Header = Header + "<BR><BR>";
        Header = Header + "<strong>";
        Header = Header + "Generado el ";
        Header = Header + String.Format("{0: dd/MM/yy - HH:mm }", dt1);
        // Header = Header + " por " + ses.Iniciales;
        Header = Header + "</strong>";
        Header = Header + "<BR><BR>";
        Header = Header + "<strong>";
        Header = Header + titulo;
        Header = Header + "<BR>";

        return Header;
    }

    protected void ButtonExportarExcel_Click(object sender, EventArgs e)
    {
        switch (MultiViewReportes.ActiveViewIndex)
        {
            case 0: // reporte areas resultados
                Exportar(GridViewReasResultados, "Reporte Resultados Competencias", "Reporte Resultados Competencias");
                break;
            case 3://reporte general
                Exportar(repeaterReporteGeneral, "Reporte General", "Reporte General");
                break;
            case 4://reporte calificaciones
                Exportar(GridViewReportCalificaciones, "Reporte por calificaciones", "Reporte por calificaciones");
                break;
            case 5://reporte por status
                Exportar(GridViewReporteStatus, GridViewReporteDetalleStatus, "Reporte por Status", "Reporte por Status");
                break;
            case 6: // reporte pmp ocultas
                Exportar(GridViewFormularios, "Reporte PMP ocultas", "Reporte PMP ocultas");
                break;
        }
        
    }

  
    protected void GridViewReporteDetalleStatus_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton LinkButtonCantidad = (LinkButton)e.Row.FindControl("LinkButtonCatidad");
            LinkButton LinkButtonPorcentaje = (LinkButton)e.Row.FindControl("LinkButtonPorcentaje");
            HiddenField HiddenFieldPasoID = (HiddenField)e.Row.FindControl("HiddenFieldPasoID");
            HiddenField HiddenFieldTotalEvaluaciones = (HiddenField)e.Row.FindControl("HiddenFieldTotalEvaluaciones");
            int TipoFormularioID = int.Parse(ListBoxFormularios.SelectedValue.ToString());
            int PeriodoID = int.Parse(ddlPeriodo.SelectedValue.ToString());
            LinkButtonCantidad.Attributes.Add("onclick", string.Format("javascript:popup('ReporteByStatus.aspx?Pasoid={0}&TipoFormularioID={1}&PeriodoID={2}&Cantidad={3}&TotalEvaluaciones={4}',500,300)", HiddenFieldPasoID.Value, TipoFormularioID, PeriodoID, LinkButtonCantidad.Text, HiddenFieldTotalEvaluaciones.Value));
            LinkButtonPorcentaje.Attributes.Add("onclick", string.Format("javascript:popup('ReporteByStatus.aspx?Pasoid={0}&TipoFormularioID={1}&PeriodoID={2}&Cantidad={3}&TotalEvaluaciones={4}',500,300)", HiddenFieldPasoID.Value, TipoFormularioID, PeriodoID, LinkButtonCantidad.Text, HiddenFieldTotalEvaluaciones.Value));
        }
    }

    protected string DefaultValComentarios(object val)
    {
 
        if (((val == System.DBNull.Value) || (val == null)))
            return "-";
        if (val == string.Empty)
            return "-";
        else
        {
            int Maximalongitud = 10;
            string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

            return Texto;
        }

    }

    protected void GridViewFormularios_PreRender(object sender, EventArgs e)
    {
        GridView grdv = (GridView)sender;
        if (grdv.Rows.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }
        else
        {
            ButtonImprimir.Visible = true;
            ButtonExportar.Visible = true;
            ButtonImprimir.OnClientClick = "ImprimirPMPOcultas()";

        }
    }

    protected void ObjectDataSourcePMPOcultas_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

        e.InputParameters["Evaluado"] = DameElementosListBox(ListBoxEvaluado);

    }

    protected void GridViewReportePorComentarios_PreRender(object sender, EventArgs e)
    {

        GridView grdv = (GridView)sender;
        if (grdv.Rows.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }

    }


   protected void GridViewReporteStatus_PreRender(object sender, EventArgs e)
    {
        GridView grdv = (GridView)sender;
        if (grdv.Rows.Count == 0)
        {
            MultiViewReportes.SetActiveView(ViewSinResultados);
        }
        else
        {
            ButtonImprimir.Visible = true;
            ButtonExportar.Visible = true;
            ButtonImprimir.OnClientClick = "ImprimirCalificaciones()";

        }

    }

   protected void ListBoxEvaluado_DataBound(object sender, EventArgs e)
   {
       
        if (ListBoxEvaluado.Items.Count > 0)
        {
            ListBoxEvaluado.Items.Insert(0, "Todos");
            ListBoxEvaluado.Items[0].Value = "0";
            ListBoxEvaluado.SelectedIndex = 0;
        }
    
   }

   protected void ListBoxAuditor_DataBound(object sender, EventArgs e)
   {

       if (ListBoxAuditor.Items.Count > 0)
        {
            ListBoxAuditor.Items.Insert(0, "Todos");
            ListBoxAuditor.Items[0].Value = "0";
            ListBoxAuditor.SelectedIndex = 0;
        }
   }

   protected void ListBoxEvaluador_DataBound(object sender, EventArgs e)
   {

       if (ListBoxEvaluador.Items.Count > 0)
        {
            ListBoxEvaluador.Items.Insert(0, "Todos");
            ListBoxEvaluador.Items[0].Value = "0";
            ListBoxEvaluador.SelectedIndex = 0;
        }
   }

   protected void ListBoxArea_DataBound(object sender, EventArgs e)
   {

       if (ListBoxArea.Items.Count > 0)
        {
            ListBoxArea.Items.Insert(0, "Todos");
            ListBoxArea.Items[0].Value = "0";
            ListBoxArea.SelectedIndex = 0;
        }
   }

   protected void ListBoxEstado_DataBound(object sender, EventArgs e)
   {

       if (ListBoxEstado.Items.Count > 0)
        {
            ListBoxEstado.Items.Insert(0, "Todos");
            ListBoxEstado.Items[0].Value = "0";
            ListBoxEstado.SelectedIndex = 0;
        }
   }

   protected void ListBoxCompetencia_DataBound(object sender, EventArgs e)
   {

       if (ListBoxCompetencia.Items.Count > 0)
        {
            ListBoxCompetencia.Items.Insert(0, "Todos");
            ListBoxCompetencia.Items[0].Value = "0";
            ListBoxCompetencia.SelectedIndex = 0;
        }
   }

   protected void ListBoxFormularios_DataBound(object sender, EventArgs e)
   {

       if (ListBoxFormularios.Items.Count > 0)
        {
            ListBoxFormularios.Items.Insert(0, "Todos");
            ListBoxFormularios.Items[0].Value = "0";
            ListBoxFormularios.SelectedIndex = 0;
        }
   }

   protected void ListBoxSector_DataBound(object sender, EventArgs e)
   {
       if (ListBoxSector.Items.Count > 0)
        {
            ListBoxSector.Items.Insert(0, "Todos");
            ListBoxSector.Items[0].Value = "0";
            ListBoxSector.SelectedIndex = 0;
        }
   }
  
}
