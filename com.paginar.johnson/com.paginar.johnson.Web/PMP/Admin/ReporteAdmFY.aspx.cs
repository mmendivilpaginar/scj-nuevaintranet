﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;
using System.Data;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteAdmFY : System.Web.UI.Page
    {

        DataView dvCalificaciones;
        DataView dvSummary;
        string elOrden = string.Empty;

        private RepositoryEvalDesemp _Repositorio = new RepositoryEvalDesemp();
        public RepositoryEvalDesemp Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryEvalDesemp();
                return _Repositorio;
            }
        }

        private RepositoryReporteAdmin _RepositorioRepAdm = new RepositoryReporteAdmin();
        public RepositoryReporteAdmin RepositorioRepAdm
        {
            get
            {
                if (_RepositorioRepAdm == null) _RepositorioRepAdm = new RepositoryReporteAdmin();
                return _RepositorioRepAdm;
            }
        }


        private DSReporteAdmin.CursosDataTable  _dtReportePMPCursos;
        private DSReporteAdmin.CursosDataTable dtReportePMPCursos
        {
            get
            {
                _dtReportePMPCursos = (DSReporteAdmin.CursosDataTable)Session["dtReportePMPCursos"];
                if (_dtReportePMPCursos == null)
                {
                    _dtReportePMPCursos = new DSReporteAdmin.CursosDataTable();
                    int? intnull = null;
                    //DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull
                    RepositorioRepAdm.GetCursosByLegajoPeriodoIdDireccionID(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), int.Parse(DropDownListDireccion.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);

                    Session["dtReportePMPCursos"] = _dtReportePMPCursos;
                }
                return _dtReportePMPCursos;
            }
            set
            {
                Session["dtReportePMPCursos"] = value;
                _dtReportePMPCursos = value;
            }
        }

        private DSReporteAdmin.CompetenciasComoFortalezasDataTable _dtReporteCompetenciasComoFortalezas;
        private DSReporteAdmin.CompetenciasComoFortalezasDataTable dtReporteCompetenciasComoFortalezas
        {
            get
            {
                _dtReporteCompetenciasComoFortalezas = (DSReporteAdmin.CompetenciasComoFortalezasDataTable)Session["dtReporteCompetenciasComoFortalezas"];
                if (_dtReporteCompetenciasComoFortalezas == null)
                {
                    _dtReporteCompetenciasComoFortalezas = new DSReporteAdmin.CompetenciasComoFortalezasDataTable();
                    int? intnull = null;
                    RepositorioRepAdm.AdapterDSReporteAdmin.CompetenciasComoFortalezas.Fill(_dtReporteCompetenciasComoFortalezas, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        int.Parse(DropDownListFortalezas.SelectedValue), int.Parse(DropDownListDireccion.SelectedValue), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);


                    Session["dtReporteCompetenciasComoFortalezas"] = _dtReporteCompetenciasComoFortalezas;
                }
                return _dtReporteCompetenciasComoFortalezas;
            }
            set
            {
                Session["dtReporteCompetenciasComoFortalezas"] = value;
                _dtReporteCompetenciasComoFortalezas = value;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        ControllerReporteAdmin CRA = new ControllerReporteAdmin();

        protected DSReporteAdmin.CalificacionesPMPDataTable DTCalificaciones = new DSReporteAdmin.CalificacionesPMPDataTable();        
        protected DSReporteAdmin.PlanAccionDataTable DTPlanAccion = new DSReporteAdmin.PlanAccionDataTable();
       // protected DSReporteAdmin.CarrerPlanDataTable DTCareerPlan = new DSReporteAdmin.CarrerPlanDataTable();

        //private DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable _dtReporteCalificaciones;
        //private DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable dtReporteCalificaciones
        //{
        //    get
        //    {
        //        _dtReporteCalificaciones = (DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable)Session["dtReporteCalificaciones"];
        //        if (_dtReporteCalificaciones == null)
        //        {
        //            _dtReporteCalificaciones = new DSEvalDesempReportes.frmGetReportePMPCalificacionesDataTable();

        //            Repositorio.AdapterEvalDesemp.GetReportePMPCalificacionesTableAdapter.Fill(_dtReporteCalificaciones, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
        //                int.Parse(DropDownListDireccion.SelectedValue), int.Parse(DropDownListCalificacionTotal.SelectedValue), int.Parse(DropDownListCalificacionFactores.SelectedValue), int.Parse(DropDownListFactores.SelectedValue));


        //            Session["dtReporteCalificaciones"] = _dtReporteCalificaciones;
        //        }
        //        return _dtReporteCalificaciones;
        //    }
        //    set
        //    {
        //        Session["dtReporteCalificaciones"] = value;
        //        _dtReporteCalificaciones = value;
        //    }
        //}

        private DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable _dtReportePlanDeAccion;
        private DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable dtReportePlanDeAccion
        {
            get
            {
                _dtReportePlanDeAccion = (DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable)Session["dtReportePlanDeAccion"];
                if (_dtReportePlanDeAccion == null)
                {
                    _dtReportePlanDeAccion = new DSEvalDesempReportes.frmGetReportePMPPlanDeAccionDataTable();

                    Repositorio.AdapterEvalDesemp.GetReportePMPPlanDeAccionTableAdapter.Fill(_dtReportePlanDeAccion, int.Parse(DropDownListPeriodo.SelectedValue), int.Parse(DropDownListEvaluado.SelectedValue),
                        int.Parse(DropDownListDireccion.SelectedValue),DameElementosListBox(DropDownListCluster));


                    Session["dtReportePlanDeAccion"] = _dtReportePlanDeAccion;
                }
                return _dtReportePlanDeAccion;
            }
            set
            {
                Session["dtReportePlanDeAccion"] = value;
                _dtReportePlanDeAccion = value;
            }
        }



        private DSReporteAdmin.CarrerPlanDataTable _DTCareerPlan;
        private DSReporteAdmin.CarrerPlanDataTable DTCareerPlan
        {
            get
            {
                _DTCareerPlan = (DSReporteAdmin.CarrerPlanDataTable)Session["dtReporteCarrerPlan"];
                if (_DTCareerPlan == null)
                {
                    _DTCareerPlan = new DSReporteAdmin.CarrerPlanDataTable();
                    int? intnull = null;
                    RepositorioRepAdm.GetCareerPlan(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);

                    Session["dtReporteCarrerPlan"] = _DTCareerPlan;
                }
                return _DTCareerPlan;
            }
            set
            {
                Session["dtReporteCarrerPlan"] = value;
                _DTCareerPlan = value;
            }
        }


        private DSReporteAdmin.FortalezasNecesidadesGralDataTable _dtReporteFortalezas;
        private DSReporteAdmin.FortalezasNecesidadesGralDataTable dtReporteFortalezas
        {
            get
            {
                _dtReporteFortalezas = (DSReporteAdmin.FortalezasNecesidadesGralDataTable)Session["_dtReporteFortalezas"];
                if (_dtReporteFortalezas == null)
                {
                    _dtReporteFortalezas = new DSReporteAdmin.FortalezasNecesidadesGralDataTable();
                    int? intnull = null;
                    RepositorioRepAdm.AdapterDSReporteAdmin.FortalezasNecesidadesGralTableAdapter.Fill(_dtReporteFortalezas, int.Parse(DropDownListPeriodo.SelectedValue), 15,
                        int.Parse(DropDownListEvaluado.SelectedValue), int.Parse(DropDownListFortalezas.SelectedValue), int.Parse(DropDownListDireccion.SelectedValue), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);


                    Session["_dtReporteFortalezas"] = _dtReporteFortalezas;
                }
                return _dtReporteFortalezas;
            }
            set
            {
                Session["_dtReporteFortalezas"] = value;
                _dtReporteFortalezas = value;
            }
        }

        private DSReporteAdmin.FortalezasNecesidadesGralDataTable _dtReporteNecesidades;
        private DSReporteAdmin.FortalezasNecesidadesGralDataTable dtReporteNecesidades
        {
            get
            {
                _dtReporteNecesidades = (DSReporteAdmin.FortalezasNecesidadesGralDataTable)Session["_dtReporteNecesidades"];
                if (_dtReporteNecesidades == null)
                {
                    _dtReporteNecesidades = new DSReporteAdmin.FortalezasNecesidadesGralDataTable();
                    int? intnull = null;
                     RepositorioRepAdm.AdapterDSReporteAdmin.FortalezasNecesidadesGralTableAdapter.Fill(_dtReporteNecesidades, int.Parse(DropDownListPeriodo.SelectedValue), 16,
                        int.Parse(DropDownListEvaluado.SelectedValue), int.Parse(DropDownListFortalezas.SelectedValue), int.Parse(DropDownListDireccion.SelectedValue), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);


                    Session["_dtReporteNecesidades"] = _dtReporteNecesidades;
                }
                return _dtReporteNecesidades;
            }
            set
            {
                Session["_dtReporteNecesidades"] = value;
                _dtReporteNecesidades = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void agragarRowalTable(DataRow dr, DataTable dt)
        {
            DataRow row = dt.NewRow();
            row = dr;
            dt.Rows.Add(row);
            dt.AcceptChanges();
            
        }

        protected void ButtonBuscarRepAdministrativos_Click(object sender, EventArgs e)
        {

          /////////////////////////////////////////////////////////////
            DataTable table = new DataTable();
            DataTable tabletemp = new DataTable();


            DataColumn col1 = new DataColumn("Legajo", typeof(int));
            DataColumn col2 = new DataColumn("NombreCompleto", typeof(string));
            DataColumn col3 = new DataColumn("Cargo", typeof(string));
            DataColumn col4 = new DataColumn("FC1", typeof(int));
            DataColumn col5 = new DataColumn("FC2", typeof(int));
            DataColumn col6 = new DataColumn("FC3", typeof(int));
            DataColumn col7 = new DataColumn("FC4", typeof(int));
            DataColumn col8 = new DataColumn("FC5", typeof(int));
            DataColumn col9 = new DataColumn("FC6", typeof(int));
            DataColumn col10 = new DataColumn("FC7", typeof(int));
            DataColumn col11 = new DataColumn("Calificacion", typeof(string));
            DataColumn col12 = new DataColumn("DireccionID", typeof(int));
            DataColumn col13 = new DataColumn("Direccion", typeof(string));

            DataColumn col1temp = new DataColumn("Legajo", typeof(int));
            DataColumn col2temp = new DataColumn("NombreCompleto", typeof(string));
            DataColumn col3temp = new DataColumn("Cargo", typeof(string));
            DataColumn col4temp = new DataColumn("FC1", typeof(int));
            DataColumn col5temp = new DataColumn("FC2", typeof(int));
            DataColumn col6temp = new DataColumn("FC3", typeof(int));
            DataColumn col7temp = new DataColumn("FC4", typeof(int));
            DataColumn col8temp = new DataColumn("FC5", typeof(int));
            DataColumn col9temp = new DataColumn("FC6", typeof(int));
            DataColumn col10temp = new DataColumn("FC7", typeof(int));
            DataColumn col11temp = new DataColumn("Calificacion", typeof(string));
            DataColumn col12temp = new DataColumn("DireccionID", typeof(int));
            DataColumn col13temp = new DataColumn("Direccion", typeof(string));




            DataRow row = tabletemp.NewRow();

            int? legajo = null;
            int? legajoAux = null;
            int counter = 1;
            string Cluster = DameElementosListBox(DropDownListCluster);
            string subQuery = string.Empty;
            dtReporteCompetenciasComoFortalezas = null;
            dtReporteNecesidades = null;
            dtReporteFortalezas = null;

            switch (DropDownListTipoReporte.SelectedValue.ToString())
            {
                case "0": //Competencias Como Fortalezas
                    
                    //ObjectDataSourceReporteCompetenciasComoFortalezas.DataBind();
                    RepeaterReporteCompetenciasComoFortalezas.DataSource = dtReporteCompetenciasComoFortalezas;//ObjectDataSourceReporteCompetenciasComoFortalezas;
                    RepeaterReporteCompetenciasComoFortalezas.DataBind();
                    if (RepeaterReporteCompetenciasComoFortalezas.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCompetenciasComoFortalezas);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Competencias como Fortalezas";
                        LabelTituloPrint.Text = "PMP - Competencias como Fortalezas";
                    }
                    else

                        MultiviewResultados.SetActiveView(ViewSinResultados);

                    break;
                case "1": //Calificaciones PMP
                case "2": //Summary
                    int? integer = null;
                    DTCalificaciones = CRA.GetCalificacionesLegajoPeriodoID(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : integer);
                    
                        
                        table.Columns.Add(col1);
                        table.Columns.Add(col2);
                        table.Columns.Add(col3);
                        table.Columns.Add(col4);
                        table.Columns.Add(col5);
                        table.Columns.Add(col6);
                        table.Columns.Add(col7);
                        table.Columns.Add(col8);
                        table.Columns.Add(col9);
                        table.Columns.Add(col10);
                        table.Columns.Add(col11);
                        table.Columns.Add(col12);
                        table.Columns.Add(col13);


                        tabletemp.Columns.Add(col1temp);
                        tabletemp.Columns.Add(col2temp);
                        tabletemp.Columns.Add(col3temp);
                        tabletemp.Columns.Add(col4temp);
                        tabletemp.Columns.Add(col5temp);
                        tabletemp.Columns.Add(col6temp);
                        tabletemp.Columns.Add(col7temp);
                        tabletemp.Columns.Add(col8temp);
                        tabletemp.Columns.Add(col9temp);
                        tabletemp.Columns.Add(col10temp);
                        tabletemp.Columns.Add(col11temp);
                        tabletemp.Columns.Add(col12temp);
                        tabletemp.Columns.Add(col13temp);


                        
                    
                        legajo = null;
                        legajoAux = null;
                        counter = 1;
                        
                        foreach (DSReporteAdmin.CalificacionesPMPRow item in DTCalificaciones.Rows)
                        {
                            
                            if (legajo == null && legajoAux == null)
                            {
                                legajoAux = int.Parse(item.Legajo.ToString());
                                legajo = int.Parse(item.Legajo.ToString());
                            }

                            if (int.Parse(item.Legajo.ToString()) != legajoAux && legajoAux != null)
                            {

                                legajoAux = int.Parse(item.Legajo.ToString());
                                    
                                tabletemp.Rows.Add(row);
                                
                                //agragarRowalTable(row, table);
                                table.ImportRow(tabletemp.Rows[0]);
                                table.AcceptChanges();
                                tabletemp.Rows.Clear();
                                row[col1temp] = DBNull.Value;
                                row[col2temp] = DBNull.Value;
                                row[col3temp] = DBNull.Value;
                                row[col4temp] = DBNull.Value;
                                row[col5temp] = DBNull.Value;
                                row[col6temp] = DBNull.Value;
                                row[col7temp] = DBNull.Value;
                                row[col8temp] = DBNull.Value;
                                row[col9temp] = DBNull.Value;
                                row[col10temp] = DBNull.Value;
                                row[col11temp] = DBNull.Value;
                                row[col12temp] = DBNull.Value;
                                row[col13temp] = DBNull.Value;
                                
                                counter = 1;
                            }

                            if (counter == 1)
                            {
                                row[col1temp] = int.Parse(item.Legajo.ToString());
                                row[col2temp] = item.NombreYApellido.ToString();
                                row[col3temp] = item.Cargo.ToString();
                                row[col11temp] = item.Calificacion.ToString();
                                row[col4temp] = int.Parse(item.Ponderacion.ToString());
                                row[col12temp] = int.Parse(item.DireccionID.ToString());
                                row[col13temp] = item.Direccion.ToString();
                            }
                            if (counter == 2)
                                row[col5temp] = int.Parse(item.Ponderacion.ToString());
                            if (counter == 3)
                                row[col6temp] = int.Parse(item.Ponderacion.ToString());
                            if (counter == 4)
                                row[col7temp] = int.Parse(item.Ponderacion.ToString());
                            if (counter == 5)
                                row[col8temp] = int.Parse(item.Ponderacion.ToString());
                            if (counter == 6)
                                row[col9temp] = int.Parse(item.Ponderacion.ToString());
                            if (counter == 7)
                                row[col10temp] = int.Parse(item.Ponderacion.ToString());

                            counter++;
                        }
                        tabletemp.Rows.Add(row);
                        table.ImportRow(tabletemp.Rows[0]);
                        table.AcceptChanges();
                        
                        dvCalificaciones = new DataView(table);
                            subQuery = string.Empty;
                            

                        if (DropDownListCalificacionFactores.SelectedValue.ToString() != "0")
                        {

                            if (DropDownListFactores.SelectedValue == "0")
                            {
                                subQuery = string.Format("FC1 = {0} and FC2 = {0} and FC3 = {0} and FC4 = {0}  and FC5 = {0}  and FC6 = {0} and FC7 = {0} ", DropDownListCalificacionFactores.SelectedValue.ToString());
                            }
                            else
                            {
                                subQuery = string.Format("FC{0} = {1}", DropDownListFactores.SelectedValue.ToString(), DropDownListCalificacionFactores.SelectedValue.ToString());
                            }

                            
                        }

                        if (DropDownListCalificacionTotal.SelectedValue != "0")
                        {
                            if (subQuery.Length > 0)
                                subQuery = subQuery + string.Format(" and Calificacion = '{0}'", DropDownListCalificacionTotal.SelectedItem.ToString());
                            else
                                subQuery = string.Format(" Calificacion = '{0}'", DropDownListCalificacionTotal.SelectedItem.ToString());
                        }

                        if (DropDownListDireccion.SelectedValue != "0")
                        {
                            if (subQuery.Length > 0)
                                subQuery = subQuery + string.Format(" and DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
                            else
                                subQuery = string.Format(" DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
                        }

                        CleanSessions();

                        if (subQuery.Length > 0) 
                        {
                            dvCalificaciones.RowFilter = subQuery;
                            if (DropDownListTipoReporte.SelectedValue.ToString() == "1")
                            {
                                RepeaterReporteCalificacionesPMP.DataSource = dvCalificaciones;
                                
                            }
                            else
                            {
                                RepeaterSummary.DataSource = dvCalificaciones;
                            }
                            //table.Clear();
                            //table = dvCalificaciones.ToTable();

                            System.Web.HttpContext.Current.Session["TCalificaciones"] = dvCalificaciones.ToTable();

                            //Session["dvCalificaciones"] = dvCalificaciones;

                        }
                        else
                        {
                            if (DropDownListTipoReporte.SelectedValue.ToString() == "1")
                                RepeaterReporteCalificacionesPMP.DataSource = table;
                            else
                                RepeaterSummary.DataSource = table;

                            

                            System.Web.HttpContext.Current.Session["TCalificaciones"] = table.Copy();
                        }

                        if (table.Rows.Count > 0 && dvCalificaciones.Count > 0)
                        {
                            if (DropDownListTipoReporte.SelectedValue.ToString() == "1")
                            {
                                RepeaterReporteCalificacionesPMP.DataBind();
                                MultiviewResultados.SetActiveView(ViewReporteCalificacionesPMP);
                                LabelResultadobusqueda.Text = "Resultados de Búsqueda - Calificaciones PMP";
                                LabelTituloPrint.Text = "PMP - Calificaciones PMP";
                            }
                            else
                            {
                                RepeaterSummary.DataBind();
                                MultiviewResultados.SetActiveView(ViewReporteSummary);
                                LabelResultadobusqueda.Text = "Resultados de Búsqueda - Summary";
                                LabelTituloPrint.Text = "PMP - Summary";
                            }
                        }
                        else
                        {
                            MultiviewResultados.SetActiveView(ViewSinResultados);

                        }

                        



                    break;

                case "3": //Planes de Accion
            DataTable tablePlan = new DataTable();
            DataTable tablePlantemp = new DataTable();

            DataColumn colPlan1 = new DataColumn("Legajo", typeof(int));
            DataColumn colPlan2 = new DataColumn("NombreCompleto", typeof(string));
            DataColumn colPlan3 = new DataColumn("Cargo", typeof(string));
            DataColumn colPlan4 = new DataColumn("TipoFormularioID", typeof(int));
            DataColumn colPlan5 = new DataColumn("PeriodoID", typeof(int));
            DataColumn colPlan6 = new DataColumn("itemEvaluacionID", typeof(int));
            DataColumn colPlan7 = new DataColumn("Ponderacion", typeof(int));
            DataColumn colPlan8 = new DataColumn("PlanAccion", typeof(string));
            DataColumn colPlan9 = new DataColumn("fy_NroOrden", typeof(int));
            DataColumn colPlan10 = new DataColumn("DireccionID", typeof(int));
            DataColumn colPlan11 = new DataColumn("Direccion", typeof(string));
            DataColumn colPlan12 = new DataColumn("Cursos", typeof(string));

            DataColumn colPlanTemp1 = new DataColumn("Legajo", typeof(int));
            DataColumn colPlanTemp2 = new DataColumn("NombreCompleto", typeof(string));
            DataColumn colPlanTemp3 = new DataColumn("Cargo", typeof(string));
            DataColumn colPlanTemp4 = new DataColumn("TipoFormularioID", typeof(int));
            DataColumn colPlanTemp5 = new DataColumn("PeriodoID", typeof(int));
            DataColumn colPlanTemp6 = new DataColumn("itemEvaluacionID", typeof(int));
            DataColumn colPlanTemp7 = new DataColumn("Ponderacion", typeof(int));
            DataColumn colPlanTemp8 = new DataColumn("PlanAccion", typeof(string));
            DataColumn colPlanTemp9 = new DataColumn("fy_NroOrden", typeof(int));
            DataColumn colPlanTemp10 = new DataColumn("DireccionID", typeof(int));
            DataColumn colPlanTemp11 = new DataColumn("Direccion", typeof(string));
            DataColumn colPlanTemp12 = new DataColumn("Cursos", typeof(string));



            int? integernull = null;
            DTPlanAccion = CRA.GetPlanByLegajoPeriodo(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : integernull);
                
                        
            tablePlan.Columns.Add(colPlan1);
            tablePlan.Columns.Add(colPlan2);
            tablePlan.Columns.Add(colPlan3);
            tablePlan.Columns.Add(colPlan4);
            tablePlan.Columns.Add(colPlan5);
            tablePlan.Columns.Add(colPlan6);
            tablePlan.Columns.Add(colPlan7);
            tablePlan.Columns.Add(colPlan8);
            tablePlan.Columns.Add(colPlan9);
            tablePlan.Columns.Add(colPlan10);
            tablePlan.Columns.Add(colPlan11);
            tablePlan.Columns.Add(colPlan12);
                        


            tablePlantemp.Columns.Add(colPlanTemp1);
            tablePlantemp.Columns.Add(colPlanTemp2);
            tablePlantemp.Columns.Add(colPlanTemp3);
            tablePlantemp.Columns.Add(colPlanTemp4);
            tablePlantemp.Columns.Add(colPlanTemp5);
            tablePlantemp.Columns.Add(colPlanTemp6);
            tablePlantemp.Columns.Add(colPlanTemp7);
            tablePlantemp.Columns.Add(colPlanTemp8);
            tablePlantemp.Columns.Add(colPlanTemp9);
            tablePlantemp.Columns.Add(colPlanTemp10);
            tablePlantemp.Columns.Add(colPlanTemp11);
            tablePlantemp.Columns.Add(colPlanTemp12);


            DataRow rowplan = tablePlantemp.NewRow();   
                    


            foreach (DSReporteAdmin.PlanAccionRow item in DTPlanAccion.Rows)
            {

                rowplan[colPlanTemp1] = int.Parse(item.Legajo.ToString());
                rowplan[colPlanTemp2] = item.NombreYApellido.ToString();
                rowplan[colPlanTemp3] = item.Cargo.ToString();
                rowplan[colPlanTemp4] = int.Parse(item.TipoFormularioID.ToString());
                rowplan[colPlanTemp5] = int.Parse(item.PeriodoID.ToString());
                rowplan[colPlanTemp6] = int.Parse(item.itemEvaluacionID.ToString());
                rowplan[colPlanTemp7] = int.Parse(item.Ponderacion.ToString());
                rowplan[colPlanTemp8] = item.PlanAccion.ToString();
                rowplan[colPlanTemp9] = int.Parse(item.fy_NroOrden.ToString());
                rowplan[colPlanTemp10] = int.Parse(item.DireccionID.ToString());
                rowplan[colPlanTemp11] = item.Direccion.ToString();
                rowplan[colPlanTemp12] = item.Cursos.ToString();
                tablePlantemp.Rows.Add(rowplan);
                tablePlan.ImportRow(tablePlantemp.Rows[0]);
                tablePlan.AcceptChanges();
                tablePlantemp.Rows.Clear();

            rowplan[colPlanTemp1] = DBNull.Value;
            rowplan[colPlanTemp2] = DBNull.Value;
            rowplan[colPlanTemp3] = DBNull.Value;
            rowplan[colPlanTemp4] = DBNull.Value;
            rowplan[colPlanTemp5] = DBNull.Value;
            rowplan[colPlanTemp6] = DBNull.Value;
            rowplan[colPlanTemp7] = DBNull.Value;
            rowplan[colPlanTemp8] = DBNull.Value;
            rowplan[colPlanTemp9] = DBNull.Value;
            rowplan[colPlanTemp10] = DBNull.Value;
            rowplan[colPlanTemp11] = DBNull.Value;
            rowplan[colPlanTemp12] = DBNull.Value;


            }
            DataView dvPlan = new DataView(tablePlan);
            subQuery = string.Empty;


            if (DropDownListDireccion.SelectedValue != "0")
            {
                if (subQuery.Length > 0)
                    subQuery = subQuery + string.Format(" and DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
                else
                    subQuery = string.Format(" DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
            }

            CleanSessions();

            if (subQuery.Length > 0)
            {
                dvPlan.RowFilter = subQuery;
               
                RepeaterPlanDeAccion.DataSource = dvPlan;
                System.Web.HttpContext.Current.Session["TPlanAccion"] = dvPlan.ToTable();
            }
            else
            {
                RepeaterPlanDeAccion.DataSource = tablePlan;
                System.Web.HttpContext.Current.Session["TPlanAccion"] = tablePlan;
            }

            if (tablePlan.Rows.Count > 0 && dvPlan.Count > 0)
            {

                RepeaterPlanDeAccion.DataBind();
                MultiviewResultados.SetActiveView(ViewReportePlanDeAccion);
                LabelResultadobusqueda.Text = "Resultados de Búsqueda - Plan de Acción";
                LabelTituloPrint.Text = "PMP - Plan de Acción";
              
            }
            else
            {
                MultiviewResultados.SetActiveView(ViewSinResultados);

            }

                    


                  
                    break;
                case "4": 

                    
                    RepeaterNecesidadesGral.DataSource = dtReporteNecesidades;
                    RepeaterFortalezasGral.DataSource = dtReporteFortalezas;
                    RepeaterNecesidadesGral.DataBind();
                    RepeaterFortalezasGral.DataBind();

                    string PorcentajeFortalezas = dtReporteFortalezas.Compute("sum(porcentaje)", "").ToString();
                    string PorcentajeNecesidades = dtReporteNecesidades.Compute("sum(porcentaje)", "").ToString();

                    string TotalFortalezas = dtReporteFortalezas.Compute("count(cantidad)", "").ToString();
                    string TotalNecesidades = dtReporteNecesidades.Compute("count(cantidad)", "").ToString();

                    LabelFortalezas.Text = "Estas " + TotalFortalezas + " Fortalezas concentran el " + PorcentajeFortalezas + "% de los resultados obtenidos";
                    LabelNecesidades.Text = "Estas " + TotalNecesidades + " Necesidades concentran el " + PorcentajeNecesidades + "% de los resultados obtenidos";

                    if (RepeaterNecesidadesGral.Items.Count > 0 || RepeaterFortalezasGral.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteFortalezasNecesidadesGral);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Fortalezas y Necesidades de Desarrollo Gral.";
                        LabelTituloPrint.Text = "PMP - Fortalezas y Necesidades <br/> de Desarrollo  Gral.";
                    }
                    else

                        MultiviewResultados.SetActiveView(ViewSinResultados);

                    break;


                case "5": //Carrer Plan
                    int? intnull = null;
                    DTCareerPlan = CRA.GetCareerPlan(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);

                    DataTable tableCarrerPlan = new DataTable();
                    DataTable tableCarrerPlantemp = new DataTable();

                    DataColumn colCarrerPlan1 = new DataColumn("seccion", typeof(int));
                    DataColumn colCarrerPlan2 = new DataColumn("Legajo", typeof(int));
                    DataColumn colCarrerPlan3 = new DataColumn("NombreCompleto", typeof(string));
                    DataColumn colCarrerPlan4 = new DataColumn("Cargo", typeof(string));
                    DataColumn colCarrerPlan5 = new DataColumn("Calificacion", typeof(string));
                    DataColumn colCarrerPlan6 = new DataColumn("DireccionID", typeof(int));
                    DataColumn colCarrerPlan7 = new DataColumn("Direccion", typeof(string));
                    DataColumn colCarrerPlan8 = new DataColumn("IDCPposicion", typeof(int));
                    DataColumn colCarrerPlan9 = new DataColumn("CPposicion", typeof(string));
                    DataColumn colCarrerPlan10 = new DataColumn("CPcomentario", typeof(string));
                    DataColumn colCarrerPlan11 = new DataColumn("IDLPposicion", typeof(int));
                    DataColumn colCarrerPlan12 = new DataColumn("LPposicion", typeof(string));
                    DataColumn colCarrerPlan13 = new DataColumn("LPcomentario", typeof(string));

                    DataColumn colCarrerPlanTemp1 = new DataColumn("seccion", typeof(int));
                    DataColumn colCarrerPlanTemp2 = new DataColumn("Legajo", typeof(int));
                    DataColumn colCarrerPlanTemp3 = new DataColumn("NombreCompleto", typeof(string));
                    DataColumn colCarrerPlanTemp4 = new DataColumn("Cargo", typeof(string));
                    DataColumn colCarrerPlanTemp5 = new DataColumn("Calificacion", typeof(string));
                    DataColumn colCarrerPlanTemp6 = new DataColumn("DireccionID", typeof(int));
                    DataColumn colCarrerPlanTemp7 = new DataColumn("Direccion", typeof(string));
                    DataColumn colCarrerPlanTemp8 = new DataColumn("IDCPposicion", typeof(int));
                    DataColumn colCarrerPlanTemp9 = new DataColumn("CPposicion", typeof(string));
                    DataColumn colCarrerPlanTemp10 = new DataColumn("CPcomentario", typeof(string));
                    DataColumn colCarrerPlanTemp11 = new DataColumn("IDLPposicion", typeof(int));
                    DataColumn colCarrerPlanTemp12 = new DataColumn("LPposicion", typeof(string));
                    DataColumn colCarrerPlanTemp13 = new DataColumn("LPcomentario", typeof(string));

                    tableCarrerPlan.Columns.Add(colCarrerPlan1);
                    tableCarrerPlan.Columns.Add(colCarrerPlan2);
                    tableCarrerPlan.Columns.Add(colCarrerPlan3);
                    tableCarrerPlan.Columns.Add(colCarrerPlan4);
                    tableCarrerPlan.Columns.Add(colCarrerPlan5);
                    tableCarrerPlan.Columns.Add(colCarrerPlan6);
                    tableCarrerPlan.Columns.Add(colCarrerPlan7);
                    tableCarrerPlan.Columns.Add(colCarrerPlan8);
                    tableCarrerPlan.Columns.Add(colCarrerPlan9);
                    tableCarrerPlan.Columns.Add(colCarrerPlan10);
                    tableCarrerPlan.Columns.Add(colCarrerPlan11);
                    tableCarrerPlan.Columns.Add(colCarrerPlan12);
                    tableCarrerPlan.Columns.Add(colCarrerPlan13);


                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp1);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp2);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp3);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp4);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp5);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp6);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp7);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp8);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp9);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp10);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp11);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp12);
                    tableCarrerPlantemp.Columns.Add(colCarrerPlanTemp13);


                    DataRow rowCareer = tableCarrerPlantemp.NewRow();

                        
                    
                        legajo = null;
                        legajoAux = null;
                        counter = 1;

                        foreach (DSReporteAdmin.CarrerPlanRow item in DTCareerPlan.Rows)
                        {
                            
                            if (legajo == null && legajoAux == null)
                            {
                                legajoAux = int.Parse(item.Legajo.ToString());
                                legajo = int.Parse(item.Legajo.ToString());
                            }

                            if (int.Parse(item.Legajo.ToString()) != legajoAux && legajoAux != null)
                            {

                                legajoAux = int.Parse(item.Legajo.ToString());

                                tableCarrerPlantemp.Rows.Add(rowCareer);
                                
                               
                                tableCarrerPlan.ImportRow(tableCarrerPlantemp.Rows[0]);
                                tableCarrerPlan.AcceptChanges();
                                tableCarrerPlantemp.Rows.Clear();
                                rowCareer[colCarrerPlanTemp1] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp2] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp3] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp4] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp5] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp6] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp7] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp8] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp9] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp10] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp11] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp12] = DBNull.Value;
                                rowCareer[colCarrerPlanTemp13] = DBNull.Value;

                                
                                counter = 1;
                            }

                            if (counter == 1)
                            {
                                rowCareer[colCarrerPlanTemp1] = int.Parse(item.seccionID.ToString());
                                rowCareer[colCarrerPlanTemp2] = int.Parse(item.Legajo.ToString());
                                rowCareer[colCarrerPlanTemp3] = item.NombreYApellido.ToString();
                                rowCareer[colCarrerPlanTemp4] = item.Cargo.ToString();
                                rowCareer[colCarrerPlanTemp5] = item.Calificacion.ToString();
                                rowCareer[colCarrerPlanTemp6] = int.Parse(item.DireccionID.ToString());
                                rowCareer[colCarrerPlanTemp7] = item.Direccion.ToString();
                                if (item.AreaInteres.ToString() == "Other")
                                {
                                    rowCareer[colCarrerPlanTemp8] = int.Parse(item.IDAreaInteres.ToString());
                                    rowCareer[colCarrerPlanTemp9] = item.fundamentacion.ToString();
                                }
                                else
                                {

                                        rowCareer[colCarrerPlanTemp8] = int.Parse(item.IDAreaInteres.ToString());
                                        rowCareer[colCarrerPlanTemp9] = item.AreaInteres.ToString();
                                }

                                        
                                

                                
                            }
                            if (counter == 2)
                                rowCareer[colCarrerPlanTemp10] = item.fundamentacion.ToString();

                            if (counter == 4)
                            {
                                if (item.AreaInteres.ToString() == "Other")
                                {
                                    rowCareer[colCarrerPlanTemp11] = int.Parse(item.IDAreaInteres.ToString());
                                    rowCareer[colCarrerPlanTemp12] = item.fundamentacion.ToString();
                                }
                                else
                                {
                                    rowCareer[colCarrerPlanTemp11] = int.Parse(item.IDAreaInteres.ToString());
                                    rowCareer[colCarrerPlanTemp12] = item.AreaInteres.ToString();
                                }
                                
                            }
                            if (counter == 5)
                                rowCareer[colCarrerPlanTemp13] = item.fundamentacion.ToString();


                            counter++;
                        }
                        tableCarrerPlantemp.Rows.Add(rowCareer);
                        tableCarrerPlan.ImportRow(tableCarrerPlantemp.Rows[0]);
                        tableCarrerPlan.AcceptChanges();

                        CleanSessions();

                        DataView dvCarerrPlan = new DataView(tableCarrerPlan);
                            subQuery = string.Empty;
                            

                     if (DropDownListCalificacionTotal.SelectedValue != "0")
                        {
                            if (subQuery.Length > 0)
                                subQuery = subQuery + string.Format(" and Calificacion = '{0}'", DropDownListCalificacionTotal.SelectedItem.ToString());
                            else
                                subQuery = string.Format(" Calificacion = '{0}'", DropDownListCalificacionTotal.SelectedItem.ToString());
                        }

                        if (DropDownListDireccion.SelectedValue != "0")
                        {
                            if (subQuery.Length > 0)
                                subQuery = subQuery + string.Format(" and DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
                            else
                                subQuery = string.Format(" DireccionID = {0}", DropDownListDireccion.SelectedValue.ToString());
                        }

                        if (DropDownListPosicion.SelectedValue != "0")
                        {
                            if (subQuery.Length > 0)
                                subQuery = subQuery + string.Format(" and IDCPposicion = {0} or IDLPposicion = {0}", DropDownListPosicion.SelectedValue.ToString());
                            else
                                subQuery = string.Format(" IDCPposicion = {0} or IDLPposicion = {0}", DropDownListPosicion.SelectedValue.ToString());
                        }




                        if (subQuery.Length > 0) 
                        {
                            dvCarerrPlan.RowFilter = subQuery;
                            RepeaterCarrerPlan.DataSource = dvCarerrPlan;                            
                            System.Web.HttpContext.Current.Session["TCareerPlan"] = dvCarerrPlan.ToTable();
                        }
                        else
                        {
                            RepeaterCarrerPlan.DataSource = tableCarrerPlan;
                            System.Web.HttpContext.Current.Session["TCareerPlan"] = tableCarrerPlan.Copy();
                        }

                        if (tableCarrerPlan.Rows.Count > 0 && dvCarerrPlan.Count > 0)
                        {

                            RepeaterCarrerPlan.DataBind();
                            MultiviewResultados.SetActiveView(ViewReporteCarrerPlan);

                            LabelResultadobusqueda.Text = "Resultados de Búsqueda - Career Plan";
                            LabelTituloPrint.Text = "PMP - Career Plan";
                        }
                        else
                        {
                            MultiviewResultados.SetActiveView(ViewSinResultados);

                        }


                    break;

                case "6": //Cursos

                    dtReportePMPCursos = null;
                    int? intnulle = null;
                    dtReportePMPCursos = CRA.GetCursosByLegajoPeriodoIdDireccionID(int.Parse(DropDownListEvaluado.SelectedValue.ToString()), int.Parse(DropDownListPeriodo.SelectedValue.ToString()), int.Parse(DropDownListDireccion.SelectedValue.ToString()), DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnulle);
                    RepeaterCursos.DataSource = dtReportePMPCursos;
                    //RepeaterCursos.DataSource = ObjectDataSourceCursos;
                    RepeaterCursos.DataBind();
                    if (RepeaterCursos.Items.Count > 0)
                    {
                        MultiviewResultados.SetActiveView(ViewReporteCursos);
                        LabelResultadobusqueda.Text = "Resultados de Búsqueda - Cursos";
                        LabelTituloPrint.Text = "PMP - Cursos";
                    }
                    else

                        MultiviewResultados.SetActiveView(ViewSinResultados);

                    break;
            }

            if (MultiviewResultados.ActiveViewIndex == 0 || MultiviewResultados.ActiveViewIndex == 1)
            {
                ButtonExportarRepSeguimientoAdm.Visible = false;
                ButtonImprimir.Visible = false;
                LabelResultadobusqueda.Visible = false;
            }
            else
            {
                ButtonExportarRepSeguimientoAdm.Visible = true;
                ButtonImprimir.Visible = true;
                LabelResultadobusqueda.Visible = true;

            }

        }

        private void CleanSessions()
        {
            System.Web.HttpContext.Current.Session["TCalificaciones"] = null;
            System.Web.HttpContext.Current.Session["TCareerPlan"] = null;
            System.Web.HttpContext.Current.Session["TPlanAccion"] = null;
        }

        protected string DefaultVal(object val)
        {
            try
            {
                int entero = int.Parse(val.ToString());
                if (entero == 0)
                    return "-";

            }
            catch
            {


            }

            if (((val == System.DBNull.Value) || (val == null)))
                return "-";
            if (val == string.Empty)
                return "-";
            else
                return val.ToString();
        }

        protected void DropDownListTipoReporte_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownListEvaluado.SelectedValue = "0";
            DropDownListFortalezas.SelectedValue = "0";
            DropDownListPosicion.SelectedValue = "0";
            DropDownListDireccion.SelectedValue = "0";
            DropDownListCalificacionFactores.SelectedValue = "0";
            DropDownListFactores.SelectedValue = "0";
            DropDownListCalificacionTotal.SelectedValue = "0";

            switch (((DropDownList)sender).SelectedValue.ToString())
            {

                case "0"://foratalezas como debilidades
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = true;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son las competencias que para el evaluado son fortalezas";
                    break;
                case "1"://calificaciones
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = true;
                    DropDownListFactores.Enabled = true;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cual es la calificación final de acuerdo a los FCE seleccionados";
                    break;
                case "2"://summary
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es visualizar la calificación final de cada evaluado";
                    break;
                case "3"://Planes De Accion
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son los objetivos planteados por los evaluados para el próximo año fiscal";
                    break;
                case "4"://Fortalezas Necesidades Gral
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = true;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son las fortalezas y necesidades más seleccionadas por los evaluados para poder planificar cursos y capacitaciones para cada caso";
                    break;
                case "5"://Carrer Plan
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = true;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = true;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado es identificar cuales son los aspectos que pretende el evaluado a corto y largo plazo";
                    break;
                case "6"://Cursos
                    DropDownListEvaluado.Enabled = true;
                    DropDownListFortalezas.Enabled = false;
                    DropDownListPosicion.Enabled = false;
                    DropDownListDireccion.Enabled = true;
                    DropDownListCalificacionFactores.Enabled = false;
                    DropDownListFactores.Enabled = false;
                    DropDownListCalificacionTotal.Enabled = false;
                    DropDownListTipoReporte.ToolTip = "El objetivo de este listado, es identificar cuales son los cursos mas solicitados por los usuarios de Johnson";
                    break;

            }

            LabelResultadobusqueda.Visible = false;
            MultiviewResultados.SetActiveView(ViewVacio);
            ButtonExportarRepSeguimientoAdm.Visible = false;
            ButtonImprimir.Visible = false;
        }

        protected void Exportar(Repeater rpt, string filename, string titulo)
        {
            if (rpt.Items.Count > 0)
            {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write("<strong>" + LabelTituloPrint.Text + "</strong>");
                Response.Write(generarExportarTodo(rpt, titulo));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
        }
        private string generarExportarTodo(Repeater rpt, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();


            Label LabelApellidoNombre = (Label)rpt.Controls[0].Controls[0].FindControl("LabelApellidoNombre");
            LinkButton LinkButtonApellidoNombre = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonApellidoNombre");

            if (LabelApellidoNombre != null && LinkButtonApellidoNombre != null)
            {
                LabelApellidoNombre.Visible = true;
                LinkButtonApellidoNombre.Visible = false;
            }

            Label LabelFortalezas = (Label)rpt.Controls[0].Controls[0].FindControl("LabelFortalezas");
            LinkButton LinkButtonFortalezas = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonFortalezas");

            if (LabelFortalezas != null && LinkButtonFortalezas != null)
            {
                LabelFortalezas.Visible = true;
                LinkButtonFortalezas.Visible = false;
            }

            Label LabelNecesidades = (Label)rpt.Controls[0].Controls[0].FindControl("LabelNecesidades");
            LinkButton LinkButtonNecesidades = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonNecesidades");

            if (LabelNecesidades != null && LinkButtonNecesidades != null)
            {
                LabelNecesidades.Visible = true;
                LinkButtonNecesidades.Visible = false;
            }



            Label LabelLegajo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelLegajo");
            LinkButton LinkButtonLegajo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonLegajo");

            if (LabelLegajo != null && LinkButtonLegajo != null)
            {
                LabelLegajo.Visible = true;
                LinkButtonLegajo.Visible = false;
            }

            Label LabelCargo = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCargo");
            LinkButton LinkButtonCargo = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCargo");

            if (LabelCargo != null && LinkButtonCargo != null)
            {
                LabelCargo.Visible = true;
                LinkButtonCargo.Visible = false;
            }


            Label LabelCalificacion = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCalificacion");
            LinkButton LinkButtonCalificacion = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCalificacion");

            if (LabelCalificacion != null && LinkButtonCalificacion != null)
            {
                LabelCalificacion.Visible = true;
                LinkButtonCalificacion.Visible = false;
            }

            Label LabelDireccion = (Label)rpt.Controls[0].Controls[0].FindControl("LabelDireccion");
            LinkButton LinkButtonDireccion = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonDireccion");

            if (LabelDireccion != null && LinkButtonDireccion != null)
            {
                LabelDireccion.Visible = true;
                LinkButtonDireccion.Visible = false;
            }



            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(rpt);
            page.RenderControl(htw);
            return sb.ToString();
        }
        protected void Exportar(Repeater rpt1, Repeater rpt2, string filename, string titulo1, string titulo2)
        {
            //  if (rpt.Items.Count > 0)
            // {
            Response.Clear();
            Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name> Reportes PMP</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            Response.Write("<strong>" + LabelTituloPrint.Text + "</strong>");
            Response.Write(generarExportarTodo(rpt1, rpt2, titulo1, titulo2));
            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
            // }
        }
        private string generarExportarTodo(Repeater rpt1, Repeater rpt2, string titulo1, string titulo2)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();

            Label LabelFortalezas = (Label)rpt1.Controls[0].Controls[0].FindControl("LabelFortaleza");
            LinkButton LinkButtonFortalezas = (LinkButton)rpt1.Controls[0].Controls[0].FindControl("LinkButtonFortaleza");

            if (LabelFortalezas != null && LinkButtonFortalezas != null)
            {
                LabelFortalezas.Visible = true;
                LinkButtonFortalezas.Visible = false;
            }

            Label LabelNecesidades = (Label)rpt2.Controls[0].Controls[0].FindControl("LabelNecesidad");
            LinkButton LinkButtonNecesidades = (LinkButton)rpt2.Controls[0].Controls[0].FindControl("LinkButtonNecesidad");

            if (LabelNecesidades != null && LinkButtonNecesidades != null)
            {
                LabelNecesidades.Visible = true;
                LinkButtonNecesidades.Visible = false;
            }



            Literal LiteralTitulo1 = new Literal();
            LiteralTitulo1.Text = "<BR><BR> <strong>" + titulo1 + "</strong><BR><BR>";

            Literal LiteralTitulo2 = new Literal();
            LiteralTitulo2.Text = "<BR><BR> <strong>" + titulo2 + "</strong><BR><BR>";

            // rpaux.EnableViewState = false;        

            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(LiteralTitulo1);
            form.Controls.Add(rpt1);
            form.Controls.Add(LiteralTitulo2);
            form.Controls.Add(rpt2);
            page.RenderControl(htw);
            return sb.ToString();
        }



        protected void Exportar2(Repeater rpt, string filename, string titulo)
        {
            if (rpt.Items.Count > 0)
            {
                Response.Clear();
                Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                Response.Write("<head>");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name> Reportes PMP</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.Write("</head>");
                Response.Write("<body>");
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.xls";
                Response.Write("<strong>" + LabelTituloPrint.Text + "</strong>");
                Response.Write(generarExportarTodo2(rpt, titulo));
                Response.Write("</body>");
                Response.Write("</html>");
                Response.End();
            }
        }
        private string generarExportarTodo2(Repeater rpt, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);


            Page page = new Page();
            HtmlForm form = new HtmlForm();

            Label LabelNombreCursoH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelNombreCurso");
            Label LabelCantidadH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelCantidad");
            Label LabelPorcentajeH = (Label)rpt.Controls[0].Controls[0].FindControl("LabelPorcentaje");

            LinkButton LinkButtonNombreCursoH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonNombreCurso");
            LinkButton LinkButtonCantidadH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonCantidad");
            LinkButton LinkButtonPorcentajeH = (LinkButton)rpt.Controls[0].Controls[0].FindControl("LinkButtonPorcentaje");

            if (LabelNombreCursoH != null && LinkButtonNombreCursoH != null)
            {
                LabelNombreCursoH.Visible = true;
                LinkButtonNombreCursoH.Visible = false;

            }

            if (LabelCantidadH != null && LinkButtonCantidadH != null)
            {
                LabelCantidadH.Visible = true;
                LinkButtonCantidadH.Visible = false;

            }

            if (LabelPorcentajeH != null && LinkButtonPorcentajeH != null)
            {
                LabelPorcentajeH.Visible = true;
                LinkButtonPorcentajeH.Visible = false;

            }


            foreach (RepeaterItem Ri in rpt.Items)
            {
                HiddenField HiddenFieldCursoID = (HiddenField)Ri.FindControl("HiddenFieldCursoID");

                LinkButton LinkButtonkPorcentaje = (LinkButton)Ri.FindControl("LinkButtonPorcentaje");
                LinkButton LinkButtonCantidad = (LinkButton)Ri.FindControl("LinkButtonCantidad");

                Label LabelPorcentaje = (Label)Ri.FindControl("LabelPorcentaje");
                Label LabelCantidad = (Label)Ri.FindControl("LabelCantidad");

                if (LinkButtonkPorcentaje != null)
                {
                    LinkButtonkPorcentaje.Visible = false;
                    LabelPorcentaje.Visible = true;
                }

                if (LinkButtonCantidad != null)
                {
                    LinkButtonCantidad.Visible = false;
                    LabelCantidad.Visible = true;
                }

            }

            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);
            form.Controls.Add(rpt);
            page.RenderControl(htw);
            return sb.ToString();
        }
        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {
            switch (MultiviewResultados.ActiveViewIndex)
            {
                case 2:
                    Exportar(RepeaterReporteCompetenciasComoFortalezas, "Reporte Competencias Como Fortalezas", "Reporte Competencias Como Fortalezas");
                    break;
                case 3:
                    Exportar(RepeaterReporteCalificacionesPMP, "Reporte por Calificaciones", "Reporte por Calificaciones");
                    break;
                case 4:
                    Exportar(RepeaterSummary, "Reporte Summary", "Reporte Summary");
                    break;
                case 5:
                    Exportar(RepeaterPlanDeAccion, "Reporte Planes de Accion", "Reporte Planes de Accion");
                    break;
                case 6:
                    Exportar(RepeaterFortalezasGral, RepeaterNecesidadesGral, "Reporte Planes de Accion", LabelFortalezas.Text, LabelNecesidades.Text);
                    break;
                case 7:
                    Exportar(RepeaterCarrerPlan, "Reporte Carrer Plan", "Reporte Carrer Plan");
                    break;
                case 8:
                    Exportar2(RepeaterCursos, "Reporte Cursos", "Reporte Cursos");
                    break;

            }
        }



        protected void RepeaterCursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton LinkButtonCantidad = (LinkButton)e.Item.FindControl("LinkButtonCantidad");
                HiddenField HiddenFieldCursoID = (HiddenField)e.Item.FindControl("HiddenFieldCursoID");
                LinkButton LinkButtonPorcentaje = (LinkButton)e.Item.FindControl("LinkButtonPorcentaje");
                int? intnull = null;
                //DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull

                LinkButtonPorcentaje.OnClientClick = string.Format("javascript:popupPmpRepAdm('/PMP/Admin/ReporteByCursoID.aspx?PeriodoID={0}&DireccionID={1}&UsuarioID={2}&CursoID={3}&Cluster={4}&TipoFormularioID={5}',600,300)", DropDownListPeriodo.SelectedValue, DropDownListDireccion.SelectedValue, DropDownListEvaluado.SelectedValue, HiddenFieldCursoID.Value, DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);
                LinkButtonCantidad.OnClientClick = string.Format("javascript:popupPmpRepAdm('/PMP/Admin/ReporteByCursoID.aspx?PeriodoID={0}&DireccionID={1}&UsuarioID={2}&CursoID={3}&Cluster={4}&TipoFormularioID={5}',600,300)", DropDownListPeriodo.SelectedValue, DropDownListDireccion.SelectedValue, DropDownListEvaluado.SelectedValue, HiddenFieldCursoID.Value, DameElementosListBox(DropDownListCluster), DDLTipoPMP.SelectedValue != "" ? int.Parse(DDLTipoPMP.SelectedValue) : intnull);

            }
        }

        protected void RepeaterCursos_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterCursos.DataSource = SelectDataTable(dtReportePMPCursos, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterCursos.DataBind();


            }

        }


        public DataTable SelectDataTable(DataTable dt, string filter, string sort)
        {
            DataRow[] rows = null;
            DataTable dtNew = default(DataTable);
            dtNew = dt.Clone();
            rows = dt.Select(filter, sort, DataViewRowState.CurrentRows);
            foreach (DataRow dr in rows)
            {
                dtNew.ImportRow(dr);
            }
            
            return dtNew;
        }

        public DataTable Sort(DataTable dt, string sort, string Order)
        {
            DataTable rows;
            if (Order == "ASC")
            {
                 rows = (from r in dt.AsEnumerable()
                                  orderby r[sort] ascending
                                  select r).CopyToDataTable();
            }
            else
            {
                 rows = (from r in dt.AsEnumerable()
                                  orderby r[sort] descending
                                  select r).CopyToDataTable();                           
            }

            return rows;


        }



        public DataTable SortV2(DataTable dt, string sort, string Order)
        {
            DataTable rows;
            if (Order == "ASC")
            {
                rows = (from r in dt.AsEnumerable()
                        orderby r[sort] ascending
                        select r).CopyToDataTable();
            }
            else
            {
                rows = (from r in dt.AsEnumerable()
                        orderby r[sort] descending
                        select r).CopyToDataTable();
            }

            return rows;


        }


        void SortOrder(string SortExpression)
        {
            if (ViewState[SortExpression] == null)
            {
                ViewState[SortExpression] = " ASC";
            }
            else if (ViewState[SortExpression].ToString() == " ASC")
            {
                ViewState[SortExpression] = " DESC";
            }
            else
            {
                ViewState[SortExpression] = " ASC";
            }

        }

        protected void RepeaterReporteCompetenciasComoFortalezas_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterReporteCompetenciasComoFortalezas.DataSource = Sort(dtReporteCompetenciasComoFortalezas, e.CommandArgument.ToString(), ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterReporteCompetenciasComoFortalezas.DataBind();
            }

        }

        protected void RepeaterReporteCalificacionesPMP_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
                       
            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterReporteCalificacionesPMP.DataSource = Sort((DataTable)Session["TCalificaciones"], e.CommandArgument.ToString(),ViewState[e.CommandArgument.ToString()].ToString().Trim());
                RepeaterReporteCalificacionesPMP.DataBind();

            }                

        }

        protected void RepeaterSummary_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterSummary.DataSource = Sort((DataTable)Session["TCalificaciones"], e.CommandArgument.ToString(), ViewState[e.CommandArgument.ToString()].ToString().Trim());
                RepeaterSummary.DataBind();

            }                      
        }

        protected void RepeaterPlanDeAccion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //Session["TPlanAccion"]
            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterPlanDeAccion.DataSource = Sort((DataTable)Session["TPlanAccion"], e.CommandArgument.ToString(), ViewState[e.CommandArgument.ToString()].ToString().Trim());
                RepeaterPlanDeAccion.DataBind();

            }     
        }

        protected void RepeaterCarrerPlan_ItemCommand(object source, RepeaterCommandEventArgs e)
        {          
            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterCarrerPlan.DataSource = Sort((DataTable)Session["TCareerPlan"], e.CommandArgument.ToString(), ViewState[e.CommandArgument.ToString()].ToString().Trim());
                RepeaterCarrerPlan.DataBind();

            }        

        }

        protected void RepeaterFortalezasGral_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterFortalezasGral.DataSource = Sort(dtReporteFortalezas, e.CommandArgument.ToString(), ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterFortalezasGral.DataBind();
            }

        }

        protected void RepeaterNecesidadesGral_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Sort")
            {
                SortOrder(e.CommandArgument.ToString());
                RepeaterNecesidadesGral.DataSource = SelectDataTable(dtReporteNecesidades, "", e.CommandArgument.ToString() + " " + ViewState[e.CommandArgument.ToString()].ToString());
                RepeaterNecesidadesGral.DataBind();
            }

        }


        private string DameElementosListBox(ListBox lst)
        {
            int[] lstbItems = lst.GetSelectedIndices();
            String Elementos = "";

            foreach (int i in lstbItems)
            {
                if (lst.Items[i].Value.ToString() == "0")
                {
                    Elementos = lst.Items[i].Value.ToString();
                    break;
                }
                else
                {
                    Elementos = Elementos + "," + lst.Items[i].Value.ToString();
                }
            }
            return Elementos.Trim(',');

        }
    

    }
}
