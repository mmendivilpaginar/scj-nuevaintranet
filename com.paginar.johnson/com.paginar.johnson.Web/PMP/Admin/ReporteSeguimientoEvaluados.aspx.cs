﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteSeguimientoEvaluados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["DireccionID"].ToString() == "0" ||  string.IsNullOrEmpty(Request.QueryString["DireccionID"].ToString()))
                PanelDireccion.Visible = false;
            else
                GridViewEvaluados.Columns[2].Visible = false;

            if (Request.QueryString["AreaID"].ToString() == "0" || string.IsNullOrEmpty(Request.QueryString["AreaID"].ToString()))
                PanelArea.Visible = false;
            else
                GridViewEvaluados.Columns[3].Visible = false;


            if (Request.QueryString["PasoID"].ToString() == "0" || string.IsNullOrEmpty(Request.QueryString["PasoID"].ToString()))
                PanelEstado.Visible = false;
            else
                GridViewEvaluados.Columns[5].Visible = false;

            if (Request.QueryString["PasoID"].ToString() == "-1" )
                GridViewEvaluados.Columns[4].Visible = false;

           

        }

        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (val.ToString());

        }
    }
}