﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP.Admin
{
    public partial class ReporteByStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["PasoID"]))
            {
                switch (Request.QueryString["PasoID"].ToString())
                {
                    case "-1":
                        LabelEstado.Text = "PMP No Inicadas";
                        break;
                    case "0":
                        LabelEstado.Visible = false;
                        LabelCantidad.Visible = false;
                        break;
                    case "1":
                        LabelEstado.Text = "PMP En Evaluador";
                        break;

                    case "2":
                        LabelEstado.Text = "PMP En Evaluado";
                        break;

                    case "3":
                        LabelEstado.Text = "PMP En Auditor";
                        break;

                    case "4":
                        LabelEstado.Text = "PMP Finalizadas";
                        break;

                }

            }
            if (!string.IsNullOrEmpty(Request.QueryString["Cantidad"]))
                LabelCantidad.Text = Request.QueryString["Cantidad"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["TotalEvaluaciones"]))
                LabelTotalEvaluaciones.Text = Request.QueryString["TotalEvaluaciones"].ToString();
            if (Request.QueryString["TipoFormularioID"] == "0")

                lblTipoFormulario.Visible = true;
            else
            {
                lblTipoFormulario.Visible = false;
                GridViewEvaluados.Columns[3].Visible = false;            
            }
        }

        protected void GridViewEvaluados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                LinkButton lnkBtnApellidoNombre = (LinkButton)e.Row.FindControl("lnkBtnApellidoNombre");
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                // Label lblApellidoNombre = (Label)e.Row.FindControl("lblApellidoNombre");
                int Legajo;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "Legajo").ToString(), out Legajo);

                int EstadoID;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "EstadoID").ToString(), out EstadoID);

                string UrlForm = string.Empty;
                FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();

                FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
                if (RelPasosTipoFormulariosDt.Rows.Count > 0)
                {
                    FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                    UrlForm = RelPasosTipoFormulariosRow.FormAspx;
                }

                bool carga = false;

                int LegajoEvaluador;
                int.TryParse(Request.QueryString["LegajoEvaluador"], out LegajoEvaluador);

                if (ViewState["HabilitaPeriodo"] != null)
                    carga = (bool)ViewState["HabilitaPeriodo"];
                else
                    carga = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));

                if (carga)
                {
                    if (EstadoID == 4 || EstadoID == 5)
                        lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('../Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                        lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('" + "../" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}&LegajoEvaluador={3}&SoloLectura=1',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, LegajoEvaluador));
                }
                else//Sino no esta en el periodo de carga, tengo q abrir impresion

                    lnkBtnApellidoNombre.Attributes.Add("onclick", string.Format("popup('../Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", Legajo, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                //if (Page.User.IsInRole("Visualizar PMPS-Reporte de Seguimiento PMPO"))
                //    lblApellidoNombre.Visible = false;
                //else
                //    lnkBtnApellidoNombre.Visible = false;
            }
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }
    }
}