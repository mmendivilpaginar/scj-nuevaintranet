﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.servicios
{
    public partial class ReporteSeguimientoByUsuarioMY : System.Web.UI.Page
    {      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Calificacion"]))
            {
                //LabelCalifiacion.Text = " (" + Request.QueryString["Calificacion"] + ")";
               // GridViewEvaluados.Columns[GridViewEvaluados.Columns.Count - 2].Visible = false;
            }

            PanelEvaluador.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"]))
            {
                PanelEvaluador.Visible = (int.Parse(Request.QueryString["LegajoEvaluador"]) != 0);
                
            }

            
             


            //SpanEstado
            if (!string.IsNullOrEmpty(Request.QueryString["EstadoID2"]))
            {
                PanelEstado.Visible = (int.Parse(Request.QueryString["EstadoID2"]) != 0);
                GridViewEvaluados.Columns[GridViewEvaluados.Columns.Count - 1].Visible = (int.Parse(Request.QueryString["EstadoID2"]) != -1);
            }
        }
        protected void GridViewEvaluados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }

       
    }
}