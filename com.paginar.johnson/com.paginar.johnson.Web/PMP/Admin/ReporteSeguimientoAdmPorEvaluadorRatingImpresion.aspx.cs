﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.PMP.Admin
{

  

    public partial class ReporteSeguimientoAdmPorEvaluadorRatingImpresion : System.Web.UI.Page
    {

        private RepositoryReportesDeSeguimiento _Repositorio = new RepositoryReportesDeSeguimiento();
        public RepositoryReportesDeSeguimiento Repositorio
        {
            get
            {
                if (_Repositorio == null) _Repositorio = new RepositoryReportesDeSeguimiento();
                return _Repositorio;
            }
        }


  private  DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable _dtReporte;
  private DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable dtReporte
    {
        get
        {
            _dtReporte = (DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable)Session["dtReporte"];
            if (_dtReporte == null)
            {
                _dtReporte = new DSReportesDeSeguimiento.eval_GetReporteSguimientoAgrupadoEvaluadorRatingDataTable();

                _dtReporte= Repositorio.eval_GetReporteSguimientoAgrupadoEvaluadorRating(int.Parse(Request.QueryString["PeriodoID"]), int.Parse(Request.QueryString["LegajoEvaluador"]), int.Parse(Request.QueryString["PasoID"]), 0, int.Parse(Request.QueryString["DireccionID"]), int.Parse(Request.QueryString["AreaID"]), Request.QueryString["Cluster"].ToString());

               Session["dtReporte"] = _dtReporte;
            }
            return _dtReporte;
        }
        set
        {
            Session["dtReporte"] = value;
            _dtReporte = value;
        }
    }
        protected void Page_Load(object sender, EventArgs e)
        {
            dtReporte = null;

            repeaterReporte.DataSource = SelectDataTable(dtReporte, "Legajo is NULL", "Evaluador"); //DistinctRows(dtReporte, "LegajoEvaluador");
            repeaterReporte.DataBind();

}

        protected void repeaterReporte_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                HiddenField hdFilterValue = (HiddenField)e.Item.FindControl("hdFilterValue");

                String filter = String.Format("LegajoEvaluador = {0} and legajo is not null", hdFilterValue.Value);
                DataTable dtNivel_0 = SelectDataTable(dtReporte, filter, "");

                Repeater rpNivel_0 = (Repeater)e.Item.FindControl("rpNivel_0");
                rpNivel_0.DataSource = dtNivel_0;
                rpNivel_0.DataBind();




            }

        }

        public DataTable SelectDataTable(DataTable dt, string filter, string sort)
        {
            DataRow[] rows = null;
            DataTable dtNew = default(DataTable);
            // copy table structure
            dtNew = dt.Clone();
            // sort and filter data
            rows = dt.Select(filter, sort, DataViewRowState.CurrentRows);
            // fill dtNew with selected rows
            foreach (DataRow dr in rows)
            {
                dtNew.ImportRow(dr);
            }
            // return filtered dt
            return dtNew;
        }

        protected DataTable DistinctRows(DataTable dt, string keyfield)
        {
            DataTable newTable = dt.Clone();
            if (newTable.Columns["rowcount"] == null)
                newTable.Columns.Add("rowcount", typeof(int));
           // int keyval = 0;
            DataView dv = dt.DefaultView;
            dv.Sort = keyfield;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr1 in dt.Rows)
                {
                    bool existe = false;
                    foreach (DataRow dr2 in newTable.Rows)
                    {
                        if (dr1[keyfield].ToString() == dr2[keyfield].ToString())
                        {
                            existe = true;
                            dr2["rowcount"] = ((int)dr2["rowcount"]) + 1;
                        }
                    }

                    if (!existe)
                    {
                        newTable.ImportRow(dr1);
                        newTable.Rows[newTable.Rows.Count - 1]["rowcount"] = 1;
                    }
                }
            }
            else
                newTable = dt.Clone();
            return newTable;
        }

        protected void ButtonExportarExcel_Click(object sender, EventArgs e)
        {
            //if (RepeaterEvaluaciones.Visible == true)
            //{
            //    if (RepeaterEvaluaciones.Items.Count != 0)
            //    {
                    DateTime fecha = DateTime.Now;
                    string filename = "";
                    filename = "Reporte_de_Seguimiento" + String.Format("{0: ddMMyyyy}", fecha);

                    Exportar(repeaterReporte, filename, "Reporte de Seguimiento");
                    //ExportarPDF(RepeaterEvaluaciones, filename, "Reporte de Seguimeinto");
           //     }
          //  }
        }

        protected void Exportar(Repeater gv, string filename, string titulo)
        {
            Response.Clear();
            Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
            Response.Write("<head>");
            Response.Write("<!--[if gte mso 9]><xml>");
            Response.Write("<x:ExcelWorkbook>");
            Response.Write("<x:ExcelWorksheets>");
            Response.Write("<x:ExcelWorksheet>");
            Response.Write("<x:Name> Reportes PMP</x:Name>");
            Response.Write("<x:WorksheetOptions>");
            Response.Write("<x:Print>");
            Response.Write("<x:ValidPrinterInfo/>");
            Response.Write("</x:Print>");
            Response.Write("</x:WorksheetOptions>");
            Response.Write("</x:ExcelWorksheet>");
            Response.Write("</x:ExcelWorksheets>");
            Response.Write("</x:ExcelWorkbook>");
            Response.Write("</xml>");
            Response.Write("<![endif]--> ");
            Response.Write("</head>");
            Response.Write("<body>");
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";

            Response.Write(generarExportarTodo(gv, titulo));

            Response.Write("</body>");
            Response.Write("</html>");
            Response.End();
        }

        private string generarExportarTodo(Repeater gv, string titulo)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Repeater gvaux = new Repeater();
            gvaux = gv;

            //foreach (RepeaterItem GVITEM in gvaux.Items)
            //{
            //    Repeater RepeaterEnNoIniciados = (Repeater)GVITEM.FindControl("RepeaterEnNoIniciados");
            //    Repeater RepeaterEnEvaluado = (Repeater)GVITEM.FindControl("RepeaterEnEvaluado");
            //    Repeater RepeaterEnEvaluador = (Repeater)GVITEM.FindControl("RepeaterEnEvaluador");
            //    Repeater RepeaterNoConcluidas = (Repeater)GVITEM.FindControl("RepeaterNoConcluidas");
            //    Repeater RepeaterAprobadas = (Repeater)GVITEM.FindControl("RepeaterAprobadas");

            //    foreach (RepeaterItem ITEMIN in RepeaterEnNoIniciados.Items)
            //    {
            //        HtmlGenericControl brNoIniciadas = (HtmlGenericControl)ITEMIN.FindControl("brhide");
            //        brNoIniciadas.Visible = false;
            //    }

            //    foreach (RepeaterItem ITEMIN in RepeaterAprobadas.Items)
            //    {
            //        HtmlGenericControl brAprobadas = (HtmlGenericControl)ITEMIN.FindControl("brhide");
            //        brAprobadas.Visible = false;
            //    }

            //    foreach (RepeaterItem ITEMIN in RepeaterEnEvaluado.Items)
            //    {
            //        HtmlGenericControl brEnEvaluado = (HtmlGenericControl)ITEMIN.FindControl("brhide");
            //        brEnEvaluado.Visible = false;
            //    }

            //    foreach (RepeaterItem ITEMIN in RepeaterEnEvaluador.Items)
            //    {
            //        HtmlGenericControl brEnEvaluador = (HtmlGenericControl)ITEMIN.FindControl("brhide");
            //        brEnEvaluador.Visible = false;
            //    }

            //    foreach (RepeaterItem ITEMIN in RepeaterNoConcluidas.Items)
            //    {
            //        HtmlGenericControl brNoConcluidas = (HtmlGenericControl)ITEMIN.FindControl("brhide");
            //        brNoConcluidas.Visible = false;
            //    }
            //}



            Page page = new Page();
            HtmlForm form = new HtmlForm();

            gvaux.EnableViewState = false;


            page.EnableEventValidation = false;

            page.DesignerInitialize();

            page.Controls.Add(form);


            form.Controls.Add(gvaux);

            page.RenderControl(htw);


            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
            string HTML;
            HTML = regEx.Replace(sb.ToString(), "");

            return HTML;

        }


        protected string DefaultVal(object val, object val2)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (val2.ToString() + " (" + val.ToString() + ")");

        }

    protected string DefaultVal1(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (  val.ToString());

        }

    }
}