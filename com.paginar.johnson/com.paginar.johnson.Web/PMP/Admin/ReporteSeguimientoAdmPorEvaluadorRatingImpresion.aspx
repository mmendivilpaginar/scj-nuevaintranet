﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteSeguimientoAdmPorEvaluadorRatingImpresion.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReporteSeguimientoAdmPorEvaluadorRatingImpresion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Johnson Intranet</title>
    <link href="../css/printReporteSeguimiento2.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function imprimir() {
            div = document.getElementById('noprint');
            div.style.display = 'none';
            window.print();
            div.style.display = 'block';
            window.close();
        }

        
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
    
       <div id="header">
            <div class="bg"></div>
            <h1><asp:Label ID="LabelTitulo" runat="server" Text="Reporte de Seguimiento"></asp:Label></h1>
            <img id="logo" src="../../images/logoprint.gif" />
        </div>

        <div id="noprint">           
            <input id="Imprimir" type="button" value="Imprimir" onclick="imprimir()" />           
            <%--<asp:Button ID="ButtonExportarWord" runat="server" Text="Exportar Word" 
                onclick="ButtonExportarWord_Click"/>--%>
            <asp:Button ID="ButtonExportarExcel" runat="server" Text="Exportar excel" 
                onclick="ButtonExportarExcel_Click"/>
            <br /><br />
        </div>

                <asp:Repeater id="repeaterReporte" runat="server" 
                    onitemdatabound="repeaterReporte_ItemDataBound" 
                    
                    >
                    <HeaderTemplate>
                    <table  id="gvReporteGeneral">
    	                <tr> 
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Evaluador</div></th> 
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Calificacion <br /> Evaluador</div></th>                          
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Evaluado</div></th> 
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Legajo</div></th> 
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Estado</div></th> 
                        <th align="center" style="border:1px solid #D9D9D9; padding:0px; background-color: #ccc;width: 180px;">
                                <div style="position:relative;"><div class="bg3"></div>Calificacion</div></th>     		                
    	                </tr>
                    </HeaderTemplate> 
                     
                    <ItemTemplate>
                        <tr>
    		                <td rowspan='<%# Eval("CantidadEvaluados") %>'  >
                                <asp:HiddenField ID="hdFilterValue" runat="server" Value='<%# Bind("LegajoEvaluador") %>' />
    		                    <asp:Label ID="lblEvaluador" runat="server" Text='<%# Bind("Evaluador") %>'></asp:Label>
								
    		                </td>

                             <td rowspan='<%# Eval("CantidadEvaluados") %>'  >
    		                    <asp:Label ID="Label1" runat="server" Text='<%# DefaultVal(Eval("Calificacion"), Eval("CalificacionDescripcion")) %>'></asp:Label>
								
    		                </td>
                            
                            <asp:Repeater id="rpNivel_0" runat="server">
       		                  <ItemTemplate>
                              <%#   ((RepeaterItem)Container).ItemIndex != 0 ? "<tr>" : ""%>
                              <td ><asp:Label ID="LabelEvaluado" runat="server" Text='<%# Eval("Evaluado") %>'></asp:Label></td>
                              <td ><asp:Label ID="Label4" runat="server" Text='<%# Eval("Legajo") %>'></asp:Label></td>
                              <td ><asp:Label ID="LabelEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label></td>
                              <td ><asp:Label ID="Label12" runat="server" Text='<%# DefaultVal1(Eval("CalificacionDescripcion")) %>' ></asp:Label></td>
                              </tr>
                              </ItemTemplate>
                            </asp:Repeater>
                            
                           
                     
                    </ItemTemplate>

                    <FooterTemplate>
   	                </table>
	                </FooterTemplate>  
                                        
                    </asp:Repeater>
    
    </div>
    </form>
</body>
</html>
