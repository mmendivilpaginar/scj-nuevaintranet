﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportesAdm.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Admin.ReportesAdm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
     <link href="../../css/styles.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
     <script type="text/javascript" src="../../js/jquery.fixedheadertable.js"></script>
     <link href="../../css/defaultTheme.css" rel="stylesheet" type="text/css" />
    
     <style type="text/css">
         
        .AspNet-GridViewReportes {
            margin-bottom:10px;
            overflow-x:auto;
            overflow-y:hidden;
            clear:both;
        }
        .AspNet-GridViewReportes table{
            width:100%;
            border: 1px solid rgb(204, 204, 204);
        }
        .AspNet-GridViewReportes table th,
        .AspNet-GridViewReportes table td {
            padding:2px;
            border:1px solid #FFFFFF;
            font-size:12px;
            vertical-align:top;
            color:#000;
        }
        
        .AspNet-GridViewReportes table span {
           color:#000;
        }

        .AspNet-GridViewReportes th{
            background-color:#E6E6E6;
            color:#666666;
            text-transform:uppercase;
            font-weight:normal;
            height:100px;
            padding: 0px;
            
     }   
     
         
   
    .leftthird {
    clear:left;
    float:left;
    width: 30%;
    font-size:12px;
    padding-left:2%;
    }
   .middlethird{
    float:left;
    width:34%;
    font-size:12px;
   }

   .rightthird{
    float:right;
    width:34%;
    font-size:12px;
    }
    
 .buttons{
    margin-bottom:15px;
    overflow:hidden;
    clear:both;
    padding-left:2%;
}

.LabelsFortalezasNecesidades{
    margin-bottom:15px;
    overflow:hidden;
    clear:both;
    padding-left:2%;
    font-size:12px;    
}

.labelTituloPrint
{
    font-size:25px; 
    font-weight:normal; 
    color:#333; 
    background:none; 
    float:left; margin:0; 
    text-indent: 0px;
    }
    
     th .bg{
    position:absolute;
    top:-16px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:48px solid #ccc;
    padding: 0px;
}     

 th .bg1{
    position:absolute;
    top:-8px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:48px solid #ccc;
    padding: 0px;
}     


 th .bg2{
    position:absolute;
    top:-20px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:57px solid #ccc;
    padding: 0px;
}     

 th .bg3{
    position:absolute;
    top:-12px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}     
 th .bg4{
    position:absolute;
    top:-2px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}  
   th .bg5{
    position:absolute;
    top:-13px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:40px solid #ccc;
    padding: 0px;
}   

th .bg6{
    position:absolute;
    top:-10px;
    left:0px;
    width:100%;
    z-index:-1;
    border-top:58px solid #ccc;
    padding: 0px;
}   

.fht-thead{
    overflow:hidden;
 
}
.fht-tbody{   
    height:240px;
}

.fht-table th{
   font-weight:bold;
    text-align:center;
} 


thead {display: table-cell;}


 



         
     </style>
     <script type="text/javascript">
         function Imprimir() {

             $('#PanelFiltros').hide();
             $('#PanelTitulo').hide();
             $('#PanelTitulo2').hide();
             $('#header').show();
             $('table.tableHeaderFixed').fixedHeaderTable('destroy');
             var $thead = $('table.tableHeaderFixed').find('thead');
             $('table.tableHeaderFixed').each(function (index) { $(this).find('thead').remove(); });
             $('table.tableHeaderFixed').each(function (index) { $(this).prepend($thead[index].innerHTML); });

             $('table.tableHeaderFixed1').fixedHeaderTable('destroy');
             var $thead1 = $('table.tableHeaderFixed1').find('thead');
             $('table.tableHeaderFixed1').each(function (index) { $(this).find('thead').remove(); });
             $('table.tableHeaderFixed1').each(function (index) { $(this).prepend($thead1[index].innerHTML); }); 
                         
             window.print();
             $('table.tableHeaderFixed tr:first').each(function (index) { $(this).remove(); });
             $('table.tableHeaderFixed > tbody').each(function (index) { $(this).prepend('<thead>' + $thead[index].innerHTML + '</thead>'); });

             $('table.tableHeaderFixed1 tr:first').each(function (index) { $(this).remove(); });
             $('table.tableHeaderFixed1 > tbody').each(function (index) { $(this).prepend('<thead>' + $thead1[index].innerHTML + '</thead>'); });     
                    
             $('#PanelFiltros').show();
             $('#PanelTitulo').show();
             $('#PanelTitulo2').show();
             $('#header').hide();
             $('table.tableHeaderFixed').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });  
            

         }

         $(document).ready(function () {
             $('#header').hide();
             $('table.tableHeaderFixed').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });
             $('table.tableHeaderFixed1').fixedHeaderTable({ footer: false, cloneHeadToFoot: false });          
            
         });

         function popupPmpRepAdm(url, ancho, alto) {
             var posicion_x;
             var posicion_y;
             posicion_x = (screen.width / 2) - (ancho / 2);
             posicion_y = (screen.height / 2) - (alto / 2);
             window.open(url, "PmpRepAdm", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

         }
         
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div class="buttons" id="PanelTitulo">
    <br />
     <u><b>Reportes PMP Full Year - Administrativos</b></u>
    <br />
    </div>
    
    <div class="box formulario" id="PanelFiltros">
        <div class="form-item leftthird">
        <label class="descripcion"> Período de Evaluación</label>
        <asp:DropDownList ID="DropDownListPeriodo" runat="server" DataSourceID="ObjectDataSourcePeriodosAdministrativos"
            DataTextField="Descripcion" DataValueField="PeriodoID">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSourcePeriodosAdministrativos" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetDataPeriodosAdministrativos" 
                TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.PeriodoTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                    <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                    <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                    <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="PeriodoID" Type="Int32" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                    <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                    <asp:Parameter Name="CargaDesde" Type="DateTime" />
                    <asp:Parameter Name="CargaHasta" Type="DateTime" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="EvaluadoDesde" Type="DateTime" />
                    <asp:Parameter Name="EvaluadoHasta" Type="DateTime" />
                    <asp:Parameter Name="CargaDesde" Type="DateTime" />
                    <asp:Parameter Name="CargaHasta" Type="DateTime" />
                    <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_EvaluadoDesde" Type="DateTime" />
                    <asp:Parameter Name="Original_EvaluadoHasta" Type="DateTime" />
                    <asp:Parameter Name="Original_CargaDesde" Type="DateTime" />
                    <asp:Parameter Name="Original_CargaHasta" Type="DateTime" />
                </UpdateParameters>
            </asp:ObjectDataSource>
                                               
        </div>
        <div class="form-item middlethird">
        <label >Tipo Reporte<br /></label>
        <asp:DropDownList ID="DropDownListTipoReporte" runat="server" AutoPostBack="True" 
                onselectedindexchanged="DropDownListTipoReporte_SelectedIndexChanged" 
                ToolTip="El objetivo de este listado es identificar cual es la calificación final de acuerdo a los FCE seleccionados">
           
         <asp:ListItem Value="1">Calificaciones PMP</asp:ListItem>
         <asp:ListItem Value="5">Career Plan</asp:ListItem>
         <asp:ListItem Value="0">Competencia como Fortalezas</asp:ListItem>         
         <asp:ListItem Value="6">Cursos</asp:ListItem>
         <asp:ListItem Value="4">Fortalezas y Necesidades de Desarrollo Gral.</asp:ListItem>         
         <asp:ListItem Value="3">Plan de Acción</asp:ListItem> 
         <asp:ListItem Value="2">Summary</asp:ListItem>
         
          
                                                         
         </asp:DropDownList>
        </div>
        <div class="form-item rightthird">
        <label>Evaluado <br /></label> 
        <asp:DropDownList ID="DropDownListEvaluado" runat="server" 
                DataSourceID="ObjectDataSourceEvaluados" DataTextField="ApellidoNombre" 
                DataValueField="usuarioid" AppendDataBoundItems="True">
                 <asp:ListItem Value="0">TODOS</asp:ListItem>
            </asp:DropDownList>       
                                               
            <asp:ObjectDataSource ID="ObjectDataSourceEvaluados" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.EvaluadosTableAdapter">
            </asp:ObjectDataSource>
                                               
        </div>        
        
        <div class="form-item leftthird">
        <label >Fortalezas/Necesidades</label>
            <asp:DropDownList ID="DropDownListFortalezas" runat="server" 
                DataSourceID="ObjectDataSourceFortalezas" DataTextField="Descripcion" 
                DataValueField="Descripcion" AppendDataBoundItems="True" Enabled="False">
                        <asp:ListItem Value="0">TODOS</asp:ListItem>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceFortalezas" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmPMPFortalezasTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_Codigo" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Idioma" Type="String" />
                    <asp:Parameter Name="Original_Modulo" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Codigo" Type="Int32" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Idioma" Type="String" />
                    <asp:Parameter Name="Modulo" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Codigo" Type="Int32" />
                    <asp:Parameter Name="Original_Descripcion" Type="String" />
                    <asp:Parameter Name="Original_Idioma" Type="String" />
                    <asp:Parameter Name="Original_Modulo" Type="String" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </div>  
        <div class="form-item middlethird">
        <label>Posición</label>
            <asp:DropDownList ID="DropDownListPosicion" runat="server" Enabled="False" 
                DataSourceID="ObjectDataSourceFrmAreas" DataTextField="DescAreas" 
                DataValueField="ID" AppendDataBoundItems="true">
            <asp:ListItem Value="0">TODOS</asp:ListItem>                   
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSourceFrmAreas" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                
                
                TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmPMPAreasTableAdapter" 
                DeleteMethod="Delete" InsertMethod="Insert" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ID" Type="Int32" />
                    <asp:Parameter Name="Original_DescAreas" Type="String" />
                    <asp:Parameter Name="Original_habilitado" Type="Boolean" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="DescAreas" Type="String" />
                    <asp:Parameter Name="habilitado" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="DescAreas" Type="String" />
                    <asp:Parameter Name="habilitado" Type="Boolean" />
                    <asp:Parameter Name="Original_ID" Type="Int32" />
                    <asp:Parameter Name="Original_DescAreas" Type="String" />
                    <asp:Parameter Name="Original_habilitado" Type="Boolean" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </div>
        <div class="form-item rightthird">
        <label >Dirección</label>
            <asp:DropDownList ID="DropDownListDireccion" runat="server" 
                        DataSourceID="ObjectDataSourceDirecciones" DataTextField="DireccionDET" 
                        DataValueField="DireccionID" AppendDataBoundItems="True">
              <asp:ListItem Value="0">TODOS</asp:ListItem>
                    </asp:DropDownList>          
            <asp:ObjectDataSource ID="ObjectDataSourceDirecciones" runat="server" 
                DeleteMethod="Delete" InsertMethod="Insert" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                TypeName="com.paginar.johnson.DAL.DSUsuariosTableAdapters.DireccionTableAdapter" 
                UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="DireccionDET" Type="String" />
                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="DireccionDET" Type="String" />
                    <asp:Parameter Name="CodigoPXXi" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionID" Type="Int32" />
                    <asp:Parameter Name="Original_DireccionDET" Type="String" />
                    <asp:Parameter Name="Original_CodigoPXXi" Type="Int32" />
                </UpdateParameters>
            </asp:ObjectDataSource>

        </div> 
        <div class="form-item leftthird">
        <label>Calificación por Factor</label>
        <asp:DropDownList ID="DropDownListCalificacionFactores" runat="server" 
                DataSourceID="ObjectDataSourceCalificacionesFactores" DataTextField="Descripcion" 
                DataValueField="CalificacionID"  AppendDataBoundItems="True" 
                >
                        <asp:ListItem Value="0">TODOS</asp:ListItem>
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSourceCalificacionesFactores" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataCalificacionesFactores" 
                
                TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmPMPCalificacionTableAdapter"></asp:ObjectDataSource>
        </div>
        
        <div class="form-item middlethird">
        <label>Factores</label>
            <asp:DropDownList ID="DropDownListFactores" runat="server">
            <asp:ListItem Value="0">TODOS</asp:ListItem>
            <asp:ListItem Value="1">FC1</asp:ListItem>
            <asp:ListItem Value="2">FC2</asp:ListItem>
            <asp:ListItem Value="3">FC3</asp:ListItem>
            <asp:ListItem Value="4">FC4</asp:ListItem>
            <asp:ListItem Value="5">FC5</asp:ListItem>
            <asp:ListItem Value="6">FC6</asp:ListItem>
            </asp:DropDownList>
        </div>  

        <div class="form-item middlethird">
        <label >Calificación Total</label>
        <asp:DropDownList ID="DropDownListCalificacionTotal" runat="server" 
                DataSourceID="ObjectDataSourceCalificacionesTotal" DataTextField="Descripcion" 
                DataValueField="CalificacionID"  AppendDataBoundItems="True" 
                >
                        <asp:ListItem Value="0">TODOS</asp:ListItem>
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSourceCalificacionesTotal" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetCalificacionesTotal" 
                
                TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmPMPCalificacionTableAdapter"></asp:ObjectDataSource>
        </div>
                                  
        <div class="buttons">
        <asp:Button ID="ButtonBuscarRepAdministrativos" runat="server" OnClick="ButtonBuscarRepAdministrativos_Click" 
                    Text="Buscar" />
       <asp:Button ID="ButtonExportarRepSeguimientoAdm" runat="server" onclick="ButtonExportarExcel_Click" 
                    Text="Exportar"  Visible="false"/> 
      <asp:Button ID="ButtonImprimir" runat="server"  Text="Imprimir"  visible="false" OnClientClick="Imprimir()"/>
        </div>
        </div>

       <div id="header" style="overflow:hidden; zoom:1; padding:20px; margin-bottom: 20px;position:relative; background:none; height:70px;">
            <div style="position: absolute; top:0; left:0; width: 100%; height:30px; border-top:100px solid #ccc; z-index:-1;"></div>
            <asp:Label ID="LabelTituloPrint" runat="server" Text="Label" CssClass="labelTituloPrint"></asp:Label>            
            <img  src="../../images/logoprint.gif" style="float:right;" />
        </div>

        <div class="buttons" id="PanelTitulo2">
        <br />
           <asp:Label ID="LabelResultadobusqueda" runat="server" Text="Resultados de Búsqueda - Competencias como Fortalezas"
           visible="false"></asp:Label>
        <br />
        </div>        
     
        <asp:multiview ID="MultiviewResultados" runat="server" ActiveViewIndex="0">
                      
            <asp:View ID="ViewVacio" runat="server">
            </asp:View>
            <asp:View ID="ViewSinResultados" runat="server">
            <div class="buttons">
            No se encontraron resultados para la búsqueda
            </div>
            </asp:View>                      
            <asp:View ID="ViewReporteCompetenciasComoFortalezas" runat="server">
                <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterReporteCompetenciasComoFortalezas" runat="server" 
                        onitemcommand="RepeaterReporteCompetenciasComoFortalezas_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                            <thead>
                                <tr style="height:50px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg1"></div>
                                        <asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br />Nombre</asp:LinkButton>
                                        <asp:Label ID="LabelApellidoNombre" runat="server" Text="Label" Visible="false">Apellido y <br />Nombre</asp:Label>
                                      </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                     <asp:Label ID="LabelFortalezas" runat="server" Text="Label">Fortalezas</asp:Label>
                                     </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                    <asp:Label ID="LabelNecesidades" runat="server" Text="Label" >Necesidades</asp:Label>
                                    </div></th>
                                </tr>
                           </thead>
                           <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td rowspan="3">
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" 
                                        Text='<%# DefaultVal(Eval("Fortaleza1")) %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" 
                                        Text='<%# DefaultVal(Eval("Necesidad1")) %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DefaultVal(Eval("Fortaleza2")) %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# DefaultVal(Eval("Necesidad2")) %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text='<%# DefaultVal(Eval("Fortaleza3")) %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# DefaultVal(Eval("Necesidad3")) %>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourceReporteCompetenciasComoFortalezas" 
                        runat="server" OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReporteCompetenciaComoFortalezasTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListFortalezas" Name="FortalezaID" 
                                PropertyName="SelectedValue" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </asp:View>
            <asp:View ID="ViewReporteCalificacionesPMP" runat="server">
                <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterReporteCalificacionesPMP" runat="server" 
                        onitemcommand="RepeaterReporteCalificacionesPMP_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                            <thead>
                                <tr style="height:50px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                        <asp:LinkButton ID="LinkButtonLegajo" runat="server" CommandName="Sort" CommandArgument="Legajo">Legajo</asp:LinkButton>
                                        <asp:Label ID="LabelLegajo" runat="server" Text="Legajo" Visible="false"></asp:Label>
                                    </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg1"></div>
                                        <asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br /> Nombre</asp:LinkButton>
                                        <asp:Label ID="LabelApellidoNombre" runat="server" Text="Label" Visible="false">Apellido y <br /> Nombre</asp:Label>
                                    </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                        <asp:LinkButton ID="LinkButtonCargo" runat="server" CommandName="Sort" CommandArgument="Cargo">Cargo</asp:LinkButton>
                                        <asp:Label ID="LabelCargo" runat="server" Text="Label" Visible="false">Cargo</asp:Label>
                                    </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC1</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC2</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC3</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC4</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC5</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>FC6</div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg1"></div>
                                        <asp:LinkButton ID="LinkButtonCalificacion" runat="server" CommandName="Sort" CommandArgument="CalificacionTotalID"> Calificación <br />Total</asp:LinkButton>
                                        <asp:Label ID="LabelCalificacion" runat="server" Text="Label" Visible="false"> Calificación <br />Total</asp:Label>
                                   </div></th>
                                </tr>
                           </thead>
                           <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("Legajo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("Cargo") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_1")) %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_2")) %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label7" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_3")) %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label8" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_4")) %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label9" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_5")) %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label10" runat="server" 
                                        Text='<%# DefaultVal(Eval("Calificacion_R_6")) %>'></asp:Label>
                                </td>
                                 <td>
                                    <asp:Label ID="Label11" runat="server" 
                                        Text='<%# DefaultVal(Eval("CalificacionTotal")) %>'></asp:Label>
                                </td>
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourceReporteCalificacionesPMP" 
                        runat="server" OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReportePMPCalificacionesTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListDireccion" Name="DireccionID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListCalificacionTotal" 
                                Name="CalificacionTotal" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListCalificacionFactores" 
                                Name="CalificacionFactor" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListFactores" Name="FactorID" 
                                PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </asp:View>
            <asp:View ID="ViewReporteSummary" runat="server">
               <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterSummary" runat="server" 
                        onitemcommand="RepeaterSummary_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                            <thead>
                                <tr style="height:50px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                       <asp:LinkButton ID="LinkButtonLegajo" runat="server" CommandName="Sort" CommandArgument="Legajo">Legajo</asp:LinkButton>
                                        <asp:Label ID="LabelLegajo" runat="server" Text="Legajo" Visible="false"></asp:Label></div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg1"></div>
                                        <asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br /> Nombre</asp:LinkButton>
                                        <asp:Label ID="LabelApellidoNombre" runat="server" Text="Label" Visible="false">Apellido y <br /> Nombre</asp:Label></div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                         <asp:LinkButton ID="LinkButtonDireccion" runat="server" CommandName="Sort" CommandArgument="direccion">Dirección</asp:LinkButton>
                                        <asp:Label ID="LabelDireccion" runat="server" Text="Label" Visible="false"> Dirección</asp:Label></div></th>                                   
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                        <asp:LinkButton ID="LinkButtonCalificacion" runat="server" CommandName="Sort" CommandArgument="CalificacionTotalID"> Calificación <br />Total</asp:LinkButton>
                                        <asp:Label ID="LabelCalificacion" runat="server" Text="Label" Visible="false"> Calificación <br />Total</asp:Label>
                                        </div></th>
                                </tr>
                           </thead>
                           <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("Legajo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("direccion") %>'></asp:Label>
                                </td>
                                
                                 <td>
                                    <asp:Label ID="Label11" runat="server" 
                                        Text='<%# DefaultVal(Eval("CalificacionTotal")) %>'></asp:Label>
                                </td>
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourceSummary" 
                        runat="server" OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReportePMPCalificacionesTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListDireccion" Name="DireccionID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListCalificacionTotal" 
                                Name="CalificacionTotal" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListCalificacionFactores" 
                                Name="CalificacionFactor" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListFactores" Name="FactorID" 
                                PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </asp:View>
            <asp:View ID="ViewReportePlanDeAccion" runat="server">
               <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterPlanDeAccion" runat="server" 
                        onitemcommand="RepeaterPlanDeAccion_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                            <thead>
                                <tr style="height:50px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                    <asp:LinkButton ID="LinkButtonLegajo" runat="server" CommandName="Sort" CommandArgument="Legajo">Legajo</asp:LinkButton>
                                        <asp:Label ID="LabelLegajo" runat="server" Text="Legajo" Visible="false"></asp:Label>
                                        </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg1"></div>
                                    <asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br /> Nombre</asp:LinkButton>
                                        <asp:Label ID="LabelApellidoNombre" runat="server" Text="Label" Visible="false">Apellido y <br /> Nombre</asp:Label>
                                        </div></th>
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg"></div>
                                    <asp:LinkButton ID="LinkButtonCargo" runat="server" CommandName="Sort" CommandArgument="Cargo">Cargo</asp:LinkButton>
                                        <asp:Label ID="LabelCargo" runat="server" Text="Label" Visible="false">Cargo</asp:Label>
                                        </div></th>                                   
                                    <th style="background-color: #ccc; width:200px;"><div style="position:relative;"><div class="bg"></div>Plan de Acción</div></th>
                                    <th style="background-color: #ccc; width:200px;"><div style="position:relative;"><div class="bg"></div>Cursos</div></th>
                                </tr>
                           </thead>
                           <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("Legajo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("cargo") %>'></asp:Label>
                                </td>
                                
                                 <td>
                                    <asp:Label ID="Label11" runat="server" 
                                        Text='<%# DefaultVal(Eval("PlanesAccion")) %>'></asp:Label>
                                </td>
                                <td> 
                                                         
                                 <asp:Repeater ID="RepeaterCursosByTramiteID" runat="server" DataSourceID="ObjectDataSourceCursosByTramiteID">
                                 <ItemTemplate>
                                   <br  style="mso-data-placement: same-cell;"/> 
                                    <%# Eval("CursoDescripcion", " - {0}").ToString()%>
                                    <br  style="mso-data-placement: same-cell;"/>
                                 </ItemTemplate>
                                 <FooterTemplate>
                                 </FooterTemplate>
                                 </asp:Repeater>
                                 <asp:HiddenField ID="HiddenFieldTramiteID" runat="server" Value='<%# Eval("tra_id") %>'/>  
                                 <asp:ObjectDataSource ID="ObjectDataSourceCursosByTramiteID" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.FrmPMPReglaCursosDevTableAdapter">
                                        <SelectParameters>
                                            <asp:Parameter Name="UsrNro" Type="Int32" />
                                            <asp:Parameter Name="PlafCod" Type="Int32" />
                                            <asp:Parameter DefaultValue="CURSOS_BY_TRAMITEID" Name="Modo" Type="String" />
                                            <asp:Parameter Name="FormularioID" Type="Int32" />
                                            <asp:Parameter Name="AreaID" Type="Int32" />
                                            <asp:Parameter Name="DebilidadID" Type="String" />
                                            <asp:ControlParameter ControlID="HiddenFieldTramiteID" DefaultValue="" 
                                                Name="TRA_ID" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </td>
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourcePlanesDeAccion" 
                        runat="server" OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        
                        
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReportePMPPlanDeAccionTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListDireccion" Name="DireccionID" 
                                PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    
                </div>
            </asp:View>
            <asp:View ID="ViewReporteFortalezasNecesidadesGral" runat="server">
               <div class="LabelsFortalezasNecesidades">
               
                 <asp:Label ID="LabelFortalezas" runat="server" Text="Label"></asp:Label>
                 
              </div>
                <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterFortalezasGral" runat="server" 
                        onitemcommand="RepeaterFortalezasGral_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                             <thead>
                                <tr style="height:40px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg5"></div>
                                        <asp:LinkButton ID="LinkButtonFortaleza" runat="server" CommandName="Sort" CommandArgument="Fortaleza">Fortalezas</asp:LinkButton>
                                        <asp:Label ID="LabelFortaleza" runat="server" Text="Label" Visible="false">Fortalezas</asp:Label>
                                    </div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>Cantidad</div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>Porcentaje</div></th>                                  
                                    
                                </tr>
                              </thead>
                              <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("Fortaleza") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("Cantidad") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("Porcentaje") %>'></asp:Label>
                                </td>
                                
                                 
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    

                    
                </div>

             <div class="LabelsFortalezasNecesidades">               
                 <asp:Label ID="LabelNecesidades" runat="server" Text="Label"></asp:Label>
                
             </div>
                <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterNecesidadesGral" runat="server" 
                        onitemcommand="RepeaterNecesidadesGral_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed1">
                            <thead>
                                <tr style="height:40px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg5"></div>
                                    <asp:LinkButton ID="LinkButtonNecesidad" runat="server" CommandName="Sort" CommandArgument="Necesidad">Necesidades</asp:LinkButton>
                                        <asp:Label ID="LabelNecesidad" runat="server" Text="Label" Visible="false">Necesidades</asp:Label>
                                        </div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>Cantidad</div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>Porcentaje</div></th>                                  
                                    
                                </tr>
                           </thead>
                           <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("Necesidad") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("Cantidad") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("Porcentaje") %>'></asp:Label>
                                </td>
                                
                                 
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                   

                    
                </div>
            </asp:View>
            <asp:View ID="ViewReporteCarrerPlan" runat="server">
               <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterCarrerPlan" runat="server" 
                        onitemcommand="RepeaterCarrerPlan_ItemCommand">
                        <HeaderTemplate>
                            <table>
                                <tbody>
                                <tr style="height:25px;">
                                    
                                    <th style="background-color: #ccc;" rowspan="2"><div style="position:relative;"><div class="bg6"></div>
                                        <asp:LinkButton ID="LinkButtonApellidoNombre" runat="server" CommandName="Sort" CommandArgument="ApellidoNombre">Apellido y <br /> Nombre</asp:LinkButton>
                                        <asp:Label ID="LabelApellidoNombre" runat="server" Text="Label" Visible="false">Apellido y <br /> Nombre</asp:Label></div></th>
                                    <th style="background-color: #ccc;" rowspan="2"><div style="position:relative;"><div class="bg2"></div>
                                        <asp:LinkButton ID="LinkButtonDireccion" runat="server" CommandName="Sort" CommandArgument="direccion">Dirección</asp:LinkButton>
                                        <asp:Label ID="LabelDireccion" runat="server" Text="Label" Visible="false"> Dirección</asp:Label></div></th>                                   
                                    <th style="background-color: #ccc;" rowspan="2"><div style="position:relative;"><div class="bg3"></div>
                                       <asp:LinkButton ID="LinkButtonCalificacion" runat="server" CommandName="Sort" CommandArgument="CalificacionTotal"> Calificación <br />Total</asp:LinkButton>
                                        <asp:Label ID="LabelCalificacion" runat="server" Text="Label" Visible="false"> Calificación <br />Total</asp:Label>
                                        </div></th>
                                    <th style="background-color: #ccc;" colspan="2"><div style="position:relative;"><div class="bg4"></div>Corto Plazo (0 a 2 años)</div></th>
                                    <th style="background-color: #ccc;" colspan="2"><div style="position:relative;"><div class="bg4"></div>Largo Plazo  (3 a 5 años)</div></th>
                                    
                                </tr>
                                <tr style="height:25px;">                                   
                                    
                                    <th style="background-color: #ccc;">Posición </th>
                                    <th style="background-color: #ccc;">Comentario <br /> Evaluado</th>
                                    <th style="background-color: #ccc;">Posición</th>
                                    <th style="background-color: #ccc;">Comentario <br />Evaluado</th>
                                </tr>
                                
                               
                           
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                
                                <td >
                                    <asp:Label ID="lblCalificacionNSEnEvaluado" runat="server" 
                                        Text='<%# Eval("ApellidoNombre") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" 
                                        Text='<%# Eval("direccion") %>'></asp:Label>
                                </td>
                                
                                 <td>
                                    <asp:Label ID="Label11" runat="server" 
                                        Text='<%# DefaultVal(Eval("CalificacionTotal")) %>'></asp:Label>
                                </td>

                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("DescAreaDeseadaCortoPlazo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="Label14" runat="server" 
                                        Text='<%# Eval("ComentarioEmpleadoCortoPlazo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="Label15" runat="server" 
                                        Text='<%# Eval("DescAreaDeseadaLargoPlazo") %>'></asp:Label>
                                </td>
                                <td >
                                    <asp:Label ID="Label16" runat="server" 
                                        Text='<%# Eval("ComentarioEmpleadolargoPlazo") %>'></asp:Label>
                                </td>
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourceCarrerPlan" 
                        runat="server" OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetData" 
                        
                        
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReportePMPCarrerPlanTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListDireccion" Name="DireccionID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListPosicion" Name="Posicion" 
                                PropertyName="SelectedValue" Type="String" />
                            <asp:ControlParameter ControlID="DropDownListCalificacionTotal" 
                                Name="CalificacionTotal" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </asp:View>

            <asp:View ID="ViewReporteCursos" runat="server">
              
                <div class="AspNet-GridViewRepPMP">
                    <asp:Repeater ID="RepeaterCursos" runat="server" 
                        onitemdatabound="RepeaterCursos_ItemDataBound" 
                        onitemcommand="RepeaterCursos_ItemCommand">
                        <HeaderTemplate>
                            <table class="tableHeaderFixed">
                            <thead>
                                <tr style="height:40px">
                                    <th style="background-color: #ccc;"><div style="position:relative;"><div class="bg5"></div>
                                    <asp:Label ID="LabelNombreCurso" runat="server" Text="Nombre del Curso" Visible="false"></asp:Label>
                                    <asp:LinkButton ID="LinkButtonNombreCurso" runat="server" CommandName="Sort" CommandArgument="cursodescripcion">Nombre del Curso</asp:LinkButton></div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>
                                    <asp:Label ID="LabelCantidad" runat="server" Text="Cantidad" Visible="false"></asp:Label>
                                    <asp:LinkButton ID="LinkButtonCantidad" runat="server" CommandName="Sort" CommandArgument="Cantidad">Cantidad</asp:LinkButton></div></th>
                                    <th style="background-color: #ccc; width:100px;"><div style="position:relative;"><div class="bg5"></div>
                                    <asp:Label ID="LabelPorcentaje" runat="server" Text="Porcentaje" Visible="false"></asp:Label>
                                    <asp:LinkButton ID="LinkButtonPorcentaje" runat="server" CommandName="Sort" CommandArgument="Porcentaje">Porcentaje</asp:LinkButton></div></th>                                  
                                    
                                </tr>
                                </thead>
                                <tbody>
                           
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td >
                                    <asp:Label ID="Label12" runat="server" 
                                        Text='<%# Eval("cursodescripcion") %>'></asp:Label>
                                </td>
                                <td >
                                       <asp:HiddenField ID="HiddenFieldCursoID" runat="server" Value='<%# Eval("cursoID") %>' />
                                       <asp:LinkButton ID="LinkButtonCantidad"  CssClass="Link" runat="server"  Text='<%# Eval("Cantidad") %>'></asp:LinkButton>
                                       <asp:Label ID="LabelCantidad" runat="server" Text='<%# Eval("Cantidad") %>' Visible="false"></asp:Label>
                                </td>
                                
                                <td>
                                     <asp:LinkButton ID="LinkButtonPorcentaje"  CssClass="Link" runat="server"  Text='<%# Eval("Porcentaje") %>'></asp:LinkButton>
                                     <asp:Label ID="LabelPorcentaje" runat="server" Text='<%# Eval("Porcentaje") %>' Visible="false"></asp:Label>
                                </td>
                                
                                 
                            </tr>
  
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>

                    <asp:ObjectDataSource ID="ObjectDataSourceCursos" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" 
                        TypeName="com.paginar.johnson.DAL.DSEvalDesempReportesTableAdapters.frmGetReportePMPCursosTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownListPeriodo" Name="PeriodoID" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListEvaluado" Name="UsuarioID" 
                                PropertyName="SelectedValue" Type="Int32" />                            
                            <asp:Parameter DefaultValue="" Name="CursoID" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownListDireccion" DefaultValue="" 
                                Name="DireccionID" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    

                    
                </div>

       
            </asp:View>

        </asp:multiview>
             
      <br />
    </div>
    </form>
</body>

</html>
