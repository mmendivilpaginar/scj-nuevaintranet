﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.formularios.businesslogiclayer;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Impresion : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }
            LogController fle = new LogController(); ;

            int legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresión PMPO", legajo.ToString());

            LabelRegistro.Text = "Fecha de Exportación/Impresión: " + DateTime.Now + "<br/>Usuario: " + ObjectUsuario.apellido + " " + ObjectUsuario.nombre;
        }
        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);

        }

        protected string GetResultado(object Ponderacion)
        {
            string PonderacionAux = Ponderacion.ToString();

            switch (PonderacionAux)
            {
                case "1":
                    return "No Satisfactorio";
                    break;
                case "2":
                    return "Regularmente Cumple / Necesita desarrollarse";
                    break;
                case "3":
                    return "Bueno";
                    break;
                case "4":
                    return "Muy Bueno";
                    break;
                default:
                    return string.Empty;
                    break;
            }


        }

        protected string DefaultVal(object val, string DefaultValue)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return DefaultValue;
            return (val.ToString());

        }

        protected void ButtonExportar_Click(object sender, EventArgs e)
        {

            Response.Redirect("exportarWordPMPO.aspx?VieneDeImpresion=si&Legajo=" + HiddenFieldlegajo.Value + "&PeriodoID=" + HiddenFieldPeriodoID.Value + "&TipoFormularioID=" + HiddenFieldTipoFormularioID.Value);

            LogController fle = new LogController(); ;


            fle.Log(this.ObjectUsuario.legajo.ToString(), "Exportación PMPO - " + ((Label)FVC.FindControl("Label1")).Text + "  Evaluado: " + ((Label)FVC.FindControl("Label2")).Text, HiddenFieldlegajo.Value.ToString());

        }

        protected void Impresion_Click(object sender, EventArgs e)
        {
            LogController fle = new LogController(); ;


            fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresión PMPO - " + ((Label)FVC.FindControl("Label1")).Text + "  Evaluado: " + ((Label)FVC.FindControl("Label2")).Text, HiddenFieldlegajo.Value.ToString());

        }
    }
}