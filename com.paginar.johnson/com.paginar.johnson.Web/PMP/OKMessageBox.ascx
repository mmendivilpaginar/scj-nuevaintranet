﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OKMessageBox.ascx.cs" Inherits="com.paginar.johnson.Web.PMP.pmp_OKMessageBox" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<style type="text/css">


</style>

<script type="text/javascript">
    function fnClickOK(sender, e) {
        __doPostBack(sender, e);
        //         if (typeof (window.opener) != 'undefined')
        //         { if (typeof (window.opener.__doPostBack) != 'unknown') window.opener.__doPostBack('close', ''); }
        //        window.close();

    }
</script>
<cc1:ModalPopupExtender ID="mpext" runat="server" BackgroundCssClass="modalBackground"
    TargetControlID="pnlPopup" PopupControlID="pnlPopup">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlPopup" CssClass="OkMessage" runat="server" Style="display:none;" DefaultButton="btnOk" >
      <asp:Label ID="lblCaption" runat="server"></asp:Label>
      <asp:Label ID="lblMessage" runat="server"></asp:Label>
      <br /><br />
      <span class="button">
         <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="formSubmit" OnClick="btnOk_Click" />
      </span>         
</asp:Panel>


