﻿<%@ Page Theme="PMP" Title=""  Language="C#" MasterPageFile="~/PMP/MasterPages/MP.Master" AutoEventWireup="true" CodeBehind="Form_PmpEnevaluador.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Form_PmpEnevaluador" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="OKMessageBox.ascx" TagName="OKMessageBox" TagPrefix="uc1" %>
<%@ Register src="WebUserControlGuardadoPrevio.ascx" tagname="WebUserControlGuardadoPrevio" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            revisarSemaforoComentario();
            revisarSemaforo();
            revisarSemaforoObjetivos();
            $('[id$=TextBoxComentarioJT]').blur(function () {
                revisarSemaforoComentario();
            });
        });
        window.onbeforeunload = confirmOnClose;
        /*
        var inFormOrLink;
        inFormOrLink = false;
        $(window).bind("beforeunload", function () {
        return inFormOrLink || (confirm("Si el formulario no fue guardado es probable que pierda los datos"));
        })

        */

        //Cieerre de X (2), Cierre Guardar sin enviar (1), Cierre por envío a evaluador (3)

        var flagCierre = 2;

        function confirmOnClose() {
            if (flagCierre == 1) {
                flagCierre = 2;
                return "Usted retornará al Panel de Control."; //Cierre Guardar sin enviar 1
            }
            else {
                if (flagCierre == 2) {
                    return 'Si el formulario no fue guardado es probable que pierda los datos'; // Cieerre de X
                }
            }
        }

        function GuardarSinEnviar() {
            flagCierre = 1;
            window.close();
        }


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH1" runat="Server">
    <asp:HiddenField ID="HiddenFieldPeriodoID" Value="1" runat="server" />

   <script type="text/javascript">
//       window.onbeforeunload = function (evt) {
//           var message = 'El Evaluado seleccionado quedará sin evaluación.';
//           var LegajoEvaluador = document.getElementById('<%= HiddenFieldLegajoEvaluador.ClientID %>').value;
//           if (LegajoEvaluador == '') return;
//           if (typeof evt == 'undefined') {
//               evt = window.event;
//           }
//           if (evt) {
//               evt.returnValue = message;
//           }
//           return message;
//       }


       ///
       var FlagCerrado = 0;
       function formIsDirty(form) {
           for (var i = 0; i < form.elements.length; i++) {
               var element = form.elements[i];
               var type = element.type;
               if (type == "checkbox" || type == "radio") {
                   if (element.checked != element.defaultChecked) {
                       return true;
                   }
               }
           }
           return false;
       }


       ///        

       var ModalProgress = '<%= ModalProgress.ClientID %>';
       var UpdateProg1 = '<%= UpdateProg1.ClientID %>';
       Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);

       Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
       var postbackElement;
       function beginReq(sender, args) {
           // muestra el popup
//           var i = 0;
//           postbackElement = args.get_postBackElement();
//           if (typeof (postbackElement) == "undefined") {
//               return;
//           }
//           if (!(args._postBackElement.id == '<%= WUCGuardadoPrevio.ClientID %>' + '_TimerPMP'))
//           //$find(ModalProgress).show();
//           //comment this line to hide modal progress form auto save
//               return;
           //               //aqui

           if (args.get_postBackElement() != null) {
               $find(ModalProgress).show();
               $('#' + UpdateProg1).show();
           }
       }

       function endReq(sender, args) {

           //  esconde el popup

           //$find(ModalProgress).hide();

           if (args.get_error() != undefined) {
               var errorMessage;
               if (args.get_response().get_statusCode() == '200') {
                   errorMessage = args.get_error().message;
               }
               else {
                   alert(args.get_error().message);
                   errorMessage = 'An unspecified error occurred. ';
               }
               args.set_errorHandled(true);
               alert(errorMessage);
           }

       }

       function ClickEnvio() {
           
           if ( $("[id*='hdConfirma']").val()=="true") { 
               if (Page_ClientValidate('EnviarEvaluado')) {
                   var VarChequeaComentario = CheckComentario('ctl00_CPH1_FormViewFormulario_TextBoxComentarioJT');
                   return VarChequeaComentario;
               }
           }
           else
               return false;
       }






       function ValidarTextBoxFundamentacion(sender, args) {

           var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
           var TextOpcional = "Puede fundamentar la fortaleza indicada";
           args.IsValid = true;

           var RBMB = sender.attributes["RBMB"].value;
           var RBB = sender.attributes["RBB"].value;
           var RBR = sender.attributes["RBR"].value;
           var RBNS = sender.attributes["RBNS"].value;
           var TextBoxFundamentacion = sender.attributes["TextBoxFundamentacion"].value;
           //alert(RBMB);
           if ($(RBR).is(':checked') || $(RBNS).is(':checked')) {
               if (($(TextBoxFundamentacion).val() == "") || ($(TextBoxFundamentacion).val() == TextRequerido) || ($(TextBoxFundamentacion).val() == TextOpcional)) {
                   args.IsValid = false;
               }

           }
       }

       function ValidarRadiosEnGrillaCompetencias(sender, args) {
           args.IsValid = ValidarRadiosEnGrilla("<%= GridViewCompetencias.ClientID %>");
       }
       function ValidarRadiosEnGrillaAspectos(sender, args) {
           args.IsValid = ValidarRadiosEnGrilla("<%= GridViewAspectos.ClientID %>");
       }
       function SetearPagina() {
           if (confirm("¿Confirma que desea enviar la evaluación al Evaluado?'")) {

               FlagCerrado = 2;
               //if (!Page_ClientValidate('EnviarEvaluado')) {
               SetearGrilla("<%= GridViewCompetencias.ClientID %>");
               SetearGrilla("<%= GridViewAspectos.ClientID %>");
               //code that need to be executed if page is validated
               //}            
               document.body.onbeforeunload = '';
               $("[id*='hdConfirma']").val("true"); 
           }
           else
               $("[id*='hdConfirma']").val("false"); 
       }
       function refreshParent() {
           if (!window.opener.closed) {
               //window.opener.refreshParent()
               window.opener.__doPostBack('ctl00_ContentPlaceHolder1_BtnBuscar', '');
           }

           if (window.opener.progressWindow) {
               window.opener.progressWindow.close()
           }
           window.close();
       }

       function confirmOnClose() {

           if (FlagCerrado != 2)
               return 'Si el formulario no fue guardado es probable que pierda los datos';
           
        }

       // window.onbeforeunload = confirmOnClose;

        function checkLogros()
        {
            
        }

        function ImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
            popup('Impresion.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
        }

     

   </script>

   <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
   <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
   <asp:HiddenField ID="HiddenFieldPasoID" Value="" runat="server" />
   <asp:HiddenField ID="HiddenFieldLegajoEvaluador" Value="" runat="server" />   
   <asp:ValidationSummary ID="ValidationSummaryFormulario" runat="server" ValidationGroup="EnviarEvaluado"
      CssClass="mensaje error" />
   <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="GrupoImprimir" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:WebUserControlGuardadoPrevio EnabledTimer="true" ID="WUCGuardadoPrevio" 
        runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
   <br />
   <h2 class="hide">Evaluación</h2>
 
   <div id="tabs">
      <ul>
         <li id="t1"><a href="#tabs-1">Competencias</a></li>
         <li id="t2"><a href="#tabs-2">Objetivos</a></li>
         <li id="t3"><a href="#tabs-3">Áreas de interés</a></li>
         <li id="t4"><a href="#tabs-4">Comentarios</a></li>
      </ul>
      <div id="tabs-1">
         <h3>Competencias</h3>
         <asp:FormView ID="FormViewPeriodo" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
            Width="100%">
            <ItemTemplate>
               <p class="descripcion">
                  Seleccionar en que Grado considera que posee las habilidades definidas, 
                  de acuerdo al desempeño del evaluado en el período
                  (<asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                  -
                  <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />)
               </p>
            </ItemTemplate>
         </asp:FormView>
         <asp:ObjectDataSource ID="ObjectDataSourcePeriodo" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetPeriodo" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         <div class="definicion-grado">
            <h4>Definición del Grado</h4>
            <br />
            <ul>
               <li><strong>Muy Bueno:</strong> El desempeño excede claramente los requerimientos para
                  la tarea.</li>
               <li><strong>Bueno:</strong> El desempeño es acorde a los requerimientos de la tarea.</li>
               <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Posee un buen desempeño, pero sin alcanzar el nivel esperado.</li>
               <li><strong>No Satisfactorio:</strong> El desempeño no satisface los requerimientos
                  mínimos para las tareas.</li>
            </ul>
         </div>
         <asp:GridView ID="GridViewCompetencias" runat="server" AutoGenerateColumns="False"
            DataKeyNames="ItemEvaluacionID" DataSourceID="ObjectDataSourceCompetencias" OnRowDataBound="GridViewCompetencias_RowDataBound">
            <Columns>
               <asp:TemplateField HeaderText="Competencias">
                  <ItemTemplate>
                    <div class="titulo">

                            <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>&nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink1" CssClass="masInfo" Visible='<%# Eval("Descripcion").ToString()!= "" %>' runat="server" ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>
                    </div>
                     <div class="titulo">
                        <%--<asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>--%>
                     </div>
                     <div class="detalle">
                        <%--<asp:Label ID="Label2" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>--%>
                        <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                     </div>
                  </ItemTemplate>
                  <ItemStyle CssClass="columnEmphasis" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Muy Bueno">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBMB" runat="server" rel="logroMB" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),4) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Bueno">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBB" runat="server" rel="logroBU" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),3) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Regular">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBR" runat="server" rel="logroRC" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),2) %>' />
                  </ItemTemplate>
                  <HeaderTemplate>
                  <asp:Label runat="server" ID="LableHeaderRegular" Text="Regularmente<br/>Cumple/<br/>Necesita<br/>desarrollarse"></asp:Label>
                  </HeaderTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="No Satisfactorio">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBNS" runat="server" rel="logroNS" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),1) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Fundamentación">
                  <ItemTemplate>
                     <asp:TextBox ID="TextBoxFundamentacion" runat="server" Text='<%# Bind("Fundamentacion") %>'
                        TextMode="MultiLine" MaxLength="5" Width="250px"></asp:TextBox>
                     <asp:CustomValidator ID="CustomValidatorTextBoxFundamentacion" runat="server" Text="*"
                        ClientValidationFunction="ValidarTextBoxFundamentacion" ValidationGroup="EnviarEvaluado"></asp:CustomValidator>
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Left" Width="300px" />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                  PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
      </div>
      <div id="tabs-2">
         <h3>Objetivos</h3>
         <asp:FormView ID="FormView1" runat="server" DataKeyNames="PeriodoID" DataSourceID="ObjectDataSourcePeriodo"
            Width="100%">
            <ItemTemplate>
               <p class="descripcion">
                  Seleccionar en que Grado considera que alcanzó los objetivos requeridos, 
                  en función del desempeño del evaluado en el período
                  (<asp:Literal ID="Label3" runat="server" Text='<%# Bind("EvaluadoDesde","{0:dd/MM/yyyy}") %>' />
                  -
                  <asp:Literal ID="Label4" runat="server" Text='<%# Bind("EvaluadoHasta","{0:dd/MM/yyyy}") %>' />)
               </p>
            </ItemTemplate>
         </asp:FormView>
         <div class="definicion-grado">
            <h4>Definición del Grado</h4>
            <br />
            <ul>
               <li><strong>Muy Bueno:</strong> Los objetivos son cumplidos con naturalidad y exceden constantemente las expectativas. Las acciones de esta persona aparecen ante los demás como un modelo para la organización.
</li>
               <li><strong>Bueno:</strong> Conoce y domina sus objetivos. La mayoría de las veces se cumplen los objetivos y los aportes individuales son positivos y evidentes. </li>
                <li><strong>Regularmente Cumple / Necesita desarrollarse:</strong> Algunos objetivos no son cumplidos. Conoce lo que se espera de su contribución pero, a pesar de su esfuerzo, no cumple totalmente con su rol requerido.  </li>
               <li><strong>No Satisfactorio:</strong> No conoce o no domina correctamente lo que se espera de su contribución. Hay diferencias notables entre objetivos y desempeño. </li>
            </ul>
         </div>
         <asp:GridView ID="GridViewAspectos" runat="server" AutoGenerateColumns="False" DataKeyNames="ItemEvaluacionID"
            DataSourceID="ObjectDataSourceAspectos" OnRowDataBound="GridViewAspectos_RowDataBound">
            <Columns>
               <asp:TemplateField HeaderText="Objetivos">
                  <ItemTemplate>
                     <div class="titulo">
                        
                            <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>&nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink1" CssClass="masInfo" runat="server" Visible='<%# Eval("Descripcion").ToString()!= "" %>' ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />")%>'>
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>

                        <%--<asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>--%>
                     </div>
                     <div class="detalle">
                        <%--<asp:Label ID="LabelDescripcion" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>--%><asp:HiddenField
                           ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                     </div>
                  </ItemTemplate>
                  <ItemStyle CssClass="columnEmphasis" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Muy Bueno">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBMB" runat="server" rel="objMB" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),4) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Bueno">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBB" runat="server" rel="objBU" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),3) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Regular">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBR" runat="server" rel="objRC" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),2) %>' />
                  </ItemTemplate>
                  <HeaderTemplate>
                  <asp:Label runat="server" ID="LableHeaderRegular" Text="Regularmente <br/> Cumple/ <br/> Necesita <br/>desarrollarse"></asp:Label>
                  </HeaderTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="No Satisfactorio">
                  <ItemTemplate>
                     <asp:RadioButton ID="RBNS" runat="server" rel="objNS" GroupName='<%# Eval("ItemEvaluacionID") %>'
                        Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),1) %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="85px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Fundamentación">
                  <ItemTemplate>
                     <asp:TextBox ID="TextBoxFundamentacion" runat="server" Text='<%# Bind("Fundamentacion") %>'
                        TextMode="MultiLine" MaxLength="5" Width="250px"></asp:TextBox>
                     <asp:CustomValidator ID="CustomValidatorTextBoxFundamentacion" runat="server" Text="*"
                        ClientValidationFunction="ValidarTextBoxFundamentacion" ValidationGroup="EnviarEvaluado"></asp:CustomValidator>
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Left" Width="300px" />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceAspectos" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:Parameter DefaultValue="2" Name="SeccionID" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                  PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
      </div>
      <div id="tabs-3">
         <h3>Áreas de interés del evaluado <span>(área a ser completada por el evaluado, opcional)</span></h3>
         
         <asp:GridView ID="GVAO" runat="server" AutoGenerateColumns="False" DataKeyNames="AreaDeOperacionID"
            DataSourceID="ObjectDataSourceAreasDeOperaciones">
            <Columns>
               <asp:TemplateField>
                  <ItemTemplate>
                     <asp:CheckBox Enabled="false" ID="CheckBoxAreaDeOperacion" runat="server" Checked='<%# Bind("Chequeado") %>' />
                     <asp:HiddenField ID="HiddenFieldAreaDeOperacionID" runat="server" Value='<%# Eval("AreaDeOperacionID") %>' />
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" Width="45px" />
               </asp:TemplateField>
               <asp:BoundField DataField="Descripcion" HeaderText="Área  de Operaciones" SortExpression="Descripcion">
                  <ItemStyle Width="140px" Font-Bold="true" />
               </asp:BoundField>
               <asp:TemplateField HeaderText="Observaciones - Roles y Funciones">
                  <ItemTemplate>
                      <asp:Label ID="LabelObservacion" runat="server" Text='<%# Eval("Observacion") %>'></asp:Label>
                     <%--<asp:TextBox Enabled="false" Text='<%# Bind("Observacion") %>' ID="TextBoxObservacion"
                        runat="server" TextMode="MultiLine" Width="660px"></asp:TextBox>--%>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:ObjectDataSource ID="ObjectDataSourceAreasDeOperaciones" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetAreasOperaciones" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
      </div>
      <div id="tabs-4">
         <h3>Comentarios</h3>
         <asp:FormView ID="FormViewFormulario" runat="server" DataKeyNames="Legajo,PeriodoID,TipoFormularioID"
            DataSourceID="ObjectDataSourceFormulario" Width="100%" 
              OnDataBound="FormViewFormulario_DataBound" 
             >
            <ItemTemplate>
               <table class="GridView">
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Evaluador<%--</div>
                        <div class="detalle">--%>&nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink1" CssClass="masInfo" runat="server" ToolTip="Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación recomendada, etc.">
                            <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>                           
                        </div>
                     </td>
                     <td>
                        <asp:TextBox ID="TextBoxComentarioJT" rel="comenEvaluador" onChange="revisarSemaforoComentario()" runat="server" TextMode="MultiLine" Text='<%# Eval("ComentarioJefeTurno") %>'
                           Width="600px"></asp:TextBox>
                     </td>
                  </tr>
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Evaluado<%--</div>
                        <div class="detalle">--%>&nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink2" CssClass="masInfo" runat="server" ToolTip="Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su rol actual, su relación con sus compañeros y supervisores. Como también, observaciones útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la Compañía.">
                            <asp:Image ID="Image2" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>  
                           
                        </div>
                     </td>
                     <td>
                        <asp:Label ID="LabelComentarioEv" runat="server" Text='<%# DefaultVal(Eval("ComentarioEvaluado")) %>'></asp:Label>
                        <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioEv" runat="server" TextMode="MultiLine" Width="600px" Text='<%# Bind("ComentarioEvaluado") %>'></asp:TextBox>               --%>
                     </td>
                     <%-- <td style="width:1%">
                    &nbsp;</td>--%>
                  </tr>
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Auditor<%--</div>
                        <div class="detalle">--%>&nbsp;&nbsp;
                        <asp:HyperLink ID="HyperLink3" CssClass="masInfo" runat="server" ToolTip="Observaciones adicionales.">
                            <asp:Image ID="Image3" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>  
                           </div>
                     </td>
                     <td>
                        <asp:Label ID="LabelComentarioJP" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta")) %>'></asp:Label>
                        <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# Bind("ComentarioJefeDePlanta") %>' runat="server" TextMode="MultiLine" Width="600px"></asp:TextBox>--%>
                     </td>
                     <%--<td style="width:1%">
                    &nbsp;</td>--%>
                  </tr>
               </table>
            </ItemTemplate>
            <InsertItemTemplate>
               <table class="GridView">
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Evaluador</div>
                        <div class="detalle">
                           Aspectos acerca del evaluado que a su juicio merezcan ser destacados, capacitación
                           recomendada, etc.
                        </div>
                     </td>
                     <td>
                        <asp:TextBox ID="TextBoxComentarioJT" runat="server" TextMode="MultiLine" Text='<%# Eval("ComentarioJefeTurno") %>'
                           Width="600px"></asp:TextBox>
                     </td>
                  </tr>
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Evaluado</div>
                        <div class="detalle">
                           Puede comentar aquí cuál es el grado de satisfacción por la tarea empleada en su
                           rol actual, su relación con sus compañeros y supervisores. Como también, observaciones
                           útiles para ser tenidas en cuenta en su desarrollo personal y profesional en la
                           Compañía.
                        </div>
                     </td>
                     <td>
                        <asp:Label ID="LabelComentarioEv" runat="server" Text='<%# DefaultVal(Eval("ComentarioEvaluado")) %>'></asp:Label>
                        <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioEv" runat="server" TextMode="MultiLine" Width="600px" Text='<%# Bind("ComentarioEvaluado") %>'></asp:TextBox>               --%>
                     </td>
                  </tr>
                  <tr>
                     <td class="columnEmphasis">
                        <div class="titulo">
                           Auditor</div>
                        <div class="detalle">
                           Observaciones adicionales</div>
                     </td>
                     <td>
                        <asp:Label ID="LabelComentarioJP" runat="server" Text='<%# DefaultVal(Eval("ComentarioJefeDePlanta")) %>'></asp:Label>
                        <%--<asp:TextBox Enabled="false" ID="TextBoxComentarioJP" Text='<%# Bind("ComentarioJefeDePlanta") %>' runat="server" TextMode="MultiLine" Width="600px"></asp:TextBox>--%>
                     </td>
                  </tr>
               </table>
            </InsertItemTemplate>
         </asp:FormView>
         <asp:ObjectDataSource ID="ObjectDataSourceFormulario" runat="server" DeleteMethod="Delete"
            InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByID"
            TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.FormularioPmpOperariosTableAdapter"
            UpdateMethod="Update">
            <DeleteParameters>
               <asp:Parameter Name="Original_Legajo" Type="Int32" />
               <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
               <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
               <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
               <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
               <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
               <asp:Parameter Name="Original_FAlta" Type="DateTime" />
            </DeleteParameters>
            <UpdateParameters>
               <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
               <asp:Parameter Name="ComentarioEvaluado" Type="String" />
               <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
               <asp:Parameter Name="FAlta" Type="DateTime" />
               <asp:Parameter Name="Original_Legajo" Type="Int32" />
               <asp:Parameter Name="Original_PeriodoID" Type="Int32" />
               <asp:Parameter Name="Original_TipoFormularioID" Type="Int32" />
               <asp:Parameter Name="Original_ComentarioJefeTurno" Type="String" />
               <asp:Parameter Name="Original_ComentarioEvaluado" Type="String" />
               <asp:Parameter Name="Original_ComentarioJefeDePlanta" Type="String" />
               <asp:Parameter Name="Original_FAlta" Type="DateTime" />
            </UpdateParameters>
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldlegajo" DefaultValue="5336" Name="Legajo"
                  PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="1" Name="PeriodoID"
                  PropertyName="Value" Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" DefaultValue="1" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
            </SelectParameters>
            <InsertParameters>
               <asp:Parameter Name="Legajo" Type="Int32" />
               <asp:Parameter Name="PeriodoID" Type="Int32" />
               <asp:Parameter Name="TipoFormularioID" Type="Int32" />
               <asp:Parameter Name="ComentarioJefeTurno" Type="String" />
               <asp:Parameter Name="ComentarioEvaluado" Type="String" />
               <asp:Parameter Name="ComentarioJefeDePlanta" Type="String" />
            </InsertParameters>
         </asp:ObjectDataSource>
      </div>
   </div>
   <!--/tabs-->
   <asp:CustomValidator ID="CustomValidatorGrillaCompetencias" ClientValidationFunction="ValidarRadiosEnGrillaCompetencias"
      runat="server" ErrorMessage="Faltan Seleccionar Items en la Grilla de Competencias"
      ValidationGroup="EnviarEvaluado">&nbsp;</asp:CustomValidator>
   <asp:CustomValidator ID="CustomValidatorGrillaAspectos" ClientValidationFunction="ValidarRadiosEnGrillaAspectos"
      runat="server" ErrorMessage="Faltan Seleccionar Items en la Grilla de Objetivos"
      ValidationGroup="EnviarEvaluado">&nbsp;</asp:CustomValidator>
   <asp:Panel Height="50px" Width="125px" Style="display: none" CssClass="mensaje exito"
      ID="PanelExito" runat="server">
      El Formulario fue enviado exitosamente
   </asp:Panel>
   <div id="acciones">
       

     <%-- <span class="button">
          <asp:UpdatePanel ID="UpdatePanelExportar" runat="server">
            <ContentTemplate>
                <asp:Button ID="ButtonExportar" 
                    ToolTip="Visualiza el formulario en modo de Exportación con los datos volcados en la base de datos"  
                    runat="server" Text="Exportar" CssClass="formSubmitAsync" EnableTheming="false" 
                    onclick="ButtonExportar_Click" />
            </ContentTemplate>           
          </asp:UpdatePanel>
      </span>--%>
            <asp:HiddenField runat="server" ID="hdConfirma"  Value="true"/>
            
            <span class="button">
            <asp:UpdatePanel ID="UpdatePanelEnviar" runat="server" RenderMode="Inline">
            <ContentTemplate>
               
                    <asp:Button ID="ButtonEnviarEvaluado" OnClientClick="SetearPagina();" runat="server"
                     Text="Enviar al Evaluado" ValidationGroup="EnviarEvaluado" 
                    OnClick="ButtonEnviarEvaluado_Click" CssClass="formSubmitAsync" 
                    EnableTheming="false" 
                    ToolTip="Guarda la Evaluación dejando la misma disponible para el Evaluado. El Evaluador no puede realizar más cambios" />
                    <%--<uc1:OKMessageBox ID="OKMessageBox1" runat="server"  />--%>
               
            </ContentTemplate>
         </asp:UpdatePanel>  
         </span>
         <asp:UpdatePanel ID="UpdatePanelDraft" runat="server" RenderMode="Inline">
            <ContentTemplate>
               <span class="button">
                  <asp:Button ID="ButtonGSinEnviar" runat="server" Text="Guardar sin enviar" 
                     OnClick="ButtonGSinEnviar_Click" CssClass="formSubmitAsync" 
                     EnableTheming="false" 
                     ToolTip="Guarda la evaluación sin enviar la misma al Evaluado" OnClientClick="" />
               </span>
               </ContentTemplate>
            </asp:UpdatePanel>
           <span class="button">
             <asp:UpdatePanel ID="UpdatePanelImprimir" runat="server">
                <ContentTemplate>
                   <asp:Button ID="ButtonImprimir" runat="server" Text="Imprimir/Exportar" 
                        ValidationGroup="GrupoImprimir" CssClass="formSubmitAsync"           
                            ToolTip="Visualiza el formulario en modo de Impresión con los datos volcados en la base de datos" 
                            onclick="ButtonImprimir_Click" EnableTheming="false" />
                </ContentTemplate>
             </asp:UpdatePanel>
          </span>
         <asp:UpdatePanel ID="UpdatePanelExito" runat="server">
            <ContentTemplate>
               <uc1:OKMessageBox ID="omb" runat="server" />
            </ContentTemplate>
            <Triggers>
               <asp:AsyncPostBackTrigger ControlID="ButtonEnviarEvaluado" EventName="Click" />
            </Triggers>
         </asp:UpdatePanel>
      
      
      
   </div><!--/acciones-->
   
   <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
      <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanelDraft">
         <ProgressTemplate>
            <div class="precarga">
               <asp:Image AlternateText="Procesando" ID="ImgProc" ImageUrl="~/pmp/img/ajax-loader.gif" runat="server" />
               <p>Operación en progreso...</p>
            </div>               
         </ProgressTemplate>
      </asp:UpdateProgress>
   </asp:Panel>
   <cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
      BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress"  />
   <asp:HiddenField ID="HiddenFieldJS" runat="server" />  
   
   
    
   
   
</asp:Content>