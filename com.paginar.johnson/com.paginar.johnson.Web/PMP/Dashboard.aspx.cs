﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Dashboard : System.Web.UI.Page
    {
        public String ComentarioJefeTurno;

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }


        protected void Page_Prerender(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                HiddenFieldLegajoEvaluador.Value = UE.legajo.ToString();
                LabelNombreUsuarioActual.Text = UE.GetUsuarioNombre(int.Parse(HiddenFieldLegajoEvaluador.Value));
                DropDownListPeriodos.Attributes.Add("onChange", string.Format("$('#{0}').attr('disabled', 'disabled')", ButtonEvaluar.ClientID)); ;

            }
            if (Request.Params.Get("__EVENTTARGET") == "ActGrilla")
            {
                GridViewFormularios.DataBind();
                DropDownListUsuarios.DataBind();
            }



        }

        private void VerificarPanelIniciarEvaluacion()
        {
            bool carga = false;

            carga = ValidaPeriodo(int.Parse(DropDownListPeriodos.SelectedValue));
            if (carga)
            {
                EjecutarScript("$(document).ready(function() {" + string.Format("$('#{0}').attr('disabled', '');", ButtonEvaluar.ClientID) + string.Format("$('#{0}').show();", PanelIniciarEvaluaciones.ClientID) + "});");
            }
            else
            {
                EjecutarScript("$(document).ready(function() {" + string.Format("$('#{0}').attr('disabled', '');", ButtonEvaluar.ClientID) + string.Format("$('#{0}').hide();", PanelIniciarEvaluaciones.ClientID) + "});");
            }
        }
        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            if (!ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack)
                GridViewFormularios.DataBind();
            VerificarPanelIniciarEvaluacion();
        }

        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (val.ToString());

        }

        protected string DefaultValComentarios(object val)
        {
            //if ((val == System.DBNull.Value) || (val == string.Empty))
            //    return ("S/C");

            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return "S/C";
            else
            {
                int Maximalongitud = 10;
                string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

                return Texto;
            }

        }

        private string GetToolTipComentario(string valor)
        {
            int Maximalongitud = 10;
            string Texto = (valor.ToString().Trim().Length > Maximalongitud) ? (valor.ToString().Substring(1, Maximalongitud) + "...") : valor.ToString();
            Texto = "<a class='tooltip' href='#'>" + Texto + "<span><strong>Evaluador:</strong><table><tr><td>aaa</td></tr><tr><td>" + valor + "</td></tr></table></span></a>";

            return Texto;
        }


        protected void GridViewFormularios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                Label LinkButtonEstado = e.Row.FindControl("LinkButtonEstado") as Label;
                HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
                ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
                CheckBox CheckBoxSinEvaluacion = e.Row.FindControl("CheckBoxSinEvaluacion") as CheckBox;
                TextBox TextBoxComentarioJefe = e.Row.FindControl("TextBoxComentarioJefe") as TextBox;
                HtmlControl LinkSinComentarioJefeTurno = e.Row.FindControl("LinkSinComentarioJefeTurno") as HtmlControl;
                HtmlControl LinkComentarioJefeTurno = e.Row.FindControl("LinkComentarioJefeTurno") as HtmlControl;
                ComentarioJefeTurno = DataBinder.Eval(e.Row.DataItem, "ComentarioJefeTurno").ToString();
                string ComentarioJefePlanta = DataBinder.Eval(e.Row.DataItem, "ComentarioJefeDePlanta").ToString();
                int EstadoID;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "EstadoID").ToString(), out EstadoID);
                string UrlForm = string.Empty;
                FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();

                FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(DropDownListPeriodos.SelectedValue), int.Parse(HiddenFieldTipoFormularioID.Value));
                if (RelPasosTipoFormulariosDt.Rows.Count > 0)
                {
                    FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                    UrlForm = RelPasosTipoFormulariosRow.FormAspx;
                }

                bool carga = false;

                if (ViewState["HabilitaPeriodo"] != null)
                    carga = (bool)ViewState["HabilitaPeriodo"];
                else
                    carga = ValidaPeriodo(int.Parse(DropDownListPeriodos.SelectedValue));

                if (carga)
                {
                    if (HiddenFieldEstadoID.Value.Trim() == "4")
                        ImageCommand.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                        ImageCommand.Attributes.Add("onclick", string.Format("popup('" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                }
                else//Sino no esta en el periodo de carga, tengo q abrir impresion
                    ImageCommand.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));

                if (HiddenFieldTipoFormularioID.Value.Trim() == "8")
                {
                    if ((HiddenFieldEstadoID.Value.Trim() == "1") && carga)
                    {
                        ImageCommand.ImageUrl = "~/pmp/img/editar.jpg";
                        ImageCommand.ToolTip = "Editar Evaluación";
                    }
                    else
                    {
                        ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                        ImageCommand.ToolTip = "Consultar evaluación en modo sólo lectura";
                    }
                }
                else
                {
                    if (((HiddenFieldEstadoID.Value.Trim() == "1") || ((HiddenFieldEstadoID.Value.Trim() == "-1"))) && carga)
                    {
                        ImageCommand.ImageUrl = "~/pmp/img/editar.jpg";
                        ImageCommand.ToolTip = "Editar Evaluación";
                    }
                    else
                    {
                        ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                        ImageCommand.ToolTip = "Consultar evaluación en modo sólo lectura";
                    }

                }

                if (HiddenFieldEstadoID.Value.Trim() == "-1")
                {
                    LinkButtonEstado.Text = "No Iniciado";
                }


                TextBoxComentarioJefe.Attributes.Add("style", "display:none;");
                if (string.IsNullOrEmpty(ComentarioJefeTurno) || (EstadoID == 1 && string.IsNullOrEmpty(ComentarioJefePlanta)))
                {
                    LinkSinComentarioJefeTurno.Attributes.Add("style", "display:block;");
                    CheckBoxSinEvaluacion.Attributes.Add("onclick", string.Format("Toggle('{0}', '{1}', '{2}');", TextBoxComentarioJefe.ClientID, LinkSinComentarioJefeTurno.ClientID, CheckBoxSinEvaluacion.ClientID));
                }

                else
                {
                    CheckBoxSinEvaluacion.Attributes.Add("onclick", string.Format("Toggle('{0}','{1}' , '{2}');", TextBoxComentarioJefe.ClientID, LinkComentarioJefeTurno.ClientID, CheckBoxSinEvaluacion.ClientID));
                    LinkComentarioJefeTurno.Attributes.Add("style", "display:block;");
                }
                if (EstadoID == 5)
                    CheckBoxSinEvaluacion.Enabled = false;


            }
        }
        protected void ButtonEvaluar_Click(object sender, EventArgs e)
        {
            if ((Page.IsValid))
            {
                HiddenFieldLegajoEvaluacion.Value = DropDownListUsuarios.SelectedValue;
                HiddenFieldPeriodoIDEvaluacion.Value = DropDownListPeriodos.SelectedValue;
                HiddenFieldTipoFormularioIDEvaluacion.Value = HiddenFieldTipoFormularioID.Value;

                int LegajoEvaluacion = Convert.ToInt32(HiddenFieldLegajoEvaluacion.Value);
                int PeriodoIDEvaluacion = Convert.ToInt32(HiddenFieldPeriodoIDEvaluacion.Value);
                int TipoFormularioIDEvaluacion = Convert.ToInt32(HiddenFieldTipoFormularioIDEvaluacion.Value);
                int LegajoEvaluador = Convert.ToInt32(HiddenFieldLegajoEvaluador.Value);
                int PasoIDEvaluacion = Convert.ToInt32(HiddenFieldPasoID.Value);
                RepeaterTieneEvaluacion.DataBind();
                FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();
                string UrlForm = string.Empty;
                FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 1, int.Parse(DropDownListPeriodos.SelectedValue), int.Parse(HiddenFieldTipoFormularioID.Value));
                if (RelPasosTipoFormulariosDt.Rows.Count > 0)
                {
                    FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                    UrlForm = RelPasosTipoFormulariosRow.FormAspx;
                }
                if (RepeaterTieneEvaluacion.Items.Count == 0)
                {
                    DashboardController DC = new DashboardController();

                    FormulariosDS.RelEvaluadosEvaluadoresDataTable DTEvaluador = new FormulariosDS.RelEvaluadosEvaluadoresDataTable();
                    FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByID(DTEvaluador, LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion, 1);
                    FormulariosDS.RelEvaluadosEvaluadoresDataTable DTAuditor = new FormulariosDS.RelEvaluadosEvaluadoresDataTable();
                    FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByID(DTAuditor, LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion, 1);

                    if (DTEvaluador.Rows.Count == 0 || DTAuditor.Rows.Count == 0)
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "OpenWin", string.Format("alert('{0}');", "El operario que está intentando agregar no tiene configurado su evaluador ni su auditor"), true);
                    else
                        EjecutarScript(string.Format("popup('" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}&LegajoEvaluador={3}',1000,600);", LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion, LegajoEvaluador));
                    //if (!DC.ExisteEvaluado(LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion))
                    //{
                    //    DC.CrearEvaluado(LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion);
                    //}
                    //DC.AsociarEvaluacion(LegajoEvaluacion, PeriodoIDEvaluacion, TipoFormularioIDEvaluacion, PasoIDEvaluacion, LegajoEvaluador);

                    //GridViewFormularios.DataBind();
                    //DropDownListUsuarios.DataBind();
                }
                else
                {

                    Literal LabelJefe = RepeaterTieneEvaluacion.Items[0].FindControl("UsuarioLabel") as Literal;
                    if (LabelJefe != null)
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "OpenWin", string.Format("alert('{0}');", "El operario ya cuenta con una Evaluación en curso. El Evaluador a cargo es " + LabelJefe.Text), true);

                }
            }

        }


        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IF100", js, true);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", js, true);
        }

        protected void CustomValidatorTieneEvaluacion_ServerValidate(object source, ServerValidateEventArgs args)
        {

        }
        protected void DropDownListPeriodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            VerificarPanelIniciarEvaluacion();

        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }

        protected void cvPeriodo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            FormulariosController f = new FormulariosController();
            args.IsValid = f.ValidarPeriodoCarga(int.Parse(DropDownListPeriodos.SelectedValue), DateTime.Now).GetValueOrDefault();
        }
        protected void ObjectDataSourceFormularios_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            e.Cancel = sm.IsInAsyncPostBack;

        }


        protected void DropDownListUsuarios_PreRender(object sender, EventArgs e)
        {
            if (DropDownListUsuarios.Items.Count == 0)
                ButtonEvaluar.Enabled = false;
        }

        protected void BtnGrabar_Click(object sender, EventArgs e)
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            int LegajoEvaluador = 0;
            int EstadoID = 0;
            try
            {
                foreach (GridViewRow Fila in GridViewFormularios.Rows)
                {
                    CheckBox CheckBoxSinEvaluacion = Fila.FindControl("CheckBoxSinEvaluacion") as CheckBox;
                    if ((CheckBoxSinEvaluacion.Checked) && (CheckBoxSinEvaluacion.Enabled))
                    {
                        HiddenField HiddenFieldLegajo = (HiddenField)Fila.FindControl("HiddenFieldLegajo");
                        HiddenField HiddenFieldPeriodoID = (HiddenField)Fila.FindControl("HiddenFieldPeriodoID");
                        HiddenField HiddenFieldTipoFormularioID = (HiddenField)Fila.FindControl("HiddenFieldTipoFormularioID");
                        HiddenField HiddenFieldEstadoID = (HiddenField)Fila.FindControl("HiddenFieldEstadoID");

                        TextBox TextBoxComentarioJefe = (TextBox)Fila.FindControl("TextBoxComentarioJefe");

                        if (!string.IsNullOrEmpty(HiddenFieldLegajo.Value))
                            legajo = Convert.ToInt32(HiddenFieldLegajo.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                            PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                            TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
                            LegajoEvaluador = Convert.ToInt32(HiddenFieldLegajoEvaluador.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldEstadoID.Value))
                            EstadoID = Convert.ToInt32(HiddenFieldEstadoID.Value);

                        FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                        FC.SetComentarioJefeTurno(TextBoxComentarioJefe.Text);

                        FC.GrabarJP();
                        if (EstadoID < 1)
                            FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);
                        FC.SaveHistorial(5, PeriodoID, legajo, TipoFormularioID);



                    }
                }

                //LabelResultado.Text = "La operación fue realizada exitosamente";
                EjecutarScript(string.Format("alert('La operación fue realizada exitosamente.')"));
            }

            catch (Exception ex)
            {
                //LabelResultado.Text = "Se produjo un error. Intente nuevamente.";
                EjecutarScript(string.Format("alert('Ha ocurrido un error. Por favor intenta nuevamente.')"));

            }
            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected void GridViewFormularios_DataBound(object sender, EventArgs e)
        {
            btnGrabar.Visible = (GridViewFormularios.Rows.Count > 0);
        }
    }
}