﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="exportarWordMY.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.exportarWordMY" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
   <link rel="stylesheet" type="text/css" href="css/styles.css" />
   <link rel="stylesheet" type="text/css" href="css/print.css" />
   <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           //quitar bordes laterales
           $('.GridView tr th:first').css('border-left', '1px solid #fff');
           $('.GridView tr th:last').css('border-right', '1px solid #fff');
           $('.GridView tr').each(function () {
               $(this).find('td:first').css('border-left', '1px solid #fff');
               $(this).find('td:last').css('border-right', '1px solid #fff');
           });
       });
   </script>
   <style type="text/css">
    #foto,
    #foto img{
        border:0!important;
    }
   </style>   
</head>
<body>


<div id="export" runat="server">
   <div id="page">
      <div id="page-inner">

         <table width="100%">
            <tr>
            <td>
         <form id="form1" runat="server">
            <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
            <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
            <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         
         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" BorderStyle="Dotted">
            <ItemTemplate>
               <div id="header1">
                  <!--<div id="headerBG"></div>-->
                  <div id="header-inner">
                     <div id="logo-title">
                      <table width="100%"><tr>
                      <td>
                      <div id="site-name">
                           <h1>Evaluación de Desempeño <br /><span><asp:Literal ID="TipoFormularioLabel" runat="server" Text='<%# Bind("TipoFormulario") %>' /></span></h1>
                        </div>
                      </td>
                      <td>
                      <div id="logo" runat="server">
                           <img id="logo-image" alt="Logo Johnson " src="img/logoprint.jpg"/>
                         </div>
                      </td>
                      </tr>
                      </table>                                                
                     </div>
                     
                     <div id="datos">
                        <!--Fechas-->
                        <table cellpadding="5" cellspacing="5">
                            <tr>
                                <td><strong>Período de Evaluación:</strong> <asp:Literal ID="Label1" runat="server" Text='<%# Bind("Periodo") %>' /></td>
                                <td>&nbsp;</td>
                                <td><strong>Fecha de Inicio del PMP:</strong> <asp:Literal ID="EvaluacionFechaLabel" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>' /></td>
                            </tr>
                        </table>

                        <!--datos de Evaluador y Evaluado-->
                        
                        <table width="100%" style="border:1px solid #ccc;">
                           <tr>
                              <td valign="top">
                                 <div id="personas1">
                                    <div id="personas-inner1">
                                       <table width="100%" cellpadding="1" cellspacing="1">
                                          <tr>
                                             <td>
                                                <div class="evaluador1">
                                                   <h2>Evaluador</h2>
                                                   <table >
                                                      <tr>
                                                         <td valign="top"><strong>Nombre y Apellido: </strong><asp:Literal ID="EvaluadorNombreLabel" runat="server" Text='<%# Bind("EvaluadorNombre") %>' /></td>
                                                         <td valign="top"><strong>Legajo: </strong><asp:Literal ID="EvaluadorLegajoLabel" runat="server" Text='<%# Bind("EvaluadorLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top"><strong>Área: </strong><asp:Literal ID="Literal1" runat="server" Text='<%# Bind("EvaluadorArea") %>' /></td>
                                                         <td valign="top"><strong>Cargo: </strong><asp:Literal ID="Literal2" runat="server" Text='<%# Bind("EvaluadorCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                             
                                             <td>
                                                <div class="evaluado1">
                                                   <h2>Evaluado</h2>
                                                   <table>
                                                      <tr>
                                                         <td valign="top"><strong>Nombre y Apellido: </strong><asp:Literal ID="Label2" runat="server" Text='<%# Bind("EvaluadoNombre") %>' /></td>
                                                         <td valign="top"><strong>Legajo: </strong><asp:Literal ID="EvaluadoLegajoLabel" runat="server" Text='<%# Bind("EvaluadoLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td valign="top"><strong>Área: </strong><asp:Literal ID="EvaluadoSectorLabel" runat="server" Text='<%# Bind("EvaluadoArea") %>' /></td>
                                                         <td valign="top"><strong>Cargo: </strong><asp:Literal ID="Literal3" runat="server" Text='<%# Bind("EvaluadoCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>                              
                              </td>
                              <td valign="top">
                                 <div id="foto" runat="server" style="width:70px;height:70px">
                                    <asp:Image ID="Image1" Width="70px" Height="70px" ToolTip='<%# Bind("EvaluadoNombre") %>' AlternateText='<%# Eval("EvaluadoNombre") %>' ImageUrl='<%# (Eval("EvaluadoFoto", GetUrl("{0}")).Replace("~/", "")).Replace("PMP/","") %>' runat="server" />
                                 </div>                              
                              </td>
                           </tr>
                        </table>

                        <div id="calificacion1">
                           <h2><span id="SpanCalificacion"><%--<asp:Literal ID="TextCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' />--%></span></h2>
                        </div>
                     </div><!--/datos-->
                     
                     <div style="text-align: right; padding-top:15px; ">
                         <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                             Text="Exportar" />
                         &nbsp;<input type="button" value="Cerrar" onclick="window.close()" class="hidebutton" />
                     </div>
                     
                  </div><!--/header-inner-->
               </div><!--/header-->
               
               <!--INICIO - ModalPopup Evaluaciones anteriores-->
               <asp:Panel ID="PanelFA" CssClass="contentPopup" Style="display: none" runat="server">
                  <asp:Panel CssClass="titleModalPopup" ID="titleBarEvalAnt" runat="server">
                     <h2>Evaluaciones anteriores</h2>
                     <span id="btnOkay" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <ul>
                        <asp:Repeater ID="RFA" runat="server" DataSourceID="ObjectDataSourceFormulariosAnteriores">
                           <ItemTemplate>
                              <li>
                                 <asp:LinkButton Text='<%# Convert.ToString(Eval("Fecha","{0:dd/MM/yyyy}"))+" "+Convert.ToString(Eval("Calificacion")) %>'
                                    ID="LinkButtonFormu" runat="server">LinkButton</asp:LinkButton></li>
                           </ItemTemplate>
                           <AlternatingItemTemplate>
                           </AlternatingItemTemplate>
                           <FooterTemplate>
                              <asp:Label ID="lblEmpty" Text="El usuario no cuenta con evaluaciones anteriores" runat="server"
                                 Visible='<%# bool.Parse((((Repeater)FVC.FindControl("RFA")).Items.Count==0).ToString())%>'>
                              </asp:Label>
                           </FooterTemplate>
                        </asp:Repeater>
                     </ul>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceFormulariosAnteriores" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorialByLegajo" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" PropertyName="Value"
                           Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Evaluaciones anteriores-->
               <asp:Panel CssClass="contentPopup" ID="PanelH" Style="display: none" runat="server">
                  <asp:Panel ID="Panel1" CssClass="titleModalPopup" runat="server">
                     <h2>
                        Historial</h2>
                     <span id="btnOkayHistorial" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <asp:GridView ID="GridViewHistorial" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceHistorial">
                        <Columns>
                           <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" SortExpression="Estado" />
                           <asp:BoundField DataField="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha"
                              SortExpression="Fecha" />
                           <asp:BoundField DataField="Usuario" HeaderText="Usuario" ReadOnly="True" SortExpression="Usuario" />
                        </Columns>
                        <EmptyDataTemplate>
                           <asp:Label ID="Label3" runat="server" Text="El formulario actual no tiene registros en el historial."></asp:Label>
                        </EmptyDataTemplate>
                     </asp:GridView>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceHistorial" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorial" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                           PropertyName="Value" Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Historial-->
            </ItemTemplate>
         </asp:FormView>
         
         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         
         <div class="bloque1">
            <h3>Logros (1º Semestre)</h3>
            <br />
            <asp:Repeater ID="RepeaterLogros" runat="server"   DataSourceID="ObjectDataSourceLogros">
          <HeaderTemplate>
          <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
          </HeaderTemplate>
          <ItemTemplate>
          <tr><td><asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
          <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>' Visible="false"></asp:Label>
           <br/> </td></tr>
          <tr>
          <td><br />
          <%--<div id="personas">
              <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Width="854px" Height="100px"></asp:Label>
              
          </div> --%>
                <div id="comentarios">
                     <div class="item">                       
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentario" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                </div>
          
          

          </td>
          </tr>
          </ItemTemplate>
          <FooterTemplate> </table></FooterTemplate>
          </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceLogros" runat="server" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:Parameter DefaultValue="3" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                      Name="PeriodoID" PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>
           
         </div>
         <div class="separador1">
         </div>
          <div class="bloque1">
            <h3>Comportamientos </h3>            
            <br />
            <asp:Repeater ID="RepeaterComportamientos" runat="server"   DataSourceID="ObjectDataSourceComportamientos">
          <HeaderTemplate>
          <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
          </HeaderTemplate>
          <ItemTemplate>
          <tr><td><asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
          <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>' Visible="false"></asp:Label>
           <br/> </td></tr>
          <tr>
          <td><br />
          <%--<div id="personas">
              <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Width="100%"></asp:Label>
              
          </div> --%>

                <div id="comentarios">
                     <div class="item">                       
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentario" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                </div>

          

          </td>
          </tr>
          </ItemTemplate>
          <FooterTemplate> </table></FooterTemplate>
          </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceComportamientos" runat="server" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:Parameter DefaultValue="4" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                      Name="PeriodoID" PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>

         </div>
         <div class="separador1">
         </div>
         <div class="bloque1">
            <h3>Competencias</h3>
            <br />
            Leadership Imperatives            
            <br />
            <br />

            <asp:GridView ID="GridViewCompetencias" runat="server" AutoGenerateColumns="False"
               DataKeyNames="ItemEvaluacionID" DataSourceID="ObjectDataSourceCompetencias" 
                 CssClass="GridView">
               <Columns>
                  <asp:TemplateField HeaderText="Leadership Imperatives">
                     <ItemTemplate>
                        <div class="titulo">
                           <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        </div>
                        <div class="detalle">
                           <asp:Label ID="LabelDescripcion" runat="server" 
                                Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>
                           <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </div>
                     </ItemTemplate>
                     <ItemStyle Width="40%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Rating">
                     <ItemTemplate>
                     <asp:Label ID="LabelPonderacion" runat="server" Text='<%# GetResultado(Eval("Ponderacion")) %>'></asp:Label>
                        <%--<asp:Label ID="LabelResultado" runat="server" Text="Muy Bueno" />--%>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Center" CssClass="toUpperCase" Width="20%" />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Comentarios">
                     <ItemTemplate>
                        <p>
                           <asp:Label ID="TextBoxFundamentacion" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion").ToString().Replace(Environment.NewLine,"<br />"),"N/C") %>'></asp:Label></p>
                     </ItemTemplate>
                     <ItemStyle HorizontalAlign="Left" Width="40%" />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" OldValuesParameterFormatString="original_{0}"
               SelectMethod="GetDataByID" 
                 TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.ItemEvaluacionTableAdapter" 
                 DeleteMethod="Delete" UpdateMethod="Update">
                <DeleteParameters>
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Titulo" Type="String" />
                    <asp:Parameter Name="Descripcion" Type="String" />
                    <asp:Parameter Name="Original_ItemEvaluacionID" Type="Int32" />
                    <asp:Parameter Name="Original_Titulo" Type="String" />
                </UpdateParameters>
               <SelectParameters>
                  <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                     Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                     PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                     PropertyName="Value" Type="Int32" />
               </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <div class="separador1">
         </div>

        
         <div class="bloque1">
            <h3> Oportunidades de Desarrollo</h3>
            <br />
            <asp:Repeater ID="Repeater2" runat="server"   DataSourceID="ObjectDataSourceOportunidadesDesarrollo">
          <HeaderTemplate>
          <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
          </HeaderTemplate>
          <ItemTemplate>
          <tr><td><asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
          <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>' Visible="false"></asp:Label>
           <br/> </td></tr>
          <tr>
          <td><br />
          <%--<div id="personas">
              <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Width="100%"></asp:Label>
              
          </div> --%>

              <div id="comentarios">
                     <div class="item">                       
                        <div class="textwrapper">
                           <div class="textwrapperInner">
                              <p>
                                 <asp:Label ID="TextBoxComentario" runat="server" Text='<%# DefaultVal(Eval("Fundamentacion"),"S/C") %>'></asp:Label></p>
                           </div>
                        </div>
                     </div>
                </div>

          

          </td>
          </tr>
          </ItemTemplate>
          <FooterTemplate> </table></FooterTemplate>
          </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceOportunidadesDesarrollo" runat="server" 
              OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
              TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
              <SelectParameters>
                  <asp:Parameter DefaultValue="5" Name="SeccionID" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                      Name="PeriodoID" PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                      PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                      Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
              </SelectParameters>
          </asp:ObjectDataSource>

         </div>
         
         <br />
         <div style=" text-align: right;">
         <asp:label ID="LabelRegistro" runat="server" text="Label"></asp:label>
         </div>
         </form>
         </td></tr></table>
      
      </div><!--/page-inner-->
   </div><!--/page-->
   
   
</div>
   
</body>
</html>
