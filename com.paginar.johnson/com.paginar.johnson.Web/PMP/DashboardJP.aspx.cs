﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using System.Web.UI.HtmlControls;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.johnson.Web.PMP
{
    public partial class DashboardJP : System.Web.UI.Page
    {
        bool ContieneItemsEditables = false;

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ContieneItemsEditables = false;
                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                HiddenFieldLegajoEvaluador.Value = UE.legajo.ToString();
                LabelNombreUsuarioActual.Text = UE.GetUsuarioNombre(int.Parse(HiddenFieldLegajoEvaluador.Value));
            }

        }
        protected void DropDownListPeriodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DropDownListJefesTurno.DataBind();
            ValidaPeriodo(int.Parse(DropDownListPeriodos.SelectedValue));
        }
        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("-");
            else
                return (val.ToString());

        }

        protected string DefaultValComentarios(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("S/C");
            else
            {
                int Maximalongitud = 10;
                string Texto = (val.ToString().Trim().Length > Maximalongitud) ? (val.ToString().Substring(0, Maximalongitud) + "...") : val.ToString();

                return Texto;
            }

        }

        private string GetToolTipComentario(string valor)
        {
            int Maximalongitud = 10;
            string Texto = (valor.ToString().Trim().Length > Maximalongitud) ? (valor.ToString().Substring(1, Maximalongitud) + "...") : valor.ToString();
            Texto = "<a class='tooltip' href='#'>" + Texto + "<span><strong>Evaluador:</strong><table><tr><td>aaa</td></tr><tr><td>" + valor + "</td></tr></table></span></a>";

            return Texto;
        }
        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();
            ViewState["HabilitaPeriodo"] = f.ValidarPeriodoCarga(periodoid, DateTime.Now);
            return bool.Parse(ViewState["HabilitaPeriodo"].ToString());
        }
        protected void GridViewFormularios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField HiddenFieldLegajo = e.Row.FindControl("HiddenFieldLegajo") as HiddenField;
                HiddenField HiddenFieldPeriodoID = e.Row.FindControl("HiddenFieldPeriodoID") as HiddenField;
                HiddenField HiddenFieldTipoFormularioID = e.Row.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                Label LinkButtonEstado = e.Row.FindControl("LinkButtonEstado") as Label;
                HiddenField HiddenFieldEstadoID = e.Row.FindControl("HiddenFieldEstadoID") as HiddenField;
                ImageButton ImageCommand = e.Row.FindControl("ImageCommand") as ImageButton;
                CheckBox RowLevelCheckBox = (CheckBox)e.Row.FindControl("RowLevelCheckBox");

                CheckBox CheckBoxSinEvaluacion = e.Row.FindControl("CheckBoxSinEvaluacion") as CheckBox;
                TextBox TextBoxComentarioJefeDePlanta = e.Row.FindControl("TextBoxComentarioJefeDePlanta") as TextBox;
                HtmlControl LinkSinComentarioJefeDePlanta = e.Row.FindControl("LinkSinComentarioJefeDePlanta") as HtmlControl;
                HtmlControl LinkComentarioJefeDePlanta = e.Row.FindControl("LinkComentarioJefeDePlanta") as HtmlControl;
                string ComentarioJefePlanta = DataBinder.Eval(e.Row.DataItem, "ComentarioJefeDePlanta").ToString();
                int EstadoID;
                int.TryParse(DataBinder.Eval(e.Row.DataItem, "EstadoID").ToString(), out EstadoID);


                FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();
                string UrlForm = string.Empty;
                FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, 3, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
                if (RelPasosTipoFormulariosDt.Rows.Count > 0)
                {
                    FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                    UrlForm = RelPasosTipoFormulariosRow.FormAspx;
                }


                bool carga = false;

                if (ViewState["HabilitaPeriodo"] != null)
                    carga = (bool)ViewState["HabilitaPeriodo"];
                else
                    carga = ValidaPeriodo(int.Parse(DropDownListPeriodos.SelectedValue));
                if (carga)
                //                ImageCommand.Attributes.Add("onclick", string.Format("popup('Form_PmpoperariosEnJp.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                {
                    if (HiddenFieldEstadoID.Value.Trim() == "4")
                        ImageCommand.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                    else
                        ImageCommand.Attributes.Add("onclick", string.Format("popup('" + UrlForm + "?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));
                }
                else//Sino no esta en el periodo de carga, tengo q abrir impresion
                    ImageCommand.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldLegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));

                if (HiddenFieldEstadoID.Value.Trim() == "3")
                {
                    ImageCommand.ImageUrl = "~/pmp/img/editar.jpg";
                    ImageCommand.ToolTip = "Editar Evaluación";
                    RowLevelCheckBox.Visible = true;
                    RowLevelCheckBox.Checked = false;
                    ContieneItemsEditables = true;
                }
                else
                {
                    ImageCommand.ImageUrl = "~/pmp/img/ver.jpg";
                    RowLevelCheckBox.Visible = false;
                    ImageCommand.ToolTip = "Consultar evaluación en modo sólo lectura";
                    if (HiddenFieldEstadoID.Value.Trim() == "4")
                    {
                        RowLevelCheckBox.Visible = true;
                        RowLevelCheckBox.Checked = true;
                        RowLevelCheckBox.Enabled = false;

                    }
                }

                if (HiddenFieldEstadoID.Value.Trim() == "-1")
                {
                    LinkButtonEstado.Text = "No Iniciado";
                }

                TextBoxComentarioJefeDePlanta.Attributes.Add("style", "display:none;");
                if (string.IsNullOrEmpty(ComentarioJefePlanta))
                {
                    LinkSinComentarioJefeDePlanta.Attributes.Add("style", "display:block;");
                    CheckBoxSinEvaluacion.Attributes.Add("onclick", string.Format("Toggle('{0}', '{1}', '{2}');", TextBoxComentarioJefeDePlanta.ClientID, LinkSinComentarioJefeDePlanta.ClientID, CheckBoxSinEvaluacion.ClientID));
                }

                else
                {
                    CheckBoxSinEvaluacion.Attributes.Add("onclick", string.Format("Toggle('{0}','{1}' , '{2}');", TextBoxComentarioJefeDePlanta.ClientID, LinkComentarioJefeDePlanta.ClientID, CheckBoxSinEvaluacion.ClientID));
                    LinkComentarioJefeDePlanta.Attributes.Add("style", "display:block;");
                }
                if (EstadoID == 5)
                    CheckBoxSinEvaluacion.Enabled = false;
            }
        }

        protected void ButtonAprobar_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow fila in GridViewFormularios.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cb = (CheckBox)fila.FindControl("RowLevelCheckBox");
                    if (cb.Checked)
                    {
                        HiddenField HiddenFieldLegajo = fila.FindControl("HiddenFieldLegajo") as HiddenField;
                        int Legajo = Convert.ToInt32(HiddenFieldLegajo.Value);
                        HiddenField HiddenFieldPeriodoID = fila.FindControl("HiddenFieldPeriodoID") as HiddenField;
                        int PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                        HiddenField HiddenFieldTipoFormularioID = fila.FindControl("HiddenFieldTipoFormularioID") as HiddenField;
                        int TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                        FormularioInfo.SaveHistorial(4, PeriodoID, Legajo, TipoFormularioID);
                    }
                }
            }
            GridViewFormularios.DataBind();
            EjecutarScript("alert('Las evaluaciónes fueron aprobada exitosamente');");


        }
        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            ContieneItemsEditables = false;
            GridViewFormularios.DataBind();
        }

        protected void GridViewFormularios_DataBound(object sender, EventArgs e)
        {
            GridViewRow RowHeader = GridViewFormularios.HeaderRow;
            if (RowHeader == null)
            {
                ButtonAprobar.Visible = false;
                return;
            }
            CheckBox cb = (CheckBox)RowHeader.FindControl("HeaderLevelCheckBox");
            if (cb == null) return;
            cb.Visible = ContieneItemsEditables;
            ButtonAprobar.Visible = ContieneItemsEditables;


        }
        protected void DropDownListJefesTurno_DataBinding(object sender, EventArgs e)
        {
            DropDownListJefesTurno.Items.Clear();
        }
        protected void DropDownListJefesTurno_DataBound(object sender, EventArgs e)
        {
            string ItemName = (DropDownListJefesTurno.Items.Count == 0) ? "-" : "TODOS";
            ListItem LI = new ListItem(ItemName, "-1");
            DropDownListJefesTurno.Items.Insert(0, LI);
        }
        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "IF100", js, true);
            else
                ClientScript.RegisterClientScriptBlock(this.GetType(), "OpenWin", js, true);
        }

        protected void BtnGravar_Click(object sender, EventArgs e)
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            int LegajoEvaluador = 0;
            int EstadoID = 0;
            try
            {
                foreach (GridViewRow Fila in GridViewFormularios.Rows)
                {
                    CheckBox CheckBoxSinEvaluacion = Fila.FindControl("CheckBoxSinEvaluacion") as CheckBox;
                    if ((CheckBoxSinEvaluacion.Checked) && (CheckBoxSinEvaluacion.Enabled))
                    {
                        HiddenField HiddenFieldLegajo = (HiddenField)Fila.FindControl("HiddenFieldLegajo");
                        HiddenField HiddenFieldPeriodoID = (HiddenField)Fila.FindControl("HiddenFieldPeriodoID");
                        HiddenField HiddenFieldTipoFormularioID = (HiddenField)Fila.FindControl("HiddenFieldTipoFormularioID");
                        HiddenField HiddenFieldEstadoID = (HiddenField)Fila.FindControl("HiddenFieldEstadoID");

                        TextBox TextBoxComentarioJefeDePlanta = (TextBox)Fila.FindControl("TextBoxComentarioJefeDePlanta");

                        if (!string.IsNullOrEmpty(HiddenFieldLegajo.Value))
                            legajo = Convert.ToInt32(HiddenFieldLegajo.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                            PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                            TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
                            LegajoEvaluador = Convert.ToInt32(HiddenFieldLegajoEvaluador.Value);
                        if (!string.IsNullOrEmpty(HiddenFieldEstadoID.Value))
                            EstadoID = Convert.ToInt32(HiddenFieldEstadoID.Value);

                        FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                        FC.SetComentarioJefeDePlanta(TextBoxComentarioJefeDePlanta.Text);

                        FC.GrabarJP();
                        if (EstadoID < 3)
                            FC.SaveHistorial(3, PeriodoID, legajo, TipoFormularioID);
                        FC.SaveHistorial(5, PeriodoID, legajo, TipoFormularioID);



                    }
                }

                //LabelResultado.Text = "La operación fue realizada exitosamente";
                EjecutarScript(string.Format("alert('La operación fue realizada exitosamente.')"));
            }

            catch (Exception ex)
            {
                //LabelResultado.Text = "Se produjo un error. Intente nuevamente.";
                EjecutarScript(string.Format("alert('Ha ocurrido un error. Por favor intenta nuevamente.')"));
                // Response.Redirect(Request.Url.AbsoluteUri);
            }
            Response.Redirect(Request.Url.AbsoluteUri);

        }
    }
}