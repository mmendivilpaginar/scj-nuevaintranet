﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.membership;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Form_PmpEnevaluado : PageBase
    {
        string ScriptJs = string.Empty;
        private bool SoloLectura
        {
            get { return (bool)ViewState["SoloLectura"]; }
            set { ViewState["SoloLectura"] = value; }
        }
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }
        protected string DefaultVal(object val, string Default)
        {

            //if ((val == System.DBNull.Value) || (val == string.Empty))
            //    return (Default);
            //else
            //    return (val.ToString());


            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }
        protected string DefaultValComentarios(object val, string Default)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }

        void omb_OkButtonPressed(object sender, EventArgs args)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();

            return ((bool)f.ValidarPeriodoCarga(periodoid, DateTime.Now));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Usuarios usercontroller = new Usuarios();
            User Usuario = null;
            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);
            string urlReturn;
            if (!Page.IsPostBack)
            {
                HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
                HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
                HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;
                urlReturn = (!string.IsNullOrEmpty(Request.QueryString["urlReturn"])) ? Request.QueryString["urlReturn"] : string.Empty;


            }
            DisableControls(GridViewAspectos, false);
            DisableControls(GridViewCompetencias, false);
            try
            {
                int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                int LegajoLog = Usuario.GetLegajo();
                SoloLectura = false;// (!(PasoActualID == 2)) || (!IsPeriodoAbierto);
                bool PuedeEditar = (PasoActualID == 2 && LegajoLog == int.Parse(HiddenFieldlegajo.Value));
                if (SoloLectura)
                {
                    DisableControls(GVAO, false);
                    DisableControls(FormViewFormulario, false);
                    ButtonEnviarJP.Visible = false;



                }//si no es de solo lectura actualizo
                else
                {
                    if (!PuedeEditar && !Page.IsPostBack)
                      Response.Redirect("~/UnauthorizedAccess.aspx");
                    
                    HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
                    BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx');");
                    
                    
                        
                }
            }
            catch (Exception)
            {

                //throw;
            }
            ;
        }
        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);
            //return true;

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void GridViewCompetencias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblFundametnacion = e.Row.FindControl("lblFundamentacion") as Label;
                if (lblFundametnacion.Text == string.Empty) lblFundametnacion.Text = "N/C";


            }
        }


        private void GrabarAreasOperaciones()
        {
            foreach (GridViewRow fila in GVAO.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int AreaDeOperacionID = 0;
                    string Observacion = string.Empty;
                    CheckBox CheckBoxAreaDeOperacion = fila.FindControl("CheckBoxAreaDeOperacion") as CheckBox;
                    if (CheckBoxAreaDeOperacion.Checked)
                    {
                        HiddenField HiddenFieldAreaDeOperacionID = fila.FindControl("HiddenFieldAreaDeOperacionID") as HiddenField;
                        AreaDeOperacionID = int.Parse(HiddenFieldAreaDeOperacionID.Value);
                        TextBox TextBoxObservacion = fila.FindControl("TextBoxObservacion") as TextBox;
                        Observacion = TextBoxObservacion.Text;
                        FC.SaveAreaOperacion(AreaDeOperacionID, Observacion);
                    }
                }

            }
        }

        protected void GuardadoPrevio()
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;

            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }


                Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
                LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
                TextBox TextBoxComentarioEv = FormViewFormulario.FindControl("TextBoxComentarioEv") as TextBox;
                Label LabelComentarioJP = FormViewFormulario.FindControl("LabelComentarioJP") as Label;
                LabelComentarioJP.Text = (LabelComentarioJP.Text == "S/C") ? string.Empty : LabelComentarioJP.Text;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioEvaluado(TextBoxComentarioEv.Text);
                try
                {

                    GrabarAreasOperaciones();
                    FC.GrabarEvaluado();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);
                }
                catch (Exception exc)
                {

                    omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                    // omb.ShowMessage(exc.Message);
                }
        
        }

        private void GuardarItems(GridView GridItems)
        {
            foreach (GridViewRow fila in GridItems.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    HiddenField HFID = fila.FindControl("HFID") as HiddenField;
                    ItemEvaluacionID = int.Parse(HFID.Value);

                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if (RBMB.Checked)
                        Ponderacion = 4;
                    if (RBB.Checked)
                        Ponderacion = 3;
                    if (RBR.Checked)
                        Ponderacion = 2;
                    if (RBNS.Checked)
                        Ponderacion = 1;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }


        protected void FormViewFormulario_DataBound(object sender, EventArgs e)
        {
            if (FormViewFormulario.DataItemCount == 0)
                FormViewFormulario.ChangeMode(FormViewMode.Insert);
            else
                FormViewFormulario.ChangeMode(FormViewMode.ReadOnly);
            ButtonEnviarJP.OnClientClick += "return ClickEnvio();";
            //string.Format("if (Page_ClientValidate('EnviarJP')) return CheckComentario('{0}');", "ctl00_CPH1_FormViewFormulario_TextBoxComentarioEv");
        }
        protected void GVAO_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox CheckBoxAreaDeOperacion = e.Row.FindControl("CheckBoxAreaDeOperacion") as CheckBox;
                TextBox TextBoxObservacion = e.Row.FindControl("TextBoxObservacion") as TextBox;
                //if (!CheckBoxAreaDeOperacion.Checked)
                //{
                //    TextBoxObservacion.CssClass = "disabled";
                //    TextBoxObservacion.Attributes.Add("disabled", "true");
                //}

                CheckBoxAreaDeOperacion.Attributes.Add("OnClick", "HabDeshab('#" + CheckBoxAreaDeOperacion.ClientID + "','#" + TextBoxObservacion.ClientID + "')");

                Label LabelDescripcion = e.Row.FindControl("LabelDescripcion") as Label;
                CustomValidator CustomValidatorTextBoxAreasDeOperaciones = e.Row.FindControl("CustomValidatorTextBoxAreasDeOperaciones") as CustomValidator;
                CustomValidatorTextBoxAreasDeOperaciones.Attributes.Add("TextBoxObservacion", "#" + TextBoxObservacion.ClientID);
                CustomValidatorTextBoxAreasDeOperaciones.Attributes.Add("CheckBoxAreaDeOperacion", "#" + CheckBoxAreaDeOperacion.ClientID);
                CustomValidatorTextBoxAreasDeOperaciones.ErrorMessage = "Debe completar el comentario del Área de Operación: " + LabelDescripcion.Text;
            }
        }
        protected void GridViewAspectos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblFundametnacion = e.Row.FindControl("lblFundamentacion") as Label;
                if (lblFundametnacion.Text == string.Empty) lblFundametnacion.Text = "N/C";




            }
        }

        protected void CustomValidatorCompetencias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewCompetencias);
        }


        protected void CustomValidatorAspectos_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewAspectos);
        }

        private bool ValidarGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                    {
                        resultado = false;
                        break;
                    }

                }

            }
            return resultado;
        }

        private bool SetearTextBoxGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    if ((RBMB.Checked || RBB.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");
                    if ((RBR.Checked || RBNS.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");


                }

            }
            return resultado;
        }




        private void DisableControls(Control c, bool habilita)
        {

            if ((c is WebControl) && ((c.GetType().Name == "RadioButton") || (c.GetType().Name == "CheckBox") || (c.GetType().Name == "TextBox")) && (c.GetType().Name != "GridView"))
                ((WebControl)c).Enabled = habilita;

            foreach (Control child in c.Controls)
                DisableControls(child, habilita);
        }
        protected void ButtonEnviarJP_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
                LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
                TextBox TextBoxComentarioEv = FormViewFormulario.FindControl("TextBoxComentarioEv") as TextBox;
                Label LabelComentarioJP = FormViewFormulario.FindControl("LabelComentarioJP") as Label;
                LabelComentarioJP.Text = (LabelComentarioJP.Text == "S/C") ? string.Empty : LabelComentarioJP.Text;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioEvaluado(TextBoxComentarioEv.Text);
                try
                {

                    GrabarAreasOperaciones();
                    FC.GrabarEvaluado();
                    FC.SaveHistorial(3, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);
                    int legajojp = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 3);
                    string clasificacion = ((Literal)((FormView)this.Master.FindControl("FVC")).FindControl("TextCalificacion")).Text;
                    string comentariosjt = LabelComentarioJT.Text;
                    string cometnariosev = TextBoxComentarioEv.Text;
                    string mailJP = UE.GetMail(legajojp);
                    string envio;
                    string URLPMP;
                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSAU&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];


                    if (!string.IsNullOrEmpty(mailJP))
                        envio = n.envioJefePlanta(mailJP, UE.GetUsuarioNombre(legajo), clasificacion, comentariosjt, cometnariosev, URLPMP);

                    EjecutarScript(string.Format("PreguntaImprimirEvaluacionV2({0},{1},{2})", legajo, PeriodoID, TipoFormularioID));
                    // if (envio == "true") 
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    //else
                    //  omb.ShowMessage(envio);
                    

                }
                catch (Exception exc)
                {

                    omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                    // omb.ShowMessage(exc.Message);
                }

            }
        }
        protected void ButtonVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/servicios/Tramites.aspx");
        }
        protected string DefaultValComentarios(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("S/C");
            else
                return val.ToString();

            return string.Empty;
        }

        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionV2({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            //EjecutarScript();
        }

        protected void ButtonExportar_Click(object sender, EventArgs e)
        {
            
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            
            //EjecutarScript();
            //ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            GuardadoPrevio();
            System.Threading.Thread.Sleep(5000);
            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ExportarEvaluacionPMPO({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ExportarEvaluacionPMPOImpresion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
        
        }

        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                GuardadoPrevio();
                omb.ShowMessage("La Evaluación fue guardada satisfactoriamente");
            }
        }

    }
}