﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ObjetivosAnteriores.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.ObjetivosAnteriores" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
		
			.buttons{
				margin-bottom:15px;
				overflow:hidden;
				clear:both;
				padding: 2%;
			}
			
		</style>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <div class="objetivosTop buttons">
        <asp:FormView ID="FormView1" runat="server" DataSourceID="ODSFrmObetivosAnt">
        <ItemTemplate>
        <h1> Objetivos <asp:Label ID="Label1" runat="server" Text='<%# DamePeriodoByFecha(Eval("Fecha","{0:dd/MM/yyyy}")) %>'></asp:Label> </h1>
        <br />
        <h2>Adjunto: <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl='<%# String.Format("~/PMP/documents/{0}",Eval("AdjuntoPath")) %>' Target="_blank" Text='<%# (String.IsNullOrEmpty(Eval("AdjuntoPath").ToString()) ? "" : Eval("AdjuntoPath").ToString().Substring(19))  %>'>HyperLink</asp:HyperLink>
          </h1>
        </ItemTemplate>
        </asp:FormView>

        <br />
        

     <div  class="GridObjetivos">
    <asp:Repeater ID="RepeaterObjetivos" runat="server" DataSourceID="ODSFrmObetivosAnt" >
        <HeaderTemplate>
		<table id="CabeceraObjetivos" style=" width:100%">
		<thead>
            <tr>
            <th style="width:10%"></th>
                <th style="width:30%">
                    Objetivos
                </th>
                <th style="width:30%">
                    Medidas
                </th>
                <th style="width:30%">
                    Resultados
                </th>
            </tr>
            </thead>
		</table>
        <table id="TableObjetivos" style=" width:100%">
           <tbody>
          </HeaderTemplate>
          <ItemTemplate>
            <tr runat="server" id="Obj1" visible='<%# Eval("objetivo1").ToString() != "" %>'>
            <td style="width:10%">1</td>
                <td style="width:30%">
                    <asp:Label ID="objetivo1Label" runat="server" Text='<%# Eval("objetivo1") %>' />
                </td>
                <td style="width:30%">
                    <asp:Label ID="medidas1Label" runat="server" Text='<%# Eval("medidas1") %>' />
                </td>
                <td style="width:30%">
                    <asp:Label ID="resultados1Label" runat="server" Text='<%# Eval("resultados1") %>' />
                </td>
            </tr>
            <tr runat="server" id="Obj2" visible='<%# Eval("objetivo2").ToString() != "" %>'>
            <td >2</td>
                <td >
                    <asp:Label ID="objetivo2Label" runat="server" Text='<%# Eval("objetivo2") %>' />
                </td>
                <td >
                    <asp:Label ID="medidas2Label" runat="server" Text='<%# Eval("medidas2") %>' />
                </td>
                <td>
                    <asp:Label ID="resultados2Label" runat="server" Text='<%# Eval("resultados2") %>' />
                </td>
            </tr>
            <tr runat="server" id="Obj3" visible='<%# Eval("objetivo3").ToString() != "" %>'>
            <td>3</td>
                <td >
                    <asp:Label ID="objetivo3Label" runat="server" Text='<%# Eval("objetivo3") %>' />
                </td>
                <td >
                    <asp:Label ID="medidas3Label" runat="server" Text='<%# Eval("medidas3") %>' />
                </td>
                <td>
                    <asp:Label ID="resultados3Label" runat="server" Text='<%# Eval("resultados3") %>' />
                </td>
            </tr>
            <tr runat="server" id="Obj4" visible='<%# Eval("objetivo4").ToString() != "" %>'>
                <td>4</td>                                
                <td >
                    <asp:Label ID="objetivo4Label" runat="server" Text='<%# Eval("objetivo4") %>' />
                </td>
                <td >
                    <asp:Label ID="medidas4Label" runat="server" Text='<%# Eval("medidas4") %>' />
                </td>
                <td>
                    <asp:Label ID="resultados4Label" runat="server" Text='<%# Eval("resultados4") %>' />
                </td>
            </tr>
            <tr runat="server" id="Obj5" visible='<%#  Eval("objetivo5").ToString() != "" %>'>
            <td>5</td>
                <td>
                    <asp:Label ID="objetivo5Label0" runat="server" Text='<%# Eval("objetivo5") %>' />

                </td>
                <td>
                    <asp:Label ID="medidas5Label" runat="server" Text='<%# Eval("medidas5") %>' />
                </td>
                <td>
                    <asp:Label ID="resultados5Label" runat="server" Text='<%# Eval("resultados5") %>' />
                </td>
            </tr>


            </ItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
         </asp:Repeater>
         </div>

    <asp:ObjectDataSource ID="ODSFrmObetivosAnt" runat="server" InsertMethod="Insert"
                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetObjetivosByObjetivoID"
                                
            TypeName="com.paginar.johnson.DAL.DSEvalDesempTableAdapters.frmObjetivosProxTableAdapter" 
            UpdateMethod="Update">
                                <InsertParameters>
                                    <asp:Parameter Name="usr_alta" Type="Int32" />
                                    <asp:Parameter Name="fecha" Type="DateTime" />
                                    <asp:Parameter Name="objetivo1" Type="String" />
                                    <asp:Parameter Name="objetivo2" Type="String" />
                                    <asp:Parameter Name="objetivo3" Type="String" />
                                    <asp:Parameter Name="objetivo4" Type="String" />
                                    <asp:Parameter Name="objetivo5" Type="String" />
                                    <asp:Parameter Name="medidas1" Type="String" />
                                    <asp:Parameter Name="medidas2" Type="String" />
                                    <asp:Parameter Name="medidas3" Type="String" />
                                    <asp:Parameter Name="medidas4" Type="String" />
                                    <asp:Parameter Name="medidas5" Type="String" />
                                    <asp:Parameter Name="resultados1" Type="String" />
                                    <asp:Parameter Name="resultados2" Type="String" />
                                    <asp:Parameter Name="resultados3" Type="String" />
                                    <asp:Parameter Name="resultados4" Type="String" />
                                    <asp:Parameter Name="resultados5" Type="String" />
                                    <asp:Parameter Name="objetivo6" Type="String" />
                                    <asp:Parameter Name="medidas6" Type="String" />
                                    <asp:Parameter Name="resultados6" Type="String" />
                                    <asp:Parameter Name="objetivo7" Type="String" />
                                    <asp:Parameter Name="medidas7" Type="String" />
                                    <asp:Parameter Name="resultados7" Type="String" />
                                </InsertParameters>
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="ObjetivoID" QueryStringField="ObjetivoID" 
                                        Type="Int32" />
                                </SelectParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="objetivo1" Type="String" />
                                    <asp:Parameter Name="objetivo2" Type="String" />
                                    <asp:Parameter Name="objetivo3" Type="String" />
                                    <asp:Parameter Name="objetivo4" Type="String" />
                                    <asp:Parameter Name="objetivo5" Type="String" />
                                    <asp:Parameter Name="medidas1" Type="String" />
                                    <asp:Parameter Name="medidas2" Type="String" />
                                    <asp:Parameter Name="medidas3" Type="String" />
                                    <asp:Parameter Name="medidas4" Type="String" />
                                    <asp:Parameter Name="medidas5" Type="String" />
                                    <asp:Parameter Name="resultados1" Type="String" />
                                    <asp:Parameter Name="resultados2" Type="String" />
                                    <asp:Parameter Name="resultados3" Type="String" />
                                    <asp:Parameter Name="resultados4" Type="String" />
                                    <asp:Parameter Name="resultados5" Type="String" />
                                    <asp:Parameter Name="objetivo6" Type="String" />
                                    <asp:Parameter Name="medidas6" Type="String" />
                                    <asp:Parameter Name="resultados6" Type="String" />
                                    <asp:Parameter Name="objetivo7" Type="String" />
                                    <asp:Parameter Name="medidas7" Type="String" />
                                    <asp:Parameter Name="resultados7" Type="String" />
                                    <asp:Parameter Name="objetivoid" Type="Int32" />
                                </UpdateParameters>
                            </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
