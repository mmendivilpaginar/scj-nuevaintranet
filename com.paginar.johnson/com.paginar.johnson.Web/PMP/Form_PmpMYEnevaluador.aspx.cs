﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Web.UI.HtmlControls;
using System.Collections;
using com.paginar.johnson.membership;
using System.Text;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Form_PmpMYEnevaluador : PageBase
    {
        string ScriptJs = string.Empty;
        private bool SoloLectura
        {
            get { return (bool)ViewState["SoloLectura"]; }
            set { ViewState["SoloLectura"] = value; }
        }
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }

        private LogController _fle;
        public LogController fle
        {
            get
            {
                _fle = (LogController)this.Session["fle"];
                if (_fle == null)
                {
                    _fle = new LogController();
                    this.Session["fle"] = _fle;
                }
                return _fle;
            }
        }

        protected string DefaultVal(object val, string Default)
        {

            //if ((val == System.DBNull.Value) || (val == string.Empty))
            //    return (Default);
            //else
            //    return (val.ToString());


            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }
        protected string DefaultValComentarios(object val, string Default)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }

        void omb_OkButtonPressed(object sender, EventArgs args)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();

            return ((bool)f.ValidarPeriodoCarga(periodoid, DateTime.Now));
        }

        void WUCGuardadoPrevio_GuardadoPrevio()
        {
            GuardadoPrevio();
        }
        bool nocargojs;
        protected void Page_Load(object sender, EventArgs e)
        {
            nocargojs = true;
            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);            
            WUCGuardadoPrevio.GuardadoPrevio += new pmp_WebUserControlGuardadoPrevio.GuardadoPrevioHandler(WUCGuardadoPrevio_GuardadoPrevio);
            Usuarios usercontroller = new Usuarios();
            FormulariosController f = new FormulariosController();
            User Usuario = null;
            if (!Page.IsPostBack)
            {
                HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
                HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
                HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;

                if (string.IsNullOrEmpty(HiddenFieldlegajo.Value) && string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value) && string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                    Response.Redirect("EvaluacionError.aspx");

                if (!f.ExisteEvaluacion(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value)))
                    Response.Redirect("EvaluacionError.aspx");


            }

           // DisableControls(GridViewCompetencias, false);
           
            //DisableRepeater(RepeaterOportunidadesDesarrollo, false);
            try
            {
                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                int LegajoLog = Usuario.GetLegajo();
                int? LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);
                
                int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
                SoloLectura = false;// (!(PasoActualID == 1)) || (!IsPeriodoAbierto);
                bool PuedeEditar = (LegajoLog == LegajoEvaluador && PasoActualID == 1);
               // ButtonGSinEnviar.Visible = false;
               // ButtonEnviarEvaluador.Visible = false;
                if (SoloLectura)
                {

                    ButtonAprobarEvaluacion.Visible = false;
                    ButtonDevolverEvaluado.Visible = false;
                    DisableGrilla(GridViewCompetencias, false);
                    DisableRepeater(RepeaterOportunidadesDesarrollo, true);// Valor Original false
                    DisableRepeater(RepeaterLogros, false);
                    DisableRepeater(RepeaterComportamientos, false);
                  //  ButtonEnviarEvaluador.Visible = false;



                }//si no es de solo lectura actualizo
                else
                {
                    if (!PuedeEditar && !Page.IsPostBack)
                      Response.Redirect("~/UnauthorizedAccess.aspx");

                   HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
                   //BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.reload();");
                   BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx?asignados=1');");
                    
                        
                }
            }
            catch (Exception)
            {

                //throw;
            }
            ;
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
          
           
            if (!SoloLectura)
            {
                //HiddenFieldJS.Value = string.Empty;
                //SetearTextBoxGrilla(GridViewAspectos);
                SetearTextBoxGrilla(GridViewCompetencias);
                //EjecutarScript(HiddenFieldJS.Value);
             
            }
            if (SoloLectura)
            {
                DisableGrilla(GridViewCompetencias, false);
                DisableRepeater(RepeaterOportunidadesDesarrollo, true);//Valor Original false
                DisableRepeater(RepeaterLogros, false);
                DisableRepeater(RepeaterComportamientos, false);
            }
        }

        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);
            //return true;

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void GridViewCompetencias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{

            //    SortedList FormatCells = new SortedList();
            //    FormatCells.Add("1", ",1,2");
            //    FormatCells.Add("2", "Ratings,2,1");
            //    FormatCells.Add("3", ",1,2");


            //    SortedList FormatCells2 = new SortedList();
            //    FormatCells2.Add("1", "Subgroup1,1,1");
            //    FormatCells2.Add("2", "Subgroup2,1,1");
            //    GetMultiRowHeader(e, FormatCells2);
            //    GetMultiRowHeader(e, FormatCells);
            //}
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                CustomValidator CustomValidatorTextBoxFundamentacion = e.Row.FindControl("CustomValidatorTextBoxFundamentacion") as CustomValidator;
                TextBox TextBoxFundamentacion = e.Row.FindControl("TextBoxFundamentacion") as TextBox;
                Label LabelTitulo = e.Row.FindControl("LabelTitulo") as Label;
                RadioButton RBFortaleza = e.Row.FindControl("RBFortaleza") as RadioButton;
                RadioButton RBAreaDesarrollo = e.Row.FindControl("RBAreaDesarrollo") as RadioButton;


                if (RBFortaleza.Checked )
                    TextBoxFundamentacion.CssClass = "opcional";

                //if ((RBFortaleza.Checked  && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                //    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");


                if (RBAreaDesarrollo.Checked )
                    TextBoxFundamentacion.CssClass = "requerido";
                //if ((RBAreaDesarrollo.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                //    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");
                //RBFortaleza.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                //RBAreaDesarrollo.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                //TextBoxFundamentacion.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                //string elscript = "$(document).ready(function() {ProcesarComentarios('" + TextBoxFundamentacion.ClientID + "','" + RBAreaDesarrollo.ClientID + "') });";
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "FSR544", elscript, true);
                if (nocargojs)
                {
                    string elscript = "$(document).ready(function() {checkLeadershipImperatives(); ProcesarComentarios('" + TextBoxFundamentacion.ClientID + "','" + RBAreaDesarrollo.ClientID + "','" + RBFortaleza.ClientID + "') });";
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "FSR544", elscript, true);
                    nocargojs = false;
                }

                RBFortaleza.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional") + string.Format("ProcesarComentarios('{0}','{1}','{2}');", TextBoxFundamentacion.ClientID, RBFortaleza.ClientID, RBAreaDesarrollo.ClientID));
                RBAreaDesarrollo.Attributes.Add("OnClick", string.Format("ProcesarComentarios('{0}','{1}','{2}');", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID, RBFortaleza.ClientID) + string.Format("CambiarClase('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido"));

                //RBFortaleza.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}')", TextBoxFundamentacion.ClientID, "opcional"));
                //RBAreaDesarrollo.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}')", TextBoxFundamentacion.ClientID, "requerido"));

                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBFortaleza", "#" + RBFortaleza.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBAreaDesarrollo", "#" + RBAreaDesarrollo.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("TextBoxFundamentacion", "#" + TextBoxFundamentacion.ClientID);
                CustomValidatorTextBoxFundamentacion.ErrorMessage = "No completó el comentario de la Competencia: " + LabelTitulo.Text + ", solapa C- Competencias";


               


            }
        }

        public void GetMultiRowHeader(GridViewRowEventArgs e, SortedList GetCels)
        {

            GridViewRow row;
            IDictionaryEnumerator enumCels = GetCels.GetEnumerator();

            row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
            while (enumCels.MoveNext())
            {

                string[] count = enumCels.Value.ToString().Split(Convert.ToChar(","));
                TableCell Cell;
                Cell = new TableCell();
                Cell.RowSpan = Convert.ToInt16(count[2].ToString());
                Cell.ColumnSpan = Convert.ToInt16(count[1].ToString());
                Cell.Controls.Add(new LiteralControl(count[0].ToString()));
                Cell.HorizontalAlign = HorizontalAlign.Center;
                Cell.ForeColor = System.Drawing.Color.Black;
                // Cell.ForeColor = System.Drawing.Color.White;
                row.Cells.Add(Cell);
            }


            e.Row.Parent.Controls.AddAt(0, row);


        }



        private void GuardarItems(GridView GridItems)
        {
            foreach (GridViewRow fila in GridItems.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                    RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;
                    HiddenField HFID = fila.FindControl("HFID") as HiddenField;
                    
                    ItemEvaluacionID = int.Parse(HFID.Value);                       

                    if (RBFortaleza.Checked)
                        Ponderacion = 1;
                    if (RBAreaDesarrollo.Checked)
                        Ponderacion = 2;

                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }






        protected void CustomValidatorCompetencias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewCompetencias);
        }




        private bool ValidarGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                    {
                        resultado = false;
                        break;
                    }
 
                }

            }
            return resultado;
        }

        private bool SetearTextBoxGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                    RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;
                   
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    if ((RBFortaleza.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");
                    if ((RBAreaDesarrollo.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");


                }

            }
            return resultado;
        }




        private void DisableControls(Control c, bool habilita)
        {

            if ((c is WebControl) && ((c.GetType().Name == "RadioButton") || (c.GetType().Name == "CheckBox") || (c.GetType().Name == "TextBox")) && (c.GetType().Name != "GridView") )
                ((WebControl)c).Enabled = habilita;

            foreach (Control child in c.Controls)
                DisableControls(child, habilita);
        }
        protected void ButtonEnviarEvaluador_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

                try
                {

                    GuardarItemsRepeater(RepeaterLogros);
                    GuardarItemsRepeater(RepeaterComportamientos);
                    GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);
                    // int legajojp = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 3);
                    //string clasificacion = ((Literal)((FormView)this.Master.FindControl("FVC")).FindControl("TextCalificacion")).Text;
                    string comentariosjt = "";
                    string cometnariosev = "";
                    //string mailJP = UE.GetMail(legajojp);
                    string envio;
                    //if (!string.IsNullOrEmpty(mailJP))
                    //    envio = n.envioJefePlanta(mailJP, UE.GetUsuarioNombre(legajo), clasificacion, comentariosjt, cometnariosev);


                    // if (envio == "true") 
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    //else
                    //  omb.ShowMessage(envio);


                }
                catch (Exception exc)
                {

                    omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                    // omb.ShowMessage(exc.Message);
                }

            }
        }
        protected void ButtonVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/servicios/Tramites.aspx");
        }
        protected string DefaultValComentarios(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("S/C");
            else
                return val.ToString();

            return string.Empty;
        }

        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                GuardadoPrevio();
                EjecutarScript("GuardarSinEnviar()");
            }
        }

        private void GuardadoPrevio()
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }


            FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

            try
            {
                GuardarItems(GridViewCompetencias);
                GuardarItemsRepeater(RepeaterLogros);
                GuardarItemsRepeater(RepeaterComportamientos);
                GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                FC.GrabarEvaluador();
                                

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void GuardarItemsRepeater(Repeater RptItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {

                int ItemEvaluacionID = 0;
                int Ponderacion = 0;
                string Fundamentacion = string.Empty;

                HiddenField HFID = item.FindControl("HFID") as HiddenField;
                ItemEvaluacionID = int.Parse(HFID.Value);

                TextBox TextBoxFundamentacion = item.FindControl("TextBoxComentario") as TextBox;
                Fundamentacion = TextBoxFundamentacion.Text;
                FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);

            }
        }


        public void AddExtraHeader()
        {


            SortedList cellSortedList = new SortedList();

            cellSortedList.Add("1", "<div  align=center>FCE(*)</div>,1,2");
            cellSortedList.Add("2", "<div  align=center> Ratings </div>,2,1");
            cellSortedList.Add("3", "<div  align=center>Comentarios</div>,1,2");


            IDictionaryEnumerator enumCells = cellSortedList.GetEnumerator();
            GridViewRow row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal); while (enumCells.MoveNext())
            {

                string[] cont = enumCells.Value.ToString().Split(Convert.ToChar(","));
                TableCell Cell;

                Cell = new TableCell();
                Cell.RowSpan = Convert.ToInt16(cont[2].ToString());

                Cell.ColumnSpan = Convert.ToInt16(cont[1].ToString());
                Cell.Controls.Add(new LiteralControl(cont[0].ToString()));

                //Cell.HorizontalAlign = HorizontalAlign.Center;
                //Cell.ForeColor = System.Drawing.Color.Black;
                //Cell.BackColor = System.Drawing.Color.FromArgb(221, 221, 221);
                //Cell.Font.Bold = true;
                //Cell.ForeColor = System.Drawing.Color.FromArgb(127, 127, 127);

                row.Cells.Add(Cell);

            }



            GridViewCompetencias.HeaderRow.Parent.Controls.AddAt(0, row);


            // creating the second row in header
            cellSortedList = new SortedList();

            cellSortedList.Add("1", "<div align=center >Fortalezas </div>,1,1");
            cellSortedList.Add("2", "<div align=center >Area de desarrollo</div>,1,1");

            enumCells = cellSortedList.GetEnumerator();

            row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
            while (enumCells.MoveNext())
            {
                string[] cont = enumCells.Value.ToString().Split(Convert.ToChar(","));

                TableCell Cell;
                Cell = new TableCell();

                Cell.RowSpan = Convert.ToInt16(cont[2].ToString());
                Cell.ColumnSpan = Convert.ToInt16(cont[1].ToString());

                Cell.Controls.Add(new LiteralControl(cont[0].ToString()));
                //Cell.HorizontalAlign = HorizontalAlign.Center; Cell.ForeColor = System.Drawing.Color.Black;
                //Cell.BackColor = System.Drawing.Color.FromArgb(221, 221, 221);
                //Cell.Font.Bold = true;
                //Cell.ForeColor = System.Drawing.Color.FromArgb(127, 127, 127);
                row.Cells.Add(Cell);

            }

            GridViewCompetencias.HeaderRow.Parent.Controls.AddAt(1, row);


        }

        protected void RepeaterLogros_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                Label lblTitulo = e.Item.FindControl("lblTitulo") as Label;
                CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                CustomValidatorTextBoxComentario.ErrorMessage = "No completó el campo " + lblTitulo.Text;
            }
        }

        protected void RepeaterComportamientos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                CustomValidatorTextBoxComentario.ErrorMessage = "No completó el campo Comportamiento";
            }
        }

        protected void RepeaterOportunidadesDesarrollo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                CustomValidatorTextBoxComentario.ErrorMessage = "No completó el campo de la solapa D- Oportunidades de Desarrollo";
            }
        }


        protected void ButtonAprobarEvaluacion_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

               
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
               
                try
                {
                    GuardarItems(GridViewCompetencias);
                    GuardarItemsRepeater(RepeaterLogros);
                    GuardarItemsRepeater( RepeaterComportamientos);
                    GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(4, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

                    string mailEvaluado = UE.GetMail(legajo);
                    string envio;
                    string urlPMPMY;
                    urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=3&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];

                    if (!string.IsNullOrEmpty(mailEvaluado))
                        envio = n.aproboevaluacionMYV2(mailEvaluado, UE.GetUsuarioNombre(legajoevaluador), urlPMPMY);

                    WUCGuardadoPrevio.EnabledTimer = false;
                    omb.ShowMessage("La Evaluación fue Aprobada");
                    EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }
        protected void ButtonDevolverEvaluado_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                //Label LabelComentarioJT = FormViewFormulario.FindControl("LabelComentarioJT") as Label;
               // LabelComentarioJT.Text = (LabelComentarioJT.Text == "S/C") ? string.Empty : LabelComentarioJT.Text;
               // Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
               // LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
               // TextBox TextBoxComentarioJP = FormViewFormulario.FindControl("TextBoxComentarioJP") as TextBox;
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
               // FC.SetComentarioJefeDePlanta(TextBoxComentarioJP.Text);
                try
                {
                    GuardarItems(GridViewCompetencias);
                    GuardarItemsRepeater(RepeaterLogros);
                    GuardarItemsRepeater(RepeaterComportamientos);
                    GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                    
                    //GuardarItems(GridViewAspectos);
                    //GrabarAreasOperaciones();
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

                    string mailEvaluado = UE.GetMail(legajo);
                    string envio;
                    string urlPMPMY;
                    urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=2&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];


                    if (!string.IsNullOrEmpty(mailEvaluado))
                        envio = n.devuelveevaluacionMYV2(mailEvaluado, UE.GetUsuarioNombre(legajoevaluador), urlPMPMY);

                    WUCGuardadoPrevio.EnabledTimer = false;
                    //if (envio == "true") 
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");

                    EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    //else
                    // omb.ShowMessage(envio);

                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        private void DisableRepeater(Repeater RptItems, bool habilita)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {
                TextBox TextBoxFundamentacion = item.FindControl("TextBoxComentario") as TextBox;
                TextBoxFundamentacion.ReadOnly = !habilita;              

            }
        }
        private void DisableGrilla(GridView grilla, bool habilita)
        {
            foreach (GridViewRow fila in grilla.Rows)
            {
                TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                TextBoxFundamentacion.ReadOnly = !habilita;

                RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                RBFortaleza.Enabled = habilita;

                RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;
                RBAreaDesarrollo.Enabled = habilita;

            }
        }

       

        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    string legajost = string.Empty;
                    legajost = HiddenFieldlegajo.Value;
                    fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresion PMPMY", legajost.ToString());
                }
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionMY({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);

            }
        }

        protected void ButtonDoc_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    string legajost = string.Empty;
                    legajost = HiddenFieldlegajo.Value;
                    fle.Log(this.ObjectUsuario.legajo.ToString(), "Exportacion PMPMY Word", legajost.ToString());
                }
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("wordEvaluacionMY({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);

            }
        }
    }



}