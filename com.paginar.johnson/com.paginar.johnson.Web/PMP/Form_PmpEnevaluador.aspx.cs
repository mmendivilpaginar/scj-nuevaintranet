﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.membership;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Form_PmpEnevaluador : PageBase
    {

        string ScriptJs = string.Empty;

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }


        private bool SoloLectura
        {
            get { return (bool)ViewState["SoloLectura"]; }
            set { ViewState["SoloLectura"] = value; }
        }



        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            if (!SoloLectura)
            {
                HiddenFieldJS.Value = string.Empty;
                SetearTextBoxGrilla(GridViewAspectos);
                SetearTextBoxGrilla(GridViewCompetencias);
                EjecutarScript(HiddenFieldJS.Value);
            }
        }
        void omb_OkButtonPressed(object sender, EventArgs args)
        {

            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }

        void WUCGuardadoPrevio_GuardadoPrevio()
        {
            GuardadoPrevio();
        }

        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();

            return ((bool)f.ValidarPeriodoCarga(periodoid, DateTime.Now));
        }
        protected string DefaultVal(object val)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return ("S/C");
            return (val.ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager _scriptMan = ScriptManager.GetCurrent(this);
            _scriptMan.AsyncPostBackTimeout = 36000;

            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);
            WUCGuardadoPrevio.GuardadoPrevio += new pmp_WebUserControlGuardadoPrevio.GuardadoPrevioHandler(WUCGuardadoPrevio_GuardadoPrevio);
            Usuarios usercontroller = new Usuarios();
            User Usuario = null;
            int LegajoLog = 0;
            int? LegajoEvaluador;
            int QueryStringSoloLectura = 0;
            if (!Page.IsPostBack)
            {
                HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
                HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
                HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;
                HiddenFieldLegajoEvaluador.Value = (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"])) ? Request.QueryString["LegajoEvaluador"] : string.Empty;

            }
            try
            {
                int.TryParse(Request.QueryString["SoloLectura"], out QueryStringSoloLectura);
                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                LegajoLog = Usuario.GetLegajo();
                LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);
                //int.TryParse(HiddenFieldLegajoEvaluador.Value.ToString(), out LegajoEvaluador);

                int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
                SoloLectura = (Page.User.IsInRole("Visualizar PMPS-Reporte de Seguimiento PMPO") && LegajoLog != LegajoEvaluador);
                bool PuedeEditar = (LegajoLog == LegajoEvaluador && (PasoActualID == 1 || PasoActualID == -1));
                    
                   // (!((PasoActualID == 1) || (PasoActualID == -1))) || (!IsPeriodoAbierto) || ((LegajoLog != LegajoEval) && LegajoEval != 0) || (QueryStringSoloLectura == 1);
                if (SoloLectura)
                {
                    SetearSoloLectura();
                }//si no es de solo lectura actualizo
                else
                {
                    if (!PuedeEditar && !Page.IsPostBack)
                         Response.Redirect("~/UnauthorizedAccess.aspx");

                    HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
                    ////BodyPmp.Attributes.Add("onunload", "window.opener.location.reload();");
                    //String js = "if (!window.opener.closed)window.opener.refreshParent();" + Environment.NewLine;
                  //  BodyPmp.Attributes.Add("onunload", js);

                   
                    BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx');");
                   
                       
                         //salir de La pagina
                    
                }
            }
            catch (Exception exc)
            {

                //PANTALLA DE EXCEPCION
            }

        }

        private void SetearSoloLectura()
        {
            //para que abra la impresion sin guardado previo
            ButtonImprimir.Attributes.Add("onclick", string.Format("popup('Impresion.aspx?Legajo={0}&PeriodoID={1}&TipoFormularioID={2}',1000,600);return false;", HiddenFieldlegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value));

            DisableControls(GridViewAspectos, false);
            DisableControls(GridViewCompetencias, false);
            DisableControls(GVAO, false);
            DisableControls(FormViewFormulario, false);
            //ButtonGSinEnviar.Visible = false;               
            //ButtonEnviarEvaluado.Visible = false;
            UpdatePanelDraft.Visible = false;
            WUCGuardadoPrevio.EnabledTimer = false;
            SoloLectura = true;
            HiddenFieldLegajoEvaluador.Value = string.Empty;
        }
        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {

            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void GridViewCompetencias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                RadioButton RBMB = e.Row.FindControl("RBMB") as RadioButton;
                RadioButton RBB = e.Row.FindControl("RBB") as RadioButton;
                RadioButton RBR = e.Row.FindControl("RBR") as RadioButton;
                RadioButton RBNS = e.Row.FindControl("RBNS") as RadioButton;
                CustomValidator CustomValidatorTextBoxFundamentacion = e.Row.FindControl("CustomValidatorTextBoxFundamentacion") as CustomValidator;
                Label LabelTitulo = e.Row.FindControl("LabelTitulo") as Label;
                TextBox TextBoxFundamentacion = e.Row.FindControl("TextBoxFundamentacion") as TextBox;
                //Si no checkeo ningun Radio tiene q estar deshabilitado
            //    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
              //      TextBoxFundamentacion.Attributes.Add("disabled", "disabled");
                if (RBMB.Checked || RBB.Checked)
                    TextBoxFundamentacion.CssClass = "opcional";

                if ((RBMB.Checked || RBB.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");

                if (RBR.Checked || RBNS.Checked)
                    TextBoxFundamentacion.CssClass = "requerido";
                if ((RBR.Checked || RBNS.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");

                RBMB.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforo();", TextBoxFundamentacion.ClientID, "opcional"));
                RBB.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforo();", TextBoxFundamentacion.ClientID, "opcional"));
                RBR.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforo();", TextBoxFundamentacion.ClientID, "requerido"));
                RBNS.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforo();", TextBoxFundamentacion.ClientID, "requerido"));
                //
                //revisarSemaforo();
                //revisarSemaforoObjetivos();
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBMB", "#" + RBMB.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBB", "#" + RBB.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBR", "#" + RBR.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBNS", "#" + RBNS.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("TextBoxFundamentacion", "#" + TextBoxFundamentacion.ClientID);
                CustomValidatorTextBoxFundamentacion.ErrorMessage = "Debe completar la fundamentación de la Competencia: " + LabelTitulo.Text;

            }
        }

        private void GuardadoPrevio()
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            int LegajoEvaluador = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
            {
                LegajoEvaluador = Convert.ToInt32(HiddenFieldLegajoEvaluador.Value);
            }

            TextBox TextBoxComentarioJT = FormViewFormulario.FindControl("TextBoxComentarioJT") as TextBox;
            Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
            Label LabelComentarioJP = FormViewFormulario.FindControl("LabelComentarioJP") as Label;

            if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
            {
                FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
                //Limpio Hiddden para q no lo tome despues en el envio y lo asociie nuevamente
                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                NotificacionesController n = new NotificacionesController();
                string mensaje = n.inicioEvaluacion(UE.GetUsuarioNombre(LegajoEvaluador), UE.GetUsuarioNombre(legajo));

                HiddenFieldLegajoEvaluador.Value = string.Empty;
                EjecutarScript(string.Format("document.getElementById('{0}').value ='';", HiddenFieldLegajoEvaluador.ClientID));

            }
            FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
            FC.SetComentarioJefeTurno(TextBoxComentarioJT.Text);
            try
            {
                GuardarItems(GridViewCompetencias);
                GuardarItems(GridViewAspectos);
                FC.GrabarEvaluador();
                FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);

            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                GuardadoPrevio();
                omb.ShowMessage("La Evaluación fue guardada satisfactoriamente");
            }
        }

        private void GrabarAreasOperaciones()
        {
            foreach (GridViewRow fila in GVAO.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int AreaDeOperacionID = 0;
                    string Observacion = string.Empty;
                    CheckBox CheckBoxAreaDeOperacion = fila.FindControl("CheckBoxAreaDeOperacion") as CheckBox;
                    if (CheckBoxAreaDeOperacion.Checked)
                    {
                        HiddenField HiddenFieldAreaDeOperacionID = fila.FindControl("HiddenFieldAreaDeOperacionID") as HiddenField;
                        AreaDeOperacionID = int.Parse(HiddenFieldAreaDeOperacionID.Value);
                        TextBox TextBoxObservacion = fila.FindControl("TextBoxObservacion") as TextBox;
                        Observacion = TextBoxObservacion.Text;
                        FC.SaveAreaOperacion(AreaDeOperacionID, Observacion);
                    }
                }

            }
        }
        private void GuardarItems(GridView GridItems)
        {
            foreach (GridViewRow fila in GridItems.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    HiddenField HFID = fila.FindControl("HFID") as HiddenField;
                    if(HFID != null)
                        ItemEvaluacionID = int.Parse(HFID.Value);

                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if (RBMB.Checked)
                        Ponderacion = 4;
                    if (RBB.Checked)
                        Ponderacion = 3;
                    if (RBR.Checked)
                        Ponderacion = 2;
                    if (RBNS.Checked)
                        Ponderacion = 1;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    string TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
                    string TextOpcional = "Puede fundamentar la fortaleza indicada";

                    if (Fundamentacion == TextRequerido || Fundamentacion == TextOpcional)
                        Fundamentacion = string.Empty;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }


        protected void FormViewFormulario_DataBound(object sender, EventArgs e)
        {
            if (FormViewFormulario.DataItemCount == 0)
                FormViewFormulario.ChangeMode(FormViewMode.Insert);
            else
                FormViewFormulario.ChangeMode(FormViewMode.ReadOnly);

            ButtonEnviarEvaluado.OnClientClick += "return ClickEnvio();";


        }
            
        protected void GridViewAspectos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RadioButton RBMB = e.Row.FindControl("RBMB") as RadioButton;
                RadioButton RBB = e.Row.FindControl("RBB") as RadioButton;
                RadioButton RBR = e.Row.FindControl("RBR") as RadioButton;
                RadioButton RBNS = e.Row.FindControl("RBNS") as RadioButton;
                TextBox TextBoxFundamentacion = e.Row.FindControl("TextBoxFundamentacion") as TextBox;
                CustomValidator CustomValidatorTextBoxFundamentacion = e.Row.FindControl("CustomValidatorTextBoxFundamentacion") as CustomValidator;
                Label LabelTitulo = e.Row.FindControl("LabelTitulo") as Label;
                //Si no checkeo ningun Radio tiene q estar deshabilitado
                //if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                //    TextBoxFundamentacion.Attributes.Add("disabled", "disabled");
                if (RBMB.Checked || RBB.Checked)
                    TextBoxFundamentacion.CssClass = "opcional";
                if ((RBMB.Checked || RBB.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");
                if (RBR.Checked || RBNS.Checked)
                    TextBoxFundamentacion.CssClass = "requerido";
                if ((RBR.Checked || RBNS.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");

                RBMB.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforoObjetivos();", TextBoxFundamentacion.ClientID, "opcional"));
                RBB.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforoObjetivos();", TextBoxFundamentacion.ClientID, "opcional"));
                RBR.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforoObjetivos();", TextBoxFundamentacion.ClientID, "requerido"));
                RBNS.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}'); revisarSemaforoObjetivos();", TextBoxFundamentacion.ClientID, "requerido"));



                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBMB", "#" + RBMB.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBB", "#" + RBB.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBR", "#" + RBR.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBNS", "#" + RBNS.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("TextBoxFundamentacion", "#" + TextBoxFundamentacion.ClientID);
                CustomValidatorTextBoxFundamentacion.ErrorMessage = "Debe completar la fundamentación del objetivo: " + LabelTitulo.Text;
            }
        }

        protected void CustomValidatorCompetencias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewCompetencias);
        }


        protected void CustomValidatorAspectos_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewAspectos);
        }

        private bool ValidarGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                    {
                        resultado = false;
                        break;
                    }

                }

            }
            return resultado;
        }

        private bool SetearTextBoxGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    if ((RBMB.Checked || RBB.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");
                    if ((RBR.Checked || RBNS.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");


                }

            }
            return resultado;
        }

        protected void ButtonEnviarEvaluado_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                int LegajoEvaluador = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
                {
                    LegajoEvaluador = Convert.ToInt32(HiddenFieldLegajoEvaluador.Value);
                }
                TextBox TextBoxComentarioJT = FormViewFormulario.FindControl("TextBoxComentarioJT") as TextBox;
                Label LabelComentarioEv = FormViewFormulario.FindControl("LabelComentarioEv") as Label;
                LabelComentarioEv.Text = (LabelComentarioEv.Text == "S/C") ? string.Empty : LabelComentarioEv.Text;
                Label LabelComentarioJP = FormViewFormulario.FindControl("LabelComentarioJP") as Label;
                LabelComentarioJP.Text = (LabelComentarioJP.Text == "S/C") ? string.Empty : LabelComentarioJP.Text;
                if (!string.IsNullOrEmpty(HiddenFieldLegajoEvaluador.Value))
                {
                    FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    string mensaje = n.inicioEvaluacion(UE.GetUsuarioNombre(LegajoEvaluador), UE.GetUsuarioNombre(legajo));

                    //Limpio Hiddden para q no lo tome despues en el envio y lo asociie nuevamente
                    HiddenFieldLegajoEvaluador.Value = string.Empty;
                    EjecutarScript(string.Format("document.getElementById('{0}').value ='';", HiddenFieldLegajoEvaluador.ClientID));

                }
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                FC.SetComentarioJefeTurno(TextBoxComentarioJT.Text);
                try
                {
                    GuardarItems(GridViewCompetencias);
                    GuardarItems(GridViewAspectos);
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);

                    NotificacionesController nc = new NotificacionesController();
                    FormulariosDS.CabeceraFormularioDataTable Cabe = FC.GetCabecera(int.Parse(PeriodoID.ToString()), int.Parse(legajo.ToString()), int.Parse(TipoFormularioID.ToString()), null);
                    ControllerUsuarios CU = new ControllerUsuarios();
                    DSUsuarios.UsuarioDataTable DTU = CU.GetUsuariosByLegajo(legajo);


                    string NombreEvaluador = Cabe.Rows[0].ItemArray[12].ToString();
                    string MailOperario = DTU.Rows[0].ItemArray[4].ToString();
                    string URLPMP;

                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=OPERARIOSE1&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];
                    string mensaje = nc.inicioEvaluacionOperario(MailOperario, NombreEvaluador, URLPMP);
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    EjecutarScript(string.Format("PreguntaImprimirEvaluacionV2({0},{1},{2})", legajo, PeriodoID, TipoFormularioID));
                    //Response.Redirect("~/UnauthorizedAccess.aspx");

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        private void DisableControls(Control c, bool habilita)
        {

            if ((c is WebControl) && ((c.GetType().Name == "RadioButton") || (c.GetType().Name == "CheckBox") || (c.GetType().Name == "TextBox")) && (c.GetType().Name != "GridView"))
                ((WebControl)c).Enabled = habilita;

            foreach (Control child in c.Controls)
                DisableControls(child, habilita);
        }


        //protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        //{

        //    if (Page.IsValid)
        //    {
        //        GuardadoPrevio();
        //        omb.ShowMessage("La Evaluación fue guardada satisfactoriamente");
        //    }
        //}
        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }


            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionV2({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            //EjecutarScript();
        }

        protected void ButtonExportar_Click(object sender, EventArgs e)
        {
            ButtonGSinEnviar_Click(ButtonGSinEnviar, null);
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ExportarEvaluacionPMPO({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);
            //EjecutarScript();

        }



    }
}