﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControlGuardadoPrevio.ascx.cs" Inherits="com.paginar.johnson.Web.PMP.pmp_WebUserControlGuardadoPrevio" %>
    <asp:Timer ID="TimerPMP" runat="server" Interval="30000" ontick="TimerPMP_Tick">
    </asp:Timer>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Label ID="UltimoBackup" runat="server" Text="Último backup:" 
            Visible="False"></asp:Label>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="TimerPMP" EventName="Tick" />
    </Triggers>
</asp:UpdatePanel>