﻿<%@ Page Theme="PMP" Title="" Language="C#" MasterPageFile="~/PMP/MasterPages/MP_PMPMY.Master" AutoEventWireup="true" CodeBehind="Form_PmpMYEnevaluado.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Form_PmpMYEnevaluado" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="OKMessageBox.ascx" tagname="OKMessageBox" tagprefix="uc1" %>
<%@ Register src="WebUserControlGuardadoPrevio.ascx" tagname="WebUserControlGuardadoPrevio" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH1" runat="Server">
    <asp:HiddenField ID="HiddenFieldPeriodoID" Value="1" runat="server" />
    <script type="text/javascript">
        function ImprimirEvaluacionMY(Legajo, PeriodoID, TipoFormularioID) {
            popup('ImpresionMY.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
        }

        function wordEvaluacionMY(Legajo, PeriodoID, TipoFormularioID) {
            popup('exportarWordMY.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
        }

        function popup(url, ancho, alto) {
            var posicion_x;
            var posicion_y;
            posicion_x = (screen.width / 2) - (ancho / 2);
            posicion_y = (screen.height / 2) - (alto / 2);
            window.open(url, "PmpOperarios_Print", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

        }

        function help(panel) {
            popup('Help.html?panel=' + panel, 600, 350);
        }

        var ModalProgress = '<%= ModalProgress.ClientID %>';
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

        function beginReq(sender, args) {

            // muestra el popup 

            //$find(ModalProgress).show();

        }



        function endReq(sender, args) {

            //  esconde el popup 

           // $find(ModalProgress).hide();

        }


        function ValidarObjetivos(sender, args) {

            var objetivos = $("[id*='RepeaterObjetivosAnt']").length;
            var adjunto = $("[id*='HyperLink14']").text();

            

            objetivos = objetivos / 4;

            


            if (objetivos >= 2 || adjunto !="" ) 
                args.IsValid = true;
            else
                args.IsValid = false;




        }

        function ValidarTextBoxComentario(sender, args) {

            var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
            var TextOpcional = "Puede fundamentar la fortaleza indicada";
            args.IsValid = true;
            var TextBoxComentario = sender.attributes["TextBoxComentario"].value;
            if (($(TextBoxComentario).val() == "") || ($(TextBoxComentario).val() == TextRequerido) || ($(TextBoxComentario).val() == TextOpcional)) {
                args.IsValid = false;
            }            
            }



        function ValidarRadiosEnGrillaCompetencias(sender, args) {
            
            args.IsValid = ValidarRadiosEnGrilla("<%= GridViewCompetencias.ClientID %>");
            
        }
      
        function SetearPagina() {
            if (!Page_ClientValidate('EnviarEvaluado')) {
                SetearGrilla("<%= GridViewCompetencias.ClientID %>");
                
                //code that need to be executed if page is validated

            }
        }

        function cambiarflag() {
            flagCierre = 3;
         }

        function ValidarTextBoxFundamentacion(sender, args) {

            var TextRequerido = "Fundamente la oportunidad de mejora indicada (campo obligatorio)...";
            var TextOpcional = "Puede fundamentar la fortaleza indicada";
            args.IsValid = true;

            var RBFortaleza = sender.attributes["RBFortaleza"].value;
            var RBAreaDesarrollo = sender.attributes["RBAreaDesarrollo"].value;
            var TextBoxFundamentacion = sender.attributes["TextBoxFundamentacion"].value;

            if ($(RBAreaDesarrollo).is(':checked')) {
                if (($(TextBoxFundamentacion).val() == "") || ($(TextBoxFundamentacion).val() == TextRequerido) || ($(TextBoxFundamentacion).val() == TextOpcional)) {
                    args.IsValid = false;
                }

            }
        }

        function ClickEnvio() {
            if (Page_ClientValidate('EnviarEvaluador')) {
                var VarCheckAreasdeInteres = CheckAreasdeInteres();
                if (!VarCheckAreasdeInteres) return VarCheckAreasdeInteres;
                var VarChequeaComentario = CheckComentario('ctl00_CPH1_FormViewFormulario_TextBoxComentarioEv');
                if (!VarChequeaComentario) return VarChequeaComentario;
                return (VarChequeaComentario && VarCheckAreasdeInteres); 
            }
        }

        window.onbeforeunload = confirmOnClose;
/*
        var inFormOrLink;
        inFormOrLink = false;
        $(window).bind("beforeunload", function () {
            return inFormOrLink || (confirm("Si el formulario no fue guardado es probable que pierda los datos"));
        })

*/
 
        //Cieerre de X (2), Cierre Guardar sin enviar (1), Cierre por envío a evaluador (3)

        var flagCierre = 2;

        function confirmOnClose() {            
            if (flagCierre == 1) {
                flagCierre = 2;
                return "Usted retornará al Panel de Control."; //Cierre Guardar sin enviar 1
            }
            else {
                if (flagCierre == 2) {
                    return 'Si el formulario no fue guardado es probable que pierda los datos'; // Cieerre de X
                }
                else if (flagCierre == 3) {

                    //PreguntaImprimirEvaluacion($("#<%= HiddenFieldlegajo.ClientID %>").val(), $("#<%= HiddenFieldPeriodoID.ClientID %>").val(), $("#<%= HiddenFieldTipoFormularioID.ClientID %>").val());
                 }
            }
        }

        function GuardarSinEnviar() {
            flagCierre = 1;
                    window.close();
                }


     

        function HidePopupEvaluador() {

            $find('<%= PanelPopEvaluador_ModalPopupExtender.ClientID %>').hide();

        }





    </script>
    <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldPasoID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldClusterID" Value="" runat="server" />

    <asp:HiddenField ID="HiddenFieldPasoIDInicio" runat="server" />

    <asp:Panel Visible="false" CssClass="mensaje exito" ID="PanelExito" runat="server">
        El Formulario fue enviado exitosamente
    </asp:Panel>
    <asp:ValidationSummary ID="ValidationSummaryFormulario" runat="server" ValidationGroup="NOHABILITADO"
        CssClass="mensaje error" />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="GrupoImprimir" />
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="EnviarEvaluador"
        CssClass="mensaje error" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:WebUserControlGuardadoPrevio EnabledTimer="true" ID="WUCGuardadoPrevio" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="PanelPopEvaluador" CssClass="contentPopupEvaluador" style="display:none" runat="server"> 
        <asp:Panel CssClass="titleModalPopup" ID="titleBarPopEval" runat="server">
            <h2>Seleccione su evaluador</h2>
            <span id="btnCloseLE" class="closeModalPopup" style=" display:none;">Cerrar</span>
        </asp:Panel>	
		<div class="contentModalPopup">

   <%--     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
             <cc1:ListSearchExtender id="ListSearchExtender1" runat="server"
                    TargetControlID="DropDownEvaluadores"
                    PromptCssClass="ListSearchExtenderPrompt"
                    PromptText="Escriba para buscar"/> 
             <asp:DropDownList ID="DropDownEvaluadores" runat="server" 
                DataSourceID="ObjectDataGetEvaluators" 
                DataTextField="ApellidoNombre" DataValueField="Legajo" 
                ondatabound="DropDownEvaluadores_DataBound" AppendDataBoundItems="true">
                <asp:ListItem Text="-Seleccione-" Value=""></asp:ListItem>
                 </asp:DropDownList>               
            <asp:RequiredFieldValidator ControlToValidate="DropDownEvaluadores" ID="RequiredFieldValidator11"
                runat="server" ErrorMessage="Debe Seleccionar un evaluador válido" ValidationGroup="CambiarEvaluador">*</asp:RequiredFieldValidator>
             <asp:ObjectDataSource ID="ObjectDataGetEvaluators" runat="server"
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetEvaluadoresByPeriodoID"
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />                    
                     <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <br />
            <br />
            <br />
            <asp:Label ID="lblEvaluadorAsignado" runat="server" Text=""></asp:Label>
            <br />           
            <br />
            <br />
            <asp:Button ID="GuardarNuevoEvaluador" runat="server" Text="Actualizar" ValidationGroup="CambiarEvaluador"
                onclick="GuardarNuevoEvaluador_Click" 
                onclientclick="javascript:document.body.onbeforeunload='';" />
            <asp:Button ID="GuardarNuevoEvaluadorNuevo" runat="server" Text="Actualizar" ValidationGroup="CambiarEvaluador"
                onclick="GuardarNuevoEvaluadorNuevo_Click" />
  <%--      </ContentTemplate>
        </asp:UpdatePanel>--%>

      
		</div>
    </asp:Panel>	
	  
    <asp:Button ID="btnChangeEvaluator" runat="server" Visible="true" Text="X" style="display:none"/>
    <cc1:ModalPopupExtender ID="PanelPopEvaluador_ModalPopupExtender" runat="server"
        TargetControlID="btnChangeEvaluator"
        OkControlID="btnCloseLE" 
        BackgroundCssClass="modalBackground" PopupControlID="PanelPopEvaluador">
    </cc1:ModalPopupExtender>


    <br />
    <br />
    <h2 class="hide">
        Evaluación</h2>
    <div id="tabs">
        <ul>
            <li id="t1"><a href="#tabs-1">A- Logros</a></li>
            <li id="t2"><a href="#tabs-2">B- Comportamientos</a></li>
            <li id="t3"><a href="#tabs-3">C- Competencias</a></li>
            <li id="t4"><a href="#tabs-4">D- Oportunidades de Desarrollo</a></li>
        </ul>
        <div id="tabs-1">
            <h3>1º Semestre</h3>
            <a class="help" href="#" onclick="help('A');">Ayuda</a>

            <asp:Repeater ID="RepeaterLogros" runat="server" DataSourceID="ObjectDataSourceLogros"
                OnItemDataBound="RepeaterLogros_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                            <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>' Visible="false"></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <br></br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:TextBox ID="TextBoxComentario" runat="server" TextMode="MultiLine" Text='<%# Bind("Fundamentacion") %>'
                                Rows="5" SkinID="form-textarea" rel="logros" OnBlur="validarpestanas()"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidatorTextBoxComentario" runat="server" Text="*"
                                ClientValidationFunction="ValidarTextBoxComentario" ValidationGroup="EnviarEvaluador"></asp:CustomValidator>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceLogros" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="3" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div id="tabs-2">
            <a class="help" href="#" onclick="help('B');">Ayuda</a>

            <asp:Repeater ID="RepeaterComportamientos" runat="server" DataSourceID="ObjectDataSourceComportamientos"
                OnItemDataBound="RepeaterComportamientos_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <br></br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:TextBox ID="TextBoxComentario" runat="server" TextMode="MultiLine" Text='<%# Bind("Fundamentacion") %>'
                                Rows="5" SkinID="form-textarea" rel="comportamiento" OnBlur="checkcomportamientos()"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidatorTextBoxComentario" runat="server" Text="*"
                                ClientValidationFunction="ValidarTextBoxComentario" ValidationGroup="EnviarEvaluador"></asp:CustomValidator>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceComportamientos" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="4" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div id="tabs-3">
            <h3>Leadership Imperatives</h3>
            <a class="help" href="#" onclick="help('C');">Ayuda</a>

            <br />
            <asp:GridView ID="GridViewCompetencias" runat="server" AutoGenerateColumns="False"
                DataKeyNames="ItemEvaluacionID" 
                DataSourceID="ObjectDataSourceCompetencias" OnRowDataBound="GridViewCompetencias_RowDataBound"
                Width="100%" OnRowCreated="GridViewCompetencias_RowCreated">
                <Columns>
                    <asp:TemplateField HeaderText="Leadership Imperatives">
                        <ItemTemplate>
                            <div class="titulo">
                                <asp:HyperLink ID="HyperLink1" CssClass="masInfo" runat="server" ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'>
                                    <asp:Label ID="LabelTitulo" runat="server" Text='<%# Eval("Titulo") %> '></asp:Label>&nbsp;&nbsp;
                                    <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                                </asp:HyperLink>
                            </div>
                            <div class="detalle">
                                <%--<asp:Label ID="Label2" runat="server" Text='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'></asp:Label>--%>
                                <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            </div>
                        </ItemTemplate>
                        <ItemStyle CssClass="columnEmphasis" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fortalezas">
                        <ItemTemplate>
                            <asp:RadioButton ID="RBFortaleza" runat="server" GroupName='<%# Eval("ItemEvaluacionID") %>'
                                  Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),1) %>' rel="rbfortaleza" />
                        </ItemTemplate>
                        <ItemStyle CssClass="alignCenter" Width="60" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:RadioButton ID="RBAreaDesarrollo" runat="server" GroupName='<%# Eval("ItemEvaluacionID") %>'
                                 Checked='<%# GetCheckRadiosItems(Eval("Ponderacion"),2) %>' rel="desarrollo"   />
                        </ItemTemplate>
                        <ItemStyle CssClass="alignCenter" Width="60" />
                        <HeaderTemplate>
                            <asp:Label ID="lblHeader" runat="server" Text="Área de <br/> Desarrollo"></asp:Label>
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comentarios">
                        <ItemTemplate>
                            <asp:TextBox ID="TextBoxFundamentacion" runat="server" Text='<%# Bind("Fundamentacion") %>'
                                TextMode="MultiLine" MaxLength="5" Width="480px"  onBlur = "checkLeadershipImperatives()" ></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidatorTextBoxFundamentacion" runat="server" Text="*"
                                ClientValidationFunction="ValidarTextBoxFundamentacion" ValidationGroup="EnviarEvaluador"></asp:CustomValidator>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="480px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSourceCompetencias" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="1" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div id="tabs-4">
            <a class="help" href="#" onclick="help('D');">Ayuda</a>

            <asp:Repeater ID="RepeaterOportunidadesDesarrollo" runat="server" DataSourceID="ObjectDataSourceOportunidadesDesarrollo"
                OnItemDataBound="RepeaterOportunidadesDesarrollo_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <h3>
                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label></h3>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:TextBox ID="TextBoxComentario" runat="server" TextMode="MultiLine" Text='<%# Bind("Fundamentacion") %>'
                                Rows="5" SkinID="form-textarea" rel="oportunidades" onBlur="validarOportunidades()"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidatorTextBoxComentario" runat="server" Text="*"
                                ClientValidationFunction="ValidarTextBoxComentario" ValidationGroup="EnviarEvaluador"
                                Enabled="false"></asp:CustomValidator>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceOportunidadesDesarrollo" runat="server"
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion"
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="5" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </div>
    <!--/tabs-->
    <asp:CustomValidator ID="CustomValidatorGrillaCompetencias" ClientValidationFunction="ValidarRadiosEnGrillaCompetencias"
        runat="server" ErrorMessage="Faltan Seleccionar ítems en la Grilla de Competencias"
        ValidationGroup="EnviarEvaluador">&nbsp;</asp:CustomValidator>
    <br />

    <asp:CustomValidator ID="CustomValidatorObjetivos"  ClientValidationFunction="ValidarObjetivos"
          runat="server" ErrorMessage="Recordá que es obligatorio cargar tus objetivos para enviar la autoevaluación. Debes como mínimo tener 2 objetivos cargados, o bien, un archivo adjunto. "
        ValidationGroup="EnviarEvaluador">&nbsp;</asp:CustomValidator>
    <br />



    <div id="acciones">
        <asp:UpdatePanel ID="upPnlPage" runat="server">
            <ContentTemplate>
                <span class="button">
                                        
                    <asp:Button ValidationGroup="EnviarEvaluador" 
                    ID="ButtonEnviarEvaluadorPrimero" runat="server"
                        Text="Enviar al Evaluador" EnableTheming="false" 
                    CssClass="formSubmitAsync"                        
                    ToolTip="Guarda la Evaluación dejando la misma disponible para el Evaluador. El Evaluado no puede realizar más cambios" 
                    OnClientClick="cambiarflag();"
                    onclick="ButtonEnviarEvaluadorPrimero_Click" />
                    <%--
                    onclientclick="flagCierre = 3;" 
                    <asp:Button ValidationGroup="EnviarEvaluador" ID="ButtonEnviarEvaluador" runat="server"
                        Text="Enviar al Evaluador" EnableTheming="false" 
                    CssClass="formSubmitAsync" OnClick="ButtonEnviarEvaluador_Click"
                    style="display: none"                        
                    ToolTip="Guarda la Evaluación dejando la misma disponible para el Evaluador. El Evaluado no puede realizar más cambios" 
                    onclientclick="flagCierre = 3;" />--%></span><!---->
                <span class="button">
                    <asp:Button ID="ButtonGSinEnviar" runat="server" Text="Guardar sin enviar" OnClick="ButtonGSinEnviar_Click"
                        EnableTheming="false" CssClass="formSubmitAsync" ToolTip="Guarda la evaluación sin enviar la misma al Evaluador"  /></span>
                <span class="button">
                    <asp:Button ID="ButtonImprimir" runat="server" Text="Imprimir" ValidationGroup=""
                        ToolTip="Visualiza el formulario en modo de Impresión con los datos volcados en la base de datos"
                        CssClass="formSubmitAsync" EnableTheming="false" OnClick="ButtonImprimir_Click" />
                </span>
                <span class="button">
                <asp:Button ID="ButtonDoc" runat="server" CssClass="formSubmitAsync" 
                    EnableTheming="false" onclick="ButtonDoc_Click" Text="Exportar" />
                    </span>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <asp:UpdatePanel ID="UpdatePanelExito" runat="server">
        <ContentTemplate>
            <uc1:OKMessageBox ID="omb" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonEnviarEvaluadorPrimero" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="upPnlPage">
            <ProgressTemplate>
                <div class="precarga">
                    <asp:Image AlternateText="Procesando" ID="ImgProc" ImageUrl="~/pmp/img/ajax-loader.gif"
                        runat="server" />
                    <p>
                        Operación en progreso...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    <asp:HiddenField ID="HiddenFieldJS" runat="server" />
 
</asp:Content>
