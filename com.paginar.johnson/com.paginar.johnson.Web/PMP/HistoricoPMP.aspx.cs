﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;

namespace com.paginar.johnson.Web.PMP
{
    public partial class HistoricoPMP : System.Web.UI.Page
    {
        int legajo = 0;
        int PeriodoID = 0;
        int TipoFormularioID = 0;

        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void CargarValoresQueryString()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["LegajoEvaluador"]))
            {
                HiddenFieldLegajoEvaluador.Value = Request.QueryString["LegajoEvaluador"];
            }

            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
        }
    }
}