﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Objetivos.aspx.cs" Inherits="com.paginar.johnson.Web.PMP.Objetivos" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title></title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.maxlength-min.js"></script>
    <style type="text/css">
		.buttons{
			margin-bottom:15px;
			overflow:hidden;
			clear:both;
			padding: 2%;
		}
    
    .status
    {
      color:#666666;
      font-size:10px;
          
        }
    </style>
    

    <script type="text/javascript">
        $(document).ready(function () {

            TextArea('<%=TextBoxObjetivo.ClientID %>', 499);
            TextArea('<%=TextBoxMedida.ClientID %>', 499);
            TextArea('<%=TextBoxResultado.ClientID %>', 499);

        });


        function TextArea(txtArea, maxlength) {
            //tamano maximio de caracteres en textarea
            $('#' + txtArea).maxlength({
                events: [], // Array of events to be triggerd    
                maxCharacters: maxlength, // Characters limit   
                status: true, // True to show status indicator bewlow the element    
                statusClass: "status", // The class on the status div  
                statusText: "caracteres disponibles", // The status text  
                notificationClass: "notification", // Will be added when maxlength is reached  
                showAlert: false, // True to show a regular alert message    
                alertText: "You have typed too many characters.", // Text in alert message   
                slider: false // True Use counter slider    
            });
        }

    </script>

</head>
<body runat="server" ID="Body">
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="buttons objetivosTop"> 

    <h1><asp:Label ID="LabelTitulo" runat="server" Text="Agregar Objetivo"></asp:Label></h1>
    
        <asp:HiddenField ID="HiddenFieldIndice" runat="server" />
    <div>
        <label class="descripcion">Objetivo</label>
    </div>
    <div  class="CargaObjetivos">
        <asp:TextBox ID="TextBoxObjetivo" runat="server" Rows="6" TextMode="MultiLine" 
            MaxLength="490"></asp:TextBox>

            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
          <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "TextBoxObjetivo" 
          ID="RegularExpressionValidator2" ValidationExpression = "^[\s\S]{499,}$" runat="server" ErrorMessage="Solo se permite un máximo 499 carácteres"></asp:RegularExpressionValidator>
        
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorObjetivo"   runat="server" ControlToValidate="TextBoxObjetivo" 
        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="guardar">Debe Completar el campo Objetivo</asp:RequiredFieldValidator>
    
    </div>
	</br>
    <div>
        <label>Medida</label>
   </div>
    <div class="CargaObjetivos">
        <asp:TextBox ID="TextBoxMedida" runat="server" Rows="6" TextMode="MultiLine"></asp:TextBox>
         
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorMedida"   runat="server" ControlToValidate="TextBoxMedida" 
        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="guardar">Debe Completar el campo Medida</asp:RequiredFieldValidator>
      </div>
	</br>
    <div>
        <label>Resultado</label>

    </div>
    <div class="CargaObjetivos">
        <asp:TextBox ID="TextBoxResultado" runat="server" Rows="6" TextMode="MultiLine"></asp:TextBox>
         
<%--        <asp:RequiredFieldValidator ID="RequiredFieldValidatorResultado" runat="server" ControlToValidate="TextBoxResultado" 
        Display="Dynamic" ErrorMessage="RequiredFieldValidator" ValidationGroup="guardar">Debe Completar el campo Resultado</asp:RequiredFieldValidator>
    --%>
    </div>

    </div>
<div class="buttons">
    <asp:Button ID="ButtonAgregar" runat="server" Text="Agregar"  ValidationGroup="guardar"
        onclick="ButtonAgregar_Click" />
    <asp:Button ID="ButtonCancelar" runat="server" Text="Cancelar" OnClientClick="javascript:window.close()" />
</div>   
</form>
</body>
</html>
