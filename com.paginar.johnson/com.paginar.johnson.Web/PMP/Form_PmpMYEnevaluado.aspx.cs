﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.formularios.dataaccesslayer;
using System.Web.UI.HtmlControls;
using System.Collections;
using com.paginar.johnson.membership;
using System.Text;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Reflection;
using System.ComponentModel;


namespace com.paginar.johnson.Web.PMP
{
    public partial class Form_PmpMYEnevaluado : PageBase
    {
        string ScriptJs = string.Empty;
        private bool SoloLectura
        {
            get { return (bool)ViewState["SoloLectura"]; }
            set { ViewState["SoloLectura"] = value; }
        }
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }

        private LogController _fle;
        public LogController fle 
        {
            get { 
                _fle = (LogController)this.Session["fle"];
                if (_fle == null)
                {
                    _fle = new LogController();
                    this.Session["fle"] = _fle;
                }
                return _fle;
            }
        }

        private SincronizadorScj _SScj;
        public SincronizadorScj SScj
        {
            get
            {
                _SScj = (SincronizadorScj)this.Session["_SScj"];
                if (_SScj == null)
                {
                    _SScj = new SincronizadorScj();
                    this.Session["_SScj"] = _SScj;
                }
                return _SScj;
            }
            set
            {
                _SScj = value;

            }
        }

        protected string DefaultVal(object val, string Default)
        {

            //if ((val == System.DBNull.Value) || (val == string.Empty))
            //    return (Default);
            //else
            //    return (val.ToString());


            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }
        protected string DefaultValComentarios(object val, string Default)
        {
            if (((val == System.DBNull.Value) || (val == null)))
                return string.Empty;
            if (val == string.Empty)
                return Default;
            return (val.ToString());

        }

        void omb_OkButtonPressed(object sender, EventArgs args)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }

        void WUCGuardadoPrevio_GuardadoPrevio()
        {
            GuardadoPrevio();
        }
        protected bool ValidaPeriodo(int periodoid)
        {
            FormulariosController f = new FormulariosController();

            return ((bool)f.ValidarPeriodoCarga(periodoid, DateTime.Now));
        }

        bool nocargojs;

        protected void Page_Load(object sender, EventArgs e)
        {

            nocargojs = true;
            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);
            WUCGuardadoPrevio.GuardadoPrevio += new pmp_WebUserControlGuardadoPrevio.GuardadoPrevioHandler(WUCGuardadoPrevio_GuardadoPrevio);
            Usuarios usercontroller = new Usuarios();
             FormulariosController f = new FormulariosController();
            User Usuario = null;
            int? PasoActualID; // = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
            if (!Page.IsPostBack)
            {
                
                
                HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
                HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
                HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;
                HiddenFieldClusterID.Value = ObjectUsuario.clusterID.ToString(); ;
                //pop up evaluador
                
                if (string.IsNullOrEmpty(HiddenFieldlegajo.Value) && string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value) && string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                    Response.Redirect("EvaluacionError.aspx");

                if(!f.ExisteEvaluacion(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value),int.Parse( HiddenFieldTipoFormularioID.Value)))
                    Response.Redirect("EvaluacionError.aspx");

                PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));

                if (PasoActualID == -1)
                {
                    HiddenFieldPasoIDInicio.Value = "-1";
                    PanelPopEvaluador_ModalPopupExtender.Show();
                    WUCGuardadoPrevio.EnabledTimer = false;
                    GuardarNuevoEvaluadorNuevo.Visible = false;
                }
                else 
                {
                    HiddenFieldPasoIDInicio.Value = "10";
                    GuardarNuevoEvaluador.Visible = false;
                }

                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                //string logros;
                //string comportamiento;
                //string oportunidadesmejora;
                //if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                //{
                //    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                //}
                //if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                //{
                //    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                //}
                //if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                //{
                //    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                //}
                
                //ButtonEnviarEvaluadorPrimero.OnClientClick = string.Format("flagCierre = 3;PreguntaImprimirEvaluacion({0}, {1}, {2});", legajo, PeriodoID, TipoFormularioID);
                ButtonEnviarEvaluadorPrimero.OnClientClick = "flagCierre = 3;";

            }
            //ObjectDataGetEvaluators.DataBind();
            //DropDownEvaluadores.Items.Clear();
            //DropDownEvaluadores.Items.Add(new ListItem("-Seleccione-", "-Seleccione-"));
            //DropDownEvaluadores.DataBind();

           
            //DisableControls(GridViewCompetencias, false);
            //try
            //{
               Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                int LegajoLog= Usuario.GetLegajo();
                PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
                PasoActualID = (PasoActualID == -1) ? 2 : PasoActualID;

                SoloLectura = false;// (!(PasoActualID == 2) || !IsPeriodoAbierto);
                bool PuedeEditar = (LegajoLog == int.Parse(HiddenFieldlegajo.Value) && PasoActualID == 2);
                if (SoloLectura)
                {                  
                  
                  //  ButtonEnviarEvaluadorPrimero.Visible = false;
                    ButtonGSinEnviar.Visible = false;
                    DisableRepeater(RepeaterComportamientos, false);
                    DisableRepeater(RepeaterOportunidadesDesarrollo, true);//valor original false
                    DisableRepeater(RepeaterLogros, false);
                    WUCGuardadoPrevio.EnabledTimer = false;


                }//si no es de solo lectura actualizo
                else
                {
                    if (!PuedeEditar && !Page.IsPostBack)
                        Response.Redirect("~/UnauthorizedAccess.aspx");
                    
                    HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
                    //BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.reload();");
                    BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx?asignados=2');");
                    
                        
                }
            //}
          //  catch (Exception)
            //{

                //throw;
           // }
            //;
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
            bool IsPeriodoAbierto = ValidaPeriodo(int.Parse(HiddenFieldPeriodoID.Value));
            SoloLectura = (!(PasoActualID == 2 || PasoActualID == -1)) || (!IsPeriodoAbierto);

            if (SoloLectura)
            {
                DisableRepeater(RepeaterLogros, false);
                DisableRepeater(RepeaterComportamientos, false);
                DisableRepeater(RepeaterOportunidadesDesarrollo, false);
                DisableGrilla(GridViewCompetencias, false);
                WUCGuardadoPrevio.EnabledTimer = false;
            }
            
               
        }

        public bool GetCheckRadiosItems(object Valor, int ValorRadio)
        {
            if (Valor == System.DBNull.Value) return false;
            int ValorAux = int.Parse(Valor.ToString());
            return (ValorAux == ValorRadio);
            //return true;

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void GridViewCompetencias_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          //if (e.Row.RowType == DataControlRowType.Header)
          //  {

          //  SortedList FormatCells = new SortedList();
          //  FormatCells.Add("1", ",1,2");
          //  FormatCells.Add("2", "Ratings,2,1");
          //  FormatCells.Add("3", ",1,2");


          //  SortedList FormatCells2 = new SortedList();
          //  FormatCells2 .Add("1", "Subgroup1,1,1");
          //  FormatCells2 .Add("2", "Subgroup2,1,1");            
          //  GetMultiRowHeader(e, FormatCells2 );
          //  GetMultiRowHeader(e, FormatCells );
          //}
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                CustomValidator CustomValidatorTextBoxFundamentacion = e.Row.FindControl("CustomValidatorTextBoxFundamentacion") as CustomValidator;
                TextBox TextBoxFundamentacion = e.Row.FindControl("TextBoxFundamentacion") as TextBox;
                Label LabelTitulo = e.Row.FindControl("LabelTitulo") as Label;
                RadioButton RBFortaleza = e.Row.FindControl("RBFortaleza") as RadioButton;
                RadioButton RBAreaDesarrollo = e.Row.FindControl("RBAreaDesarrollo") as RadioButton;


                if (RBFortaleza.Checked)
                    TextBoxFundamentacion.CssClass = "opcional";

                //if ((RBFortaleza.Checked  && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                //    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");


                if (RBAreaDesarrollo.Checked)
                    TextBoxFundamentacion.CssClass = "requerido";
                //if ((RBAreaDesarrollo.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty) && (!SoloLectura))
                //    HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");

                //RBFortaleza.Attributes.Add("onClick", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                //RBAreaDesarrollo.Attributes.Add("onClick", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                //TextBoxFundamentacion.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID));
                if (nocargojs)
                {
                    string elscript = "$(document).ready(function() {checkLeadershipImperatives(); ProcesarComentarios('" + TextBoxFundamentacion.ClientID + "','" + RBAreaDesarrollo.ClientID + "','" + RBFortaleza.ClientID + "') });";
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "FSR544", elscript, true);
                    nocargojs = false;
                }
                //RBFortaleza.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}','{2}')", TextBoxFundamentacion.ClientID, RBFortaleza.ClientID, RBAreaDesarrollo.ClientID));
                //RBAreaDesarrollo.Attributes.Add("onBlur", string.Format("ProcesarComentarios('{0}','{1}','{2}')", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID, RBFortaleza.ClientID));

                RBFortaleza.Attributes.Add("OnClick", string.Format("CambiarClase('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional") + string.Format("ProcesarComentarios('{0}','{1}','{2}');", TextBoxFundamentacion.ClientID, RBFortaleza.ClientID, RBAreaDesarrollo.ClientID));
                RBAreaDesarrollo.Attributes.Add("OnClick", string.Format("ProcesarComentarios('{0}','{1}','{2}');", TextBoxFundamentacion.ClientID, RBAreaDesarrollo.ClientID, RBFortaleza.ClientID) + string.Format("CambiarClase('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido"));
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBFortaleza", "#" + RBFortaleza.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("RBAreaDesarrollo", "#" + RBAreaDesarrollo.ClientID);
                CustomValidatorTextBoxFundamentacion.Attributes.Add("TextBoxFundamentacion", "#" + TextBoxFundamentacion.ClientID);
                CustomValidatorTextBoxFundamentacion.ErrorMessage = "No completó el comentario de la Competencia: " + LabelTitulo.Text + ", solapa C- Competencias";
                 
            }
        }

        public void GetMultiRowHeader(GridViewRowEventArgs e, SortedList GetCels)
        {

                GridViewRow row;
                IDictionaryEnumerator enumCels = GetCels.GetEnumerator();

                row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
                while (enumCels.MoveNext())
                {

                    string[] count = enumCels.Value.ToString().Split(Convert.ToChar(","));
                    TableCell Cell;
                    Cell = new TableCell();
                    Cell.RowSpan = Convert.ToInt16(count[2].ToString());
                    Cell.ColumnSpan = Convert.ToInt16(count[1].ToString());
                    Cell.Controls.Add(new LiteralControl(count[0].ToString()));
                    Cell.HorizontalAlign = HorizontalAlign.Center;
                    Cell.ForeColor = System.Drawing.Color.Black;
                   // Cell.ForeColor = System.Drawing.Color.White;
                    row.Cells.Add(Cell);
                }


                e.Row.Parent.Controls.AddAt(0, row);

            

        } 


    
        private void GuardarItems(GridView GridItems)
        {
            foreach (GridViewRow fila in GridItems.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    HiddenField HFID = fila.FindControl("HFID") as HiddenField;
                    ItemEvaluacionID = int.Parse(HFID.Value);

                    RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                    RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;

                    if (RBFortaleza.Checked)
                        Ponderacion = 1;
                    if (RBAreaDesarrollo.Checked)
                        Ponderacion = 2;
                   
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
                }
            }
        }

        
        protected void CustomValidatorCompetencias_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidarGrilla(GridViewCompetencias);
        }


 

        private bool ValidarGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBMB = fila.FindControl("RBMB") as RadioButton;
                    RadioButton RBB = fila.FindControl("RBB") as RadioButton;
                    RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    if ((!RBMB.Checked) && (!RBB.Checked) && (!RBR.Checked) && (!RBNS.Checked))
                    {
                        resultado = false;
                        break;
                    }

                }

            }
            return resultado;
        }

        private bool SetearTextBoxGrilla(GridView grilla)
        {
            bool resultado = true;
            foreach (GridViewRow fila in grilla.Rows)
            {
                if (fila.RowType == DataControlRowType.DataRow)
                {
                    RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                    RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;
                    //RadioButton RBR = fila.FindControl("RBR") as RadioButton;
                    //RadioButton RBNS = fila.FindControl("RBNS") as RadioButton;
                    TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                    if ((RBFortaleza.Checked ) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "opcional");
                    if ((RBAreaDesarrollo.Checked) && (TextBoxFundamentacion.Text.Trim() == string.Empty))
                        HiddenFieldJS.Value += string.Format("SeteoValores('#{0}','{1}');", TextBoxFundamentacion.ClientID, "requerido");


                }

            }
            return resultado;
        }

        private void DisableGrilla(GridView grilla, bool habilita)
        {
             foreach (GridViewRow fila in grilla.Rows)
            {
                TextBox TextBoxFundamentacion = fila.FindControl("TextBoxFundamentacion") as TextBox;
                TextBoxFundamentacion.ReadOnly = !habilita;

                RadioButton RBFortaleza = fila.FindControl("RBFortaleza") as RadioButton;
                RBFortaleza.Enabled = habilita;

                RadioButton RBAreaDesarrollo = fila.FindControl("RBAreaDesarrollo") as RadioButton;
                RBAreaDesarrollo.Enabled = habilita;

            }
        }


        private void DisableControls(Control c, bool habilita)
        {

            if ((c is WebControl) && ((c.GetType().Name == "RadioButton") || (c.GetType().Name == "CheckBox") || (c.GetType().Name == "TextBox")) && (c.GetType().Name != "GridView"))
                ((WebControl)c).Enabled = habilita;

            foreach (Control child in c.Controls)
                DisableControls(child, habilita);
        }
        protected void ButtonEnviarEvaluador_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                //string logros;
                //string comportamiento;
                //string oportunidadesmejora;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }              
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                NotificacionesController n = new NotificacionesController();
                int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);  

                ///El evaluador esta deshabilitado?
                ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
                DSUsuarios.UsuarioDataTable Evaluador = new DSUsuarios.UsuarioDataTable();

                int Estado = 0;

                Evaluador = ControllerUsuarios1.GetUsuariosByLegajo(legajoevaluador);
                

                foreach (DSUsuarios.UsuarioRow EvaluadorRow in Evaluador.Rows)
                {
                    Estado = EvaluadorRow.Estado;
                 }

                if (Estado != 2)
                {
                    WUCGuardadoPrevio.EnabledTimer = false;
                    ObjectDataGetEvaluators.DataBind();
                    DropDownEvaluadores.Items.Clear();
                    DropDownEvaluadores.Items.Add(new ListItem("-Seleccione-", ""));
                    DropDownEvaluadores.DataBind();
                    PanelPopEvaluador_ModalPopupExtender.Show();
                    return;
                }
                
               
                try
                {
                    //EjecutarScript("flagCierre = 3;");//envio agregado
                    WUCGuardadoPrevio.EnabledTimer = false;

                    GuardarItemsRepeater(RepeaterLogros);
                    GuardarItemsRepeater(RepeaterComportamientos);
                    GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                    GuardarItems(GridViewCompetencias);
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);

                                    
                   
                    string mailEvaluador = UE.GetMail(legajoevaluador);
                    string envio;
                    string urlPMPMY;
                    urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=1&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];

                    if (!string.IsNullOrEmpty(mailEvaluador))
                        envio = n.envioEvaluadorMYV2(mailEvaluador, UE.GetUsuarioNombre(legajo), GetComentario(RepeaterLogros), GetComentario(RepeaterComportamientos), GetComentario(RepeaterOportunidadesDesarrollo), urlPMPMY);

                    
                    // if (envio == "true") 
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    //EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    //else
                    //  omb.ShowMessage(envio);


                }
                catch (Exception exc)
                {

                    omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                    // omb.ShowMessage(exc.Message);
                }

            }
        }
        protected void ButtonVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/servicios/Tramites.aspx");
        }
        protected string DefaultValComentarios(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("S/C");
            else
                return val.ToString();

            return string.Empty;
        }

        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                GuardadoPrevio();
                EjecutarScript("GuardarSinEnviar()");

            }
        }

        private void GuardadoPrevio()
        {
            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            int LegajoEvaluador = 0;
            HiddenField HiddenFieldLegajoEvaluador = (HiddenField)this.Master.FindControl("HiddenFieldLegajoEvaluador");
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
          

            FormulariosDS.RelEvaluadosEvaluadoresDataTable RelEvaluadosEvaluadoresDt = new FormulariosDS.RelEvaluadosEvaluadoresDataTable();
            FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByFormularioID(RelEvaluadosEvaluadoresDt, legajo, PeriodoID, TipoFormularioID);
            RelEvaluadosEvaluadoresDt.Select("PasoID=2");
            LegajoEvaluador = int.Parse(RelEvaluadosEvaluadoresDt.Rows[0]["LegajoEvaluador"].ToString());
           

            if (!string.IsNullOrEmpty(RelEvaluadosEvaluadoresDt.Rows[0]["LegajoEvaluador"].ToString()))
            {
                FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
                //Limpio Hiddden para q no lo tome despues en el envio y lo asociie nuevamente
                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                NotificacionesController n = new NotificacionesController();               
            }
            FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
           
            try
            {               
                GuardarItems(GridViewCompetencias);
                GuardarItemsRepeater(RepeaterLogros);
                GuardarItemsRepeater(RepeaterComportamientos);
                GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                FC.GrabarEvaluador();
                FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);

                //omb.ShowMessage("La Evaluación fue Grabada satisfactoriamente");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GuardarItemsRepeater(Repeater RptItems)
        {
           foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)

            {
               
                    int ItemEvaluacionID = 0;
                    int Ponderacion = 0;
                    string Fundamentacion = string.Empty;

                    HiddenField HFID = item.FindControl("HFID") as HiddenField;
                    ItemEvaluacionID = int.Parse(HFID.Value);

                    TextBox TextBoxFundamentacion = item.FindControl("TextBoxComentario") as TextBox;
                    Fundamentacion = TextBoxFundamentacion.Text;
                    FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);
               
            }
        }


        public void AddExtraHeader()
        {

           
                SortedList cellSortedList = new SortedList();

                cellSortedList.Add("1", "<div  align=center>FCE(*)</div>,1,2");
                cellSortedList.Add("2", "<div  align=center> Ratings </div>,2,1");
                cellSortedList.Add("3", "<div  align=center>Comentarios</div>,1,2");
                

                IDictionaryEnumerator enumCells = cellSortedList.GetEnumerator();
                GridViewRow row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal); while (enumCells.MoveNext())
                {

                    string[] cont = enumCells.Value.ToString().Split(Convert.ToChar(","));
                    TableCell Cell;

                    Cell = new TableCell();
                    Cell.RowSpan = Convert.ToInt16(cont[2].ToString());

                    Cell.ColumnSpan = Convert.ToInt16(cont[1].ToString());
                    Cell.Controls.Add(new LiteralControl(cont[0].ToString()));

                    //Cell.HorizontalAlign = HorizontalAlign.Center;
                    //Cell.ForeColor = System.Drawing.Color.Black;
                    //Cell.BackColor = System.Drawing.Color.FromArgb(221, 221, 221);
                    //Cell.Font.Bold = true;
                    //Cell.ForeColor = System.Drawing.Color.FromArgb(127, 127, 127);

                    row.Cells.Add(Cell);

                }

                

                GridViewCompetencias.HeaderRow.Parent.Controls.AddAt(0, row);


                // creating the second row in header
                cellSortedList = new SortedList();

                cellSortedList.Add("1", "<div align=center >Fortalezas </div>,1,1");                
                cellSortedList.Add("2", "<div align=center >Area de desarrollo</div>,1,1");                 

                enumCells = cellSortedList.GetEnumerator();

                row = new GridViewRow(-1, -1, DataControlRowType.Header, DataControlRowState.Normal);
                while (enumCells.MoveNext())
                {
                    string[] cont = enumCells.Value.ToString().Split(Convert.ToChar(","));

                    TableCell Cell;
                    Cell = new TableCell();

                    Cell.RowSpan = Convert.ToInt16(cont[2].ToString());
                    Cell.ColumnSpan = Convert.ToInt16(cont[1].ToString());

                    Cell.Controls.Add(new LiteralControl(cont[0].ToString()));
                    //Cell.HorizontalAlign = HorizontalAlign.Center; Cell.ForeColor = System.Drawing.Color.Black;
                    //Cell.BackColor = System.Drawing.Color.FromArgb(221, 221, 221);
                    //Cell.Font.Bold = true;
                    //Cell.ForeColor = System.Drawing.Color.FromArgb(127, 127, 127);
                    row.Cells.Add(Cell);

                }
                
                GridViewCompetencias.HeaderRow.Parent.Controls.AddAt(1, row);


            }

        protected void RepeaterLogros_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                Label lblTitulo = e.Item.FindControl("lblTitulo") as Label;
                CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                CustomValidatorTextBoxComentario.ErrorMessage = "No completó el campo: " + lblTitulo.Text + ", solapa A- Logros";
            }
        }

        protected void RepeaterComportamientos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                CustomValidatorTextBoxComentario.ErrorMessage = "No completó el campo de la solapa B- Comportamientos";
            }
        }

        protected void RepeaterOportunidadesDesarrollo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                //CustomValidator CustomValidatorTextBoxComentario = e.Item.FindControl("CustomValidatorTextBoxComentario") as CustomValidator;
                //TextBox TextBoxComentario = e.Item.FindControl("TextBoxComentario") as TextBox;
                //CustomValidatorTextBoxComentario.Attributes.Add("TextBoxComentario", "#" + TextBoxComentario.ClientID);
                //CustomValidatorTextBoxComentario.ErrorMessage = "No completo el campo de la solapa D- Oportunidades de Desarrollo";
            }
        }

        private void DisableRepeater(Repeater RptItems, bool habilita)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {
                TextBox TextBoxFundamentacion = item.FindControl("TextBoxComentario") as TextBox;
                TextBoxFundamentacion.ReadOnly = !habilita;

            }
        }

        private string GetComentario(Repeater RptItems)
        {
            string Fundamentacion = string.Empty;  
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {               
                TextBox TextBoxFundamentacion = item.FindControl("TextBoxComentario") as TextBox;
                Fundamentacion = Fundamentacion+ "<br/><br/>"+TextBoxFundamentacion.Text;               

            }
            return Fundamentacion;
        }

        protected void GridViewCompetencias_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    GridView HeaderGrid = (GridView)sender;
            //    GridViewRow HeaderGridRow =  new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            //    TableCell HeaderCell = new TableCell();
            //    HeaderCell.Text = "FCE(*)";
            //    HeaderCell.ColumnSpan = 1;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "Raiting";
            //    HeaderCell.ColumnSpan = 2;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    HeaderCell = new TableCell();
            //    HeaderCell.Text = "Comentarios";
            //    HeaderCell.ColumnSpan = 1;
            //    HeaderGridRow.Cells.Add(HeaderCell);

            //    GridViewCompetencias.Controls[0].Controls.AddAt (0, HeaderGridRow);

            //}

        }

        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                    fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresion PMPMY", legajo.ToString());
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionMY({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);

            }
        }


        protected void ButtonDoc_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                    fle.Log(this.ObjectUsuario.legajo.ToString(), "Exportacion PMPMY Word", legajo.ToString());
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("wordEvaluacionMY({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);

            }
            
        }

        protected void DropDownEvaluadores_DataBound(object sender, EventArgs e)
        {
            
           int? LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);
            
            if (DropDownEvaluadores.Items.FindByValue(LegajoEvaluador.ToString()) != null)
            {

                DropDownEvaluadores.SelectedValue = LegajoEvaluador.ToString();
                lblEvaluadorAsignado.Text = "Su evaluador por defecto es: " + DropDownEvaluadores.SelectedItem.Text;
            }
            else
                lblEvaluadorAsignado.Text = "Su evaluador por defecto ya no es válido, deberá seleccionar un evaluador habilitado";
        }

        protected void DropDownEvaluadores_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        protected void GuardarNuevoEvaluador_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardarNuevoEvaluador.Visible = false;
                GuardarNuevoEvaluadorNuevo.Visible = true;
                //cuando cambia actualizamos el usuario, esta accion sea visa en la pestaña una vez realizada
                //lblEvaluadorElegido.Text = DropDownEvaluadores.SelectedItem.Text;

                //aqui guardar evaluador
                FC.ActualizarEvaluador(DropDownEvaluadores.SelectedItem.Value, HiddenFieldlegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, 1);

                (this.Master as IMasterPMP).setHiddenEvaluador(DropDownEvaluadores.SelectedItem.Value);

                (this.Master as IMasterPMP).bindFVC();
                //               

                WUCGuardadoPrevio.EnabledTimer = true;

            }
        }


        protected void ButtonEnviarEvaluadorPrimero_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FormulariosDS.HistorialDataTable DTH = new FormulariosDS.HistorialDataTable();
                DTH = FC.GetHistorial(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value));

                if (int.Parse(HiddenFieldPasoIDInicio.Value) == -1 || DTH.Count == 1)
                    PanelPopEvaluador_ModalPopupExtender.Show();
                else
                {
                    guardarTodoYenviar();
                }
            }

            
        }

        protected void GuardarNuevoEvaluadorNuevo_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                guardarTodoYenviar();
                
            }

            
        }

        protected void guardarTodoYenviar()
        {
            FC.ActualizarEvaluador(DropDownEvaluadores.SelectedItem.Value, HiddenFieldlegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, 1);

            (this.Master as IMasterPMP).setHiddenEvaluador(DropDownEvaluadores.SelectedItem.Value);

            (this.Master as IMasterPMP).bindFVC();


            PanelPopEvaluador_ModalPopupExtender.Hide();



            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            //string logros;
            //string comportamiento;
            //string oportunidadesmejora;
            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }
            FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

            UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
            NotificacionesController n = new NotificacionesController();
            int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

            ///El evaluador esta deshabilitado?
            ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable Evaluador = new DSUsuarios.UsuarioDataTable();

            int Estado = 0;

            Evaluador = ControllerUsuarios1.GetUsuariosByLegajo(legajoevaluador);


            foreach (DSUsuarios.UsuarioRow EvaluadorRow in Evaluador.Rows)
            {
                Estado = EvaluadorRow.Estado;
            }

            if (Estado != 2)
            {
                WUCGuardadoPrevio.EnabledTimer = false;
                ObjectDataGetEvaluators.DataBind();
                DropDownEvaluadores.Items.Clear();
                DropDownEvaluadores.Items.Add(new ListItem("-Seleccione-", ""));
                DropDownEvaluadores.DataBind();
                PanelPopEvaluador_ModalPopupExtender.Show();
                return;
            }

            
            try
            {
                
                WUCGuardadoPrevio.EnabledTimer = false;

                GuardarItemsRepeater(RepeaterLogros);
                GuardarItemsRepeater(RepeaterComportamientos);
                GuardarItemsRepeater(RepeaterOportunidadesDesarrollo);
                GuardarItems(GridViewCompetencias);
                FC.GrabarEvaluador();
                FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);
                FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);


             
                string mailEvaluador = UE.GetMail(legajoevaluador);
                string envio;
                string urlPMPMY;
                urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=1&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];

                if (!string.IsNullOrEmpty(mailEvaluador))
                    envio = n.envioEvaluadorMYV2(mailEvaluador, UE.GetUsuarioNombre(legajo), GetComentario(RepeaterLogros), GetComentario(RepeaterComportamientos), GetComentario(RepeaterOportunidadesDesarrollo), urlPMPMY);

                
                // if (envio == "true") 
                omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                EjecutarScript(string.Format("flagCierre = PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                //  omb.ShowMessage(envio);
                //EjecutarScript(string.Format("Alert('Test');PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));

            }
            catch (Exception exc)
            {

                omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                // omb.ShowMessage(exc.Message);
            }        
        }

       }

    

}
