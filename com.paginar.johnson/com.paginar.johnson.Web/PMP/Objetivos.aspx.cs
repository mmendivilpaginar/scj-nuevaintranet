﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.PMP
{
    public partial class Objetivos : PageBase
    {

        private ControllerEvalDesemp _CED;
        public ControllerEvalDesemp CED
        {
            get
            {
                _CED = (ControllerEvalDesemp)this.Session["CED"];
                if (_CED == null)
                {
                    _CED = new ControllerEvalDesemp();
                    this.Session["CED"] = _CED;
                }
                return _CED;
            }
            set
            {
                this.Session["CED"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                HtmlGenericControl BodyPmp = this.FindControl("Body") as HtmlGenericControl;
                BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location=window.opener.location.toString().split('?')[0] + '?Pestania=Objetivos';");
                //window.opener.closed)window.opener.location.reload();");
                //window.opener.myPostBack();");

                if(!string.IsNullOrEmpty( Request.QueryString["indice"]))
                {
                  HiddenFieldIndice.Value = Request.QueryString["indice"];
                  List<Objetivo> Objetivos = CED.GetObjetivoByIndice(int.Parse(Request.QueryString["indice"]));
                  TextBoxMedida.Text = Objetivos[0].Medidas;
                  TextBoxResultado.Text = Objetivos[0].Resultados;
                  TextBoxObjetivo.Text = Objetivos[0].Objetivos;
                  ButtonAgregar.Text = "Actualizar";
                  LabelTitulo.Text = "Actualizar Objetivo";
                }
            }

        }

        protected void ButtonAgregar_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            //si e snullo es uno nuevo
            if (string.IsNullOrEmpty(HiddenFieldIndice.Value))
            {
                CED.AddObjetivo(TextBoxObjetivo.Text, TextBoxMedida.Text, TextBoxResultado.Text);
                CED.SaveEvalDesemp(this.ObjectUsuario.usuarioid);
                EjecutarScript("window.close()");
            }
            else
            {
               CED.UpdateItem(int.Parse(HiddenFieldIndice.Value), TextBoxObjetivo.Text, TextBoxMedida.Text, TextBoxResultado.Text);            
               CED.SaveEvalDesemp(this.ObjectUsuario.usuarioid);
               EjecutarScript("window.close()");
            
            }
        }

        protected void ButtonCancelar_Click(object sender, EventArgs e)
        {

        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }
    }
}