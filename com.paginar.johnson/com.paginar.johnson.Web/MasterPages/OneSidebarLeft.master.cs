﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web
{
    public partial class OneSidebarLeft : MasterBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
                ((HtmlGenericControl)Master.FindControl("MasterBody")).Attributes.Add("class", "sidebar-left");
        }
    }
}