﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web
{
    public partial class TwoSidebars : MasterBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Usuario u = (Usuario)Session["Usuario"];
            if (u.clusteridactual.ToString() == "1")
            {
                ucBannerComedor.Visible = true;
                BannerTiendaOnline.Visible = true;
            }
            else
            {
                BannerTiendaOnline.Visible = false;
                ucBannerComedor.Visible = false;
            }

            if (u.clusteridactual == 1)
                ucViviScj1.Visible = true;
            else
                ucViviScj1.Visible = false;

            if (u.clusteridactual == 1)
                ucProgReconocimientos1.Visible = true;
            else
                ucProgReconocimientos1.Visible = false;

            if (u.clusteridactual == 1 || u.clusteridactual == 2 || u.clusteridactual == 3 || u.clusteridactual == 4)
            {
                //ucTarjetasNavidenias1.Visible = true;
                BannerEstrategia2016.Visible = true;
            }
            else
            {
                //ucTarjetasNavidenias1.Visible = false;
                BannerEstrategia2016.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                ((HtmlGenericControl)Master.FindControl("MasterBody")).Attributes.Add("class", "two-sidebars");
            }
            ucDiarios1.clusterDefault = this.ObjectUsuario.clusteridactual;
        }
    }
}