﻿using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.UI.WebControls;
using System;
using com.paginar.johnson.BL;
using com.paginar.johnson.Web.UserControl_Home;
using System.Web.UI;
using com.paginar.johnson.membership;


namespace com.paginar.johnson.Web
{
    public partial class Site : MasterBase
    {

        ControllerContenido c = new ControllerContenido();
        ControllerEstadisticas E = new ControllerEstadisticas();

        protected void Page_Load(object sender, EventArgs e)
        {
            User Usuario = null;
            Usuarios usercontroller = new Usuarios();
            ControllerUsuarios uc = new ControllerUsuarios();
            int LegajoLog = 0;
            Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
            LegajoLog = Usuario.GetLegajo();
            HiddenFieldTipoEvaluado.Value = uc.DameTipoEvaluado(LegajoLog, null);


            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0"); // Proxies.

            if (Session["Usuario"] == null) {
                Response.Redirect("/Logout.aspx");
            }
            
            if (!Page.IsPostBack)
            {
                setUsuario();
                setHoraFecha();
                setCombo();
                HyperLinkCambiarContrasena.Visible = this.EditaClave;
                InsertarEstadistica();
            }


        }

        protected void InsertarEstadistica()
        {
            string[] urlsTit= processURL(Request.Url.PathAndQuery.ToString().ToLower());
            E.Repositorio.AdapterDSEstadisticas.Estadisticas.SetNewRegisterEstadistic(this.ObjectUsuario.usuarioid,urlsTit[0].ToString(),urlsTit[1].ToString(),"Pagina");        
          // E.Repositorio.AdapterDSEstadisticas.Estadisticas.SetNewRegisterEstadistic(this.ObjectUsuario.usuarioid, Request.Url.PathAndQuery.ToString().ToLower(),null);        
        }

        protected string[] processURL(string URL)
        {
            string[] StringReturn = new string[2];

            if (URL.IndexOf("clasificados") > 0)
            {
                StringReturn[1] = "Clasificados";
                StringReturn[0] = "/Clasificados/Clasificados.aspx";
            }
            else if (URL.IndexOf("default") > 0 && URL.IndexOf("voluntariado") < 0)
            {
                StringReturn[1] = "Home";
                StringReturn[0] = URL;
            }
            //
            else if (URL.IndexOf("sustentabilidad") > 0)
            {
                StringReturn[1] = "Sustentabilidad";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("campaniaseguridad") > 0)
            {
                StringReturn[1] = "10 en seguridad";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("cambiapass") > 0)
            {
                StringReturn[1] = "Cambio de Contraseña ";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("miperfil") > 0)
            {
                StringReturn[1] = "Mi Perfil ";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("elearning") > 0)
            {
                StringReturn[1] = "E-learning ";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("estrategias2016") > 0)
            {
                StringReturn[1] = "Estrategia 2016";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("visor4_default.aspx") > 0)
            {
                StringReturn[1] = "Impuesto a las ganancias";
                StringReturn[0] = "/recursos_humanos/visor4_default.aspx";
            }
            else if (URL.IndexOf("recursos_humanos/reportebuxys.asp") > 0)
            {
                StringReturn[1] = "Reporte de Viáticos";
                StringReturn[0] = URL;
            }
            else if (URL.IndexOf("form_evaldesemp") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Evaluacion de desempeño";
                StringReturn[0] = "/servicios/form_evaldesemp.aspx";            
                
            }else if(URL.IndexOf("tramites") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_solicitudtrabajarferiado") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Solicitud Trabajar Feriado";
                StringReturn[0] = "/servicios/Tramites.aspx";            
            }
            else if (URL.IndexOf("form_anticiporemuneraciones") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Anticipo Remuneraciones";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_prestamo") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Prestamo";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_diajohnson") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Día Johnson";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_notificacionvacaciones") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Notificación de Vacaciones";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_notificacionmaternidad") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites -Notificación por Licencia por Maternidad";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("politicasdeempresa") > 0)
            {
                StringReturn[1] = "Nuestra Compañía - Políticas de Empresa";
                StringReturn[0] = "/nuestracompania/politicasdeempresa.aspx";
            }
            else if (URL.IndexOf("enestocreemos") > 0)
            {
                StringReturn[1] = "Nuestra Compañía - En Esto Creemos";
                StringReturn[0] = "/nuestracompania/enestocreemos.aspx";
            }
            else if (URL.IndexOf("comunidad") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Relaciones con la Comunidad";
                StringReturn[0] = "/recursos_humanos/comunidad.aspx";
            }
            else if (URL.IndexOf("cursos") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Capacitación, Desarrollo y Selección";
                StringReturn[0] = "/recursos_humanos/cursos.aspx";
            }
            else if (URL.IndexOf("ingresosegresos") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Ingresos / Egresos";
                StringReturn[0] = "/novedades/ingresosegresos.aspx";
            }
            else if (URL.IndexOf("promocionescambios") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Promociones / Cambios";
                StringReturn[0] = "/novedades/promocionescambios.aspx";
            }
            else if (URL.IndexOf("reportebuxys") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Reporte de Viáticos";
                StringReturn[0] = "/ASPPages.aspx?url=recursos_humanos/reportebuxys.asp";
            }
            else if (URL.IndexOf("dashboardpayroll") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Pay Roll";
                StringReturn[0] = "/servicios/DashBoardPayroll.aspx";
            }
            else if (URL.IndexOf("diascompensatoriosadministrativos.aspx") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Días Compensatorios";
                StringReturn[0] = "/Compensatorios/DiasCompensatoriosAdministrativos.aspx";
            }
            else if (URL.IndexOf("agendatel") > 0)
            {
                StringReturn[1] = "Servicios - Agenda Telefónica";
                StringReturn[0] = "/servicios/agendatel.aspx";
            }
            else if (URL.IndexOf("calendariofiscal") > 0)
            {
                StringReturn[1] = "Servicios - Calendario Fiscal";
                StringReturn[0] = "/servicios/calendariofiscal.aspx";
            }
            else if (URL.IndexOf("Clasificados") > 0)
            {
                StringReturn[1] = "Servicios - Clasificados";
                StringReturn[0] = "/Clasificados/Clasificados.aspx";
            }
            else if (URL.IndexOf("comedor") > 0)
            {
                StringReturn[1] = "Servicios - Comedor";
                StringReturn[0] = "/eventos/comedor.aspx";
            }
            else if (URL.IndexOf("cumpleanios") > 0)
            {
                StringReturn[1] = "Servicios - Cumpleaños";
                StringReturn[0] = "/servicios/cumpleanios.aspx";
            }
            else if (URL.IndexOf("eap") > 0)
            {
                StringReturn[1] = "Servicios - EAP";
                StringReturn[0] = "/novedades/eap.aspx";
            }
            else if (URL.IndexOf("feriados") > 0)
            {
                StringReturn[1] = "Servicios - Feriados";
                StringReturn[0] = "/servicios/feriados.aspx";
            }
            else if (URL.IndexOf("proveeduria") > 0)
            {
                StringReturn[1] = "Servicios - Proveeduría";
                StringReturn[0] = "/novedades/proveeduria.aspx";
            }
            else if (URL.IndexOf("salas") > 0)
            {
                StringReturn[1] = "Servicios - Reservas de Salas";
                StringReturn[0] = "/ASPPages.aspx?url=salas/buscar.asp";
            }
            else if (URL.IndexOf("salud") > 0)
            {
                StringReturn[1] = "Servicios - Salud";
                StringReturn[0] = "/servicios/salud.aspx";
            }
            else if (URL.IndexOf("transporte") > 0)
            {
                StringReturn[1] = "Servicios - Transporte";
                StringReturn[0] = "/servicios/transporte.aspx";
            }
            else if (URL.IndexOf("horarios") > 0)
            {
                StringReturn[1] = "Servicios - Horarios de Servicios";
                StringReturn[0] = "/servicios/horarios.aspx";
            }
            else if (URL.IndexOf("noticias/buscador") > 0)
            {
                StringReturn[1] = "Noticas";
                StringReturn[0] = "/noticias/buscador.aspx";
            }
            else if (URL.IndexOf("noticias/noticia") > 0)
            {
                StringReturn[1] = "Noticas - Articulo";
                StringReturn[0] = URL;
            }
            //agregamos los tramites que faltan
            else if (URL.IndexOf("form_SolicitudCompensatorioAdministrativo") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Solicitud Franco Compensatorio Administrativo";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_Transferencia") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Transferencia de Personal";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_CambioDomicilio") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Cambio de Domicilio";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_Incorporacion") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Incorporación de Personal Temporario";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_NotificacionCasamiento") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Notificación por Licencia de Casamiento";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_NotificacionPaternidad") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Notificación por Licencia por Paternidad";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_NotificacionNacimiento") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Notificación por Nacimiento de un Hijo";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_OtrasLicencias") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Otras Licencias ( Examen, Mudanza, Fliar Enfermo )";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("form_solicitud_inscripcion") > 0)
            {
                StringReturn[1] = "Recursos Humanos - Trámites - Solicitud de Cursos de Idiomas";
                StringReturn[0] = "/servicios/Tramites.aspx";
            }
            else if (URL.IndexOf("voluntariado") > 0)
            {
                StringReturn[1] = "Voluntariado";
                StringReturn[0] = "/voluntariado/default.aspx";            
            }
            else if (URL.IndexOf("Tango.aspx") > 0 || URL.IndexOf("tango.aspx") > 0)
            {
                StringReturn[1] = "Embalados";
                StringReturn[0] = "/nuestracompania/Tango.aspx";            
            
            }
            else if (URL.IndexOf("Argo.aspx") > 0 || URL.IndexOf("argo.aspx") > 0)
            {
                StringReturn[1] = "Argo";
                StringReturn[0] = "/nuestracompania/Argo.aspx";

            }
            else //en caso de que falte alguna pagina, toma por defecto la home. Esto como se explico es una funcion Estatica cuando se agreguen nuevas secciones hay que agregarlas aquí
            {
                StringReturn[1] = "Home";
                StringReturn[0] = "/default.aspx";
            }


            return StringReturn;
        }

        private void setUsuario()
        {
            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();
        }


        private void setHoraFecha()
        {
            try
            {

                wsTime.TimeService ws = new wsTime.TimeService();
                DateTime thora = DateTime.Parse(ws.getTimeZoneTime("GMT"));

                ControllerCluster cc = new ControllerCluster();
                Usuario u = (Usuario)Session["Usuario"];
                double gmt = cc.GetClusterGMT(u.clusteridactual);
                thora = thora.AddHours(gmt);
                lblHora.Text = String.Format("{0: HH.mm}", thora);

                lblFecha.Text = string.Format("{0:dddd  dd  'de'  MMMM   yyyy}", thora, "(es-LA)");

                EjecutarScript("  $(document).ready(function() {  showtime(" + thora.Hour + ", " + thora.Minute + " ," + thora.Second + ");       });");
            }
            catch (Exception e)
            {

            }

        }
        protected void imgDesplegar_Click(object sender, ImageClickEventArgs e)
        {
            slickbox.Visible = !slickbox.Visible;
            if (slickbox.Visible) SetHora();
            //else EjecutarScript("StopClock();");
        }

        protected void imgHorarioCerrar_Click(object sender, ImageClickEventArgs e)
        {
            slickbox.Visible = false;
        }

        protected void SetHora()
        {

            wsTime.TimeService ws = new wsTime.TimeService();
            DateTime wshora = DateTime.Parse(ws.getTimeZoneTime("GMT"));

            foreach (DataListItem item in dtHorariosPaises.Items)
            {
                Label lblgmt = item.FindControl("lblGMT") as Label;
                Label lblhora = item.FindControl("lblhora") as Label;
                DateTime hora = new DateTime();
                if (lblgmt.Text.IndexOf(',') > 0) //fracciones de hora.
                {
                    hora = wshora.AddHours(int.Parse(lblgmt.Text.Split(',')[0]));
                    hora = hora.AddMinutes((double.Parse(lblgmt.Text.Split(',')[1]) / 10) * -60);
                }
                else
                {
                    hora = wshora.AddHours(int.Parse(lblgmt.Text));
                }
                lblhora.Text = String.Format("{0: HH.mm}", hora);
            }

            ControllerCluster cc = new ControllerCluster();
            Usuario u = (Usuario)Session["Usuario"];
            double gmt = cc.GetClusterGMT(u.clusteridactual);
            DateTime horaactual = wshora.AddHours(gmt);

            //  EjecutarScript("Clock(" + horaactual.Hour + "," + horaactual.Minute + "," + horaactual.Second + ")");
        }



        private void setCombo()
        {
            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();
            PaisesDropDownList.SelectedValue = this.ObjectUsuario.clusteridactual.ToString();
        }
        protected void PaisesDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int clusterid;
            clusterid = int.Parse(PaisesDropDownList.SelectedValue);
            Usuario u = (Usuario)Session["Usuario"];
            u.clusteridactual = clusterid;
            Session["Usuario"] = u;

            nav.DataBind();

            VisibilityControls(this);
            Response.Redirect("~/Default.aspx");
        }
        private void VisibilityControls(Control c)
        {

            if (c is CustomControlUser)
                (c as CustomControlUser).Check();

            foreach (Control child in c.Controls)
                VisibilityControls(child);
        }

        protected void nav_MenuItemDataBound(object sender, MenuEventArgs e)
        {



            DAL.DsContenido.Content_ItemsDataTable dt = c.Content_ItemsGetMenu(int.Parse(e.Item.DataPath), this.ObjectUsuario.clusteridactual);

            if (dt.Rows.Count == 0)
            {
                MenuItem itemborrar = nav.FindItem(e.Item.ValuePath);
                if (itemborrar != null)
                {
                    if (itemborrar.Parent != null)
                    {
                        MenuItem parent = itemborrar.Parent;
                        if (parent != null)
                        {
                            parent.ChildItems.Remove(itemborrar);
                        }
                        else
                        {
                            nav.Items.Remove(itemborrar);
                        }
                    }
                    else nav.Items.Remove(itemborrar);
                }

            }
            else
            {
                DAL.DsContenido.Content_ItemsRow dr = (DAL.DsContenido.Content_ItemsRow)dt.Rows[0];

                if (!dr.IstooltipNull())
                    if (dr.tooltip.ToString().Trim() != "")
                        e.Item.ToolTip = dr.tooltip.ToString();

                if (!dr.IsnavigateurlNull())
                {
                    if (dr.navigateurl.ToString() != "")
                        e.Item.NavigateUrl = dr.navigateurl.ToString();
                }
                else
                    e.Item.NavigateUrl = String.Empty;

            }


        }



        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;
        }

        protected void HyperLinkSalir_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Logout.aspx");
        }







    }
}
