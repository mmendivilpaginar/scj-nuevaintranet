﻿<h3>Manual del Empleado</h3>

<h4>INTRODUCCIÓN</h4>
<p>
El presente manual tiene el propósito de ser un texto de consulta para todo el personal brindando información sobre normas, procedimientos, beneficios y políticas de nuestra compañía.</p>
<p>
El contenido de este manual se encuentra en vigencia para todo el personal de S.C. Johnson And Son Chile Limitada, en adelante también "S.C. Johnson o Johnson ".</p>

<h4>ACUERDOS GENERALES</h4>
<p>
Antes de entrar en detalles sobre los diferentes temas laborales y beneficios que en este <b>"NUESTRO MANUAL"</b> pretendemos exponer periódicamente, es necesario definir o aclarar algunos criterios básicos, que nos permitirán entendernos mejor. Hemos considerado oportuno iniciar estar publicación refiriéndonos a los beneficios que nos entrega nuestra Compañía. Al respecto, es importante que tengamos claro lo siguiente :</p>
<p>
S.C. Johnson And Son Chile Limitada, otorga a su personal diversos beneficios adicionales a los establecidos legalmente, los que mencionaremos más adelante.</p>
<p>
<b>¿Cuáles son los requisitos para recibir los beneficios S.C. Johnson?</b></p>

<ul>
<li>Ser empleado con contrato indefinido. </li>
<li>Ser carga familiar reconocida y autorizada por la Caja de Compensación o por el organismo previsional correspondiente. </li>
<li>También pueden acceder a los beneficios de salud, los cónyuges que no son carga familiar del trabajador Johnson y que NO reciben este tipo de beneficios de parte de su empleador, situación que en cada caso deberá ser certificada. </li>
</ul>
<p><b>
¿Cuándo puede solicitar los beneficios de salud Johnson?</b></p>
<p>
Estos beneficios, que son internos de la empresa, los puede solicitar después de haber cobrado el reembolso a la Isapre correspondiente, seguro de salud, seguro complementario de salud o Plan de Acceso Universal con Garantías Explicitas (AUGE). La ayuda Johnson se aplicará luego de que haya obtenido todo beneficio externo a ella.</p>
<p>
¿Cómo debe solicitar los beneficios Johnson?</p>

Cada vez que necesite solicitar este beneficio, debe completar la "solicitud de beneficio" correspondiente, indicando claramente los datos necesarios y adjuntando la documentación de respaldo que se requiere en cada caso.

¿Existen plazos para solicitar los beneficios?

Si, solo podrá solicitar los beneficios hasta 30 días después de haber efectuado el gasto correspondiente.

Los beneficios por gastos médicos (reembolsos) serán pagados dentro de los 45 días siguientes, a partir de la fecha de recepción de los documentos que acrediten la efectividad de este tipo de gastos.

A continuación, entregamos el detalle de los beneficios que S.C. Johnson otorga con los requisitos necesarios para solicitarlos y el procedimiento que debe seguir para optar a ellos.

