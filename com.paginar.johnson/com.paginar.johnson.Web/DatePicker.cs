﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace com.paginar.johnson.Web
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DatePicker runat=server></{0}:DatePicker>")]
    public class DatePicker : CompositeControl
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]


        private TextBox _TxtDate = new TextBox();
        private Image _ImgDate = new Image();
        private RequiredFieldValidator _rfv = new RequiredFieldValidator();
        private LiteralControl lc = new LiteralControl();
        private bool _isRequired;
        private RangeValidator _rv = new RangeValidator();
        private CalendarExtender _cext = new CalendarExtender();
        private FilteredTextBoxExtender _fte = new FilteredTextBoxExtender();
        private int _width = 75;
        private string _dateFormat = "dd/MM/yyyy";
        private string _errorRequired = "Ingrese Fecha";
        private string _errorRange = "Fecha Inválida";
        private string _since = "01/01/1754";
        private string _until = "31/12/9999";
        private string _validationGroup;
        private string _ImageUrl;
        private bool _AutoPostBack;
        private string _Value;
        private string _ImageCssClass;
        private string _TextBoxCssClass;
        private bool _IsVisible = true;

        /// <summary>
        /// URL de la imágen para el calendario
        /// </summary>
        [Bindable(true), Browsable(true), Category("Appearance"), DefaultValue(""), Description("URL de la imágen para el calendario")]
        public string ImageUrl
        {
            get
            {
                return _ImageUrl;
            }
            set
            {
                this._ImageUrl = value;
            }
        }

        /// <summary>
        /// Ancho del TextBox
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Description("Ancho del TextBox")]
        public int TextWidth
        {
            get
            {
                return _width;
            }
            set
            {
                this._width = value;
            }
        }

        /// <summary>
        /// Grupo de validación al que pertenece
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Description("Grupo de validación al que pertenece")]
        public string ValidationGroup
        {
            get
            {
                EnsureChildControls();
                return _validationGroup;
            }
            set
            {
                EnsureChildControls();
                this._validationGroup = value;
            }
        }

        /// <summary>
        /// Mensaje de error para campos requeridos
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Description("Mensaje de error para campos requeridos")]
        public string ErrorRequired
        {
            get
            {
                return _errorRequired;
            }
            set
            {
                this._errorRequired = value;
            }
        }

        /// <summary>
        /// Mensaje de error para fechas fuera de rango o inválidas
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Description("Mensaje de error para fechas fuera de rango o inválidas")]
        public string ErrorRange
        {
            get
            {
                return _errorRange;
            }
            set
            {
                this._errorRange = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true)]
        private string _Text
        {
            get
            {
                EnsureChildControls();
                String s = (String)ViewState["Text"];
                return ((s == null) ? string.Empty : s);
            }

            set
            {
                EnsureChildControls();
                ViewState["Text"] = value;
            }
        }

        /// <summary>
        /// Valor
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("La fecha del TextBox")]
        public string Text
        {
            get
            {
                EnsureChildControls();
                return _Value = _TxtDate.Text;
            }

            set
            {
                EnsureChildControls();
                if (value.Length > 10)
                {
                    string[] splitted = value.Split('/');
                    string finalDate = splitted[0] + "/" + splitted[1] + "/" + splitted[2].Substring(0, 4);
                    value = finalDate;
                }
                _Value = _TxtDate.Text = value;
            }
        }

        /// <summary>
        /// Si debe estar habilitado el postback para este control.
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("Determina si el control debe o no hacer un roundtrip")]
        public bool AutoPostBack
        {
            get
            {
                return _AutoPostBack;
            }

            set
            {
                _AutoPostBack = value;
            }
        }

        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("Determina si el control debe ser visible")]
        public override bool Visible
        {
            get
            {
                return _IsVisible;
            }

            set
            {
                _IsVisible = value;
            }
        }

        /// <summary>
        /// Si el campo es obligatorio.
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("Determina si el campo es requerido o no")]
        public bool IsRequired
        {
            get
            {
                return _isRequired;
            }

            set
            {
                _isRequired = value;
            }
        }

        /// <summary>
        /// Css para la imagen.
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("CSS para el botón de imágen")]
        public string ImageCssClass
        {
            get
            {
                return _ImageCssClass;
            }

            set
            {
                _ImageCssClass = value;
            }
        }

        /// <summary>
        /// Css para el textbox
        /// </summary>
        [Bindable(true), Category("Appearance"), DefaultValue(""), Localizable(true), Description("CSS para el textbox")]
        public string TextBoxCssClass
        {
            get
            {
                return _TextBoxCssClass;
            }

            set
            {
                _TextBoxCssClass = value;
            }
        }

        [Bindable(true), Category("Custom"), DefaultValue(""), Localizable(true), Description("Comando para el control")]
        public string CommandName
        {
            get
            {
                string s = ViewState["CommandName"] as string;
                return s == null ? String.Empty : s;
            }
            set
            {
                ViewState["CommandName"] = value;
            }
        }

        [Bindable(true), Category("Custom"), DefaultValue(""), Localizable(true), Description("Argumento para el control")]
        public string CommandArgument
        {
            get
            {
                string s = ViewState["CommandArgument"] as string;
                return s == null ? String.Empty : s;
            }
            set
            {
                ViewState["CommandArgument"] = value;
            }
        }

        /// <summary>
        /// Fecha Mínima.
        /// </summary>
        [Bindable(true), Category("Custom"), DefaultValue(""), Localizable(true), Description("Fecha mínima que se pueda ingresar")]
        public string Since
        {
            get
            {
                EnsureChildControls();
                return _since;
            }
            set
            {
                EnsureChildControls();
                _since = value;
            }
        }

        /// <summary>
        /// Fecha Máxima.
        /// </summary>
        [Bindable(true), Category("Custom"), DefaultValue(""), Localizable(true), Description("Fecha máxima que se pueda ingresar")]
        public string Until
        {
            get
            {
                EnsureChildControls();
                return _until;
            }
            set
            {
                EnsureChildControls();
                _until = value;
            }
        }

        /// <summary>
        /// Formato de fecha. Ej. dd/MM/yyyy
        /// </summary>
        [Bindable(true), Category("Custom"), DefaultValue(""), Localizable(true), Description("Formato de fecha. Ej. dd/MM/yyyy")]
        public string DateFormat
        {
            get
            {
                EnsureChildControls();
                return _dateFormat;
            }
            set
            {
                EnsureChildControls();
                _dateFormat = value;
            }
        }

        protected static readonly object EventCommandObj = new object();

        public event CommandEventHandler Command
        {
            add
            {
                Events.AddHandler(EventCommandObj, value);
            }
            remove
            {
                Events.RemoveHandler(EventCommandObj, value);
            }
        }

        //this will raise the bubble event
        protected virtual void OnCommand(CommandEventArgs commandEventArgs)
        {
            CommandEventHandler eventHandler = (CommandEventHandler)Events[EventCommandObj];
            if (eventHandler != null)
            {
                eventHandler(this, commandEventArgs);
            }
            base.RaiseBubbleEvent(this, commandEventArgs);
        }

        //this will be initialized to  OnTextChanged event on the normal textbox
        private void OnTextChanged(object sender, EventArgs e)
        {
            if (this.AutoPostBack)
            {
                //pass the event arguments to the OnCommand event to bubble up
                CommandEventArgs args = new CommandEventArgs(this.CommandName, this.CommandArgument);
                OnCommand(args);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            //AddStyleSheet();
            //AddJavaScript();
            base.OnInit(e);

            // For TextBox
            // setting name for textbox. using t just to concat with this.ID for unqiueName
            _TxtDate.ID = this.ID + "t";
            // setting postback
            _TxtDate.AutoPostBack = this.AutoPostBack;
            _TxtDate.MaxLength = 10;
            // giving the textbox default value for date
            _TxtDate.Text = this.Text;
            _TxtDate.Width = Unit.Pixel(TextWidth);
            //Initializing the TextChanged with our custom event to raise bubble event
            _TxtDate.TextChanged += new System.EventHandler(this.OnTextChanged);
            //Setting textbox to readonly to make sure user dont play with the textbox
            //_TxtDate.Attributes.Add("readonly", "readonly");
            // adding stylesheet 
            _TxtDate.Attributes.Add("class", this.TextBoxCssClass);



            // For Image
            // setting alternative name for image
            _ImgDate.AlternateText = "imageURL";
            _ImgDate.ImageAlign = ImageAlign.AbsMiddle;
            if (!string.IsNullOrEmpty(_ImageUrl))
                _ImgDate.ImageUrl = _ImageUrl;

            //setting name for image
            _ImgDate.ID = this.ID + "i";
            //setting image class for textbox
            _ImgDate.Attributes.Add("class", this.ImageCssClass);

            _cext.Format = _dateFormat;
            _cext.ID = this.ID + "cext";
            _cext.PopupButtonID = _ImgDate.ID;
            _cext.TargetControlID = _TxtDate.ID;

            _fte.ID = this.ID + "fte";
            _fte.FilterMode = FilterModes.ValidChars;
            _fte.FilterType = FilterTypes.Custom;
            _fte.TargetControlID = _TxtDate.ID;
            _fte.ValidChars = "0123456789/";

            if (_isRequired)
            {
                _rfv.ID = this.ID + "rfv";
                _rfv.ControlToValidate = _TxtDate.ID;
                _rfv.Display = ValidatorDisplay.Dynamic;
                _rfv.ErrorMessage = _errorRequired;
                _rfv.ValidationGroup = _validationGroup;
                _rfv.Text = "*";
            }

            _rv.ID = this.ID + "rv";
            _rv.ControlToValidate = _TxtDate.ID;
            _rv.Display = ValidatorDisplay.Dynamic;
            _rv.ErrorMessage = _errorRange;
            _rv.MaximumValue = _until;
            _rv.MinimumValue = _since;
            _rv.Text = "*";
            _rv.Type = ValidationDataType.Date;
            _rv.ValidationGroup = _validationGroup;
        }

        /// <summary>
        /// adding child controls to composite control
        /// </summary>
        protected override void CreateChildControls()
        {
            this.Controls.Add(_TxtDate);

            lc.Text = "&nbsp;";
            this.Controls.Add(lc);
            this.Controls.Add(_cext);
            this.Controls.Add(_fte);
            this.Controls.Add(_ImgDate);
            if (_isRequired)
                this.Controls.Add(_rfv);
            this.Controls.Add(_rv);
            base.CreateChildControls();
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            // render textbox and image
            if (_IsVisible)
            {
                _TxtDate.RenderControl(writer);
                lc.RenderControl(writer);
                _ImgDate.RenderControl(writer);
                _cext.RenderControl(writer);
                _fte.RenderControl(writer);
                if (_isRequired)
                    _rfv.RenderControl(writer);
                _rv.RenderControl(writer);
                RenderContents(writer);
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.Write(_Text);
        }
    }
}