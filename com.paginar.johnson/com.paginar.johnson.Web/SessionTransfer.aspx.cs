using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace com.paginar.johnson.Web
{
    public partial class SessionTransfer : PageBase
//namespace pcf_solution_cs
//{
//    /// <summary>
//    /// Summary description for SessionTransfer.
//    /// </summary>
//    public class SessionTransfer : 
	{
		private void Page_Load(object sender, System.EventArgs e)
		{
			string guidSave;
			if (Request.QueryString["dir"] == "2asp")
			{
				//Add the session information to the database, and redirect to the
				//  ASP implementation of SessionTransfer.
				guidSave = AddSessionToDatabase();
                string ASP_SITE = ConfigurationManager.AppSettings["ASP_SITE"];
				Response.Redirect(ASP_SITE+"SessionTransferNew.asp?dir=2asp&guid=" + guidSave + 
					"&url=" + Server.UrlEncode( Request.QueryString["url"]));
			}
			else
			{
				//Retreive the session information, and redirect to the URL specified
				//  by the querystring.                
                guidSave = AddSessionToDatabase();
                string ASP_SITE = ConfigurationManager.AppSettings["ASP_SITE"];
                Response.Redirect(ASP_SITE + "SessionTransfer.aspx?dir=2asp&guid=" + guidSave +
                    "&url=" + Server.UrlEncode(Request.QueryString["url"]));
			}
		}


		//This method adds the session information to the database and returns the GUID
		//  used to identify the data.
		private string AddSessionToDatabase()
		{

			//**************************************
			//Enter connection information here
            System.Configuration.ConnectionStringSettings CN = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["com.paginar.johnson.DAL.Properties.Settings.SCJ_CONNECTION"];
            SqlConnection con = new SqlConnection(CN.ConnectionString);
			
			//**************************************

			SqlCommand cmd = new SqlCommand();
            string strSql, guidTemp = GetGuid();

            try
            {
                con.Open();
                cmd.Connection = con;
                int i = 0;
              





                foreach (var property in this.ObjectUsuario.GetType().GetProperties())
                {
                    if (property.CanRead)
                    {
                        object o = property.GetValue(this.ObjectUsuario, null);

                        strSql = "INSERT INTO ASPSessionState (GUID, SessionKey, SessionValue) " +
                            "VALUES ('" + guidTemp + "', '" + property.Name + "', '" +
                            Regex.Replace(o.ToString(), "'", "''") + "')";
                        cmd.CommandText = strSql;
                        cmd.ExecuteNonQuery();
                    }

                }
                //while (i < Session.Contents.Count)
                //{
                //    strSql = "INSERT INTO ASPSessionState (GUID, SessionKey, SessionValue) " + 
                //        "VALUES ('" + guidTemp + "', '" + Session.Contents.Keys[i].ToString() + "', '" + 
                //        Session.Contents[i].ToString() + "')";
                //    cmd.CommandText = strSql;
                //    cmd.ExecuteNonQuery();
                //    i++;
                //}
            }
            catch (Exception)
            {
                //-- Log aqui
            }
            finally
            {
                con.Close();

            }

		


			if(cmd!=null) cmd.Dispose();
			if(con!=null) con.Dispose();

			return guidTemp;
		}


		//This method retrieves the session information identified by the parameter
		//  guidIn from the database.
		private void GetSessionFromDatabase(string guidIn)
		{
			//**************************************
			//Enter connection information here
            System.Configuration.ConnectionStringSettings CN = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["com.paginar.johnson.DAL.Properties.Settings.SCJ_CONNECTION"];
            SqlConnection con = new SqlConnection(CN.ConnectionString);
		
			
			//**************************************
			SqlCommand cmd = new SqlCommand();
			SqlDataReader dr=null;

            try //--Agrego CEGV 28-07-2011
            {
                con.Open();
                cmd.Connection = con;

                string strSql, guidTemp = GetGuid();

                //Get a DataReader that contains all the Session information
                strSql = "SELECT  GUID, SessionKey, SessionValue FROM ASPSessionState WHERE GUID = '" + guidIn + "'";
                cmd.CommandText = strSql;
                dr = cmd.ExecuteReader();

                //Iterate through the results and store them in the session object
                while (dr.Read())
                {
                    Session[dr["SessionKey"].ToString()] = dr["SessionValue"].ToString();
                }
            }
            catch(Exception){

            
            }
            finally
            {
            //Clean up database objects
			    dr.Close();
			    con.Close();
            }

			
            if(cmd!=null) cmd.Dispose();
            if(con!=null) con.Dispose();
		}


		//This method removes all session information from the database identified by the 
		//  the GUID passed in through the parameter guidIn.
		private void ClearSessionFromDatabase(string guidIn)
		{
			//**************************************
			//Enter connection information here

            System.Configuration.ConnectionStringSettings CN = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["com.paginar.johnson.DAL.Properties.Settings.SCJ_CONNECTION"];
            SqlConnection con = new SqlConnection(CN.ConnectionString);
			
			//**************************************
			SqlCommand cmd = new SqlCommand();
			con.Open();
			cmd.Connection = con;
			string strSql;
			
			strSql = "DELETE FROM ASPSessionState WHERE GUID = '" + guidIn + "'";
			cmd.CommandText = strSql;
			cmd.ExecuteNonQuery();

			con.Close();
			cmd.Dispose();
			con.Dispose();
		}


		//This method returns a new GUID as a string.
		private string GetGuid()
		{
			return System.Guid.NewGuid().ToString();
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
