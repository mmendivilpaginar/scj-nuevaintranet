﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO.Compression;
using System.IO;
using System.Web.UI;

namespace com.paginar.johnson.Web.Compress
{

    public class CompressByPreRequest
    {

        public void SetCompresionStream(object objectApp)
        {
            HttpApplication app = objectApp as HttpApplication;
            ValidatePage currentPage = new ValidatePage(app);
            Stream pageStreamOriginal = app.Response.Filter;

           
            if(currentPage.IsPage)
            {
                if (currentPage.IsAjaxPage)
                    return;
                else
                {
                    if (currentPage.IsSupportEncondig)
                    {
                      
                        ResultStream result = this.FactoryStremCompress(currentPage.EncondingPage, pageStreamOriginal);
                        this.Configure(app.Response,result);
                    }
                }
            }

            
        }

        private void Configure(HttpResponse httpResponse, ResultStream result)
        {
            httpResponse.Filter = result.Result;
            httpResponse.AppendHeader("Content-Encoding", result.TypeCompress);
        }


        private ResultStream FactoryStremCompress(string acceptEncoding, Stream prevUncompressedStream)
        {
            ResultStream result = null;
            acceptEncoding = acceptEncoding.ToLower();

            if (acceptEncoding.Contains("deflate") || acceptEncoding == "*")
            {
                result = new ResultStream("deflate", new DeflateStream(prevUncompressedStream,
                             CompressionMode.Compress));
            }
            else
            {
                if (acceptEncoding.Contains("gzip"))
                {
                    result = new ResultStream("gzip", new GZipStream(prevUncompressedStream,
                               CompressionMode.Compress));

                }
            }


            return result;
        }

      
    }

    internal class ValidatePage

    {
        private HttpApplication App;


        public Boolean IsPage
        {
            get
            {
                return this.IsRequest_FromPage(App);
            }
        }


        public Boolean IsAjaxPage
        {
            get
            {
                return this.Page_ContainAJAX(App);
            }
        }

        public Boolean IsSupportEncondig
        {
            get
            {
                return this.Page_SupportEncondig(EncondingPage);
            }
        }

        public string EncondingPage
        {
            get
            {
                return App.Request.Headers["Accept-Encoding"];
            }
        }
        

        public ValidatePage(HttpApplication app)
        {
            this.App = app;
        }



        private bool Page_ContainAJAX(HttpApplication app)
        {
            return app.Request["HTTP_X_MICROSOFTAJAX"] != null;
        }

        private bool IsRequest_FromPage(HttpApplication app)
        {
            return app.Context.CurrentHandler is Page;
        }

        private bool Page_SupportEncondig(string acceptEncoding)
        {
            return !(acceptEncoding == null || acceptEncoding.Length == 0);
        }
    }

    internal class ResultStream
    {
        public string TypeCompress;
        public Stream Result;

        public ResultStream(string _typeCompress, Stream result)
        {
            this.TypeCompress = _typeCompress;
            this.Result = result;
        }
    }
}
