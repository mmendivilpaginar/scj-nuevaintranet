﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="CambiaPass.aspx.cs" Inherits="com.paginar.johnson.Web.CambiaPass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>Cambiar contraseña</h2>    
    <asp:ChangePassword ID="ChangePassword1" runat="server" 
        CancelButtonText="Cancelar" ChangePasswordButtonText="Cambiar" 
        ChangePasswordTitleText="" 
        ConfirmNewPasswordLabelText="Confirmación Contraseña:" 
        ConfirmPasswordCompareErrorMessage="La Confirmación de Contraseña debe ser igual a la Contraseña Nueva." 
        ConfirmPasswordRequiredErrorMessage="La Confirmación de Contraseña es obligatoria." 
        ContinueButtonText="Continuar" NewPasswordLabelText="Contraseña Nueva:" 
        NewPasswordRequiredErrorMessage="El campo Contraseña Nueva es obligatorio." 
        PasswordLabelText="Contraseña Anterior:" 
        PasswordRequiredErrorMessage="El campo Contraseña Anterior es obligatorio." 
        SuccessText="La Contraseña  ha sido modificada." SuccessTitleText="">
    </asp:ChangePassword>

</asp:Content>
