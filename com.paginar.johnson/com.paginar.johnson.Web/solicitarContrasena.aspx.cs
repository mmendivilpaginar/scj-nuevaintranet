﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.IO;
using com.paginar.johnson.DAL;
using com.paginar.johnson.utiles;


namespace com.paginar.johnson.Web
{
    public partial class solicitarContrasena : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

             
            string operacion = string.Empty;
            operacion = Request.QueryString["tipo"];
            // recuperar o solicitar
            if (operacion == "solicitar")
            {
                MVSolicitarContrasena.SetActiveView(VSolicitudOperario);
                
            }
            else if (operacion == "recuperar")
            {
                MVSolicitarContrasena.SetActiveView(VSolicitudClave);
            }


        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            
            tbNombres.Text      = "";
            tbApellidos.Text    = "";
            tbTelefono.Text     = "";
            DDLCluster.SelectedIndex = 0;
            tbNombres.Focus();
            //Message M = new Message(email, string.Format("Comentario sobre su clasificado {0} ({1:dd/MM/yyyy})", ClasiRow.Titulo, ClasiRow.FHAlta), string.Format("Recibi&oacute un comentario sobre su clasificado {0}, publicado el {1:dd/MM/yyyy}.<p>{2}</p> ", ClasiRow.Titulo, ClasiRow.FHAlta, comentario.Truncate(25, "...", false) + "..." + string.Format("<a href='{0}'>Ver mas</a>", Page)));
            //M.Send();

        }

        protected void bntEnviarLeg_Click(object sender, EventArgs e)
        {

            int Legajo;
            bool enviado = false;
            Legajo = int.Parse(tbLegajo.Text);
            ControllerUsuarios UC = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable UDT = new DSUsuarios.UsuarioDataTable();
            UDT = UC.GetUsuariosByLegajo(Legajo);
            
            if (UDT.Count > 0)
            {
                string cuerpo = string.Empty;
                string to = string.Empty;
                cuerpo = "Recientemente solicitaste reestablecer tu contraseña para poder ingresar a la intranet de Johnson<br />";

                foreach (DSUsuarios.UsuarioRow USRrow in UDT.Rows)
                {
                    if (USRrow.Estado == 1)
                    {
                        cuerpo += "Su Usuario se encuentra deshabilitado, póngase en contacto con el responsable del Cluster<br />";
                    }
                    else
                    {
                        cuerpo += String.Format("Tu nombre de usuario es {0} <br />", USRrow.Identif);
                        cuerpo += String.Format("Tu contrase&ntilde;a es {0} <br />", USRrow.Clave);
                    }
                    to = USRrow.Email.ToString();
                }
                cuerpo += "Esta solicitud se realizó " + System.DateTime.Now.ToShortDateString() + " - " + System.DateTime.Now.ToLongTimeString();
                Message M = new Message(to, "Intranet Johnson: Recuperacion de contraseña", cuerpo);
                enviado = M.Send();
                if (enviado)
                {
                    lblEstadoEnvio.Text = "<br /><br /><br />La informaci&oacute;n solicitada fue enviada a su casilla de correos.";
                    
                }
                else
                {
                    lblEstadoEnvio.Text = "<br /><br /><br />Ocurrio un error en el envio del mail, intente nuevamente más tarde.<br />En caso de que no tenga definido un e-mail, pongase en contacto con el encargado/a del Cluster al que pertenece";
                    
                }

                MVSolicitarContrasena.SetActiveView(VEstadoEnvio);
            }
            else
                {
                    lblEstadoEnvio.Text = "<br /><br /><br />No se encontro coincidencia con el número de legajo ingresado.<br /> En caso de que no cuente con un Usuario en la intranet, siga las intrucciones detalladas en la pagina de login para solicitar una cuenta.";
                    MVSolicitarContrasena.SetActiveView(VEstadoEnvio);    
                }

                
            


        }

        protected void BtnEnviar_Click(object sender, EventArgs e)
        {
            string cuerpo = string.Empty;
            bool enviado = false;

            string toA1 = string.Empty;
            string toA2 = string.Empty;
            
            string toC1 = string.Empty;
            string toC2 = string.Empty;

            string toE = string.Empty;
            cuerpo += "";
            int seleccionado = int.Parse(DDLCluster.SelectedValue.ToString());

            cuerpo += "Solicitud Cuenta Intranet Johnson<br /><br />";
            cuerpo += string.Format("Apellidos: {0}<br />", tbApellidos.Text);
            cuerpo += string.Format("Nombres: {0}<br />", tbNombres.Text);
            cuerpo += string.Format("Nº Teléfono: {0}<br />", tbTelefono.Text);
            cuerpo += string.Format("Cluster: {0}<br />", DDLCluster.SelectedItem.ToString());
            //defecto

            switch (seleccionado) 
            { 
                case 1:
                case 3:
                case 4:
                    toA1 = "MArangur@scj.com";
                    Message M1 = new Message(toA1, "Intranet Johnson: Solicitud de Cuenta", cuerpo);
                    enviado = M1.Send();

                    //toA2 = "MArangur@scj.com";
                    //Message M2 = new Message(toA2, "Intranet Johnson: Solicitud de Cuenta", cuerpo);
                    //enviado = M2.Send();

                    break;
                case 2:
                    toC1 = "FMBerri@scj.com";
                    Message M3 = new Message(toC1, "Intranet Johnson: Solicitud de Cuenta", cuerpo);
                    enviado = M3.Send();

                    toC2 = "Sconejer@scj.com";
                    Message M4 = new Message(toC2, "Intranet Johnson: Solicitud de Cuenta", cuerpo);
                    enviado = M4.Send();

                    break;
                default:
                    toE = "MArangur@scj.com";
                    Message M5 = new Message(toE, "Intranet Johnson: Solicitud de Cuenta", cuerpo);
                    enviado = M5.Send();

                    break;
            }



            if (enviado)
            {
                lblEstadoEnvio.Text = "<br /><br /><br />Los datos fueron enviados, el encargado del Cluester se pondra en contacto con Usted.";

            }
            else
            {
                lblEstadoEnvio.Text = "<br /><br /><br />Ocurrio un error en el envio del mail, intente nuevamente más tarde.";

            }

            MVSolicitarContrasena.SetActiveView(VEstadoEnvio);


        }
    }
}