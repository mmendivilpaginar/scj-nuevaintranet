﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucNuestraGente : CustomControlUser
    {
        private bool _displaycluster;
        public bool displaycluster
        {
            get { return _displaycluster; }
            set { _displaycluster = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MasterBase mb = (MasterBase)this.Page.Master;
            hdClusterID.Value = mb.ObjectUsuario.clusteridactual.ToString(); 
        }

        protected void imgprev_Click(object sender, ImageClickEventArgs e)
        {
            frmUsuario.DataBind();
        }

        protected void imgnext_Click(object sender, ImageClickEventArgs e)
        {
            frmUsuario.DataBind();
        }

      

        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }

        protected void updUsuarios_Load(object sender, EventArgs e)
        {
            EjecutarScript("  $(document).ready(function () {   $('a.toolficha').cluetip({ splitTitle: '|' }) });");
        }

    }
}