﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using AjaxControlToolkit;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Configuration;

namespace com.paginar.johnson.Web.UserControl_Home
{

    public partial class ucNoticias : CustomControlUser
    {
        public enum Navigation
        {
            None,
            First,
            Next,
            Previous,
            Last,
            Pager,
            Sorting
        }

        private bool _displaycluster;
        public bool displaycluster
        {
            get { return _displaycluster; }
            set { _displaycluster = value; }
        }


        private bool _displayHeader =true;
        public bool displayHeader
        {
            get { return _displayHeader; }
            set { _displayHeader = value; }
        }

        private int cantFilas;

        public int CantFilas
        {
            get { return (cantFilas == 0) ? 10 : cantFilas; }
            set { cantFilas = value; }
        }


        public int ListCount
        {
            get
            {
                object obj = ViewState["_ListCount"];
                if (obj == null)
                    return 0;
                else
                    return (int)obj;
            }
            set
            {
                this.ViewState["_ListCount"] = value;
            }
        }

        public int Cluster
        {
            get
            {
                object obj = ViewState["_Cluster"];
                if (obj == null)
                    return 0;
                else
                    return (int)obj;
            }
            set
            {
                this.ViewState["_Cluster"] = value;
            }
        }


        public bool IsHome
        {
            get
            {
                object obj = ViewState["_IsHome"];
                if (obj == null)
                    return false;
                else
                return (bool)obj;
            }
            set
            {
                this.ViewState["_IsHome"] = value;
            }
        }

        private DSNoticias.infInfoDataTable _infInfoDt;
        public DSNoticias.infInfoDataTable infInfoDt
        {
            get
            {

                _infInfoDt = (DSNoticias.infInfoDataTable)this.Session["infInfoDt"];
                if (_infInfoDt == null)
                {
                    _infInfoDt = new DSNoticias.infInfoDataTable();
                    this.Session["NC"] = _infInfoDt;
                }
                return _infInfoDt;
            }
            set
            {
                this.Session["infInfoDt"] = value;
                ListCount = infInfoDt.Rows.Count;
            }
        }
        


        private ControllerNoticias cn = new ControllerNoticias();
        private ControllerNoticias cnr = new ControllerNoticias();


        public void CargarDatos()
        {
            FillRepeater(Navigation.First);
        }

        public void CargarDatosHome(int _cluster)
        {
            IsHome = true;
            Cluster = _cluster;
            FillRepeaterHome(Navigation.First);
            
        }

        private void FillRepeater(Navigation navigation)
        {
            //Create the object of PagedDataSource
            PagedDataSource objPds = new PagedDataSource();

            //Assign our data source to PagedDataSource object
            objPds.DataSource = infInfoDt.DefaultView;

            //Set the allow paging to true
            objPds.AllowPaging = true;

            //Set the number of items you want to show
            objPds.PageSize = CantFilas;

            //Based on navigation manage the NowViewing
            switch (navigation)
            {
                case Navigation.Next:       //Increment NowViewing by 1
                    NowViewing++;
                    break;
                case Navigation.Previous:   //Decrement NowViewing by 1
                    NowViewing--;
                    break;
                case Navigation.Last:       //Make NowViewing to last page for PagedDataSource
                    NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Pager:      //Change NowViewing based on pager size and page count
                    if (CantFilas >= objPds.PageCount)
                        NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Sorting:
                    break;
                default:                    //Default NowViewing set to 0
                    NowViewing = 0;
                    break;
            }

            //Set the current page index
            objPds.CurrentPageIndex = NowViewing;

            //Change the text Now viewing text
            lblCurrentPage.Text = lblCurrentPageTop.Text = "Página " + (NowViewing + 1).ToString() + " de " + objPds.PageCount.ToString();

            // Disable Prev, Next, First, Last buttons if necessary
            lbtnPrev.Enabled = lbtnPrevTop.Enabled = !objPds.IsFirstPage;
            lbtnNext.Enabled = lbtnNextTop.Enabled = !objPds.IsLastPage;
            lbtnFirst.Enabled = lbtnFirstTop.Enabled = !objPds.IsFirstPage;
            lbtnLast.Enabled = lbtnLastTop.Enabled = !objPds.IsLastPage;

            //Assign PagedDataSource to repeater
            RNoticias.DataSource = objPds;
            RNoticias.DataBind();

        }


        private void FillRepeaterHome(Navigation navigation)
        {

            ControllerNoticias CN = new ControllerNoticias();
            DSNoticias.GetInfoClusterPagDataTable DT = CN.Repositorio.AdapterDSNoticias.GetInfoClusterPag.GetNoticiasByClusterPag(Cluster, null, NowViewing * 10);

            if (DT.Rows.Count > 1)
            {
                if((int.Parse(DT.Rows[0]["Cantidad"].ToString()) % 10)>0)

                    cantFilas =( int.Parse(DT.Rows[0]["Cantidad"].ToString()) / 10 )+1;
                else
                    cantFilas = int.Parse(DT.Rows[0]["Cantidad"].ToString()) / 10;

                ListCount = int.Parse(DT.Rows[0]["Cantidad"].ToString());
            }

           // NowViewing pagina actual
            switch (navigation)
            {
                case Navigation.Next:       //Increment NowViewing by 1
                    NowViewing++;
                    break;
                case Navigation.Previous:   //Decrement NowViewing by 1
                    NowViewing--;
                    break;
                case Navigation.Last:       //Make NowViewing to last page for PagedDataSource
                    NowViewing = cantFilas-1;
                    break;
                case Navigation.Pager:      //Change NowViewing based on pager size and page count
                    //if (CantFilas >= objPds.PageCount)
                    //    NowViewing = objPds.PageCount - 1;
                    break;
                case Navigation.Sorting:
                    break;
                default:                    //Default NowViewing set to 0
                    NowViewing = 0;
                    break;
            }

            
            
            

            //Change the text Now viewing text
            lblCurrentPage.Text = lblCurrentPageTop.Text = "Página " + (NowViewing + 1).ToString() + " de " + cantFilas;

            // Disable Prev, Next, First, Last buttons if necessary
            lbtnPrev.Enabled = lbtnPrevTop.Enabled = (NowViewing!=0);
            lbtnNext.Enabled = lbtnNextTop.Enabled = (NowViewing != (cantFilas-1));
            lbtnFirst.Enabled = lbtnFirstTop.Enabled = (NowViewing != 0);
            lbtnLast.Enabled = lbtnLastTop.Enabled = (NowViewing != (cantFilas - 1));

            //Assign PagedDataSource to repeater

            
            RNoticias.DataSource = CN.Repositorio.AdapterDSNoticias.GetInfoClusterPag.GetNoticiasByClusterPag(Cluster,null,NowViewing*10);
            RNoticias.DataBind();

        }

        public int NowViewing
        {
            get
            {
                object obj = ViewState["_NowViewing"];
                if (obj == null)
                    return 0;
                else
                    return (int)obj;
            }
            set
            {
                this.ViewState["_NowViewing"] = value;
            }
        }


        protected void RNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
                Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
                HiddenField InfoIDHiddenField = e.Item.FindControl("InfoIDHiddenField") as HiddenField;
                Image ImagenNoticia = e.Item.FindControl("ImagenNoticia") as Image;
                HiddenField RecordatorioHiddenField = e.Item.FindControl("RecordatorioHiddenField") as HiddenField;
                Label RecordatorioLiteral = e.Item.FindControl("RecordatorioLiteral") as Label;

                HtmlGenericControl DivImagen = e.Item.FindControl("DivImagen") as HtmlGenericControl;

                Control RecordatorioItem = e.Item.FindControl("RecordatorioItem") as Control;

                Label FechaLiteral = (Label)e.Item.FindControl("LabelFechaAsoc");

                RecordatorioItem.Visible = Boolean.Parse(RecordatorioHiddenField.Value);

                DateTime FechaPublicacion = DateTime.Parse(FechaLiteral.Text);
                if (Boolean.Parse(RecordatorioHiddenField.Value))
                {
                    DSNoticias.infRecordatoriosDataTable dsinftab = new DSNoticias.infRecordatoriosDataTable();

                    dsinftab = cnr.getRecordatoriosInfByID(int.Parse(InfoIDHiddenField.Value));

                    DateTime fechaRecordatorio = new DateTime();
                    bool flag = false;
                    foreach (DSNoticias.infRecordatoriosRow infR in dsinftab.Rows)
                    {

                        if ((DateTime.Compare(infR.FHRecordatorio, FechaPublicacion) == 1) && (DateTime.Compare(infR.FHRecordatorio, System.DateTime.Now) <= 0))
                        {
                            fechaRecordatorio = infR.FHRecordatorio;
                            flag = true;
                            
                        }
                        // }

                    }

                    if (flag)
                        FechaLiteral.Text = fechaRecordatorio.ToShortDateString();
                }
                

                ControllerNoticias CN = new ControllerNoticias();
                int InfoID = int.Parse(InfoIDHiddenField.Value);
                CN.GetInfInfoByInfoID(InfoID);
                DSNoticias.infImagenDataTable ImagenDT = CN.Get_ImagenesByInfoID();

                SlideShowExtender ImagenNoticia_SlideShowExtender = e.Item.FindControl("ImagenNoticia_SlideShowExtender") as SlideShowExtender;
                if (ImagenDT.Rows.Count == 0)
                {
                    ImagenNoticia_SlideShowExtender.Enabled = false;
                    ImagenNoticia.Visible = false;
                    DivImagen.Visible = false;

                }
                else
                {
                    if (bool.Parse(ImagenDT.Rows[0]["flyer"].ToString()))
                    {
                        //flyer                                               
                        HtmlControl divNoFlyer = e.Item.FindControl("divNoFlyer") as HtmlControl;
                        HtmlControl divFlyer = e.Item.FindControl("divFlyer") as HtmlControl;
                        Image imgFlyer = e.Item.FindControl("imgFlyer") as Image;
                        imgFlyer.ImageUrl = "~/noticias/imagenes/"+ImagenDT.Rows[0]["path"].ToString();

                         string url = ImagenDT.Rows[0]["Descrip"].ToString();
                         HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImgflyer"));
                         if (url != "")
                         {
                             if (!url.Contains("http://") && (!url.Contains("https://")))
                                 url = "http://" + url;
                             lnkImg.HRef = url;
                         }
                         else
                         {
                             lnkImg.Attributes.Remove("href");
                             imgFlyer.Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + ImagenDT.Rows[0]["path"] + "'," + ImagenDT.Rows[0]["InfoID"].ToString() + ");");
                             imgFlyer.Attributes.Add("style", "cursor: pointer");
                         }

                        divNoFlyer.Visible = false;
                        divFlyer.Visible = true;
                        DivImagen.Visible = false;
                    }
                    else//imagen comun o video
                    {
                        string rutaimg = " ";
                        string rutavid = " ";
                        string url = ""; // si tiene para linkear
                        bool video = false;
                        bool imagen = false;

                        foreach (DAL.DSNoticias.infImagenRow imgs in ImagenDT.Rows)
                        {
                            string ruta = imgs.Path.ToString();
                            url = imgs.Descrip;

                            if (ruta.Contains(".mp4")) { video = true; rutavid = ruta; }
                            if (ruta.Contains(".jpg") || ruta.Contains("png")) { imagen = true; rutaimg = ruta; }
                        }


                      

                        if (imagen && !video)
                        {
                            url = setImagen(e, rutaimg, url, ImagenDT.Rows[0]["InfoId"].ToString());
                        }

                        if (video && !imagen)
                        {
                            DivImagen.Visible = false;
                            setVideo(e, DivImagen, rutavid);
                        }
                       

                        if (video && imagen)
                        {
                            url = setImagen(e, rutaimg, url, ImagenDT.Rows[0]["InfoId"].ToString());
                            setVideo(e, DivImagen, rutavid);
                        }

                    }
                }

                //
                DAL.DSNoticias.infLinkDataTable linkDT = new DAL.DSNoticias.infLinkDataTable();
                DataList RepeaterLink = e.Item.FindControl("RTLinks") as DataList;
                linkDT = cn.GetLinkByInfoID(InfoID);
                RepeaterLink.DataSource = linkDT;
                RepeaterLink.DataBind();
                //
                

                Repeater RepeaterArchivos = e.Item.FindControl("RepeaterArchivos") as Repeater;
                RepeaterArchivos.DataSource = CN.Get_ArchivosByInfoID();
                RepeaterArchivos.DataBind();
                ImagenNoticia_SlideShowExtender.ContextKey = InfoIDHiddenField.Value;

                Label lblConComents = (Label)e.Item.FindControl("lblConComents");
                lblConComents.Text = cn.getCantComments(InfoID).ToString();

                Label lblLikeCount = (Label)e.Item.FindControl("lblLikeCount");
                lblLikeCount.Text = cn.getVotos(InfoID, 1).ToString();

                //Label lblUnLikeCount = (Label)e.Item.FindControl("lblUnLikeCount");
                //lblUnLikeCount.Text = cn.getVotos(InfoID, -1).ToString();
            }
            //HtmlAnchor Eltooltip = new HtmlAnchor();
            Control votantes = e.Item.FindControl("votantes") as Control;
            HtmlAnchor Eltooltip = e.Item.FindControl("eltooltip") as HtmlAnchor;
            if (votantes != null)
            {
                Eltooltip.HRef = "#" + votantes.ClientID;
                Eltooltip.Attributes.Add("rel", "#" + votantes.ClientID);
            }

        }

        private void setVideo(DataListItemEventArgs e, HtmlGenericControl DivImagen,string rutavid)
        {
         
            string site = GetUrlSite().Replace("/", "%2F").Replace(":", "%3A");
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;
            ltvideo.Text = " <embed src='../noticias/imagenes/gddflvplayer.swf' flashvars='?&autoplay=false&sound=70&buffer=2&vdo=" + site + "%2Fnoticias%2Fimagenes%2F" + rutavid.Replace(".", "%2E") + "' width='420' height='315' allowFullScreen='false' quality='best' wmode='transparent' allowScriptAccess='always'  pluginspage='http://www.macromedia.com/go/getflashplayer'  type='application/x-shockwave-flash'></embed>";
        }

        private static string setImagen(DataListItemEventArgs e, string rutaimg, string url,string InfoId)
        {
            int cantidad = rutaimg.Length;
            string extencion = rutaimg.Substring(cantidad - 3, 3);
            HtmlAnchor lnkImg = ((HtmlAnchor)e.Item.FindControl("lnkImg"));

            if (rutaimg != " " && (extencion == "jpg" || extencion == "png") && (url != ""))
            {

                if (!url.Contains("http://") && (!url.Contains("https://")))
                    url = "http://" + url;
                lnkImg.HRef = url;
            }
            else
            {
                lnkImg.Attributes.Remove("href");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("onclick", "flyer2('~/noticias/Imagenes/" + rutaimg + "'," + InfoId + ");");
                ((Image)e.Item.FindControl("ImagenNoticia")).Attributes.Add("style", "cursor: pointer");
            }
            return url;
        }


        protected string GetUrlSite()
        {
            string url = ConfigurationManager.AppSettings["ASP_SITE"].ToString().Replace(":8080", "");//produccion
            string barra = url.Substring(url.Length - 1, 1);
            if (barra == "/")
                return url.Substring(0, url.Length - 1);
            else
                return ConfigurationManager.AppSettings["ASP_SITE"].ToString().Replace(":8080", "");//produccion

        }

        protected void RTLinks_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Literal ltvideo = e.Item.FindControl("ltvideo") as Literal;


            HyperLink link = e.Item.FindControl("HyperLink1") as HyperLink;
            link.Visible = false;//solo mostrara videos no links

            if (link.NavigateUrl.Contains("www.youtube.com") || link.NavigateUrl.Contains("http://youtu.be"))
            {
                string url;
                link.Visible = false;
                url = link.NavigateUrl.Replace("http://youtu.be/", "https://www.youtube.com/v/");

                url = url.Replace("watch?v=", "v/");
                ltvideo.Text = " <embed  width='420' height='315' src='" + url + "' > ";
            }



            if (link.NavigateUrl.Contains("vimeo.com"))
            {
                link.Visible = false;
                string url = link.NavigateUrl.Replace("https://vimeo.com/", "");
                ltvideo.Text = "<iframe src='//player.vimeo.com/video/" + url + "' width='420' height='315' frameborder='0' ></iframe>";
            }

        }

        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static AjaxControlToolkit.Slide[] GetSlides(string contextKey)
        {

            ControllerNoticias CN = new ControllerNoticias();
            int InfoID = int.Parse(HttpContext.Current.Request.QueryString["InfoID"]);
            CN.GetInfInfoByInfoID(InfoID);
            DSNoticias.infImagenDataTable ImagenDT = CN.GetImagenByInfoID();
            AjaxControlToolkit.Slide[] Imagenes = new AjaxControlToolkit.Slide[ImagenDT.Rows.Count];
            int i = 0;
            foreach (DSNoticias.infImagenRow imagen in ImagenDT.Rows)
            {
                Imagenes[i] = new AjaxControlToolkit.Slide(imagen.Path, "", "");
                i++;

            }
            return Imagenes;
        }






        protected void RNoticias_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "like")
            {
                ControllerNoticias CN = new ControllerNoticias();
                int InfoID = int.Parse(e.CommandArgument.ToString());
                if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID))
                {
                    CN.InsertInfoUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID, 1);
                    Label lblLikeCount = (Label)e.Item.FindControl("lbllikecount");
                    lblLikeCount.Text = CN.getVotos(InfoID, 1).ToString();
                    EjecutarScript("alert('Gracias por Votar');");
                }
                else
                    EjecutarScript("alert('Esta Noticia ya cuenta con su voto');");

            }

            if (e.CommandName == "unlike")
            {
                ControllerNoticias CN = new ControllerNoticias();
                int InfoID = int.Parse(e.CommandArgument.ToString());
                if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID))
                {
                    CN.InsertInfoUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, InfoID, -1);
                    Label lblUnLikeCount = (Label)e.Item.FindControl("lblUnLikeCount");
                    lblUnLikeCount.Text = CN.getVotos(InfoID, -1).ToString();
                    EjecutarScript("alert('Gracias por Votar');");
                }
                else
                    EjecutarScript("alert('Esta Noticia ya cuenta con su voto');");
            }
        }

        protected string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            litDivHeaderOpen.Visible = _displayHeader;
            litDivHeaderClose.Visible = _displayHeader;

        }

        public string TruncateString(string valueToTruncate, int maxLength, TruncateOptions options)
        {
            if (valueToTruncate == null || maxLength <= 0)
            {
                return "";
            }

            if (valueToTruncate.Length <= maxLength)
            {
                return valueToTruncate;
            }

            bool includeEllipsis = (options & TruncateOptions.IncludeEllipsis) == TruncateOptions.IncludeEllipsis;
            bool finishWord = (options & TruncateOptions.FinishWord) == TruncateOptions.FinishWord;
            bool allowLastWordOverflow = (options & TruncateOptions.AllowLastWordToGoOverMaxLength) == TruncateOptions.AllowLastWordToGoOverMaxLength;

            string retValue = valueToTruncate;

            if (includeEllipsis)
            {
                maxLength -= 1;
            }

            int lastSpaceIndex = retValue.LastIndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);

            if (!finishWord)
            {
                retValue = retValue.Remove(maxLength);
            }
            else if (allowLastWordOverflow)
            {
                int spaceIndex = retValue.IndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);
                if (spaceIndex != -1)
                {
                    retValue = retValue.Remove(spaceIndex);
                }
            }
            else if (lastSpaceIndex > -1)
            {
                retValue = retValue.Remove(lastSpaceIndex);
            }

            if (includeEllipsis && retValue.Length < valueToTruncate.Length)
            {
                retValue += " [&hellip;]";
            }
            return retValue;
        }

        protected string TruncarString(string Texto, int Longitud)
        {
            return TruncateString(Server.HtmlDecode(Texto), Longitud, TruncateOptions.IncludeEllipsis);
        }
        public enum TruncateOptions
        {
            None = 0x0,
            [Description("Make sure that the string is not truncated in the middle of a word")]
            FinishWord = 0x1,
            [Description("If FinishWord is set, this allows the string to be longer than the maximum length if there is a word started and not finished before the maximum length")]
            AllowLastWordToGoOverMaxLength = 0x2,
            [Description("Include an ellipsis HTML character at the end of the truncated string.  This counts as one of the characters for the maximum length")]
            IncludeEllipsis = 0x4
        }
        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }

        protected void lbtnFirst_Click(object sender, EventArgs e)
        {
            if (IsHome)
                FillRepeaterHome(Navigation.First);
            else
                FillRepeater(Navigation.First);
        }

        protected void lbtnPrev_Click(object sender, EventArgs e)
        {
            //Fill repeater for Previous event

            if (IsHome)
                FillRepeaterHome(Navigation.Previous);
            else
            FillRepeater(Navigation.Previous);
        }

        protected void lbtnNext_Click(object sender, EventArgs e)
        {
            //Fill repeater for Previous event
            if (IsHome)
                FillRepeaterHome(Navigation.Next);
            else
            FillRepeater(Navigation.Next);
        }

        protected void lbtnLast_Click(object sender, EventArgs e)
        {

            if (IsHome)
                FillRepeaterHome(Navigation.Last);
            else
             FillRepeater(Navigation.Last);
        }
        public string Visibility(string message)
        {
            string visibility = "visibility: {0}";

            //Generates the visibility for Mmessage
            if (message.Equals("MSG", StringComparison.CurrentCultureIgnoreCase))
                return string.Format(visibility, ListCount == 0 ? "visible" : "hidden");

            if (message.Equals("Navigation", StringComparison.CurrentCultureIgnoreCase))
                return string.Format(visibility, ListCount < CantFilas ? "hidden" : "visible");
            //Generates the visibility for Navigation
            if (message.Equals("Navigation", StringComparison.CurrentCultureIgnoreCase))
                return string.Format(visibility, ListCount == 0 ? "hidden" : "visible");

            //Generates the visibility for Repeater
            if (message.Equals("Repeater", StringComparison.CurrentCultureIgnoreCase))
                return string.Format(visibility, ListCount > 0 ? "visible" : "hidden");

            //Default visibility
            return string.Format(visibility, "hidden");
        }

        protected void updLike_DataBinding(object sender, EventArgs e)
        {
            Label lblll = (Label)RNoticias.FindControl("lblcontent");
            
        }

    
    }
}
