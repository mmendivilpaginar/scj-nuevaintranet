﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucVoluntariadoActividades.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucVoluntariadoActividades" %>
<%@ Register src="ucBannerVoluntariado.ascx" tagname="ucBannerVoluntariado" tagprefix="uc2" %>
<script language="javascript" type="text/javascript">
    $overlay = $('<div id="contentx"></div>');
    function getinformation(identificador) {
        var btn = document.getElementById('<%= btnTrigger.ClientID %>');
        var id = parseInt(identificador);
        $("input[id*=HFLegajo]").val(identificador);
        btn.click();
    }
    function pageScroll() {
        document.getElementById('detalle').scrollIntoView();
    }

</script>
<h2>Actividades de Voluntariado</h2>

<uc2:ucBannerVoluntariado ID="ucBannerVoluntariado1" runat="server" />
<br />

<div id="msgOk" class="messages msg-exito" runat="server" visible="false">
    La operación fue realizada
</div>
<asp:HiddenField ID="HFLegajo" runat="server" />

                <asp:Repeater ID="rptAliadosActivos" runat="server" DataSourceID="ODSGVInscripcion">
                    <HeaderTemplate><div class="wrapper-grilla voluntariado-actividades"><ul class="pnet-action-list"></HeaderTemplate>
                    <ItemTemplate>
                        <li><asp:Literal ID="Literal1" runat="server" Text='<%# getinformationV2(Eval("IdActividad"),RutaImagen(Eval("Imagen"))) %>'></asp:Literal></li>
                    </ItemTemplate>
                    <FooterTemplate></div></ul></FooterTemplate>
                </asp:Repeater>
<asp:ObjectDataSource ID="ODSGVInscripcion" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="ActividadSeleccionarDisponibles" 
    TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
    <SelectParameters>
        <asp:ControlParameter ControlID="HFLegajo" Name="Legajo" PropertyName="Value" 
            Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<br />
<div id="detalle"></div>
<asp:Button ID="btnSuscribirse" runat="server" Text="Suscribir" 
    onclick="btnSuscribirse_Click" Visible="False" />
<asp:Button ID="btnTrigger" runat="server" style="display:none" Text="x" />
<br />
    <div class="pnet-modal-overlay"></div>
    <div class="pnet-wrapper-modal">
        <a href="javascript:void(0)" class="pnet-modal-cerrar" onclick="cerrarModal()">Cerrar</a>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:FormView ID="FVDetalleActividad" runat="server" DataKeyNames="IdActividad" 
    DataSourceID="ODSFVActividad" ondatabound="FVDetalleActividad_DataBound">
    <ItemTemplate>
    <div class="pnet-inner-modal">
        <div class="pic">
        <div class="item">
        <asp:Image ID="Image2" runat="server" ImageUrl='<%# "/Voluntariado/Imagenes/" + Eval("Imagen") %>' Width="100" />
        </div>
        </div>
        <div class="info">
        <div class="item">
        <strong>
        Actividad:
        </strong><br />
        <asp:Label ID="ActividadLabel" runat="server" Text='<%# Bind("Actividad") %>' />
        <br />
        </div>
        <div class="item">
        <strong>
        Descripción:
        </strong><br />
        <asp:Label ID="DescripcionLabel" runat="server" 
            Text='<%# Bind("Descripcion") %>' />
        </div>
        <div class="item">
        <strong>
        Que:
        </strong><br />
        <asp:Label ID="QueLabel" runat="server" Text='<%# Bind("Que") %>' />
        </div>
        <div class="item">
        <strong>
        Cuando:
        </strong><br />
        <asp:Label ID="CuandoLabel" runat="server" Text='<%# Bind("Cuando") %>' />
        </div>
        <div class="item">
        <strong>
        A Quienes:
        </strong><br />        
        <asp:Label ID="AQuienesLabel" runat="server" Text='<%# Bind("AQuienes") %>' />
        </div>
        </div>

    </ItemTemplate>
</asp:FormView>
<asp:ObjectDataSource ID="ODSFVActividad" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="ActividadSelect" 
    TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
    <SelectParameters>
        <asp:ControlParameter ControlID="HFLegajo" Name="IdActividad" 
            PropertyName="Value" Type="Int32" />
    </SelectParameters>
    
</asp:ObjectDataSource>
<asp:Literal ID="ltscroll" runat="server"></asp:Literal>
</ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnTrigger" EventName="Click" />
                    </Triggers>
</asp:UpdatePanel>
</div>





