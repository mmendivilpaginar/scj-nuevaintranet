﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucEfemerides.ascx.cs"
    Inherits="com.paginar.johnson.Web.UserControl_Home.ucEfemerides" %>
<asp:HiddenField ID="hdClusterID" runat="server" />
<div class="box box-ext">
    <!--[titulo]-->
    <h2>
        Efemérides</h2>
    <!--[/titulo]-->
    <!--[contenido]-->
    <div id="efemerides">
        <div class="tabs">
            <ul>
                <li><a href="#efemerides-t1">Hoy</a></li>
                <li><a href="#efemerides-t2">Del mes</a></li>
            </ul>
            <div id="efemerides-t1">
                <asp:DataList ID="dtHoy" runat="server" DataSourceID="odsHoy" RepeatLayout="Flow">
                    <ItemTemplate>
                        <asp:Literal ID="ltefemeride" runat="server" Text='<%# Bind("descripcion") %>'></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtHoy.Items.Count==0).ToString())%>' Text="No hay efemérides en el día de hoy">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsHoy" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getEfemeridesHoy" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdClusterID" Name="clusterid" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <div id="efemerides-t2" class="sinBullets">
                <asp:DataList ID="dtMes" runat="server" DataSourceID="odsMes" RepeatLayout="Flow">
                    <ItemTemplate>
                        <asp:Literal ID="ltDia" runat="server" Text='<%# Eval("dia","{0: d}") %>'></asp:Literal>
                        -
                        <asp:Literal ID="ltefemeride" runat="server" Text='<%# Bind("descripcion") %>'></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtMes.Items.Count==0).ToString())%>' Text="No hay efemérides este mes">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsMes" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getEfemeridesMes" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdClusterID" Name="clusterid" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
    <!--[/contenido]-->
</div>
