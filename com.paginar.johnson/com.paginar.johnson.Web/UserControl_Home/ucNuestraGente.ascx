﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNuestraGente.ascx.cs"
    Inherits="com.paginar.johnson.Web.UserControl_Home.ucNuestraGente" %>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $('a.toolficha').cluetip({
            splitTitle: '|',
            width: 110,
            positionBy: 'bottomTop',
            arrows: false,
            clickThrough: true
        });
    });

    function load() {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    }
    function EndRequestHandler() {
        $('a.toolficha').cluetip({
            splitTitle: '|',
            width: 110,
            positionBy: 'bottomTop',
            arrows: false,
            clickThrough: true
        });
    }
    $(window).load(function () {
        load();
    });
</script>

<div class="box box-ext">
    <!--[titulo]-->
    <h2>
        Nuestra Gente</h2>
    <!--[/titulo]-->
    <!--[contenido]-->
    <asp:HiddenField ID="hdClusterID" runat="server" />
    <div id="usuarios">
        <asp:UpdatePanel ID="updUsuarios" runat="server" onload="updUsuarios_Load">
            <ContentTemplate>
                <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                    <ItemTemplate>
                        <div class="foto">
                            <a class="toolficha" href='<%# "../nuestracompania/buscadordepersonas.aspx?UsuarioID="+ Eval("usuarioid")  %>' title='Ver ficha|'>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' /></a>
                                <br />
                                
                                <%# Eval("interno").ToString() != "" ? " Interno: " + Eval("interno") + "<br />": ""%>
                                <asp:Image ID="imgBandera" runat="server" 
                                    ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>'  Width="16px" Height="11px" /> <%# Eval("UBICACIONDET").ToString() != "" ? " " + Eval("UBICACIONDET") + " ": ""%> 
                                
                                
                        </div>
                        <div class="info">
                            <ul>
                                <asp:HiddenField ID="hdUsuario" runat="server" Value="13742" />
                                <li><b>
                                    <%# Eval("nombre")%>
                                    <%# Eval("apellido")%></b></li>
                                <li>
                                    <%# Eval("DireccionDET").ToString().Trim() != "" ? "- " + Eval("DireccionDET") : ""%>
                                </li>
                                <li>
                                    <%# Eval("areadesc").ToString().Trim() != "" ? "- " + Eval("areadesc") : ""%>
                                </li>
                                <li>
                                    <%--<%# Eval("cargodet").ToString().Trim() != "" ? "- " + Eval("cargodet") : ""%>--%>
                                </li>
                                
                                <li><a href='/nuestracompania/buscadordepersonas.aspx?UsuarioID=<%# Eval("usuarioid") %>'><strong> Ver Ficha</strong></a></li>

                            </ul>
                            <ul class="action-list">
                                <li>
                                    <asp:ImageButton ID="imgprev" runat="server" ImageUrl="~/css/images/prev.gif" ToolTip="Anterior"
                                        OnClick="imgprev_Click" /></li>
                                <li>
                                    <asp:ImageButton ID="imgnext" runat="server" ImageUrl="~/css/images/next.gif" ToolTip="Siguiente"
                                        OnClick="imgnext_Click" /></li>
                                <li>
                                    <asp:UpdateProgress ID="upgUsuarios" runat="server" AssociatedUpdatePanelID="updUsuarios">
                                        <ProgressTemplate>
                                            <img src="../images/preloader.gif" class="preloader" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </li>
                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--/usuarios-->
    <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="getDataUsuarioRandom" TypeName="com.paginar.johnson.BL.ControllerUCHome">
    </asp:ObjectDataSource>
    <!--[/contenido]-->
</div>
