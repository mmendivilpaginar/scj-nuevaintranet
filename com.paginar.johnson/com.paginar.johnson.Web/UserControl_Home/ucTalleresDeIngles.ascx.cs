﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucTalleresDeIngles : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            switch (((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString())
            {
                case "1":
                    DivTaller.Visible = true;
                    break;
                default:
                    DivTaller.Visible = false;
                    break;
            }

        }
    }
}