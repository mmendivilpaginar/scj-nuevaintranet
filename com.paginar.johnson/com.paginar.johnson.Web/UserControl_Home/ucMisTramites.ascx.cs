﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Data;


namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucMisTramites : CustomControlUser
    {

        public DataTable SelectDataTable(DataTable dt, string filter, string sort, int contador)
        {
            int count = 1;
            DataRow[] rows = null;
            DataTable dtNew = default(DataTable);
            // copy table structure
            dtNew = dt.Clone();
            // sort and filter data
            rows = dt.Select(filter, sort, DataViewRowState.CurrentRows);
            // fill dtNew with selected rows
            foreach (DataRow dr in rows)
            {
                if(count <= contador)
                    dtNew.ImportRow(dr);
                count++;
            }
            // return filtered dt
            return dtNew;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdUsuarioID.Value = ((MasterBase)this.Page.Master).ObjectUsuario.usuarioid.ToString();
                
                //ControllerUCHome ch = new ControllerUCHome();
                //DsUCHome.frmTramiteDevDataTable dt = ch.frmMisTramiteDev(int.Parse(hdUsuarioID.Value));                
                //dt = (DsUCHome.frmTramiteDevDataTable)SelectDataTable(dt, "tra_estado=1", "tra_FHAlta desc", 4);
                //if (dt.Rows.Count > 3)
                //{
                //    hlvermas.Enabled = true;
                //    hlvermas.Visible = true;

                //}  

                //dt = (DsUCHome.frmTramiteDevDataTable)SelectDataTable(dt, "tra_estado=1", "tra_FHAlta desc", 3);
                  
                //DLMisTramites.DataSource = dt;
                //DLMisTramites.DataBind();

                

                //// Tramites asignados
                //DsUCHome.frmTramiteDevDataTable dtfrmTramitesAsignados = ch.frmTramitesAsignadosDev(int.Parse(hdUsuarioID.Value));
                //dtfrmTramitesAsignados = (DsUCHome.frmTramiteDevDataTable)SelectDataTable(dtfrmTramitesAsignados, "tra_estado=1", "tra_FHAlta desc", 4);

                //if (dtfrmTramitesAsignados.Rows.Count > 3)
                //{
                //    HyperLinkVerMas.Enabled = true;
                //    HyperLinkVerMas.Visible = true;
                //}

                //dtfrmTramitesAsignados = (DsUCHome.frmTramiteDevDataTable)SelectDataTable(dtfrmTramitesAsignados, "tra_estado=1", "tra_FHAlta desc", 3);

                //DataListTramitesAsignados.DataSource = dtfrmTramitesAsignados;
                //DataListTramitesAsignados.DataBind();

                
                //if (DataListTramitesAsignados.Items.Count == 0 && DLMisTramites.Items.Count == 0) this.Visible = false;

            }
        }

        protected void odsTramites_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception != null)
            {

                //this tells the ObjectDatasource : It's ok, i'm taking care of this
                //and don't rethrow it.
                e.ExceptionHandled = true;

                // handle exception here (log/display to user etc ...)
            }

        }
    }
}