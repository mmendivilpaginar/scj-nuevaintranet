﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.UserControl_Home
{

   

    public partial class ucBienvenido : CustomControlUser
    {


        private int _usuarioid;
        public int usuarioid
        {
            get { return _usuarioid; }
            set { _usuarioid = value; }
        }

    

        protected void Page_Load(object sender, EventArgs e)
        {
            hdUsuario.Value = ((MasterBase)this.Page.Master).ObjectUsuario.usuarioid.ToString();
            int clusterid = int.Parse(((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString());
            HyperLink MiPerfil = (HyperLink)frmUsuario.FindControl("MiPerfil");
            if (clusterid > 4)
            {            
                MiPerfil.Visible = false;
            }
            
        }

        protected void frmUsuario_OnDataBound(object sender, EventArgs e)
        {
            if (((Image)frmUsuario.FindControl("imgFoto")).ImageUrl.ToUpper() == "../IMAGES/PERSONAS/SINIMAGEN.GIF")
            {
                ((Panel)frmUsuario.FindControl("pnlFaltaFoto")).Visible = true;
                if (((HiddenField)frmUsuario.FindControl("hidCluster")).Value == "2")
                    ((Label)frmUsuario.FindControl("lblContacto")).Text = "CONTACTAR A: Jazmín Sanhueza o Fabia Berri";
                else
                    ((Label)frmUsuario.FindControl("lblContacto")).Text = "CONTACTAR A:  María del Carmen Aranguren";

            }
        }
     

    }
}