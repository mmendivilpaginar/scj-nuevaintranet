﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMisTramites.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucMisTramites" %>

<div class="box box-ext">

    <asp:HiddenField ID="hdUsuarioID" runat="server" />
    <!--[titulo]-->
    <%--<h2>Mis Trámites</h2>--%>
    <hr />
    <!--[/titulo]-->

    <!--[contenido]-->
    <div id="mis-tramites">

      <a HREF="https://fed.scj.com:9031/idp/startSSO.ping?PartnerSpId=http://www.workday.com"   onclick="javascript:CallServiceGuardarEstadistica('https://fed.scj.com:9031/idp/startSSO.ping?PartnerSpId=http://www.workday.com','WorkDay');" target="_blank">
 
       <div  style="display: block;    margin-left: auto;  margin-right: auto;" >
       
                    <asp:Image ID="Image1"  runat="server"   ImageUrl="~/images/Workday_logo_r.jpg"  Width="200px"/>
                
       </div>
       </a>
      <%--  <div class="tabs">
            <ul>
                <li><a href="#mis-tramites-t1">Iniciados</a></li>
                <li><a href="#mis-tramites-t2">Asignados</a></li>
            </ul>
            <div id="mis-tramites-t1">
                <asp:DataList ID="DLMisTramites" runat="server" RepeatLayout="Flow" >
                 <ItemTemplate>
                      <asp:HyperLink ID="HyperLink1" CssClass="listaTramites" runat="server" Text='<%# Eval("frm_titulo") %>' NavigateUrl ='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                        </asp:HyperLink>
                  </ItemTemplate>
                </asp:DataList>

                <asp:ObjectDataSource ID="odsTramites" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="frmTramiteDev" 
                    TypeName="com.paginar.johnson.BL.ControllerUCHome" 
                    onselected="odsTramites_Selected">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdUsuarioID" Name="usralta" 
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                 <br />
                <asp:HyperLink ID="hlvermas" runat="server" Enabled="False" Font-Bold="True" 
                     NavigateUrl="~/servicios/Tramites.aspx" Visible="False">Ver Más</asp:HyperLink>
          </div>
            <div id="mis-tramites-t2">
                 <asp:DataList ID="DataListTramitesAsignados" runat="server" 
                    RepeatLayout="Flow"  >
                 <ItemTemplate>
                      <asp:HyperLink ID="HyperLink1" CssClass="listaTramites" runat="server" Text='<%# Eval("frm_titulo") %>' NavigateUrl ='<%# "/ASPForm.aspx?url=" + Server.UrlEncode( "servicios/" + Eval("frm_asp") + "?FormularioID=" + Eval("frm_ID") + "&ASP=" + Eval("frm_asp") + "&TramiteID=" + Eval("tra_id") + "&FormEstado=NUEVO_PASO")  %>'>                                
                                        </asp:HyperLink>
                  </ItemTemplate>
                </asp:DataList>

                <asp:ObjectDataSource ID="ObjectDataSourceTramitesAsignados" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="frmTramitesAsignadosDev" 
                    TypeName="com.paginar.johnson.BL.ControllerUCHome" 
                    onselected="odsTramites_Selected">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdUsuarioID" DefaultValue="" Name="usrActual" 
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>

                 <br />
                <asp:HyperLink ID="HyperLinkVerMas" runat="server" Enabled="False" Font-Bold="True" 
                     NavigateUrl="~/servicios/Tramites.aspx" Visible="False">Ver Más</asp:HyperLink>

          </div>
          </div>--%>
    </div>
    <!--[/contenido]-->
</div>






