﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPDA.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucPDA" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:HiddenField ID="HFUsuarioId" runat="server" />
<script language="javascript" type="text/javascript">

    function loadMandatory() {
        $('.item-modulo.obligatory').append('<div class="mandatory-tooltip"></div>');
    }

    function getinformation(identificador) {
        //$('.content-modal').html('<img src="../images/preloader.gif" alt="Cargando..." />');
        var btn = document.getElementById('<%= btnTrigger.ClientID %>');
        var id = parseInt(identificador);
        $("input[id*=HFCursoSelect]").val(identificador);
        btn.click();
        $('.modal-overlay,.content-modal').show();        
        //$('#<%= btnTrigger.ClientID %>').addClass('hover');
        $('.item-modulo:nth('+(identificador - 1)+')').addClass('hover');
    }

    function cerrarModal() {
        $('.modal-overlay,.content-modal').hide();
        $('.item-modulo.hover').removeClass('hover');
    }

    function toggleButton(nuMod) 
    {
        
        if ($("input[id*=CheckBox1_" + (nuMod - 1) + "]").is(':checked')) {
            $("input[id*=CheckBox1_" + (nuMod - 1) + "]").attr('checked', false);
            $("input[id*=CheckBox1_" + (nuMod - 1) + "]").parent().removeClass('selected');
        }
        else {
            if ($("input[id*=CheckBox1_" + (nuMod - 1) + "]").is(':enabled')) {
                $("input[id*=CheckBox1_" + (nuMod - 1) + "]").attr('checked', true);
                $("input[id*=CheckBox1_" + (nuMod - 1) + "]").parent().addClass('selected');
            } else {
                alert("El módulo que intenta seleccionar no se encuentra habilitado");
            }

        }
        return false;
    }

    function clickaction(){

    var seleccionado = false;
    var contador = 0;
    var indice = 1;
    var cursos;
    cursos = "";

    $(".colpda-all :checkbox").each(function () {
        
        if (this.checked) {
            seleccionado = true;
            cursos += "- " + $("#<%=RPTcursos.ClientID%>").find("div").eq(indice).find("input").eq(1).html() + "\n";

            contador++;
        }
        indice++;
    });

    if (contador > 0) {

        if ($("#<%=HFmisCursosCant.ClientID%>").val() >= 5) {
            alert("Usted ya se inscribió en los cursos.")
            return false;
        }
        
        if (contador < 3 || contador > 5) {
            alert("Debe seleccionar entre 1 y 3 cursos.")
            return false;
        }
        if (!confirm("Tu participación en este/os curso/s seleccionados será confirmada.\n Recursos Humanos se estará poniendo en contacto con vos. \n Muchas gracias \n ¿Desea continuar?")) {
            return false;
        }
        else {
            return true;
        }

    } else {
        alert("Debe seleccionar al menos un curso");
        return false;
    }

    return true;
}


    

</script>
<asp:HiddenField ID="HFCursoSelect" runat="server" Value="0"></asp:HiddenField>
<asp:HiddenField ID="HFmisCursosCant" runat="server" />
<div id="contentx"></div>
<div id="logoSCJ">
    <img src="css/images/logo.png" />
</div>

    <asp:Panel ID="pnlOk" runat="server">

<div class="header-pda">
<div id="producto">
    <img src="css/images/producto.png" />
</div>
<h2>ELEGÍ LOS MÓDULOS QUE MÁS TE INTERESEN Y ENVIANOS TU SOLICITUD</h2>
<div id="pdaLogo">
    <img src="css/images/logoPDA.png" />
</div>
</div>
<div class="subheader-pda">
<h3>SELECCIONA 5 MÓDULOS Y ENVIÁ EL FORMULARIO</h3>
</div>
<div class="content-pda">
<div class="colpda-left">
<div class="data-item"><asp:Label ID="lblApyNom" runat="server" Text=""></asp:Label></div>
<div class="data-item"><asp:Label ID="lblLegajo" runat="server" Text=""></asp:Label></div>
<div class="data-item"><asp:Label ID="lblMail" runat="server" Text=""></asp:Label></div>
<div class="data-item"><asp:Label ID="lblSector" runat="server" Text=""></asp:Label></div>
</div>
    
<div class="colpda-rigth">
    <asp:TextBox ID="tbComentario" runat="server" TextMode="MultiLine">Comentario:</asp:TextBox>
</div>

<div class="colpda-all">

    <p>Posá el mouse sobre los módulos y conocé su contenido. Cliqueá sobre ellos para elegirlos. Podes seleccionar hasta 3 módulos</p>

    <asp:Repeater ID="RPTcursos" runat="server" DataSourceID="ODScursoPDSGV" 
        onitemcommand="RPTcursos_ItemCommand" 
        onitemdatabound="RPTcursos_ItemDataBound">
        <ItemTemplate>
            <asp:Literal ID="estado" Text='<%# dameEnableEstadoStr(Eval("idEstado"),Eval("idCurso")) %>' runat="server"></asp:Literal>
        
            <asp:CheckBox ID="CheckBox1" runat="server" 
        Enabled='<%# dameEnableEstado(Eval("idEstado")) %>' />
            <asp:LinkButton ID="LinkButtonN" runat="server" rel='<%# Eval("idCurso") %>'
                Text='<%# getModuloLabel(Eval("idCurso")) %>' OnClientClick="return toggleButton($(this).attr('rel'))"
                ondatabinding="LinkButton1_DataBinding"></asp:LinkButton>

            <asp:HiddenField ID="HiddenField1" runat="server" 
            Value='<%# Eval("idCurso") %>' />
        </div>
        </ItemTemplate>
    </asp:Repeater>
    <br />


<asp:ObjectDataSource ID="ODScursoPDSGV" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="pda_cursoSelectAll" 
    TypeName="com.paginar.johnson.BL.ControllerPDA">
</asp:ObjectDataSource>
<asp:Button ID="btnSuscribir" runat="server" OnClientClick="return clickaction()" onclick="btnSuscribir_Click" 
    Text="Enviar" />
<br />
<asp:Button ID="btnTrigger" runat="server" style="display:none" Text="x" />
<div class="modal-overlay"></div>
<div class="content-modal">
<%--<div class="modal-cerrar" onclick="javascript:cerrarModal()"></div>--%>
    
    
<asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
    <ContentTemplate> 
<asp:FormView ID="FVdetallePDA" runat="server" DataKeyNames="idCurso" 
    DataSourceID="ODSFVDetallesPDA">
    <ItemTemplate>
        <div class="wapper">            
        <div class="mod-number">MÓDULO <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("idCurso") %>'></asp:Literal></div>
        <div class="titulo">
        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("Nombre") %>'></asp:Literal>        
        </div>
        <div class="duracion-wrapp">
        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Copete") %>' ></asp:Literal>                
        </div>
        <div class="objetivos">
        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Literal>                
        </div>

        <div class="duracion-wrapp">
        TEORÍA Y PRÁCTICA <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("Duracion") %>'></asp:Literal>                
        </div>
        <div class="duracion-wrapp">
        PERÍODO TENTATIVO <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("PeriodoTentativo") %>'></asp:Literal>
        </div>


        </div>
    </ItemTemplate>
</asp:FormView>

<asp:ObjectDataSource ID="ODSFVDetallesPDA" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="pda_cursoSelectByID" 
    TypeName="com.paginar.johnson.BL.ControllerPDA">
    <SelectParameters>
        <asp:ControlParameter ControlID="HFCursoSelect" Name="CursoId" 
            PropertyName="Value" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
    </ContentTemplate> 
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnTrigger" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
</div>
</div>
</div>
    </asp:Panel>
    <asp:Panel ID="pnlBad" runat="server">
    <div class="header-pda">
    <div id="Div1">
        
    </div>
    <h2></h2>
    <div id="Div2">
        
    </div>
    </div>

    <div class="subheader-pda">
    <h3></h3>
    </div>
    <div class="content-pda">
        
    
        <asp:Literal ID="lblmsg" runat="server"></asp:Literal>
    </div>
    </asp:Panel>
    <asp:Panel ID="okConfirm" runat="server">  
        <div class="header-pda">
    <div id="Div3">
        
    </div>
    <h2></h2>
    <div id="Div4">
        
    </div>
    </div>

    <div class="subheader-pda">
    <h3></h3>
    </div>
    <div class="content-pda">
    <div class="messages msg-exito">
        <span lang="ES-TRAD">La operación fue realizada exitosamente</span>    
        </div>
        </div>
    </asp:Panel>