﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBienvenido.ascx.cs"
    Inherits="com.paginar.johnson.Web.UserControl_Home.ucBienvenido" %>
<script type="text/javascript">
    $(function () {
        $('a.load-localFoto').cluetip({
            local: true,
            hideLocal: true,
            sticky: true,
            arrows: true,
            cursor: 'pointer',
            width: 300,
            mouseOutClose: true

        });

    });
</script>
<style type="text/css">
    #contactoFoto
    {
        height: 50px;
        overflow: auto;
    }
    #contactoFoto table
    {
        border: 1px solid #2A3C6D;
        border-collapse: collapse;
    }
    #contactoFoto table th, .normal td
    {
        border: 1px solid #2A3C6D;
        text-align: center;
    }
    #contactoFoto table th
    {
        text-transform: uppercase;
        padding-bottom: 10px;
        padding-left: 10px;
        padding-right: 10px;
        text-align: center;
    }
</style>
<div class="box">
    <!--[titulo]-->
    <h2>
        Bienvenido</h2>
    <!--[/titulo]-->
    <!--[contenido]-->
    <div id="bienvenido">
        <asp:HiddenField ID="hdUsuario" runat="server" />
        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario" OnDataBound="frmUsuario_OnDataBound">
            <ItemTemplate>
                <span class="foto">
                    <asp:Image ID="imgFoto" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                    <asp:Panel ID="pnlFaltaFoto" Visible="false" runat="server">
                        ¿Falta tu foto?
                        <div style="text-align: center">
                            <a class="load-localFoto" href="#contactoFoto" rel="#contactoFoto">Click Aquí</a>
                        </div>
                        <div id="contactoFoto">
                            <table border="1">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblContacto" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </span><span class="info">
                    <ul>
                        <li><b>
                            <%# Eval("nombre")%>
                            <%# Eval("apellido")%></b></li>
                        <li>
                            <%# Eval("UBICACIONDET").ToString().Trim() != "" ? "- " + Eval("UBICACIONDET") : ""%>
                        </li>
                        <li>
                            <%# Eval("DireccionDet").ToString().Trim() != "" ? "- " + Eval("DireccionDet") : ""%>
                        </li>
                        <li>
                            <%# Eval("areadesc").ToString().Trim() != "" ? "- " + Eval("areadesc") : ""%>
                        </li>
                       <%-- <li>
                            <%# Eval("cargodet").ToString().Trim() != "" ? "- " + Eval("cargodet") : ""%>
                        </li>--%>
                        <li>
                            <%# Eval("interno").ToString().Trim() != "" ? "- Interno: " + Eval("interno") : ""%>
                        </li>
                    </ul>
                    <asp:HyperLink ID="MiPerfil" runat="server" NavigateUrl="~/nuestracompania/miPerfil.aspx">Mi perfil</asp:HyperLink>
                    <!--a href="../nuestracompania/miPerfil.aspx" title="Mi perfil" target="">Mi perfil</a-->
                </span>
                <asp:HiddenField ID="hidCluster" Value='<%#Eval("Cluster") %>' runat="server" />
            </ItemTemplate>
        </asp:FormView>
        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
            <SelectParameters>
                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    <!--[/contenido]-->
</div>
