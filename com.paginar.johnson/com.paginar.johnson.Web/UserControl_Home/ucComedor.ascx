﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucComedor.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucComedor" %>
<div class="box box-ext">
   
    <h2>Menú del Día</h2>  
       
    <div id="santos">
        <div class="tabs">
	        <ul>
		        <li><a href="#santos-t1">Hoy</a></li>
		        <li><a href="#santos-t2">Esta Semana</a></li>
	        </ul>
	        <div id="santos-t1">
                <asp:HiddenField ID="hdClusterID" runat="server" />

                    <asp:DataList ID="dtHoy" runat="server" DataSourceID="odsUbicacion" RepeatLayout="Flow" onitemdatabound="dtHoy_ItemDataBound">
                        <ItemTemplate>        
                            <asp:Label ID="lbldescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>                                                     

                            <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                            <asp:Panel ID="pnlComedor" runat="server">                                
-                                <div class="comedorCard">
                                    <asp:HiddenField ID="hdUbicacionID" runat="server"  Value='<%# Bind("ubicacionid") %>'/>                                    
                                    Menu del Día
                                    <br />
                                    <asp:Label ID="lblUbicacion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>                                                     
                                    <br />
                                    <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsComedor">
                                        <ItemTemplate>                                           
                                            <div class="info">
                                                <ul>
                                                    <li> <%# Eval("Entrada").ToString()!=""? ">Entrada: "+Eval("Entrada") : "" %>  </li>
                                                    <li> <%# Eval("PPrincipal").ToString() != "" ? ">Plato Principal: " + Eval("PPrincipal") : ""%>  </li>                                                    
                                                    <li> <%# Eval("Postre").ToString() != "" ? ">Postre: " + Eval("Postre") : ""%>  </li>
                                                    <li> <%# Eval("Cena").ToString() != "" ? ">Cena: " + Eval("Cena") : ""%>  </li>                                                    
                                                    <li> <%# Eval("Express").ToString() != "" ? ">Menú Express: " + Eval("Express") : ""%>  </li>                                                    
                                                    <li> <%# Eval("Vegetariano").ToString() != "" ? ">Menú Vegetariano: " + Eval("Vegetariano") : ""%>  </li>                                                                                                        
                                                    <li> <%# Eval("Dieta").ToString() != "" ? ">Dieta: " + Eval("Dieta") : ""%>  </li>                                                                                                        
                                                    <li> <%# Eval("Oficina").ToString() != "" ? ">Menú Oficina: " + Eval("Oficina"): ""%>  </li>                                                                                                                                                            
                                                </ul>                                                
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsComedor" runat="server" 
                                        OldValuesParameterFormatString="original_{0}" SelectMethod="getComedorHoy" 
                                        TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdUbicacionID" Name="ubicacionid" 
                                                PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>

                            </asp:Panel>

                        </ItemTemplate>
                    </asp:DataList>
                    <asp:ObjectDataSource ID="odsUbicacion" runat="server" 
                         OldValuesParameterFormatString="original_{0}" SelectMethod="getUbicacion" 
                         TypeName="com.paginar.johnson.BL.ControllerUCHome">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hdClusterID" Name="clusterid" 
                                PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                </asp:ObjectDataSource>
	        </div>
	        <div id="santos-t2">
            <ul>
             <li>> Lunes </li>
             <li>> Martes</li>
             <li>> Miercoles</li>
             <li>> Jueves</li>
             <li>> Viernes</li>
            </ul>

<%--                <asp:DataList ID="dtMes" CssClass="default-list"  runat="server" DataSourceID="odsMes" RepeatLayout="Flow">
                <ItemTemplate> 
                    <asp:Literal ID="ltDia" runat="server" Text='<%# Eval("dia","{0: dd}") %>'></asp:Literal> -       
                    <asp:Literal ID="ltsanto" runat="server"  Text='<%# Bind("descripcion") %>'></asp:Literal> 
                </ItemTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsMes" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getComedorMes" 
                    TypeName="com.paginar.johnson.BL.ControllerUCHome"></asp:ObjectDataSource>--%>
	        </div>
        </div>
    </div>

</div>

