﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;
using System.Globalization;



namespace com.paginar.johnson.Web.UserControl_Home
{
    //CronogramaFechaByCronSelect
    //CronogramaSelectActivos
    public partial class ucVoluntariadoCronograma : System.Web.UI.UserControl
    {
        ControllerVoluntariado
            ControladorCronogramaSelectActivos = new ControllerVoluntariado();
        ControllerVoluntariado
            ControladorCronogramaFechaPorSelectos = new ControllerVoluntariado();
        CultureInfo myCIintl = new CultureInfo("es-ES",true
            );
        protected int indiceElemento;
        protected string cadenaRetorno = string.Empty;
        string Output = string.Empty;
        string[] Meses = new string[13]; //DateTimeFormatInfo.InvariantInfo.MonthNames;
        
        protected int UltimoMesProcesado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                Meses[0] = "Enero";
                Meses[1] = "Febrero";
                Meses[2] = "Marzo";
                Meses[3] = "Abril";
                Meses[4] = "Mayo";
                Meses[5] = "Junio";
                Meses[6] = "Julio";
                Meses[7] = "Agosto";
                Meses[8] = "Septiembre";
                Meses[9] = "Octubre";
                Meses[10] = "Noviembre";
                Meses[11] = "Diciembre";
                Meses[12] = "";
                Output = "<table class=\"Cronograma\">";
                Output += ConstructorCabecera();
                Output += ConstructorCuerpo();
                Output += "</table>";
               // lblOutput.Text = Output;

            }

        }

        private string ConstructorCuerpo()
        {
            string OutputCuerpo = string.Empty;
            int cantidadElementos;
            DSVoluntariado.vol_CronogramaDataTable DTCronograma = new DSVoluntariado.vol_CronogramaDataTable();
            DTCronograma = ControladorCronogramaSelectActivos.CronogramaSelectActivos();
            foreach (DSVoluntariado.vol_CronogramaRow row in DTCronograma.Rows)
            {
                DSVoluntariado.vol_CronogramaFechaDataTable
                    DTCronogramaFecha = new DSVoluntariado.vol_CronogramaFechaDataTable();
                DTCronogramaFecha = ControladorCronogramaFechaPorSelectos.CronogramaFechaByCronSelect((int.Parse(row.IdCronograma.ToString())));
                if (DTCronogramaFecha.Rows.Count == 0)
                {
                    continue;
                }

                OutputCuerpo += "<tr><td class=\"cronoTitulo\">" + row.Actividad.ToString() + "</td>" ;
                
                cantidadElementos = DTCronogramaFecha.Count;
                int MesMenor = int.Parse(retornaMes(((DSVoluntariado.vol_CronogramaFechaRow)DTCronogramaFecha.Rows[0]).Fecha.ToString()));
                int MesMayor = int.Parse(retornaMes(((DSVoluntariado.vol_CronogramaFechaRow)DTCronogramaFecha.Rows[cantidadElementos - 1]).Fecha.ToString()));
                indiceElemento = 0;
                UltimoMesProcesado = 0;
                cadenaRetorno = string.Empty;
                string ultimoMes = string.Empty;
                foreach (DSVoluntariado.vol_CronogramaFechaRow rowF in DTCronogramaFecha.Rows)
                {
                    bool esTBD = bool.Parse(rowF.TBD.ToString());
                    if (ultimoMes != retornaMes(rowF.Fecha).ToString() || ultimoMes == string.Empty)
                    {
                        OutputCuerpo += procesarTDparaCuerpo(retornaMes(rowF.Fecha), MesMenor, MesMayor, esTBD);
                        ultimoMes = retornaMes(rowF.Fecha);
                    }
                }
                OutputCuerpo += "</tr>"; 
            }

            return OutputCuerpo;
        }

        protected string generarMesesCabecera()
        {
            string mesesCabecera = string.Empty;
            for (int i = 0; i < 13; i++)
            {
                mesesCabecera += "<th>" + Meses[i] + "</th>";
            }
            return mesesCabecera;
        }

        protected string ConstructorCabecera()
        {
            string OutputCabecera = string.Empty;

            OutputCabecera += "<thead>";
            OutputCabecera += "<tr><th>Actividades</th>";
            for (int i = 0; i < 12; i++)
            {
                OutputCabecera += "<th>"+ Meses[i] +"</th>";
                
            }
            OutputCabecera += "</tr></thead>";

            return OutputCabecera;
        }


        protected string procesarTDparaCuerpo(string fecha, int MesMenor, int MesMayor, bool esTBD)
        {
            string TDparaCuerpo = string.Empty;
            int mesNumerico = int.Parse(fecha.ToString());

            if ((MesMenor == MesMayor) && MesMayor == 12)
            {
                for (int i = indiceElemento + 1; i < mesNumerico; i++)
                {
                    TDparaCuerpo += "<td></td>";
                    indiceElemento++;
                }
                TDparaCuerpo += "<td>" + agregarTilde(esTBD) + "</td>";
                return TDparaCuerpo;
            }

            if (MesMenor == 1 && indiceElemento == 0)
            {
                indiceElemento++;
                UltimoMesProcesado = mesNumerico;
                return "<td>" + agregarTilde(esTBD) + "</td>";

            }
            else if (mesNumerico == MesMayor)
            {
                if (MesMayor == 12)
                {
                    return "<td>" + agregarTilde(esTBD) + "</td>";
                }
                else
                {
                    for (int i = indiceElemento + 1; i < mesNumerico; i++)
                    {
                        TDparaCuerpo += "<td></td>";
                        indiceElemento++;
                    }
                    TDparaCuerpo += "<td>" + agregarTilde(esTBD) + "</td>";
                    indiceElemento++;
                    UltimoMesProcesado = mesNumerico;

                    for (int i = MesMayor; i < 12; i++)
                    {
                        TDparaCuerpo += "<td></td>";
                        indiceElemento++;
                    }

                    return TDparaCuerpo;

                }

            }
            else if (indiceElemento < mesNumerico)
            {
                for (int i = indiceElemento + 1; i < mesNumerico; i++)
                {
                    TDparaCuerpo += "<td></td>";
                    indiceElemento++;
                }
                TDparaCuerpo += "<td>" + agregarTilde(esTBD) + "</td>";
                UltimoMesProcesado = mesNumerico;
                indiceElemento++;
                return TDparaCuerpo;
            }
            return TDparaCuerpo;
        }



        protected string agregarTilde(bool esTBD)
        {
            if (esTBD)
            {

                return "<img src=\"/images/tildeNaranja.png\" alt=\"Fecha a Definir\"/>";
            }
            else
            {
                return "<img src=\"/images/tildeAzul.png\" alt=\"Fecha a Estimada\"/>";
            }
        }

        protected string retornaMes(object fecha)
        {
            string FechaCadena = fecha.ToString();
            string[] FechaArreglo = FechaCadena.Split('/');
            return FechaArreglo[1];
        }


    }
}