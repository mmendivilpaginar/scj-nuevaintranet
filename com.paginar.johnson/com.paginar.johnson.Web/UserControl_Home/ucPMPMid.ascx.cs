﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucPMPMid : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int clusteridactual = ((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual;
            UsuariosEvaluacion U = new UsuariosEvaluacion(Page.User.Identity.Name);
            this.Visible = (U.RealizaEvaluacionPeriodoActual(U.legajo, 8) || U.RealizaEvaluacionPeriodoActual(U.legajo, 9)) && (clusteridactual != 3) && (clusteridactual != 4);
           
        }
    }
}