﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucEfemerides : CustomControlUser
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdClusterID.Value = ((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString();
            }
        }
    }
}