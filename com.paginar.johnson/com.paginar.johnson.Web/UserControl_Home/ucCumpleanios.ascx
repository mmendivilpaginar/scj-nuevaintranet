﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCumpleanios.ascx.cs"
    Inherits="com.paginar.johnson.Web.UserControl_Home.ucCumpleanios" %>
<div class="box box-ext">
    <!--[titulo]-->
    <h2>
        Cumpleaños</h2>
    <!--[/titulo]-->
    <!--[contenido]-->
    <div id="cumpleanios">
        <div class="tabs">
            <ul>
                <li><a href="#cumpleanios-t1">Hoy</a></li>
                <li><a href="#cumpleanios-t2">Esta semana</a></li>
            </ul>
            <div id="cumpleanios-t1">
                <asp:DataList ID="dtHoy" runat="server" DataSourceID="odsHoy" RepeatLayout="Flow"
                    OnItemDataBound="dtHoy_ItemDataBound">
                    <ItemTemplate>
                        <asp:Label ID="lblnombreapellido" runat="server" Text='<%# Eval("nombre") +" " + is_externo(Eval("apellido").ToString(), Eval("UsuarioID").ToString()) %>'></asp:Label>
                        <asp:Image ID="imgBandera" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                        <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                        <asp:Panel ID="pnlUsuario" runat="server">
                            <div class="userCard">
                                <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                    <ItemTemplate>
                                        <div class="foto">
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                        </div>
                                        <div class="info">
                                            <ul>
                                                <li>
                                                    <%# Eval("nombre")%>
                                                    <%# is_externo(Eval("apellido").ToString(), Eval("UsuarioID").ToString())%></li>
                                                <li>
                                                    <%# Eval("UBICACIONDET")%>
                                                </li>
                                                <li>
                                                    <%# Eval("DireccionDET")%>
                                                </li>
                                                <li>
                                                    <%# Eval("areadesc")%>
                                                </li>
                                                <%--<li>
                                                    <%# Eval("cargodet")%>
                                                </li>--%>
                                                <li>Interno:
                                                    <%# Eval("interno")%>
                                                </li>
                                            </ul>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                                <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </div>
                        </asp:Panel>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtHoy.Items.Count==0).ToString())%>' Text="No hay cumpleaños en el día de hoy">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsHoy" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getCumpleanioHoy" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                </asp:ObjectDataSource>
            </div>
            <div id="cumpleanios-t2" class="sinBullets">
                <asp:DataList ID="dtMes" runat="server" DataSourceID="odsSemana" RepeatLayout="Flow"
                    OnItemDataBound="dtHoy_ItemDataBound">
                    <ItemTemplate>
                        <%#  Eval("dia", "{0: d}")%>/<%#  Eval("mes", "{0: m}")%>-
                        <asp:Label ID="lblnombreapellido" runat="server" Text='<%# Eval("nombre") +" " +Eval("apellido") %>'></asp:Label>
                        <asp:Image ID="imgBandera" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                        <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                        <asp:Panel ID="pnlUsuario" runat="server">
                            <div class="userCard">
                                <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                    <ItemTemplate>
                                        <div class="foto">
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                        </div>
                                        <div class="info">
                                            <ul>
                                                <li>
                                                    <%# Eval("nombre")%>
                                                    <%# is_externo(Eval("apellido").ToString(), Eval("UsuarioID").ToString())%></li>
                                                <li>
                                                    <%# Eval("UBICACIONDET")%>
                                                </li>
                                                <li>
                                                    <%# Eval("DireccionDET")%>
                                                </li>
                                                <li>
                                                    <%# Eval("areadesc")%>
                                                </li>
                                                <%--<li>
                                                    <%# Eval("cargodet")%>
                                                </li>--%>
                                                <li>Interno:
                                                    <%# Eval("interno")%>
                                                </li>
                                            </ul>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                                <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                            Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </div>
                        </asp:Panel>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtMes.Items.Count==0).ToString())%>' Text="No hay cumpleaños en este mes">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsSemana" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getCumpleanioSemana" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
    <!--[/contenido]-->
</div>
