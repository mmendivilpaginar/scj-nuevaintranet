﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucDiarios : CustomControlUser
    {
        private bool _displaycluster;
        public bool displaycluster
        {
            get { return _displaycluster; }
            set { _displaycluster = value; }
        }

        private int _clusterDefault;
        public int clusterDefault
        {
            get { return _clusterDefault; }
            set { _clusterDefault = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            int aux = 0;

            switch (_clusterDefault)
            {
                case 1: //Arg
                    aux = 0;
                    break;
                case 2: //Chile
                    aux = 2;
                    break;
                case 3: //Uruguay
                    aux = 9;
                    break;
                case 4:
                    aux = 7;
                    break;
                default:
                    aux = 0;
                    break;
            }

            litPaisDefault.Text = "<script type=\"text/javascript\">$('.accordion').accordion({active: " + aux.ToString() + "});</script>";
        }
    }
}