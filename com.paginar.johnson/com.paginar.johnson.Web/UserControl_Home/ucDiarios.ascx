﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucDiarios.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucDiarios" %>

<div class="box box-ext">
    <!--[titulo]-->
    <h2>Diarios</h2>
    <!--[/titulo]-->

    <!--[contenido]-->
    <div class="accordion">
	    <h3><a href="#">Argentina</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.clarin.com.ar" target="_blank">Clar&iacute;n</a></li>
                <li><a href="http://www.lanacion.com.ar" target="_blank">La Naci&oacute;n</a></li>
                <li><a href="http://www.cronista.com" target="_blank">El Cronista</a></li>
                <li><a href="http://www.infobae.com" target="_blank">Infobae</a></li>
                <li><a href="http://www.ambito.com" target="_blank">Ambito Financiero</a></li>
            </ul>
	    </div>
        <h3><a href="#">Brasil</a></h3>
	    <div>
            <ul>
                <li><a href="http://oglobo.globo.com/" target="_blank">O Globo (Río)</a></li>
                <li><a href="http://www.folha.uol.com.br/" target="_blank">Folha de San Pablo (San Pablo)</a>	</li>
                <li><a href="http://www.valoronline.com.br/" target="_blank">Valor Econónco (Económico)</a></li>
                <li><a href="http://www.acritica.com.br" target="_blank">A Crítica (Manaus)</a></li>
                <li><a href="http://www.estadao.com.br/" target="_blank">Estad&atilde;o (San Pablo)</a></li>
                
            </ul>
	    </div>


	    <h3><a href="#">Chile</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.emol.com" target="_blank">El Mercurio</a></li>
                <li><a href="http://www.mercuriovalpo.cl" target="_blank">El Mercurio de Valparaíso</a></li>
                <li><a href="http://www.diariofinanciero.cl" target="_blank">Diario Financiero</a></li>
                <li><a href="http://www.tercera.cl" target="_blank">La Tercera</a></li>
                <li><a href="http://www.lanacion.cl" target="_blank">La Nación</a></li>
            </ul>
	    </div>
        <h3><a href="#">Colombia</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.eltiempo.com " target="_blank">El Tiempo</a></li>
                <li><a href="http://www.elespectador.com " target="_blank">El Espectador</a></li>
                <li><a href="http://www.portafolio.com.co " target="_blank">Portafolio </a></li>
            </ul>
	    </div>
        <h3><a href="#">Costa Rica</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.nacion.com/" target="_blank">Nación</a></li>
            </ul>
	    </div>
        <h3><a href="#">Ecuador</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.eluniverso.com" target="_blank">El Universo</a></li>
                <li><a href="http://www.expreso.ec" target="_blank">Expreso </a></li>
                <li><a href="http://www.elcomercio.com" target="_blank">El Comercio</a></li>
                <li><a href="http://www.hoy.com.ec" target="_blank">Hoy</a></li>
            </ul>
	    </div>
        <h3><a href="#">México</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.exonline.com.mx/home/" target="_blank">Exc&eacute;lsior</a></li>
                <li><a href="http://www.reforma.com" target="_blank">Reforma</a></li>
                <li><a href="http://www.eluniversal.com.mx/" target="_blank">El Universal</a></li>
                <li><a href="http://www.elfinanciero.com.mx/" target="_blank">El Financiero</a></li>
            </ul>
	    </div>
	    <h3><a href="#">Paraguay</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.abc.com.py/"  target="_blank">ABC Color </a></li>
            </ul>
	    </div>
        <h3><a href="#">Perú</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.elcomercio.com.pe" target="_blank">El Comercio</a></li>
                <li><a href="http://www.larepublica.com.pe  " target="_blank">La República</a></li>
                <li><a href="http://peru21.pe" target="_blank">Perú 21 </a></li>
                <li><a href="http://www.expreso.com.pe" target="_blank">Expreso </a></li>
            </ul>
	    </div>
	    <h3><a href="#">Uruguay</a></h3>
	    <div>
            <ul>
                <li><a href="http://www.elpais.com.uy" target="_blank">El País </a></li>
                <li><a href="http://www.observador.com.uy" target="_blank">El Observador</a>	</li>
            </ul>
	    </div>
	    <h3><a href="#">Venezuela</a></h3>
	    <div>
            <ul>
                <li><a href="http://caracas.eluniversal.com/" target="_blank">El Universal</a></li>
                <li><a href="http://www.el-nacional.com/www/site/p_contenido.php" target="_blank">El Nacional</a></li>
                <li><a href="http://www.elsiglo.com.ve/" target="_blank">El Siglo</a></li>
                <li><a href="http://www.el-aragueno.com.ve/" target="_blank">El Aragüeño</a></li>
            </ul>
	    </div>

	 

    </div>
    <!--[/contenido]-->
</div>
<asp:Literal ID="litPaisDefault" runat="server"></asp:Literal>

