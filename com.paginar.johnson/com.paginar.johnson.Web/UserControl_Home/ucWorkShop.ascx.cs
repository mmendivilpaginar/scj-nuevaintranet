﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucWorkShop : System.Web.UI.UserControl
    {
        Notificaciones m = new Notificaciones();


        bool MODO;
        string SMTP;
        string MailToTest;
        string MailRRHH;
        string smtp_host;
        string port;
        string from_dir;
        string pass;

        ControllerWorkShop WSC = new ControllerWorkShop();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GVSelectWorkShop.Rows.Count < 0)
            {
                btnSuscribir.Visible = false;
                lblmsg.Text = "En este momento no hay Cursos Disponibles ingrese en los proximos dias.";
            }

            if(!Page.IsPostBack)
            {
                HFLegajo.Value = ((MasterBase)this.Page.Master).ObjectUsuario.legajo.ToString();
               
            }
            
        }

        protected void btnSuscribir_Click(object sender, EventArgs e)
        {
           DataTable DSWS = WSC.WSDataTable();
           string elbody = string.Empty;

            elbody = "Se informa por medio del presente mail que: <br /><strong>";
            elbody += ((MasterBase)this.Page.Master).ObjectUsuario.apellido + ", ";
            elbody += ((MasterBase)this.Page.Master).ObjectUsuario.nombre + "</strong><br />";
            elbody += "<strong>Legajo Nº: </strong> " + ((MasterBase)this.Page.Master).ObjectUsuario.legajo + "<br />";
            elbody += "<strong>E-mail: </strong>" + ((MasterBase)this.Page.Master).ObjectUsuario.email + "<br />";
            elbody += "Se muestra interesado/a en el/los siguiente/s curso/s <br />";
            elbody += "<ul>";
           foreach (GridViewRow row in GVSelectWorkShop.Rows)
           {
               string a = row.ToString();

               int i = 0;
               if (((CheckBox)row.FindControl("CheckBox1")).Checked == true)
               {
                   HiddenField hf = (HiddenField)row.FindControl("HiddenField1");
                   int Legajo = ((MasterBase)this.Page.Master).ObjectUsuario.legajo;
                   int cluster = ((MasterBase)this.Page.Master).ObjectUsuario.clusterID;
                   WSC.WorkShopInsetInscripcion(int.Parse(hf.Value), Legajo, cluster,"", DateTime.Now, true);
                   // ((((TextBox)row.FindControl("TextBox1")).Text.Length > 0) ? ((TextBox)row.FindControl("TextBox1")).Text : "")
                   elbody += "<li>" + row.Cells[3].Text.ToString();

               }
           }
            elbody += "</ul><br />Muchas Gracias";
            // Produccion
            //Message M = new Message("cdnevar@scj.com, fprimite@scj.com", "Gestión de Talentos - Suscripción Workshop", elbody);

            // Desarrollo
            NotificacionesController n = new NotificacionesController();
            n.EnvioMailWorkshop("cdnevar@scj.com, fprimite@scj.com", "Gestión de Talentos - Suscripción Workshop", elbody);

           GVSelectWorkShop.DataBind();
           msgOk.Visible = true;

        }

        public string cortarCadena(string cadenaCompleta)
        {
            string cadenaCortada = string.Empty;
            if (cadenaCompleta.Length > 12)
            {
                cadenaCortada = cadenaCompleta.Substring(0, 12) + "...";
            }
            else
            {
                cadenaCortada = cadenaCompleta;
            }


            return cadenaCortada;

        }
             
    }
}