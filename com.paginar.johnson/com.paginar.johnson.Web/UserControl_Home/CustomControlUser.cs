﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public class CustomControlUser: System.Web.UI.UserControl
    {

        protected string Identificador
        {
            get
            {
                return this.GetType().BaseType.Name;
            }
        }



        protected  virtual void Page_Init(object sender, EventArgs e)
        {
            Check();
        }

        public void Check()
        {
            MasterBase mb = (MasterBase)this.Page.Master;
            this.Visible = new ControllerContenido().EsVisible(this.Identificador, mb.ObjectUsuario.clusteridactual);
        }


    }
}