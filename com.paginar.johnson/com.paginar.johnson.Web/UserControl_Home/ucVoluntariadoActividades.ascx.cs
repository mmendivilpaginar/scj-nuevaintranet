﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucVoluntariadoActividades : System.Web.UI.UserControl
    {

        Notificaciones m = new Notificaciones();


        string SMTP;
        string MailToTest;
        string MailRRHH;
        string smtp_host;
        string port;
        string from_dir;
        string pass;

        ControllerVoluntariado CV = new ControllerVoluntariado();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (GVDetalleActividad.Rows.Count < 0)
            //{
            //    btnSuscribirse.Visible = false;
            //}

            if (!Page.IsPostBack)
            {
                HFLegajo.Value = ((MasterBase)this.Page.Master).ObjectUsuario.legajo.ToString();
            }


        }

        public string RutaImagen(object Imagen)
        {
            return "/Voluntariado/Thumbnail/" + Imagen.ToString();
        }


        protected string getinformationV2(object Id, object img)
        {
            return string.Format("<a href='javascript:getinformation({0});' onclick='modal()'><img src='{1}' width='100px' height='{2}' ></a>", Id.ToString(), img.ToString(), getHeight(img.ToString()));
        }

        protected string getHeight(string imgPath)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(imgPath));
            int ActualWidth = image.Width;
            int ActualHeight = image.Height;
            image.Dispose();
            string altoImagen = string.Empty;
            return altoImagen = Math.Round(double.Parse(((ActualHeight * 100) / ActualWidth).ToString()), 0).ToString();

        }

        protected void btnSuscribirse_Click(object sender, EventArgs e)
        {
            string elbody = string.Empty;

            elbody = "Se informa por medio del presente mail que: <br /><strong>";
            elbody += ((MasterBase)this.Page.Master).ObjectUsuario.apellido + ", ";
            elbody += ((MasterBase)this.Page.Master).ObjectUsuario.nombre + "</strong><br />";
            elbody += "<strong>Legajo Nº: </strong> " + ((MasterBase)this.Page.Master).ObjectUsuario.legajo + "<br />";
            elbody += "<strong>E-mail: </strong>" + ((MasterBase)this.Page.Master).ObjectUsuario.email + "<br />";
            elbody += "Se muestra interesado/a en el/las siguiente/s actividad/s de Voluntariado<br />";
            elbody += "<ul>";
            //foreach (GridViewRow row in GVDetalleActividad.Rows)
            //{
            //    string a = row.ToString();

            //    int i = 0;
            //    if (((CheckBox)row.FindControl("CheckBox1")).Checked == true)
            //    {
            //        HiddenField hf = (HiddenField)row.FindControl("HiddenField1");
            //        int Legajo = ((MasterBase)this.Page.Master).ObjectUsuario.legajo;
            //        int cluster = ((MasterBase)this.Page.Master).ObjectUsuario.clusterID;
            //        CV.ActividadInscripcion(int.Parse(hf.Value), Legajo, cluster, "", true);
            //        elbody += "<li>" + ((Label)(row.FindControl("Label2"))).Text;

            //    }
            //}
            elbody += "</ul><br />Muchas Gracias";
            NotificacionesController n = new NotificacionesController();
            n.EnvioMailVoluntariado("evaluado@mom", "Voluntariado - Suscripción Actividades", elbody);
            //n.EnvioMailVoluntariado("LMontaru@scj.com", "Voluntariado - Suscripción Actividades", elbody);

            //GVDetalleActividad.DataBind();
            msgOk.Visible = true;

        }

        protected void GVDetalleActividad_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName == "Select")
            //{
            //    ltscroll.Text = "<script>pageScroll();</script>";
            //    //ltscroll.Text = "<script>$(document).ready(function() { pageScroll();  });</script>";
            //}
        }

        protected void FVDetalleActividad_DataBound(object sender, EventArgs e)
        {
           // ltscroll.Text = "<script>pageScroll();</script>";
        }
    }
}