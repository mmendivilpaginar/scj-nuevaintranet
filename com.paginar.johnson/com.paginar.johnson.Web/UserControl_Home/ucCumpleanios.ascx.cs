﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucCumpleanios : CustomControlUser
    {

        private bool _displaycluster;
        public bool displaycluster
        {
            get { return _displaycluster; }
            set { _displaycluster = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }


   
        protected void dtHoy_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                FormView frmUsuario = (FormView)e.Item.FindControl("frmUsuario");
                Label lblnombreapellido = (Label)e.Item.FindControl("lblnombreapellido");
                Panel pnl = (Panel)e.Item.FindControl("pnlUsuario");
                pnl.Attributes.Add("style", "display:none");
                if (frmUsuario.PageCount > 0)
                {
                    Literal ltjs = (Literal)e.Item.FindControl("ltjs");
                    ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
                }
            }

        }

        protected string is_externo(string apellido, string usuarioid)
        {

            if (usuarioid == "12060" || usuarioid == "12097" || usuarioid == "12197" || usuarioid == "12221" || usuarioid == "13250")
            {
                return apellido + " (En el Exterior)";
            }
            return apellido;
        }

        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

    }
}