﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Configuration;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucPDA : System.Web.UI.UserControl
    {
        Notificaciones m = new Notificaciones();

        bool MODO;
        string SMTP;
        string MailToTest;
        string MailRRHH;
        string smtp_host;
        string port;
        string from_dir;
        string pass;

        ControllerPDA PDA = new ControllerPDA();
        ControllerUsuarios CU = new ControllerUsuarios();

        public Usuario ObjectUsuario
        {
            get
            {
                if (Session["Usuario"] == null)
                {
                    if (Page.User.Identity.IsAuthenticated)
                    {
                        Usuario u = new Usuario();
                        ControllerSeguridad CS = new ControllerSeguridad();
                        u = CS.GetDatosUsuarioByUserName(Page.User.Identity.Name);
                        Session["Usuario"] = u;
                    }
                }
                return Session["Usuario"] as Usuario;
            }
        }


        protected void cargarScript()
        {
            DSPda.pda_cursoDataTable CursoUsuario =  PDA.pda_cursoSelectInscriptoByUsuario(this.ObjectUsuario.usuarioid);
            int cursos = CursoUsuario.Rows.Count;
            if (cursos >= 5)
            {
                HFmisCursosCant.Value = cursos.ToString();
            }
        }

        protected string getModuloLabel(object o)
        {
            return "Módulo " + o.ToString();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            pnlBad.Visible = false;
            pnlOk.Visible = false;
            okConfirm.Visible = false;


            if (!Page.IsPostBack)
            {
                if (permisos(this.ObjectUsuario.usuarioid))
                {
                    pnlOk.Visible = true;
                    if (RPTcursos.Items.Count < 0)
                    {
                        btnSuscribir.Visible = false;
                        lblmsg.Text = "En este momento no hay Cursos Disponibles ingrese en los proximos dias.";
                    }
                }
                else
                {
                    pnlBad.Visible = true;
                }

                cargarScript();
                cargarDatosUsuario();                
                HFUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();
            }

            if (permisos(this.ObjectUsuario.usuarioid))
            {
                pnlOk.Visible = true;
            }

        }

        private void cargarDatosUsuario()
        {
            lblApyNom.Text = "<span class='referencepda'>Apellido y Nombre: </span>" + this.ObjectUsuario.apellido + ", " + this.ObjectUsuario.nombre;
            lblLegajo.Text = "<span class='referencepda'>Legajo: </span>" + this.ObjectUsuario.legajo.ToString();
            lblMail.Text = "<span class='referencepda'>E-mail: </span>" + this.ObjectUsuario.email;
            lblSector.Text = "<span class='referencepda'>Sector: </span>" + ((DSUsuarios.UsuarioDataTable)CU.GetUsuarioByUsuarioID(int.Parse(this.ObjectUsuario.usuarioid.ToString()))).Rows[0].ItemArray[33];
        }

        private bool permisos(int p)
        {
            bool valorRetorno = true;

            DSUsuarios.UsuarioDataTable UDT = new DSUsuarios.UsuarioDataTable();
            UDT = CU.GetUsuarioByUsuarioID(p);
            string ingresoStr = UDT.Rows[0].ItemArray[7].ToString();

            DateTime ingreso = DateTime.Parse(ingresoStr);
            DateTime hoy = DateTime.Now;

            // Difference in days, hours, and minutes.
            TimeSpan ts = hoy - ingreso;

            // Difference in days.
            int diasdesdequeingreso = ts.Days;

            string mensaje = string.Empty;

            if (PDA.pda_cursoSelectInscriptoByUsuario(p).Rows.Count > 0)
            {
                mensaje = "Usted ya se inscribió a los cursos PDA.";
                valorRetorno = false;            
            }

            if (diasdesdequeingreso < 330)
            {
                mensaje = "Debe tener al menos un año de antiguedad.";
                valorRetorno = false;
            }
            //
            string categoria = CU.DameEscalaSalarial(p);
            if (!categoria.StartsWith("A"))
            {
                if (mensaje.Length > 0)
                    mensaje += "<br />";

                mensaje += "Estos cursos no estan disponibles para su categoría.";
                valorRetorno = false;
            }

            if (!valorRetorno)
            {
                lblmsg.Text = "<div class=\"messages msg-info\" ><span lang=\"ES-TRAD\">" + mensaje + "<span lang=\"ES-TRAD\"></div>";
            }
            


            

            return valorRetorno;
        }

        protected void btnSuscribir_Click(object sender, EventArgs e)
        {
            DataTable DSWS = PDA.PDAcursoDT();
            string elbody = string.Empty;

            elbody = "Se informa por medio del presente mail que: <br /><strong>";
            elbody += this.ObjectUsuario.apellido + ", ";
            elbody += this.ObjectUsuario.nombre + "</strong><br />";
            elbody += "<strong>Legajo Nº: </strong> " + this.ObjectUsuario.legajo + "<br />";
            elbody += "<strong>E-mail: </strong>" + this.ObjectUsuario.email + "<br />";
            elbody += "Se muestra interesado/a en el/los siguiente/s curso/s <br />";
            elbody += "<ul>";
            foreach (RepeaterItem item in RPTcursos.Items)
            {
                //string a = row.ToString();

                int i = 0;
                if (((CheckBox)item.FindControl("CheckBox1")).Checked == true)
                {
                    HiddenField hf = (HiddenField)item.FindControl("HiddenField1");
                    int Legajo = this.ObjectUsuario.legajo;
                    int idUsuario = this.ObjectUsuario.usuarioid;
                    int cluster = this.ObjectUsuario.clusterID;
                    if (int.Parse(hf.Value) == 1)
                    {
                        PDA.pda_InscriptoInsert(int.Parse(hf.Value), idUsuario, Legajo, cluster, tbComentario.Text.Trim(), DateTime.Now, true);
                    }
                    else
                    {
                        PDA.pda_InscriptoInsert(int.Parse(hf.Value), idUsuario, Legajo, cluster, "", DateTime.Now, true);
                    }

                    DSPda.pda_cursoDataTable CDRT = PDA.pda_cursoSelectByID(int.Parse(hf.Value));
                    elbody += "<li>" + CDRT.Rows[0].ItemArray[1].ToString() + "</li>";

                }
            }
            elbody += "</ul><br />Muchas Gracias";

            NotificacionesController n = new NotificacionesController();

                try
                {
                    n.EnvioMailWorkshop(this.ObjectUsuario.email, "Gestión de Talentos - Suscripción PDA", elbody);
                }
                catch (Exception)
                {

                    lblmsg.Text = "<div id=\"msgErr11\" class=\"messages msg-error\" runat=\"server\" visible=\"false\"><span lang=\"ES-TRAD\">No se pudo enviar el mail, posiblemente su cuenta no este configurada en su perfil.</span></div>";
                    
                }

                okConfirm.Visible = true;
                pnlBad.Visible = false;
                pnlOk.Visible = false;
                
            
            
            
        }

        public string cortarCadena(string cadenaCompleta)
        {
            string cadenaCortada = string.Empty;
            if (cadenaCompleta.Length > 12)
            {
                cadenaCortada = cadenaCompleta.Substring(0, 12) + "...";
            }
            else
            {
                cadenaCortada = cadenaCompleta;
            }


            return cadenaCortada;

        }


        protected string procesarCadenaMesAno(string p)
        {
            string valorRetorno = string.Empty;
            string[] arrayDate = p.Split('/');
            valorRetorno = arrayDate[1].ToString() + "/" + arrayDate[2].ToString().Substring(0, 4);
            return valorRetorno;
        }


        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            Response.Redirect("~/recursos_humanos/cursoPDA.aspx");
        }

        protected string dameEstado(object e)
        {
            string valorRetorno = string.Empty;
            switch (e.ToString())
            {
                case "1":
                    valorRetorno = "Pendiente";
                    break;
                case "2":
                    valorRetorno = "Abierto";
                    break;
                case "3":
                    valorRetorno = "Cerrado";
                    break;

            }

            return valorRetorno;
        }
        protected string dameEnableEstadoStr(object estado, object id)
        {

            string valorRetorno = string.Empty;

            if (id.ToString() == "1" || id.ToString() == "9")
                valorRetorno = "<div class=\"item-modulo disabled selected obligatory\">";
            else
                valorRetorno = "<div class=\"item-modulo disabled\">";
             
            switch (estado.ToString())
            {

                case "2":
                    if (id.ToString() == "1" || id.ToString() == "9")
                        valorRetorno = "<div class=\"item-modulo disabled selected 	obligatory\">";
                    else
                        valorRetorno = "<div class=\"item-modulo\">";
                    break;


            }

            return valorRetorno;


        }

        protected bool dameEnableEstado(object estado)
        {
            bool valorRetorno = false;
            switch (estado.ToString())
            {
                case "1":
                    valorRetorno = false;
                    break;
                case "2":
                    valorRetorno = true;
                    break;
                case "3":
                    valorRetorno = false;
                    break;

            }

            return valorRetorno;        
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/recursos_humanos/cursoPDA.aspx");
        }

        protected void RPTcursos_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }



        protected void LinkButton1_DataBinding(object sender, EventArgs e)
        {
            string id = ((LinkButton)sender).ClientID;
            ((LinkButton)sender).Attributes.Add("onmouseover", "getinformation($(this).attr('rel'))");
            ((LinkButton)sender).Attributes.Add("onmouseout", "cerrarModal()");
                
        }

        protected void RPTcursos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int a = 0;

            if(int.Parse(((HiddenField)((Repeater)sender).Controls[((Repeater)sender).Controls.Count-1].FindControl("HiddenField1")).Value.ToString()) == 1 || int.Parse(((HiddenField)((Repeater)sender).Controls[((Repeater)sender).Controls.Count-1].FindControl("HiddenField1")).Value.ToString()) == 9)    
            {
                ((CheckBox)((Repeater)sender).Controls[((Repeater)sender).Controls.Count-1].FindControl("CheckBox1")).Checked = true;
                ((CheckBox)((Repeater)sender).Controls[((Repeater)sender).Controls.Count-1].FindControl("CheckBox1")).Enabled = false;
            }
                       
        }

    }
}