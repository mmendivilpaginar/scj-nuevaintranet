﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucCombi : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int cluster = ((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual;
            if (cluster != 1)
            {
                this.Visible = false;
            }
            else
            {
                this.Visible = true;
            }


        }
    }
}