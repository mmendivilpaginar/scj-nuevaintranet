﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNoticias.ascx.cs"
    Inherits="com.paginar.johnson.Web.UserControl_Home.ucNoticias" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<asp:HiddenField ID="hdClusterid" runat="server" Value="1" />
<asp:HiddenField ID="CategoriaIDHiddenField" runat="server" />
    <script type="text/javascript">
        $(function () {
            $('a.load-votantes').cluetip({
                local: true,
                hideLocal: true,
                sticky: true,
                arrows: true,
                cursor: 'pointer',
                width: 300,
                mouseOutClose: true

            });

        });

        function flyer(path) {

            if (window.screen) {
                w = window.screen.availWidth;
                h = window.screen.availHeight;
            }
            winpopup = window.open("../Flyer.aspx?path=" + path, "flyer", "'menubar=no,resizable=yes,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + w + ",height=" + h + "'");
        }

    </script>
<asp:Literal ID="litDivHeaderOpen" runat="server" Visible="false">

<div id="noticias" class="box">
    <h2>
        Noticias</h2>

</asp:Literal>
<div style='<%= Visibility("Navigation") %>'>
    <table width="100%" border="0">
        <tr>
            <td align="right">
                <asp:Label ID="lblCurrentPageTop" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:LinkButton ID="lbtnFirstTop" runat="server" Text=" Primera" 
                    OnClick="lbtnFirst_Click"></asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lbtnPrevTop" runat="server" Text=" Anterior " OnClick="lbtnPrev_Click"></asp:LinkButton>&nbsp;
                &nbsp;
                <asp:LinkButton ID="lbtnNextTop" runat="server" Text=" Siguiente " OnClick="lbtnNext_Click"></asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lbtnLastTop" runat="server" Text=" Última " 
                    OnClick="lbtnLast_Click"></asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
<asp:DataList ID="RNoticias" runat="server" OnItemDataBound="RNoticias_ItemDataBound"
    OnItemCommand="RNoticias_ItemCommand" DataKeyField="InfoID" RepeatLayout="Flow">
    <ItemTemplate>
        <div class="noticia">
            <ul class="tags">
                <li><span>
                    <%# Eval("CategoriaDescrip")%></span></li>
                <li runat="server" id="RecordatorioItem" class="recordatorio">
                    <asp:Label ID="RecordatorioLiteral" Text="Recordatorio" runat="server"></asp:Label></li>
            </ul>
            <h3>
                <%--<asp:Image ID="RecordatorioImagen" runat="server" AlternateText="Recordatorio" ImageUrl="~/css/images/ico-recordatorio.jpg"  />--%>
                <asp:HyperLink ID="hplTitulo" runat="server" Text='<%# Eval("Titulo")%>' NavigateUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX") %>'></asp:HyperLink>
                <asp:HiddenField ID="RecordatorioHiddenField" runat="server" Value='<%# Eval("Recordatorio") %>' />
            </h3>
            <div class="nota">
                <p>
                    <div class="ImgDesHome" runat="server" id="DivImagen">
                         <a href="#"  runat="server" ID="lnkImg"  target="_blank"> 

                         <asp:Image ID="ImagenNoticia" runat="server" />

                        </a>
                        <br />
                        <b>
                            <%--<asp:Label ID="ImagenNoticiaDescripcion" runat="server" Text=""></asp:Label>--%>

                        </b>
                         <br />
                    </div>
                    <div runat="server" id="divNoFlyer">
                        <i>
                            <asp:Label Font-Italic="true" runat="server" ID="LabelFechaAsoc" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                        </i>-
                        <%# TruncarString(StripTagsCharArray(Eval("texto").ToString()), 250)%>
                        <ul>
                            <asp:Repeater ID="RepeaterArchivos" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <%--<img src="../css/images/attach.png" title="Ver Adjunto" border="0" />--%>
                                        <span onclick='<%# "flyer('~/noticias/Imagenes/"+Eval("Path") %>'>'
                                            <asp:HyperLink ID="HyperLinkArchivoAdjunto" Text='<%# Eval("Descrip") %>' runat="server"
                                                NavigateUrl='<%# "~/noticias/Imagenes/"+Eval("Path") %>' Target="_blank" Visible='<%# Eval("Path").ToString()!="" %>'>  ss</asp:HyperLink>
                                        </span></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                    <div runat="server" id="divFlyer" visible="false">
                     <i>
                            <asp:Label Font-Italic="true" runat="server" ID="Label1" Text='<%# Eval("FechaAsoc","{0:dd/MM/yyyy}").ToString()%>'></asp:Label>
                        </i> <br /><br />
                           <asp:Label Font-Italic="true" runat="server" ID="LiteralCopete" Text='<%#Eval("Copete")%>'></asp:Label>
                    <%# Eval("Copete").ToString()!=""?"<p></p>":""%>
                         <br />
                         <a href="#"  runat="server" ID="lnkImgflyer"  target="_blank"> 
                                <asp:Image ID="imgFlyer" runat="server" Width="450px" />
                             </a>
                        <br />
                    </div>
              
                </p>
                <div id="divVideo"  align="center">
                   <asp:Literal ID="ltvideo" runat="server"></asp:Literal>
                </div>
                <br />  <br />
                <div id="divlinks"  style="margin-left:20px">

                    <asp:DataList ID="RTLinks" runat="server" OnItemDataBound="RTLinks_ItemDataBound">
                        <ItemTemplate>
                            <br />
                            <asp:Literal ID="ltvideo" runat="server"></asp:Literal>
                            <asp:HyperLink Target="_blank" ID="HyperLink1" NavigateUrl='<%# Eval("url")%>' Text='<%# Eval("descrip")%>' runat="server" />

                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
            <br />
            <ul class="action-list">
                <li>
                    <asp:Label ID="lblConComents" runat="server"></asp:Label>
                    <asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                        ToolTip="Ver Comentarios" PostBackUrl='<%#  "/noticias/noticia.aspx?infoID=" +Eval("InfoIDAUX")+"#comentarios-abajo" %>' />
                </li>
              <%--  <li class="unlike">--%>
                    <asp:UpdatePanel ID="updUnLike" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                         <%--   <asp:Label ID="lblUnLikeCount" runat="server"></asp:Label>
                            <asp:ImageButton ID="imgUnLike" runat="server" ImageUrl="~/css/images/ico-unlike.gif"
                                CommandName="unlike" CommandArgument='<%# Bind("InfoIDAUX") %>' />--%>
                        
                        </ContentTemplate>
                    </asp:UpdatePanel>
              <%--  </li>--%>
                <li class="like">
                    <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional" 
                        ondatabinding="updLike_DataBinding">
                        <ContentTemplate>
                        <div id="votantes"  runat="server"  style="display:none; height:auto; width:auto !important;" class="userCard">

                                <asp:HiddenField ID="HFInfoIDToVotos" runat="server" 
                                    Value='<%# Eval("InfoIDAUX") %>' />
                                <asp:GridView ID="GVVotantes" runat="server" AutoGenerateColumns="False" 
                                    DataKeyNames="UsuarioID" DataSourceID="ODSUsuariosVotantesInfo" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblvotHeader" runat="server" Text="Esta Noticia fue votada por:" CssClass="izquierda"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/images/personas/"+Eval("usuariofoto")%>' />                               
                                                </div>
                                                <div class="informacion">
                                                <b><%# Eval("Apellido") + " " + Eval("Nombre") %></b>
                                                <br />
                                                <%# Eval("ubicaciondescripcion")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="lblEmpty" runat="server" Text="Esta Noticia aún no fue votada"></asp:Label>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:ObjectDataSource ID="ODSUsuariosVotantesInfo" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="getUsuariosVotantesByInfoID" 
                                TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="HFInfoIDToVotos" Name="infoID" 
                                            PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                            </asp:ObjectDataSource>
<%--                                <table border="1">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblcontent" runat="server" Text='<%# Eval("InfoIDAUX")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>--%>
                            </div> 
                           <a class="load-votantes" runat="server" id="eltooltip"><asp:Label ID="lblLikeCount" runat="server"></asp:Label></a>
                           
                            <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif"
                                CommandName="like" CommandArgument='<%# Bind("InfoIDAUX") %>' />
                                
                                    <%-- [Ver más]<asp:Image ID="imgVerMas" runat="server" ImageUrl="~/images/verMas.png" Height="16px" Width="16" />--%> 

                          
                        <!-- -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </li>
            </ul>
            <asp:UpdateProgress ID="upgNoticiasLike" runat="server" AssociatedUpdatePanelID="updLike">
                <ProgressTemplate>
                    <img src="../images/preloader.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdateProgress ID="upgNoticiasUnLike" runat="server" AssociatedUpdatePanelID="updUnLike">
                <ProgressTemplate>
                    <img src="../images/preloader.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:HiddenField ID="InfoIDHiddenField" runat="server" Value='<%# Eval("InfoIDAUX")%>' />
            <asp:SlideShowExtender SlideShowServicePath="~/WsNoticias.asmx" ID="ImagenNoticia_SlideShowExtender"
                runat="server" SlideShowServiceMethod="GetSlides" TargetControlID="ImagenNoticia"
                UseContextKey="True" NextButtonID="" PlayButtonID="" PlayButtonText="play" StopButtonText="stop"
                PreviousButtonID="" AutoPlay="true" Loop="True" PlayInterval="10000" ImageDescriptionLabelID="ImagenNoticiaDescripcion">
            </asp:SlideShowExtender>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        <asp:Label ID="lblEmpty" CssClass="messages msg-info" Text="No se encontraron Resultados"
            runat="server" Visible='<%#bool.Parse((RNoticias.Items.Count==0).ToString())%>'>

        </asp:Label>
    </FooterTemplate>
</asp:DataList>
<div style='<%= Visibility("Navigation") %>'>
    <table width="100%" border="0">
        <tr>
            <td align="right">
                <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:LinkButton ID="lbtnFirst" runat="server" Text=" Primera" 
                    OnClick="lbtnFirst_Click"></asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lbtnPrev" runat="server" Text=" Anterior " OnClick="lbtnPrev_Click"></asp:LinkButton>&nbsp;
                &nbsp;
                <asp:LinkButton ID="lbtnNext" runat="server" Text=" Siguiente " OnClick="lbtnNext_Click"></asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lbtnLast" runat="server" Text=" Última " 
                    OnClick="lbtnLast_Click"></asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
<asp:Literal ID="litDivHeaderClose" runat="server" Visible="false">
</div>
</asp:Literal>
