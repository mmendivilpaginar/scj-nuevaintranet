﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucWorkShop.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucWorkShop" %>
<asp:HiddenField ID="HFLegajo" runat="server" />
<script language="javascript" type="text/javascript">

    $().ready(function () {


        $("#<%=btnSuscribir.ClientID%>").click(function () {

            var seleccionado = false;
            var contador = 0;
            var indice = 1;
            var cursos;
            cursos = "";

            $("#<%=GVSelectWorkShop.ClientID%> :checkbox").each(function () {

                if (this.checked) {
                    seleccionado = true;
                    cursos += "- " + $("#<%=GVSelectWorkShop.ClientID%>").find("tr").eq(indice).find("td").eq(2).html() + "\n";

                    contador++;
                }
                indice++;
            });
            if (contador > 0) {
                if (!confirm("Tu participación en este/os taller/es:\n" + cursos + "\n Será confirmada una vez evaluado tu nivel de inglés. Recursos Humanos se estará poniendo en contacto con vos. \n Muchas gracias \n ¿Desea continuar?")) {
                    return false;
                }
            } else {
                alert("Debe seleccionar al menos un curso");
                return false;
            }


        })


    });

</script>
<h2>Talleres de Inglés</h2>
            <div id="msgOk" class="messages msg-exito" runat="server" visible="false">
                <span lang="ES-TRAD">La operación fue realizada exitosamente</span>
            </div>
<asp:GridView ID="GVSelectWorkShop" runat="server" AutoGenerateColumns="False" 
    DataKeyNames="IdWorkshop" DataSourceID="ODSGVSelectWorkShop" 
    CssClass="workshop" style="margin-right: 1px" Width="598px" 
    AllowPaging="True" PageSize="6">
    <Columns>
        <asp:CommandField SelectText="Ver más..." ShowSelectButton="True" />
        <asp:TemplateField>
            <EditItemTemplate>
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </EditItemTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="CheckBox1" runat="server" />
                <asp:HiddenField ID="HiddenField1" runat="server" 
                    Value='<%# Eval("IdWorkshop") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Id" InsertVisible="False" 
            SortExpression="IdWorkshop" ShowHeader="False" Visible="False">
            <EditItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("IdWorkshop") %>'></asp:Label>
            </EditItemTemplate>
            <ItemTemplate>
            
                <asp:Label ID="Label2" runat="server" Text='<%# Bind("IdWorkshop") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
            SortExpression="Nombre" />
        <asp:TemplateField HeaderText="Comentario" Visible="False">
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        En este momento no hay cursos disponibles.
    </EmptyDataTemplate>
</asp:GridView>


<asp:ObjectDataSource ID="ODSGVSelectWorkShop" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="WorkshopDisponiblesPorUsuario" 
    TypeName="com.paginar.johnson.BL.ControllerWorkShop">
    <SelectParameters>
        <asp:ControlParameter ControlID="HFLegajo" DefaultValue="" Name="Legajo" 
            PropertyName="Value" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:Button ID="btnSuscribir" runat="server" onclick="btnSuscribir_Click" 
    Text="Suscribir" />

<asp:Label ID="lblmsg" runat="server"></asp:Label>

<br />
<br /><br />
<asp:FormView ID="FormView1" runat="server" DataKeyNames="IdWorkshop" 
    DataSourceID="ODSFVDetalesWorkshop">
    <EditItemTemplate>
        IdWorkshop:
        <asp:Label ID="IdWorkshopLabel1" runat="server" 
            Text='<%# Eval("IdWorkshop") %>' />
        <br />
        Nombre:
        <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
        <br />
        Objetivo:
        <asp:TextBox ID="ObjetivoTextBox" runat="server" 
            Text='<%# Bind("Objetivo") %>' />
        <br />
        Metodologia:
        <asp:TextBox ID="MetodologiaTextBox" runat="server" 
            Text='<%# Bind("Metodologia") %>' />
        <br />
        Requisitos:
        <asp:TextBox ID="RequisitosTextBox" runat="server" 
            Text='<%# Bind("Requisitos") %>' />
        <br />
        Duracion:
        <asp:TextBox ID="DuracionTextBox" runat="server" 
            Text='<%# Bind("Duracion") %>' />
        <br />
        FechaInicioWorkshop:
        <asp:TextBox ID="FechaInicioWorkshopTextBox" runat="server" 
            Text='<%# Bind("FechaInicioWorkshop","{0:dd/MM/yyyy}") %>' />
        <br />
        IdEstado:
        <asp:TextBox ID="IdEstadoTextBox" runat="server" 
            Text='<%# Bind("IdEstado") %>' />
        <br />
        PeriodoInscripcionInicio:
        <asp:TextBox ID="PeriodoInscripcionInicioTextBox" runat="server" 
            Text='<%# Bind("PeriodoInscripcionInicio") %>' />
        <br />
        PeriodoInscripcionFin:
        <asp:TextBox ID="PeriodoInscripcionFinTextBox" runat="server" 
            Text='<%# Bind("PeriodoInscripcionFin") %>' />
        <br />
        <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
            CommandName="Update" Text="Update" />
        &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
            CausesValidation="False" CommandName="Cancel" Text="Cancel" />
    </EditItemTemplate>

    <InsertItemTemplate>
        Nombre:
        <asp:TextBox ID="NombreTextBox" runat="server" Text='<%# Bind("Nombre") %>' />
        <br />
        Objetivo:
        <asp:TextBox ID="ObjetivoTextBox" runat="server" 
            Text='<%# Bind("Objetivo") %>' />
        <br />
        Metodologia:
        <asp:TextBox ID="MetodologiaTextBox" runat="server" 
            Text='<%# Bind("Metodologia") %>' />
        <br />
        Requisitos:
        <asp:TextBox ID="RequisitosTextBox" runat="server" 
            Text='<%# Bind("Requisitos") %>' />
        <br />
        Duracion:
        <asp:TextBox ID="DuracionTextBox" runat="server" 
            Text='<%# Bind("Duracion") %>' />
        <br />
        FechaInicioWorkshop:
        <asp:TextBox ID="FechaInicioWorkshopTextBox" runat="server" 
            Text='<%# Bind("FechaInicioWorkshop") %>' />
        <br />
        IdEstado:
        <asp:TextBox ID="IdEstadoTextBox" runat="server" 
            Text='<%# Bind("IdEstado") %>' />
        <br />
        PeriodoInscripcionInicio:
        <asp:TextBox ID="PeriodoInscripcionInicioTextBox" runat="server" 
            Text='<%# Bind("PeriodoInscripcionInicio") %>' />
        <br />
        PeriodoInscripcionFin:
        <asp:TextBox ID="PeriodoInscripcionFinTextBox" runat="server" 
            Text='<%# Bind("PeriodoInscripcionFin") %>' />
        <br />
        <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
            CommandName="Insert" Text="Insert" />
        &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
            CausesValidation="False" CommandName="Cancel" Text="Cancel" />
    </InsertItemTemplate>
    <ItemTemplate>
        <br />
        <strong>Nombre:</strong><br />
        <asp:Label ID="NombreLabel" runat="server" Text='<%# Bind("Nombre") %>' />
        <br />
        <strong>
        <br />
        Objetivo:</strong><br />
        <asp:Label ID="ObjetivoLabel" runat="server" Text='<%# Bind("Objetivo") %>' />
        <br />
        <strong>
        <br />
        Metodología:</strong><br />
        <asp:Label ID="MetodologiaLabel" runat="server" 
            Text='<%# Bind("Metodologia") %>' />
        <br />
        <strong>
        <br />
        Requisitos:</strong><br />
        <asp:Label ID="RequisitosLabel" runat="server" 
            Text='<%# Bind("Requisitos") %>' />
        <br />
        <strong>
        <br />
        Duración:</strong><br />
        <asp:Label ID="DuracionLabel" runat="server" Text='<%# Bind("Duracion") %>' />
        <br />
        <strong>
        <br />
        Período de Inicio:</strong><br /><strong>
        <asp:Label ID="FechaInicioWorkshopLabel" runat="server" 
            Text='<%# Bind("FechaInicioWorkshop","{0:dd/MM/yyyy}") %>' 
            Visible="True" />
            </strong>
        <br />


    </ItemTemplate>
</asp:FormView>
<asp:ObjectDataSource ID="ODSFVDetalesWorkshop" runat="server" 
    OldValuesParameterFormatString="original_{0}" 
    SelectMethod="WorkshopGetSpecific" 
    TypeName="com.paginar.johnson.BL.ControllerWorkShop">
    <SelectParameters>
        <asp:ControlParameter ControlID="GVSelectWorkShop" Name="IdWorkshop" 
            PropertyName="SelectedValue" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>



