﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSantos.ascx.cs" Inherits="com.paginar.johnson.Web.UserControl_Home.ucSantos" %>
<div class="box box-ext">
    <!--[titulo]-->
    <h2>
        Santos</h2>
    <!--[/titulo]-->
    <!--[contenido]-->
    <div id="santos">
        <div class="tabs">
            <ul>
                <li><a href="#santos-t1">Hoy</a></li>
                <li><a href="#santos-t2">Del mes</a></li>
            </ul>
            <div id="santos-t1">
                <asp:DataList ID="dtHoy" CssClass="default-list" runat="server" DataSourceID="odsHoy"
                    RepeatLayout="Flow">
                    <ItemTemplate>
                        <asp:Literal ID="ltsanto" runat="server" Text='<%# Bind("descripcion") %>'></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtHoy.Items.Count==0).ToString())%>'
                            Text="No hay santos en el día de hoy">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsHoy" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getSantosHoy" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                </asp:ObjectDataSource>
            </div>
            <div id="santos-t2" class="sinBullets">
                <asp:DataList ID="dtMes" CssClass="default-list" runat="server" DataSourceID="odsMes"
                    RepeatLayout="Flow">
                    <ItemTemplate>
                        <asp:Literal ID="ltDia" runat="server" Text='<%# Eval("dia","{0: d}") %>'></asp:Literal>
                        -
                        <asp:Literal ID="ltsanto" runat="server" Text='<%# Bind("descripcion") %>'></asp:Literal>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((dtMes.Items.Count==0).ToString())%>'
                            Text="No hay santos en este mes">
                        </asp:Label>
                    </FooterTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="odsMes" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getSantosMes" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
    <!--[/contenido]-->
</div>
