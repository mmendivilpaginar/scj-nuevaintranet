﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;

namespace com.paginar.johnson.Web.UserControl_Home
{
    /// <summary>
    /// Summary description for WebServiceEvaluaciones
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class wsEstadisticas : System.Web.Services.WebService
    {

        public wsEstadisticas()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }


        [WebMethod]
        public void GuardarEstadistica(int UsuarioID, string Pagina, string Titulo)
        {
            ControllerEstadisticas E = new ControllerEstadisticas();
            E.Repositorio.AdapterDSEstadisticas.Estadisticas.SetNewRegisterEstadistic(UsuarioID,Pagina, Titulo,"Banner");        

        }
    }
}
