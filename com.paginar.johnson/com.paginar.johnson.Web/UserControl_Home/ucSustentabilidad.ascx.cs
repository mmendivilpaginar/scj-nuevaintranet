﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucSustentabilidad : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString() == "1"))
                ImageSustentabilidad.Visible = true;
            else
                ImageSustentabilidad.Visible = false;

        }
    }
}