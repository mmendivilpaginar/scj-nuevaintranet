﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucComedor : CustomControlUser
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdClusterID.Value = ((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString(); 
            }
        }

        protected void dtHoy_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Label lbl = (Label)e.Item.FindControl("lbldescripcion");
            Panel pnl = (Panel)e.Item.FindControl("pnlComedor");
            pnl.Attributes.Add("style", "display:none");
            Literal ltjs = (Literal)e.Item.FindControl("ltjs");
            ltjs.Text = GetToolTipFunction(lbl.ClientID, pnl.ClientID);
        }



        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

    }
}