﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.UserControl_Home
{
    public partial class ucLogoEnjoyDiference : CustomControlUser
    {
        private bool _displaycluster;
        public bool displaycluster
        {
            get { return _displaycluster; }
            set { _displaycluster = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //TimeSpan ts = new TimeSpan(1,0, 0);
            //DateTime diaD = new DateTime(2012, 09, 21);                       
            //diaD = diaD + ts;
            //DateTime diaHoy = new DateTime();
            //diaHoy = DateTime.Now;
            //if (diaHoy.CompareTo(diaD) > 0)
            //{

                if((((MasterBase)this.Page.Master).ObjectUsuario.clusteridactual.ToString()=="1"))
                   ImageButtonEnjoyDiference.Visible = true;
            else
                    ImageButtonEnjoyDiference.Visible = false;

        }
    }
}