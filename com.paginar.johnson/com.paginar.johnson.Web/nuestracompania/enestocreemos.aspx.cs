﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class enestocreemos : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TreeViewEnEstoCreemos.Attributes.Add("onclick", "pageScroll()");
                ControllerContenido cc = new ControllerContenido();
                RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("En Esto Creemos", this.ObjectUsuario.clusteridactual).ToString();
     
            }
        }

        protected void TreeViewEnEstoCreemos_SelectedNodeChanged(object sender, EventArgs e)
        {
            HiddenFieldItemID.Value = TreeViewEnEstoCreemos.SelectedNode.DataPath;            
        }

    }
}