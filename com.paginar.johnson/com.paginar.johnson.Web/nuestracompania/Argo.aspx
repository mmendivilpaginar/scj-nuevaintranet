﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/MasterPages/OneSidebarLeft.master"
CodeBehind="Argo.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.Argo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia" TagPrefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<h2>Argo</h2>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="Argo" ID="PanelEstrategias" runat="server">
            <ContentTemplate>
                <div id="encabezado" class="DivEncabezadoARgo">
                    <div id="imagen" class="leftHalfEstrategias">
                        <img src="../images/argologo.png" alt="Argo" width="200px" />
                    </div>
                    <div id="bienvenida" class="rightHalfEstrategias">

                        <p>
                            Tenemos un desafío y una gran oportunidad en nuestras manos. El desafío: ¿Cómo hacemos
                            para llegar a la rentabilidad definida en el presupuesto (Corplan)? Y la Oportunidad:
                            ¿Cómo desarrollamos, a partir de esta nueva realidad de la Argentina y de la compañía,
                            una nueva forma de trabajar que sea motivante, eficiente y competitiva?
                        </p>
                        <p>
                            Así nace el Desafío ArGo, con el fin de "Seguir creciendo, a la vez que mejoramos
                            el margen e incrementamos las inversiones estratégicas (en las marcas y en nuestra
                            gente), con alto nivel de eficiencia". Para ello, se definieron 4 grupos de trabajo
                            que utilizan la metodología Breakthrough, con el fin de generar una nueva mirada
                            sobre las posibilidades y oportunidades, para lograr resultados exponenciales.
                        </p>
                        <p>
                            ArGo ya no es un proyecto, sino que es la columna vertebral de la forma de trabajo
                            de este año. Es muy importante que TODOS adoptemos el desafío, porque TODOS somos
                            ArGo.
                        </p>
                        <p>
                            ARgentina GO!! "Juntos, comprometidos para ganar y crear cada día, una compañía
                            más fuerte".
                        </p>
                    </div>
                    <br />
                    <br />
                </div>

            </ContentTemplate>
        </asp:TabPanel>
         <asp:TabPanel HeaderText="Contenidos" ID="TabPanel1" runat="server">
             <ContentTemplate>
                 <div id="Div1" class="DivContenidoARgo">
                     <div id="Div2" class="rightHalf">
                         <img src="../images/argologo.png" alt="Argo" width="200px" />
                     </div>
                     <div id="Div3" class="leftHalf">
                         <div>
                             <ul>
                                 <li><a href="/nuestracompania/documents/argo/Misión (Argo).pptx">Misión y Objetivos
                                     del desafío ArGo </a></li>
                                 <li><a href="/nuestracompania/documents/argo/Equipo ArGo.ppt">Conformación de los Equipos</a>
                                 </li>
                                 <li><a href="/nuestracompania/documents/argo/Promesas y principales acciones.pptx">Promesas
                                     y principales Acciones de los Equipos </a></li>
                                 <li><a href="/nuestracompania/documents/argo/Quick Wins (Argo).pptx">Quick Wins y Logros
                                 </a></li>
                                 <li>Identidad de ArGo (nombre y logo)
                                     <ul>
                                         <li><a href="/nuestracompania/documents/argo/Nombre y logo de ArGo.ppt">El porqué del
                                             nombre y el logo </a></li>
                                         <li><a href="/nuestracompania/documents/argo/argologo.jpg" target="_blank">Logo ArGo
                                         </a></li>
                                     </ul>
                                 </li>
                                 <li><a href="https://collab.scj.com/Projects/ARGO/SitePages/Home.aspx" target="_blank">Sharepoint ArGo
                                 </a></li>
                                 <li>BreakThrough: una nueva manera de trabajar
                                     <ul>
                                         <li><a href="/nuestracompania/documents/argo/Breakthrough (Argo).pptx">Qué es Breakthrough
                                         </a></li>
                                         <li><a href="/nuestracompania/documents/argo/Playbook - Breakthrough comunidad Argo noviembre 2013.pptx [Autosaved].pptx">
                                             Playbook </a></li>
                                     </ul>
                                 </li>
                             </ul>
                         </div>
                    </div>
                    <div class="clear"></div>
                </div>
             </ContentTemplate>
         </asp:TabPanel>
         <asp:TabPanel HeaderText="Noticias" ID="PanelNoticias" runat="server">
            <ContentTemplate>
            
               <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="1" runat="server" />

            </ContentTemplate>
         </asp:TabPanel>
    </asp:TabContainer>


 
    


</asp:Content>