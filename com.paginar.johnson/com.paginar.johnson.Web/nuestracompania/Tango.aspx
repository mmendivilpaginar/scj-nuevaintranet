﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Tango.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.Tango" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderhead" runat="server">
    <script type="text/javascript">
        function SetActiveTab(id,index) {
            var ctrl = $find(id);
            //var ctrl = document.getElementById(id);
            //ctrl.tabIndex = index;
            ctrl.set_activeTab(ctrl.get_tabs()[index]);
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <h2>Tango</h2>
    <div style="display:none">
      <asp:Button ID="buttonPostback" runat="server" Text="Nada" 
            onclick="buttonPostback_Click" />
    </div>
    <asp:TabContainer ID="TabTango" runat="server" ActiveTabIndex="2">
        <asp:TabPanel HeaderText="OFICINAS SAN ISIDRO" ID="PanelTango" runat="server">
            <contenttemplate>
                <div id="encabezado" class="DivContenidoARgo">
                    <div id="imagen" class="rightHalf">
                        <img src="/images/EnjoyDiference-4.jpg" alt="Tango" width="200px" />
                    </div>
                    <div id="bienvenida" class="leftHalft">
                        <ul>
                            <li><a href="/nuestracompania/documents/tango/PlandeMudanzaOlas.pptx">
                                 Plan de Mudanza - Olas  
                                </a>
                            </li>

                            <li><a href="/nuestracompania/documents/tango/GuíasdeCambio.pptx">
                                 Guías de Cambio 
                                </a>
                            </li>

                            
                            <li><a href="/nuestracompania/documents/tango/LosSíylosNoalahorademudarse.pptx">
                                Los Sí y los No a la hora de mudarnos 
                                </a>
                            </li>

                            <li><a href="/nuestracompania/documents/tango/TangoFotosadiciembre13.pptx">
                                Fotos de Tango 
                                </a>
                            </li>

                            

                             <li><a href="/nuestracompania/Vitrina.aspx"  target="_blank">
                                Vitrina Online
                                </a>
                            </li>

                           <li><a href="/nuestracompania/documents/tango/Cleaning day Mudanza 2014.pptx">
                              Cleaning Days
                                </a>
                            </li>

                            
                            <li>¿Cómo hacer la mudanza? 
                                <ul>
                                    <li>

                                        <%--<a href="javascript:window.open('TangoVideo.aspx','VideoExplicativo','width=740,height=350');">Video explicativo</a>--%>

                                        <a href="javascript:void();" onclick="window.open('TangoVideo.aspx','VideoExplicativo','width=655,height=360');">Video explicativo</a>
                                    </li>
                                    <li><a href="/nuestracompania/documents/tango/20131218044039196.pdf" target="_blank">
                                          Instructivo de mudanza 
                                        </a>
                                    </li>
                                </ul>
                            </li>


                            <li><a href="/nuestracompania/documents/tango/SalasdeReunionesSanIsidro.xlsx">
                              Salas de Reuniones de San Isidro
                                </a>
                            </li>

                           <li><a href="/nuestracompania/documents/tango/OficinasSanIsidroInternosTelefónicos.xlsx">
                             Dirección e Internos Telefónicos
                              </a>
                           </li>

                        </ul>


                    </div>
                    <div class="clear"></div>
                </div>
            </contenttemplate>



         </asp:TabPanel>
         <asp:TabPanel HeaderText="Novedades" ID="PanelNovedades" runat="server">
            <ContentTemplate>
            
               <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="102" runat="server" />

            </ContentTemplate>
         </asp:TabPanel>
         <asp:TabPanel ID="tabCompartiExperiencia" HeaderText="¡LLEGADA!: Comparti tu experiencia" runat="server">
            <ContentTemplate>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/BANNER_LLEGADA_EXPERIENCIA.jpg" Width="680px" />
                <asp:Literal ID="ltrScript" runat="server"></asp:Literal>   
             <asp:TabContainer ID="PublicacionesTabContainer" runat="server" ActiveTabIndex="3" 
                    CssClass="">
        <asp:TabPanel HeaderText="Experiencias" ID="TabBuscador" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">

                   <asp:Repeater runat="server" ID="rptBuscador" 
                        onitemcommand="rptBuscador_ItemCommand1" 
                        onitemdatabound="rptBuscador_ItemDataBound1">
                    <ItemTemplate>
                        <asp:HiddenField ID="HiddenFieldPublicacionId" runat="server" Value='<%# Eval("publicacionid") %>'/>
                       <div class="userCard">
                        
                        <div class="foto">
                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>'  />
                         </div>

                          <div class="info">
                         <strong>PUBLICADO POR</strong><br />
                         <strong>
                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("fecha","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("Nombre") +" "+ Eval("Apellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                int:
                                <%# Eval("interno")%></strong>
                            <br />
                            <%# Eval("areadesc")%>,<br />
                            <%# Eval("DireccionDET")%>,
                            <%# Eval("UbicacionDET")%>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                          </div>
                        </div>

                        <div class="detailAviso">
                            <ul>
                                <li class="first"></li>
                                <li>
                                    <h3> <%# Eval("Titulo").ToString().ToUpper() %></h3>
                                </li>                                
                                <li>
                                     <a href='<%#  Eval("link").ToString() %>' target="_blank" >   <%#  Eval("link").ToString() %> </A>
                                </li>
                            </ul>
                            <div class="detailAviso-imagen">
                                    <asp:Repeater runat="server" ID="rptLinks" DataSourceID="ODSLinks" OnItemDataBound="rptLinks_ItemDataBound1">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="HDPath" Value='<%# Eval("path").ToString() %>' />
                                            <asp:HyperLink ID="hplPath" runat="server" NavigateUrl='<%# "~/nuestracompania/tango/"+Eval("path") %>'
                                                Target="_blank"  > <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                            
                                            <asp:Image ID="imgPath" runat="server"  Width="150px"/>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                      <asp:HiddenField  runat="server" ID="hdPublicacionID" Value='<%# Eval("publicacionid") %>' />

                                    <asp:ObjectDataSource ID="ODSLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="getLinksByID" TypeName="com.paginar.johnson.BL.ControllerExperiencia">
                                        <SelectParameters>                                            
                                            <asp:ControlParameter ControlID="hdPublicacionID" Name="publicacionid" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                            </div>
                            <div class="detailAviso-texto">
                            <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("descripcion").ToString().Replace("\r\n","<br>") %>' />
                        </div>
                        <ul class="action-list">
                            <li>
                                <asp:Label ID="lblConComents" runat="server"></asp:Label>
                                <asp:ImageButton ID="imgComment" runat="server" ImageUrl="~/css/images/ico-comments.gif"
                                    ToolTip="Ver Comentarios"  PostBackUrl='<%#  "/nuestracompania/Tango.aspx?VERpublicacionID=" +Eval("publicacionid")+"#comentarios-abajo" %>'/>
                                    <%--  --%>
                            </li>  
                <li class="like">
                    <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional" 
                        ondatabinding="updLike_DataBinding">
                        <ContentTemplate>                      
                           <a class="load-votantes" runat="server" id="eltooltip"><asp:Label ID="lblLikeCount" runat="server" Text="0"></asp:Label></a>
                           
                            <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif" CommandName="like" CommandArgument='<%# Eval("publicacionid") %>'/>
                                
                                    <%-- [Ver más]<asp:Image ID="imgVerMas" runat="server" ImageUrl="~/images/verMas.png" Height="16px" Width="16" />--%> 

                          
                        <!-- -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
            <asp:UpdateProgress ID="upgNoticiasLike" runat="server" AssociatedUpdatePanelID="updLike">
                <ProgressTemplate>
                    <img src="../../images/preloader.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>
                </li>                                                      
                        </ul>
                        </div>



                    </ItemTemplate>


                    <FooterTemplate>

                     <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((rptBuscador.Items.Count==0).ToString())%>'
                     CssClass="messages msg-info"  Text="La búsqueda no produjo resultados para los parámetros indicados."></asp:Label>

                    </FooterTemplate>


                </asp:Repeater>



                </asp:Panel>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Mis Experiencias" ID="TabMisPublicaciones" runat="server"
            TabIndex="1">
            <ContentTemplate>
                
                <asp:HiddenField  runat="server"  ID="hdUsuarioId" />
                <asp:Repeater runat="server" ID="rptMisPublicaciones" DataSourceID="ODSMisPublicaciones" OnItemCommand="rptMisPublicaciones_ItemCommand" >
                    <ItemTemplate>
                       <div class="userCard">
                        
                        <div class="foto">
                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>'  />
                         </div>

                          <div class="info">
                         <strong>PUBLICADO POR</strong><br />
                         <strong>
                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("fecha","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("Nombre") +" "+ Eval("Apellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                int:
                                <%# Eval("interno")%></strong>
                            <br />
                            <%# Eval("areadesc")%>,<br />
                            <%# Eval("DireccionDET")%>,
                            <%# Eval("UbicacionDET")%>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                          </div>
                        </div>

                        <div class="detailAviso">
                            <ul>
                                <li class="first"></li>
                                <li>
                                    <h3>
                                        <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("publicacionid") %>'  
                                            CommandName="Editar" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                </li>                                
                                <li>
                                     <a href='<%#  Eval("link").ToString() %>' target="_blank" >   <%#  Eval("link").ToString() %> </A>
                                </li>
                            </ul>
                            <div class="detailAviso-imagen">
                                    <asp:Repeater runat="server" ID="rptLinks" DataSourceID="ODSLinks" OnItemDataBound="rptLinks_ItemDataBound1">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="HDPath" Value='<%# Eval("path").ToString() %>' />
                                            <asp:HyperLink ID="hplPath" runat="server" NavigateUrl='<%# "~/nuestracompania/vitrina/"+Eval("path") %>'
                                                Target="_blank"  > <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                            
                                            <asp:Image ID="imgPath" runat="server"  Width="150px"/>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                      <asp:HiddenField  runat="server" ID="hdPublicacionID" Value='<%# Eval("publicacionid") %>' />

                                    <asp:ObjectDataSource ID="ODSLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="getLinksByID" TypeName="com.paginar.johnson.BL.ControllerExperiencia">
                                        <SelectParameters>                                            
                                            <asp:ControlParameter ControlID="hdPublicacionID" Name="publicacionid" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                            </div>
                            <div class="detailAviso-texto">
                            <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("descripcion").ToString().Replace("\r\n","<br>") %>' />
                        </div>
                        </div>



                    </ItemTemplate>
                </asp:Repeater>

                <asp:ObjectDataSource ID="ODSMisPublicaciones" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getPublicaciones" 
                    TypeName="com.paginar.johnson.BL.ControllerExperiencia">
                    <SelectParameters>
                        <asp:Parameter Name="clusterid" Type="Int32" />
                        <asp:ControlParameter ControlID="hdUsuarioId" Name="usuarioid" 
                            PropertyName="Value" Type="Int32" />
                        <asp:Parameter Name="estado" Type="Boolean" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Publicar" ID="TabPublicar" runat="server" TabIndex="2">
            <ContentTemplate>
                <div class="block">
                    <div class="col-left">
                        <div>
               <asp:HiddenField runat="server" ID="hdModo" />
               <asp:HiddenField runat="server" ID="hdPublicacionID" />
                <label>
                                Usuario:
                </label>
                <asp:Label runat="server" ID="lblUsuario" />
                        </div>
                    </div>

                    <div class="col-left">
                        <div>
                            <label>F. Publicación:</label>
                   <asp:TextBox ID="txtFecha" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" PopupButtonID="IMGCalen1"
                    TargetControlID="txtFecha" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFecha"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Grabar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"
                    ValidationGroup="Grabar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                    ControlToValidate="txtFecha" SetFocusOnError="True">*</asp:RangeValidator>

                           <asp:RequiredFieldValidator ID="rfFecha"  ErrorMessage="Ingrese una Fecha"
                    runat="server" ControlToValidate="txtFecha" 
                    ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <div class="block centerBlock">
                    <div>
                        <label>Título:</label>
                <asp:TextBox runat="server" ID="txtTitulo" />
                        <asp:RequiredFieldValidator ID="rfTitulo" runat="server" ControlToValidate="txtTitulo" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <label>Descripción:</label>
                        <asp:TextBox runat="server" ID="txtDescripcion" Height="55px" TextMode="MultiLine" Width="335px" />
                        <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <label>Link:</label>
                        <asp:TextBox runat="server" ID="txtLink" Width="335px" />
       <asp:RegularExpressionValidator   
            ID="RegularExpressionValidator1"  
            runat="server"   
            ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"  
            ControlToValidate="txtLink" 
            ValidationGroup="Grabar" 
            ErrorMessage="El link ingresado no es válido. (Ej.: http://www.scj.com.ar)"  
            ></asp:RegularExpressionValidator> 
                        <%--<asp:RequiredFieldValidator ID="rfLink" runat="server" ControlToValidate="txtLink" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div>
                        <label>Adjuntar Archivo:</label>
                <asp:FileUpload ID="FULinks" runat="server" />
                        <span>(Hasta 5 MB)</span>
                        <asp:ImageButton ID="imgAgregarLink" runat="server" ImageUrl="~/images/verMas.png" OnClick="imgAgregarLink_Click" ValidationGroup="links" ToolTip="Agregar Archivo" />
                        <asp:GridView ID="gvLinks" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnRowCommand="gvLinks_RowCommand" Width="50px" GridLines="None" CssClass="mt-10">
                <Columns>
                <asp:BoundField DataField="path" />
                <asp:TemplateField  >
                <ItemTemplate  >
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png"
                CommandName="Quitar" CommandArgument='<%# Eval("path") %>' />
                </ItemTemplate>
                <ItemStyle Width="20px" />
                </asp:TemplateField>
                </Columns>
                </asp:GridView>
                    </div>

                    <div>
                        <label>Estado:</label>
                <asp:RadioButtonList runat="server" ID="rbEstado">
                    <asp:ListItem Text="Habilitado" Value="True" />
                    <asp:ListItem Text="Deshabilitado" Value="False" />
                </asp:RadioButtonList>
                    </div>
                    
                <div class="controls">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar"
                        ValidationGroup="Grabar" onclick="btnGuardar_Click" />
                    <asp:Button ID="btnCancelar" runat="server" CausesValidation="False" Text="Cancelar" OnClick="btnCancelar_Click" />
                </div>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel ID="TabComentario" runat="server" TabIndex="3" HeaderText="Comentarios">
            <ContentTemplate>
            <asp:HiddenField ID="HiddenFieldPublicacionId" runat="server" />
        <asp:UpdatePanel ID="updComments" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
   
            <div class="noticia">
                
                <asp:DataList ID="DLExperiencia" runat="server" 
                    onitemdatabound="DLExperiencia_ItemDataBound">
                    <ItemTemplate>
                        
                       <div class="userCard">
                        
                        <div class="foto">
                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>'  />
                         </div>

                          <div class="info">
                         <strong>PUBLICADO POR</strong><br />
                         <strong>
                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("fecha","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("Nombre") +" "+ Eval("Apellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                int:
                                <%# Eval("interno")%></strong>
                            <br />
                            <%# Eval("areadesc")%>,<br />
                            <%# Eval("DireccionDET")%>,
                            <%# Eval("UbicacionDET")%>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                          </div>
                        </div>

                        <div class="detailAviso">
                            <ul>
                                <li class="first"></li>
                                <li>
                                    <h3> <%# Eval("Titulo").ToString().ToUpper() %></h3>
                                </li>                                
                                <li>
                                     <a href='<%#  Eval("link").ToString() %>' target="_blank" >   <%#  Eval("link").ToString() %> </A>
                                </li>
                            </ul>
                            <div class="detailAviso-imagen">
                                    <asp:Repeater runat="server" ID="rptLinks" DataSourceID="ODSLinks" OnItemDataBound="rptLinks_ItemDataBound1">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="HDPath" Value='<%# Eval("path").ToString() %>' />
                                            <asp:HyperLink ID="hplPath" runat="server" NavigateUrl='<%# "~/nuestracompania/tango/"+Eval("path") %>'
                                                Target="_blank"  > <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                            
                                            <asp:Image ID="imgPath" runat="server"  Width="150px"/>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                      <asp:HiddenField  runat="server" ID="hdPublicacionID" Value='<%# Eval("publicacionid") %>' />

                                    <asp:ObjectDataSource ID="ODSLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="getLinksByID" TypeName="com.paginar.johnson.BL.ControllerExperiencia">
                                        <SelectParameters>                                            
                                            <asp:ControlParameter ControlID="hdPublicacionID" Name="publicacionid" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                            </div>
                            <div class="detailAviso-texto">
                            <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("descripcion").ToString().Replace("\r\n","<br>") %>' />
                        </div>
                        <ul class="action-list">
  <%-- 
                <li class="like">
                    <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional" 
                        ondatabinding="updLike_DataBinding">
                        <ContentTemplate>                      
                           <a class="load-votantes" runat="server" id="eltooltip"><asp:Label ID="lblLikeCount" runat="server" Text="0"></asp:Label></a>
                           
                            <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif" CommandName="like" CommandArgument='<%# Eval("publicacionid") %>'/>
                                
                                  

                          
                        <!-- -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
            <asp:UpdateProgress ID="upgNoticiasLike" runat="server" AssociatedUpdatePanelID="updLike">
                <ProgressTemplate>
                    <img src="../../images/preloader.gif" />
                </ProgressTemplate>
            </asp:UpdateProgress>
                </li>        --%>                                              
                        </ul>
                        </div>



                    </ItemTemplate>
                </asp:DataList>
    </div>
    <!--COMENTARIOS-->
    <div class="box comentarios">

                <a name="comentarios-abajo"></a>
                <div class="usuario-comentar">
                    <asp:Image ID="imgUsuario" runat="server" Height="50px" Width="50px" CssClass="hide" />
                    <div class="form-item">
                        <label>
                            Agregar comentario:</label>
                        <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComentario"
                            ValidationGroup="Comentar"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controls">
                        <asp:Button ID="btnComentar" runat="server" Text="Comentar"
                            ValidationGroup="Comentar" onclick="btnComentar_Click" />
                    </div>
                </div>
                 <div class="lista-comentarios">
                     <asp:DataList ID="dlComentarios" runat="server" DataKeyField="ComentarioId" 
                         DataSourceID="ObjectDataSourceListadoComentarios">
                         <ItemTemplate>
                            <div class="comentario">
                                <div class="pic">
                                    <asp:Image ID="Image2" runat="server" ToolTip='<%# Eval("nombrecompleto") %>' ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif"  %>' />
                                </div>
                                <div class="content">
                                    <p>
                                        <asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                    <p class="fecha">
                                        Fecha:
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy - hh:MM tt}") %>' /></p>
                                    <p>
                                        Comentario:
                                        <asp:Label ID="comentariosLabel" runat="server" Text='<%# Eval("Comentario") %>' /></p>
                                </div>
                            </div>
                         </ItemTemplate>
                     </asp:DataList>
                     <asp:ObjectDataSource ID="ObjectDataSourceListadoComentarios" runat="server" 
                         OldValuesParameterFormatString="original_{0}" 
                         SelectMethod="getComentariosByPublicacionId" 
                         TypeName="com.paginar.johnson.BL.ControllerExperiencia">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="HiddenFieldPublicacionId" Name="publicacionId" 
                                 PropertyName="Value" Type="Int32" />
                         </SelectParameters>
                     </asp:ObjectDataSource>

    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="upgComments" runat="server" AssociatedUpdatePanelID="updComments">
            <ProgressTemplate>
                <img src="../images/preloader.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>
               
            </ContentTemplate>
        </asp:TabPanel>
        </asp:TabContainer>
                
            </ContentTemplate>
         </asp:TabPanel>

    </asp:TabContainer>


    
</asp:Content>
