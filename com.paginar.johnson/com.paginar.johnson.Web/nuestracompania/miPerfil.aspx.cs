﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Web.UI.HtmlControls;

namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class miPerfil : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // if (!Page.IsPostBack)
           // {
                HDUsuarioID.Value = ((MasterBase)this.Page.Master).ObjectUsuario.usuarioid.ToString();
                //HDUsuarioID.Value = this.ObjectUsuario.usuarioid.ToString();
                
                FormViewUsuario.DataBind();
                if (FormViewUsuario.DataItemCount < 1)
                {

                    Label10.Text = "<div>La información solicitada no se encuentra disponible.</div><div style=\" font-family:Arial; font-size:11px; border: 1px solid; margin-top: 50px; padding: 15px 50px 15px 50px; background-repeat: no-repeat; background-position: 10px center; display: block; color: #9F6000; background-color: #FEEFB3; background-image: url(images/alerta.png);\">Para consultas sobre esta sección o su contenido, y para agregar tu foto, por favor contactar a <b>María del Carmen Aranguren</b> </div>";
                }else{
            //}
           // if (FormViewUsuario.DataItemCount < 1)
           // {
                ControllerUsuarios CU = new ControllerUsuarios();
                DSUsuarios.HijosDataTableDataTable hijos = new DSUsuarios.HijosDataTableDataTable();
                hijos = CU.getHijosByUsuarioID(int.Parse(HDUsuarioID.Value));
                Label cantidadHijos = (Label)FormViewUsuario.FindControl("cantHijos");
                Label hijosLink = (Label)FormViewUsuario.FindControl("lblHijosLink");
                hijosLink.Enabled = false;
                if (hijos.Count > 0)
                {
                    hijosLink.Text = "<li class=\"conbulet\"><a class=\"mishijos\" runat=\"server\" href=\"#dialogh\" name=\"modal\">Hijos</a></li>";
                    hijosLink.Enabled = true;
                    cantidadHijos.Text = hijos.Count.ToString();
                }

              HtmlGenericControl liModificarDomicilio= (HtmlGenericControl)FormViewUsuario.FindControl("liModificarDomicilio");
              HtmlGenericControl liSolicitarLicencia = (HtmlGenericControl)FormViewUsuario.FindControl("liSolicitarLicencia");
              if (ObjectUsuario.clusterID == 3 || ObjectUsuario.clusterID == 4 || ObjectUsuario.clusterID == 2)
                  liModificarDomicilio.Visible = liSolicitarLicencia.Visible = false;
            }


        }


        protected bool IsEmpty(object cadena)
        {
            if (cadena != null)
            {

                return string.IsNullOrEmpty(cadena.ToString());
            }
            return true;
        }

        protected bool IsArgentina(object Cluster)
        {
            int cluster = int.Parse(Convert.ToString(Cluster));
            return (cluster == 1);
        }

        public string FormatearFecha(object fecha)
        {

            if (fecha != null)
            {
                DateTime FechaAux = DateTime.MinValue;
                if (DateTime.TryParse(Convert.ToString(fecha), out FechaAux))
                {
                    return string.Format("{0:dd} de {1:MMMM} de {0:yyyy}", FechaAux, FechaAux, FechaAux);
                }
            }
            return string.Empty;


        }

        protected bool IsEqual(object cadena, string otracadena)
        {
            if (cadena != null)
            {
                return (Convert.ToString(cadena) == otracadena);
            }
            return false;
        }

        protected void ObjectDataSourceUsuarioDet_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.Cancel = string.IsNullOrEmpty(Request.QueryString["UsuarioID"]);
        }

        public string FechaCorta(string Fecha)
        {
            if (string.IsNullOrEmpty(Fecha))
                return "";

            DateTime fecha2 = Convert.ToDateTime(Fecha);
            
            return fecha2.ToShortDateString();
        }

        public string CalcularEdad(object birthdateaux)
        {
           if (string.IsNullOrEmpty(birthdateaux.ToString()))
                return "";

             DateTime birthdate =  Convert.ToDateTime(birthdateaux.ToString());
            // get the difference in years
            int years = DateTime.Now.Year - birthdate.Year;
            // subtract another year if we're before the
            // birth day in the current year
            if (DateTime.Now.Month < birthdate.Month || (DateTime.Now.Month == birthdate.Month && DateTime.Now.Day < birthdate.Day))
                years--;

            return years.ToString() + " Años";
        }

        public string departamento(string depto)
        {

            if (depto.Length >= 1)
                return " <strong>Depto:</strong> " + depto;
            else
                return " ";
        }
        public string piso(string ps)
        {
            if (ps.Length >= 1)
                return " <strong>Piso:</strong> " + ps;
            else
                return " ";
        }

       
    }
}