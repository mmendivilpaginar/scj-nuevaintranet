﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;


namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class politicasdeempresa : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TreeViewPoliticas.Attributes.Add("onclick", "pageScrollPoliticasDeEmpresa(event)");

                ControllerContenido cc = new ControllerContenido();
                RelationalSystemDataSource1.Path= cc.Content_TipoGetIdrootByClusterId("Políticas de Empresa", this.ObjectUsuario.clusteridactual).ToString();

                RelationalSystemDataSource1.DataBind();
                TreeViewPoliticas.DataBind();

                HiddenFieldItemID.Value = "0";
                frmContenido.DataBind();

                if (Request.QueryString["PoliticasDeReconocimiento"] != null)
                {
                    TreeNode node = TreeViewPoliticas.FindNode("Política de Reconocimientos");
                    if (node != null) node.Selected = true;
                         HiddenFieldItemID.Value = node.DataPath;
                }

                if (Request.QueryString["Sustentabilidad"] != null)
                {
                    TreeNode node = TreeViewPoliticas.FindNode("Política de Sustentabilidad");
                    if (node != null) node.Selected = true;
                    HiddenFieldItemID.Value = node.DataPath;
                }
                
                              
               
            }

            
           
        }

        protected void TreeViewContenido_SelectedNodeChanged(object sender, EventArgs e)
        {
           // System.Threading.Thread(5000);
            HiddenFieldItemID.Value = TreeViewPoliticas.SelectedNode.DataPath;
        }

        protected void TreeViewPoliticas_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
           {
            HiddenFieldItemID.Value = e.Node.DataPath;
            //datasource del contenido que se carga en el formulario
            odsContenido.DataBind();
            frmContenido.DataBind();
            string contenidoStr;
            Literal Contenido = (Literal)frmContenido.FindControl("ltContenido");
            contenidoStr = Contenido.Text;
            contenidoStr.Substring(contenidoStr.Length - 3);
            if (contenidoStr.Substring(contenidoStr.Length - 3) == "pps" || contenidoStr.Substring(contenidoStr.Length - 3) == "xls" || contenidoStr.Substring(contenidoStr.Length - 3) == "ppt" || contenidoStr.Substring(contenidoStr.Length - 3) == "doc" || contenidoStr.Substring(contenidoStr.Length - 3) == "htm" || contenidoStr.Substring(0, 4) == "http")
            {
                e.Node.NavigateUrl = Contenido.Text;
                e.Node.Target = "_blank";    
                

            }

            //else if (Contenido.Text == "Tipos de Reconocimiento" || Contenido.Text == "¡Muy bien hecho!")
            //    e.Node.NavigateUrl = "/asa/asa.asp";
          

            switch (e.Node.Text) { 
                case "Formulario solicitud del reconocimiento":
                    e.Node.ToolTip = "Realizar una copia en tu pc antes de completarlo y enviarlo a tu jefe";
                    break;
                case "Carta de comunicación":
                case "Integridad":
                case "Velocidad":
                case "Compromiso Personal":
                    e.Node.ToolTip = "Realizar una copia en tu pc antes de completarla e imprimirla";
                    break;
                case "¡Muy Bien Hecho! *":
                    e.Node.ToolTip = "Certificado de agradecimiento a contribuciones de empleados que han demostrado algún valor, actitud o comportamiento destacable en el desarrollo de tareas o proyectos, generando un valor agregado a su gestión diaria. Lo puede proponer cualquier empleado otorgarlo a un compañero de su misma área o de diferentes áreas. No requiere nivel de aprobación.";
                    break;
                case "¡Gracias Equipo! *":
                    e.Node.ToolTip = "Reconocimiento a través de una celebración que se realizará para un Equipo por los resultados de un excelente trabajo realizado. Lo puede proponer el gerente y aprueba el director del área.";
                    break;
                case "Reconocimientos de la Dirección (Mc Award) *":
                    e.Node.ToolTip = "Reconocimientos por aportaciones que excedan significativamente los estándares y/o expectativas para un proyecto prioritario para el negocio a nivel local, de acuerdo con los principios de 'En Esto Creemos' y la 'Estrategia 2016'. Serán definidos y aprobados tanto por los directores que formen parte del Comité de Dirección como del Centro de Servicios Compartidos.";
                    break;
                case "Objetivo del Programa *":
                    e.Node.ToolTip="Otorgar reconocimientos al personal, individual o grupal, por alguna acción que genere valor agregado tanto en resultados como en actitudes.";
                    break;
                case "¡Gracias! *":
                    e.Node.ToolTip = "Reconocimiento para un empleado o grupo de empleados que obtengan logros / aportaciones extraordinarias, ya sea por ser parte de las responsabilidades de su trabajo, o bien adicionales a ellas, pero que excedan significativamente los estándares y/o expectativas de un objetivo fijado previamente. Lo puede proponer el jefe o gerente y aprueba el director del área.";
                    break;
                

            }


            if (contenidoStr == "&nbsp;")
                e.Node.SelectAction = TreeNodeSelectAction.Expand;
            else
                e.Node.SelectAction = TreeNodeSelectAction.SelectExpand;

            HiddenFieldItemID.Value = "0";
            frmContenido.DataBind();

        }



    }
}