﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Estrategias2016.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.Estrategias2016" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource" TagPrefix="cc1" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">
        $(function () {
            $('a.linkToolTip').cluetip({
                local: true,
                hideLocal: true,
                sticky: false,
                cursor: 'pointer',
                showTitle: false,
                width: '200px'

            });

        });
  </script>
<h2>Estrategia 2016</h2>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="Estrategia 2016" ID="PanelEstrategias" runat="server">
            <ContentTemplate>
                    
                  <div id="encabezado" class="DivEncabezadoEstrategias2016">
                  <div id="imagen" class="leftHalfEstrategias">
                  <img  src="../images/estrategias2016.jpg" alt="Estrategias 2016"/>
                  </div>
                  <div id="bienvenida" class="rightHalfEstrategias">
                   <p>Bienvenidos a la sección de Estrategia 2016 de la Intranet Cono Sur. Aquí, encontrarán información sobre el camino que está emprendiendo SCJ por los próximos años, cómo lo conseguiremos y la importancia del rol que cada uno juega para lograr alcanzar los objetivos.</p>
                   <p>Importante: Estrategia 2016 es confidencial y el material aquí expuesto es sólo para el uso interno. NO está permitido copiarlo, fowardearlo o distribuirlo con ningún otro fin.</p>
                   <p>El éxito de la compañía depende de la integridad de todos quienes la componen. Por favor, protejan esta información confidencial.</p>
                  </div>
                  <br />
                  <br />
                  </div>

                  <div id="contenido" class="DivContenidoEstrategias2016">
                  <h3><a class="linkToolTip" href="#Contenido" rel="#Contenido" style="text-decoration: none;">Contenido</a>:</h3>
                  <div id="Contenido">Desde aquí podrá acceder al contenido de cada link</div>
                  <div class="leftHalf">
                  <asp:TreeView ID="TreeViewEstrategias2016" runat="server" 
                          DataSourceID="RelationalSystemDataSourceEstrategias2016" OnSelectedNodeChanged="TreeViewEstrategias2016_SelectedNodeChanged"  
                               ontreenodedatabound="TreeViewEstrategias2016_TreeNodeDataBound">
                    <DataBindings>
                        <asp:TreeNodeBinding TextField="Name" />
                    </DataBindings>
                </asp:TreeView>

                  <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSourceEstrategias2016" IncludeRoot="false" />
                  </div>
                  <div class="rightHalf"> 
                  <asp:TreeView ID="TreeViewEstrategias2016_2" runat="server" 
                          DataSourceID="RelationalSystemDataSourceEstrategias2016_2" OnSelectedNodeChanged="TreeViewEstrategias2016_SelectedNodeChanged"  
                               ontreenodedatabound="TreeViewEstrategias2016_TreeNodeDataBound">
                    <DataBindings>
                        <asp:TreeNodeBinding TextField="Name" />
                    </DataBindings>
                </asp:TreeView>
                  <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSourceEstrategias2016_2" IncludeRoot="false" />
                   </div>
                  </div>
                  
                  <img   src="../images/PieEstrategias2016.jpg"/> 
            </ContentTemplate>
         </asp:TabPanel>
        <asp:TabPanel HeaderText="Noticias" ID="PanelNoticias" runat="server">
            <ContentTemplate>
               
               <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="92"  TagID="1" runat="server" />
     
             <img   src="../images/PieEstrategias2016.jpg"/>
            </ContentTemplate>
         </asp:TabPanel>
     </asp:TabContainer>




 
      
 
   

</asp:Content>
