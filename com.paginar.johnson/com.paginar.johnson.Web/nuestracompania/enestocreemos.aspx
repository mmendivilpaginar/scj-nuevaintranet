﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="enestocreemos.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.enestocreemos" %>

<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script src="../js/init.js" type="text/javascript"></script>
    <h2>
        En Esto Creemos</h2>
    <h3>
        El compromiso de la familia Johnson
    </h3>
    <strong>Pilares:</strong>
    <asp:TreeView ID="TreeViewEnEstoCreemos" runat="server" DataSourceID="RelationalSystemDataSource1"
        OnSelectedNodeChanged="TreeViewEnEstoCreemos_SelectedNodeChanged">
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    <br /><br />
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
    <a name="contenido-abajo"></a>
    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
            <div id="divContenido">
                <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                    <ItemTemplate>
                        <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewEnEstoCreemos" EventName="SelectedNodeChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
