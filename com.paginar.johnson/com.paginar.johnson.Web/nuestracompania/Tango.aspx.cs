﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Data;
using System.IO;
using System.Text;
using AjaxControlToolkit;

namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class Tango : PageBase
    {
        private DataTable _dtLinks;
        public DataTable dtLinks
        {
            get
            {
                if (Session["dtLinks"] == null)
                {
                    _dtLinks = new DataTable();
                    _dtLinks.Columns.Add("path");
                    Session["dtLinks"] = _dtLinks;
                }
                return (DataTable)Session["dtLinks"];
            }

            set
            {
                Session["dtLinks"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request["publicacionID"] != null)
                {
                    Editar(int.Parse(Request["publicacionid"].ToString()));

                }
                else
                {
                    Nuevo();
                    TabTango.ActiveTab = tabCompartiExperiencia;
                    PublicacionesTabContainer.ActiveTab = TabBuscador;
                }

                BuscarTodos();
            }

            if (Request["VERpublicacionID"] != null)
            {
                CargarDatos(int.Parse(Request["VERpublicacionID"]));
            }
            else
            {
                TabComentario.Visible = false;
            }            

            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();

        }

        protected void CargarDatos(int publicacionId)
        {
            TabComentario.Visible = true;
            HiddenFieldPublicacionId.Value = publicacionId.ToString();
            PublicacionesTabContainer.ActiveTabIndex = 3;
            ControllerExperiencia CE = new ControllerExperiencia();
            DLExperiencia.DataSource = CE.getPublicacionByID(publicacionId); 
                //CE.getComentariosByPublicacionId(publicacionId);
            DLExperiencia.DataBind();
        
        }


        private void Nuevo()
        {
            hdModo.Value = "Nuevo";
            Session["dtLinks"] = null;
            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();
            lblUsuario.Text = this.ObjectUsuario.apellido + " " + this.ObjectUsuario.nombre;
            rbEstado.SelectedIndex = 0;
        }


        protected void Editar(int publicacionid)
        {
            lblUsuario.Text = this.ObjectUsuario.apellido + " " + this.ObjectUsuario.nombre;
            hdModo.Value = "Editar";
            hdPublicacionID.Value = publicacionid.ToString();
            ControllerExperiencia ce = new ControllerExperiencia();
            DSExperiencia.exp_PublicacionDataTable dt = ce.getPublicacion(publicacionid);        

            txtFecha.Text = string.Format("{0: dd/MM/yyyy}", dt.Rows[0]["fecha"]);
            txtTitulo.Text = dt.Rows[0]["titulo"].ToString();
            txtDescripcion.Text = dt.Rows[0]["descripcion"].ToString();
            txtLink.Text = dt.Rows[0]["link"].ToString();


            if (dt.Rows[0]["estado"].ToString() == "True")
                rbEstado.SelectedIndex = 0;
            else
                rbEstado.SelectedIndex = 1;




            DSExperiencia.exp_LinkDataTable dl = ce.getLinksByID(publicacionid);            
            dtLinks = dl;
            gvLinks.DataSource = dtLinks;
            gvLinks.DataBind();
            TabTango.ActiveTab = tabCompartiExperiencia;
            PublicacionesTabContainer.ActiveTabIndex = 2;


        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();

            BuscarTodos();
            PublicacionesTabContainer.ActiveTabIndex = 1;
        }

        protected string httpAdd(string url)
        {
            string returnValue = string.Empty;
            if (url.Substring(0, 7) == "http://" || url.Substring(0, 8) == "https://")
                returnValue = url;
            else
                returnValue = "http://" + url;
            return returnValue;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ControllerExperiencia ce = new ControllerExperiencia();

                if (hdModo.Value == "Nuevo")
                {
                    ce.AgregarPublicacion(this.ObjectUsuario.usuarioid, DateTime.Parse(txtFecha.Text), txtTitulo.Text, txtDescripcion.Text, bool.Parse(rbEstado.SelectedValue.ToString()), txtLink.Text, dtLinks);

                }

                if (hdModo.Value == "Editar")
                {
                    ce.ModificarPublicacion(int.Parse(hdPublicacionID.Value), this.ObjectUsuario.usuarioid, DateTime.Parse(txtFecha.Text), txtTitulo.Text, txtDescripcion.Text, bool.Parse(rbEstado.SelectedValue.ToString()), txtLink.Text, dtLinks);
                    PublicacionesTabContainer.ActiveTabIndex = 1;
                }

                dtLinks = null;
                gvLinks.DataSource = dtLinks;
                gvLinks.DataBind();
                TabTango.ActiveTab = tabCompartiExperiencia;
                PublicacionesTabContainer.ActiveTab = TabMisPublicaciones;
                rptMisPublicaciones.DataBind();



                Limpiar();

                BuscarTodos();

            }


        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }
        protected void rptLinks_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            HiddenField hd = (HiddenField)e.Item.FindControl("hdPath");
            HyperLink hpl = (HyperLink)e.Item.FindControl("hplPath");
            Image img = (Image)e.Item.FindControl("imgPath");

            string path = hd.Value;

            if (path.Contains(".jpg") || path.Contains(".png"))
            {
                img.ImageUrl = "~/nuestracompania/tango/" + path;
                img.Attributes.Add("onclick", "flyer('~/nuestracompania/tango/" + path + "');");
                img.Attributes.Add("style", "cursor: pointer");
                img.Visible = true;
                hpl.Visible = false;
            }
            else
            {
                img.Visible = false;
                hpl.Visible = true;
            }

        }


        protected void rptMisPublicaciones_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Editar")
            {
                Response.Redirect("Tango.aspx?publicacionID=" + e.CommandArgument);
            }
        }


        protected void imgAgregarLink_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {



                if (FULinks.HasFile)
                {
                    string filename = Path.GetFileNameWithoutExtension(FULinks.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(FULinks.FileName);
                    string strPath = MapPath("~/nuestracompania/tango/") + filename;
                    FULinks.SaveAs(@strPath);

                    DataRow dr = dtLinks.NewRow();
                    dr["path"] = filename;
                    if (hdModo.Value == "Editar") dr["publicacionid"] = 0;

                    dtLinks.Rows.Add(dr);

                    gvLinks.DataSource = dtLinks;
                    gvLinks.DataBind();

                }




            }
        }


        protected void gvLinks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Quitar")
            {
                string publicacionid = e.CommandArgument.ToString();

                DataRow[] row = dtLinks.Select("path='" + publicacionid.ToString() + "'");
                if (row.Length > 0)
                    row[0].Delete();

                gvLinks.DataSource = dtLinks;
                gvLinks.DataBind();

            }
        }


        protected void ButtonBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void Buscar()
        {
            ControllerExperiencia cv = new ControllerExperiencia();

            rptBuscador.DataSource = cv.getPublicaciones(1, 0, true);
            
            rptBuscador.DataBind();
            TabTango.ActiveTab = tabCompartiExperiencia;
            PublicacionesTabContainer.ActiveTab = TabBuscador;
        }

        private void BuscarTodos()
        {
            ControllerExperiencia cv = new ControllerExperiencia();

            rptBuscador.DataSource = cv.getPublicaciones(1, 0, true);
            rptBuscador.DataBind();
            TabTango.ActiveTab = tabCompartiExperiencia;
            //PublicacionesTabContainer.ActiveTab = TabBuscador;
        }


        private void Limpiar()
        {
            txtFecha.Text = "";
            txtLink.Text = "";
            txtTitulo.Text = "";
            txtDescripcion.Text = "";
        }

        protected void updLike_DataBinding(object sender, EventArgs e)
        {
            //Label lblll = 

        }

        protected void rptBuscador_ItemCommand1(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "like")
            {
                ControllerExperiencia CE = new ControllerExperiencia();
                int InfoID = int.Parse(e.CommandArgument.ToString());
                if ((CE.UserLikeThis(InfoID, ((MasterBase)this.Page.Master).ObjectUsuario.usuarioid)).Rows.Count == 0)
                {
                    CE.setLike(InfoID, ((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, DateTime.Now);
                    Label lblLikeCount = (Label)e.Item.FindControl("lbllikecount");
                    lblLikeCount.Text = (CE.getLikes(InfoID)).Rows[0].ItemArray[5].ToString();
                    EjecutarScript("alert('Gracias por Votar');");
                }
                else
                    EjecutarScript("alert('Esta Experiencia ya cuenta con su voto');");

            }
        }

        protected void rptBuscador_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
                Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            
                int publicacionId = int.Parse(((HiddenField)e.Item.FindControl("HiddenFieldPublicacionId")).Value.ToString());
                ControllerExperiencia CE = new ControllerExperiencia();
                Label lblLikeCount = (Label)e.Item.FindControl("lblLikeCount");
                Label lblConComents = (Label)e.Item.FindControl("lblConComents");
                lblLikeCount.Text = (CE.getLikes(publicacionId)).Rows[0].ItemArray[5].ToString();
                lblConComents.Text = (CE.GetMessagesCount(publicacionId)).Rows[0].ItemArray[7].ToString();

            }
        }

        protected void DLExperiencia_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Response.ContentEncoding = Encoding.GetEncoding("iso-8859-1");
            Request.ContentEncoding = Encoding.GetEncoding("iso-8859-1");

        }

        protected void btnComentar_Click(object sender, EventArgs e)
        {
            ControllerExperiencia CE = new ControllerExperiencia();
            string userText = txtComentario.Text.ToString();
            userText = userText.Replace("\r\n", "<br/>").Replace("\r", "<br/>");
            CE.InsertCommentByPublicacionId(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, userText, DateTime.Now, int.Parse(HiddenFieldPublicacionId.Value.ToString()));
            txtComentario.Text = "";
            dlComentarios.DataBind();
            EjecutarScript("document.getElementById('" + buttonPostback.ClientID + "').click();");
        }

        protected void buttonPostback_Click(object sender, EventArgs e)
        {
            BuscarTodos();
        }

    }
}