﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master"  AutoEventWireup="true" CodeBehind="Sustentabilidad.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.Sustentabilidad" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource" TagPrefix="cc1" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<style type="text/css">
.layer1 {
margin: 0;
padding: 0;
width: 500px;
}
 
.heading {
margin: 1px;

padding: 3px 10px;
cursor: pointer;
position: relative;

}
.content {
padding: 5px 10px;
padding-left: 25px;
background-color:#fafafa;
}
p { padding: 5px 0; }
</style>
    <script type="text/javascript">
        $(function () {
            $('a.linkToolTip').cluetip({
                local: true,
                hideLocal: true,
                sticky: false,
                cursor: 'pointer',
                showTitle: false,
                width: '200px'

            });

        });


        $(document).ready(function () {
            jQuery(".content").hide();
            //toggle the componenet with class msg_body
            jQuery(".heading").click(function () {
                var up = "../css/images/bullet-azul.png";
                var down = "../css/images/bullet-celeste-abajo.png";

                var icon = $("img[id=img" + $(this).attr("id") + "]");

                var iconoAnterior = icon.attr("src");


                jQuery(this).next(".content").slideToggle(500);

                icon.attr("src", iconoAnterior == up ? down : up);

            });
        });
      
  </script>
<h2>Sustentabilidad</h2>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="Sustentabilidad" ID="PanelSustentabilidad" runat="server">
            <ContentTemplate>
                    
                 <div class="DivSustentabilidad" >
                  <div id="imagen" class="leftHalfSustentabilidad" >
                 
                  <img  src="../images/Sustentabilidad.jpg" alt="banner sustentabilidad"/>
                 
                  
                  </div>
                  <div id="bienvenida" class="rightHalfSustentabilidad">
                   <p>La sustentabilidad es un aspecto intrínseco a la gestión del negocio de SC Johnson & Son, 
                   resultado de un proceso dinámico que se inició con el nacimiento de la compañía y se consolidó en prácticas y políticas de sus diferentes áreas y en la relación con todos sus grupos de interés.</p>
                   <p>La gestión responsable y ética en sus distintos ámbitos y con sus diferentes públicos.
                   La gestión sustentable de SC Johnson & Son de Argentina se construye sobre cinco ejes:</p>
                   <p><ul><li >Cuidar a nuestra gente y ofrecer un excelente lugar de trabajo</li>
                      <li >Mejorar continuamente nuestros productos y envases</li>
	                  <li >Fortalecer a nuestras comunidades</li>
	                  <li >Usar responsablemente los recursos</li>
	                  <li >Desarrollar nuestra cadena de valor</li></ul></p>
                  </div>
                
                  </div>

                  <div id="contenido" class="DivContenidoEstrategias2016">
                  <br />
                  <br />
                  <h3>Contenido:</h3>

                  <div class="layer1">
                  <p class="heading" id="1"><img id="img1"  src="../css/images/bullet-azul.png" />&nbsp Cuidar a nuestra gente y ofrecer un excelente lugar de trabajo</p>
                  
                  <div class="content">
                   Del mismo modo que SC Johnson lidera gran parte de los mercados en los que compite, es también un fuerte referente en cómo cuidar a sus empleados, al ser considerada una de las mejores compañías para trabajar de la Argentina.
                  </div>
                 <p class="heading" id="2"><img id="img2"  src="../css/images/bullet-azul.png" />&nbsp Mejorar continuamente nuestros productos y envases</p>
                 <div class="content">Trabajamos constantemente para hacer productos de calidad y desempeño superior.</div>
   
                 <p class="heading" id="3"><img id="img3"  src="../css/images/bullet-azul.png" />&nbsp Fortalecer a nuestras comunidades</p>
                  <div class="content">Asumimos el compromiso de fomentar el bienestar en cada comunidad donde operamos, contribuyendo en la mejora de la calidad de vida de las personas y familias, con trabajo, voluntariado e inversión social.</div>
 
                 <p class="heading" id="4"><img id="img4"  src="../css/images/bullet-azul.png" />&nbsp Usar responsablemente los recursos</p>
                  <div class="content">Sobre la base del concepto de Ecoeficiencia, promovemos un diseño integral para minimizar el uso de materiales y energía durante todo el ciclo de vida de nuestros productos y en cada servicio que prestamos.</div>
 
                 <p class="heading" id="5"><img id="img5"  src="../css/images/bullet-azul.png" />&nbsp Desarrollar nuestra cadena de valor</p>
                 <div class="content"> Desde que comenzó a operar en la Argentina, la compañía trabaja con el compromiso de desarrollar su cadena de valor, a través de prácticas de gestión sustentable con proveedores, distribuidores, clientes y consumidores. </div>
                  
                 <p class="heading"> <a href="../NuestraCompania/politicasdeempresa.aspx?Sustentabilidad=1" target="_self" style="text-decoration: none;"> <img  src="../css/images/bullet-azul.png" /> &nbsp Política de sustentabilidad</a></p>

                 <p class="heading"> <a href="http://www.scjohnson.com.ar/sustentabilidad/Pages/Sustentabilidad-ambiental.aspx" target="_blank" style="text-decoration: none;"> <img  src="../css/images/bullet-azul.png" /> &nbsp Leer más sobre Sustentabilidad ambiental en el sitio de SC Johnson & Son</a></p>
                 
                  </div>


                 
                  </div>
            </ContentTemplate>
         </asp:TabPanel>
        <asp:TabPanel HeaderText="Noticias" ID="PanelNoticias" runat="server">
            <ContentTemplate>
               
               <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="93" TagID="2" runat="server" />
     

            </ContentTemplate>
         </asp:TabPanel>
     </asp:TabContainer>




 
      
 
   

</asp:Content>
