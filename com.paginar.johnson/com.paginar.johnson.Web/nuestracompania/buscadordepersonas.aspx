﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master"
    AutoEventWireup="true" CodeBehind="buscadordepersonas.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.buscadordepersonas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Buscador de personas</h2>
    <div class="box formulario">
        <asp:Panel ID="pnlBusqueda" runat="server" DefaultButton="btnBuscar">
            <div class="form-item leftHalf">
                <label>
                    <asp:Label ID="lblPais" runat="server" Text="País"></asp:Label></label>
                <asp:DropDownList ID="drpPais" runat="server" AutoPostBack="True" DataSourceID="odsPais"
                    DataTextField="Descripcion" DataValueField="ID" AppendDataBoundItems="True" OnSelectedIndexChanged="drpPais_SelectedIndexChanged">
                    <asp:ListItem Value="0">Todos</asp:ListItem>
                </asp:DropDownList>
                <asp:ObjectDataSource ID="odsPais" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getCluster" TypeName="com.paginar.johnson.BL.ControllerCluster">
                </asp:ObjectDataSource>
            </div>
            <div class="form-item rightHalf">
                <asp:UpdatePanel ID="updUbicacion" runat="server">
                    <ContentTemplate>
                        <label>
                            <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label></label>
                        <asp:DropDownList ID="drpUbicacion" runat="server" DataSourceID="odsUbicacion" DataTextField="Descripcion"
                            DataValueField="UbicacionID" AppendDataBoundItems="True">
                            <asp:ListItem Value="0">Todas</asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drpPais" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:ObjectDataSource ID="odsUbicacion" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="getUbicacion" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="drpPais" Name="clusterid" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <div class="form-item full">
                <label>
                    <asp:Label ID="lblBuscar" runat="server" Text="Buscar"></asp:Label></label>
                <asp:TextBox ID="txtBuscar" runat="server"></asp:TextBox>
            </div>
            <div class="controls">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
            </div>
        </asp:Panel>
    </div>
    <asp:MultiView ID="mvUsuarios" runat="server">
        <asp:View runat="server" ID="Busqueda">
            <asp:Panel ID="pnlArgentina" runat="server">
                <h3>
                    Argentina</h3>
                <asp:GridView ID="gvArgentina" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "buscadordepersonas.aspx?usuarioid=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("UBICACIONDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("DireccionDET")%>
                                                        </li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
                                                  <%--      <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>--%>
                                                        <li>Int:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicación" DataField="ubicaciondescripcion" />
                        <asp:BoundField HeaderText="Dirección" DataField="direcciondescripcion" />
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                                
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlChile" runat="server">
                <h3>
                    Chile</h3>
                <asp:GridView ID="gvChile" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "buscadordepersonas.aspx?UsuarioID=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
<%--                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>--%>
                                                        <li>Int:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlParaguay" runat="server">
                <h3>
                    Paraguay</h3>
                <asp:GridView ID="gvParaguay" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "buscadordepersonas.aspx?UsuarioID=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
<%--                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>--%>
                                                        <li>int:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlUruguay" runat="server">
                <h3>
                    Uruguay</h3>
                <asp:GridView ID="gvUruguay" runat="server" AutoGenerateColumns="False" OnRowDataBound="grid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Detalle">
                            <ItemTemplate>
                                <asp:HyperLink ID="lblnombreapellido" runat="server" NavigateUrl='<%# "buscadordepersonas.aspx?UsuarioID=" +Eval("usuarioid") %>'><%# Eval("Apellido")+", "+Eval("Nombre") %></asp:HyperLink>
                                <asp:Literal ID="ltjs" runat="server"></asp:Literal>
                                <asp:Panel ID="pnlUsuario" runat="server">
                                    <div class="userCard">
                                        <asp:HiddenField ID="hdUsuario" runat="server" Value='<%# Bind("usuarioid") %>' />
                                        <asp:FormView ID="frmUsuario" runat="server" DataSourceID="odsUsuario">
                                            <ItemTemplate>
                                                <div class="foto">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "../images/personas/"+Eval("usuariofoto")%>' />
                                                </div>
                                                <div class="info">
                                                    <ul>
                                                        <li>
                                                            <%# Eval("nombre")%>
                                                            <%# Eval("apellido")%></li>
                                                        <li>
                                                            <%# Eval("areadesc")%>
                                                        </li>
<%--                                                        <li>
                                                            <%# Eval("cargodet")%>
                                                        </li>--%>
                                                        <li>Int:
                                                            <%# Eval("interno")%>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </ItemTemplate>
                                        </asp:FormView>
                                        <asp:ObjectDataSource ID="odsUsuario" runat="server" OldValuesParameterFormatString="original_{0}"
                                            SelectMethod="getDataUsuario" TypeName="com.paginar.johnson.BL.ControllerUCHome">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="hdUsuario" Name="usuarioid" PropertyName="Value"
                                                    Type="Int32" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Ubicacion" DataField="ubicaciondescripcion" />
                        <asp:BoundField HeaderText="Interno" DataField="interno" />
                        <asp:TemplateField HeaderText="Interno IP">
                            <ItemTemplate>
                                <%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Prefijo IP" DataField="prefixip" />
                        <asp:BoundField HeaderText="Interno IP" DataField="internoip" />--%>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ProceseMail(Eval("Email")) %>'></asp:Label>                                
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Label ID="lblEmptyUsuarios" CssClass="messages msg-info" Text="No se encontraron Resultados"
                runat="server" Visible="false"></asp:Label>
        </asp:View>
        <asp:View runat="server" ID="Card">
            <div class="box resultados">
                <asp:FormView Width="100%" ID="FormViewUsuario" runat="server" DataKeyNames="UsuarioID">
                    <ItemTemplate>
                        <div class="container">
                            <div class="header">
                                <h4>
                                    <asp:Label ID="Label1" runat="server" CssClass="QEQ_Nombre" Text='<%# Eval("NombreCompleto") %>'></asp:Label></h4>
                            </div>
                            <div class="content">
                                <div class="pic">
                                    <asp:Image ID="Image2" ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "/ResizeImage.aspx?Image=~/images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif"   %>'
                                        runat="server" Width="120" Height="120" />
                                </div>
                                <div class="info">
                                    <ul>
                                        <li runat="server" id="CtrlUbicacion" visible='<%# !(IsEmpty(Eval("UBICACIONDET"))) %>'>
                                            <strong>Ubicación: </strong>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("UBICACIONDET") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlDireccion" visible='<%# !(IsEmpty(Eval("DireccionDET"))) %>'>
                                            <strong>Dirección: </strong>
                                            <asp:Label ID="SectorLabel" runat="server" Text='<%# Bind("DireccionDET") %>'></asp:Label></li>
                                        <li runat="server" id="CtrlArea" visible='<%# !(IsEmpty(Eval("AreaDESC"))) %>'>
                                            <strong>Área: </strong>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("AreaDESC") %>'></asp:Label></li>
                                        <%--<li runat="server" id="CtrlCargo" visible='<%# !(IsEmpty(Eval("CargoDET"))) %>'>
                                            <strong>Cargo: </strong>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("CargoDET") %>'></asp:Label></li>--%>
                                        <li runat="server" id="CtrlCumpleanos" visible='<%# (IsArgentina(Eval("Cluster"))) %>'>
                                            <strong>Cumpleaños: </strong>
                                            <asp:Label ID="Label6" runat="server" Text='<%# FormatearFecha(Eval("FHNacimiento")) %>'></asp:Label></li>
                                        <li runat="server" id="CtrlInterno" visible='<%# (!(IsEmpty(Eval("Interno"))))%>'><strong>
                                            Interno: </strong>
                                            <asp:Label ID="LabelInterno" runat="server" Text='<%# Bind("Interno") %>'></asp:Label></li>
                                        <li runat="server" id="Li1" visible='<%# (!(IsEmpty(Eval("InternoIP"))))%>'><strong>
                                            Interno IP: </strong>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("internoip").ToString() != "" ? Eval("prefixip").ToString()+"+"+Eval("internoip").ToString() : ""%>'></asp:Label>
                                        </li>
                                        <li class="last" runat="server" id="CtrlEmail" visible='<%# (!(IsEmpty(Eval("Email")))) %>'>
                                            <strong>Email: </strong>
                                            <asp:HyperLink NavigateUrl='<%# Bind("Email","mailto:{0}") %>' Text='<%# Bind("Email") %>'
                                                ID="HyperLink1" runat="server"></asp:HyperLink>
                                            <asp:HiddenField ID="HiddenFieldUbicacionID" runat="server" Value='<%# Eval("UbicacionID") %>' />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
                <div class="messages msg-alerta">
                    Para consultas sobre esta sección o su contenido, por favor contactar a <b>María del Carmen Aranguren</b>
                     
                </div>
            </div>
            <!--/resultados-->
            <asp:ObjectDataSource ID="ObjectDataSourceUsuarioDet" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetUsuarioByUsuarioID" TypeName="com.paginar.johnson.BL.ControllerUsuarios"
                OnSelecting="ObjectDataSourceUsuarioDet_Selecting">
                <SelectParameters>
                    <asp:QueryStringParameter Name="UsuarioID" QueryStringField="UsuarioID" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:View>
    </asp:MultiView>
</asp:Content>
