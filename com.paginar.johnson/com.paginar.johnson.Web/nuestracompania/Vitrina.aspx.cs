﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Data;
using System.IO;

namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class Vitrina : PageBase
    {


        private DataTable _dtLinks;
        public DataTable dtLinks
        {
            get
            {
                if (Session["dtLinks"] == null)
                {
                    _dtLinks = new DataTable();
                    _dtLinks.Columns.Add("path");
                    Session["dtLinks"] = _dtLinks;
                }
                return (DataTable)Session["dtLinks"];
            }

            set
            {
                Session["dtLinks"] = value;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Request["publicacionID"] != null)
                {
                    Editar(int.Parse(Request["publicacionid"].ToString()));
                }
                else
                {
                    Nuevo();
                    PublicacionesTabContainer.ActiveTab = TabBuscador;
                }

                BuscarTodos();
            }

            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();

        }



        private void Nuevo()
        {
            hdModo.Value = "Nuevo";
            Session["dtLinks"] = null;
            hdUsuarioId.Value = this.ObjectUsuario.usuarioid.ToString();
            lblUsuario.Text = this.ObjectUsuario.apellido + " " + this.ObjectUsuario.nombre;
            rbTipo.SelectedIndex = 0;
            rbEstado.SelectedIndex = 0;
        }


        protected void Editar(int publicacionid)
        {
            lblUsuario.Text = this.ObjectUsuario.apellido + " " + this.ObjectUsuario.nombre;
            hdModo.Value = "Editar";
            hdPublicacionID.Value = publicacionid.ToString();
            ControllerVitrina cv = new ControllerVitrina();
            DSVitrina.vitra_PublicacionDataTable dt = cv.getPublicacion(publicacionid);

            txtFecha.Text = string.Format("{0: dd/MM/yyyy}", dt.Rows[0]["fecha"]);
            txtFechaLogro.Text = string.Format("{0: dd/MM/yyyy}", dt.Rows[0]["fechalogro"]);
            txtTitulo.Text = dt.Rows[0]["titulo"].ToString();
            txtDescripcion.Text = dt.Rows[0]["descripcion"].ToString();
            txtLink.Text = dt.Rows[0]["link"].ToString();


            rbTipo.SelectedValue = dt.Rows[0]["tipoid"].ToString();

            if(dt.Rows[0]["estado"].ToString() =="True")
                rbEstado.SelectedIndex = 0;
            else
                rbEstado.SelectedIndex = 1;


            


            DSVitrina.vitra_LinkDataTable dl = cv.getLinksByID(publicacionid);
            dtLinks = dl;
            gvLinks.DataSource = dtLinks;
            gvLinks.DataBind();

            PublicacionesTabContainer.ActiveTab = TabPublicar;

        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ControllerVitrina cv = new ControllerVitrina();

                if (hdModo.Value == "Nuevo")
                {
                    cv.AgregarPublicacion(this.ObjectUsuario.usuarioid, int.Parse(rbTipo.SelectedItem.Value), DateTime.Parse(txtFecha.Text), DateTime.Parse(txtFechaLogro.Text), txtTitulo.Text, txtDescripcion.Text, bool.Parse(rbEstado.SelectedValue.ToString()), txtLink.Text, dtLinks);

                }

                if (hdModo.Value == "Editar")
                {
                    cv.ModificarPublicacion(int.Parse(hdPublicacionID.Value), this.ObjectUsuario.usuarioid, int.Parse(rbTipo.SelectedItem.Value), DateTime.Parse(txtFecha.Text), DateTime.Parse(txtFechaLogro.Text), txtTitulo.Text, txtDescripcion.Text, bool.Parse(rbEstado.SelectedValue.ToString()), txtLink.Text, dtLinks);
                }

                dtLinks = null;
                gvLinks.DataSource = dtLinks;
                gvLinks.DataBind();

                PublicacionesTabContainer.ActiveTab = TabMisPublicaciones;
                rptMisPublicaciones.DataBind();

                

                Limpiar();

                BuscarTodos();

            }

            
        }


        protected void rptLinks_ItemDataBound1(object sender, RepeaterItemEventArgs e)
        {
            HiddenField hd = (HiddenField)e.Item.FindControl("hdPath");
            HyperLink hpl = (HyperLink)e.Item.FindControl("hplPath");
            Image img = (Image)e.Item.FindControl("imgPath");

            string path = hd.Value;

            if (path.Contains(".jpg") || path.Contains(".png"))
            {
                img.ImageUrl = "~/nuestracompania/vitrina/" + path;
                img.Attributes.Add("onclick", "flyer('~/nuestracompania/vitrina/" + path + "');");
                img.Attributes.Add("style", "cursor: pointer");
                img.Visible = true;
                hpl.Visible = false;
            }
            else
            {
                img.Visible = false;
                hpl.Visible = true;
            }

        }


        protected void rptMisPublicaciones_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Editar")
            {
                Response.Redirect("Vitrina.aspx?publicacionID=" + e.CommandArgument);
            }
        }


        protected void imgAgregarLink_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {



                if (FULinks.HasFile)
                {
                    string filename = Path.GetFileNameWithoutExtension(FULinks.FileName) + DateTime.Now.Ticks.ToString() + Path.GetExtension(FULinks.FileName);
                    string strPath = MapPath("~/nuestracompania/vitrina/") + filename;
                    FULinks.SaveAs(@strPath);

                    DataRow dr = dtLinks.NewRow();
                    dr["path"] = filename;
                    if (hdModo.Value == "Editar") dr["publicacionid"] = 0;

                    dtLinks.Rows.Add(dr);

                    gvLinks.DataSource = dtLinks;
                    gvLinks.DataBind();

                }




            }
        }

        
        protected void gvLinks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Quitar")
            {
                string publicacionid = e.CommandArgument.ToString();

                DataRow[] row = dtLinks.Select("path='" + publicacionid.ToString() + "'");
                if (row.Length > 0)
                    row[0].Delete();

                gvLinks.DataSource = dtLinks;
                gvLinks.DataBind();

            }
        }


        protected void ButtonBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void Buscar()
        {
            ControllerVitrina cv = new ControllerVitrina();

            rptBuscador.DataSource = cv.getPublicaciones(int.Parse(drpTipoPublicacion.SelectedValue), 1, 0, true);
            rptBuscador.DataBind();

            PublicacionesTabContainer.ActiveTab = TabBuscador;
        }

        private void BuscarTodos()
        {
            ControllerVitrina cv = new ControllerVitrina();

            rptBuscador.DataSource = cv.getPublicaciones(0, 1, 0, true);
            rptBuscador.DataBind();

            //PublicacionesTabContainer.ActiveTab = TabBuscador;
        }


        private void Limpiar()
        {
            txtFecha.Text = "";
            txtFechaLogro.Text = "";
            txtLink.Text = "";
            txtTitulo.Text = "";
            txtDescripcion.Text = "";
            
        }


    }
}