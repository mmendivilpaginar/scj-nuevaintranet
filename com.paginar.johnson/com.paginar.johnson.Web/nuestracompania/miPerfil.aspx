﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="miPerfil.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.miPerfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            //select all the a tag with name equal to modal
            $('a[name=modal]').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                //Get the A tag
                var id = $(this).attr('href');

                //Get the screen height and width
                var maskHeight = $(document).height();
                var maskWidth = $(window).width();

                //Set heigth and width to mask to fill up the whole screen
                $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

                //transition effect		
                $('#mask').fadeIn(1000);
                $('#mask').fadeTo("slow", 0.8);

                //Get the window height and width
                var winH = $(window).height();
                var winW = $(window).width();

                //Set the popup window to center
                $(id).css('top', winH / 2 - $(id).height() / 2);
                $(id).css('left', winW / 2 - $(id).width() / 2);

                //transition effect
                $(id).fadeIn(300);

            });

            //if close button is clicked
            $('.window .close').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                $('#mask').hide();
                $('.window').hide();
            });

            //if mask is clicked
            $('#mask').click(function () {
                $(this).hide();
                $('.window').hide();
            });

        });

    </script>

    <h2>
        Mi perfil</h2>
    <p>
    </p>
    <%--<p>
        Próximamente podrás validar tu información personal desde la Intranet.</p>--%>
    
        <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
    
        <asp:HiddenField ID="HDUsuarioID" runat="server" Value="" />
        <asp:FormView Width="100%" ID="FormViewUsuario" runat="server"
            DataSourceID="odsUsuario" >
            <ItemTemplate>
                <div id="miPerfil" class="box resultados">
                    <div class="container">
                        <div class="header">
                            <h4><asp:Label ID="Label1" runat="server" CssClass="QEQ_Nombre" Text='<%# Eval("NOME") %>'></asp:Label></h4>
                        </div>
                        <div class="content">
                            <div class="pic">
                            </div>
                            <div class="info">
                                <ul>
                                    <%--<li><strong>Legajo: </strong><asp:Label ID="Label2" runat="server" Text='<%# Eval("CHAPA") %>'></asp:Label></li>--%>
                                    <li><strong>Legajo: </strong><asp:Label ID="Label2" runat="server" Text='<%# Eval("Legajowd") %>'></asp:Label></li>
                                    <li runat="server" id="CtrlCumpleanos"><strong>Fecha de Nacimiento: </strong><asp:Label ID="Label6" runat="server" Text='<%# FormatearFecha(Eval("DATA_NASC")) %>'></asp:Label></li>
                                    <li runat="server" id="CtrlInterno"><strong>Estado Civil: </strong><asp:Label ID="Label3" runat="server" Text='<%# Eval("ESTADOCIVIL")+"/a" %>'></asp:Label></li>
                                    <li><strong>Nacionalidad: </strong><asp:Label ID="Label4" runat="server" Text='<%# Eval("NACIONALIDAD") %>'></asp:Label></li>
                                    <li><strong>Domicilio: </strong><asp:Label ID="Label5" runat="server" Text='<%# Eval("RES_ENDEREC")+" "+Eval("RES_NUMERO") + piso(Eval("RES_COMPL").ToString())+ departamento(Eval("RES_DDD").ToString()) %>'></asp:Label></li>
                                    <li><strong>Localidad: </strong><asp:Label ID="Label7" runat="server" Text='<%# Eval("CIUDAD") %>'></asp:Label></li>
                                    <li><strong>Provincia: </strong><asp:Label ID="Label8" runat="server" Text='<%# Eval("PROVINCIA") %>'></asp:Label></li>
                                    <li><strong>Telefono Residencial: </strong><asp:Label ID="Label9" runat="server" Text='<%# Eval("RES_FONE") %>'></asp:Label></li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer">
                            <ul class="lista-vertP1">
                            <asp:Label ID="lblHijosLink" runat="server"></asp:Label>
                            </ul>
                            <ul class="lista-vertP">
                             <li class="conbulet" id="liModificarDomicilio" runat="server"><a class="conbulet" href="/ASPForm.aspx?url=servicios/form_CambioDomicilio.asp?FormularioID=7&ASP=form_CambioDomicilio.asp">
                            Modificar mi domicilio</a></li>                            
                            <li class="conbulet"><a class="miperfilmodifica conbulet" href="#dialog" name="modal">Imprimir declaración de domicilio y recorrido para la ART</a></li>
                            <li class="conbulet" id="liSolicitarLicencia" runat="server"><a class="conbulet" href="/ASPForm.aspx?url=servicios/form_OtrasLicencias.asp?FormularioID=13&ASP=form_OtrasLicencias.asp">
                            Solicitar licencia por mudanza</a></li>
                            </ul>
                        </div>
                    </div><!--/container-->
                </div><!---/box-->               

                <div id="boxes">
                    <div id="dialog" class="window">
                        <div class="header">
                            <h3>Modificación de Domicilio</h3>
                            <a href="#" class="close cerrarPerfil"><img src="../css/images/ico-cerrar.gif" alt="Cerrar" /></a>
                        </div>
                        <div class="content" >
                            <div class="messages msg-info">Te agradecemos imprimas y remitas ambos formularios completos a Administraci&oacute;n de Personal.</div>    
                            <ul>    
                                <li><a href="../nuestracompania/documents/Cambios+de+domicilio+ART.doc">Formulario Cambio de Domicilio</a></li>
                                <li><a href="../nuestracompania/documents/DECLARACION+DE+DOMICILIO.doc">Formulario Declaraci&oacute;n de Domicilio</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="dialogh" class="window" style="height:auto">
                        <div class="header">
                            <h3>Hijos</h3>
                            <a href="#" class="close"><img src="../css/images/ico-cerrar.gif" alt="Cerrar" /></a>
                        </div>
                        <div class="content">
                            <h4>Cantidad de Hijos/as <asp:Label ID="cantHijos" runat="server"></asp:Label></h4>
                            <div class="AspNet-GridView">
                                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ODSHijos">
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <th>Apellido y Nombre</th>
                                                <th>DNI</th>
                                                <th>Fecha Nac.</th>
                                                <th width="50">Edad</th>
                                            </tr>
                                    </HeaderTemplate>

                                    <ItemTemplate>
                                        <tr">
                                            <td><asp:Label ID="NOMELabel" runat="server" Text='<%# Eval("APELLIDO")+" "+Eval("NOME") %>' /></td>
                                            <td><asp:Label ID="NUM_DOCLabel" runat="server" Text='<%# " " + Eval("NUM_DOC") %>' /></td>
                                            <td><asp:Label ID="NASC_DATALabel" runat="server" Text='<%# " " + FechaCorta(Eval("NASC_DATA").ToString())+" " %>' /></td>
                                            <td><asp:Label ID="EDAD" runat="server" Text='<%# " " + CalcularEdad(Eval("NASC_DATA"))  %>' /></td>
                                        </tr>                                  
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div><!--/AspNet-GridView-->

                            <asp:ObjectDataSource ID="ODSHijos" runat="server" 
                                OldValuesParameterFormatString="original_{0}" 
                                SelectMethod="getHijosByUsuarioID" 
                                TypeName="com.paginar.johnson.BL.ControllerUsuarios">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="HDUsuarioID" Name="usuarioID" 
                                        PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            

                        </div>
                    </div>
                    <div id="mask"></div>
                </div>

            </ItemTemplate>
        </asp:FormView>
        <asp:ObjectDataSource ID="odsUsuario" runat="server"
            OldValuesParameterFormatString="original_{0}" SelectMethod="getPerfilById"
            TypeName="com.paginar.johnson.BL.ControllerUsuarios">
            <SelectParameters>
                <asp:ControlParameter ControlID="HDUsuarioID" Name="usrId" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    
</asp:Content>
