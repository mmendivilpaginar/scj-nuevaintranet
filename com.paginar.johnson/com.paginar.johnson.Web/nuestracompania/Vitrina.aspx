﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Vitrina.aspx.cs" Inherits="com.paginar.johnson.Web.nuestracompania.Vitrina" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

    <script type="text/javascript">
        function validateTipo(source, args) {
            var radGender = document.getElementById("<%= rbTipo.ClientID  %>");
            var elements = radGender.getElementsByTagName("input");
            var selected = false;

            for (var i = 0; i < elements.length; i++) {
                if (elements[i].checked == true) {
                    selected = true;
                    break;
                }
            }

            if (!selected) {
                args.isValid = false;
                return args.isValid;
            } else {
                args.isValid = true;
                return args.isvalid;
            }

        }
    </script>

    <h2>Vitrina Online</h2>



    <asp:TabContainer ID="PublicacionesTabContainer" runat="server" 
        ActiveTabIndex="1">
        <asp:TabPanel HeaderText="BUSCADOR" ID="TabBuscador" runat="server" TabIndex="0">
            <HeaderTemplate>
                BUSCADOR
            </HeaderTemplate>
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server" DefaultButton="ButtonBuscar">
                    <label>
                        Tipo de <span>Publicación</span>
                    </label>
                    <asp:DropDownList ID="drpTipoPublicacion" runat="server" DataSourceID="ODSTipo" DataTextField="Descripcion"
                        DataValueField="TipoID" AppendDataBoundItems="True">
                        <%--<asp:ListItem Value="-1" Text="Todos"></asp:ListItem>--%>
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="ODSTipo" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="getTipo" TypeName="com.paginar.johnson.BL.ControllerVitrina"></asp:ObjectDataSource>
                    <div class="controls">
                         <asp:Button ID="ButtonBuscar" runat="server" Text="Buscar" 
                         onclick="ButtonBuscar_Click" />
                    </div>

                   <asp:Repeater runat="server" ID="rptBuscador" >
                    <ItemTemplate>
                       <div class="userCard">
                        
                        <div class="foto">
                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>'  />
                         </div>

                          <div class="info">
                         <strong>PUBLICADO POR</strong><br />
                         <strong>
                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("fecha","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("Nombre") +" "+ Eval("Apellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                int:
                                <%# Eval("interno")%></strong>
                            <br />
                            <%# Eval("areadesc")%>,<br />
                            <%# Eval("DireccionDET")%>,
                            <%# Eval("UbicacionDET")%>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                          </div>
                        </div>

                        <div class="detailAviso">
                            <ul>
                                <li class="first"></li>
                                <li>
                                    <h3> <%# Eval("Titulo").ToString().ToUpper() %></h3>
                                </li>                                
                                <li>
                                     <a href='<%#  Eval("link").ToString() %>' target="_blank" >   <%#  Eval("link").ToString() %> </A>
                                </li>
                            </ul>
                            <div class="detailAviso-imagen">
                                    <asp:Repeater runat="server" ID="rptLinks" DataSourceID="ODSLinks" OnItemDataBound="rptLinks_ItemDataBound1">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="HDPath" Value='<%# Eval("path").ToString() %>' />
                                            <asp:HyperLink ID="hplPath" runat="server" NavigateUrl='<%# "~/nuestracompania/vitrina/"+Eval("path") %>'
                                                Target="_blank"  > <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                            
                                            <asp:Image ID="imgPath" runat="server"  Width="150px"/>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                      <asp:HiddenField  runat="server" ID="hdPublicacionID" Value='<%# Eval("publicacionid") %>' />

                                    <asp:ObjectDataSource ID="ODSLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="getLinksByID" TypeName="com.paginar.johnson.BL.ControllerVitrina">
                                        <SelectParameters>                                            
                                            <asp:ControlParameter ControlID="hdPublicacionID" Name="publicacionid" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                            </div>
                            <div class="detailAviso-texto">
                            <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("descripcion").ToString().Replace("\r\n","<br>") %>' />
                        </div>
                        </div>



                    </ItemTemplate>


                    <FooterTemplate>

                     <asp:Label ID="lblEmpty" runat="server" Visible='<%#bool.Parse((rptBuscador.Items.Count==0).ToString())%>'
                     CssClass="messages msg-info"  Text="La búsqueda no produjo resultados para los parámetros indicados."></asp:Label>

                    </FooterTemplate>


                </asp:Repeater>



                </asp:Panel>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Publicar" ID="TabPublicar" runat="server" TabIndex="1">
            <ContentTemplate>
                <div class="block">
                    <div class="col-left">
                        <div>
               <asp:HiddenField runat="server" ID="hdModo" />
               <asp:HiddenField runat="server" ID="hdPublicacionID" />
                <label>
                                Usuario:
                </label>
                <asp:Label runat="server" ID="lblUsuario" />
                        </div>
                        <div>
                            <label>Tipo de Publicación:</label>
                            <asp:RadioButtonList runat="server" ID="rbTipo" DataSourceID="ODSTipo2" DataTextField="Descripcion"  
                            DataValueField="TipoId">
                            </asp:RadioButtonList>
                            <asp:ObjectDataSource ID="ODSTipo2" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="getTipo" TypeName="com.paginar.johnson.BL.ControllerVitrina"></asp:ObjectDataSource>

                            <asp:CustomValidator runat="server" ValidateEmptyText="True" 
                            ClientValidationFunction="validateTipo" ValidationGroup="Grabar"
                            ID="cvTipo"  Font-Bold="True" Font-Size="Medium" ErrorMessage="*"
                            ControlToValidate="rbTipo" Text="*"></asp:CustomValidator>
                        </div>
                    </div>

                    <div class="col-right">
                        <div>
                            <label>F. Publicación:</label>
                   <asp:TextBox ID="txtFecha" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" PopupButtonID="IMGCalen1"
                    TargetControlID="txtFecha" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtFecha"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Grabar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"
                    ValidationGroup="Grabar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                    ControlToValidate="txtFecha" SetFocusOnError="True">*</asp:RangeValidator>

                           <asp:RequiredFieldValidator ID="rfFecha"  ErrorMessage="Ingrese una Fecha"
                    runat="server" ControlToValidate="txtFecha" 
                    ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                        </div>

                        <div>
                <label>
                                F. del Recuerdo/Logro/Reconocimiento:</label>
                <asp:TextBox ID="txtFechaLogro" runat="server" SkinID="form-date" CausesValidation="True"></asp:TextBox>
                <asp:ImageButton ID="IMGCalen2" runat="server" ImageUrl="~/images/Calendar.png" />
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" PopupButtonID="IMGCalen2"
                    TargetControlID="txtFechaLogro" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFechaLogro"
                    ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy."
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="Grabar" Display="Dynamic">*</asp:CompareValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="La fecha desde debe ser mayor a 01/01/1900"
                    ValidationGroup="Grabar" MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date"
                    ControlToValidate="txtFechaLogro" SetFocusOnError="True">*</asp:RangeValidator>

                            <asp:RequiredFieldValidator ID="rfFechaLogo"  ErrorMessage="Ingrese una Fecha de Logro"
                    runat="server" ControlToValidate="txtFechaLogro" 
                    ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <div class="block centerBlock">
                    <div>
                        <label>Título:</label>
                <asp:TextBox runat="server" ID="txtTitulo" />
                        <asp:RequiredFieldValidator ID="rfTitulo" runat="server" ControlToValidate="txtTitulo" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <label>Descripción:</label>
                        <asp:TextBox runat="server" ID="txtDescripcion" Height="55px" TextMode="MultiLine" Width="335px" />
                        <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" ControlToValidate="txtDescripcion" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <label>Link:</label>
                        <asp:TextBox runat="server" ID="txtLink" Width="335px" />
                        <%--<asp:RequiredFieldValidator ID="rfLink" runat="server" ControlToValidate="txtLink" ErrorMessage="*" ValidationGroup="Grabar"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div>
                        <label>Adjuntar Archivo:</label>
                <asp:FileUpload ID="FULinks" runat="server" />
                        <span>(Hasta 5 MB)</span>
                        <asp:ImageButton ID="imgAgregarLink" runat="server" ImageUrl="~/images/verMas.png" OnClick="imgAgregarLink_Click" ValidationGroup="links" ToolTip="Agregar Archivo" />
                        <asp:GridView ID="gvLinks" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnRowCommand="gvLinks_RowCommand" Width="50px" GridLines="None" CssClass="mt-10">
                <Columns>
                <asp:BoundField DataField="path" />
                <asp:TemplateField  >
                <ItemTemplate  >
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete.png"
                CommandName="Quitar" CommandArgument='<%# Eval("path") %>' />
                </ItemTemplate>
                <ItemStyle Width="20px" />
                </asp:TemplateField>
                </Columns>
                </asp:GridView>
                    </div>

                    <div>
                        <label>Estado:</label>
                <asp:RadioButtonList runat="server" ID="rbEstado">
                    <asp:ListItem Text="Habilitado" Value="True" />
                    <asp:ListItem Text="Deshabilitado" Value="False" />
                </asp:RadioButtonList>
                    </div>
                    
                <div class="controls">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar"
                        ValidationGroup="Grabar" onclick="btnGuardar_Click" />
                    <asp:Button ID="btnCancelar" runat="server" CausesValidation="False" Text="Cancelar" />
                </div>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Mis Publicaciones" ID="TabMisPublicaciones" runat="server"
            TabIndex="2">
            <ContentTemplate>
                
                <asp:HiddenField  runat="server"  ID="hdUsuarioId" />
                <asp:Repeater runat="server" ID="rptMisPublicaciones" DataSourceID="ODSMisPublicaciones" OnItemCommand="rptMisPublicaciones_ItemCommand" >
                    <ItemTemplate>
                       <div class="userCard">
                        
                        <div class="foto">
                            <img src='<%# "../images/personas/"+ Eval("usuariofoto")%>'  />
                         </div>

                          <div class="info">
                         <strong>PUBLICADO POR</strong><br />
                         <strong>
                                <asp:Label ID="FHAltaLabel" runat="server" Text='<%# Eval("fecha","{0:dd/MM/yyyy}") %>' />&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<%# Eval("Nombre") +" "+ Eval("Apellido")%>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                                int:
                                <%# Eval("interno")%></strong>
                            <br />
                            <%# Eval("areadesc")%>,<br />
                            <%# Eval("DireccionDET")%>,
                            <%# Eval("UbicacionDET")%>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/images/banderas/"+Eval("bandera") %>' />
                          </div>
                        </div>

                        <div class="detailAviso">
                            <ul>
                                <li class="first"></li>
                                <li>
                                    <h3>
                                        <asp:LinkButton ID="TituloLinkButton" runat="server" CommandArgument='<%# Eval("publicacionid") %>'  
                                            CommandName="Editar" Text='<%# Eval("Titulo").ToString().ToUpper() %>'></asp:LinkButton></h3>
                                </li>                                
                                <li>
                                     <a href='<%#  Eval("link").ToString() %>' target="_blank" >   <%#  Eval("link").ToString() %> </A>
                                </li>
                            </ul>
                            <div class="detailAviso-imagen">
                                    <asp:Repeater runat="server" ID="rptLinks" DataSourceID="ODSLinks" OnItemDataBound="rptLinks_ItemDataBound1">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="HDPath" Value='<%# Eval("path").ToString() %>' />
                                            <asp:HyperLink ID="hplPath" runat="server" NavigateUrl='<%# "~/nuestracompania/vitrina/"+Eval("path") %>'
                                                Target="_blank"  > <img src="/css/images/attach.png" title="Ver Adjunto" border="0" /> Ver Adjunto</asp:HyperLink>
                                            
                                            <asp:Image ID="imgPath" runat="server"  Width="150px"/>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                      <asp:HiddenField  runat="server" ID="hdPublicacionID" Value='<%# Eval("publicacionid") %>' />

                                    <asp:ObjectDataSource ID="ODSLinks" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="getLinksByID" TypeName="com.paginar.johnson.BL.ControllerVitrina">
                                        <SelectParameters>                                            
                                            <asp:ControlParameter ControlID="hdPublicacionID" Name="publicacionid" PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                            </div>
                            <div class="detailAviso-texto">
                            <asp:Label ID="DescripLabel" runat="server" Text='<%# Eval("descripcion").ToString().Replace("\r\n","<br>") %>' />
                        </div>
                        </div>



                    </ItemTemplate>
                </asp:Repeater>

                <asp:ObjectDataSource ID="ODSMisPublicaciones" runat="server" 
                    OldValuesParameterFormatString="original_{0}" SelectMethod="getPublicaciones" 
                    TypeName="com.paginar.johnson.BL.ControllerVitrina">
                    <SelectParameters>
                        <asp:Parameter Name="tipoid" Type="Int32" />
                        <asp:Parameter Name="clusterid" Type="Int32" />
                        <asp:ControlParameter ControlID="hdUsuarioId" Name="usuarioid" 
                            PropertyName="Value" Type="Int32" />
                        <asp:Parameter Name="estado" Type="Boolean" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>





</asp:Content>
