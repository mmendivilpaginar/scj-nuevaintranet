﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="politicasdeempresa.aspx.cs"
    Inherits="com.paginar.johnson.Web.nuestracompania.politicasdeempresa" MasterPageFile="~/MasterPages/TwoSidebars.master" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<script type="text/javascript">

    $(function () { $('a.load-local').cluetip({local: true,showTitle: false, hoverIntent: false, stycky: true, mouseOutClose: false});    });

    $(document).ready(function () {
            var elem = document.getElementById('<%= TreeViewPoliticas.ClientID %>' + '_SelectedNode');
            if (elem.value != null && elem.value.length != 0) {
                var node = document.getElementById(elem.value);
                if (node != null) {
                    node.scrollIntoView(true);
                }
            }

            $('#<%= TreeViewPoliticas.ClientID %> a[title] ')
                    .each(function(){ 
                                       $(this).attr('rel', 'PoliticaEmpresa');
                                       $(this).cluetip({ local: true, showTitle: false, hoverIntent: false, stycky: true, mouseOutClose: false, clickThrough: true });
                                    }
                                    );


    });

</script>
    <script src="../js/init.js" type="text/javascript"></script>
   
    <h2>
        Políticas de Empresa</h2>
    <asp:TreeView ID="TreeViewPoliticas" runat="server" DataSourceID="RelationalSystemDataSource1" OnSelectedNodeChanged="TreeViewContenido_SelectedNodeChanged"  
                   ontreenodedatabound="TreeViewPoliticas_TreeNodeDataBound">
        <DataBindings>
            <asp:TreeNodeBinding TextField="Name" />
        </DataBindings>
    </asp:TreeView>
    <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="false" />
    
    <span style=" color:#666666; font-size:11px;">* Pasar el mouse por el ítem para ver su descripción </span>
    <br />
    <br />
    <br />
    <a name="contenido-abajo"></a>
    <div style=" text-align:center">
    <asp:UpdateProgress ID="upgcontenido" runat="server"  DisplayAfter="1">
        <ProgressTemplate>  
            <asp:Label ID="LabelCargando" runat="server" Text="cargando"></asp:Label>                  
            <img src="../images/preloader.gif" class="preloader" alt="cargando..." />
        </ProgressTemplate>
    </asp:UpdateProgress> 
    </div>   
    <asp:UpdatePanel ID="updContenido" runat="server">
        <ContentTemplate>           
            <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
            <div id="divContenido">
                <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                    <ItemTemplate>                        
                        <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="TreeViewPoliticas" EventName="SelectedNodeChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <br />

      
</asp:Content>
