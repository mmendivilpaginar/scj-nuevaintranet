﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class buscadordepersonas : PageBase
    {
        enum Cluster
        {
            Argentina=1,
            Chile=2,
            Uruguay=3,
            Paraguay=4
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if(Request["Busqueda"]!=null)
                {
                    ControllerUCHome cc = new ControllerUCHome();
                    string busqueda= Request["Busqueda"].ToString();

                    gvArgentina.DataSource = cc.GetBusquedaUsuarios(busqueda,(int) (Cluster.Argentina), "");
                    gvArgentina.DataBind();
                    if (gvArgentina.Rows.Count == 0) pnlArgentina.Visible = false;
                    
                    gvChile.DataSource = cc.GetBusquedaUsuarios(busqueda, (int) (Cluster.Chile), "");
                    gvChile.DataBind();
                    if (gvChile.Rows.Count == 0) pnlChile.Visible = false;

                    gvUruguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Uruguay), "");
                    gvUruguay.DataBind();
                    if (gvUruguay.Rows.Count == 0) pnlUruguay.Visible = false;

                    gvParaguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Paraguay), "");
                    gvParaguay.DataBind();
                    if (gvParaguay.Rows.Count == 0) pnlParaguay.Visible = false;

                    mvUsuarios.ActiveViewIndex = 0;
                }

                if (Request["UsuarioID"] != null)
                {

                    ControllerUsuarios cu = new ControllerUsuarios();
                    FormViewUsuario.DataSource = cu.GetUsuarioByUsuarioID(int.Parse(Request["UsuarioID"]));                        
                    FormViewUsuario.DataBind();

                    mvUsuarios.ActiveViewIndex = 1;
                }

               
            }
        }

        protected string ProceseMail(object mail)
        {
            string mailto = string.Empty;
            if (mail.ToString().Length > 0)
                mailto = "<a href='mailto:" + mail.ToString().Trim() + "'>" + mail.ToString().Trim() + "</a>";
            return mailto;
        }

        protected void Buscar()
        {
            if (Page.IsValid)
            {
                string ubicacionid = "";
                string busqueda= "";

                if (txtBuscar.Text != "") busqueda = txtBuscar.Text;
                if (drpUbicacion.SelectedIndex > 0) ubicacionid = drpUbicacion.SelectedValue; 

                ControllerUCHome cc = new ControllerUCHome();

                pnlArgentina.Visible = false;
                pnlChile.Visible = false;
                pnlParaguay.Visible = false;
                pnlUruguay.Visible = false;
                gvArgentina.DataSource=null;
                gvArgentina.DataBind();
                gvChile.DataSource = null;
                gvChile.DataBind();
                gvParaguay.DataSource = null;
                gvParaguay.DataBind();
                gvUruguay.DataSource = null;
                gvUruguay.DataBind();


                if (drpPais.SelectedValue == "1" || drpPais.SelectedValue == "0")
                {
                    gvArgentina.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Argentina), ubicacionid);
                    gvArgentina.DataBind();
                    if (gvArgentina.Rows.Count > 0) pnlArgentina.Visible = true;
                }

                if (drpPais.SelectedValue == "2" || drpPais.SelectedValue == "0")
                {
                    gvChile.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Chile), ubicacionid);
                    gvChile.DataBind();
                    if (gvChile.Rows.Count > 0) pnlChile.Visible = true;
                }

                if (drpPais.SelectedValue == "3" || drpPais.SelectedValue == "0")
                {
                    gvUruguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Uruguay), ubicacionid);
                    gvUruguay.DataBind();
                    if (gvUruguay.Rows.Count > 0) pnlUruguay.Visible = true;
                }

                if (drpPais.SelectedValue == "4" || drpPais.SelectedValue == "0")
                {
                    gvParaguay.DataSource = cc.GetBusquedaUsuarios(busqueda, (int)(Cluster.Paraguay), ubicacionid);
                    gvParaguay.DataBind();
                    if (gvParaguay.Rows.Count > 0) pnlParaguay.Visible = true;
                }

                if (gvArgentina.Rows.Count == 0 && gvChile.Rows.Count == 0 && gvUruguay.Rows.Count == 0 && gvParaguay.Rows.Count == 0)
                    lblEmptyUsuarios.Visible = true;
                else
                    lblEmptyUsuarios.Visible = false;

                mvUsuarios.ActiveViewIndex = 0;



            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HyperLink lblnombreapellido = (HyperLink)e.Row.FindControl("lblnombreapellido");                
                Panel pnl = (Panel)e.Row.FindControl("pnlUsuario");
                pnl.Attributes.Add("style", "display:none");
                Literal ltjs = (Literal)e.Row.FindControl("ltjs");
                ltjs.Text = GetToolTipFunction(lblnombreapellido.ClientID, pnl.ClientID);
            }
        }


        private string GetToolTipFunction(string imgbtnID, string pnlID)
        {
            string script = "<script type=\"text/javascript\"> $(function() { $(\"#" + imgbtnID + "\").tooltip({  ";
            script = script + " bodyHandler: function() {  ";
            script = script + "return $(\"#" + pnlID + "\").html(); ";
            script = script + "},top: -15, left: 5, showURL: false }); }); </script>";
            return script;
        }

        protected void drpPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpUbicacion.Items.Clear();
            ListItem li = new ListItem();
            li.Text = "Todos";
            li.Value = "0";
            drpUbicacion.Items.Add(li);
            drpUbicacion.DataBind();
        }


        protected bool IsEmpty(object cadena)
        {
            if (cadena != null)
            {

                return string.IsNullOrEmpty(cadena.ToString().Trim());
            }
            return true;
        }

        protected bool IsArgentina(object Cluster)
        {
            int cluster = int.Parse(Convert.ToString(Cluster));
            return (cluster == 1);
        }

        public string FormatearFecha(object fecha)
        {

            if (fecha != null)
            {
                DateTime FechaAux = DateTime.MinValue;
                if (DateTime.TryParse(Convert.ToString(fecha), out FechaAux))
                {
                    return string.Format("{0:dd} de {1:MMMM}", FechaAux, FechaAux);
                }
            }
            return string.Empty;


        }

        protected bool IsEqual(object cadena, string otracadena)
        {
            if (cadena != null)
            {
                return (Convert.ToString(cadena) == otracadena);
            }
            return false;
        }

        protected void ObjectDataSourceUsuarioDet_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.Cancel = string.IsNullOrEmpty(Request.QueryString["UsuarioID"]);
        }

    }
}