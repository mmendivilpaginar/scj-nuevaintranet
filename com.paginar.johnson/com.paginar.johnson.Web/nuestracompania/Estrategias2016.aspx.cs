﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;


namespace com.paginar.johnson.Web.nuestracompania
{
    public partial class Estrategias2016 : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ControllerNoticias CN = new ControllerNoticias();

               // ucCategoriaNoticia1.infInfoDt = CN.Repositorio.AdapterDSNoticias.infInfo.GetInfoCluster(this.ObjectUsuario.clusteridactual, 92);
               // ucCategoriaNoticia1.CargarDatos();
            
            ControllerContenido cc = new ControllerContenido();
            RelationalSystemDataSourceEstrategias2016.Path = cc.Content_TipoGetIdrootByClusterId("Estrategia 2016", this.ObjectUsuario.clusteridactual).ToString();

            RelationalSystemDataSourceEstrategias2016.DataBind();
            TreeViewEstrategias2016.DataBind();

            RelationalSystemDataSourceEstrategias2016_2.Path = cc.Content_TipoGetIdrootByClusterId("Estrategia 2016 2", this.ObjectUsuario.clusteridactual).ToString();

            RelationalSystemDataSourceEstrategias2016_2.DataBind();
            TreeViewEstrategias2016_2.DataBind();

            TreeViewEstrategias2016_2.CollapseAll();
            TreeViewEstrategias2016.CollapseAll();
            }

        }

        protected void TreeViewEstrategias2016_SelectedNodeChanged(object sender, EventArgs e)
        {

        }

        protected void TreeViewEstrategias2016_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
        {
            int ItemID= int.Parse(e.Node.DataPath);
            string contenidoStr;
            ControllerContenido CC = new ControllerContenido();
            DsContenido.Content_ItemsDataTable CIDT = CC.GetContentById(ItemID);
            DsContenido.Content_ItemsRow ru = (DsContenido.Content_ItemsRow)CIDT.Rows[0];
            contenidoStr = ru.Contenido;

            if (contenidoStr.Length <= 3)
                return;
            
            if (contenidoStr.Substring(contenidoStr.Length - 3) == "pps" || contenidoStr.Substring(contenidoStr.Length - 3) == "jpg" || contenidoStr.Substring(contenidoStr.Length - 3) == "ppt" || contenidoStr.Substring(contenidoStr.Length - 3) == "doc" || contenidoStr.Substring(contenidoStr.Length - 3) == "pdf" || contenidoStr.Substring(contenidoStr.Length - 3) == "mp3" || contenidoStr.Substring(0, 4) == "http")
            {
                e.Node.NavigateUrl = ru.Contenido;
                e.Node.Target = "_blank";
            }

            if (contenidoStr == "&nbsp;")
                e.Node.SelectAction = TreeNodeSelectAction.Expand;
            else
                e.Node.SelectAction = TreeNodeSelectAction.SelectExpand;

        }
    }
}