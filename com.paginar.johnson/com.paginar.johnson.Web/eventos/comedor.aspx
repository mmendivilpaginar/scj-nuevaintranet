﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="comedor.aspx.cs" Inherits="com.paginar.johnson.Web.eventos.comedor" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
  <h2>Comedor</h2> 
  
    <div class="box formulario">
        <div class="form-item leftHalf"> 
            <label>Planta:</label>
            <asp:HiddenField ID="hdClusterID" runat="server" />
            <asp:DropDownList ID="drpUbicacion" runat="server" DataSourceID="odsUbicacion" 
                DataTextField="Descripcion" DataValueField="UbicacionID" 
                AppendDataBoundItems="True">
            <asp:ListItem>Todas</asp:ListItem>
            </asp:DropDownList>
            <asp:ObjectDataSource ID="odsUbicacion" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getUbicacion" 
                TypeName="com.paginar.johnson.BL.ControllerUCHome">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hdClusterID" Name="clusterid" 
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>

        <div class="form-item rightHalf">
            <label>Fecha:</label>
            <asp:TextBox  SkinID="form-date" ID="txtFecha" runat="server"></asp:TextBox>
            <asp:ImageButton ID="IMGCalen1" runat="server" ImageUrl="~/images/Calendar.png" />
            <asp:CalendarExtender ID="ceFecha" runat="server"
                Enabled="True" PopupButtonID="IMGCalen1" TargetControlID="txtFecha" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <asp:CompareValidator ID="cvFecha" runat="server" Display="Dynamic" 
                ControlToValidate="txtFecha" 
                ErrorMessage="El formato de la fecha ingresada en Desde es incorrecto. El formato de fecha es dd/mm/yyyy." 
                Operator="DataTypeCheck" Type="Date" ValidationGroup="Buscar">*</asp:CompareValidator>


                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="La fecha hasta debe ser mayor a 01/01/1900"  ValidationGroup="Buscar" 
                    MaximumValue="1/1/2100" MinimumValue="1/1/1900" Type="Date" 
                    ControlToValidate="txtFecha" SetFocusOnError="True">*</asp:RangeValidator>

            <br/>
            <asp:RequiredFieldValidator ID="rfFecha" runat="server" Display="Dynamic"
                ErrorMessage="Ingrese una Fecha" ControlToValidate="txtFecha" 
                ValidationGroup="Buscar"></asp:RequiredFieldValidator>
            
        </div>

        <div class="controls">
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" onclick="btnBuscar_Click" ValidationGroup="Buscar" />
        </div>
    </div><!--/formulario-->

    <div class="box resultados">
       
        <asp:DataList ID="dtUbicaciones" runat="server" RepeatLayout="Flow" onitemdatabound="dtUbicaciones_ItemDataBound" onitemcommand="dtUbicaciones_ItemCommand">
            <ItemTemplate>        
              <h4><asp:Label ID="lbldescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label></h4>
              <asp:HiddenField ID="hdUbicacionID" runat="server"  Value='<%# Bind("ubicacionid") %>'/>
                <asp:FormView ID="frmComedor" runat="server">
                    <ItemTemplate>     
                        <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                            <div class="container">
			                    <div class="header">
                                    <asp:Label ID="lblFecha" runat="server" Text='<%# Eval("Fecha","{0: dd/MM/yyyy}") %>' /> 
                                </div>
                                <div class="content">
                                    <ul>
                                        <li> <%# Eval("Entrada").ToString()!=""? "<strong>Entrada:</strong> "+Eval("Entrada") : "" %>  </li>
                                        <li> <%# Eval("PPrincipal").ToString() != "" ? "<strong>Plato Principal:</strong> " + Eval("PPrincipal") : ""%>  </li>                                                    
                                        <li> <%# Eval("Postre").ToString() != "" ? "<strong>Postre:</strong> " + Eval("Postre") : ""%>  </li>
                                        <li> <%# Eval("Cena").ToString() != "" ? "<strong>Cena:</strong> " + Eval("Cena") : ""%>  </li>                                                    
                                        <li> <%# Eval("Express").ToString() != "" ? "<strong>Menú Express:</strong> " + Eval("Express") : ""%>  </li>                                                    
                                        <li> <%# Eval("Vegetariano").ToString() != "" ? "<strong>Menú Vegetariano:</strong> " + Eval("Vegetariano") : ""%>  </li>                                                                                                        
                                    
                                        <li> <%# Eval("Parrillas").ToString() != "" ? "<strong>Menú Parrillas:</strong> " + Eval("Parrillas") : ""%>  </li>
                                        <li> <%# Eval("Pastas").ToString() != "" ? "<strong>Menú Pastas:</strong> " + Eval("Pastas") : ""%>  </li>
                                    
                                        <li> <%# Eval("Dieta").ToString() != "" ? "<strong>Dieta:</strong> " + Eval("Dieta") : ""%>  </li>                                                                                                        
                                        <li> <%# Eval("Oficina").ToString() != "" ? "<strong>Menú Oficina:</strong> " + Eval("Oficina"): ""%>  </li>                                                                                                                                                            
                                    </ul>   
                                </div>
			                    <div class="footer">
                                    <ul>
                                        <li class="first">
                                            <asp:Label ID="lblConComents"  runat="server"></asp:Label>                               
                                            <asp:ImageButton ID="imgComment" runat="server"     ImageUrl="~/css/images/ico-comments.gif"  ToolTip="Ver Comentarios" PostBackUrl='<%#  "~/eventos/infocomedor.aspx?ID=" +Eval("ID")+"#comentarios-abajo" %>'  />
                                        </li>
                                        <li>
                                            <asp:UpdatePanel ID="updUnLike" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>          
                                                    <asp:Label ID="lblUnLikeCount" runat="server"></asp:Label>                                                                          
                                                    <asp:ImageButton ID="imgUnLike" runat="server" ImageUrl="~/css/images/ico-unlike.gif" CommandName="unlike" CommandArgument='<%# Bind("ID") %>'  />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </li>
                                        <li class="last">               
                                            <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>          
                                                    <asp:Label ID="lblLikeCount" runat="server"></asp:Label>                   
                                                    <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif" CommandName="like" CommandArgument='<%# Bind("ID") %>'  />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </li>
                                    </ul>   
                                </div>                                          
                            </div>                        
                        </ItemTemplate>

                </asp:FormView>

                <asp:Label ID="lblEmpty" Text="No se encontraron Resultados" runat="server" CssClass="messages msg-info"></asp:Label>
            </ItemTemplate>
            
         

        </asp:DataList>
    
    </div><!--/resultados-->
 </asp:Content>
