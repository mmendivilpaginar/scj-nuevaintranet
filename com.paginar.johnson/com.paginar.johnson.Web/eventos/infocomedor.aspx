﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master" AutoEventWireup="true" CodeBehind="infocomedor.aspx.cs" Inherits="com.paginar.johnson.Web.eventos.infocomedor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>Comedor</h2> 

    <div class="box resultados">
        <div class="container">
            
            <asp:HiddenField ID="hdID" runat="server" />
            <asp:FormView ID="frmComedor" runat="server" DataSourceID="odsComedor" 
            DataKeyNames="ID">
                <ItemTemplate>
                    <asp:HiddenField ID="hdUbicacionID" runat="server" Value='<%#  Eval("ubicacionid")%>' />
                    <h3><asp:Label ID="lblUbicacion" runat="server" Text='<%#  Eval("descripcionubicacion")%>'></asp:Label></h3>
                    <asp:ObjectDataSource ID="odsUbicacion" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="getUbicacionByID" 
                        TypeName="com.paginar.johnson.BL.ControllerUCHome">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="hdUbicacionID" Name="ubicacionid" 
                                PropertyName="Value" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
            
                
                    <div class="header">
                        <asp:Label ID="lblFecha" runat="server" Text='<%# Eval("Fecha","{0: dd/MM/yyyy}") %>'> </asp:Label>
                    </div>
                    <div class="content">
                        <ul>
                            <li> <%# Eval("Entrada").ToString()!=""? ">Entrada: "+Eval("Entrada") : "" %>  </li>
                            <li> <%# Eval("PPrincipal").ToString() != "" ? ">Plato Principal: " + Eval("PPrincipal") : ""%>  </li>                                                    
                            <li> <%# Eval("Postre").ToString() != "" ? ">Postre: " + Eval("Postre") : ""%>  </li>
                            <li> <%# Eval("Cena").ToString() != "" ? ">Cena: " + Eval("Cena") : ""%>  </li>                                                    
                            <li> <%# Eval("Express").ToString() != "" ? ">Menú Express: " + Eval("Express") : ""%>  </li>                                                    
                            <li> <%# Eval("Vegetariano").ToString() != "" ? ">Menú Vegetariano: " + Eval("Vegetariano") : ""%>  </li>                                                                                                        
                            <li> <%# Eval("Dieta").ToString() != "" ? ">Dieta: " + Eval("Dieta") : ""%>  </li>                                                                                                        
                            <li> <%# Eval("Oficina").ToString() != "" ? ">Menú Oficina: " + Eval("Oficina"): ""%>  </li>                                                                                                                                                            
                        </ul>   
                    </div>
                </ItemTemplate>
            </asp:FormView>
            <asp:ObjectDataSource ID="odsComedor" runat="server" 
            OldValuesParameterFormatString="original_{0}" SelectMethod="getComedorByID" 
            TypeName="com.paginar.johnson.BL.ControllerUCHome">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hdID" Name="id" PropertyName="Value" 
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <div class="footer">
                <ul>
                    <li>
                        <asp:UpdatePanel ID="updUnLike" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>          
                                <asp:Label ID="lblUnLikeCount" runat="server"></asp:Label>                   
                                <asp:ImageButton ID="imgUnLike" runat="server" ImageUrl="~/css/images/ico-unlike.gif"  CommandName="unlike"  onclick="imgUnLike_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </li>
                    <li>               
                        <asp:UpdatePanel ID="updLike" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>          
                                <asp:Label ID="lblLikeCount" runat="server"></asp:Label>                   
                                <asp:ImageButton ID="imgLike" runat="server" ImageUrl="~/css/images/ico-like.gif" CommandName="like" onclick="imgLike_Click"  />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="imgLike" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </li>
                </ul>            
            </div>

        </div>    
    </div><!--/resultados-->
    


   <div class="box comentarios">
        <asp:UpdatePanel ID="updComments" runat="server"  UpdateMode="Conditional">
            <ContentTemplate>
                <a name="comentarios-abajo"></a>
                
                <div class="lista-comentarios">
                    <asp:DataList ID="dtComments" runat="server" DataKeyField="comentarioID" DataSourceID="odsComments" RepeatLayout="Flow">
                        <ItemTemplate>
                            <div class="comentario">
                                <div class="pic">
                                    <asp:Image ID="Image2" runat="server"   ImageUrl='<%# Eval("UsuarioFoto").ToString()!=""?  "../images/personas/"+Eval("UsuarioFoto") : "../images/personas/SinImagen.gif"  %>' />
                                </div>
                                <div class="content">
                                    <p>Fecha:<asp:Label ID="FechaLabel" runat="server" Text='<%# Eval("Fecha","{0:dd/MM/yyyy}") %>' /></p>
                                    <p class="hide"><asp:Label ID="lblNombreCompleto" runat="server" Text='<%# Eval("nombrecompleto") %>' /></p>
                                    <p>Comentario:<asp:Label ID="comentariosLabel" runat="server"  Text='<%# Eval("comentarios") %>' /></p>
                                </div>                        
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>

                <asp:ObjectDataSource ID="odsComments" runat="server" 
                        OldValuesParameterFormatString="original_{0}" SelectMethod="getComments" 
                        TypeName="com.paginar.johnson.BL.ControllerUCHome" >   
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdID" Name="id" PropertyName="Value" 
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
                <div class="usuario-comentar">
                    <asp:Image ID="imgUsuario" runat="server" Height="50px" Width="50px" CssClass="hide" />
                    <div class="form-item">
                        <label>Descripción:</label>
                        <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtComentario" ValidationGroup="Comentar"></asp:RequiredFieldValidator>
                    </div>
                    <div class="controls">
                        <asp:Button ID="btnComentar" runat="server" Text="Comentar" onclick="btnComentar_Click" ValidationGroup="Comentar" />                
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


</asp:Content>
