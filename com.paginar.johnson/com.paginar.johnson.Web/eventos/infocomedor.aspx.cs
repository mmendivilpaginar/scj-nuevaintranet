﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web.eventos
{
    public partial class infocomedor : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request["ID"] != null)
                {
                    hdID.Value = Request["ID"].ToString();
                    CargarDatos();
                }
            }
        }

        protected void CargarDatos()
        {
            int ID = int.Parse(hdID.Value);
            ControllerUCHome CN = new ControllerUCHome();            
            lblUnLikeCount.Text = CN.getVotos(ID, -1).ToString();
            lblLikeCount.Text = CN.getVotos(ID, 1).ToString();
        }

        protected void btnComentar_Click(object sender, EventArgs e)
        {
            ControllerUCHome CN = new ControllerUCHome();
            CN.InsertInfoComedorComentarios(this.ObjectUsuario.usuarioid, int.Parse(hdID.Value), txtComentario.Text);
            txtComentario.Text = "";
            dtComments.DataBind();       
        }

        protected void imgUnLike_Click(object sender, ImageClickEventArgs e)
        {
            ControllerUCHome CN = new ControllerUCHome(); 
            int ID = int.Parse(hdID.Value);
            if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID))
            {
                CN.InsertInfoComedorUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID, -1);
                lblUnLikeCount.Text = CN.getVotos(ID, -1).ToString();
                EjecutarScript("alert('Gracias por Votar');");
            }
            else
                EjecutarScript("alert('Esta Menú ya cuenta con su voto');");
 
        }

        protected void imgLike_Click(object sender, ImageClickEventArgs e)
        {
            ControllerUCHome CN = new ControllerUCHome();
            int ID = int.Parse(hdID.Value);
            if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID))
            {
                CN.InsertInfoComedorUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID, 1);
                lblLikeCount.Text = CN.getVotos(ID, 1).ToString();
                EjecutarScript("alert('Gracias por Votar');");
            }
            else
                EjecutarScript("alert('Esta Menú ya cuenta con su voto');");
 
        }


        int ScriptNro = 1;
        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            else
                this.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
            ScriptNro++;
        }
    
    }
}