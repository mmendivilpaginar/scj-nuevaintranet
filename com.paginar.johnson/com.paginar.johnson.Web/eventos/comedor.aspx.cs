﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;


namespace com.paginar.johnson.Web.eventos
{
    public partial class comedor : PageBase
    {
        ControllerUCHome cc = new ControllerUCHome();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdClusterID.Value = this.ObjectUsuario.clusteridactual.ToString() == "5" ? "1" : this.ObjectUsuario.clusteridactual.ToString();
                txtFecha.Text = String.Format("{0: dd/MM/yyyy}", DateTime.Now);
                dtUbicaciones.DataSource = cc.getUbicacion(int.Parse(hdClusterID.Value));
                dtUbicaciones.DataBind();
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            if(drpUbicacion.SelectedValue=="Todas")
                dtUbicaciones.DataSource = cc.getUbicacion(int.Parse(hdClusterID.Value));
            else
                dtUbicaciones.DataSource = cc.getUbicacionByID(drpUbicacion.SelectedValue);
                
            dtUbicaciones.DataBind();
        }


         protected void dtUbicaciones_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Footer)
            {
                HiddenField hdUbicacionID = (HiddenField)e.Item.FindControl("hdUbicacionID");
                DateTime fecha = DateTime.Parse(txtFecha.Text);
                FormView frmComedor = (FormView)e.Item.FindControl("frmComedor");
                DAL.DsUCHome.infComedorDataTable dt = cc.getComedorBuscador(hdUbicacionID.Value, fecha);
                frmComedor.DataSource = dt;
                frmComedor.DataBind();
                Label lblEmpty = (Label)e.Item.FindControl("lblEmpty");
                if (dt.Rows.Count == 0) lblEmpty.Visible = true;
                else lblEmpty.Visible = false;

                if (frmComedor.DataItemCount != 0)
                {
                    HiddenField hdID = (HiddenField)frmComedor.FindControl("hdID");
                    int ID = int.Parse(hdID.Value);
                    Label lblConComents = (Label)frmComedor.FindControl("lblConComents");
                    lblConComents.Text = cc.getCantComments(ID).ToString();

                    Label lblLikeCount = (Label)frmComedor.FindControl("lblLikeCount");
                    lblLikeCount.Text = cc.getVotos(ID, 1).ToString();

                    Label lblUnLikeCount = (Label)frmComedor.FindControl("lblUnLikeCount");
                    lblUnLikeCount.Text = cc.getVotos(ID, -1).ToString();
                }
            }
        }


         protected void dtUbicaciones_ItemCommand(object source, DataListCommandEventArgs e)
         {
             if (e.CommandName == "like")
             {
                 
                 ControllerUCHome CN = new ControllerUCHome();
                 int ID = int.Parse(e.CommandArgument.ToString());

                 if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID))
                 {
                     CN.InsertInfoComedorUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID, 1);
                     Label lbl = (Label)e.Item.FindControl("frmcomedor").FindControl("lbllikecount");
                     lbl.Text = cc.getVotos(ID, 1).ToString();
                     EjecutarScript("alert('Gracias por Votar');");
                 }
                 else
                     EjecutarScript("alert('Esta Menú ya cuenta con su voto');");

             }

             if (e.CommandName == "unlike")
             {
                 ControllerUCHome CN = new ControllerUCHome();
                 int ID = int.Parse(e.CommandArgument.ToString());

                 if (!CN.UsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID))
                 {
                     CN.InsertInfoComedorUsuarioVotacion(((MasterBase)this.Page.Master).ObjectUsuario.usuarioid, ID, -1);
                     Label lbl = (Label)e.Item.FindControl("frmcomedor").FindControl("lblUnLikeCount");
                     lbl.Text = cc.getVotos(ID, -1).ToString();
                     EjecutarScript("alert('Gracias por Votar');");
                 }
                 else
                     EjecutarScript("alert('Esta Menú ya cuenta con su voto');");
             }
         }
    
  


         int ScriptNro = 1;
         private void EjecutarScript(string js)
         {
             ScriptManager sm = ScriptManager.GetCurrent(this);
             if (sm.IsInAsyncPostBack)
                 System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
             else
                 this.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100" + ScriptNro.ToString(), js, true);
             ScriptNro++;
         }








    }
}