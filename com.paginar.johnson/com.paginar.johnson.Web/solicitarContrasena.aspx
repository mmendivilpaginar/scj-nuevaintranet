﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="solicitarContrasena.aspx.cs" Inherits="com.paginar.johnson.Web.solicitarContrasena" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 50%;
            text-align:Left;
            margin-left: 30%;
        }
        .style7
        {
            width: 795px;
        }
        .style8
        {
            width: 329px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <asp:MultiView ID="MVSolicitarContrasena" runat="server" ActiveViewIndex="0">
        <asp:View ID="VSolicitudOperario" runat="server">
        
        <div style="font-size: 12px; color: #2A3C6D; font-family: Arial; text-align: center;">
        <h3>Solicitud de cuenta</h3>
        <h4>Complete los datos seleccionados que serán enviados al responsable del Cluster que indique</h4>      

            <table class="style1">
                <tr>
                    <td class="style8">
                        <asp:Label ID="lblNombres" runat="server" Text="Nombres: "></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:TextBox ID="tbNombres" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="tbNombres">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        <asp:Label ID="lblApellidos" runat="server" Text="Apellidos: "></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:TextBox ID="tbApellidos" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="tbApellidos">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        <asp:Label ID="lblTelefono" runat="server" Text="Nº Telefono: "></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:TextBox ID="tbTelefono" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="tbTelefono">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        <asp:Label ID="lblCluster" runat="server" Text="Cluster: "></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:DropDownList ID="DDLCluster" runat="server" DataSourceID="ODSCluster" 
                            DataTextField="Descripcion" DataValueField="ID">
                            <asp:ListItem Selected="True" Value="0">Seleccione</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style8">
                        &nbsp;</td>
                    <td class="style7">
                        <asp:Button ID="BtnEnviar" runat="server" onclick="BtnEnviar_Click" 
                            Text="Enviar" />
                        <asp:Button ID="btnCancelar" runat="server" onclick="btnCancelar_Click" 
                            Text="Cancelar" />
                    </td>
                </tr>
            </table>
            <asp:ObjectDataSource ID="ODSCluster" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="getCluster" 
                TypeName="com.paginar.johnson.BL.ControllerCluster"></asp:ObjectDataSource>
        </div>
        </asp:View>    
        <asp:View ID="VSolicitudClave" runat="server">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                ValidationGroup="Enviar" />
        <div style="font-size: 12px; color: #2A3C6D; font-family: Arial; text-align: center;">
        <h3>¿Ha olvidado la contraseña?</h3>
            Para reestablecer la contraseña, escribe el Nro de Legajo/Empleado.<br />
            <asp:Label ID="Label1" runat="server" Text="Ingrese Numero de Legajo/Empleado:"></asp:Label>
            <asp:TextBox ID="tbLegajo" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="tbLegajo" 
                ErrorMessage="Solo se adminte el ingreso de números." 
                ValidationExpression="[0-9]*" ValidationGroup="Enviar">*</asp:RegularExpressionValidator>
            <asp:Button ID="bntEnviarLeg" runat="server" Text="Enviar" 
                onclick="bntEnviarLeg_Click" ValidationGroup="Enviar" />
        <div>Por Ej.: 3547</div>
            
        </div>
        <div><asp:Label ID="lblMensaje" runat="server"></asp:Label></div>

        </asp:View>
        <asp:View ID="VEstadoEnvio" runat="server">
        <div style="font-size: 12px; color: #2A3C6D; font-family: Arial; text-align: center;">
            <asp:Label ID="lblEstadoEnvio" runat="server"></asp:Label>
        </div>
        </asp:View>

    </asp:MultiView>
            <div style=" font-family:Arial; font-size:11px; border: 1px solid; margin-left: 0px; margin-right: 0px; margin-top: 50px;
                padding: 15px 0px 15px 0px; background-repeat: no-repeat; background-position: 10px center;
                display: block; color: #9F6000; background-color: #FEEFB3; background-image: url(images/alerta.png);">
                Para consultas sobre esta sección o su contenido, y para agregar tu foto, por favor
                contactar a <b>María del Carmen Aranguren        <b> </div>
    </form>
</body>
</html>
