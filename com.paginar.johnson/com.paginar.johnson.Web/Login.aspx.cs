﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Threading;
using System.Web.Security;
using System.Security.Principal;
using com.paginar.johnson.BL;

namespace com.paginar.johnson.Web
{
    public partial class Login : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (!this.IsPostBack)
            {
                string userlogged = ConfigurationManager.AppSettings["DEBUG_MODE_USER"];
                string UserName = string.Empty;
                ControllerSeguridad CS = new ControllerSeguridad();
                if (ConfigurationManager.AppSettings["DEBUG_MODE"] == "false")
                {
                    if (Request.QueryString["Forced"] == null)
                    {

                        WindowsIdentity user = WindowsIdentity.GetCurrent();
                        string UserNameAux = string.Empty;

                        int iVal = user.Name.IndexOf('\\');
                        if (iVal == -1)
                            UserNameAux = user.Name;
                        else
                            UserNameAux = user.Name.Split('\\')[1];

                       
                        UserName = CS.GetUserNameByUserAD(UserNameAux);

                        if (!string.IsNullOrEmpty(UserName))
                        {
                            if (Membership.GetUser(UserName) != null)
                            {
                                FormsAuthentication.SetAuthCookie(UserName, true);

                                GuardarLog(UserName);

                                if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                                    Response.Redirect(Request.QueryString["ReturnUrl"]);
                                else
                                    Response.Redirect("~/");
                            }
                        }
                        //Sino se se autentica y redirecciona mando a pantalla de acceso denegado
                        //Response.Redirect("AccesoDenegado.aspx");
                    }
                    else
                    {
                        //forzó el logout
                        
                    }
                }
                else
                {
                    //UserName = ConfigurationManager.AppSettings["DEBUG_MODE_USER"];
                    UserName = CS.GetUserNameByUserAD(ConfigurationManager.AppSettings["DEBUG_MODE_USER"]);

                    if (!string.IsNullOrEmpty(UserName))
                    {
                        FormsAuthentication.SetAuthCookie(UserName, true);

                        GuardarLog(UserName);

                        if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                            Response.Redirect(Request.QueryString["ReturnUrl"]);
                        else
                            Response.Redirect("~/");
                    }
                }
            }
        }

        protected void Login2_LoggedIn(object sender, EventArgs e)
        {
            this.EditaClave = true;
            GuardarLog(((System.Web.UI.WebControls.Login)sender).UserName);
        }

        protected void GuardarLog(string username)
        {
            try
            {
                ControllerSeguridad CS = new ControllerSeguridad();
                Usuario u = CS.GetDatosUsuarioByUserName(username);
                CS.GuardarLog(u.usuarioid, DateTime.Now, Request.ServerVariables["REMOTE_ADDR"].ToString());
            }
            catch (Exception ex) { }

        }

        protected void Login2_LoggingIn(object sender, LoginCancelEventArgs e)
        {

            System.Web.UI.WebControls.Login Loggg = (System.Web.UI.WebControls.Login)(sender);
            if (Loggg.UserName.Length > 7)
            {
                string global = Loggg.UserName.Substring(0, 7);
                Int32 longitud = Loggg.UserName.Length - 7;
                if (global.ToUpper() == "GLOBAL\\")
                {
                    string usernameB = Loggg.UserName.Substring(7, longitud);
                    Loggg.UserName = usernameB;
                }

            }

        }
       
    }
}