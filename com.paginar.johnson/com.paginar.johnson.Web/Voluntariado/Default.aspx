﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="com.paginar.johnson.Web.Voluntariado.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../noticias/ucCategoriaNoticia.ascx" tagname="ucCategoriaNoticia" tagprefix="uc1" %>
<%@ Register Assembly="com.paginar.johnson.BL" Namespace="com.paginar.HierarchicalDataSource"
    TagPrefix="cc1" %>

<%@ Register src="../UserControl_Home/ucVoluntariadoActividades.ascx" tagname="ucVoluntariadoActividades" tagprefix="uc3" %>
<%@ Register src="../UserControl_Home/ucVoluntariadoCronograma.ascx" tagname="ucVoluntariadoCronograma" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <script type="text/javascript" src="libreria/jquery.nivo.slider.js"></script>
    <script src="libreria/jquery.jqzoom-core-pack.js" type="text/javascript"></script>
    <link href="css/jquery.jqzoom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/default/default.css" type="text/css" media="screen" />

<script type="text/javascript">
    //$('head').append('<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />');
    //$('head').append('<link rel="stylesheet" href="css/default/default.css" type="text/css" media="screen" />');

    $(function () {
        //$("#noticias h2").hide();
        var galleries = $('.ad-gallery').adGallery();
        $('#switch-effect').change(
      function () {
          galleries[0].settings.effect = $(this).val();
          return false;
      }
    );
        $('#toggle-slideshow').click(
      function () {
          galleries[0].slideshow.toggle();
          return false;
      }
    );

  });

  $(document).ready(function () {
      $('.jqzoom').jqzoom({
          zoomType: 'standard',
          lens: true,
          preloadImages: false,
          alwaysOn: false
      });
      $('#slider').nivoSlider();
      $('#slider2').nivoSlider();
      jQuery(".contenidoHome").hide();
      //toggle the componenet with class msg_body
      jQuery(".heading").click(function () {
          var up = "../css/images/bullet-azul.png";
          var down = "../css/images/bullet-celeste-abajo.png";

          var icon = $("img[id=img" + $(this).attr("id") + "]");

          var iconoAnterior = icon.attr("src");


          jQuery(this).next(".contenidoHome").slideToggle(500);

          icon.attr("src", iconoAnterior == up ? down : up);

      });
  });

  </script>
    <style type="text/css">
  
  pre {
    font-family: "Lucida Console", "Courier New", Verdana;
    border: 1px solid #CCC;
    background: #f2f2f2;
    padding: 10px;
  }
  code {
    font-family: "Lucida Console", "Courier New", Verdana;
    margin: 0;
    padding: 0;
  }

  #gallery {
    padding: 30px;
    background: #d9d9d9;
  }
  #descriptions {
    position: relative;
    height: 50px;
    background: #EEE;
    margin-top: 10px;
    width: 640px;
    padding: 10px;
    overflow: hidden;
  }
    #descriptions .ad-image-description {
      position: absolute;
    }
      #descriptions .ad-image-description .ad-description-title {
        display: block;
      }
      
.layer1 {
margin: 0;
padding: 0;
width: 500px;
}
 
.heading {
margin: 1px;

padding: 3px 10px;
cursor: pointer;
position: relative;

}
.contenidoHome {
padding: 5px 10px;
padding-left: 25px;
background-color:#fafafa;
}      
  </style>


<h2>Voluntariado</h2>
    
    <asp:TabContainer ID="TabContainerVoluntariado" runat="server" 
        ActiveTabIndex="4">
        <asp:TabPanel ID="TPHome" HeaderText="Voluntariado" runat="server">
        <ContentTemplate>
        <h4>Sumar Voluntariado es una conjunción de:</h4>
         
                    
                    <ul>
                        <li>
                        &nbsp;las necesidades de la <strong>comunidad</strong>,
                        </li>
                        <li>
                        &nbsp;los intereses de los <strong>empleados</strong>,
                        </li>
                        <li>
                        &nbsp;las prioridades de la <strong>compañía</strong>.
                        </li>
                    </ul>
                    <br />
                    <h4>Programa de presencia Cluster (Argentina, Chile, Paraguay y Uruguay)</h4><br /><br />

                    <asp:Image ID="ImgPrograma" ImageUrl="~/Voluntariado/Imagenes/programasumervoluntarios.jpg" runat="server" />
            
            <br /><br />

<%--            <div class="layer1">
                  <p class="heading" id="1"><img id="img1"  src="../css/images/bullet-azul.png" />&nbsp Objetivos</p>--%>
                  <div class="layer1">
                    <h3>Objetivos generales del programa Sumar Voluntariado</h3>
                    <ul>
                        <li>
                            Brindar un <strong>espacio de acción social</strong> para aquellos empleados y jubilados de SCJ que posean iniciativa y motivación de contribuir con el desarrollo social, a través del compromiso y el trabajo profesional con la comunidad.
                        </li>
                        <li>
                            Fortalecer el <strong>vínculo de los empleados de SCJ con las comunidades prioritarias para la empresa</strong> como un modo de expresión de los valores corporativos.
                        </li>
                        <li>
                            Colaborar con el <strong>desarrollo sostenible de la comunidad</strong> a través de alianzas con organizaciones que potencien el impacto social de nuestras acciones voluntarias.
                        </li>
                    </ul>
                    <%--</div>--%>

                 <%-- <p class="heading" id="2"><img id="img2"  src="../css/images/bullet-azul.png" />&nbsp Más información sobre Sumar</p>--%>
               <%--   <div class="contenidoHome">
                              
                <h3>Instructivo</h3>
                        <asp:TreeView ID="TreeViewVoluntariado" runat="server" DataSourceID="RelationalSystemDataSource1"
                            OnSelectedNodeChanged="TreeViewVoluntariado_SelectedNodeChanged">
                            <DataBindings>
                                <asp:TreeNodeBinding TextField="Name" />
                            </DataBindings>
                        </asp:TreeView>
                        <cc1:RelationalSystemDataSource runat="server" ID="RelationalSystemDataSource1" IncludeRoot="False" />
                        <a name="contenido-abajo"></a>
                        <br />
                        <asp:UpdatePanel ID="updContenido" runat="server">
                            <ContentTemplate>
                                <asp:HiddenField ID="HiddenFieldItemID" runat="server" />
                                <div id="divContenido">
                                    <asp:FormView ID="frmContenido" runat="server" DataKeyNames="Content_ItemId" DataSourceID="odsContenido">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltContenido" runat="server" Text='<%# Bind("Contenido") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </asp:FormView>
                                    <asp:ObjectDataSource ID="odsContenido" runat="server" OldValuesParameterFormatString="original_{0}"
                                        SelectMethod="GetContentById" TypeName="com.paginar.johnson.BL.ControllerContenido">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldItemID" Name="IdItem" PropertyName="Value"
                                                Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="TreeViewVoluntariado" EventName="SelectedNodeChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                        </div>--%>

                         </div>


            <br /><br />
       
</ContentTemplate>
        
</asp:TabPanel>
        <asp:TabPanel HeaderText="Noticias" ID="TPVolutariadoNoticias" runat="server">
            <ContentTemplate>
                <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="58"  TagID="4" runat="server" />
            
</ContentTemplate>
        
</asp:TabPanel>
<%--        <asp:TabPanel HeaderText="Actividades" ID="TPVolutariadoActividades" runat="server">
            <ContentTemplate>
                <uc3:ucVoluntariadoActividades ID="ucVoluntariadoActividades1" runat="server" />

            
</ContentTemplate>--%>
        
</asp:TabPanel>
        <asp:TabPanel HeaderText="Cronograma" ID="TPVolutariadoInstructivo" runat="server">
            <ContentTemplate>
                <uc4:ucVoluntariadoCronograma ID="ucVoluntariadoCronograma1" runat="server" />
            
</ContentTemplate>
        
</asp:TabPanel>
        <asp:TabPanel HeaderText="Galería" ID="TPVolutariadoEventos" runat="server">
            <ContentTemplate>
                <asp:MultiView ID="MVGaleria" runat="server">
                    <asp:View ID="VEventosG" runat="server">
                        <asp:Repeater ID="REventos" runat="server" DataSourceID="ODSEventosTodos">
                        <HeaderTemplate>
                        <%--<ul>--%>
                        <div class="slider-wrapper theme-default">
                            <div id="slider" class="nivoSlider">

                        </HeaderTemplate>
                        <ItemTemplate>
<%--                            <li>
                            <div class="Econteiner">
                            <div class="Eimagen">
                                <a href='<%# "?EventoActivo=" + Eval("IdEvento").ToString() + "&Seccion=galeria"  %>'> <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/Voluntariado/Thumbnail/" + Eval("Imagen") %>' /></a>
                            </div>
                            <div class="ECuerpo">
                             <a href='<%# "?EventoActivo=" + Eval("IdEvento").ToString() + "&Seccion=galeria" %>'><h3><%# Eval("Nombre") + " (" + Eval("Fecha", "{0:MM}") + "/" + Eval("Fecha", "{0:yyyy}") + ")"%></h3></a>
                            <p>
                              <%# Eval("Descripcion")%>
                            </p>
                            </div>
                            </div>
                            </li>--%>

                <a href='<%# "?EventoActivo=" + Eval("IdEvento").ToString() + "&Seccion=galeria" %>'><img src='<%# "/Voluntariado/Imagenes/" + Eval("Imagen") %>' data-thumb='<%# "/Voluntariado/Thumbnail/" + Eval("Imagen") %>' alt="" title="<%# Eval("Nombre") + " (" + Eval("Fecha", "{0:MM}") + "/" + Eval("Fecha", "{0:yyyy}") + ")"%>" /></a>


<%--
            <div id="htmlcaption" class="nivo-html-caption">
                <a href='<%# "?EventoActivo=" + Eval("IdEvento").ToString() + "&Seccion=galeria" %>'><h3><%# Eval("Nombre") + " (" + Eval("Fecha", "{0:MM}") + "/" + Eval("Fecha", "{0:yyyy}") + ")"%></h3></a>
            </div>--%>

                        </ItemTemplate>
                        <FooterTemplate>
                            </div>

                        </div>
                       <%-- </ul>--%>
                        </FooterTemplate>
                        </asp:Repeater>

                        <asp:ObjectDataSource ID="ODSEventosTodos" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="EventoSelectWImagen" 
                            TypeName="com.paginar.johnson.BL.ControllerVoluntariado" 
                            DeleteMethod="ImagenDelete" InsertMethod="ImagenInsert" 
                            UpdateMethod="ImagenUpdate">
                            <DeleteParameters>
                                <asp:Parameter Name="IdImagen" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="IdEvento" Type="Int32" />
                                <asp:Parameter Name="Imagen" Type="String" />
                                <asp:Parameter Name="Descripcion" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="IdImagen" Type="Int32" />
                                <asp:Parameter Name="IdEvento" Type="Int32" />
                                <asp:Parameter Name="Imagen" Type="String" />
                                <asp:Parameter Name="Descripcion" Type="String" />
                            </UpdateParameters>
                        </asp:ObjectDataSource>

                    </asp:View>
                    <asp:View ID="VGaleria" runat="server">
                        <asp:Literal ID="ltrlTitulo" runat="server"></asp:Literal>
                        <div id="gallery" class="ad-gallery">
                                  <div class="ad-image-wrapper">
                                  </div>
                                  <div class="ad-controls">
                                  </div>
                                  <div class="ad-nav">
                                    <div class="ad-thumbs">
                                      <ul class="ad-thumb-list">
                                            <asp:Repeater ID="RGaleria" runat="server" DataSourceID="ODSGaleria">
                                            <ItemTemplate>
                                                  <a href='<%# RutaImagenEvento(Eval("Imagen")) %>'>
                                                    <img src='<%# "/Voluntariado/Thumbnail/" + Eval("Imagen") %>' title="" alt='<%# Eval("Descripcion") %>' class="image3">
                                                  </a>

                                            </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:ObjectDataSource ID="ODSGaleria" runat="server" 
                                                OldValuesParameterFormatString="original_{0}" 
                                                SelectMethod="ImagenSelectByEvento" 
                                                TypeName="com.paginar.johnson.BL.ControllerVoluntariado">
                                                <SelectParameters>
                                                    <asp:QueryStringParameter Name="IdEvento" QueryStringField="EventoActivo" 
                                                        Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                      </ul>
                                    </div>
                                      <asp:Button ID="btnVolver" runat="server" Text="Volver índice de galerías" 
                                          onclick="btnVolver_Click" />
                                  </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            
</ContentTemplate>
        
</asp:TabPanel>
    </asp:TabContainer>
    

    
</asp:Content>
