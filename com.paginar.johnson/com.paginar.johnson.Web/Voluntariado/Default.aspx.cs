﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.BL;
using System.Data;


namespace com.paginar.johnson.Web.Voluntariado
{
    public partial class Default : PageBase
    {
        string EventoActivo = string.Empty;
        string Seccion = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //EventoActivo

            if (!Page.IsPostBack)
            {
                
                if (this.ObjectUsuario.clusteridactual == 1)
                {
                    //TPVolutariadoActividades.Visible = true;
                    TPVolutariadoInstructivo.Visible = true;
                    TPVolutariadoEventos.Visible = true;

                }
                else
                {
                    //TPVolutariadoActividades.Visible = false;
                    TPVolutariadoInstructivo.Visible = false;
                    TPVolutariadoEventos.Visible = false;
                
                }
                Seccion = Request.QueryString["Seccion"];
                if (Seccion != null)
                {

                    switch (Seccion.ToString())
                    {
                        case "galeria":
                        case "eventos":
                            TabContainerVoluntariado.ActiveTabIndex = 4;

                            break;
                        case "":
                            break;
                        //default:
                    }
                    //
                }
                EventoActivo = Request.QueryString["EventoActivo"];
                //MVGaleria.SetActiveView(VGaleria);
                if (EventoActivo != null)
                {

                    MVGaleria.SetActiveView(VGaleria);
                    ControllerVoluntariado CV = new ControllerVoluntariado();
                    //CV.EventoSelect(int.Parse(EventoActivo.ToString())).Rows[0].ItemArray[1];
                    ltrlTitulo.Text = "<H3>" + CV.EventoSelect(int.Parse(EventoActivo.ToString())).Rows[0].ItemArray[1].ToString() + "</h3>";
                }
                else
                {
                    MVGaleria.SetActiveView(VEventosG);
                    //EventoActivo
                }

            }
            else
            {
            }

            //TreeViewVoluntariado.Attributes.Add("onclick", "pageScroll()");
            ControllerContenido cc = new ControllerContenido();
            //RelationalSystemDataSource1.Path = cc.Content_TipoGetIdrootByClusterId("Voluntariado Corporativo", this.ObjectUsuario.clusteridactual).ToString();
        }

        protected void TreeViewVoluntariado_SelectedNodeChanged(object sender, EventArgs e)
        {
            //HiddenFieldItemID.Value = TreeViewVoluntariado.SelectedNode.DataPath;
        }

        public string RutaImagenEvento(object imagen)
        {
            return "/Voluntariado/Imagenes/" + imagen.ToString();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;

            Response.Redirect(path + "?Seccion=galeria");        
        }

        //protected void LinkButton1_Click(object sender, EventArgs e)
        //{
        //    MVListado.SetActiveView(Vprograma);
        //}

        //protected void LinkButton2_Click(object sender, EventArgs e)
        //{
        //    MVListado.SetActiveView(Vobjetivos);
        //}

        //protected void LinkButton3_Click(object sender, EventArgs e)
        //{
        //    MVListado.SetActiveView(VInstructivo);
        //}

    }
}