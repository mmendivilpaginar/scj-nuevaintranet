﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="campaniaseguridad.aspx.cs" Inherits="com.paginar.johnson.Web.novedades.campaniaseguridad" %>

<%@ Register Src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <div id="nobg"></div>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <asp:TabPanel HeaderText="10 en Seguridad" ID="Panel1" runat="server">
            <ContentTemplate>
                <h2 class="destacado">
                    <span>Compromison Johnson</span>
                    10 en Seguridad
                </h2>
                <p class="destacado">
                    <img src="../images/seguridad2.jpg" class="floatRight">
                    Esta campaña tiene como fin lograr la total y absoluta concientización, en los temas
                    de Seguridad (SHE).</p>
                <p>
                    Para ello, se refuerza el conocimiento de normas, procedimientos, instalaciones,
                    usos y acciones correctas, para garantizar el comportamiento seguro, de cada uno
                    de nosotros, en nuestro lugar de trabajo.</p>
                <p>
                    Esperamos que esta Campaña sea una motivación más para que todo el equipo de SCJ
                    participe activamente. De esta forma, todos podremos decir que somos... ¡10 en Seguridad!</p>
                <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" runat="server" Categoriaid="61" />
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel HeaderText="Links" ID="TabPanel1" runat="server">
            <ContentTemplate>
                <ul>
<%--                    <li><a title="" href="file://arpodef0/common/Public/SIG/Registros/Impactos%20Ambientales"
                        target="_blank" class="fecha">Impactos ambientales</a></li>--%>
                    <li><a title="" href="file://arpodef0/common/Public/SHE_Publico/Capacitaciones%20SHE/Material%20de%20Capacitaciones/07-08/Gestión%20de%20residuos%2007-08/Gestión%20de%20residuos,%20ahorro%20de%20energía,%20indicadores%20de%20planta,%20impactos%20ambientales%202007.ppt"
                        target="_blank" class="fecha">Gestión de residuos, ahorro de energía, indicadores
                        e impactos ambientales</a></li>
                </ul>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
</asp:Content>
