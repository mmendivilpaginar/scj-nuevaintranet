﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="proveeduria.aspx.cs" Inherits="com.paginar.johnson.Web.novedades.proveeduria" %>

<%@ Register Src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Proveeduría</h2>
    <span class="texto">
        <%--<p>--%>
            <div class="box box-ext">
                <!--[contenido]-->
                <div class="accordion">
                    <h5>
                        <a href="#">Horarios de atención al personal efectivo</a></h5>
                    <div>
                        <ul>
                            <li>Lunes a jueves de todo el año: de 9.00 a 12.30</li>
                            <li>Viernes de invierno: de 9.00 a 12.30 y de 13.30 a 14.00</li>
                            <li>Viernes de verano: de 9.00 a 12.00</li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Horarios de atención a jubilados</a></h5>
                    <div>
                        <ul>
                            <li>Martes, miércoles y jueves: de 9.00 a 12.30</li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Horarios de atención para personal externo y temporarios</a></h5>
                    <div>
                        <ul>
                            <li>Personal temporario Operativo, Externos de Guardería y Comedor: miércoles de 9.00
                                a 12.30</li>
                            <li>Personal temporario Administrativo y externos: martes, miércoles y jueves, de 9.00
                                a 12.30</li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Formas de pago</a></h5>
                    <div>
                        <ul>
                            <li>Personal efectivo y contratado: el pago se descuenta de sus haberes.</li>
                            <li>Personal recomendado y temporarios: el pago es al contado, con un descuento especial.</li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Cantidad máxima de productos por mes</a></h5>
                    <div>
                        <ul>
                            <li>El tope máximo para la compra mensual para empleados es de 45 productos, con un
                                máximo de 5 unidades por variedad. Externos y Temporarios, 30 unidades.</li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Lista y Contactos</a></h5>
                    <div>
                        <ul>
                            <li><a href="file://arpodef0/common/Public/RH/Proveeduria/INTRANET_LISTA_PRECIOS_ACTUAL.xls"
                                class="fecha" target="_blank">Lista de precios actualizada</a></li>
                            <li><a href="mailto:A069375@scj.com" class="fecha" target="_blank">E-mail de Proveeduría
                                (Luis Kreser)</a></li>
                            <li><a href="mailto:rrmagnel@scj.com" class="fecha" target="_blank">E-mail de Proveeduría
                                (Ricardo Magnelli)</a></li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Lista para pedidos</a></h5>
                    <div>
                        <ul>
                            <li>El personal de Pacheco, Pilar e Interior, puede hacer su pedido a través de esta
                                planilla que deberán enviar por e-mail a Ricardo Magnelli.<br>
                            </li>
                            <li><a href="file://arpodef0/common/Public/RH/Proveeduria/INTRANET_PLANILLA_ACTUAL.xls"
                                class="fecha" target="_blank">Click aquí para ver la planilla</a></li>
                        </ul>
                    </div>
                    <h5>
                        <a href="#">Tienda Online para Empleados</a></h5>
                    <div>
                        <ul>                            
                           <li> Podrás hacer los pedidos desde las líneas, desde tu escritorio y hasta en tu casa (accediendo a través de la red de SCJ).</li>
                           <li> Podrás comprar 5 productos más por mes (máximo 50 unidades).</li>
                           <li> En la primera compra, recibirás un 10% de descuento adicional.</li>
                           <li> Acceda a la tienda online desde 
                           <a href="http://americas-store.scj.com:9080/scj-tienda-empleados/auth/login?targetUri=%2F" target="_blank" >aquí</a>
                           , solo ingresando su número de legajo como usuario, 'scjar' como contraseña y 'Argentina' como país (la contraseña es sin las comillas, en tu primer ingreso te recomendamos cambiarla).</li>
                           <li> Por dudas o consulta sobre el uso de la Tienda Online, comunicate con Rocardo Magnelli al interno 8247 de Podestá </li>
                        </ul>
                    </div>
                </div>
                <!--[/contenido]-->
            </div>
       <%-- </p>--%>
        <img src="../images/obra-social.jpg" width="120" />
    </span>
    <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="52" runat="server" />
</asp:Content>
