﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/TwoSidebars.master"
    AutoEventWireup="true" CodeBehind="eap.aspx.cs" Inherits="com.paginar.johnson.Web.novedades.eap" %>

<%@ Register Src="../noticias/ucCategoriaNoticia.ascx" TagName="ucCategoriaNoticia"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        EAP</h2>
    <p>
        <strong>EAP</strong> significa Programa de Ayuda al Empleado (Employee Assistance
        Program), y es un servicio que SCJ ofrece al personal y su familia, en forma totalmente
        gratuita, con el fin de asistirlos en cualquier tipo de consulta o dificultad legal,
        financiera o psicológica.</p>
    <p>
        Para utilizar este servicio, sólo es necesario llamar al 0800-77-78327 y la consulta
        será respondida en forma absolutamente confidencial</p>
    <br />
    <h3>
        Preguntas y respuestas sobre EAP</h3>
    <h4>
        ¿Quién puede utilizar este servicio?</h4>
    <p>
        Absolutamente todo el personal de SCJ, cónyuges e hijos de hasta 25 años.</p>
    <h4>
        ¿En qué horarios se puede llamar a EAP?</h4>
    <p>
        De lunes a viernes de 10 a 20 horas. Fuera de ese horario, un contestador remite
        a un servicio de emergencia telefónica.</p>
    <h4>
        ¿Este servicio tiene algún costo?</h4>
    <p>
        No, es totalmente gratuito para todos los empleados de SCJ y sus familiares dependientes.</p>
    <h4>
        ¿Cuándo utilizar EAP?</h4>
    <p>
        Cuando quieran realizar una consulta preventiva o necesiten asistencia en temas
        relacionados con una dificultad legal, financiera o psicológica.</p>
    <p>
        Entre otras cosas, EAP puede ayudar en casos de:</p>
    <ul>
        <li>Cambios de estados de ánimo (angustia, desgano, tristeza).</li>
        <li>Stress laboral y sentimientos de fatiga. </li>
        <li>Temas relacionados con la crianza y la educación de hijos.</li>
        <li>Conflictos de pareja.</li>
        <li>Temores y ansiedad.</li>
        <li>Situaciones de crisis.</li>
        <li>Experiencias penosas o dolorosas.</li>
        <li>Problemas con el sueño.</li>
        <li>Baja autoestima.</li>
        <li>Abuso o dependencia de alcohol o drogas.</li>
        <li>Conflictos legales.</li>
        <li>Problemas financieros.</li>
    </ul>
    <br />
    <address>
        <p>
            <strong>EAP</strong></p>
        <p>
            Tel.: 4706-0390<br />
            E-mail: <a href="mailto:eap@sueap.com" class="texto">eap@sueap.com</a>
            <br />
            Web: <a href="http://www.eaplatina.com" target="_blank">www.eaplatina.com</a>
            <br />
            Dirección: Av. Del Libertador 6049 1° A &#8211; (C1428ARD) Buenos Aires &#8211;
            Argentina.</p>
    </address>
    <uc1:ucCategoriaNoticia ID="ucCategoriaNoticia1" Categoriaid="60" TagID="5" runat="server" />
</asp:Content>
