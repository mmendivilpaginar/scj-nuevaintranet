﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Flyer.aspx.cs" Inherits="com.paginar.johnson.Web.Flyer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Johnson Intranet</title>
    <script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.4.2.min.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div align="center"  >

        <div onclick="Redireccionar();">
         <asp:Image ID="image" runat="server"  />
         </a>    
         </div>

          <asp:HiddenField ClientIDMode="static" ID="hdInfoID" runat="server"/>
       

        <asp:Literal ID="ltjs" runat="server" />
        <script type="text/javascript">

            function Redireccionar() {
                //var InfoID = document.getElementById('<%= hdInfoID %>').value;
                var InfoID = $("#hdInfoID").val();
                opener.location.href = '/noticias/noticia.aspx?infoID=' + InfoID;
                window.close();
          

            }

        </script>
    </div>
    </form>
</body>
</html>
