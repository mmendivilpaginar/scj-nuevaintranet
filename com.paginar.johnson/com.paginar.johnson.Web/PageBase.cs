﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.paginar.johnson.BL;
using System.Configuration;

namespace com.paginar.johnson.Web
{
    public class PageBase : System.Web.UI.Page
    {

        //protected override void OnInit(EventArgs e)
        //{
        //    Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TATi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
        //}

        public string ASP_SITE
        {
            get
            {
               return  ConfigurationManager.AppSettings["ASP_SITE"];
            }
        }

        public Usuario ObjectUsuario
        {
            get
            {
                if (Session["Usuario"] == null)
                {                    
                    if (Page.User.Identity.IsAuthenticated)
                    {
                        Usuario u = new Usuario();
                        ControllerSeguridad CS = new ControllerSeguridad();
                        u = CS.GetDatosUsuarioByUserName(Page.User.Identity.Name);
                        Session["Usuario"] = u;    
                    }                                    
                }
                return Session["Usuario"] as Usuario; 
            }
        }

        public bool EditaClave
        {
            get {
                if (Session["EditaClave"] == null)
                return false; 
                else
                    return ((bool)Session["EditaClave"]);
            }
            set {  Session["EditaClave"] = value; }
        }
    }

}