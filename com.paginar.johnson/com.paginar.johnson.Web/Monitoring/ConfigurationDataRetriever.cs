﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace com.paginar.johnson.Web.Monitoring
{
    public class ConfigurationDataRetriever:IDataRetriever
    {
        #region IDataRetriever Members

        public Boolean EstaEnProduccion
        {
             get {

                if (ValuesConfig["ISPRODUCCION"].ToUpper().Equals("1"))
                    return true;
                else
                    return false;
            }
        }

        public string User
        {
            get { return ValuesConfig["USER"];
            }
        }

        public string Asunto
        {
            get {
                return ValuesConfig["ASUNTO"];
            }
        }

        public string Port
        {
            get { return ValuesConfig["PORT"]; }
        }

        public string From
        {
            get { return ValuesConfig["CUENTAORIGEN"]; }
        }

        public string To
        {
            get { return ValuesConfig["DESTINATARIOS"]; }
        }

        public string SmtpServer
        {
            get { return ValuesConfig["SMTP"]; }
        }

        public string Password
        {
            get { return ValuesConfig["PASSWORD"]; }
        }

        public bool Enable_Ssl
        {
            get {
                if (ValuesConfig["ENABLESSL"].ToUpper().Equals("TRUE"))
                    return true;
                else
                    return false;
            }
        }

        #endregion


        private string PATH_FILE_CONFIG;
        public ConfigurationDataRetriever(string fileConfig)
        {
            this.PATH_FILE_CONFIG = fileConfig;
        }

        private XDocument _DocumentoNotify;
        private  XDocument DocumentoNotify
        {
            get
            {
                if (_DocumentoNotify == null)
                    _DocumentoNotify = XDocument.Load(PATH_FILE_CONFIG);

                return _DocumentoNotify;
            }
        }



        private Dictionary<string, string> _ValuesConfig = null;
        private Dictionary<string, string> ValuesConfig
        {
            get
            {
                if (_ValuesConfig == null)
                {
                    var result =
                    from c in DocumentoNotify.Elements("EmailReport").Elements("ItemMail")
                    select new
                    {
                        Atributo = c.Attribute("Id").Value.ToUpper(),
                        Valor = c.Attribute("value").Value
                    };

                    _ValuesConfig = new Dictionary<string, string>();
                    foreach (var item in result)
                    {
                        _ValuesConfig.Add(item.Atributo, item.Valor);
                    }
                }
                return _ValuesConfig;
            }
        }

    }
}
