﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.Web.Monitoring
{
   public interface IContentsMail
    {
        string TextToSend { get;}
        List<System.Net.Mail.Attachment> Adjuntos{get;}
    }
}
