﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace com.paginar.johnson.Web.Monitoring
{
   public class Mail
    {
       private IDataRetriever SourceParameter;
       private IContentsMail SourceContents;
       public IFilter SourceFilter;

       public Mail(IDataRetriever source, IContentsMail  sourceContents)
       {
           SourceParameter = source;
           SourceContents =sourceContents;
       }

       public void EnviarNotificacion()
       {

           if (SourceFilter != null)
               if (!this.SourceFilter.IsValid())
                   return;

           try
           {
               SmtpClient smtpMail=null;

               //---Verificar servidor smtp y puertos
               if (this.SourceParameter.Port!= string.Empty)
               {
                   smtpMail = new SmtpClient(this.SourceParameter.SmtpServer, Int32.Parse(this.SourceParameter.Port));
               }
               else
               {
                   smtpMail = new SmtpClient(this.SourceParameter.SmtpServer);
               }
               //----------------------
               MailAddress _from = new MailAddress(this.SourceParameter.From, "SCJ-Intranet");
               MailMessage _mensaje = new MailMessage();

               _mensaje.Body = this.SourceContents.TextToSend;
               _mensaje.Subject = this.SourceParameter.Asunto;
               _mensaje.From = _from;
               _mensaje.IsBodyHtml = true;

               string[] listaMensaje = this.SourceParameter.To.Split(',');
               foreach (String unDestino in listaMensaje)
               {
                   //mensajes.Add(new System.Net.Mail.MailMessage(from, unDestino, Asunto, cuerpoMensaje));
                   _mensaje.To.Add(unDestino);
               }

               if (this.SourceContents.Adjuntos != null && this.SourceContents.Adjuntos.Count > 0)
               {
                   foreach (Attachment  item in this.SourceContents.Adjuntos)
                   {
                       _mensaje.Attachments.Add(item);
                   }
               }

               smtpMail.EnableSsl = this.SourceParameter.Enable_Ssl;
               //else

               //------------Verficar Credenciales
               if (this.SourceParameter.User == string.Empty || this.SourceParameter.Password== string.Empty)
                   smtpMail.UseDefaultCredentials = true;
               else
                   smtpMail.Credentials = new System.Net.NetworkCredential(this.SourceParameter.User, this.SourceParameter.Password);
               //-----------------------------------------------------
               smtpMail.Send(_mensaje);
           }
           catch (Exception)
           {


           }

       }
    }
}
