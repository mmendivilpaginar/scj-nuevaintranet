﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace com.paginar.johnson.Web.Monitoring
{
    public class Log
    {

        private string StringLog;
        StreamWriter StreamW;
        public Log(string _StringLog, StreamWriter stream)
        {
            this.StringLog = _StringLog;
            this.StreamW = stream;
        }

        public void Run()
        {
            try
            {
                StreamW.WriteLine("{0}", StringLog);

              //  sqlList.ForEach(r => StreamW.WriteLine("{0}", r));
            }
            finally
            {
                StreamW.Close();
            }

            
        }
    }
}