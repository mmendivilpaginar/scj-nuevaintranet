﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.Web.Monitoring
{
   public interface IFilter
    {
        Boolean IsValid();
    }
}
