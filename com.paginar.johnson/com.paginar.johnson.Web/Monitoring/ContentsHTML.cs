﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.Web.Monitoring
{
   public class ContentsHTML:IContentsMail
    {
        #region IContentsMail Members

       private string _TextToSend
       {
           get
           {
               string val = @"<html>
                        <body>
                          <h1>Ocurrio un error! </h1>
                           <h2>Fecha y hora: T_I_E_M_P_O</h2>
                           <h3>Usuarios conectados: U_S_U_A_R_I_O_S</h3>
                            <h3>Servidor: SERVER_NAME</h3>
                          <table cellpadding=""5"" cellspacing=""0"" border=""1"">
                          <tr>
                          <td style=""text-align: right;font-weight: bold"">URL:</td>
                          <td>{0}</td>
                          </tr>
                          <tr>
                          <td style=""text-align: right;font-weight: bold"">User:</td>
                          <td>{1}</td>
                          </tr>
                          <tr>
                          <td style=""text-align: right;font-weight: bold"">Exception Type:</td>
                          <td>{2}</td>
                          </tr>
                          <tr>
                          <td style=""text-align: right;font-weight: bold"">Message:</td>
                          <td>{3}</td>
                          </tr>
                          <tr>
                          <td style=""text-align: right;font-weight: bold"">Stack Trace:</td>
                          <td>{4}</td>
                          </tr> 
                          </table>
                        </body>
                        </html>";
               return val;
           }
       }

       string URL;
       string User;
       string ExceptionType;
       string Message;
       string StackTrace;
       string NumbersUser = "";
       string Server_Name = "";
       public ContentsHTML(string URL, string User, string ExceptionType, string Message,string StackTrace,string _numberUsers, string serverName)
       {
           this.URL = URL;
           this.User = User;
           this.ExceptionType = ExceptionType;
           this.Message = Message;
           this.StackTrace = StackTrace;
           this.NumbersUser = _numberUsers;
           this.Server_Name = serverName;


       }
       public string TextToSend
        {
            get {
                
                string ret=string.Format(_TextToSend, URL,User,ExceptionType,Message,StackTrace);
                ret = ret.Replace("T_I_E_M_P_O", DateTime.Now.ToString());
                ret = ret.Replace("U_S_U_A_R_I_O_S", this.NumbersUser);
             
               ret = ret.Replace("SERVER_NAME", this.Server_Name);
                return ret;
            }
        }


        private List<System.Net.Mail.Attachment> _Adjuntos = null;
        public List<System.Net.Mail.Attachment> Adjuntos
        {
            get {
                if (_Adjuntos == null)
                    _Adjuntos = new List<System.Net.Mail.Attachment>();
                return _Adjuntos;
            }
        }

        #endregion
    }
}
