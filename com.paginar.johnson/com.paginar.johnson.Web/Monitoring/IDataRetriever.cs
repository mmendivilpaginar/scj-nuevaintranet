﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.Web.Monitoring
{
   public interface IDataRetriever
    {

       Boolean EstaEnProduccion { get; }
        
        string User { get; }

        string Asunto
       {
           get;
       }
        string Port
       {
           get
           ;
       }

        string From
       {
           get
           ;
       }

        string To
       {
           get
           ;
       }

        string SmtpServer
       {
           get
           ;
       }


        string Password
       {
           get;
       }

        Boolean Enable_Ssl
       {
           get;
       }


      
    }
}
