﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.paginar.johnson.Web.Monitoring
{
    public class FilterByMail:IFilter
    {
        public IDataRetriever DataByFilter;
        public FilterByMail()
        {

        }

        public FilterByMail(IDataRetriever dataFilter)
        {
            this.DataByFilter = dataFilter;
        }
        public bool IsValid()
        {
            Boolean ret=true;

            if (!this.DataByFilter.EstaEnProduccion)
                ret = false;


            return ret;
        }
    }
}