﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="ASPPages.aspx.cs" Inherits="com.paginar.johnson.Web.ASPPages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<iframe name="IAsp" runat="server" ClientIDMode="Static" id="IAsp"  width="100%"  marginheight="0" marginwidth="0" scrolling="auto" frameborder="0" allowTransparency="true">
    
  <p>Your browser does not support iframes.</p>
</iframe>
<script language='javascript'>

    function getDocHeight(doc) {
        var docHt = 0, sh, oh;
        if (doc.height) {
            docHt = doc.height;
        }
        else if (doc.body) {
            if (doc.body.scrollHeight) docHt = sh = doc.body.scrollHeight;
            if (doc.body.offsetHeight) docHt = oh = doc.body.offsetHeight;
            if (sh && oh) docHt = Math.max(sh, oh);
        }
        return docHt;
    }
    function getReSize() {
        var iframeWin = window.frames['IAsp'];
        var iframeEl = window.document.getElementById ? window.document.getElementById('IAsp') : document.all ? document.all['IAsp'] : null;
        if (iframeEl && iframeWin) {
            var docHt = getDocHeight(iframeWin.document);
            if (docHt != iframeEl.style.height) iframeEl.style.height = docHt + 'px';
        }
        else { // FireFox
            var docHt = window.document.getElementById('IAsp').contentDocument.height;
            window.document.getElementById('IAsp').style.height = docHt + 'px';
        }
    }
    function getRetry() {
        getReSize();
        setTimeout('getRetry()', 500);
    }
    getRetry();
</script>
</asp:Content>
