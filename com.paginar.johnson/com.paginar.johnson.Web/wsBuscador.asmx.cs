﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using com.paginar.johnson.BL;
using com.paginar.johnson.DAL;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;


public class Fila
{
   public string row;
}


namespace com.paginar.johnson.Web
{
    /// <summary>
    /// Summary description for wsBuscador
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsBuscador : System.Web.Services.WebService
    {

        
        [WebMethod(EnableSession= true)]


        public List<Fila> Buscador(string busqueda)
        {
            Usuario u = new Usuario();
            u = Session["Usuario"] as Usuario;
            int clusterid = u.clusteridactual;
            

            ControllerUCHome cc = new ControllerUCHome();
            ControllerNoticias cn = new ControllerNoticias();
            ControllerAgendaTel ca = new ControllerAgendaTel();
            //DsUCHome.UsuarioDataTable dtu = cc.GetBusquedaUsuario(busqueda);
            DsUCHome.UsuarioDataTable usu = cc.GetDataUsuarioUbicacion(u.usuarioid);
            string idUbic = null;
            foreach(DsUCHome.UsuarioRow usua in usu.Rows)
            {
                idUbic = usua.ubicacionid;
            }
            DsUCHome.UsuarioDataTable dtu = new DsUCHome.UsuarioDataTable();
            DSNoticias.infInfoDataTable dtn = new DSNoticias.infInfoDataTable();
            DSAgendaTel.AgendaTelDataTable dag = new DSAgendaTel.AgendaTelDataTable();

            if (!string.IsNullOrEmpty(busqueda) && busqueda.Length > 3)
            {

                dtu = cc.GetBusquedaUsuarioUbicacion(busqueda, idUbic);
                dtn = cn.GetBusquedaFE(busqueda, null, null, null, null, clusterid,null);
                dag = ca.GetBusqueda(null, null, busqueda);
            }
            
            List<Fila> html = new List<Fila>();
            Fila divresultados = new Fila();
            divresultados.row = "<div id='lista-resultados'>";
            html.Add(divresultados);

            
           int filascount = 0;


           if (dtu.Rows.Count > 0)
           {
               //PERSONAS            
               Fila headerpersonas = new Fila();
               headerpersonas.row = " <div class='bloque'> <h3>PERSONAS  </h3>";
               html.Add(headerpersonas);

               Fila vertodas = new Fila();
               vertodas.row = "<a  class='more' href='/nuestracompania/buscadordepersonas.aspx?Busqueda=" + busqueda + "' >Ver Todas</a>";
               html.Add(vertodas);


               string planta = "";
               string classcebra = "";

               foreach (DsUCHome.UsuarioRow dr in dtu.Rows)
               {

                   if (planta != dr.ubicaciondescripcion)
                   {
                       if (planta != "")
                       {
                           Fila fhooter = new Fila();
                           fhooter.row = "</div>";
                           html.Add(fhooter);
                       }

                       planta = dr.ubicaciondescripcion;
                       Fila fh = new Fila();
                       fh.row = " <div class='sub-bloque'> <h4>" + planta + "</h4>";
                       html.Add(fh);
                       Fila ulheader = new Fila();
                       ulheader.row = "<ul>";
                       html.Add(ulheader);
                       classcebra = "";
                   }

                   string ubicacion= !dr.IsubicaciondescripcionNull()?  dr.ubicaciondescripcion : "";
                   string direccion = !dr.IsdirecciondescripcionNull()? dr.direcciondescripcion :"";
                   string cargo = !dr.IsCargoDETNull() ? dr.CargoDET : "";
                   string area = !dr.IsAreaDESCNull() ? dr.AreaDESC : "";
                   string telefono = !dr.IsInternoNull() ? dr.Interno : "";

                   string htmluser = "";
                   if (classcebra == "ext par") classcebra = "ext impar";
                   else
                       classcebra = "ext par";

                   if (classcebra == "") classcebra = "ext par";
                   
                       htmluser += "<li class='" + classcebra + "'>";

                   if (!dr.IsUsuarioFotoNull())
                   {
                       htmluser += "<div class='pic'>";
                       htmluser += "<a href='/nuestracompania/buscadordepersonas.aspx?UsuarioID=" + dr.UsuarioID + "'  ><img src='../images/personas/" + dr.UsuarioFoto + "'  /></a>";
                       htmluser += "</div>";
                   }

                   htmluser += "<div class='content'>";
                   
                   //htmluser += "<a href='/nuestracompania/buscadordepersonas.aspx?UsuarioID=" + dr.UsuarioID + "'  >  " + getResultadoNegrita(dr.Apellido, busqueda) + " " + getResultadoNegrita(dr.Nombre, busqueda) + " " + "Telefono:" + getResultadoNegrita(telefono, busqueda) + " " + getResultadoNegrita(area, busqueda) + " " + getResultadoNegrita(cargo, busqueda) + "</a>";

                   htmluser += "<ul>";

                   htmluser += "<li><a href='/nuestracompania/buscadordepersonas.aspx?UsuarioID=" + dr.UsuarioID + "'  ><strong> " + getResultadoNegrita(dr.Apellido, busqueda) + " " + getResultadoNegrita(dr.Nombre, busqueda) + "</strong></a></li>";

                   htmluser += "<li>" + getResultadoNegrita(ubicacion, busqueda) + "</li>";

                   htmluser += "<li>" + getResultadoNegrita(direccion, busqueda) + "</li>";

                   htmluser += "<li>" + getResultadoNegrita(area, busqueda) + "</li>";

                   //htmluser += "<li>" + getResultadoNegrita(cargo, busqueda) + "</li>";

                   if(dr.ubicacionid=="600")
                       htmluser += "<li>Interno (Marcar primero conmutador):" + getResultadoNegrita(telefono, busqueda) + "</li>";
                   else
                       htmluser += "<li>Interno:" + getResultadoNegrita(telefono, busqueda) + "</li>";

                   htmluser += "<li><a href='/nuestracompania/buscadordepersonas.aspx?UsuarioID=" + dr.UsuarioID + "'><strong> Ver Ficha... </strong></a></li>";


                   htmluser += "</ul>";

                   htmluser += "</div>";


                   htmluser += "</li>";

                   Fila div = new Fila();
                   div.row = htmluser;
                   html.Add(div);

               }

               Fila ulfooter = new Fila();
               ulfooter.row = "</ul>";
               html.Add(ulfooter);

               Fila divsubloque = new Fila();
               divsubloque.row = "</div>";
               html.Add(divsubloque);

 

               Fila footerpersonas = new Fila();
               footerpersonas.row = " </div>  ";
               html.Add(footerpersonas);

               filascount++;
           }




           if (dtn.Rows.Count > 0)
           {

               string classcebra = "";

               Fila headernoticias = new Fila();
               headernoticias.row = " <div class='bloque'> <h3>NOTICIAS   </h3>";
               html.Add(headernoticias);

               Fila vertodas = new Fila();
               vertodas.row = "<a class='more'  href='/noticias/buscador.aspx?busqueda=" + busqueda + "' >Ver Todas</a>";
               html.Add(vertodas);


               //NOTICIAS
               Fila ulheader = new Fila();
               ulheader.row = "<ul>";
               html.Add(ulheader);

               string htmlnoticia = "";


               DataView dvn = new DataView(dtn, "", "", DataViewRowState.CurrentRows);
               dvn = GetTopDataViewRows(dvn, 10);

               foreach (DataRow dr in dvn.ToTable().Rows)
               {
                   if (classcebra == "par") classcebra = "impar";
                   else
                       classcebra = "par";

                   if (classcebra == "") classcebra = "par";

                   htmlnoticia = "<li class='" + classcebra + "'>";

                                //agregar class='ext' para poner foto algun dia

                   htmlnoticia += "<div class='content'>";

                   string fecha = "";

                   if(dr["FHRecordatorio"].ToString()!="")
                       fecha = String.Format("{0: dd/MM/yyyy}", dr["FHRecordatorio"]);
                   else
                     fecha = String.Format("{0: dd/MM/yyyy}", dr["FechaAsoc"]);
                   

                   htmlnoticia += "<a href='/noticias/noticia.aspx?infoID=" + dr["infoidaux"] + "' ><strong>" + fecha + " - " + getResultadoNegrita(dr["Titulo"].ToString(), busqueda) + "</strong></a>";

                   string copete = dr["Copete"].ToString().Length < 100 ? dr["Copete"].ToString() : dr["Copete"].ToString().Substring(0, 100) + "...";

                   htmlnoticia += "<div class='copete'>" + getResultadoNegrita(copete, busqueda) + "</div>";

                   htmlnoticia += "</div>";

                   htmlnoticia += "</li>";

                   Fila div = new Fila();
                   div.row = htmlnoticia;
                   html.Add(div);
                   filascount++;
               }



               Fila ulfooter = new Fila();
               ulfooter.row = "</ul>";
               html.Add(ulfooter);
           }


           //AGenda
           if (dag.Rows.Count > 0)
           {
               string classcebra = "";

               Fila headeragenda = new Fila();
               headeragenda.row = " <div class='bloque'> <h3>OTROS   </h3>";
               html.Add(headeragenda);


               Fila vertodas = new Fila();
               vertodas.row = "<a class='more'  href='/servicios/agendatel.aspx?busqueda=" + busqueda + "' >Ver Todas</a>";
               html.Add(vertodas);

               //AGENDA
               Fila ulheader = new Fila();
               ulheader.row = "<ul>";
               html.Add(ulheader);

               string htmlagenda = "";

               foreach (DataRow dr in  dag.Rows)
               {
                   if (classcebra == "par") classcebra = "impar";
                   else
                       classcebra = "par";

                   if (classcebra == "") classcebra = "par";

                   htmlagenda = "<li class='" + classcebra + "'>";
                   
                   htmlagenda += "<div class='content'>";

                   htmlagenda += dr["ubicaciondescripcion"] +"<br>";

                   htmlagenda += "Detalle: " + "<a href='/servicios/agendatel.aspx?agendatelid=" +dr["agendatelid"]+"' >" +   dr["detalle"] + " </a> <br>";

                   htmlagenda += "Teléfono: "+dr["telefono"] +" "+dr["interno"];
                   
                   htmlagenda += "</div>";

                   htmlagenda += "</li>";

                   Fila div = new Fila();
                   div.row = htmlagenda;
                   html.Add(div);
                   filascount++;
               }

               Fila ulfooter = new Fila();
               ulfooter.row = "</ul>";
               html.Add(ulfooter);

           }


           if (dtu.Rows.Count == 0 && dtn.Rows.Count == 0 && dag.Rows.Count == 0)
           {
               string htmlempty = "<ul>";
               htmlempty += "<li> No se encontraron Resultados </li>";;
               htmlempty += "</ul>";
               Fila empty   = new Fila();
               empty.row = htmlempty;
               html.Add(empty);
           }


           Fila tend = new Fila();
           tend.row = "</div><script> $('#BuscadorContent').fadeIn(); $('#BusquedaTextBox').removeAttr('disabled'); </script>";
           html.Add(tend);

           return html;
        }


       [WebMethod(EnableSession = true)]
        public  string[]  getpredictiveWords(string prefixText, int count)
        {


            Usuario u = new Usuario();
            u = Session["Usuario"] as Usuario;
            int clusterid = u.clusteridactual;


            ControllerUCHome cc = new ControllerUCHome();
            ControllerNoticias cn = new ControllerNoticias();
            ControllerAgendaTel ca = new ControllerAgendaTel();
            //DsUCHome.UsuarioDataTable dtu = cc.GetBusquedaUsuario(busqueda);
            DsUCHome.UsuarioDataTable usu = cc.GetDataUsuarioUbicacion(u.usuarioid);
            string idUbic = null;
            foreach (DsUCHome.UsuarioRow usua in usu.Rows)
            {
                idUbic = usua.ubicacionid;
            }
            DsUCHome.UsuarioDataTable dtu = new DsUCHome.UsuarioDataTable();
            DSNoticias.infInfoDataTable dtn = new DSNoticias.infInfoDataTable();
            DSAgendaTel.AgendaTelDataTable dag = new DSAgendaTel.AgendaTelDataTable();

            if (!string.IsNullOrEmpty(prefixText) && prefixText.Length > 3)
            {

                dtu = cc.GetBusquedaUsuarioUbicacion(prefixText, idUbic);
                //dtn = cn.GetBusquedaFE(prefixText, null, null, null, null, clusterid);
                //dag = ca.GetBusqueda(null, null, prefixText);
            }

            int n = dtu.Count; //+dtn.Count + dag.Count;

            List<string> sbwords = new List<string>();


            if (n > 0)
            {

                DataView dvn = new DataView(dtu, "", "", DataViewRowState.CurrentRows);
                dvn = GetTopDataViewRows(dvn, 10);

                string text="";
                foreach (DataRow dr in dvn.ToTable().Rows)
                {

                    text = dr["apellido"].ToString().ToLower() + " " + dr["nombre"].ToString();

                    if (!sbwords.Contains(text))
                    {
                        sbwords.Add(dr["apellido"].ToString() + " " + dr["nombre"].ToString());
                    }


                }

                //DataView dvn = new DataView(dtn, "", "", DataViewRowState.CurrentRows);
                //dvn = GetTopDataViewRows(dvn, 4);

                //foreach (DataRow dr in dvn.ToTable().Rows)
                //{
                //    if (dr["Titulo"].ToString().ToLower().Contains(prefixText.ToLower()))
                //    sbwords.Add(dr["Titulo"].ToString());
                //}

                //foreach (DataRow dr in dag.Rows)
                //{
                //    if (dr["detalle"].ToString().ToLower().Contains(prefixText.ToLower()))
                //    sbwords.Add(dr["detalle"].ToString());
                //}


            }



            return sbwords.ToArray();
        }

        private DataView GetTopDataViewRows(DataView dv, Int32 n)
        {
            DataTable dt = dv.Table.Clone();

            for (int i = 0; i < n - 1; i++)
            {
                if (i >= dv.Count)
                {
                    break;
                }
                dt.ImportRow(dv[i].Row);
            }
            return new DataView(dt, dv.RowFilter, dv.Sort, dv.RowStateFilter);
        }



        public string getResultadoNegrita(string pValor,string busqueda)
        {
            try
            {
                string pBusq = busqueda;
                
                if (pBusq.Length > 0)
                {
                    string v1 = pValor.Substring(pValor.ToLower().IndexOf(pBusq.ToLower()), pBusq.Length);
                    return Regex.Replace(pValor, pBusq, "<span style='font-weight:bold;color:#000000'>" + v1 + "</span>", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                }
                else
                    return pValor;
            }
            catch (Exception ex)
            {
                return pValor;
            }
        }


        [WebMethod(EnableSession = true)]

        public string[] getusermails(string prefixText, int count)
        {
            ControllerUsuarios cu = new ControllerUsuarios();
            List<string> mails = new List<string>();
            
            cu.getUsuariosmails(prefixText);

            foreach (DataRow dr in cu.getUsuariosmails(prefixText).Rows)
            {
                
                    mails.Add(dr["email"].ToString()) ;
               


            }

            return mails.ToArray(); ;
        }
    }





}

