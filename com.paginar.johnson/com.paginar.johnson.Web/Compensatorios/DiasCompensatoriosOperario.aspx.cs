﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.johnson.membership;

namespace com.paginar.johnson.Web.Compensatorios
{
    public partial class DiasCompensatoriosOperario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Usuarios usercontroller = new Usuarios();
            User Usuario = null;
            if (!Page.IsPostBack)
            {
                Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);
                HiddenFieldUsuarioID.Value = Usuario.GetLoginId().ToString();

                if (!Page.User.IsInRole("Días Compensatorio Operarios"))
                {
                    TabContainer1.Tabs[0].Enabled = false;
                    TabContainer1.Tabs[1].Enabled = false;
                    TabContainer1.ActiveTabIndex = 2;
                }

                if (!Page.User.IsInRole("Jefes Compensatorios"))
                    TabContainer1.Tabs[2].Enabled = false;

            }
        }
        protected void ButtonSCompensatorio_Click(object sender, EventArgs e)
        {
            string RedirectURL;
            RedirectURL = Request.Url.Host + "/SessionTransfer.aspx?dir=2asp&url=" + "servicios/form_SolicitudCompensatorioOperario.asp?FormularioID=25&ASP=form_SolicitudCompensatorioOperario.asp";
            EjecutarScript(string.Format("self.parent.location='{0}'", RedirectURL));
        }

        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "IF100", js, true);

        }
    }
}