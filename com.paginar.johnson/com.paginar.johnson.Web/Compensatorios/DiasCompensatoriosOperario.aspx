﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/OneSidebarLeft.master" AutoEventWireup="true" CodeBehind="DiasCompensatoriosOperario.aspx.cs" Inherits="com.paginar.johnson.Web.Compensatorios.DiasCompensatoriosOperario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <h2>
        Francos Compensatorios</h2>
<asp:HiddenField ID="HiddenFieldUsuarioID" runat="server" />
    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
        <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Bienes">
            <HeaderTemplate>
                Días a Compensar</HeaderTemplate>
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <p>
                                        <asp:Label ID="Label4" runat="server" CssClass="destacados" Text="Estos son los días que dispone para solicitar un franco compensatorio"></asp:Label>
                                    </p>
                                    <asp:GridView ID="GridViewFeriadosTrabajados" runat="server" AutoGenerateColumns="False"
                                        DataKeyNames="ID" DataSourceID="ObjectDataSourceFeriadosTrabajados" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Domingo Trabajado" SortExpression="FeriadoTrabajado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FeriadoTrabajado", "{0:dd/MM/yyyy}") %>'></asp:Label></ItemTemplate>
                                                <HeaderTemplate>
                                                    <strong>
                                                        <asp:LinkButton ID="lnkFeriadoTrabajado" runat="server" CommandName="Sort" CssClass="destacados"
                                                            CommandArgument="FeriadoTrabajado" Text="Domingo Trabajado">
                                                        </asp:LinkButton></strong></HeaderTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vencimiento">
                                                <ItemTemplate>
                                                    <%--<asp:Label ID="TextBox1" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",DateTime.Parse(Eval("FechaSolicitud").ToString())) %>'></asp:Label>---%>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",DateTime.Parse(Eval("FeriadoTrabajado").ToString()).AddDays(15)) %>'></asp:Label></ItemTemplate>
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DiaSolicitado" HeaderText="Día Solicitado" DataFormatString="{0:dd/MM/yyyy}"
                                                ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Estado") %>'></asp:Label></ItemTemplate>
                                                <HeaderTemplate>
                                                    <strong>
                                                        <asp:LinkButton ID="lnkEstado" runat="server" CommandName="Sort" CssClass="destacados"
                                                            CommandArgument="Estado" Text="Estado">
                                                        </asp:LinkButton></strong></HeaderTemplate>
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ObservacionesOperario" HeaderText="Obs. Operario" ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObservacionesJefe" HeaderText="Obs. Jefe" ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObservacionesRRHH" HeaderText="Obs. RR.HH." ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            
                                                <div class="messages msg-info">
                                       Ud. no cuenta con días compensados en esta sección.
                                        </div>
                                                </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceFeriadosTrabajados" runat="server" DeleteMethod="Delete"
                                        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByBusqueda"
                                        TypeName="com.paginar.johnson.dataaccess.DSCompensatoriosTableAdapters.frmFeriadosTrabajadosTableAdapter"
                                        UpdateMethod="Update">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldUsuarioID" DefaultValue="" Name="UsuarioID"
                                                PropertyName="Value" Type="Int32" />
                                            <asp:Parameter DefaultValue="OPERARIO_PENDIENTES_A_TOMAR" Name="MODO" Type="String" />
                                            <asp:Parameter DefaultValue="" Name="FechaDesde" Type="DateTime" />
                                                                                    <asp:Parameter Name="FechaHasta" Type="DateTime" />
                                                                                    <asp:Parameter Name="Legajo" Type="Int32" />
                                                                                    <asp:Parameter Name="FormularioID" Type="Int32" />
                                                                                    <asp:Parameter Name="Liquidado" Type="Boolean" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <br />
                            <asp:Button ID="ButtonSCompensatorio" runat="server" Text="Solicitar Compensatorio"
                                OnClick="ButtonSCompensatorio_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Historial">
            <ContentTemplate>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                            <p>
                                <asp:Label ID="Label5" CssClass="destacados" runat="server" Text="Días Compensados"></asp:Label></p>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewHistorial" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                        DataSourceID="ObjectDataSourceHistorial" AllowSorting="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Domingo Trabajado" SortExpression="FeriadoTrabajado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FeriadoTrabajado", "{0:dd/MM/yyyy}") %>'></asp:Label></ItemTemplate>
                                                <HeaderTemplate>
                                                    <headertemplate><strong><asp:LinkButton ID="lnkFeriadoTrabajado" runat="server" CommandName="Sort" CssClass="destacados"
                               CommandArgument="FeriadoTrabajado" Text="Domingo Trabajado"> </asp:LinkButton></strong></headertemplate>
                                                </HeaderTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Día Compensado" SortExpression="DiaSolicitado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DiaSolicitado", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    <strong>
                                                        <asp:LinkButton ID="lnkDiaSolicitado" runat="server" CommandName="Sort" CssClass="destacados"
                                                            CommandArgument="DiaSolicitado" Text="Día Compensado">
                                                        </asp:LinkButton>
                                                    </strong>
                                                </HeaderTemplate>
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Estado") %>'></asp:Label></ItemTemplate>
                                                <HeaderTemplate>
                                                    <strong>
                                                        <asp:LinkButton ID="lnkEstado" runat="server" CommandName="Sort" CssClass="destacados"
                                                            CommandArgument="Estado" Text="Estado">
                                                        </asp:LinkButton></strong></HeaderTemplate>
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ObservacionesOperario" HeaderText="Obs. Operario" ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObservacionesJefe" HeaderText="Obs. Jefe" ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ObservacionesRRHH" HeaderText="Obs. RR.HH." ReadOnly="True">
                                                <HeaderStyle CssClass="destacados" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <div class="messages msg-info">No posee días compensados.
                                        </div>
                                           </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSourceHistorial" runat="server" DeleteMethod="Delete"
                                        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByBusqueda"
                                        TypeName="com.paginar.johnson.dataaccess.DSCompensatoriosTableAdapters.frmFeriadosTrabajadosTableAdapter"
                                        UpdateMethod="Update">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="HiddenFieldUsuarioID" DefaultValue="" Name="UsuarioID"
                                                PropertyName="Value" Type="Int32" />
                                            <asp:Parameter DefaultValue="OPERARIO_HISTORIAL" Name="MODO" Type="String" />
                                            <asp:Parameter DefaultValue="" Name="FechaDesde" Type="DateTime" />
                                                                                    <asp:Parameter Name="FechaHasta" Type="DateTime" />
                                                                                    <asp:Parameter Name="Legajo" Type="Int32" />
                                                                                    <asp:Parameter Name="FormularioID" Type="Int32" />
                                                                                    <asp:Parameter Name="Liquidado" Type="Boolean" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel ID="TabPanelReporteJefes" runat="server" HeaderText="Reporte">
        <ContentTemplate> 
        <div class="framemargin">
       
         <table class="destacados">
                    <tr>
                    <td>Usuario a Cargo</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlUsrCargo" runat="server" 
                            DataSourceID="ObjectDataSource4" DataTextField="apenom" 
                            DataValueField="usuarioid" AppendDataBoundItems="True" >
                            <asp:ListItem Value="0">Todos</asp:ListItem>
                         </asp:DropDownList> </td>
                     <td colspan="4">
                        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" 
                                                OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByJefe" 
                                                TypeName="com.paginar.johnson.dataaccess.DSCompensatoriosTableAdapters.frmUsuariosTableAdapter">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="HiddenFieldUsuarioID" Name="usuarioID" 
                                                        PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource> </td> 
                    </tr>
                    <tr>
                    <td>Fecha Desde</td>
                    <td> 
                    <asp:TextBox ID="txtFechaDesde" runat="server"></asp:TextBox>&nbsp;
                        <asp:RegularExpressionValidator Text="*" ID="RegularExpressionValidator3" runat="server" 
                        ControlToValidate="txtFechaDesde" ValidationExpression="^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$" 
                        ErrorMessage="El formato de la fecha Desde es incorrecta. El formato de fecha es dd/mm/aaaa." 
                        ValidationGroup="Buscar" Display="Dynamic"></asp:RegularExpressionValidator>
                                <cc1:CalendarExtender ID="ceFechaDesde" runat="server" 
                                    Format="dd/MM/yyyy" PopupButtonID="imgbFechaDesde" 
                                    TargetControlID="txtFechaDesde">
                                </cc1:CalendarExtender> </td>
                       <td>     
                                <asp:ImageButton ID="imgbFechaDesde" runat="server" 
                                    ImageUrl="~/images/Calendar.png" /></td>
                    <td>Fecha Hasta</td>
                    <td>
                        <asp:TextBox ID="txtFechaHasta" runat="server"></asp:TextBox>&nbsp;
                        <asp:RegularExpressionValidator Text="*" ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtFechaHasta" ValidationExpression="^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$" 
                        ErrorMessage="El formato de la fecha hasta es incorrecta. El formato de fecha es dd/mm/aaaa." 
                        ValidationGroup="Buscar" Display="Dynamic"></asp:RegularExpressionValidator>
                                <cc1:CalendarExtender ID="ceFechaHasta" runat="server" 
                                    Format="dd/MM/yyyy" PopupButtonID="imgbFechaHasta" 
                                    TargetControlID="txtFechaHasta">
                                </cc1:CalendarExtender>
                     </td>
                      <td>      
                                <asp:ImageButton ID="imgbFechaHasta" runat="server" 
                                    ImageUrl="~/images/Calendar.png" />
                    </td>
                    </tr>
                    <tr>
                    <td>Estado</td>
                    <td><asp:DropDownList ID="ddlEstado" runat="server">
                         <asp:ListItem value="-1">Todos</asp:ListItem>
                         <asp:ListItem Value="1">En Tramite</asp:ListItem>
                         <asp:ListItem Value="0">Aprobados</asp:ListItem>
                         <asp:ListItem Value="3">Rechazados</asp:ListItem>
                         <asp:ListItem Value="4">N/A</asp:ListItem>                  
                        </asp:DropDownList>
                    </td>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                    <td colspan="6" style="text-align:right">    
                         <asp:Button ID="ButtonBuscar" runat="server" Text="Buscar" 
                                    ValidationGroup="Buscar" />
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6">
                    <asp:ValidationSummary ID="vdSBuscar" runat="server" 
                                    ValidationGroup="Buscar" />
                    </td>
                    </tr>
             </table>
    <asp:GridView ID="gvReporte" runat="server" AutoGenerateColumns="False" 
        DataSourceID="ObjectDataSource1" AllowSorting="True" >
        <Columns>
            <asp:TemplateField HeaderText="Legajo" SortExpression="legajosolicitante">
              <HeaderTemplate>
                  <strong>
                  <asp:Label ID="lblLegajo" runat="server" Text="Legajo"></asp:Label>
                  </strong>
              </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("legajosolicitante") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" 
                        Text='<%# Bind("legajosolicitante") %>'></asp:TextBox>
                </EditItemTemplate>
                <HeaderStyle CssClass="destacados" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Solicitante" 
                SortExpression="nombreapellidosolicitante">
                 <HeaderTemplate>
                  <strong>
                  <asp:Label ID="lblSolicitante" runat="server" Text="Solicitante"></asp:Label>
                  </strong>
              </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" 
                        Text='<%# Bind("nombreapellidosolicitante") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="Label2" runat="server" 
                        Text='<%# Eval("nombreapellidosolicitante") %>'></asp:Label>
                </EditItemTemplate>
                <HeaderStyle CssClass="destacados" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Feriado/Domingo Trabajado" 
                SortExpression="feriadotrabajado">
                <HeaderTemplate>
              <strong>
                <asp:LinkButton ID="lnkFeriadoTrabajado" runat="server" CommandName="Sort" CssClass="destacados"
                    CommandArgument="FeriadoTrabajado" Text="Feriado/Domingo <br /> Trabajado">
                </asp:LinkButton>
            </strong>
                
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("feriadotrabajado", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" 
                        Text='<%# Bind("feriadotrabajado", "{0:dd/MM/yyyy}") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dia Compensado" 
                SortExpression="diacompensatorio">
                
             <HeaderTemplate>
              <strong>
                <asp:LinkButton ID="lnkDiaCompensado" runat="server" CommandName="Sort" CssClass="destacados"
                    CommandArgument="diacompensatorio" Text="Día Compensado">
                </asp:LinkButton>
            </strong>
                
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("diacompensatorio", "{0:dd/MM/yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" 
                        Text='<%# Bind("diacompensatorio") %>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                 <HeaderTemplate>
                 <strong>
                    <asp:LinkButton ID="lnkEstado" runat="server" CommandName="Sort" CssClass="destacados"
                    CommandArgument="Estado" Text="Estado">
                 </asp:LinkButton>
                </strong>
                
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
         <EmptyDataTemplate>
            
                <div class="messages msg-info">El usuario seleccionado no posee Francos Compensados
                                        </div>
        </EmptyDataTemplate>

    </asp:GridView>
     <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
          OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataBusqueda"         
        
        
        TypeName="com.paginar.johnson.dataaccess.DSCompensatoriosTableAdapters.frmReporteFeriadosTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="REPORTE_JEJE" Name="MODO" Type="String" />
            <asp:ControlParameter ControlID="ddlUsrCargo" DefaultValue="" Name="usuarioID" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="HiddenFieldUsuarioID" DefaultValue="" 
                Name="JefeID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtFechaDesde" DefaultValue="" 
                Name="FechaDesde" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="txtFechaHasta" DefaultValue="" 
                Name="FechaHasta" PropertyName="Text" Type="DateTime" />
            <asp:ControlParameter ControlID="ddlEstado" DefaultValue="" Name="Estado" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
</asp:ObjectDataSource>
            
        
      </div>       
        </ContentTemplate>
        </cc1:TabPanel>
        
    </cc1:TabContainer>
</asp:Content>
