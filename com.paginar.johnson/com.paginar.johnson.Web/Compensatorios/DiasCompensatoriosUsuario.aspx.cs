﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.paginar.johnson.Web.Compensatorios
{
    public partial class DiasCompensatoriosUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.User.IsInRole("Días Compensatorio Administrativos"))
            {
                Response.Redirect("~/Compensatorios/DiasCompensatoriosAdministrativos.aspx");
                return;
            }else if (Page.User.IsInRole("Días Compensatorio Operarios"))
            {
                    Response.Redirect("~/Compensatorios/DiasCompensatoriosOperario.aspx");
                    return;
            }
            else
            {
                TimerRedireccion.Enabled = true;  

            }
        }

        protected void TimerRedireccion_Tick(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }
    }
}