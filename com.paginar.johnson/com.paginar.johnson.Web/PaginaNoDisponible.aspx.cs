﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace com.paginar.johnson.Web
{
    public partial class PaginaNoDisponible : System.Web.UI.Page
    {
        private string IsWindowNomal = "IsWindowNormal";

        private string Page_Default = "Default.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.Request.QueryString.AllKeys.Contains(IsWindowNomal))
                {
                    RedirectToLogin();
                }
            }
        }

       

        private void RedirectToLogin()
        {
            this.Session.Clear();
            this.Session.Abandon();
            this.Response.Redirect(Page_Default);
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            StringBuilder urlToRedirect = new StringBuilder(System.IO.Path.GetFileName(Request.Path));
            urlToRedirect.Append("?");
            urlToRedirect.Append(IsWindowNomal);
            urlToRedirect.Append("=1");
            string scr = "<script language='javascript'> if (typeof(window.opener) =='undefined'){ document.location.href='" + urlToRedirect + "' } else window.close();</script>";
            Response.Write(scr);
        }
    }
}