//var CursosDisponiblesAux;
//var CursosSeleccionadosAux;
var flagCierre = 2;
//var selectList_cache; 
$(document).ready(function () {

   // LlenarCursosAux();
    SetCursoSeleccionados();//hiddenFieldCursosSeleccionados
   // SetDebilidadesSeleccionadas();// llena los radio button con debilidades
   // FiltrarListboxCursos();//filtra cursos segun debilidad seleccionada

    //SoportePNG();	
    ActivarTabs();
    // GridView();
    TextArea();
    Buttons();
    RoundedCorners();
    toolTip();
    validarpestanas();

    SetOnblurFuntionInControls();
    VisualizarObjetivos();



});


function toolTip() {

    //personalizando el tooltip
    $('.masInfo').cluetip({
        splitTitle: '|',
        sticky: true,
        arrows: true,
        cursor: 'pointer',
        mouseOutClose: true
    });

  }

function PreguntaImprimirEvaluacion(Legajo, PeriodoID, TipoFormularioID) {
    flagCierre = 3;
    if (confirm(String.fromCharCode(191) + 'Desea Imprimir/Exportar la Evaluaci\u00f3n?'))
        ImprimirEvaluacionFY(Legajo, PeriodoID, TipoFormularioID);
}

function ImprimirEvaluacionFY(Legajo, PeriodoID, TipoFormularioID) {
    popup('Impresion.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID, 1000, 600);
}

function ImprimirSeccion() {

    var TipoFormularioID = $('input[name$=HiddenFieldTipoFormularioID]').val();
    var PeriodoID = $('input[name$=HiddenFieldPeriodoID]').val();
    var Legajo = $('input[name$=HiddenFieldLegajo]').val();
    var seccion = $('select[name$=DropDownListImprimirSeccion').find('option:selected').val();

    popup('Impresion.aspx?Legajo=' + Legajo + '&PeriodoID=' + PeriodoID + '&TipoFormularioID=' + TipoFormularioID + '&Seccion=' + seccion, 1000, 600);


}



function popup(url, ancho, alto) {
    var posicion_x;
    var posicion_y;
    posicion_x = (screen.width / 2) - (ancho / 2);
    posicion_y = (screen.height / 2) - (alto / 2);
    window.open(url, "PmpAdministrativosFY_Print", "width=" + ancho + ",height=" + alto + ",menubar=0,toolbar=0,directories=0,scrollbars=yes,resizable=no,left=" + posicion_x + ",top=" + posicion_y + "");

}

function AgregarObjetivo(num) {
    

    $('#DivObjetivo' + num).show();

  
    $('input[name$=ButtonShowDivObjetivo' + num + ']').hide();
}

function VisualizarObjetivos() {

    var TieneCantedidoCargado = false;


    $('#RepeaterIndividualObjetives10').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives10 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        AgregarObjetivo(6);
        AgregarObjetivo(7);
        AgregarObjetivo(8);
        AgregarObjetivo(9);
        AgregarObjetivo(10);
        return;
    }


    $('#RepeaterIndividualObjetives9').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives9 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        AgregarObjetivo(6);
        AgregarObjetivo(7);
        AgregarObjetivo(8);
        AgregarObjetivo(9);        
        return;
    }

    $('#RepeaterIndividualObjetives8').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives8 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        AgregarObjetivo(6);
        AgregarObjetivo(7);
        AgregarObjetivo(8);
        
        return;
    }

    $('#RepeaterIndividualObjetives7').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives7 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        AgregarObjetivo(6);
        AgregarObjetivo(7);
        

        return;
    }

    $('#RepeaterIndividualObjetives6').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives6 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        AgregarObjetivo(6);
        


        return;
    }

    $('#RepeaterIndividualObjetives5').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives5 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        AgregarObjetivo(5);
        return;
    }

    $('#RepeaterIndividualObjetives4').find('textarea').each(function () {
        if ($(this).val().length != 0)
            TieneCantedidoCargado = true;

    });

    $('#RepeaterIndividualObjetives4 select').each(function () {
        if ($(this).val() != 0)
            TieneCantedidoCargado = true;

    });

    if (TieneCantedidoCargado) {
        AgregarObjetivo(4);
        return;
    }


}


function ActivarTabs() {
    $("#tabs").tabs();
    if ($('input[name$=HiddenFieldTipoFormularioID]').val() == '10')
        $("#tabs").tabs("remove", 5);

    $('.ui-tabs-panel').wrapInner('<div class="ui-tabs-panel-inner" />');

    $("#tabs").bind('tabsselect', function (event, ui) {
        $('.masInfo').trigger('hideCluetip');
    });

}



function Buttons() {

    $('.button').each(function () { if ($(this).html() == "")  $(this).remove();});
     
    $('input.formSubmit').each(function(){
    if(! $(this).parent().hasClass('button'))
        $(this).wrap('<span class="button"></span>');
                                        });

    $('a.button').wrapInner('<span class="inner" />');
}







function TextArea() {

     //tamano maximio de caracteres en textarea
    $('#IndividualObjetive textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 500, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    $('#RepeaterDEVELOPMENTACTIONS textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 700, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    $('#RepeaterPerformanceReviewSummary textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 8000, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    $('#DataListExperience textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 500, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    $('#RepeaterResume textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 330, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });


    $('#RepeaterCareerPlanLongTerm textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 240, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    $('#RepeaterCareerPlanNearTerm textarea').maxlength({
        events: [], // Array of events to be triggerd    
        maxCharacters: 240, // Characters limit   
        status: true, // True to show status indicator bewlow the element    
        statusClass: "status", // The class on the status div  
        statusText: "", // The status text  
        notificationClass: "notification", // Will be added when maxlength is reached  
        showAlert: false, // True to show a regular alert message    
        alertText: "You have typed too many characters.", // Text in alert message   
        slider: false // True Use counter slider    
    });

    

    
    

   

}

function RoundedCorners() {
    DD_roundies.addRule('#personas', '5px', true);
    DD_roundies.addRule('#foto', '5px', true);
}

function SoportePNG() {
    if ($.browser.msie) {
        if ($.browser.version >= 7) {
            //codigo para ie7 y superior
        } else {
            //codigo para ie6
            DD_belatedPNG.fix('img');
        }
    }
}






//function CallServiceGetCursosByDebilidad(TipoFormularioID, AreaID, Debilidad) {


//    var jsonParameter = "{'TipoFormularioID': '" + TipoFormularioID + "', 'AreaID': '" + AreaID + "' , 'Debilidad': '" + Debilidad + "' }";

//    $.ajax({
//        type: "POST",
//        url: "../PMP/WebServiceEvaluaciones.asmx/GetCursosByDebilidad",
//        contentType: "application/json; charset=utf-8",
//        data: jsonParameter,
//        dataType: "json",
//        success: OnSuccess,
//        error: OnError
//    });

//    function OnSuccess(data, status) {
//        CursosDisponiblesAux.empty();

//        for (i = 0; i < data.d.length; i++) {
////            $('select[name$=ListBoxCursosDisponibles]').append($('<option>', { value: data.d[i].CursoID }).text(data.d[i].CursoDescripcion));
////            $('select[name$=ListBoxCursosDisponibles] option:last').attr('deb', data.d[i].DebilidadID);
//            //            $('select[name$=ListBoxCursosDisponibles] option:last').attr('value', data.d[i].CursoID);

//            CursosDisponiblesAux.append($('<option>', { value: data.d[i].CursoID }).text(data.d[i].CursoDescripcion));
//            $(CursosDisponiblesAux).find('option:last').attr('deb', data.d[i].DebilidadID);
//            $(CursosDisponiblesAux).find('option:last').attr('value', data.d[i].CursoID);
//             
//        }

//       // LlenarCursosAux();


//    }

//    function OnError(request, status, error) {

//    }


//}




//function  FiltrarListboxCursos() {
//    
//    var val = $('input[name=Debilidades]:checked').val();


//    $('select[name$=ListBoxCursosDisponibles]').empty();

//   
//    var CursosDisponibles = CursosDisponiblesAux.find('option').clone();
//    var CursosSeleccionados = CursosSeleccionadosAux.find('option').clone();

//    

//    CursosDisponibles.filter(function (idx) {


//        return val === 'ALL' || $(this).attr("deb").indexOf('[' + val + ']') >= 0;


//    }).appendTo('select[name$=ListBoxCursosDisponibles]');


//}

//function LlenarCursosAux() {
// 
//    CursosDisponiblesAux = $('select[name$=ListBoxCursosDisponibles]').clone();
//    CursosSeleccionadosAux = $('select[name$=ListBoxCursosSeleccionados]').clone();
//}


//function SetDebilidadesSeleccionadas() {
//    var Debilidades = "";
//   
//    var length = $('#RepeaterDevelopmentNeeds tr').find("select").length;

//   // $('select[name$=ListBoxNecesidades]').append($('<option>', { value: '0' }).text('ALL'));

//    $('#RepeaterDevelopmentNeeds tr').find("select")
//                .each(function (index, element) {

//                    //lleno el listbox de necesidades con las nuevas necesidades seleccionadas
//                    if ($(this).find('option:selected').val() != 0 && !$('#DivNecesidades input[value=' + $(this).find('option:selected').val() + ']').length) {

//                        var labeltitulo = '<label class="Tolltipx" title="Pasos para seleccionar los cursos: <br/>Paso 1: seleccionar necesidad. <br/>Paso 2: seleccionar 1 o m�s cursos.<br/> <br/> Nota: Debe seleccionarse, al menos 1 curso para cada necesidad de desarrollo.">' + $(this).find('option:selected').text() + '</label>';
//                        $('#DivNecesidades').append('<input type="radio" name="Debilidades" Text="' + $(this).find('option:selected').text() + '" value="' + $(this).find('option:selected').val() + '" onclick="FiltrarListboxCursos()" /> ' + labeltitulo + ' <br/>');

//                        // title="Steps to select the courses: <br/>Step 1: Select necessity <br/>Step 2: Select 1 or more courses. "



//                        //concateno las necesidades
//                        if (index < length - 1)
//                            Debilidades = Debilidades + $(this).find('option:selected').val() + ',';

//                        else
//                            Debilidades = Debilidades + $(this).find('option:selected').val();
//                    }



//                });


//                $('input[name$=HiddenFieldDebilidadesSeleccionadas]').val(Debilidades);

//                $('.Tolltipx').cluetip({ splitTitle: '|', cursor: 'pointer', mouseOutClose: true });



// }

 function SetCursoSeleccionados() {
     var Cursos = "";
     var length = $('select[name$=ListBoxCursosSeleccionados]').find('option').length;

     $('select[name$=ListBoxCursosSeleccionados]').find('option')
                .each(function (index, element) {

                      //concateno las necesidades
                        if (index < length - 1)
                            Cursos = Cursos + $(this).val() + ',';

                        else
                            Cursos = Cursos + $(this).val();

                });

    $('input[name$=HiddenFieldCursosSeleccionados]').val(Cursos);


 }


// function LlenarListboxCursosDisponibles(e) {

//	var NecesitaConfirmar = ((CursosSeleccionadosAux.find('option').length > 0)); //&&(selIndex >0));
//	//var confirma = ((!NecesitaConfirmar))?true:alert("Se eliminaran los Cursos Seleccionados.");

//	if (NecesitaConfirmar)
//	   alert("Se eliminaran los Cursos Seleccionados.");

//        var TipoFormularioId = $('input[name$=HiddenFieldTipoFormularioID]').val();
//        var AreaID = $('input[name$=HiddenFieldAreaID]').val();

//        
//       
//        $('#DivNecesidades').empty();
//       
//       
//        $('select[name$=ListBoxCursosSeleccionados]').empty();
//        $('select[name$=ListBoxCursosDisponibles]').empty();

//        CursosSeleccionadosAux = $('select[name$=ListBoxCursosSeleccionados]').clone();

//        SetDebilidadesSeleccionadas();//llena radio buttons

//            
//        

//        var Debilidades = $('input[name$=HiddenFieldDebilidadesSeleccionadas]').val();
//        if (Debilidades != "")//llena la lista con los nuevos cursos
//           CallServiceGetCursosByDebilidad(TipoFormularioId, AreaID,  Debilidades);

//      }




function Autocompletar() {
    $('#tabs select').each(function () {

        $(this).val($(this).find('option:last').val());
    });

    $('#tabs input[type=text]').each(function () {

        $(this).val($(this).attr('id'));
    });


}

function SetDEVELOPMENTACTIONSinMSD(value) {

    $('#RepeaterSectionH tr:first').find('span:first').html(value);


}


function validarpestanas() {
  
    CheckObjetivos();
    CheckPerformanceReview();
    CheckCurrentStrengthsDevelopmentNeeds();
    CheckDevelopmentActions();
    CheckSummary();

    var TipoFormularioID = $('input[name$=HiddenFieldTipoFormularioID]').val();
    if (TipoFormularioID!=10)
        CheckMSAndD();

    CheckResume();
    CheckCareerPlan();
    CheckFeedBack();


}

function SetOnblurFuntionInControls() {
    $('#tabs-1 textarea').blur(function () { CheckObjetivos(); });
    $('#tabs-1 select').blur(function () { CheckObjetivos(); });

    $('#tabs-2 select').blur(function () { CheckPerformanceReview(); });

    $('#tabs-3 select').blur(function () { CheckCurrentStrengthsDevelopmentNeeds(); CheckDevelopmentActions(); });


    $('#tabs-4 select').blur(function () { CheckDevelopmentActions(); });
    $('#tabs-4 textarea').blur(function () { CheckDevelopmentActions(); });

    $('#tabs-5 select').blur(function () { CheckSummary(); });
    $('#tabs-5 textarea').blur(function () { CheckSummary(); });

    var TipoFormularioID = $('input[name$=HiddenFieldTipoFormularioID]').val();
    if (TipoFormularioID != 10) {

        $('#tabs-6 select').blur(function () { CheckMSAndD(); });
        $('#tabs-6 input[type=text]').blur(function () { CheckMSAndD(); });

    }

    $('#tabs-7 textarea').blur(function () { CheckResume(); });
    $('#tabs-7 input[type=text]').blur(function () { CheckResume(); });

    $('#tabs-8 select').blur(function () { CheckCareerPlan(); });
    $('#tabs-8 textarea').blur(function () { CheckCareerPlan(); });

    $('#tabs-9 input[type=text]').blur(function () { CheckFeedBack(); });
    


}

function CheckObjetivos() {

    var IsValid = true;

    $('#RepeaterIndividualObjetives1').find('textarea').each(function () {
        if ($(this).val().length == 0)
            IsValid = false;

    });

    $('#RepeaterIndividualObjetives1 select').each(function () {
        if ($(this).val() == 0)
            IsValid = false;

    });


    $('#RepeaterIndividualObjetives2').find('textarea').each(function () {
        if ($(this).val().length == 0)
            IsValid = false;

    });

    $('#RepeaterIndividualObjetives2 select').each(function () {
        if ($(this).val() == 0)
            IsValid = false;

    });

    $('#RepeaterIndividualObjetives3').find('textarea').each(function () {
        if ($(this).val().length == 0)
            IsValid = false;

    });

    $('#RepeaterIndividualObjetives3 select').each(function () {
        if ($(this).val() == 0)
            IsValid = false;

    });


    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#t1').removeClass('oKgreen');
        $('#tabs-1').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t1').addClass('oKgreen');
        $('#tabs-1').addClass('oKgreen');
    }

}

function CheckPerformanceReview() {
    var IsValid = true;

    $('#RepeaterSuccessFactors tr').each(function () {
        if ($(this).find('select[name$=DropDownListOptional]').length) {
           if( $(this).find('select[name$=DropDownListOptional]').val() != '' && $(this).find('select[name$=DropDownListRating]').val() == 0)
            IsValid = false;
        }
        else {
            if ($(this).find('select[name$=DropDownListRating]').val() == 0)
                IsValid = false;
             }


    });

    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#t2').removeClass('oKgreen');
        $('#tabs-2').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t2').addClass('oKgreen');
        $('#tabs-2').addClass('oKgreen');
    }
}

function CheckCurrentStrengthsDevelopmentNeeds() {

    var IsValid = true;

    $('#RepeaterSTRENGTHS select').each(function () {
       
            if ($(this).val() == 0)
                IsValid = false;

        });

        $('#RepeaterDevelopmentNeeds select').each(function () {

            if ($(this).val() == 0)
                IsValid = false;

        });



        if (!ValidarRepetidos('RepeaterSTRENGTHS'))
            IsValid = false;

        if (!ValidarRepetidos('RepeaterDevelopmentNeeds'))
            IsValid = false;

        if (!ValidarRepStregthsNeeds())
            IsValid = false;

    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#t3').removeClass('oKgreen');
        $('#tabs-3').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t3').addClass('oKgreen');
        $('#tabs-3').addClass('oKgreen');
    }
}

function ValidarRepStregthsNeeds() {
    IsValid = true;
    var grid1 = document.getElementById('RepeaterSTRENGTHS');
    var grid2 = document.getElementById('RepeaterDevelopmentNeeds');

    if (grid1.rows.length > 0 && grid2.rows.length > 0) {

        for (i = 0; i < grid1.rows.length; i++) {

            cell1 = grid1.rows[i];//.cells[1];
           // dropDown1 = cell1.childNodes[0];
           // var Fortaleza = dropDown1.options[dropDown1.selectedIndex].value;
            var Fortaleza = $(cell1).find('select option:selected').val();

            if (Fortaleza != 0) {
                for (j = 0; j < grid2.rows.length; j++) {

                    cell1 = grid2.rows[j];//.cells[1];
                   // dropDown2 = cell1.childNodes[0];


                   // var Necesidad = dropDown2.options[dropDown2.selectedIndex].value;
                    var Necesidad = $(cell1).find('select option:selected').val()
                    if (Fortaleza == Necesidad) {
                        IsValid = false;
                        
                    }
                }
            }
        }
    }
    return IsValid;
}

function ValidarRepetidos(repeater) {
   IsValid = true;

    var grid = document.getElementById(repeater);

    if (grid.rows.length > 0) {

        for (i = 0; i < grid.rows.length; i++) {
            cell1 = grid.rows[i];//.cells[1];
            var Value1 = $(cell1).find('select option:selected').val();


            if (Value1 != 0) {

                for (j = i + 1; j < grid.rows.length; j++) {

                    cell1 = grid.rows[j];//.cells[1];
                    var Value2 = $(cell1).find('select option:selected').val();
                    if (Value1 == Value2) {
                        IsValid = false;
                        return;
                    }
                }
            }

        }
    }

    return IsValid;



}

function ValidarRepetidosCareerInterest(repeater) {
    IsValid = true;

    var grid = document.getElementById(repeater);

    if (grid.rows.length > 0) {

        for (i = 0; i < grid.rows.length; i++) {
            cell1 = grid.rows[i]; //.cells[1];
            var Value1 = $(cell1).find('select option:selected').val();
            var Value1Descrip = $(cell1).find('select option:selected').text();

            if (Value1 != 0 && Value1Descrip!='Other') {

                for (j = i + 1; j < grid.rows.length; j++) {

                    cell1 = grid.rows[j]; //.cells[1];
                    var Value2 = $(cell1).find('select option:selected').val();
                    if (Value1 == Value2) {
                        IsValid = false;
                        return;
                    }
                }
            }

        }
    }

    return IsValid;



}

function CheckDevelopmentActions() {

    var IsValid = true;


    $('#RepeaterDEVELOPMENTACTIONS').find('textarea').each(function(){
    
            if($(this).val().length==0)
               IsValid=false;

            });

        var CursosSeleccionados = $('select[name$=ListBoxCursosSeleccionados]').find('option').length;
        if (CursosSeleccionados < 1)
            IsValid = false;

//$('#DivNecesidades input[type=radio]').each(function () {

//    val = $(this).val();


//    if (val != 0 && CursosSeleccionados.filter(function (idx) {
//        if ($(this).attr("deb")) return $(this).attr("deb").indexOf('[' + val + ']') >= 0;
//        else return false;
//    }).length == 0 && val != 'ALL')
//        IsValid = false;


//});


if (!IsValid) {
    //aqui cambiamos el class para cuando no es valido
    $('#t4').removeClass('oKgreen');
    $('#tabs-4').removeClass('oKgreen');
}
else {
    // aqui el class para el valido
    $('#t4').addClass('oKgreen');
    $('#tabs-4').addClass('oKgreen');
} 



     }

     function CheckSummary() {

         var IsValid = true;
         var PasoID= $('input[name$=HiddenFieldPasoID]').val();



         $('#RepeaterPerformanceReviewSummary tr').each(function () {



             if ($(this).find('textarea').length) {
                 var Comentario = $(this).find('textarea').val().length;

                 var ItemEvaluacionID = $(this).find('input[name*=HFID]').val();

                 if (((ItemEvaluacionID == 142 || ItemEvaluacionID == 174 || ItemEvaluacionID == 333) && PasoID == 1 && Comentario == 0) || ((ItemEvaluacionID == 143 || ItemEvaluacionID == 175 || ItemEvaluacionID == 334) && PasoID == 2 && Comentario == 0))
                     IsValid = false;

             }
         });

         $('#RepeaterPerformanceReviewSummary select').each(function () {

             if ($(this).val() == 0 && PasoID==1)
                IsValid = false;

    });




         if (!IsValid) {
             //aqui cambiamos el class para cuando no es valido
             $('#t5').removeClass('oKgreen');
             $('#tabs-5').removeClass('oKgreen');
         }
         else {
             // aqui el class para el valido
             $('#t5').addClass('oKgreen');
             $('#tabs-5').addClass('oKgreen');
         } 
}

function CheckMSAndD() {
    var IsValid = true;
    var PasoID = $('input[name$=HiddenFieldPasoID').val();

    //education

    $('#DataListEducation tr').each(function () {

        var year = $('input[name*=TextBoxYear]').val();
        var Degre = $('select[name*=DropDownListDegree').val();
        var School = $('input[name*=TextBoxSchool').val();


        if (year != '' || Degre != '' || School != '') {
            if (year == '' || Degre == '' || School == '')
               IsValid = false;

        }
    });

//mobility and travel
    if ($('#RepeaterMobilityAndTravel input[type=text]').val().length == 0)
        IsValid = false;

    $('#RepeaterMobilityAndTravel select').each(function(){
      if($(this).val==0)
       IsValid = false;
    });

  //language

  $('#RepeaterLanguage tr').each(function () {

      var Idioma = $('input[name*=TextBoxIdioma]').val();
      var Proficiency = $('select[name*=DropDownListProficiency]').val();
      var IsNative = $('input[name*=CheckBoxNative]').attr('checked');
      var Number = $('input[name*=HiddenFieldNumber]').val();


      if (Idioma != '' || Proficiency != '' || Number == 1) {
          if (Idioma == '' || Proficiency == '')
              IsValid = false;

      }

  });

  ///career interests

  $('#RepeaterCareerInterests select').each(function () {

      if ($(this).val() == 0)
          IsValid = false;
  });

  if (!ValidarRepetidosCareerInterest('RepeaterCareerInterests'))
      IsValid = false;

  //next assigment

  if (PasoID == 1) {

      $('#RepeaterNextAssigment select').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });


      if (!ValidarRepetidos('RepeaterNextAssigment'))
          IsValid = false;

      $('#RepeaterPotencialReplacements1 select').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });

      $('#RepeaterPotencialReplacements1 input[type=text]').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });

      $('#RepeaterPotencialReplacements2 select').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });

      $('#RepeaterPotencialReplacements2 input[type=text]').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });

      $('#RepeaterPotencialReplacements3 select').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });

      $('#RepeaterPotencialReplacements3 input[type=text]').each(function () {

          if ($(this).val() == 0)
              IsValid = false;
      });


  }

    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#t6').removeClass('oKgreen');
        $('#tabs-6').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t6').addClass('oKgreen');
        $('#tabs-6').addClass('oKgreen');
    } 
}

function CheckResume() {
    var IsValid = true;

    $('#DataListExperience table:first').each(function () {
        var FromDate = $(this).find('input[name*=TextBoxFromDate]').val();
        var ToDate = $(this).find('input[name*=TextBoxToDate]').val();
        if (FromDate.length == 0 || ToDate.length == 0)
            IsValid = false;
        else {
            if (ValidarFecha(FromDate) && ValidarFecha(ToDate)) {
                if (!ValidarRango(FromDate, ToDate))
                    IsValid = false;
            }

            else
                IsValid = false;
        }

        //var Company = TextBoxCompany      TextBoxPosition   TextBoxDescription
        $(this).find('textarea').each(function () {
            if ($(this).val().length == 0)
                IsValid = false;
        });

        $(this).find('input[type=text]').each(function () {
            if ($(this).val().length == 0)
                IsValid = false;
        });



    });



    $('#RepeaterResume table').each(function () {
        var ItemEvaluacionID = $(this).find('input[name*=HFID]').val();
        var Comentario= $(this).find('textarea').val().length;

        if(ItemEvaluacionID!=301 && ItemEvaluacionID!=304 && ItemEvaluacionID!=389 && Comentario==0)
            IsValid = false;

    });




    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#t7').removeClass('oKgreen');
        $('#tabs-7').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#t7').addClass('oKgreen');
        $('#tabs-7').addClass('oKgreen');
    } 




}

function CheckCareerPlan() {
    var IsValid = true;
    var PasoID = $('input[name$=HiddenFieldPasoID]').val();



    $('#RepeaterCareerPlanLongTerm tr').each(function () {



        if ($(this).find('textarea').length) {
            var Comentario = $(this).find('textarea').val().length;

            var ItemEvaluacionID = $(this).find('input[name*=HFID]').val();

            if (((ItemEvaluacionID == 149 || ItemEvaluacionID == 181 || ItemEvaluacionID == 340) && PasoID == 1 && Comentario == 0) || ((ItemEvaluacionID == 146 || ItemEvaluacionID == 178 || ItemEvaluacionID == 337) && PasoID == 2 && Comentario == 0))
                IsValid = false;

        }
    });

    $('#RepeaterCareerPlanLongTerm select').each(function () {

        if ($(this).val() == 0 )
            IsValid = false;

    });


    $('#RepeaterCareerPlanNearTerm tr').each(function () {



        if ($(this).find('textarea').length) {
            var Comentario = $(this).find('textarea').val().length;

            var ItemEvaluacionID = $(this).find('input[name*=HFID]').val();

            if (((ItemEvaluacionID == 148 || ItemEvaluacionID == 180 || ItemEvaluacionID == 339) && PasoID == 1 && Comentario == 0) || ((ItemEvaluacionID == 147 || ItemEvaluacionID == 179 || ItemEvaluacionID == 338) && PasoID == 2 && Comentario == 0))
                IsValid = false;

        }
    });


    $('#RepeaterCareerPlanNearTerm select').each(function () {

        if ($(this).val() == 0 )
            IsValid = false;

    });


    if (!IsValid) {
        //aqui cambiamos el class para cuando no es valido
        $('#CPH1_t8').removeClass('oKgreen');
        $('#tabs-8').removeClass('oKgreen');
    }
    else {
        // aqui el class para el valido
        $('#CPH1_t8').addClass('oKgreen');
        $('#tabs-8').addClass('oKgreen');
    } 
}

function CheckFeedBack() {
    var IsValid = true;

$('#RepeaterFeedback input[type=text]').each(function () {
    if ($(this).val().length == 0)
        IsValid = false;

});


if (!IsValid) {
    //aqui cambiamos el class para cuando no es valido
    $('#t9').removeClass('oKgreen');
    $('#tabs-9').removeClass('oKgreen');
}
else {
    // aqui el class para el valido
    $('#t9').addClass('oKgreen');
    $('#tabs-9').addClass('oKgreen');
}
}





function ValidarFecha(fecha, msg) {
    fecha = fecha.trim();
    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var matchArray = fecha.match(datePat);
    if (matchArray == null) {
        //alert(msg);
        return false;
    }
    if (matchArray.length != 6) {
       // alert(msg);
        return false;
    }


    day = matchArray[1]; // p@rse date into variables
    month = matchArray[3];
    year = matchArray[5];

    if (month < 1 || month > 12) { // check month range
       
        return false;
    }

    if (day < 1 || day > 31) {
       
        return false;
    }

    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
       
        return false;
    }

    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
           
            return false;
        }
    }
    return true; // date is valid
}

function ValidarRango(FechaDesde, FechaHasta, msg) {
    aux = FechaDesde.split("/");
    sd = new Date(aux[2], aux[1], aux[0]);
    aux = FechaHasta.split("/");
    ed = new Date(aux[2], aux[1], aux[0]);
    if (sd > ed) {
       
        return false;
    }
    return true;
}



