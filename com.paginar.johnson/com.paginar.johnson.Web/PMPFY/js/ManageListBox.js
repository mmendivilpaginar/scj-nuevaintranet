﻿//--------


var selectedList;
var availableList;
function createListObjects() {
    // availableList = document.getElementById('<%= ListBoxCursosDisponibles.ClientID %>');
    // selectedList = document.getElementById('<%= ListBoxCursosSeleccionados.ClientID %>');

    availableList = $('select[name$=ListBoxCursosDisponibles]')[0];
    selectedList = $('select[name$=ListBoxCursosSeleccionados]')[0];
}


function delAttribute() {
    createListObjects();
    var selIndex = selectedList.selectedIndex;
    if (selIndex < 0)
        return;
    /////
//    CursosSeleccionadosAux.find('option').each(function () {
//        if ($(this).val() == selectedList.options.item(selIndex).value)
//            $(this).remove();
//    });




//    CursosDisponiblesAux.append(selectedList.options.item(selIndex).outerHTML);
    ////

    availableList.appendChild(selectedList.options.item(selIndex))

  //  FiltrarListboxCursos();
    selectNone(selectedList, availableList);
    setSize(availableList, selectedList);

    SetCursoSeleccionados();
    CheckDevelopmentActions();
}
function addAttribute() {
    createListObjects();
    var addIndex = availableList.selectedIndex;
    var textSelected = availableList.options[addIndex].text;
    if (addIndex < 0)
        return;
    ////
//    CursosDisponiblesAux.find('option').each(function () {
//        if ($(this).val() == availableList.options.item(addIndex).value)
//            $(this).remove();
//    });
//    CursosSeleccionadosAux.append(availableList.options.item(addIndex).outerHTML);

    ////


    selectedList.appendChild(availableList.options.item(addIndex));
    showSelected(selectedList);
    setSize(selectedList, availableList);
    if (textSelected == 'Otro' || textSelected == 'Desarrollo en el puesto') {
        $('#mensajeAdd').html('<span style=" border: #558dc6 1px solid; padding:5px; margin:10px">' + textSelected + ': Indicar en el campo "Fill the training needs/proyects to assign"</span>');
    }
    SetCursoSeleccionados();
    CheckDevelopmentActions();
}
function delAll() {
    createListObjects();
    var len = selectedList.length - 1;
    for (i = len; i >= 0; i--) {
        availableList.appendChild(selectedList.item(i));
    }
    selectNone(selectedList, availableList);
    setSize(selectedList, availableList);

}
function addAll() {
    createListObjects();
    var len = availableList.length - 1;
    for (i = len; i >= 0; i--) {
        selectedList.appendChild(availableList.item(i));
    }
    //selectNone(selectedList,availableList);
    showSelected(selectedList);
    setSize(selectedList, availableList);

}

function selectNone(list1, list2) {
    list1.selectedIndex = -1;
    list2.selectedIndex = -1;
    addIndex = -1;
    selIndex = -1;
}
function setSize(list1, list2) {
    list1.size = getSize(list1);
    list2.size = getSize(list2);
}
function getSize(list) {
    /* Mozilla ignores whitespace, IE doesn't - count the elements in the list */
    var len = list.childNodes.length;
    var nsLen = 0;
    //nodeType returns 1 for elements
    for (i = 0; i < len; i++) {
        if (list.childNodes.item(i).nodeType == 1)
            nsLen++;
    }
    if (nsLen < 2)
        return 2;
    else
        return nsLen;
}
function showSelected(optionList) {
    //var optionList = document.getElementById("CursosSeleccionados").options;
    var data = '';
    var len = optionList.length;
    for (i = 0; i < len; i++) {
        if (i > 0)
            data += ',';
        data += optionList.item(i).value;
    }
    //alert(data);
}

function selectAllList() {
    var aSelect = document.getElementById("CursosSeleccionados");
    var aSelectLen = aSelect.length;
    for (i = 0; i < aSelectLen; i++) {
        aSelect.options[i].selected = true;
    }
}


//------