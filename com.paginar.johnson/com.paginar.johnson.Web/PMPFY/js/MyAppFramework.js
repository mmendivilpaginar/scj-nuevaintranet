﻿Type.registerNamespace('AjaxControlToolkit');
function FixModalPopupReloadProblem() {
    if (Sys && Sys.Browser && Sys.Browser.agent == Sys.Browser.InternetExplorer && Sys.Browser.version <= 6) {
        CommonToolkitScripts.getLocation = function(element) {

            /// <summary>Gets the coordinates of a DOM element.</summary>

            /// <param name="element" domElement="true"/>

            /// <returns type="Sys.UI.Point">

            /// A Point object with two fields, x and y, which contain the pixel coordinates of the element.

            /// </returns>



            // workaround for an issue in getLocation where it will compute the location of the document element.

            // this will return an offset if scrolled.

            //
            if (element === document.documentElement) {
                return new Sys.UI.Point(0, 0);

            }



            // Workaround for IE6 bug in getLocation (also required patching getBounds - remove that fix when this is removed)
            if (Sys.Browser.agent == Sys.Browser.InternetExplorer && Sys.Browser.version < 7) {
                if (element.window === element || element.nodeType === 9 || !element.getClientRects || !element.getBoundingClientRect) return new Sys.UI.Point(0, 0);



                // Get the first bounding rectangle in screen coordinates
                var screenRects = element.getClientRects();

                if (!screenRects || !screenRects.length) {
                    return new Sys.UI.Point(0, 0);

                }
                var first = screenRects[0];



                // Delta between client coords and screen coords
                var dLeft = 0;

                var dTop = 0;
                var inFrame = false; try {

                    inFrame = element.ownerDocument.parentWindow.frameElement;

                } catch (ex) {

                    // If accessing the frameElement fails, a frame is probably in a different

                    // domain than its parent - and we still want to do the calculation below
                    inFrame = true;

                }

                // If we're in a frame, get client coordinates too so we can compute the delta
                if (inFrame) {



                    // Get the bounding rectangle in client coords
                    var clientRect = element.getBoundingClientRect();

                    if (!clientRect) {
                        return new Sys.UI.Point(0, 0);

                    }



                    // Find the minima in screen coords
                    var minLeft = first.left;

                    var minTop = first.top;
                    for (var i = 1; i < screenRects.length; i++) {

                        var r = screenRects[i];
                        if (r.left < minLeft) {

                            minLeft = r.left;

                        }
                        if (r.top < minTop) {

                            minTop = r.top;

                        }

                    }



                    // Compute the delta between screen and client coords

                    dLeft = minLeft - clientRect.left;

                    dTop = minTop - clientRect.top;

                }



                // Subtract 2px, the border of the viewport (It can be changed in IE6 by applying a border style to the HTML element,

                // but this is not supported by ASP.NET AJAX, and it cannot be changed in IE7.), and also subtract the delta between

                // screen coords and client coords

                try {
                    var ownerDocument = element.document.documentElement; return new Sys.UI.Point(first.left - 2 - dLeft + ownerDocument.scrollLeft, first.top - 2 - dTop + ownerDocument.scrollTop);

                }
                catch (ex) {
                    return new Sys.UI.Point(0, 0);

                }

            }


            return Sys.UI.DomElement.getLocation(element);



        }

    }

}


Sys.Application.add_load(FixModalPopupReloadProblem);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();