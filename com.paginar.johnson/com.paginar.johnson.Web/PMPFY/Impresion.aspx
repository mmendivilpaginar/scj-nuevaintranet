﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="Impresion.aspx.cs" Inherits="com.paginar.johnson.Web.PMPFY.Impresion" EnableEventValidation="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title></title>
    <link href="css/print/career.css" rel="stylesheet" type="text/css" />
    <link href="css/print/contributors.css" rel="stylesheet" type="text/css" />
    <link href="css/print/estilos.css" rel="stylesheet" type="text/css" />
    <link href="css/print/formPMPv1.css" rel="stylesheet" type="text/css" />

    <%-- <link href="css/print/managers.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        function Imprimir() {
            var bot = document.getElementById('print');
            bot.style.display = 'none';
            window.print();
            bot.style.display = 'block';
        }
    </script>
   
</head>
<body>


<style type="text/css" media="print">

@page{
   /*margin:.3in .3in .3in .3in;*/
   margin-left:.3in;
   margin-right:.3in;
   /*mso-header-margin:.3in;
   mso-footer-margin:.3in;*/
}

</style>
   <style type="text/css">
 @page Section2 {margin:.3in .3in .3in .3in;mso-header-margin:.3in;mso-footer-margin:.3in;}
div.Section2 {page:Section2;}      


    </style>

   <div id="export" runat="server" class="Section2">
    <form id="form1" runat="server">
 
    <%-- <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="false" runat="server">
     </asp:ScriptManager>--%>
 
     <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         
          <asp:HiddenField ID="HiddenFieldPasoID" runat="server" />
         <div style=" text-align:center" id="print">
             <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <ContentTemplate>--%>                 <%--<input type="button" value="imprimir" onclick="Imprimir()" class="destacados" id="Bprint"/>--%>
                 <asp:Button ID="btnImprimir" runat="server"  OnClientClick="Imprimir()"
                     Text="Imprimir" onclick="Imprimir_Click"  />
                 <asp:Button ID="Exportar" runat="server" onclick="Button1_Click" Text="Exportar" />
                 
             <%--</ContentTemplate>
                 <Triggers>
                     <asp:AsyncPostBackTrigger ControlID="Exportar" EventName="Click" />
                     <asp:AsyncPostBackTrigger ControlID="Imprimir" EventName="Click" />
                 </Triggers>
             </asp:UpdatePanel>--%>

         </div>
         
         <!-- ============== [CABECERA] ============== -->
         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" BorderStyle="Dotted">
            <ItemTemplate>
                      <!--Logo y Titulo-->
                      <table class="seccion1" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="150" rowspan="2" style="border-bottom:4px solid black;">
                              <div id="logo" runat="server">
                                 <img id="logo-image" alt="Logo Johnson " src="/PMPFY/img/logoprint3.jpg" />
                              </div>
                          </td>
                          <td width="370" rowspan="2" style="border-bottom:4px solid black;">
                            <p class="titulosCabeceras" style="margin-left:15px;">
                                Performance Management Process <br />
                                For the Performance Year  <u>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PeriodoDesde","{0:yyyy}") %>'></asp:Label> / <asp:Label ID="Label9" runat="server" Text='<%# Bind("PeriodoHasta","{0:yyyy}") %>'></asp:Label></u>
                                <br />
                                <%--Managers and Supervisors --%>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("TipoFormulario") %>'></asp:Label>
                            </p>
                          </td>
                          <td height="35" valign="top" align="right" style="font-size:10px">
                            <asp:Label ID="lblCodeForm" runat="server" Text=""></asp:Label><!--  SFO000024 -->
                          </td>
                        </tr>
                        <tr>
                          <td style="border-bottom:4px solid black;" height="22" valign="bottom" align="right"> 
                            <span class="wrapperDate">
                                <span class="label">Date:</span> 
                                <span class="value"><asp:Label ID="Label8" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>'></asp:Label></span>
                            </span>
                          </td>
                        </tr>
                      </table>
                      
                      <table class="seccion2" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="bottom">
                          <td width="48%" height="20" colspan="3" class="bordeinferior1_x"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="93%" height="50" valign="bottom" ><asp:Label ID="Label5" runat="server" Text='<%# Bind("EvaluadoNombre") %>'></asp:Label></td>
                              <td width="7%" valign="bottom"><asp:Label ID="Label6" runat="server" Text='<%# Bind("EvaluadoLegajo") %>'></asp:Label></td>
                            </tr>
                          </table></td>
                          <td width="4%">&nbsp;</td>
                          <td width="48%" colspan="3" class="bordeinferior1_x"> 
                              <asp:Label ID="Label4" runat="server" Text='<%# Bind("EvaluadorNombre") %>'></asp:Label></td>
                        </tr>
                        <tr>
                          <td width="171" class="letras_grises">Appraisee</td>
                          <td colspan="2" class="letras_grises" width="80"><div align="right">Employee Number</div></td>
                          <td>&nbsp;</td>
                          <td colspan="3" class="letras_grises">Appraising Manager</td>
                        </tr>

                        <!--SEPARADOR-->
                        <tr><td colspan="5" height="10"></td></tr>

                        <tr valign="bottom">
                          <td height="20" class="bordeinferior1_x">&nbsp;</td>
                          <td colspan="2" align="right" class="bordeinferior1_x">&nbsp;&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="letras_grises">Appraisee Signature</td>
                          <td colspan="2" class="letras_grises"><div align="right">Date</div></td>
                          <td>&nbsp;</td>
                          <td colspan="2" class="letras_grises">Appraising Manager&rsquo;s Signature</td>
                          <td width="61" class="letras_grises"><div align="center">Date</div></td>
                        </tr>

                        <!--SEPARADOR-->
                        <tr><td colspan="5" height="10"></td></tr>

                        <tr valign="bottom">
                          <td height="20" colspan="3" class="bordeinferior1_x"><asp:Label ID="Label7" runat="server" Text='<%# Bind("EvaluadoArea") %>'></asp:Label></td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x"><div align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>        </td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="3" class="letras_grises">Division</td>
                          <td>&nbsp;</td>
                          <td width="214" class="letras_grises">Review - Next Level Management</td>
                          <td width="71" class="letras_grises"><div align="center">Initials</div></td>
                          <td><div align="center" class="letras_grises">Date</div></td>
                        </tr>

                        <!--SEPARADOR-->
                        <tr><td colspan="5" height="10"></td></tr>

                        <tr valign="bottom">
                          <td  height="20" width="220" class="bordeinferior1_x"><%--<p align="left" class="x"><b > --%>
                              <%--<asp:Label ID="Label3" runat="server" Text='<%# Bind("Periodo") %>'></asp:Label>--%>
                              <asp:Label ID="Label23" runat="server" Text='<%# Bind("PeriodoDesde","{0:dd/MM/yyyy}") %>'></asp:Label> To <asp:Label ID="Label25" runat="server" Text='<%# Bind("PeriodoHasta","{0:dd/MM/yyyy}") %>'></asp:Label>
                              <%--</b></p>--%></td>
                        <td width="60" class="bordeinferior1_x"><p class="x"><b style='mso-bidi-font-weight:normal'><span style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:
                      Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"></span></b></p></td>
                          <td width="10" class="bordeinferior1_x">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x"><div align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div></td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="3" class="letras_grises"  align="left">For the Period Covering</td>
                          <td>&nbsp;</td>
                          <td class="letras_grises">Human Resources Manager</td>
                          <td class="letras_grises"><div align="center">Initials</div></td>
                          <td><div align="center" class="letras_grises">Date</div></td>
                        </tr>
                      </table>
            </ItemTemplate>
         </asp:FormView>

         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         <!-- ============== [/CABECERA] ============== -->

         

         <!-- ============== [PMP] ============== -->
         <!--Performance Plan & Review-->        
        <asp:Panel ID="PPReview" runat="server" CssClass="bloque">
        <%--<H1 class="SaltoDePagina" runat="server" id="SaltoPaginaEncabezado"> </H1>--%>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                <tr>
                    <td colspan="5" class="pnet-subtitle" style="border-bottom:4px solid black;">
                        Performance Plan &amp; Review
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="10"></td>
                </tr>
                <tr>
                    <td colspan="5">
                        <span class="" style="font-size:12pt;font-family:Arial;">
                        If planning the YEAR AHEAD, list the
                        desired goals for this individual, up to five(5), and indicate measures
                        (how the degree of achievement will be assessed). If reviewing the YEAR
                        COMPLETED, list the results and the degree of completion (1-5) as follows:
                        <o:p></o:p>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="10"></td>
                </tr>
                <tr>
                    <td height="19">
                        <span class="subtitulos_12" style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
                            1 = Did not meet expectation
                            <o:p></o:p>
                        </span>
                    </td>
                    <td>
                        <span class="subtitulos_12" style="font-size:12.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
                        2 = Met some, but not all expectations
                        <o:p></o:p>
                        </span>
                    </td>
                    <td>
                        <span class="subtitulos_12" style="font-size:12.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
                            3 = Met expectations
                            <o:p></o:p>
                        </span>
                    </td>
                    <td><span class="subtitulos_12" style="font-size:12.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
                    4 = Met expectations
          <o:p></o:p>
      </span></td>
                    <td><span class="subtitulos_12" style="font-size:12.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">
                    5 = Exceeded most expectations
          <o:p></o:p>
      </span></td>
                </tr>
                <tr>
                    <td colspan="5" height="20"></td>
                </tr>            
            </table>
            <table class="pnet-table" width="100%" cellspacing="0" cellspacing="0">
                <tr>
                    <td width="30%" align="left" style="border-top:4px solid black;"><span style="font-weight:bold;font-size:12pt;mso-bidi-font-size:12.0pt;">Goals</span></td>
                    <td width="30%" align="left" style="border-top:4px solid black;"><span style="font-weight:bold;font-size:12pt;mso-bidi-font-size:12.0pt;">Measures</span></td>
                    <td width="30%" align="left" style="border-top:4px solid black;"><span style="font-weight:bold;font-size:12pt;mso-bidi-font-size:12.0pt;">Results</span></td>
                    <td width="10%" align="left" style="border-top:4px solid black;"><span style="font-weight:bold;font-size:12pt;mso-bidi-font-size:12.0pt;">Rating</span></td>
                </tr>
                    
                <asp:ObjectDataSource ID="ObjectDataSourceCalificaciones" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCalificaciones" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                 <SelectParameters>
                     <asp:Parameter DefaultValue="I" Name="Idioma" Type="String" />
                     <asp:Parameter DefaultValue="P" Name="modulo" Type="String" />
                 </SelectParameters>
                </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives1" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives1" OnPreRender="RepeaterIndividualObjetives_PreRender">
                    <HeaderTemplate><tr></HeaderTemplate>
                    <ItemTemplate>
                            <td valign="top">
                                <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                                <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </td>

                   
                    </ItemTemplate>
                    <FooterTemplate></tr></FooterTemplate>
                </asp:Repeater>

                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="24" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives2" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives2" OnPreRender="RepeaterIndividualObjetives_PreRender">
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                        <td valign="top">
                                <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                                <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
        </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="25" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives3" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives3" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives3_ItemDataBound" >
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="26" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives4" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives4" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives4_ItemDataBound"> 
          
                    <HeaderTemplate>
                <tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives4" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="27" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives5" runat="server" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          DataSourceID="ObjectDataSourceIndividualObjetives5" 
          onitemdatabound="RepeaterIndividualObjetives5_ItemDataBound" >
          
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives5" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="28" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives6" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives6" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives6_ItemDataBound" >
          
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives6" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="29" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives7" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives7" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives7_ItemDataBound" >
          
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives7" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="30" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives8" runat="server"  
          DataSourceID="ObjectDataSourceIndividualObjetives8" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives8_ItemDataBound" >
          
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives8" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="31" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives9" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives9" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives9_ItemDataBound">
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives9" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="32" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <asp:Repeater ID="RepeaterIndividualObjetives10" runat="server" Visible="false"
          DataSourceID="ObjectDataSourceIndividualObjetives10"  
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives10_ItemDataBound" >
          
                <HeaderTemplate><tr ></HeaderTemplate>
                <ItemTemplate>
                            <td valign="top">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>

            </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives10" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="33" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                <%--    <tr>
                      <td height="22" align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
                      <td align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
                      <td align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
                      <td align="center" valign="middle" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
                    </tr>--%>
            </table>
        </asp:Panel>
        <br />

        <!-- Performance Review - Leadership Imperatives -->
        <asp:Panel ID="PLeaderImperatives" runat="server" CssClass="bloque">
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pnet-subtitle">
                        Performance Review - Leadership Imperatives
                    </td>
                </tr>
            </table>

            <table width="100%" border="0" cellpadding="0"  cellspacing="0" class="pnet-table">
                <tr>
                    <!-- <img src="Img/FondoNegro2.jpg" alt="titulo" width="733" height="24" />-->
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">1 &ndash; Did not meet expectation</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">2 &ndash; Met some, but not all expectations</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">3 &ndash; Met expectations</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">4 &ndash; Exceeded most expectations</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">5 &ndash; Significantly exceeded expectations</td>
                </tr>
                <tr valign="top">
                    <td width="20%">
                        This person is currently <b style='mso-bidi-font-weight:normal'>not meeting</b> the performance standards of the company for this position.
                    </td>
                    <td width="20%">
                        This person meets <b style='mso-bidi-font-weight:normal'>some but not all </b>of the performance standards of the company for this position.<o:p></o:p>
                    </td>
                    <td width="20%">
                        This person <b style='mso-bidi-font-weight:normal'>meets and occasionally exceeds</b> the high standards of the company for this position.<o:p></o:p>
                    </td>
                    <td width="20%">
                        This person <b style='mso-bidi-font-weight:normal'>meets and frequently exceeds</b> the high standards of the company for this position.<o:p></o:p>
                    </td>
                    <td width="19%">
                        Through leadership and positive example, this person is a positive influence to the broader organization.<o:p></o:p>
                    </td>
                </tr>
            </table>

            <asp:Repeater ID="RepeaterSuccessFactors" runat="server" 
                DataSourceID="ObjectDataSourceSuccessFactors">
                <HeaderTemplate>
                  <table width="100%" cellspacing="0" cellpadding="0" class="pnet-table">
                    <tr>
                      <td width="97%">&nbsp;</td>
                      <td width="2%"><div align="center"><span class="texto10negrita">Rating</span></div></td>
                    </tr>                  
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td valign="top">
                                <asp:Label ID="titulo" runat="server"  Text='<%# formatearTexto(Eval("Titulo").ToString(),Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />"))%>'></asp:Label>
                                <asp:Label ID="lblOptional" runat="server" Text='<%# "<br />" + devuelveFortalezaNecesidad(Eval("Fundamentacion").ToString()) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OPTIONAL")>=0 %>'></asp:Label>
                          </td>
                          <td>
                            <div align="center">
                                <span class="texto10negrita">&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text='<%# Eval("Ponderacion").ToString()!="0" ? Eval("Ponderacion"): ""%>'></asp:Label>&nbsp;</span>
                            </div>
                          </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <asp:ObjectDataSource ID="ObjectDataSourceSuccessFactors" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="7" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                        Name="PeriodoID" PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
        </asp:Panel>
        <br />

        <!--Current Strengths/Development Needs-->
        <asp:Panel ID="PSDNeed" runat="server" CssClass="bloque" Visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr>
                    <td>
                        <span class="pnet-subtitle subtitulos_14">Current Strengths/Development Needs</span><br/>
                        <span class="texto12italica">Use Skills from the <asp:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl="http%3A%2F%2Fsharepoint.scj.com%2Fsites%2Fglobal%2FFormsLibrary%2FMemo%2FCompetency%20List%20for%20PMP.pdf" runat="server">SCJ Competency List</asp:HyperLink>.</span>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" class="pnet-table">
                <tr>
                    <td width="50%" style="border-top:4px solid black;"><div align="center"><span class="subtitulos_12">Strengths</span></div></td>
                    <td width="50%" style="border-top:4px solid black;"><div align="center"><span class="subtitulos_12">Development Needs</span></div></td>
                </tr>
                <tr>
                  <td width="50%">
                        <asp:Repeater ID="RepeaterSTRENGTHS" runat="server" 
                            DataSourceID="ObjectDataSourceSTRENGTHS">
                            <HeaderTemplate>
                                <table id="RepeaterSTRENGTHS0" width="100%" cellspacing="0" cellpadding="0">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label>
                            
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:ObjectDataSource ID="ObjectDataSourceSTRENGTHS" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                            TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="15" Name="SeccionID" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                                    Name="PeriodoID" PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                                    Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                  </td>
                  <td width="50%">
                        <asp:Repeater ID="RepeaterDevelopmentNeeds" runat="server" 
                            DataSourceID="ObjectDataSourceDevelopmentNeeds" >
                            <HeaderTemplate>
                                <table id="RepeaterDevelopmentNeeds0" width="100%" cellspacing="0" cellpadding="0">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:ObjectDataSource ID="ObjectDataSourceDevelopmentNeeds" runat="server" 
                            OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                            TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="16" Name="SeccionID" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                                    Name="PeriodoID" PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                                    Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                  </td>
                </tr>
              </table>
            <asp:ObjectDataSource ID="ObjectDataSourceFortalezas" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetFortalezasByTipoFormularioID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:Panel>
        <br />

        <!--Development Actions for the next fiscal year-->
        <asp:Panel ID="PDAFTNextFiscalYear" runat="server" CssClass="bloque" Visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="1">
                <tr>
                    <td class="pnet-subtitle">Development Actions for the next fiscal year</td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" class="pnet-table">
                <asp:Repeater ID="RepeaterDEVELOPMENTACTIONS" runat="server" 
                    DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS">
                    <HeaderTemplate>
                            <tr>
                            <td style="border-top:4px solid black;">                      
                    </HeaderTemplate>
                    <ItemTemplate>
                              <asp:Label ID="Label4" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label>
                                <%--<asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' 
                                TextMode="MultiLine" Rows="5"  SkinID="form-textarea"></asp:TextBox>--%>
                    </ItemTemplate>
                    <FooterTemplate>
                        </td>
                        </tr>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceDEVELOPMENTACTIONS" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="9" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </table>
            <br/> 
            <asp:Repeater runat="server" ID="RepeaterCursosSeleccionados2010_2011" 
                DataSourceID="ObjectDataSourceDevelopmentNeeds" 
                onitemdatabound="RepeaterCursosSeleccionados_ItemDataBound">
            <HeaderTemplate>
            <table width="100%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
            <thead>
            <tr> <td width="40%" class="borde_superior3">Development Needs</td><td width="60%" class="borde_superior3">Related Courses</td></tr>
            </thead>
            </HeaderTemplate>
            <ItemTemplate>
            <tr>
            <td><asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label>
                <asp:HiddenField  runat="server" ID="HiddenFieldDebilidadID" value='<%# Eval("Ponderacion") %>'/></td>
            <td>
             <asp:Literal runat="server" ID="LiteralCursosRelacionados"></asp:Literal>
            
            </td>

            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>  
            </FooterTemplate>
            
            </asp:Repeater> 
            <asp:Repeater runat="server" ID="RepeaterCursos"  
                DataSourceID="ObjectDataSourceCursos">
            <HeaderTemplate>
            <table width="100%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
            <thead>
            <tr> <td width="60%" class="borde_superior3">Related Courses</td></tr>
            </thead>
            </HeaderTemplate>
            <ItemTemplate>
            <tr>
            <td>
             <asp:Label runat="server" ID="CursoDescripcion"  Text='<%# Eval("CursoDescripcion") %>'>'></asp:Label>
            
            </td>

            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>  
            </FooterTemplate>
            
            </asp:Repeater> 
            <asp:ObjectDataSource ID="ObjectDataSourceCursos" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCursosByTramiteID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:Panel>
        <br />

        <!--Performance Review - Summary-->
        <asp:Panel ID="PPerformanceReviewSummary" runat="server" CssClass="bloque">
            <div style="page-break-before:always;">&nbsp;</div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="pnet-subtitle subtitulos_14">Performance Review - Summary</td>
                </tr>
            </table>
            <asp:Repeater ID="RepeaterPerformanceReviewSummary" runat="server" 
                DataSourceID="ObjectDataSourcePerformanceReviewSummary" >
                <ItemTemplate>
                    <asp:Label ID="lblTitulo" runat="server" Text='<%# summaryComments(Eval("Titulo"), Eval("Fundamentacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>
                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                    <asp:Label ID="lblChosseOptionRating" runat="server" Text='<%# OverallRating(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>
                </ItemTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourcePerformanceReviewSummary" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="10" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </asp:Panel>   
        <br />

        <div style="page-break-before:always;">&nbsp;</div>
        <!-- ============== [/PMP] ============== -->            

        <!--
        <div runat="server" id="Divbr1">
            <br style="page-break-before:always">
        </div>
        -->

        <!-- ============== [MSD] ============== -->
        <asp:Panel ID="PExecutiveManagementDevelopmentPlan" runat="server" CssClass="bloque">

            <!-- Executive Management Succession & Development Plan -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="pnet-subtitle">
                    <asp:Repeater ID="RepeaterPeriodo" runat="server" DataSourceID="ObjectDataSourceCabecera">
                    <HeaderTemplate></HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label runat="server" ID="LabelPeriodoDesde" Text='<%# incrementarUnAnio(Eval("PeriodoDesde" , "{0:yyyy}")) + "/" + incrementarUnAnio(Eval("PeriodoHasta" , "{0:yy}")) %>'></asp:Label>
                        <%-- /           <asp:Label runat="server" ID="Label26" Text='<%# Bind("PeriodoHasta" , "{0:yy}") %>'></asp:Label>--%>
                    </ItemTemplate>
                    <FooterTemplate></FooterTemplate>
                    </asp:Repeater>
                    Executive Management Succession &amp; Development Plan <i>(For International Use Only)</i>
                </td>
            </tr>
            </table>
            <br />
            <div class="wrapper">

            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr>
                    <td width="50%">
                        <span class="Calibri9">LAST NAME:<br/>
                        <span class="CalibriNegrita12"> <asp:Label ID="lblApellidoEvaluado" runat="server" Text=""></asp:Label> &nbsp;</span></span>
                    </td>
                    <td width="25%">
                        <span class="Calibri9">FIRST NAME:<br/>
                        <span class="CalibriNegrita12"><asp:Label ID="lblNombreEvaluado" runat="server" Text=""></asp:Label></span></span>
                    </td>
                    <td width="25%">
                        <span class="Calibri9">MI:<br/>
                            <span class="X"> 
                                <span class="CalibriNegrita12">
                                    <asp:Label ID="lblPadding" runat="server" Text=" &amp;nbsp; &amp;nbsp;"></asp:Label>
                                </span> 
                            </span>
                        </span>
                    </td>
                </tr>
            </table>


            <!--SECTION A: CURRENT JOB-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                    <td colspan="3" class="BORDEiz-ar-deGris">
                        <span class="Calibri95NegritaBlue">SECTION A: CURRENT JOB</span>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <span class="Calibri9">TITLE:</span><br/>
                        <span class="CalibriNegrita10"> <asp:Label ID="lblTitleA" runat="server" Text="Label"></asp:Label> </span>
                    </td>
                    <td width="25%">
                        <span class="Calibri9">LEVEL:</span><br/>
                        <span class="CalibriNegrita10"><asp:Label ID="lblEscalaSalarial" runat="server" Text="Label"></asp:Label> </span>
                    </td>
                    <td width="25%"><span class="Calibri9">TIME IN JOB:</span><br/>
                        <span class="CalibriNegrita10">  <asp:Label ID="LabelTimeInJob" runat="server" Text=""></asp:Label></span>
                    </td>
                </tr>
            </table>

            <!--SECTION B: LAST PERFORMANCE RATINGS-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                    <td class="BORDEiz-arGris" width="50%">
                        <span class="Calibri95NegritaBlue">SECTION B: LAST PERFORMANCE RATINGS</span>
                    </td>
                    <td width="50%" colspan="3" class="BORDEiz-ar-deGris">
                        <span class="Calibri95NegritaBlue">EDUCATION</span>
                    </td>
                </tr>
                <tr> 
                    <td>
                        <span class="Calibri8Italica">Enter last 3 years of performance ratings.</span>
                        <asp:Repeater ID="RepeaterSeccionBPerformanceRatings" runat="server" DataSourceID="ObjectDataSourceLastRating">
                            <HeaderTemplate>
                            <table border="0">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><span style="mso-no-proof:yes" class="Calibri85">
                                    <b  style='mso-bidi-font-weight:normal'>
                                    <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label></b>&nbsp;</span>
                                    <span class="Calibri9"> <asp:Label ID="LabelCalificacion" runat="server" Text='<%# Eval("Calificacion") %>'></asp:Label></span> </td>                     
                                </tr>                                              
                            </ItemTemplate>
                            <FooterTemplate>
                            </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td><span class="Calibri9">DEGREE:</span></td>
                                <td><span class="Calibri9" >YEAR:</span></td>
                                <td><span class="Calibri9">SCHOOL: </span></td>
                            </tr>
                            <asp:Repeater ID="DataListEducation" runat="server"  >
                                <HeaderTemplate></HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td  width="17%"><span class="Calibri10"><asp:Label ID="Label7" runat="server" Text='<%# DameResumeDegree(Eval("Degree")) %>'></asp:Label>&nbsp;</span></td>
                                            <td  width="17%"><span class="Calibri10"  ><asp:Label ID="Label8" runat="server" Text='<%# Bind("Year") %>' ></asp:Label>&nbsp;</span></td>
                                            <td ><span class="Calibri10" style="word-wrap: break-word;"><asp:Label ID="Label9" runat="server" Text='<%# DameSchool(Eval("School")) %>'></asp:Label></span></td>        
                                        </tr>
                                    </ItemTemplate>
                                <FooterTemplate></FooterTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
            </table>

            <!--SECTION C: MOBILITY &TRAVEL-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                    <td width="50%" colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION C: MOBILITY &amp;TRAVEL (Completed by Employee)</span></td>
                </tr>
                <tr>
                    <asp:Repeater ID="RepeaterMobilityAndTravel" runat="server">
                        <ItemTemplate>
                            <asp:Literal ID="lblTitulo" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "185" || Eval("ItemEvaluacionID").ToString()== "390" %>' Text='<%# "<td colspan=\"2\"><span class=\"Calibri8\">" + Eval("Titulo").ToString() +":" %>'>
                            </asp:Literal> 
                            <asp:Label ID="Label10" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "185" || Eval("ItemEvaluacionID").ToString()== "390" %>' Text='<%# setMobilidad(Eval("Ponderacion")) %>'></asp:Label>
                            <asp:Label ID="Label18" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "186" || Eval("ItemEvaluacionID").ToString()== "391" %>' Text='<%# "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + Eval("Titulo").ToString() %>'>
                            Willingness to Travel: </asp:Label>
                            <asp:Literal ID="Label16" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "186" || Eval("ItemEvaluacionID").ToString()== "391" %>' Text='<%# (setMobilidadYN(Eval("Ponderacion")) + "</span></td>") %>'></asp:Literal>
                    
                            <td width="50%" class="BORDEiz" id="tdModbiOne" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392" %>'>
                                <span class="Calibri8Negrita">Limitations to mobility/travel. The character limit should not exceed 45.</span>
                                <br>
                                <span class="Calibri9"><asp:Label ID="Label11" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392" %>'></asp:Label></span>
                            </td>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </table>
  
            
            <!--SECTION D: LANGUAGE PROFICIENCY-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                    <td class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION D: LANGUAGE PROFICIENCY (Completed by Employee)</span></td>
                </tr>
                <tr style='mso-yfti-irow:11;page-break-inside:avoid;height:13.0pt'>
                    <td width="100%" class="BORDEiz-ar-deGris">
                        <asp:Repeater ID="RepeaterLanguage" runat="server"  >
                            <HeaderTemplate>
                                <table width="100%">
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr>
                                        <td width="2%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>' ><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:10.0pt'><b>
                                            <asp:Label runat="server" ID="LabelNumber" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>'></asp:Label>   
                                            <asp:HiddenField ID="HiddenFieldNumber" Value='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' runat="server" />                    

                                        </b></span></td>
                                        <td width="35%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'>
                                        <span style='font-size:9.0pt;letter-spacing:.1pt' class="Calibri9"><b style='mso-bidi-font-weight:normal'>
                                            <asp:Label ID="Label22" runat="server" Text='<%# Eval("Idioma") %>'></asp:Label></b>
                                            </span></td>
                                        <td width="5%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9"><asp:label ID="LabelNative22" runat="server" Visible='<%# Eval("IsNative").ToString()=="True" %>' >Native:</asp:label></span></td>


                                        <td  width="10%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9">
                                        <%--<asp:CheckBox ID="CheckBoxNative" runat="server"  Text="Native:" Checked='<%# Eval("IsNative").ToString()=="1" %>' Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>'  TextAlign="Left"/>--%>
                                        <asp:Label runat="server" ID="LabelNative" Text='<%# Eval("IsNative").ToString()=="True"? "Yes": "" %>'></asp:Label>
                                        </span></td>
                            
                                        <td width="10%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9">
                                        </span></td>
                                        <td width="7%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'>
                                        <span class="Calibri9">Proficiency: </span></td>
                                        <td style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9"> <asp:Label ID="LabelProfiency" runat="server" Text='<%# DameProficiency(Eval("Proficiency")) %>'></asp:Label> </span></td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate></table></FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr> 
  
            </table>
  
  
            <!--SECTION E: CAREER INTERESTS-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                  <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION E: CAREER INTERESTS (Completed by Employee)</span></td>
                </tr>
                <tr>
                  <td width="50%" colspan="2">
                    <span class="Calibri8">
                        List jobs in order of greatest interest to you. 
                        <span class="Calibri8"><i> <b>(IF NEEDED) (45 character limit)</b></i></span>
                        <!--Link for list of current job titles <a href="documents\JobCodes_Full_List.xls">click here</a>-->
                    </span>
                  </td>
                  <td width="50%" class="BORDEiz">
                    <span class="Calibri8NegritaMayuscula">ADDITIONAL COMMENTS </span>
                    <span class="Calibri8"><i> <b>(IF NEEDED) (45 character limit)</b></i></span>
                  </td>
                </tr>
                <asp:Repeater ID="RepeaterCareerInterests" runat="server">
                    <ItemTemplate>
                        <tr style="line-height: 20px;">
                          <td style="padding-bottom: 3.5pt">
                                <span class="Calibri9Negrita" >
                                    <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                                </span>
                                <span class="Calibri8">Code:</span> 
                                <span class="Calibri9Negrita">
                                    <asp:Label ID="Label13" runat="server" Text='<%# setCareerCode(Eval("Ponderacion")) %>'></asp:Label>
                               </span>
                          </td>
                          <td style="padding-bottom: 3.5pt">
                            <span class="Calibri8">Description:</span>
                            <span class="Calibri8">
                                <asp:Label ID="Label5" runat="server" Text='<%# setCareer(Eval("Ponderacion")) %>'></asp:Label> 
                            </span> 
                          </td>
                          <td valign="top" class="BORDEiz" style="padding-bottom: 3.5pt">
                            <span class="Calibri8"><asp:Label ID="Label14" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label></span>
                          </td>
                        </tr>
                    </ItemTemplate>
                  </asp:Repeater>
            </table>
  
            <!--SECTION F: NEXT ASSIGNMENT-->
            <asp:Panel runat="server" ID="PanelSectionF">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                    <tr bgcolor="#DFDFDF">
                      <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaRed">SECTION F: NEXT ASSIGNMENT (Completed by Manager)</span></td>
                    </tr>
                    <tr>
                      <td width="50%" colspan="2">
                        <span class="Arial8">Prioritize 1-3 job(s) as logical next assignment(s). <span class="Calibri8"><i> <b>(IF NEEDED) (45 character limit)</b></i></span>
                            <!-- For a list of SCJ  job titles <a href="http://sharepoint.scj.com/sites/global/FormsLibrary/Memo/JobCodes_Full%20List.xls">click here</a> -->
                        </span>
                      </td>
                      <td width="50%" class="BORDEiz">
                        <span class="Calibri8NegritaMayuscula"> &nbsp;ADDITIONAL  COMMENTS </span>
                        <span class="Arial8"><i> <b>(IF NEEDED) (45 character limit)</b></i></span>
                      </td>
                    </tr>

                   <asp:Repeater ID="RepeaterNextAssigment" runat="server">
                    <ItemTemplate>
                    <tr style=" line-height: 20px;">
                      <td style="padding-bottom: 3.5pt">
                        <span class="Calibri9Negrita">
                            <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />     
                        </span> 
                        <span class="Calibri8">Code:</span> 
                        <span class="Calibri9Negrita"><asp:Label ID="Label13" runat="server" Text='<%# setCareerCode(Eval("Ponderacion")) %>'></asp:Label> &nbsp;</span>
                      </td>
                      <td style="padding-bottom: 3.5t">
                        <span class="Calibri8">Description: </span>
                        <span class="Calibri8"><asp:Label ID="Label5" runat="server" Text='<%# setCareer(Eval("Ponderacion")) %>'></asp:Label> &nbsp;</span>
                      </td>
                      <td  style="padding-bottom: 3.5pt" valign="top" class="BORDEiz">
                        <span class="Calibri8"><asp:Label ID="Label14" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label> &nbsp;</span>
                      </td>
                    </tr>
                    </ItemTemplate>
                </asp:Repeater>
              </table>
            </asp:Panel>

            
            <!--SECTION G: DEVELOPMENT SUMMARY -->
            <asp:Panel runat="server" ID="PanelSectionG">
  
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                  <td colspan="2" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaRed">SECTION G: DEVELOPMENT SUMMARY (Completed by Manager)</span></td>
                </tr>
                <tr>
                  <td colspan="2"><span class="Calibri9"><i>1-3 Strengths/Development Needs from PMP form per the SCJ Competency List</i> <!--(Note: &quot;Other&quot; may be selected once). <a href="http://sharepoint.scj.com/sites/global/FormsLibrary/Memo/Skill%20List%20Definitions-master.xls" target="_blank">SCJ Skills List</a>--> </span><span class="Arial10Blue"></span></td>
                </tr>
                <tr style=" line-height: 20px;">
                  <td class="BORDEde" width="50%" ><span class="Calibri9Negrita">Strengths (Skill)</span></td>
                  <td width="50%"><span class="Calibri9Negrita">Comments (Behavior) (<i>25 character limit</i>):</span></td>
                </tr>
                <!---->
                      <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ObjectDataSourceSTRENGTHS">

                            <ItemTemplate>
                                <tr style="line-height: 20px;">
                                    <td class="BORDEde" style="padding-bottom: 3.5pt"><span class="Calibri9Negrita">
                                       <asp:Label ID="Label6" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />                         
                                    </span><span class="Calibri9">
                                        <asp:Label ID="Label15" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>'></asp:Label> &nbsp; </span></td>
                                    <td valign="top" style="padding-bottom: 3.5pt"><span class="Calibri9">
                                    <asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label> 
                                     &nbsp;</span></td>
                                </tr>
                       
                            </ItemTemplate>

                        </asp:Repeater>
                <!---->  
                <tr style=" line-height: 20px;">
                  <td class="BORDEde" ><span class="Calibri9Negrita">Development Needs (Skill)</span></td>
                  <td><span class="Calibri9Negrita">Comments (Behavior) (<i>25 character limit</i>):</span></td>
                </tr>

                <!---->

                <asp:Repeater ID="Repeater2" runat="server" DataSourceID="ObjectDataSourceDevelopmentNeeds">
                        <ItemTemplate>
                                <tr style=" line-height: 20px;">
                                    <td class="BORDEde" style="padding-bottom: 3.5pt" ><span class="Calibri9Negrita">
                                       <asp:Label ID="Label6" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />                         
                                    </span><span class="Calibri9">
                                        <asp:Label ID="Label15" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>'></asp:Label> &nbsp; </span></td>
                                    <td valign="top" style="padding-bottom: 3.5pt"><span class="Calibri9">
                                    <asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label> 
                                     &nbsp;</span></td>
                                </tr>
                       
                            </ItemTemplate>
                
                        </asp:Repeater>

                <!---->    
              </table>
  
            </asp:Panel>
  
            <!--SECTION H:  DEVELOPMENT ACTIONS-->
            <asp:Panel runat="server" ID="PanelSectionH">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
                <tr bgcolor="#DFDFDF">
                    <td class="BORDEiz-ar-deGris">
                    <span class="Calibri95NegritaBlue" style="font-size: 9.5pt">
                        <span style="mso-bidi-font-size:10.0pt;color:blue">SECTION H:&nbsp; DEVELOPMENT ACTIONS (Completed by Employee)
                        <o:p></o:p>
                        </span>
                    </span>
                    </td>
                </tr>
                <tr>
                    <td height="30">
       
        
                    <asp:Repeater ID="RepeaterSectionH" runat="server" DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS">
                    <ItemTemplate>
                                    <p><span class="Calibri9"><asp:Label runat="server" ID="LabelSectionH" Text='<%# Bind("Fundamentacion") %>' ></asp:Label>&nbsp;</span></p>
                   
                            </ItemTemplate>

                        </asp:Repeater>
                    </td>
                </tr>
                </table>
            </asp:Panel>
  
            
            <!--SECTION I:  POTENTIAL REPLACEMENTS-->
            <asp:Panel runat="server" ID="PanelSectionI">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">  
                    <tr bgcolor="#DFDFDF">
                        <td colspan="27" class="BORDEiz-ar-deGris">
                            <span class="Calibri95NegritaRed">SECTION I:&nbsp; POTENTIAL REPLACEMENTS (Completed by Manager)</span>
                        </td>
                    </tr>
                    <tr style='page-break-inside:avoid;'>
                        <td style="background:#E5E5E5;border:none;" colspan="8" valign="top">
                            <span class="Calibri9Negrita" >&nbsp;&nbsp;NAME  (Priority Order) </span> 
                        </td>
                        <td colspan="11" style='width:18.3%;border-top:none;border-left:solid windowtext 1.0pt; background:#E5E5E5;border-bottom:none;border-right:solid windowtext 1.0pt;'>
                            <span>
                                <b><span lang=EN-US style='text-transform:uppercase;mso-ansi-language:EN-US'><span class="Calibri9Negrita" >readiness</span></span></b>
                            </span>
                            <span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt'></span></span>  
                        </td>
                        <td colspan="8" style=' background:#E5E5E5;width:19.28%;border:none;padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt'>
                            <span style='mso-bookmark:Casilla1'>
                                <b><span lang=EN-US style='font-size:9.0pt;text-transform:uppercase;mso-ansi-language:EN-US'><span class="Calibri9Negrita">availability</span></span></b>
                            </span>
                            <span><span style='font-size:9.0pt'></span></span>  
                        </td>
                    </tr> 
                    <asp:Repeater ID="RepeaterPotencialReplacements1" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements1">
                <HeaderTemplate>
                    <tr style='page-break-inside:avoid; line-height: 15px;'>
                </HeaderTemplate>
                <ItemTemplate>
                            
                              <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "194" || Eval("ItemEvaluacionID").ToString()== "399" %>'  width="99%" colspan="8" style='width:62.14%;border:none;'>
                              <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b>
                              <span lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                  <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                  <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                  </span></b></p>  </td>
                              <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "195" || Eval("ItemEvaluacionID").ToString()== "400" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:
                              solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
                              ">
                              <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                               &nbsp;</o:p></span></span></p>  </td>
                   
                              <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "196" || Eval("ItemEvaluacionID").ToString()== "401" %>' width="19%" colspan=8 style='width:19.28%;border:none;'>
                              <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                              <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              

                   
                </ItemTemplate>
                <FooterTemplate>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="34" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                    <asp:Repeater ID="RepeaterPotencialReplacements2" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements2">
                   <HeaderTemplate>
                    <tr style='mso-yfti-irow:42;page-break-inside:avoid; line-height: 15px;'>
                </HeaderTemplate>
                <ItemTemplate>
                            
                              <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "197" || Eval("ItemEvaluacionID").ToString()== "402" %>'  width="99%" colspan=8 style='width:62.14%;border:none;'>
                              <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b><span
                              lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                  <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                  <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                  </span></b></p>  </td>
                              <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "198" || Eval("ItemEvaluacionID").ToString()== "403" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:
                              solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
                              ">
                              <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                               &nbsp;</o:p></span></span></p>  </td>
                   
                              <span id="span1" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "198"  || Eval("ItemEvaluacionID").ToString()== "403" %>' style="mso-bookmark:Casilla1"></span>
                              <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "199" || Eval("ItemEvaluacionID").ToString()== "404"  %>' width="19%" colspan=8 style='width:19.28%;border:none;'>
                              <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                              <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              
                   
                </ItemTemplate>
                <FooterTemplate>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="35" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                    <asp:Repeater ID="RepeaterPotencialReplacements3" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements3">
                    <HeaderTemplate>
                    <tr style='mso-yfti-irow:42;page-break-inside:avoid; line-height: 15px;'>
                    </HeaderTemplate>
                    <ItemTemplate>
                            
                                <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "200"  || Eval("ItemEvaluacionID").ToString()== "405" %>'  width="99%" colspan=8 style='width:62.14%;border:none;'>
                                <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b><span
                                lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                    <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                    <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                    </span></b></p>  </td>
                                <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "201" || Eval("ItemEvaluacionID").ToString()== "406" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;">
                                <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                                &nbsp;</o:p></span></span></p>  </td>
                   
                                <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "202" || Eval("ItemEvaluacionID").ToString()== "407" %>' width="19%" colspan=8 style='width:19.28%;border:none;'>
                                <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                                <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              
                              
                   
                    </ItemTemplate>
                    <FooterTemplate>
                
                    </tr>
                    </FooterTemplate>
                    </asp:Repeater>
                    <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="36" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
                </table> 
            </asp:Panel>
 
            </div><!--/wrapper-->

            <div style="page-break-before:always">&nbsp;</div>
        </asp:Panel>
        <!-- ============== [/MSD] ============== -->
        

        <!--
        <div runat="server" id="Divbr2">
            <br style="page-break-before:always">
        </div>
        -->


        <!-- ============== [RESUME] ============== -->
        <asp:Panel ID="PanelResume" runat="server" CssClass="bloque">

            <h1 align="center" style="text-transform:uppercase;font-family:Arial;">
                <asp:Label ID="lblNombreC" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblApellidoC" runat="server" Text=""></asp:Label>
            </h1>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pnet-subtitle" style="border-bottom:4px solid black;">Experience</td>
                </tr>
            </table>

            <!---->
            <asp:Repeater ID="DataListExperience" runat="server" DataSourceID="ObjectDataSourceExperience" >
            <%--onitemcommand="DataListExperience_ItemCommand" >--%>
                   <%-- onitemdatabound="DataListExperience_ItemDataBound">--%>
             
                <HeaderTemplate>
                    <table id="DataListExperience" class="pnet-table" width="100%">
                </HeaderTemplate>
                     <ItemTemplate>
                         <tr id="tridexperiencia" runat="server" Visible='<%# (Eval("Company").ToString() != "") %>' >
                            <td class="exp_block">
                             <table width="100%">
                                 <tr>
                                   <td colspan="2">
                                    <strong><asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Label></strong>
                                 </td>
                                 </tr>
                                 <tr>
                                    <td width="35%">
                                        <strong>
                                            <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%#(((RepeaterItem)Container).ItemIndex+1) %>' />
                                            <asp:Label ID="Label21" runat="server" Text='<%# Eval("FromDate").ToString() + " " %>' ></asp:Label>
                                            To <asp:Label ID="lblTodata" runat="server" Text='<%# " " + Eval("ToDate").ToString() %>'></asp:Label>
                                        </strong>
                                    </td>
                                    <td>
                          
                                    <strong>
                                    <asp:Label ID="lblPosition" runat="server" Text='<%# Eval("Position") %>'></asp:Label></strong>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lblJobDescriotion" runat="server" Text='<%# Eval("Description") %>'></asp:Label>    
                                    </td>
                                </tr>
                             </table>
                         </td>
                         </tr>
                     </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <asp:ObjectDataSource ID="ObjectDataSourceExperience" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetExperienceByTramiteID" 
                    TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            <!---->

            <br />

            <asp:Repeater ID="RepeaterResume" runat="server" DataSourceID="ObjectDataSourceResume">
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="pnet-subtitle" style="border-bottom:4px solid black;">
                                <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            </td>
                        </tr>
                    </table>

                    <table cellspacing="0" cellpadding="0" width="100%" class="pnet-table">
                        <tr>
                            <td>     
                                <asp:Label ID="lblComentarios" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>  
                            </td>
                        </tr>
                    </table><br />
                </ItemTemplate>
                <FooterTemplate></FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceResume" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="13" Name="SeccionID" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                                PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                                Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                </asp:ObjectDataSource>

        </asp:Panel>
        <!-- ============== [/RESUME] ============== -->

        
        <!--<div runat="server" id="SaltoPaginaResume"><br clear="all" style="page-break-before:always"/></div>-->


        <!-- ============== [CAREER PLAN] ============== -->
        <!--1-Career Plan (Employee and Supervisor)-->
        <asp:Panel runat="server" ID="PanelCareerPlan" CssClass="bloque">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                  <td class="pnet-subtitle" style="border-bottom:4px solid black;">
                    Career Plan
                  </td>
                  <td align="right" style="border-bottom:4px solid black;">
                    <asp:Label runat="server" ID="LabelPeriodoFiscalDescripcion"></asp:Label>
                  </td>
              </tr>
            </table>
            <br/>
            <table class="header2" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>1 - Career Plan (Employee and Supervisor)</td>
                </tr>
            </table>
            <h2 class="header2Print">1 - Career Plan (Employee and Supervisor)<span class="bg"></span></h2>


            <br/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="33%">
                    <strong>Name:</strong>
                    <asp:Label ID="lblNombreEvaluadoX" runat="server" Text=""></asp:Label> 
                    <asp:Label ID="lblApellidoEvaluadoX" runat="server" Text=""></asp:Label>  
                  </td>
                  <td width="33%">
                    <strong>Title:</strong>
                    <asp:Label ID="lblCargoX" runat="server" Text=""></asp:Label>
                  </td>
                  <td width="34%">
                    <strong>Job Level:</strong>
                    <asp:Label ID="lblJobLevelH" runat="server" Text="Label"></asp:Label>
                  </td>
                </tr>
            </table>
            <br />

            <!--Long Term:-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pnet-table">
                <tr>
                    <td colspan="2">
                        <h3>Long Term:</h3>
                        3-5 Year &ldquo;End in Mind&rdquo;. List 1-3 types of jobs and organizational levels that represent the career goals:</p>
                    </td>
                </tr>
                <asp:Repeater ID="RepeaterCareerPlanLongTerm" runat="server" DataSourceID="ObjectDataSourceCareerPlanLongTerm" >
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# buildLongTerm(Eval("ItemEvaluacionID") , Eval("Titulo"), Eval("Fundamentacion"),Eval("Ponderacion")) %>' ></asp:Label>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanLongTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="17" Name="SeccionID" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <!---->
            </table>        
            <br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pnet-table">
                <tr>
                    <td colspan="4" bgcolor="#E5E5E5">
                        <i>This section to be prepared following an additional management
                        review of the employee&rsquo;s long term career goals. When completed,
                        Supervisor and Employee sign and date.</i>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" valign="bottom" height="50" bgcolor="#E5E5E5"></td>
                </tr>
                <tr>
                    <td width="35%" bgcolor="#E5E5E5" height="15">Supervisor</td>
                    <td width="15%" bgcolor="#E5E5E5">Date</td>
                    <td width="35%" bgcolor="#E5E5E5">Employee</td>
                    <td width="15%" bgcolor="#E5E5E5">Date</td>
                </tr>
            </table>
            <br/>

            <!--Near Term:-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pnet-table">
                <tr>
                    <td colspan="2">
                        <h3>Near Term:</h3>
                        What is (are) the most logical assignment(s) in the near-term (0-2) years out?</p>
                    </td>
                </tr>
                <asp:Repeater ID="RepeaterCareerPlanNearTerm" runat="server" DataSourceID="ObjectDataSourceCareerPlanNearTerm">               
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# buildNearTerm(Eval("ItemEvaluacionID") , Eval("Titulo"), Eval("Fundamentacion"),Eval("Ponderacion")) %>' ></asp:Label>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanNearTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="18" Name="SeccionID" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </table>
            <br/>
        </asp:Panel>  

        <!--2-Feedback-->
        <asp:Panel runat="server" ID="PanelFeedback" CssClass="bloque">
            <table class="header2" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>2 - Feedback</td>
                </tr>
            </table>
            <h2 style="display:none;">2 - Feedback<span class="bg"></span></h2>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="3" height="10"></td>
                </tr>
                <tr>
                  <td width="48%" class="texto10times"><strong>List the top three rated Key Success in last year.</strong></td>
                  <td width="4%" valign="top" class="texto10times">&nbsp;</td>
                  <td width="48%" valign="top" class="texto10times"><strong>List performance ratings for last  three years.</strong></td>
                </tr>
                <tr>
                    <td colspan="3" height="10"></td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="48%">
                        <asp:Repeater ID="RepeaterFeedback" runat="server" DataSourceID="ObjectDataSourceFeedback">
                            <HeaderTemplate>
                                <table  width="100%" id="RepeaterFeedback">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="bordeinferior1_x"><asp:label ID="labelComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="70"></asp:label></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:ObjectDataSource ID="ObjectDataSourceFeedback" runat="server" OldValuesParameterFormatString="original_{0}"
                            SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="12" Name="SeccionID" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                                    PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                                    Type="Int32" />
                                <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                    PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                    <td width="4%"></td>
                    <td width="48%">
                        <asp:Repeater ID="RepeaterPerformanceRatings" runat="server" 
                        DataSourceID="ObjectDataSourceLastRating" >
                            <HeaderTemplate>
                                <table width="100%">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                <td  class="bordeinferior1_x">
                                    <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label>
                                    <asp:Label ID="LabelCalificacion" runat="server" Text='<%# DefaultVal(Eval("Calificacion")) %>'></asp:Label> 
                                    <asp:Label ID="Label12" runat="server" Text='<%# GetTextRating((((RepeaterItem)Container).ItemIndex+1)) %>'></asp:Label>
                                    </td>
                                </tr>
                   
                            </ItemTemplate>
                            <FooterTemplate>
                                </table></FooterTemplate>
                        </asp:Repeater>
                        <asp:ObjectDataSource ID="ObjectDataSourceLastRating" runat="server" 
                        OldValuesParameterFormatString="original_{0}" 
                        SelectMethod="GetLastRatings" 
                        TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                                PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="periodoActual" 
                                PropertyName="Value" Type="Int32" />
                                <asp:Parameter DefaultValue="3" Type="Int32" Name="TipoPeriodoID" />
                        </SelectParameters>
                        </asp:ObjectDataSource>
                    </td>
                </tr>
            </table>

  
  
            <br />
            <br />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="texto10timesitalica">Please sign to verify that this sheet accurately reflects Supervisor/Employee input. It will be used in subsequent career goals discussions/MS&amp;D forums.</td>
    </tr>
  </table>
            <br/>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="291" class="bordeinferior1_x">&nbsp;</td>
            <td width="80" class="bordeinferior1_x">&nbsp;</td>
            <td width="17">&nbsp;</td>
            <td width="268" class="bordeinferior1_x">&nbsp;</td>
            <td width="80" class="bordeinferior1_x">&nbsp;</td>
        </tr>
        <tr>
            <td class="texto10times">Supervisor</td>
            <td class="texto10times">Date</td>
            <td>&nbsp;</td>
            <td class="texto10times">Employee</td>
            <td class="texto10times">Date</td>
        </tr>
        </table>	
    </asp:Panel>
        <!-- ============== [/CAREER PLAN] ============== -->


        <div class="registro">
            <asp:label ID="LabelRegistro" runat="server" text="Label"></asp:label>
        </div>
    </form>
</div>
</body>
</html>

 
