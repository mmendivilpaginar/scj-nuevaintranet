﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPPlanAndReview.ascx.cs" Inherits="com.paginar.johnson.Web.PMPFY.UserControlPMPFY.ucPPlanAndReview" %>

    <h3>Performance Plan & Review</h3>
    <div class="PMPIntructions">
    <p>If planning the YEAR AHEAD, list the desired objectives for this individual, up to five(5), and indicate measures (how the degree of achievement will be assessed). If reviewing the YEAR COMPLETED, list the results and the degree of completion (1-5) as follows:</p>
    <ul class="valuation">
        <li>1 = not achieved</li>
        <li>2 = partially achieved</li>
        <li>3 = achieved</li>
        <li>4 = exceeded</li>
        <li>5 = far exceeded</li>
    </ul> 
    </div>
    <div class="PMPContent">
        <table>
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <asp:Label ID="lblContenidoTabla" runat="server" Text=""></asp:Label>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>