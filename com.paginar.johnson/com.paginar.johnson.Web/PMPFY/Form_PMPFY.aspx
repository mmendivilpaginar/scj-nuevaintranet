﻿<%@ Page Title="" Theme="PMPFY"  Language="C#" MasterPageFile="~/PMPFY/MasterPages/MPFY.Master" AutoEventWireup="true" CodeBehind="Form_PMPFY.aspx.cs" Inherits="com.paginar.johnson.Web.PMPFY.Form_PMPFY"
 EnableEventValidation="false" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../PMP/OKMessageBox.ascx" tagname="OKMessageBox" tagprefix="uc1" %>
<%@ Register src="../PMP/WebUserControlGuardadoPrevio.ascx" tagname="WebUserControlGuardadoPrevio" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="/PMPFY/js/initpmpFY.js"></script>
    <script type="text/javascript" src="/PMPFY/js/ManageListBox.js"></script>
   <script type="text/javascript">


       function MostrarOtraAreaDeseada(value, IDTextBoxOther) {

           if (value == "310")

               $(IDTextBoxOther).show();
           else
               $(IDTextBoxOther).hide();

       }
       function ValidarCareerPlanOther(sender, args) {

           args.IsValid = true;

           var DropDownListRating = sender.attributes["DropDownListRating"].value;
           var TextBoxOther = sender.attributes["TextBoxOther"].value;


           if ($(DropDownListRating).val() == "310" && $(TextBoxOther).val() == "")
               args.IsValid = false;



       }

       function ValidarCursosSeleccionados(sender, args) {
           args.IsValid = true;
           // var CursosSeleccionados = CursosSeleccionadosAux.find('option').clone();


           var CursosSeleccionados = $('select[name$=ListBoxCursosSeleccionados]').find('option').length;
           if (CursosSeleccionados < 1)
               args.IsValid = false;

       }




       function ValidarEducacion(sender, args) {
           args.IsValid = true;

           // $('#DataListEducation tr').each(function () {

           var TextBoxYear = sender.attributes["TextBoxYear"].value;
           var DropDownListDegree = sender.attributes["DropDownListDegree"].value;
           var TextBoxSchool = sender.attributes["TextBoxSchool"].value;
           var HiddenFieldID = sender.attributes["HiddenFieldID"].value;



           var year = $(TextBoxYear).val();
           var Degre = $(DropDownListDegree).val();
           var School = $(TextBoxSchool).val();
           var ID = $(HiddenFieldID).val();


           if (year != '' || Degre != '' || School != '' || ID == '1') {
               if (year == '' || Degre == '' || School == '')
                   args.IsValid = false;

           }




       }

       function ValidarLanguage(sender, args) {
           args.IsValid = true;

           var TextBoxIdioma = sender.attributes["TextBoxIdioma"].value;
           var DropDownListProficiency = sender.attributes["DropDownListProficiency"].value;
           var CheckBoxNative = sender.attributes["CheckBoxNative"].value;
           var HiddenFieldNumber = sender.attributes["HiddenFieldNumber"].value;

           var Idioma = $(TextBoxIdioma).val();
           var Proficiency = $(DropDownListProficiency).val();
           var IsNative = $(CheckBoxNative).attr('checked');
           var Number = $(HiddenFieldNumber).val();


           if (Idioma != '' || Proficiency != '' || Number == 1) {
               if (Idioma == '' || Proficiency == '')
                   args.IsValid = false;

           }




       }

       function ValidarExperience(sender, args) {
           args.IsValid = true;

           var TextBoxToDate = sender.attributes["TextBoxToDate"].value;
           var TextBoxFromDate = sender.attributes["TextBoxFromDate"].value;
           var TextBoxCompany = sender.attributes["TextBoxCompany"].value;
           var TextBoxPosition = sender.attributes["TextBoxPosition"].value;
           var TextBoxDescription = sender.attributes["TextBoxDescription"].value;
           var HiddenFieldID = sender.attributes["HiddenFieldID"].value;


           var FromDate = $(TextBoxToDate).val();
           var ToDate = $(TextBoxFromDate).val();
           var Company = $(TextBoxCompany).val();
           var Position = $(TextBoxPosition).val();
           var Description = $(TextBoxDescription).val();
           var ID = $(HiddenFieldID).val();

           var TipoFormularioID = $('input[name$=HiddenFieldTipoFormularioID]').val();





           if (FromDate != '' || ToDate != '' || Company != '' || Position != '' || Description != '' || ID == '1') {
               if (FromDate == '' || ToDate == '' || Company == '' || Position == '' || Description == '')
                   args.IsValid = false;

           }




       }

       function ValidarJobDescription(sender, args) {
           args.IsValid = true;


           var TextBoxDescription = sender.attributes["TextBoxDescription"].value;
           var HiddenFieldID = sender.attributes["HiddenFieldID"].value;


           var Description = $(TextBoxDescription).val().length;
           var ID = $(HiddenFieldID).val();

           var TipoFormularioID = $('input[name$=HiddenFieldTipoFormularioID]').val();

           if (ID == '1' && TipoFormularioID == '11' && Description < 100) {
               args.IsValid = false;

           }




       }



       function ValidarSuccessFactors(sender, args) {

           args.IsValid = true;

           var DropDownListRating = sender.attributes["DropDownListRating"].value;
           var DropDownListOptional;

           if (sender.attributes["DropDownListOptional"] != null) {
               DropDownListOptional = sender.attributes["DropDownListOptional"].value;

               if ($(DropDownListOptional).val() != 0 && $(DropDownListRating).val() == 0)
                   args.IsValid = false;
           }

           else {
               if ($(DropDownListRating).val() == 0)
                   args.IsValid = false;
           }

       }


       function ValidarDevelopmentNeeds(sender, args) {

           args.IsValid = true;

           var grid = document.getElementById('RepeaterDevelopmentNeeds');

           if (grid.rows.length > 0) {

               for (i = 0; i < grid.rows.length; i++) {
                   cell1 = grid.rows[i];

                   var Necesidad1 = $(cell1).find('select option:selected').val();

                   if (Necesidad1 != 0) {

                       for (j = i + 1; j < grid.rows.length; j++) {

                           cell1 = grid.rows[j];
                           var Necesidad2 = $(cell1).find('select option:selected').val();

                           if (Necesidad1 == Necesidad2) {
                               args.IsValid = false;
                               return;
                           }
                       }
                   }

               }
           }



       }

       function ValidarStrengths(sender, args) {
           args.IsValid = true;

           var grid = document.getElementById('RepeaterSTRENGTHS');

           if (grid.rows.length > 0) {

               for (i = 0; i < grid.rows.length; i++) {
                   cell1 = grid.rows[i];

                   var Strengths1 = $(cell1).find('select option:selected').val();
                   if (Strengths1 != 0) {

                       for (j = i + 1; j < grid.rows.length; j++) {

                           cell1 = grid.rows[j];
                           var Strengths2 = $(cell1).find('select option:selected').val();
                           if (Strengths1 == Strengths2) {
                               args.IsValid = false;
                               return;
                           }
                       }
                   }

               }
           }



       }

       function ValidarStregthsNeeds(sender, args) {
           args.IsValid = true;
           var grid1 = document.getElementById('RepeaterSTRENGTHS');
           var grid2 = document.getElementById('RepeaterDevelopmentNeeds');

           if (grid1.rows.length > 0 && grid2.rows.length > 0) {

               for (i = 0; i < grid1.rows.length; i++) {

                   cell1 = grid1.rows[i]; //.cells[1];

                   var Fortaleza = $(cell1).find('select option:selected').val();
                   if (Fortaleza != 0) {
                       for (j = 0; j < grid2.rows.length; j++) {

                           cell1 = grid2.rows[j]; //.cells[1];
                           var Necesidad = $(cell1).find('select option:selected').val();
                           if (Fortaleza == Necesidad) {
                               args.IsValid = false;
                               return;
                           }
                       }
                   }
               }
           }
       }



       function ValidarNextAssigment(sender, args) {
           args.IsValid = true;

           if ($('input[name$=HiddenFieldTipoFormularioID]').val() == '10')
               return true;

           if ($('input[name$=HiddenFieldPasoID]').val() == '2')
               return true;

           var grid = document.getElementById('RepeaterNextAssigment');

           if (grid.rows.length > 0) {

               for (i = 1; i < grid.rows.length; i++) {
                   cell1 = grid.rows[i].cells[1];

                   var Value1 = $(cell1).find('select option:selected').val();

                   if (Value1 != 0) {

                       for (j = i + 1; j < grid.rows.length; j++) {

                           cell1 = grid.rows[j].cells[1];
                           var Value2 = $(cell1).find('select option:selected').val();
                           if (Value2 == Value1) {
                               args.IsValid = false;
                               return;
                           }
                       }
                   }

               }
           }

       }
       function ValidarCareerInterests(sender, args) {
           args.IsValid = true;

           if ($('input[name$=HiddenFieldTipoFormularioID]').val() == '10')
               return true;

           var grid = document.getElementById('RepeaterCareerInterests');

           if (grid.rows.length > 0) {

               for (i = 1; i < grid.rows.length; i++) {
                   cell1 = grid.rows[i].cells[1];

                   var Value1 = $(cell1).find('select option:selected').val();
                   var ValueDescrip = $(cell1).find('select option:selected').text();

                   if (Value1 != 0 && ValueDescrip != 'Other') {

                       for (j = i + 1; j < grid.rows.length; j++) {

                           cell1 = grid.rows[j].cells[1];
                           var Value2 = $(cell1).find('select option:selected').val();
                           if (Value2 == Value1) {
                               args.IsValid = false;
                               return;
                           }
                       }
                   }

               }
           }

       }


       function SetLastRating(IDDropDown) {


           var value = $('#' + IDDropDown + ' option:selected').text();
           if (value != "Choose an option") {
               $('#RepeaterSeccionBPerformanceRatings tr:first').find("span:eq(2)").html(value);
               $('#RepeaterPerformanceRatings tr:first').find('span:eq(2)').html(value);
           }
           else {
               $('#RepeaterSeccionBPerformanceRatings tr:first').find("span:eq(2)").html("");
               $('#RepeaterPerformanceRatings tr:first').find('span:eq(2)').html("");

           }


       }

       ////

       window.onbeforeunload = confirmOnClose;
       // var flagCierre;// = 2;

       function confirmOnClose() {
           if (flagCierre == 1) {
               flagCierre = 2;
               return "Usted retornará al Panel de Control."; //Cierre Guardar sin enviar 1
           }
           else {
               if (flagCierre == 2) {
                   return 'Si el formulario no fue guardado es probable que pierda los datos'; // Cieerre de X
               }
           }
       }

       function GuardarSinEnviar() {
           flagCierre = 1;
           window.close();
       }
       ////



   </script>

   <style>
       
.info, .success, .warning, .error, .validation {
border: 1px solid;
margin: 10px 0px;
padding:15px 10px 15px 50px;
background-repeat: no-repeat;
background-position: 10px center;
}

.warning {
color: #9F6000;
background-color: #FEEFB3;
background-image: url('../css/images/alerta.png');
}
   </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPH1" runat="server">
    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
        var PanelPopEvaluador_ModalPopupExtender = '<%= PanelPopEvaluador_ModalPopupExtender.ClientID %>';
        var UpdateProg1 = '<%= UpdateProg1.ClientID %>';

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

        function beginReq(sender, args) {

            // muestra el popup
            if (args.get_postBackElement() != null) {
                $find(ModalProgress).show();
                $('#' + UpdateProg1).show();
            }


        }



        function endReq(sender, args) {

            //  esconde el popup 
            $find(ModalProgress).hide();

        }



</script>
    <asp:HiddenField ID="HiddenFieldPeriodoID" Value="1" runat="server" />
    <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldPasoID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldClusterID" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldAreaID" Value="0" runat="server" />
    <asp:HiddenField ID="HiddenFieldDebilidadesSeleccionadas" Value="" runat="server" />
    <asp:HiddenField ID="HiddenFieldCursosSeleccionados" Value="" runat="server" />

    <asp:ValidationSummary ID="ValidationSummaryEnviarEvaluador" runat="server" ValidationGroup="EnviarEvaluador"
        CssClass="mensaje error" />

<%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
            <uc2:WebUserControlGuardadoPrevio EnabledTimer="true" ID="WUCGuardadoPrevio" runat="server" />
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>

        <asp:Panel ID="PanelPopEvaluador" CssClass="contentPopupEvaluador" style="display:none" runat="server"> 
        <asp:Panel CssClass="titleModalPopup" ID="titleBarPopEval" runat="server">
            <h2>Seleccione su evaluador</h2>
            <span id="btnCloseLE" class="closeModalPopup" style=" display:none;">Cerrar</span>
        </asp:Panel>	
		<div class="contentModalPopup">

            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                    TargetControlID="DropDownEvaluadores"
                    PromptCssClass="ListSearchExtenderPrompt"
                    PromptText="Escriba para buscar"/> 
             <asp:DropDownList ID="DropDownEvaluadores" runat="server" 
                DataSourceID="ObjectDataGetEvaluators" 
                DataTextField="ApellidoNombre" DataValueField="Legajo" 
                ondatabound="DropDownEvaluadores_DataBound" AppendDataBoundItems="true">
                <asp:ListItem Text="-Seleccione-" Value=""></asp:ListItem>
                 </asp:DropDownList>               
            <asp:RequiredFieldValidator ControlToValidate="DropDownEvaluadores" ID="RequiredFieldValidator11"
                runat="server" ErrorMessage="Debe Seleccionar un evaluador válido" ValidationGroup="CambiarEvaluador">*</asp:RequiredFieldValidator>
             <asp:ObjectDataSource ID="ObjectDataGetEvaluators" runat="server"
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetEvaluadoresByPeriodoID"
                TypeName="com.paginar.formularios.dataaccesslayer.FormulariosDSTableAdapters.Eval_evaluadoresTableAdapter">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />                    
                     <asp:ControlParameter ControlID="HiddenFieldLegajo" Name="Legajo" PropertyName="Value"
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <asp:Label ID="lblEvaluadorAsignado" runat="server" Text="Seleccione su evaluador"></asp:Label>
              
            <asp:Button ID="GuardarNuevoEvaluador" runat="server" Text="Actualizar" ValidationGroup="CambiarEvaluador"
                onclick="GuardarNuevoEvaluador_Click" visible="false"
                onclientclick="javascript:document.body.onbeforeunload='';" />
        <asp:UpdatePanel ID="UpdatePanelEnviar" runat="server">
            <ContentTemplate>
            <br />
            <span class="button">
            <asp:Button ID="EnviarEvaluacion" runat="server" Text="Enviar" ValidationGroup="CambiarEvaluador"
                onclick="EnviarEvaluacion_Click" visible="false"
                onclientclick="javascript:document.body.onbeforeunload=''; $find(PanelPopEvaluador_ModalPopupExtender).hide();" />
            </span>
                </ContentTemplate>
       </asp:UpdatePanel>
      
		</div>
    </asp:Panel>	

  
    <asp:Button ID="btnChangeEvaluator" runat="server" Visible="true" Text="X" style="display:none"/>
    <asp:ModalPopupExtender ID="PanelPopEvaluador_ModalPopupExtender" runat="server"
        TargetControlID="btnChangeEvaluator"
        OkControlID="btnCloseLE" 
        BackgroundCssClass="modalBackground" PopupControlID="PanelPopEvaluador">
    </asp:ModalPopupExtender>


        <asp:modalpopupextender id="modalpopupImprimir" runat="server" 
        okcontrolid="btnOkayImprimir" 
        targetcontrolid="Imprimir" popupcontrolid="PanelImpresion" 
        backgroundcssclass="modalBackground">
    </asp:modalpopupextender>

        <asp:Panel ID="PanelImpresion" CssClass="contentPopupEvaluador" style="display:none" runat="server"> 
        <asp:Panel CssClass="titleModalPopup" ID="Panel2" runat="server">
            <h2>Seleccione una seccion a Imprimir</h2>
            <span id="btnOkayImprimir" class="closeModalPopup">Cerrar</span>
        </asp:Panel>	
		<div class="contentModalPopup">

            <asp:DropDownList ID="DropDownListImprimirSeccion" runat="server">
                <asp:ListItem Text="Print All" Value="PrintAll" Selected="True"></asp:ListItem>
                <asp:ListItem Text="PMP" Value="PMP"></asp:ListItem>
                <asp:ListItem Text="MS&D" Value="MSD"></asp:ListItem>
                <%--<asp:ListItem Text="Career Plan" Value="CareerPlan"></asp:ListItem>--%>                
                <asp:ListItem Text="Resume" Value="Resume"></asp:ListItem>
            </asp:DropDownList>           
            <br />    
            <br />   
         <asp:Label ID="Label7" runat="server" Text="(Guardar el formulario antes de mandar a imprimir para ver los últimos datos ingresados)" Font-Size="11px"></asp:Label>
         <br />   
         <br />   
            <asp:Button ID="ButtonImprimirPopUp" runat="server" Text="Imprimir/Exportar" OnClientClick="ImprimirSeccion();return false" />
      
		</div>
    </asp:Panel>	


  <%--    </td>
                </tr> 
            <tr> --%>
  <h2 class="hide">Evaluación</h2>
    <div id="tabs">
        
      <ul>
        <li id="t1"><a href="#tabs-1">Performance Plan & Review</a></li>
        <li id="t2"><a href="#tabs-2">Performance Review</a></li>
        <li id="t3"><a href="#tabs-3">Current Strengths/Development Needs</a></li>
        <li id="t4"><a href="#tabs-4">Development Actions For The Next Fiscal Year</a></li>
        <li id="t5"><a href="#tabs-5">Performance Review - Summary</a></li>
        <li id="t6"><a href="#tabs-6">EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN</a> </li>
        <li id="t7"><a href="#tabs-7">Resume</a> </li>
        <li id="t8" runat="server"><a href="#tabs-8">Career Plan</a> </li>
        <li id="t9" runat="server"><a href="#tabs-9">Feedback</a> </li>        
      </ul>

        <div id="tabs-1">
        <h3>PERFORMANCE PLAN & REVIEW </h3>
        
       
         

        If planning the YEAR AHEAD, list the desired objectives for this individual, up to five (5), and indicate measures (how the degree of achievement will be assessed). 
        If reviewing the YEAR COMPLETED, list the results and the degree of completion (1-5) as follows:  
        <ul class="horizontal-list dos_cols">

               <li><strong>1=</strong>Did not meet expectation</li>
               <li class="border-left"><strong>2=</strong>Met some, but not all expectations</li>
               <li class="border-left"><strong>3=</strong>Met expectations </li>
               <li class="border-left"><strong>4=</strong>Exceeded most expectations </li>
               <li class="border-left"><strong>5=</strong>Significantly exceeded expectations  </li>
         </ul>
        
        
        


                
       <asp:ObjectDataSource ID="ObjectDataSourceCalificaciones" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCalificaciones" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                 <SelectParameters>
                     <asp:Parameter DefaultValue="I" Name="Idioma" Type="String" />
                     <asp:Parameter DefaultValue="P" Name="modulo" Type="String" />
                 </SelectParameters>
         </asp:ObjectDataSource>


<div id="IndividualObjetive">


<h4>1. INDIVIDUAL GOALS</h4>




              <asp:Repeater ID="RepeaterIndividualObjetives1" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives1">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives1" >
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>' 
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>

                          <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage="PERFORMANCE PLAN & REVIEW: Rating 1" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="none">*</asp:CompareValidator>
                        


                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5"  SkinID="form-textarea" MaxL="500"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentario" runat="server" ErrorMessage='<%# String.Format("PERFORMANCE PLAN & REVIEW: No completó el campo {0} 1", Eval("Titulo") )%>' ValidationGroup="EnviarEvaluador"
                         ControlToValidate="TextBoxComentario"  Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>' Display="None">*</asp:RequiredFieldValidator>
                         <asp:Label ID="Label2" runat="server" >*</asp:Label>  







                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="24" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>


<h4>2. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives2" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives2">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives2">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                          <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage="PERFORMANCE PLAN & REVIEW: Rating 2" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="none">*</asp:CompareValidator>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentario" runat="server" ErrorMessage='<%# String.Format("PERFORMANCE PLAN & REVIEW: No completó el campo {0} 2", Eval("Titulo") )%>' ValidationGroup="EnviarEvaluador"
                         ControlToValidate="TextBoxComentario"  Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>' Display="None">*</asp:RequiredFieldValidator>
                        <asp:Label ID="Label2" runat="server" >*</asp:Label>  



                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="25" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>


<h4>3. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives3" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives3">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives3">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                          <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage="PERFORMANCE PLAN & REVIEW: Rating 3" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="none">*</asp:CompareValidator>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentario" runat="server" ErrorMessage='<%# String.Format("PERFORMANCE PLAN & REVIEW: No completó el campo {0} 3", Eval("Titulo") )%>' ValidationGroup="EnviarEvaluador"
                         ControlToValidate="TextBoxComentario"  Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>' Display="None">*</asp:RequiredFieldValidator>

                         <asp:Label ID="Label2" runat="server" >*</asp:Label>  


                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="26" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

<asp:Button ID="ButtonShowDivObjetivo4" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(4); return false;"/>
<div id="DivObjetivo4" style=" display:none">
<h4>4. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives4" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives4">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives4">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives4" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="27" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo5" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(5); return false;"  />
</div>



<div id="DivObjetivo5" style=" display:none">
<h4>5. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives5" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives5">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives5">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives5" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="28" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo6" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(6); return false;"  />
</div>

<div id="DivObjetivo6" style=" display:none">
<h4>6. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives6" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives6">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives6">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives6" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="29" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo7" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(7); return false;"  />
</div>
<div id="DivObjetivo7" style=" display:none">
<h4>7. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives7" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives7">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives7">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives7" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="30" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo8" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(8); return false;"  />
</div>
<div id="DivObjetivo8" style=" display:none">
<h4>8. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives8" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives8">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives8">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives8" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="31" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo9" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(9); return false;"  />
</div>
<div id="DivObjetivo9" style=" display:none">
<h4>9. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives9" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives9">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives9">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives9" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="32" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<asp:Button ID="ButtonShowDivObjetivo10" runat="server" Text="Agregar Meta" ToolTip="Agregar Objetivo" OnClientClick="AgregarObjetivo(10); return false;"  />
</div>
<div id="DivObjetivo10" style=" display:none">
<h4>10. INDIVIDUAL GOALS</h4>


              <asp:Repeater ID="RepeaterIndividualObjetives10" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives10">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterIndividualObjetives10">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificaciones" SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="descripcion" DataValueField="calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                        TextMode="MultiLine" Rows="5" SkinID="form-textarea"></asp:TextBox>




                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives10" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="33" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
</div>
 </div>      

       
       
       
       </div>
        <div id="tabs-2">
         <h3>PERFORMANCE REVIEW - Success Factors for Effective Leadership </h3>

        <table class="review-info">
            <tr> <td ><div align="center">( 1 )  
                Did not meet expectation </div></td>
                <td >This person is currently not meeting the performance standards of the company for this position </td>
            </tr>
            <tr>
                <td><div align="center">( 2 )
                Met some, but not all expectations </div></td>
                <td>This person meets some but not all of the performance standards of the company for this position</td>
            </tr>
            <tr>
                <td ><div align="center">( 3 ) 
                Met expectations </div></td>
                <td  >This person meets and occasionally exceeds the high standards of the company for this position</td>
            </tr>
            <tr>
                <td  ><div align="center">( 4 ) 
                Exceeded most expectations </div></td>
                <td  >This person meets and frequently exceeds the high standards of the company for this position</td>
            </tr>
            <tr>
                <td  ><div align="center">( 5 ) 
                Significantly exceeded expectations </div></td>
                <td  >Through leadership and positive example, this person is a positive influence to the broader organization</td>
            </tr>
            </table>   
        
        
        

                <asp:Repeater runat="server" id="RepeaterSuccessFactors" 
                DataSourceID="ObjectDataSourceSuccessFactors" 
                onitemdatabound="RepeaterSuccessFactors_ItemDataBound">
             <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" class="separateTR" id="RepeaterSuccessFactors">
             </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' /> 
                        <asp:HyperLink ID="HyperLink1" CssClass="masInfo" runat="server" ToolTip='<%# Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />") %>'>
                        <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        <asp:Image ID="Image1" runat="server" ImageUrl="img/icoHelp.jpg" />
                        </asp:HyperLink>
                    </td>
                <%-- </tr>
                <tr>
                    <td>
                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("Descripcion") %>'></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td>--%>
                            <asp:DropDownList ID="DropDownListOptional" runat="server" DataSourceID="ObjectDataSourceFortalezas"   SelectedValue='<%# Eval("Fundamentacion") %>'
                              DataTextField="descripcion" DataValueField="Codigo" AppendDataBoundItems="True" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OPTIONAL")>=0 %>'>
                            <asp:ListItem Value="">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                        TargetControlID="DropDownListOptional"
                        PromptCssClass="ListSearchExtenderPrompt"
                        PromptText="Escriba para buscar"/> 
                <%--    </td>
                </tr> 
            <tr> --%>
            <td class="block">Rating:
                 <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificacionesSummary" SelectedValue='<%# Eval("Ponderacion") %>'
                          DataTextField="calificacion" DataValueField="calificacion" AppendDataBoundItems="True">
                           <asp:ListItem Value="0">Choose an option</asp:ListItem>
                 </asp:DropDownList>
                 <asp:CustomValidator ID="CustomValidatorSuccessFactors" runat="server" Text="*"
                                ClientValidationFunction="ValidarSuccessFactors" ValidationGroup="EnviarEvaluador" Display="None"></asp:CustomValidator>
            <asp:Label runat="server" ID="LabelAsterisco" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OPTIONAL")<0 %>'>*</asp:Label>

                 
                 </td></tr>

            </ItemTemplate>

            <FooterTemplate>
                    </table></FooterTemplate>
       </asp:Repeater>

        <asp:ObjectDataSource ID="ObjectDataSourceSuccessFactors" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="7" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

       

        </div>
        <div id="tabs-3">
            <div class="warning">
             NO DEBEN COMPLETARSE LOS DATOS DE ESTA SOLAPA
           </div>

        <h3>CURRENT STRENGTHS / DEVELOPMENT NEEDS</h3>

        <div>Use skills from the <a href="documents/SCJCompetencyList2012_2013.xls" target="_blank">SCJ Competency List</a>.  Be sure to complete the observations. Comments (Behavior) (100 character limit) </div>

       

        <div class="separador">
            <h4>STRENGTHS</h4>

             <asp:Repeater ID="RepeaterSTRENGTHS" runat="server" DataSourceID="ObjectDataSourceSTRENGTHS">
                    <HeaderTemplate>
                        <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="95%" id="RepeaterSTRENGTHS">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td  >
                               <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>
                            <td   >

                                <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceFortalezas"  SelectedValue='<%# Eval("Ponderacion") %>'
                                   Enabled="false"
                                     DataTextField="Descripcion" DataValueField="codigo" AppendDataBoundItems="true">
                                     <asp:ListItem Value="0">Choose an option</asp:ListItem>
                                </asp:DropDownList>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                              <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("CURRENT STRENGTHS / DEVELOPMENT NEEDS: CURRENT STRENGTHS. No completó el campo strength {0}", Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador" Enabled="false"
                               Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None">*</asp:CompareValidator>

                              <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                            </td>
                            <td style="width:60%" ><asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="100" SkinID="form-textareaAlt" ReadOnly="true"></asp:TextBox>
                             
                                
                                


                                    </td>
                        </tr>
                   
                    </ItemTemplate>
                    <FooterTemplate>
                        </table></FooterTemplate>
                </asp:Repeater>

             <asp:CustomValidator ID="CustomValidatorStrengths" runat="server" ErrorMessage="CURRENT STRENGTHS / DEVELOPMENT NEEDS: CURRENT STRENGTHS. No debe duplicar las Fortalezas"  ClientValidationFunction="ValidarStrengths" ValidationGroup="EnviarEvaluador"
               Enabled="false"
              ></asp:CustomValidator>
             <asp:ObjectDataSource ID="ObjectDataSourceSTRENGTHS" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="15" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        
        <div class="separador">
            <h4>DEVELOPMENT NEEDS</h4>
            <asp:Repeater ID="RepeaterDevelopmentNeeds" runat="server" 
                DataSourceID="ObjectDataSourceDevelopmentNeeds" 
                onitemdatabound="RepeaterDevelopmentNeeds_ItemDataBound" >
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="95%" id="RepeaterDevelopmentNeeds">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>
                        <td>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                            <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceFortalezas"  SelectedValue='<%# Eval("Ponderacion") %>' Enabled="false"
                                    DataTextField="Descripcion" DataValueField="codigo" AppendDataBoundItems="true">
                                    <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" 
                            ErrorMessage='<%# String.Format("CURRENT STRENGTHS / DEVELOPMENT NEEDS: DEVELOPMENT NEEDS. No completó el campo Development Need {0}", Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador" Enabled="false"
                            Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None">*</asp:CompareValidator>
                           

                           <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                        </td>
                        <td  style="width:60%" ><asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="100" SkinID="form-textareaAlt" ReadOnly="false"></asp:TextBox>
                            
                            


                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>

        <asp:CustomValidator ID="CustomValidatorDevelopmentNeeds" runat="server" ErrorMessage="CURRENT STRENGTHS / DEVELOPMENT NEEDS: DEVELOPMENT NEEDS No debe duplicar las Necesidades"  ClientValidationFunction="ValidarDevelopmentNeeds" ValidationGroup="EnviarEvaluador"  Enabled="false" ></asp:CustomValidator>
        <asp:CustomValidator ID="CustomValidatorStrengthsNeeds" runat="server" ErrorMessage="CURRENT STRENGTHS / DEVELOPMENT NEEDS: Una fortaleza no puede ser tambien una necesidad"  ClientValidationFunction="ValidarStregthsNeeds" ValidationGroup="EnviarEvaluador" Enabled="false">*</asp:CustomValidator>
         <asp:ObjectDataSource ID="ObjectDataSourceDevelopmentNeeds" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="16" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        
        <asp:ObjectDataSource ID="ObjectDataSourceFortalezas" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetFortalezasByTipoFormularioID"  TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
        <SelectParameters>
            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID" PropertyName="Value" Type="Int32" />            
        </SelectParameters>
         </asp:ObjectDataSource>
        </div>
        <div id="tabs-4">
          <div class="warning">
             NO DEBEN COMPLETARSE LOS DATOS DE ESTA SOLAPA
           </div>

        <h3>DEVELOPMENT ACTIONS FOR THE NEXT FISCAL YEAR</h3>

         
            <div class="separador">
                
              <asp:Repeater ID="RepeaterDEVELOPMENTACTIONS" runat="server" 
                    DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS" 
                    onitemdatabound="RepeaterDEVELOPMENTACTIONS_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterDEVELOPMENTACTIONS">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                        <asp:DropDownList ID="DropDownListRating" runat="server"  Visible="false">
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>

                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                            </td>

                            </tr>
                            <tr>
                            <td>                      

                                <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>'  ReadOnly="true"
                                TextMode="MultiLine" Rows="5"  SkinID="form-textarea"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentario" runat="server" ErrorMessage="DEVELOPMENT ACTIONS FOR THE NEXT FISCAL YEAR: No completó el campo Development Actions" ValidationGroup="EnviarEvaluador" Enabled="false"
                                 ControlToValidate="TextBoxComentario" Display="None" >*</asp:RequiredFieldValidator>
                                 <asp:Label ID="Label2" runat="server" >*</asp:Label>  






                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceDEVELOPMENTACTIONS" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="9" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

            
            </div>
            
            
            <div class="separador">

   
                <div style=" border: #558dc6 1px solid; padding:5px; margin:10px;margin-left:0;">
                <strong> Agregue al menos un curso que considere necesario. Para ello deberá posicionarse en el listado izquierdo, seleccionar el mismo y presionar el botón ">>". Para eliminarlo deberá posicionarse en el listado derecho, seleccionar el mismo y presionar el botón "<<".  
                </strong>
                </div>

                <div class="izq_der_form tiene_medio">
                    <div class="izq">
                        <asp:ListBox ID="ListBoxCursosDisponibles" runat="server" Width="100%" Rows="10"  Height="200px" SelectionMode="Single"  Enabled="false" >
                        </asp:ListBox>
                    </div>

                    <div class="med">
                        <asp:Button ID="Button1" runat="server" Text=">>" ToolTip="Agregar curso" OnClientClick="addAttribute(); return false;"  Enabled="false" />
                        <asp:Button ID="Button2" runat="server" Text="<<" ToolTip="Quitar curso" OnClientClick="delAttribute();return false;"   Enabled="false" />
                    </div>
            
                    <div class="der">
                        <asp:ListBox ID="ListBoxCursosSeleccionados" runat="server" Width="100%" Rows="10" SelectionMode="Single" Height="200px"></asp:ListBox>
                        <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                    </div>
                    
                 </div>
                 <br />
                <div id="mensajeAdd">
                </div>

                 <asp:CustomValidator ID="CustomValidatorCursosSeleccionados" runat="server" ErrorMessage="DEVELOPMENT ACTIONS FOR THE NEXT FISCAL YEAR: Debe seleccionar al menos un curso" Enabled="false"
                    ClientValidationFunction="ValidarCursosSeleccionados" ValidationGroup="EnviarEvaluador" Display="None" >*</asp:CustomValidator>




            </div>
        </div>

        <div id="tabs-5">
        <h3>PERFORMANCE REVIEW - SUMMARY </h3>

        <div class="margin20-0">
            <div>
                Performance on Objectives (Wt. = 60%) 
            </div>
            <div>
                Performance on Success Factors (Wt. = 40%)
            </div>
        </div>
               <asp:ObjectDataSource ID="ObjectDataSourceCalificacionesSummary" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCalificaciones" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                 <SelectParameters>
                     <asp:Parameter DefaultValue="I" Name="Idioma" Type="String" />
                     <asp:Parameter DefaultValue="R" Name="modulo" Type="String" />
                 </SelectParameters>
         </asp:ObjectDataSource>
         <asp:Repeater ID="RepeaterPerformanceReviewSummary" runat="server" 
                DataSourceID="ObjectDataSourcePerformanceReviewSummary" 
                onitemdatabound="RepeaterPerformanceReviewSummary_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterPerformanceReviewSummary">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <div class="separador">
                               <b><asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label></b>
                               <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                               <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCalificacionesSummary"  SelectedValue='<%# Eval("Ponderacion") %>'
                                  
                                     DataTextField="Descripcion" DataValueField="Calificacion" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0%>' >
                                     <asp:ListItem Value="0">Choose an option</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage="PERFORMANCE REVIEW - SUMMARY: No completó el campo overall rating" 
                                 Enabled='<%# (Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0)&&(HiddenFieldPasoID.Value=="1") %>' 
                                 ValidationGroup="EnviarEvaluador"
                                   Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None">*</asp:CompareValidator>

                                <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'
                                TextMode="MultiLine" Rows="8"  SkinID="form-textarea" Enabled='<%# (Eval("Titulo").ToString().ToUpper().IndexOf("SUPERVISOR")>=0 && HiddenFieldPasoID.Value=="1")|| (Eval("Titulo").ToString().ToUpper().IndexOf("APPRAISEE")>=0 && HiddenFieldPasoID.Value=="2")%>'></asp:TextBox>

                                <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server"  Display="None"
                                ErrorMessage='<%# String.Format("PERFORMANCE REVIEW - SUMMARY: No completó el campo {0}",Eval("Titulo")) %>' 
                                ValidationGroup="EnviarEvaluador" Enabled='<%# (Eval("Titulo").ToString().ToUpper().IndexOf("SUPERVISOR")>=0) && (HiddenFieldPasoID.Value=="1") %>'>*</asp:RequiredFieldValidator>
                       
                                <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator1" runat="server"   Display="None"
                                ErrorMessage='<%# String.Format("PERFORMANCE REVIEW - SUMMARY: No completó el campo {0}",Eval("Titulo")) %>' 
                                ValidationGroup="EnviarEvaluador" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("APPRAISEE")>=0 %>'
                                >*</asp:RequiredFieldValidator>

                                <asp:Label ID="Label2" runat="server" >*</asp:Label>
                            </div>
                        </td>
                    </tr>                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePerformanceReviewSummary" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="10" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>


        </div>
        <div id="tabs-6">
        <h3>EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN </h3>
        
        


       <div id="SectionB_LastPerformanceRating" class="separador"><b>SECTION B: LAST PERFORMANCE RATINGS (Completed by Employee).</b>
        
          


       <div class="margin10-0"> Enter last 3 years of performance ratings:</div>
        <asp:Repeater ID="RepeaterSeccionBPerformanceRatings" runat="server" DataSourceID="ObjectDataSourcePerformanceRatings">
            <HeaderTemplate>
                <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterSeccionBPerformanceRatings">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                <td>
                Rating 
                <asp:Label ID="Label1" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>'></asp:Label>
                :
                </td>
                    <td>
                        <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label>

                    </td>
                    <td>
                       <asp:Label ID="LabelCalificacion" runat="server" Text='<%# DefaultVal(Eval("Calificacion")) %>'></asp:Label>  </td>
             
                </tr>
                   
            </ItemTemplate>
            <FooterTemplate>
                </table></FooterTemplate>
        </asp:Repeater>
        </div>
        <div class="separador">
        <b>EDUCATION</b>
        
        



         <asp:Repeater ID="DataListEducation" runat="server"  
                DataSourceID="ObjectDataSourceEducation" 
                onitemdatabound="DataListEducation_ItemDataBound" >
         <HeaderTemplate>
          <table id="DataListEducation">
           <tr> 
             <td>Degree</td>
             <td class="educa_year">Year</td>
              <td class="educa_school">School</td>
            </tr>
          </HeaderTemplate>
         <ItemTemplate>
         <tr>
         <td>
             <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%#(((RepeaterItem)Container).ItemIndex+1) %>' />
             <asp:HiddenField ID="HiddenFieldDegree" runat="server"  Value='<%# Eval("Degree") %>'/>
             <asp:DropDownList ID="DropDownListDegree" runat="server" >
             <asp:ListItem Value="" Text="Please Select"></asp:ListItem>
             <asp:ListItem Value="School studies" Text="School studies"></asp:ListItem>
             <asp:ListItem Value="Graduated studies" Text="Graduated studies"></asp:ListItem>
             <asp:ListItem Value="Post graduated studies" Text="post Graduated studies"></asp:ListItem>             
             </asp:DropDownList>
          </td>
         <td><asp:TextBox ID="TextBoxYear" runat="server" MaxLength="4"></asp:TextBox>
              <asp:HiddenField ID="HiddenFieldYear" runat="server"  Value='<%# Eval("Year") %>'/>
             <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderTextBoxYear" runat="server" TargetControlID="TextBoxYear" ValidChars="1234567890" />    
                 </td>
         <td><asp:TextBox ID="TextBoxSchool" runat="server" Text='<%# Bind("School") %>' 
                 MaxLength="70"></asp:TextBox>
         <asp:CustomValidator ID="CustomValidatorEducation" runat="server" ErrorMessage= '<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN: No completó todos los campos en education {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' 
          ClientValidationFunction="ValidarEducacion" ValidationGroup="EnviarEvaluador"
          >*</asp:CustomValidator>                 
                 
                 </td>
         </tr>

             
         
         </ItemTemplate>
         <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>

         
        


        <asp:ObjectDataSource ID="ObjectDataSourceEducation" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetEducationByTramiteID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
         </div>
         <div id="SectionC_MobilityAndTravel" class="separador">
        <b>SECTION C: MOBILITY & TRAVEL (Completed by Employee) </b>
        
        



         <asp:Repeater ID="RepeaterMobilityAndTravel" runat="server" 
                DataSourceID="ObjectDataSourceMobilityAndTravel" 
                onitemdatabound="RepeaterMobilityAndTravel_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterMobilityAndTravel">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:HiddenField ID="HiddenFieldPonderacion" runat="server" Value='<%# Eval("Ponderacion") %>' />

                        </td>

                         <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()!= "187" && Eval("ItemEvaluacionID").ToString()!= "392" %>'  >
                                 <asp:ListItem Value="0">Please Select</asp:ListItem>                                 
                         </asp:DropDownList>
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392" %>' MaxLength="45"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN: SECTION C: No completó el campo {0}",Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador" Enabled='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392"%>' Display="None">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN: SECTION C: No completó el campo {0}",Eval("Titulo")) %>' Enabled='<%# Eval("ItemEvaluacionID").ToString()!= "187" && Eval("ItemEvaluacionID").ToString()!= "392" %>' ValidationGroup="EnviarEvaluador" Display="None"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating">*</asp:CompareValidator>
                              
                         *       </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceMobilityAndTravel" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="20" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div id="SectionD_LanguageProficiency" class="separador">
        <b>SECTION D: LANGUAGE PROFICIENCY (Completed by Employee)</b>
        
        


                 <asp:Repeater ID="RepeaterLanguage" runat="server" 
                DataSourceID="ObjectDataSourceLanguage" 
                onitemdatabound="RepeaterLanguage_ItemDataBound" >
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterLanguage">
                    
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>    
                         <asp:Label runat="server" ID="LabelNumber" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>'></asp:Label>   
                         <asp:HiddenField ID="HiddenFieldNumber" Value='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' runat="server" />                    
                        </td>
                       

                        
                         <td>
                         <asp:TextBox ID="TextBoxIdioma" runat="server" Text='<%# Eval("Idioma") %>' MaxLength="25"></asp:TextBox>
                         <asp:Label ID="LabelAsterisco" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>' >*</asp:Label>
                         </td>
                         <td>                        
                         <asp:CheckBox ID="CheckBoxNative" runat="server"  Text="Native:" Checked='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>' Enabled="false" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>'  TextAlign="Left"/>
                         
                        
                         </td>
                         <td>
                         <label>Proficiency:</label>
                         </td>
                         <td>
                         <asp:DropDownList ID="DropDownListProficiency" runat="server" 
                SelectedValue='<%# Eval("Proficiency") %>'>
                                 <asp:ListItem Value="">Please Select</asp:ListItem>
<%--                                 <asp:ListItem Value="1">High</asp:ListItem>
                                 <asp:ListItem Value="2">Medium</asp:ListItem>
                                 <asp:ListItem Value="3">Low</asp:ListItem> --%>       
                                 <asp:ListItem Value="4">Conversational</asp:ListItem>
                                 <asp:ListItem Value="5">Business Fluent</asp:ListItem>                        
                         </asp:DropDownList>
                         <asp:Label ID="Label2" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>' >*</asp:Label>
         <asp:CustomValidator ID="CustomValidatorLanguage" runat="server" ErrorMessage= '<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN SECTION D: No completó todos los campos en Language proficiency  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' 
          ClientValidationFunction="ValidarLanguage" ValidationGroup="EnviarEvaluador" Display="None"
          >*</asp:CustomValidator> 
                         </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceLanguage" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetLanguageByTramiteID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        
        


        </div>
        <div id="SectionE_CareerInterests" class="separador">
        <b>SECTION E: CAREER INTERESTS (Completed by Employee)  </b>
          
          



       <asp:Repeater ID="RepeaterCareerInterests" runat="server" DataSourceID="ObjectDataSourceCareerInterests">
        <HeaderTemplate>
            <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterCareerInterests">
            <tr>
            <td colspan="2">List jobs in order of greatest interest to you. Link for list of current job titles <a href="documents\JobCodes_Full_List.xls" target="_blank">click here<a/></td>
            <td> ADDITIONAL COMMENTS (IF NEEDED) (45 character limit):</td>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                </td>
                <td>
                  
                    <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCareers"  
                               SelectedValue='<%# Eval("Ponderacion") %>'
                            DataTextField="Descripcion" DataValueField="IDGenerado" AppendDataBoundItems="true" >
                            <asp:ListItem Value="0">Choose an option</asp:ListItem>
                     </asp:DropDownList>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                     <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                         <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage= '<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, SECTION E: No completó el campo Career interest  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' 
                           ValidationGroup="EnviarEvaluador" Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None">*</asp:CompareValidator>
                        
                <td><asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="45"></asp:TextBox>
                <%--<asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage="*" ValidationGroup="EnviarEvaluado" >*</asp:RequiredFieldValidator>--%>
                        
                        </td>
            </tr>
                   
        </ItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>
         <asp:CustomValidator ID="CustomValidatorCareerInterests" runat="server" ErrorMessage= "EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, SECTION E: No debe duplicar las Career interest" 
                  ClientValidationFunction="ValidarCareerInterests" ValidationGroup="EnviarEvaluador">*</asp:CustomValidator> 
       <asp:ObjectDataSource ID="ObjectDataSourceCareerInterests" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="21" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        
        


            <asp:ObjectDataSource ID="ObjectDataSourceCareers" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetCareerActivas" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
            </asp:ObjectDataSource>
        
        


        </div>
        <div runat="server" id="SectionF_NextAssignment" class="separador">
        <b>SECTION F: NEXT ASSIGNMENT (Completed by Manager) </b>
         
        



       <asp:Repeater ID="RepeaterNextAssigment" runat="server" DataSourceID="ObjectDataSourceNextAssigment">
        <HeaderTemplate>
            <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterNextAssigment">
            <tr>
            <td colspan="2">Prioritize 1-3 job(s) as logical next assignment(s). For a list of SCJ job titles <a href="documents\JobCodes_Full_List.xls" target="_blank">click here<a/></td>
            <td> ADDITIONAL COMMENTS (IF NEEDED) (45 character limit):</td>
            </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                </td>
                <td>
                   <%-- <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceAreas"  SelectedValue='<%# Eval("Ponderacion") %>'
                            DataTextField="DescAreas" DataValueField="ID" AppendDataBoundItems="true" >
                            <asp:ListItem Value="0">Choose an option</asp:ListItem>
                    </asp:DropDownList>--%>

                    <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceCareers"  
                               SelectedValue='<%# Eval("Ponderacion") %>'
                            DataTextField="Descripcion" DataValueField="IDGenerado" AppendDataBoundItems="true" >
                            <asp:ListItem Value="0">Choose an option</asp:ListItem>
                     </asp:DropDownList>

                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                   <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                     <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section F: No Completó el campo {0}",Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Enabled='<%# (HiddenFieldPasoID.Value=="1") %>' Display="None">*</asp:CompareValidator>              
               </td>
                <td><asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="45"></asp:TextBox>
               <%-- <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%= String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section F: No Completó el campo {0}",Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador"
                Enabled='<%# HiddenFieldPasoID.Value=="1" %>' >*</asp:RequiredFieldValidator>--%>
                       
                        </td>
            </tr>
                   
        </ItemTemplate>
        <FooterTemplate>
            </table></FooterTemplate>
    </asp:Repeater>

             <asp:CustomValidator ID="CustomValidatorNextAssigment" runat="server" ErrorMessage= "EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, SECTION F: No debe duplicar las NextAssigment" 
                  ClientValidationFunction="ValidarNextAssigment" ValidationGroup="EnviarEvaluador">*</asp:CustomValidator> 
       <asp:ObjectDataSource ID="ObjectDataSourceNextAssigment" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="22" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
           
        
           


         </div>  
         <div runat="server" id="SectionG_DevelopmentSummary" class="separador">
        <b>SECTION G: DEVELOPMENT SUMMARY (Completed by Manager) </b>
        
         


        1-3 Strengths/Development Needs from PMP form per the <a href="documents/SCJCompetencyList2012_2013.xls" target="_blank">SCJ Competency List</a> (Note: "Other" may be selected once). SCJ Skills List 
          <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ObjectDataSourceSTRENGTHS">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">                    
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceFortalezas"  SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="Descripcion" DataValueField="codigo" AppendDataBoundItems="true" Enabled="false">
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>

                            <%--<asp:Label ID="LabelSkill" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>'></asp:Label>--%>

                        </td>
                        <td><asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label> 
                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
        Development Needs 
         <asp:Repeater ID="Repeater2" runat="server" DataSourceID="ObjectDataSourceDevelopmentNeeds">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>
                        <td >
                            <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceFortalezas"  SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="Descripcion" DataValueField="codigo" AppendDataBoundItems="true" Enabled="false">
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                           
                        </td>
                        <td >
                        <asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label>
                        
                            
                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>

 
 
        
         


        </div>   
        <div runat="server" id="SectionH_DevelopmentActions" class="separador">            
        <b>SECTION H: DEVELOPMENT ACTIONS (Completed by Employee)  </b>
        
              


        
        <asp:Repeater ID="RepeaterSectionH" runat="server" DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS">
                <HeaderTemplate>
                    <table id="RepeaterSectionH" cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                            <tr>
                            <td> 
                            <asp:Label runat="server" ID="LabelSectionH" Text='<%# Bind("Fundamentacion") %>' ></asp:Label>                   
                             </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
       
        
        


         </div>
         <div runat="server" id="SectionI_PotencialReplacements" class="separador">
        <b>SECTION I: POTENTIAL REPLACEMENTS (Completed by Manager) </b>
         <asp:Repeater ID="RepeaterPotencialReplacements1" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements1">
                <HeaderTemplate>
                    <div class="separador">
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterPotencialReplacements1">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>

                         <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server"  Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 %>'   SelectedValue='<%# Eval("Ponderacion") %>'>
                                 <asp:ListItem Value="0">Please Select</asp:ListItem>
                                 <asp:ListItem Value="1">Now</asp:ListItem>
                                 <asp:ListItem Value="2">1 year</asp:ListItem>
                                 <asp:ListItem Value="3">2 Years</asp:ListItem>
                                 <%--<asp:ListItem Value="4">3 years</asp:ListItem>--%>
                                 <asp:ListItem Value="6">More than 2 years</asp:ListItem>
                            </asp:DropDownList>                     
                            <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 && HiddenFieldPasoID.Value=="1" %>'>*</asp:CompareValidator>              

                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0 %>' MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador"
                Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0 && HiddenFieldPasoID.Value=="1" %>' Display="None">*</asp:RequiredFieldValidator>                              
                               <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                                </td>

                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    </div>
                    </FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="34" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

         <asp:Repeater ID="RepeaterPotencialReplacements2" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements2">
                <HeaderTemplate>
                    <div class="separador">
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterPotencialReplacements2">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>

                         <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server"  Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 %>'  SelectedValue='<%# Eval("Ponderacion") %>'>
                                 <asp:ListItem Value="0">Please Select</asp:ListItem>
                                 <asp:ListItem Value="1">Now</asp:ListItem>
                                 <asp:ListItem Value="2">1 year</asp:ListItem>
                                 <asp:ListItem Value="3">2 Years</asp:ListItem>
                                 <%--<asp:ListItem Value="4">3 years</asp:ListItem>--%>
                                 <asp:ListItem Value="6">More than 2 years</asp:ListItem>
                            </asp:DropDownList>
                     <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 && HiddenFieldPasoID.Value=="1" %>'>*</asp:CompareValidator>    

                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0  %>' MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador"
                Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0 && HiddenFieldPasoID.Value=="1" %>' Display="None" >*</asp:RequiredFieldValidator>                              
                             <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></div></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="35" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

         <asp:Repeater ID="RepeaterPotencialReplacements3" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements3">
                <HeaderTemplate>
                    <div class="separador sinFoot">
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterPotencialReplacements3">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                           <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                        </td>

                         <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server"  Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 %>'  SelectedValue='<%# Eval("Ponderacion") %>'>
                                 <asp:ListItem Value="0">Please Select</asp:ListItem>
                                 <asp:ListItem Value="1">Now</asp:ListItem>
                                 <asp:ListItem Value="2">1 year</asp:ListItem>
                                 <asp:ListItem Value="3">2 Years</asp:ListItem>
                                 <%--<asp:ListItem Value="4">3 years</asp:ListItem>--%>
                                 <asp:ListItem Value="6">More than 2 years</asp:ListItem>
                            </asp:DropDownList>
                     <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")<0 && HiddenFieldPasoID.Value=="1" %>'>*</asp:CompareValidator>    
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0 %>' MaxLength="25"></asp:TextBox>
                <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN, Section I: No Completó el campo {0}",Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador"
                Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("NAME")>=0 && HiddenFieldPasoID.Value=="1" %>' Display="None" >*</asp:RequiredFieldValidator> 
                            <asp:Label ID="Label2" runat="server" >*</asp:Label>  
                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></div></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="36" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        
        


        </div>
        </div>
        <div id="tabs-7">
        <h3>RESUME</h3>
        <label>Experience</label>
            <div class="separador">
            That section should enter only the last three jobs.
             <asp:Repeater ID="DataListExperience" runat="server" 
                    DataSourceID="ObjectDataSourceExperience" 
                    onitemdatabound="DataListExperience_ItemDataBound">
             <HeaderTemplate><table id="DataListExperience"></HeaderTemplate>
             <ItemTemplate>
             <tr><td class="exp_block">
             <table width="100%">
             <tr><td>Period </td>
                 <td class="periodo">
                 <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%#(((RepeaterItem)Container).ItemIndex+1) %>' />
                 <label>From:</label>
                 <asp:TextBox ID="TextBoxFromDate" runat="server" Text='<%# Bind("FromDate") %>' MaxLength="10"  Width="80px"></asp:TextBox>
                 <asp:Label ID="LabelAsterisco" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" && HiddenFieldTipoFormularioID.Value=="11" %>' >*</asp:Label>
                      <asp:CompareValidator ID="CompareValidatorTextBoxFromDate" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="TextBoxFromDate"  ValidationGroup="EnviarEvaluador"
                       ErrorMessage= '<%# String.Format("RESUME: Ingrese una fecha valida en Experience  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' Display="None" >*</asp:CompareValidator>
                    <asp:ImageButton ID="ImageButtonFromDate" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>
                    <asp:MaskedEditExtender runat="server" ID="MaskedEditExtenderTextBoxFromDate"  TargetControlID="TextBoxFromDate"  Mask="99/99/9999" MaskType="Date" />               
                    <asp:CalendarExtender ID="CalendarExtender_TextBoxFromDate" runat="server" Enabled="True"
                        TargetControlID="TextBoxFromDate" PopupButtonID="ImageButtonFromDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                    
                        </td>
                  <td>
                  <label>To:</label>
                  <asp:TextBox ID="TextBoxToDate" runat="server" Text='<%# Bind("ToDate") %>' MaxLength="10" Width="80px"></asp:TextBox>
                  <asp:Label ID="Label3" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" && HiddenFieldTipoFormularioID.Value=="11" %>' >*</asp:Label>
                      <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="TextBoxToDate"  ValidationGroup="EnviarEvaluador"
                       ErrorMessage= '<%# String.Format("RESUME: Ingrese una fecha valida en Experience  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' Display="None" >*</asp:CompareValidator>

                      <asp:CompareValidator ID="CompareValidator2" runat="server" Type="Date" Operator="LessThan" ValidationGroup="EnviarEvaluador" Display="None"
                       ErrorMessage= '<%# String.Format("RESUME: Fecha desde debe ser menor a fecha hasta en Experience  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' ControlToValidate="TextBoxFromDate" ControlToCompare="TextBoxToDate" >*</asp:CompareValidator>

                    <asp:ImageButton ID="ImageButtonToDate" runat="server" ImageUrl="~/images/Calendar.png"></asp:ImageButton>               
                    <asp:CalendarExtender ID="CalendarExtender_ToDate" runat="server" Enabled="True" Format="dd/MM/yyyy"
                        TargetControlID="TextBoxToDate" PopupButtonID="ImageButtonToDate"></asp:CalendarExtender>
                    <asp:MaskedEditExtender runat="server" ID="MaskedEditExtender1"  TargetControlID="TextBoxToDate"  Mask="99/99/9999" MaskType="Date" /> 
                              


                        </td>
                    
                        </tr>
             
             <tr><td>Company</td><td colspan="2"><asp:TextBox ID="TextBoxCompany" runat="server" Text='<%# Bind("Company") %>' MaxLength="50" SkinID="form-textarea"></asp:TextBox>
             <asp:Label ID="Label4" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>' >*</asp:Label></td></tr>
             <tr><td>Position</td><td colspan="2"><asp:TextBox ID="TextBoxPosition" runat="server" Text='<%# Bind("Position") %>' MaxLength="50" SkinID="form-textarea"></asp:TextBox>
             <asp:Label ID="Label5" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>' >*</asp:Label></td></tr>
             <tr><td>Job Description </td><td colspan="2"><asp:TextBox ID="TextBoxDescription" runat="server" Text='<%# Bind("Description") %>' SkinID="form-textarea" TextMode="MultiLine" Rows="6"></asp:TextBox>
             <asp:Label ID="Label6" runat="server" Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1"  %>' >*</asp:Label>
              <asp:CustomValidator ID="CustomValidatorExperience" runat="server" ErrorMessage= '<%# String.Format("RESUME: No completó todos los campos en Experience  {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>' 
              ClientValidationFunction="ValidarExperience" ValidationGroup="EnviarEvaluador"
              >*</asp:CustomValidator> 
              <asp:CustomValidator ID="CustomValidatorJobDescription" runat="server" ErrorMessage="RESUME:Debe completar al menos 100 caracteres en el campo  Job Description 1" 
              ClientValidationFunction="ValidarJobDescription" ValidationGroup="EnviarEvaluador"
              >*</asp:CustomValidator> 
             </td>
         
             </tr>
             </table>
             </td>
             </tr>
             </ItemTemplate>
             <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>

            <asp:ObjectDataSource ID="ObjectDataSourceExperience" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetExperienceByTramiteID" 
                    TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
             <div class="separador">
                 <asp:Repeater ID="RepeaterResume" runat="server" DataSourceID="ObjectDataSourceResume">
                        <HeaderTemplate>
                            <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterResume">
                        </HeaderTemplate>
                        <ItemTemplate>
                        <tr>
                        <td>
                        <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                        
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownListRating" runat="server"  Visible="false">
                                        <asp:ListItem Value="0">Choose an option</asp:ListItem>
                                    </asp:DropDownList>
                                    <h4><asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label></h4>
                                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>                      
                                    <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' 
                                    TextMode="MultiLine" Rows="5"  SkinID="form-textarea"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxComentario" runat="server" ErrorMessage='<%# String.Format("RESUME: No completó el campo {0}" ,Eval("Titulo")) %>'
                                    ValidationGroup="EnviarEvaluador" ControlToValidate="TextBoxComentario" Enabled='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OTHER")<0 %>' Display="None">*</asp:RequiredFieldValidator>
                                    <asp:Label ID="LabelAsterisco" runat="server" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OTHER")<0 %>' >*</asp:Label>
                                </td>
                            </tr>

                            </table>
                        </td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                 </asp:Repeater>
                 <asp:ObjectDataSource ID="ObjectDataSourceResume" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="13" Name="SeccionID" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                                PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                                Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
        <asp:Panel ID="tab8x" runat="server">
        
        <div id="tabs-8">
        <h3>CAREER PLAN (Employee and Supervisor)</h3>
        
        <div class="separador">
        <h4>Long Term</h4>
        <div class="margin20-0">3-5 Year "End in Mind". List 1-3 types of jobs and organizational levels that represent the career objective:</div>

       

         <asp:Repeater ID="RepeaterCareerPlanLongTerm" runat="server" 
                DataSourceID="ObjectDataSourceCareerPlanLongTerm" 
                onitemdatabound="RepeaterCareerPlanLongTerm_ItemDataBound">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterCareerPlanLongTerm">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='<%# Eval("Titulo") %>' />

                            </td>
                            <td>
                         <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceAreas"  SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="DescAreas" DataValueField="ID" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("PLEASE SELECT")>=0 %>' >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                         <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("CAREER PLAN, Long Term: No completó el campo  {0}", Eval("Titulo")) %>'  Enabled='<%# Eval("ItemEvaluacionID").ToString()== "144"  %>' ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating" Display="None" >*</asp:CompareValidator>

                        <asp:TextBox ID="TextBoxOther" runat="server" Text='<%# Bind("Fundamentacion") %>'
                        TextMode="SingleLine" SkinID="form-textarea" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="CustomValidatorCareerPlanOther" runat="server" ErrorMessage= "CAREER PLAN, Long Term: No completó el campo Other" 
                             ClientValidationFunction="ValidarCareerPlanOther" ValidationGroup="EnviarEvaluador" Display="None">*</asp:CustomValidator>

                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("PLEASE SELECT")<0%>'
                        TextMode="MultiLine" Rows="5"  SkinID="form-textarea"                        
                        ></asp:TextBox>

                        <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("CAREER PLAN, Long Term: No completó el campo  {0}", Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"  
                        Enabled='<%# (Eval("ItemEvaluacionID").ToString()== "149") && (HiddenFieldPasoID.Value=="1") %>' Display="None">*</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator1" runat="server"  ErrorMessage='<%# String.Format("CAREER PLAN, Long Term: No completó el campo  {0}", Eval("Titulo")) %>'  ValidationGroup="EnviarEvaluador"  
                        Enabled='<%# Eval("ItemEvaluacionID").ToString()== "146"  %>' Display="None">*</asp:RequiredFieldValidator>
                          <asp:Label ID="Label2" runat="server" Visible='<%# HiddenFieldTipoFormularioID.Value=="10" %>' >*</asp:Label>
                        </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanLongTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="17" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
      
        </div>
        <div class="separador">
        <h4>Near Term</h4>
         <div class="margin20-0">What is (are) the most logical assignment(s) in the near-term (0-2) years out?</div>
         <asp:Repeater ID="RepeaterCareerPlanNearTerm" runat="server" DataSourceID="ObjectDataSourceCareerPlanNearTerm"
onitemdatabound="RepeaterCareerPlanLongTerm_ItemDataBound">
               
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterCareerPlanNearTerm">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                           <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:HiddenField ID="HiddenFieldTitulo" runat="server" Value='<%# Eval("Titulo") %>' />

                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListRating" runat="server" DataSourceID="ObjectDataSourceAreas"  SelectedValue='<%# Eval("Ponderacion") %>'
                                 DataTextField="DescAreas" DataValueField="ID" AppendDataBoundItems="true" Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("PLEASE SELECT")>=0 %>'>
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListSearchExtender id="ListSearchExtender1" runat="server"
                                                    TargetControlID="DropDownListRating"
                                                    PromptCssClass="ListSearchExtenderPrompt"
                                                    PromptText="Escriba para buscar"/> 
                         <asp:CompareValidator ID="CompareValidatorDropDownListRating" runat="server" ErrorMessage='<%# String.Format("CAREER PLAN, Near Term: No completó el campo  {0}", Eval("Titulo")) %>' Enabled='<%# Eval("ItemEvaluacionID").ToString()== "145" %>' ValidationGroup="EnviarEvaluador"
                           Operator="NotEqual" ValueToCompare="0" ControlToValidate="DropDownListRating"  Display="None">*</asp:CompareValidator>
                        <asp:TextBox ID="TextBoxOther" runat="server" Text='<%# Bind("Fundamentacion") %>'
                        TextMode="SingleLine" SkinID="form-textarea" MaxLength="50"></asp:TextBox>
                        <asp:CustomValidator ID="CustomValidatorCareerPlanOther" runat="server" ErrorMessage= "CAREER PLAN, Near Term: No completó el campo Other" 
                             ClientValidationFunction="ValidarCareerPlanOther" ValidationGroup="EnviarEvaluador" Display="None">*</asp:CustomValidator> 
                        <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%#  Eval("Titulo").ToString().ToUpper().IndexOf("PLEASE SELECT")<0 %>'
                        TextMode="MultiLine" Rows="5"  SkinID="form-textarea"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server" ErrorMessage='<%# String.Format("CAREER PLAN, Near Term: No completó el campo  {0}", Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador" Enabled='<%# (Eval("ItemEvaluacionID").ToString()== "148")&& (HiddenFieldPasoID.Value=="1") %>' Display="None">*</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%# String.Format("CAREER PLAN, Near Term: No completó el campo  {0}", Eval("Titulo")) %>' ValidationGroup="EnviarEvaluador" Enabled='<%# Eval("ItemEvaluacionID").ToString()== "147" %>' Display="None">*</asp:RequiredFieldValidator>
                        <asp:Label ID="Label2" runat="server" Visible='<%# HiddenFieldTipoFormularioID.Value=="10" %>' >*</asp:Label>    
                                </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanNearTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="18" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            
        </div>
           
       <asp:ObjectDataSource ID="ObjectDataSourceAreas" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetAreasActivas" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
         </asp:ObjectDataSource>
        </div>
        
        <div id="tabs-9">

        <h3>FEEDBACK</h3>

        <div class="margin10-0">List the top three rated key success in last year</div>
        <div class="separador">
        <asp:Repeater ID="RepeaterFeedback" runat="server" DataSourceID="ObjectDataSourceFeedback">
            <HeaderTemplate>
                <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterFeedback">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td width="20%">
                        <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                        <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                    </td>
                    <td><asp:DropDownList ID="DropDownListRating" runat="server" AppendDataBoundItems="true" Visible="false" >
                                 <asp:ListItem Value="0">Choose an option</asp:ListItem>
                        </asp:DropDownList>

                    <asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="70" SkinID="form-textarea"
                     ></asp:TextBox>
                     *
                      <asp:RequiredFieldValidator ControlToValidate="TextBoxComentario" ID="RequiredFieldValidator11" runat="server"  Display="none"
                      ErrorMessage='<%# String.Format("FEEDBACK: No completó el campo Success {0}", (((RepeaterItem)Container).ItemIndex+1).ToString()) %>'  ValidationGroup="EnviarEvaluador">*</asp:RequiredFieldValidator>
                   

                            
                            </td>
                </tr>
                   
            </ItemTemplate>
            <FooterTemplate>
                </table></FooterTemplate>
        </asp:Repeater>
        <asp:ObjectDataSource ID="ObjectDataSourceFeedback" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="12" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div class="separador">

            <h4>List performance ratings for last three years:</h4>

            <asp:Repeater ID="RepeaterPerformanceRatings" runat="server" DataSourceID="ObjectDataSourcePerformanceRatings">
                <HeaderTemplate>
                    <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterPerformanceRatings">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td>
                    Rating 
                    <asp:Label ID="Label1" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>'></asp:Label>
                    :
                    </td>
                        <td>
                            <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label>

                        </td>
                        <td>
                           <asp:Label ID="LabelCalificacion" runat="server" Text='<%# DefaultVal(Eval("Calificacion")) %>'></asp:Label>  </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>

            <asp:ObjectDataSource ID="ObjectDataSourcePerformanceRatings" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetLastRatings" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="periodoActual" 
                        PropertyName="Value" Type="Int32" />
                        <asp:Parameter DefaultValue="3" Type="Int32" Name="TipoPeriodoID" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </div>
        </div>
        </asp:Panel>
    </div>

    <div id="acciones">
        <asp:UpdatePanel ID="upPnlPage" runat="server">
            <ContentTemplate>
                <span class="button">
                    <asp:Button ValidationGroup="EnviarEvaluador" ID="ButtonEnviarEvaluador" runat="server"
                        Text="Enviar al Evaluador" EnableTheming="false"  
                    CssClass="formSubmitAsync" OnClick="ButtonEnviarEvaluador_Click"                        
                    ToolTip="Guarda la Evaluación dejando la misma disponible para el Evaluador. El Evaluado no puede realizar más cambios" 
                    onclientclick="flagCierre = 3;" /></span>
                <span class="button">
                    <asp:Button CssClass="formSubmitAsync" ID="ButtonDevolverEvaluado" runat="server"
                        Text="Devolver al Evaluado" OnClick="ButtonDevolverEvaluado_Click" ToolTip="Guarda la Evaluación y reenvía la misma al Evaluado para reiniciar el proceso de evaluación."
                        EnableTheming="false"  onclientclick="flagCierre = 3;  if(!confirm('Usted está por devolver la evaluación a su evaluado. Desea confirmar?')) return false;" />  
                </span><span class="button">
                    <asp:Button CssClass="formSubmitAsync" ID="ButtonAprobarEvaluacion" runat="server"  
                        OnClick="ButtonAprobarEvaluacion_Click" Text="Aprobar Evaluación" ToolTip="Guarda la Evaluación finalizando la misma"                        
                        EnableTheming="false" ValidationGroup="EnviarEvaluador" OnClientClick="flagCierre = 3; if(!confirm('Usted está por aprobar la evaluación. Desea confirmar?')) return false; "/>
                </span>
                <span class="button">
                    <asp:Button ID="ButtonGSinEnviar" runat="server" Text="Guardar sin enviar" OnClick="ButtonGSinEnviar_Click"
                        EnableTheming="false" CssClass="formSubmitAsync" ToolTip="Guarda la evaluación sin enviar la misma al Evaluador"  />
                        
                        </span>
                

            </ContentTemplate>
        </asp:UpdatePanel>
        <div style=" float: right; width:50%">
        <span class="button" >
                    <asp:Button ID="Imprimir" runat="server" Text="Imprimir/Exportar" ToolTip="Visualiza el formulario en modo de Impresión con los datos volcados en la base de datos"
                        CssClass="formSubmitAsync" EnableTheming="false"  
                OnClientClick="return false;" onclick="Imprimir_Click"/>
                        
                </span>
               

        </div>

        <br /> <br />

    </div>
    <asp:UpdatePanel ID="UpdatePanelExito" runat="server">
        <ContentTemplate>
            <uc1:OKMessageBox ID="omb" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonEnviarEvaluador" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="upPnlPage">
            <ProgressTemplate>
                <div class="precarga">
                    <asp:Image AlternateText="Procesando" ID="ImgProc" ImageUrl="~/pmp/img/ajax-loader.gif"
                        runat="server" />
                    <p>
                        Operación en progreso...</p>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:modalpopupextender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" 
        PopupControlID="panelUpdateProgress" />

    
</asp:Content>
