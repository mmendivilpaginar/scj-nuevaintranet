﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImpresionScrapper.aspx.cs" Inherits="com.paginar.johnson.Web.PMPFY.ImpresionScrapper" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
   <style type="text/css">
       
        .SaltoDePagina{
        page-break-after: always;
        }

        .Simbol10 {
	        font-family: Symbol;
	        font-size: 10pt;
        }
        .Arial12NegritaWhite {
	        font-family: Arial;
	        font-size: 12pt;
	        color: #FFFFFF;
	        font-weight: bold;
        }



        .Arial9 {
	        font-family: Arial;
	        font-size: 9pt;
        }
        .Arial9BlueSubrayado {
        }
        .ArialBlack9Red {
	        font-family: "Arial Black";
	        font-size: 9pt;
	        color: #FF0000;
        }
        .Wingdings {
	        font-family: Wingdings;
	        font-size: 9pt;
        }





        .Arial95NegritaBlue {
	        font-family: Arial;
	        font-size: 9.5pt;
	        color: #0000FF;
	        font-weight: bold;
        }
        .Arial10NegritaMayuscula {
	        font-family: Arial;
	        font-size: 10pt;
	        font-weight: bold;
	        text-transform: uppercase;
        }



        .Arial8Italica {
	        font-family: Arial;
	        font-size: 8pt;
	        font-style: italic;
        }
        .Arial85Negrita {
	        font-family: Arial;
	        font-size: 8.5pt;
	        font-weight: bold;
        }

        .Arial8ItalicaBlue {
	        font-family: Arial;
	        font-size: 8pt;
	        font-style: italic;
	        color: #0000FF;
	        text-decoration: underline;
        }
        .Arial8NegritaMayuscula {
	        font-family: Arial;
	        font-size: 8pt;
	        font-weight: bold;
	        text-transform: uppercase;
        }
        .Arial9Negrita {
	        font-family: Arial;
	        font-size: 9pt;
	        font-weight: bold;
        }
        .Arial8 {
	        font-family: Arial;
	        font-size: 8pt;
        }
        .Arial95NegritaRed {
	        font-family: Arial;
	        font-size: 9.5pt;
	        color: #FF0000;
	        font-weight: bold;
        }
        .Arial9Negrita {
	        font-family: Arial;
	        font-size: 9pt;
	        font-weight: bold;
        }
        .Arial9Italica {
	        font-family: Arial;
	        font-size: 9pt;
	        font-style: italic;
        }
        .Arial10Blue {
	        font-family: Arial;
	        font-size: 10pt;
	        color: #0000FF;
	        font-style: normal;
        }
        .Arial10Red {
	        font-family: Arial;
	        font-size: 10pt;
	        color: #FF0000;
        }
        .ArialBlack9Blue {
	        font-family: "Arial Black";
	        font-size: 9pt;
	        color: #0000FF;
        }
        .Arial9BlueSubrayado {
	        font-family: Arial;
	        font-size: 9pt;
	        color: #0000FF;
	        text-decoration: underline;
        }
        .Arial10ItalicaWhite {
	        font-family: Arial;
	        font-size: 10pt;
	        color: #FFFFFF;
	        font-style: italic;
        }
        .ArialBlack12 {
	        font-family: "Arial Black";
	        font-size: 12pt;
        }
        
        .ArialBlack11 {
	        font-family: "Arial Black";
	        font-size: 11pt;
        }        

        .ArialBlack10 {
	        font-family: "Arial Black";
	        font-size: 10pt;
        } 
.Calibri9 {
	font-family: Arial;
	font-size: 9pt;
}
.Calibri10 {
	font-family: Arial;
	font-size: 10pt;
}
.Calibri9Negrita {
	font-family: Arial;
	font-size: 9pt;
	font-weight:bold;
}

.Calibri95NegritaBlue {
	font-family: Arial;
	font-size: 9.5pt;
	color: #0000FF;
	font-weight: bold;
}

.Calibri8Italica {
	font-family: Arial;
	font-size: 8pt;
	font-style: italic;
}

.Calibri85Negrita {
	font-family: Arial;
	font-size: 8.5pt;
	font-weight: bold;
}

.Calibri85 {
	font-family: Arial;
	font-size: 8.5pt;
	
}
.Calibri8NegritaMayuscula {
	font-family: Arial;
	font-size: 8pt;
	font-weight: bold;
	text-transform: uppercase;
}

.Calibri8Negrita {
	font-family:  Arial;
	font-size: 8pt;
	font-weight: bold;	
}

.Calibri8 {
	font-family:  Arial;
	font-size: 8pt;
}

.Calibri95NegritaRed {
	font-family: Arial;
	font-size: 9.5pt;
	color: #FF0000;
	font-weight: bold;
}

.ArialBlack10 {
     font-family: "Arial Black";
	 font-size: 10pt;
     } 
     
.CalibriNegrita10 {
     font-family: Arial;
	 font-size: 10pt;
	 font-weight: bold;
     } 
	 
	 .Calibri9Italica {
	font-family: Arial;
	font-size: 9pt;
	font-style: italic;
}

.CalibriBlack12 {
	font-family: Arial Black;
	font-size: 12pt;
	letter-spacing:-1;
}

.CalibriNegrita12 {
	font-family: Arial;
	font-weight: bold;
	font-size: 12pt;
}
.CalibriBlack10 {
	font-family:  Arial Black;
	font-size: 10pt;
	
}        
        
        .BORDEiz-ar {
	        border-top-width: 1pt;
	        border-right-width: 0pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: none;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        
        .BORDEiz-arGris {
	        border-top-width: 1pt;
	        border-right-width: 0pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: none;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #C0C0C0;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }        
        .BORDEiz-ar-de-ab {
	        border-top-width: 1pt;
	        border-right-width: 1pt;
	        border-bottom-width: 1pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        
        .BORDEiz-ar-de-abGris {
	        border-top-width: 1pt;
	        border-right-width: 1pt;
	        border-bottom-width: 1pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #C0C0C0;
	        border-right-color: #000000;
	        border-bottom-color: #C0C0C0;
	        border-left-color: #000000;
        }
        .BORDEiz-ar-de {
	        border-top-width: 1pt;
	        border-right-width: 1pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        
 .BORDEiz-ar-deGris {
	        border-top-width: 1pt;
	        border-right-width: 1pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #C0C0C0;
	        border-right-color: #000000;
	        border-bottom-color: #C0C0C0;
	        border-left-color: #000000;
        }        
        
       .BORDEiz-ar-ab {
            border-top-width: 1pt;
	        border-right-width: 0pt;
	        border-bottom-width: 1pt;
	        border-left-width: 1pt;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        .BORDEiz-de {




	        border-top-width: 0pt;
	        border-right-width: 1pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: none;
	        border-right-style: solid;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        .BORDEiz {
	        border-top-width: 0pt;
	        border-right-width: 0pt;
	        border-bottom-width: 0pt;
	        border-left-width: 1pt;
	        border-top-style: none;
	        border-right-style: none;
	        border-bottom-style: none;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }

        .BORDEde {
	        border-right-width: 1pt;
	        border-right-style: solid;		
	        border-right-color: #000000;	
        }
        .BORDEar {
	        border-top-width: 1pt;
	        border-top-style: solid;		
	        border-top-color: #000000;		
        }

        .BORDEde_ar {
	        border-top-width: 1pt;
	        border-top-style: solid;		
	        border-top-color: #000000;		
	        border-right-width: 1pt;
	        border-right-style: solid;		
	        border-right-color: #000000;
        }
        .BORDEde_ab {
	        border-bottom-width: 1pt;
	        border-bottom-style: solid;		
	        border-bottom-color: #000000;		
	        border-right-width: 1pt;
	        border-right-style: solid;		
	        border-right-color: #000000;
        }
        .BORDEiz_ab {
	        border-bottom-width: 1pt;
	        border-bottom-style: solid;		
	        border-bottom-color: #000000;		
	        border-left-width: 1pt;
	        border-left-style: solid;		
	        border-left-color: #000000;
        }

        .BORDEab {
	        border-bottom-width: 1pt;
	        border-bottom-style: solid;		
	        border-bottom-color: #000000;		

        }
        .BORDEiz-de-ab {
	        border-right-width: 1pt;
	        border-bottom-width: 1pt;
	        border-left-width: 1pt;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }

        .Arial95Negrita {

	        font-family: Arial;
	        font-size: 9.5pt;
	        font-weight: bold;
        }
       
               .titulos {
	        font-family: Arial;
	        font-size: 12.5pt;
	        font-style: italic;
	        font-weight: bold;
        }
   
        .subtitulos_14_bordeinferior_3 {
	        font-family: Arial;
	        font-size: 13pt;
	        font-weight: bold;
	        border-top-width: 0px;
	        border-right-width: 0px;
	        border-bottom-width: 3px;
	        border-left-width: 0px;
	        border-top-style: none;
	        border-right-style: none;
	        border-bottom-style: solid;
	        border-left-style: none;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        .subtitulos_14 {
	        font-family: Arial;
	        font-size: 13pt;
	        font-weight: bold;
        }
        .letras_blancas_chicas {
	        font-family: Arial;
	        font-size: 10pt;
	        font-weight: bold;
	        color: #FFFFFF;
        }
        .letras_negras_chicas {
	        font-family: Arial;
	        font-size: 9pt;
	        font-weight: bold;
        }
        .subtitulos_12 {
	        font-family: Arial;
	        font-size: 12pt;
	        font-weight: bold;
        }
        .texto12 {
	        font-family: Arial;
	        font-size: 12pt;
        }
        .texto10 {
	        font-family: Arial;
	        font-size: 10pt;
        }
        .texto10negrita {
	        font-family: Arial;
	        font-size: 10pt;
	        font-weight: bold;
        }
        .texto12italica {
	        font-family: Arial;
	        font-size: 12pt;
	        font-style: italic;
        }
        .texto11 {
	        font-family: Arial;
	        font-size: 11pt;
        }
        .titulo_date {
	        border-top-width: 3px;
	        border-right-width: 3px;
	        border-bottom-width: 3px;
	        border-left-width: 3px;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
	        font-family: Arial;
	        font-size: 10pt;
	        font-weight: bold;
        }
        .letras_grises {
	        font-family: Arial;
	        font-size: 10pt;
	        color: #808080;
        }
        .borde_inferior3 {
	        border-top-width: 0px;
	        border-right-width: 0px;
	        border-bottom-width: 3px;
	        border-left-width: 0px;
	        border-top-style: none;
	        border-right-style: none;
	        border-bottom-style: solid;
	        border-left-style: none;
	        border-bottom-color: #000000;
	        
        }
        .saco_borde_superior {
	        border-top-width: 0px;
        }
        .bordeinferior1_x {
	        border:none;
			border-bottom:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .75pt;
  
        }
        .x {
	        font-family: Arial;
	        font-size: 9pt;
	        font-weight: bold;
        }
        .borde_superior3 {
	      /*  border-top-width: 3px;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
	        border-right-width: 1px;
	        border-bottom-width: 1px;
	        border-left-width: 1px;
	        font-family: Arial;
	        font-size: 12pt;
	        font-weight: bold;*/
	        width:151.2pt;
	        padding: 0cm 5.4pt 0cm 5.4pt;
	        height:28.75pt
        }
        .bordeinferior3 {
	        font-family: Arial;
	        font-size: 12pt;
	        font-weight: bold;
	        border-top-width: 1px;
	        border-right-width: 1px;
	        border-bottom-width: 3px;
	        border-left-width: 1px;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }

        .bordecompleto{
	        font-family: Arial;
	        font-size: 11pt;
	        font-weight: bold;
	        border-top-width: 3px;
	        border-right-width: 3px;
	        border-bottom-width: 3px;
	        border-left-width: 3px;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
        }
        .borderojo {
	        border: 3pt solid #FF0000;
        }
        .texto12negrita_enblanco {
	        font-family: Arial;
	        font-size: 12pt;
	        font-weight: bold;
	        color: #FFFFFF;
        }
        .texto10italica {
	        font-family: Arial;
	        font-size: 10pt;
	        font-style: italic;
        }
       
        .titulos_helvetica {
	        font-family: Helvetica;
	        font-size: 16pt;
	        font-weight: bold;
        }
        .subtitulos_helvetica12negrita {
	        font-family: Helvetica;
	        font-size: 12pt;
	        font-weight: bold;
        }
        .subtitulos_helvetica12 {
	        font-family: Helvetica;
	        font-size: 12pt;
        }







        .texto10times {
	        font-family: Times;
	        font-size: 10pt;
        }
        .texto10timesitalica {
	        font-family: Times;
	        font-size: 10pt;
	        font-style: italic;
        }



        .bordesuperior1 {
	        border-top-width: 1pt;
	        border-right-width: 0pt;
	        border-bottom-width: 0pt;
	        border-left-width: 0pt;
	        border-top-style: solid;
	        border-right-style: none;
	        border-bottom-style: none;
	        border-left-style: none;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
	        font-family: Times;
	        font-size: 10px;
        }


        .bordeinferior1_times8 {
	        font-family: Times;
	        border-top-width: 0px;
	        border-right-width: 0px;
	        border-bottom-width: 1pt;
	        border-left-width: 0px;
	        border-top-style: solid;
	        border-right-style: solid;
	        border-bottom-style: solid;
	        border-left-style: solid;
	        border-top-color: #000000;
	        border-right-color: #000000;
	        border-bottom-color: #000000;
	        border-left-color: #000000;
	        font-size: 8pt;
	
        }

        .times8 {
	        font-family: Times;
	        font-size: 8pt;
	
        }




        .subtitulos_helvetica14 {
	        font-family: Helvetica;
	        font-size: 14pt;
	        font-weight: bold;
	        color: #FFFFFF;
        }
        .texto11times {
	        font-family: Times;
	        font-size: 11pt;
        }
        .Perspectivas {
	        font-family: Times;
	        font-size: 10pt;
        }
        .texto11timesitalica {
	        font-family: Times;
	        font-size: 11pt;
	        font-style: italic;
        }
        
 @page Section2 {margin:.3in .3in .3in .3in;mso-header-margin:.3in;mso-footer-margin:.3in;}
div.Section2 {page:Section2;}      


    </style>
   <div runat="server" style="width:100%" class="Section2">
   

    <form id="form2" runat="server">
   
         <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldPasoID" runat="server" />
          <asp:Panel runat="server" ID="PanelPMP">

         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" 
             BorderStyle="Dotted">
            <ItemTemplate>
                      
                      <table width="99%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="196" rowspan="2" class="borde_inferior3">
                          <div id="logo" runat="server">
                             <img alt="Imagen" src="http://Word/logoprint.jpg">
                           </div>
                           </td>
                              <td width="396" rowspan="2" class="borde_inferior3"><p class="titulos"> Performance Management Process <br />
                            For the Performance Year  <u>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PeriodoDesde","{0:yyyy}") %>'></asp:Label> / <asp:Label ID="Label9" runat="server" Text='<%# Bind("PeriodoHasta","{0:yyyy}") %>'></asp:Label> </u> <br />
                          <%--Managers and Supervisors --%>
                              <asp:Label ID="Label2" runat="server" Text='<%# Bind("TipoFormulario") %>'></asp:Label> </p> </td>
                          <td width="100" height="35" valign="top" align="right" style="font-family:Arial, Helvetica, sans-serif;font-size:10px"><asp:Label ID="lblCodeForm" runat="server" Text=""></asp:Label><!--  SFO000024 --></td>
                        </tr>
                        <tr>
                          <td height="22" valign="bottom" class="bordecompleto" > 
                              <asp:Label ID="Label8" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>'></asp:Label></td>
                        </tr>
                    </table>
                      <table width="99%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="bottom">
                          <td height="20" colspan="3" class="bordeinferior1_x"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="93%" height="50" valign="bottom" ><asp:Label ID="Label5" runat="server" Text='<%# Bind("EvaluadoNombre") %>'></asp:Label></td>
                              <td width="7%" valign="bottom"><asp:Label ID="Label6" runat="server" Text='<%# Bind("EvaluadoLegajo") %>'></asp:Label></td>
                            </tr>
                          </table></td>
                          <td width="48">&nbsp;</td>
                          <td colspan="3" class="bordeinferior1_x"> 
                              <asp:Label ID="Label4" runat="server" Text='<%# Bind("EvaluadorNombre") %>'></asp:Label></td>
                        </tr>
                        <tr>
                          <td width="171" class="letras_grises">Appraisee</td>
                          <td colspan="2" class="letras_grises"><div align="right">Employee
                          Number</div></td>
                          <td>&nbsp;</td>
                          <td colspan="3" class="letras_grises">Appraising Manager</td>
                        </tr>
                        <tr valign="bottom">
                          <td height="50" class="bordeinferior1_x">&nbsp;</td>
                          <td colspan="2" align="right" class="bordeinferior1_x">&nbsp;&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td class="letras_grises">Appraisee Signature</td>
                          <td colspan="2" class="letras_grises"><div align="right">Date</div></td>
                          <td>&nbsp;</td>
                          <td colspan="2" class="letras_grises">Appraising Manager&rsquo;s 
                    Signature</td>
                          <td width="61" class="letras_grises"><div align="center">Date</div></td>
                        </tr>
                        <tr valign="bottom">
                          <td height="20" colspan="3" class="bordeinferior1_x"><asp:Label ID="Label7" runat="server" Text='<%# Bind("EvaluadoArea") %>'></asp:Label></td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x"><div align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div>        </td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="3" class="letras_grises">Division</td>
                          <td>&nbsp;</td>
                          <td width="214" class="letras_grises">Review - Next Level Management</td>
                          <td width="71" class="letras_grises"><div align="center">Initials</div></td>
                          <td><div align="center" class="letras_grises">Date</div></td>
                        </tr>
                        <tr valign="bottom">
                          <td  height="20" width="220" class="bordeinferior1_x"><%--<p align="left" class="x"><b > --%>
                              <%--<asp:Label ID="Label3" runat="server" Text='<%# Bind("Periodo") %>'></asp:Label>--%>
                              <asp:Label ID="Label23" runat="server" Text='<%# Bind("PeriodoDesde","{0:dd/MM/yyyy}") %>'></asp:Label> To <asp:Label ID="Label25" runat="server" Text='<%# Bind("PeriodoHasta","{0:dd/MM/yyyy}") %>'></asp:Label>
                              <%--</b></p>--%></td>
                        <td width="60" class="bordeinferior1_x"><p class="x"><b style='mso-bidi-font-weight:normal'><span style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:
                      Arial;mso-bidi-font-family:&quot;Times New Roman&quot;"></span></b></p></td>
                          <td width="10" class="bordeinferior1_x">&nbsp;</td>
                          <td>&nbsp;</td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                          <td class="bordeinferior1_x"><div align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</div></td>
                          <td class="bordeinferior1_x">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="3" class="letras_grises"  align="left">For the Period
                          Covering</td>
                          <td>&nbsp;</td>
                          <td class="letras_grises">Human Resources Manager</td>
                          <td class="letras_grises"><div align="center">Initials</div></td>
                          <td><div align="center" class="letras_grises">Date</div></td>
                        </tr>
                      </table>
            </ItemTemplate>
         </asp:FormView>

         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
         
         
                 <H1 class="SaltoDePagina"> </H1> 
          </asp:Panel>         
         <asp:Panel ID="PPReview" runat="server">
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="5" class="subtitulos_14_bordeinferior_3" height="50" valign="bottom">Performance Plan &amp; Review</td>
    </tr>
    <tr>
      <td colspan="5"><span class="letras_grises" style="font-size:9pt;mso-bidi-font-size:10.0pt;font-family:Arial; mso-bidi-font-family:&quot;Times New Roman&quot;">If planning the YEAR AHEAD, list the
desired goals for this individual, up to five(5), and indicate measures
(how the degree of achievement will be assessed). If reviewing the YEAR
COMPLETED, list the results and the degree of completion (1-5) as follows:
          <o:p></o:p>
      </span></td>
    </tr>
    <tr>
      <td height="19"><span class="subtitulos_12" style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">1 = not achieved
          <o:p></o:p>
      </span></td>
      <td><span class="subtitulos_12" style="font-size:11.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">2 = partially achieved
          <o:p></o:p>
      </span></td>
      <td><span class="subtitulos_12" style="font-size:11.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">3 = achieved
          <o:p></o:p>
      </span></td>
      <td><span class="subtitulos_12" style="font-size:11.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">4 = exceeded
          <o:p></o:p>
      </span></td>
      <td><span class="subtitulos_12" style="font-size:11.0pt;mso-bidi-font-size:11.0pt;font-family: Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">5 = far exceeded
          <o:p></o:p>
      </span></td>
    </tr>
  </table>
  <table width="99%" border="1px" cellpadding="1"  cellspacing="0" bordercolor="#000000">
    <tr class="borde_superior3">
      <td width="30%" align="center" class="borde_superior3">Objective</td>
      <td width="30%" align="center" class="borde_superior3"><span style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:
   Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Measures</span></td>
      <td width="30%" align="center" class="borde_superior3"><span style="font-size:12.0pt;mso-bidi-font-size:10.0pt;font-family:
   Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Results</span></td>
      <td width="10%" align="center" class="borde_superior3"><span style="font-size:12.0pt;
   mso-bidi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Rating</span></td>
    </tr>
                    
       <asp:ObjectDataSource ID="ObjectDataSourceCalificaciones" runat="server" OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCalificaciones" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                 <SelectParameters>
                     <asp:Parameter DefaultValue="I" Name="Idioma" Type="String" />
                     <asp:Parameter DefaultValue="P" Name="modulo" Type="String" />
                 </SelectParameters>
         </asp:ObjectDataSource>
          <asp:Repeater ID="RepeaterIndividualObjetives1" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives1" OnPreRender="RepeaterIndividualObjetives_PreRender">
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
                    </asp:Repeater>

         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="24" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        <asp:Repeater ID="RepeaterIndividualObjetives2" runat="server" DataSourceID="ObjectDataSourceIndividualObjetives2" OnPreRender="RepeaterIndividualObjetives_PreRender">
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
        </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="25" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Repeater ID="RepeaterIndividualObjetives3" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives3" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives3_ItemDataBound" >
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="26" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Repeater ID="RepeaterIndividualObjetives4" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives4" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives4_ItemDataBound"> 
          
                    <HeaderTemplate>
                <tr>
                </HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives4" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="27" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
              <asp:Repeater ID="RepeaterIndividualObjetives5" runat="server" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          DataSourceID="ObjectDataSourceIndividualObjetives5" 
          onitemdatabound="RepeaterIndividualObjetives5_ItemDataBound" >
          
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives5" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="28" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
              <asp:Repeater ID="RepeaterIndividualObjetives6" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives6" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives6_ItemDataBound" >
          
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives6" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="29" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
              <asp:Repeater ID="RepeaterIndividualObjetives7" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives7" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives7_ItemDataBound" >
          
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives7" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="30" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
              <asp:Repeater ID="RepeaterIndividualObjetives8" runat="server"  
          DataSourceID="ObjectDataSourceIndividualObjetives8" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives8_ItemDataBound" >
          
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives8" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="31" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>



                    
              <asp:Repeater ID="RepeaterIndividualObjetives9" runat="server" 
          DataSourceID="ObjectDataSourceIndividualObjetives9" 
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives9_ItemDataBound">
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives9" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="32" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>


              <asp:Repeater ID="RepeaterIndividualObjetives10" runat="server" Visible="false"
          DataSourceID="ObjectDataSourceIndividualObjetives10"  
          OnPreRender="RepeaterIndividualObjetives_PreRender" 
          onitemdatabound="RepeaterIndividualObjetives10_ItemDataBound" >
          
                <HeaderTemplate><tr></HeaderTemplate>
                <ItemTemplate>
                            <td>
                            <span class="texto10">
                            <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                            <asp:Label ID="Label2" runat="server" Text='<%# setValorPonderacion(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>

                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>           
                            </span>
                       </td>

                   
                </ItemTemplate>
            <FooterTemplate></tr></FooterTemplate>

            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceIndividualObjetives10" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="33" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<%--    <tr>
      <td height="22" align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
      <td align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
      <td align="left" valign="top" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
      <td align="center" valign="middle" class="texto10"><div class="style2" style="word-wrap:break-word">&nbsp;</div></td>
    </tr>--%>
</table>
  <br/>
            </asp:Panel>


        <asp:Panel ID="PLeaderImperatives" runat="server">
                <table width="1202" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1202" class="subtitulos_14"><span style='mso-bookmark:SCJ_Skills_List'><b
            style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;
            mso-bidi-font-size:10.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;' lang="EN-US" xml:lang="EN-US">Performance
            Review </span></b></span><span style='mso-bookmark:SCJ_Skills_List'><b
            style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;
            mso-bidi-font-size:10.0pt;font-family:Symbol;mso-ascii-font-family:Arial;
            mso-hansi-font-family:Arial;mso-char-type:symbol;mso-symbol-font-family:Symbol' lang="EN-US" xml:lang="EN-US"><span
            style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>-</span></span></b></span><span
            style='mso-bookmark:SCJ_Skills_List'><b style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;mso-bidi-font-size:10.0pt;font-family:Arial;
            mso-bidi-font-family:&quot;Times New Roman&quot;'
            lang="EN-US" xml:lang="EN-US">Leadership Imperatives</span></b></span></td>
                </tr>
            </table>
                <table width="99%" border="0" cellpadding="0"  cellspacing="0" bordercolor="#000000">
                <tr>
                    <!-- <img src="Img/FondoNegro2.jpg" alt="titulo" width="733" height="24" />-->
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">1 &ndash; Unsatisfactory</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">2 &ndash; Needs Development</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">3 &ndash; Effective</td>
	                <td width="20%" bgcolor="#000000" class="letras_blancas_chicas">4 &ndash; Excellent</td>
	                <td width="19%" bgcolor="#000000" class="letras_blancas_chicas">5 &ndash; Outstanding</td>
                </tr>
                <tr valign="top">
                    <td width="20%" height="42" class="bordeinferior3"><div class="texto10" style="font-size:8.0pt;mso-bidi-font-size:
                10.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;word-wrap:break-word;">This person
                is currently <b style='mso-bidi-font-weight:normal'>not meeting</b> the
                performance standards of the company for this position.</div></td>
                    <td width="20%" valign="top" class="bordeinferior3"><div class="texto10" style="font-size:8.0pt;mso-bidi-font-size:10.0pt;font-family:Arial;
                mso-bidi-font-family:&quot;Times New Roman&quot;word-wrap:break-word;">This person meets <b
                style='mso-bidi-font-weight:normal'>some but not all </b>of the performance
                standards of the company for this position.
                        <o:p></o:p>
                    </div></td>
                    <td width="20%" valign="top" class="bordeinferior3"><div class="texto10" style="font-size:8.0pt;mso-bidi-font-size:10.0pt;font-family:Arial;
                mso-bidi-font-family:&quot;Times New Roman&quot;word-wrap:break-word;">This person <b style='mso-bidi-font-weight:
                normal'>meets and occasionally exceeds</b> the high standards of the company
                for this position.
                        <o:p></o:p>
                    </div></td>
                    <td width="20%" class="bordeinferior3"><div class="texto10" style="font-size:8.0pt;font-family:Arial;
                mso-bidi-font-family:&quot;Times New Roman&quot;word-wrap:break-word;">This person <b style='mso-bidi-font-weight:
                normal'>meets and frequently exceeds</b> the high standards of the company
                for this position.
                        <o:p></o:p>
                    </div></td>
                    <td width="19%" class="bordeinferior3"><div class="texto10" style="font-size:8.0pt;font-family:Arial;
                Times New Roman;&quot;word-wrap:break-word;">Through leadership and positive
                example, this person is a positive influence to the broader organization.
                        <o:p></o:p>
                    </div></td>
                </tr>
            </table>

   
       


            <asp:Repeater ID="RepeaterSuccessFactors" runat="server" 
                DataSourceID="ObjectDataSourceSuccessFactors">
                <HeaderTemplate>
                  <table width="99%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
                    <tr>
                      <td width="97%" class="saco_borde_superior">&nbsp;</td>
                      <td width="2%" class="saco_borde_superior"><div align="center"><span class="texto10negrita" style="font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;">Rating</span></div></td>
                    </tr>                  
                </HeaderTemplate>
                <ItemTemplate>

                        <tr>
                          <td valign="top" width="960" height="32"><span class="texto10"><asp:Label ID="titulo" runat="server"  Text='<%# formatearTexto(Eval("Titulo").ToString(),Eval("Descripcion").ToString().Replace(Environment.NewLine,"<br />"))%>'></asp:Label>
                          <strong>
                          <asp:Label ID="lblOptional" runat="server" Text='<%# "<br />" + devuelveFortalezaNecesidad(Eval("Fundamentacion").ToString()) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("OPTIONAL")>=0 %>'></asp:Label>
                          </strong>
                          </td>
                          <td width="36"><div align="center">
                            <span class="texto10negrita">&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text='<%# Eval("Ponderacion").ToString()!="0" ? Eval("Ponderacion"): ""%>'></asp:Label>&nbsp;</span>
                          </div></td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceSuccessFactors" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="7" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                        Name="PeriodoID" PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </asp:Panel>

        <asp:Panel ID="PSDNeed" runat="server">

  <br/>
  <table width="99%" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td><p><span class="subtitulos_14">Current Strengths/Development Needs</span><br/>
          <span class="texto12italica">Use skills from the <asp:HyperLink ID="HyperLink1" Target="_blank" NavigateUrl="http://sharepoint.scj.com/sites/global/FormsLibrary/Memo/SCJ%20Skill%20List%2011_15_2011.xls" runat="server">SCJ Skills List</asp:HyperLink>.</span></td>
    </tr>
  </table>
  <table width="99%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
    <tr>
      <td width="368" class="borde_superior3"><div align="center"><span class="texto10">Strengths</span></div></td>
      <td width="368" class="borde_superior3"><div align="center"><span class="texto10">Development Needs</span></div></td>
    </tr>
    <tr>
      <td width="368" height="22">
            <asp:Repeater ID="RepeaterSTRENGTHS" runat="server" 
                DataSourceID="ObjectDataSourceSTRENGTHS">
                <HeaderTemplate>
                    <table id="RepeaterSTRENGTHS0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><span class="texto10">
                            <asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label>
                            </span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceSTRENGTHS" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="15" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                        Name="PeriodoID" PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
      </td>
      <td width="368">
            <asp:Repeater ID="RepeaterDevelopmentNeeds" runat="server" 
                DataSourceID="ObjectDataSourceDevelopmentNeeds" >
                <HeaderTemplate>
                    <table id="RepeaterDevelopmentNeeds0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><span class="texto10">
                        <asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label>
                        </span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:ObjectDataSource ID="ObjectDataSourceDevelopmentNeeds" runat="server" 
                OldValuesParameterFormatString="original_{0}" SelectMethod="GetItemEvaluacion" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="16" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" 
                        Name="PeriodoID" PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
      </td>
    </tr>
  </table>

       
      
            <asp:ObjectDataSource ID="ObjectDataSourceFortalezas" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetFortalezasByTipoFormularioID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br/> 
          </asp:Panel>


        <asp:Panel ID="PDAFTNextFiscalYear" runat="server">

  <table width="99%" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td class="subtitulos_14">Development Actions for the next fiscal year</td>
    </tr>
  </table>
  <table width="99%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
            <asp:Repeater ID="RepeaterDEVELOPMENTACTIONS" runat="server" 
                    DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS">
                <HeaderTemplate>

                            <tr>
                            <td>                      

                </HeaderTemplate>
                <ItemTemplate>
                              <span class="texto10">  <asp:Label ID="Label4" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label></span>
                                <%--<asp:TextBox ID="TextBoxComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' 
                                TextMode="MultiLine" Rows="5"  SkinID="form-textarea"></asp:TextBox>--%>
                   
                </ItemTemplate>
                <FooterTemplate>
                        </td>
                    </tr>

                    </FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceDEVELOPMENTACTIONS" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="9" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
  </table>
  <br/> 

            <asp:Repeater runat="server" ID="RepeaterCursosSeleccionados2010_2011" 
                DataSourceID="ObjectDataSourceDevelopmentNeeds" 
                onitemdatabound="RepeaterCursosSeleccionados_ItemDataBound">
            <HeaderTemplate>
            <table width="99%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
            <thead>
            <tr> <td width="40%" class="borde_superior3">Development Needs</td><td width="60%" class="borde_superior3">Related Courses</td></tr>
            </thead>
            </HeaderTemplate>
            <ItemTemplate>
            <tr>
            <td><span class="texto10"><asp:Label ID="lblFortalezas" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>' ></asp:Label></span>
                <asp:HiddenField  runat="server" ID="HiddenFieldDebilidadID" value='<%# Eval("Ponderacion") %>'/></td>
            <td>
             <span class="texto10"><asp:Literal runat="server" ID="LiteralCursosRelacionados"></asp:Literal></span>
            
            </td>

            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>  
            </FooterTemplate>
            
            </asp:Repeater> 


            <asp:Repeater runat="server" ID="RepeaterCursos"  
                DataSourceID="ObjectDataSourceCursos">
            <HeaderTemplate>
            <table width="99%" border="1" bordercolor="#000000"  cellspacing="0" cellpadding="1">
            <thead>
            <tr> <td width="60%" class="borde_superior3">Related Courses</td></tr>
            </thead>
            </HeaderTemplate>
            <ItemTemplate>
            <tr>
            <td>
             <span class="texto10"><asp:Label runat="server" ID="CursoDescripcion"  Text='<%# Eval("CursoDescripcion") %>'>'></asp:Label></span>
            
            </td>

            </tr>
            </ItemTemplate>
            <FooterTemplate>
            </table>  
            </FooterTemplate>
            
            </asp:Repeater> 
            
            
            <asp:ObjectDataSource ID="ObjectDataSourceCursos" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetCursosByTramiteID" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" 
                        Name="TipoFormularioID" PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </asp:Panel>
            <asp:Panel ID="PPerformanceReviewSummary" runat="server">
 <br/> 
  <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="subtitulos_14">Performance Review - Summary</td>
    </tr>
  </table>
  

         <asp:Repeater ID="RepeaterPerformanceReviewSummary" runat="server" 
                DataSourceID="ObjectDataSourcePerformanceReviewSummary" >

                <ItemTemplate>
                               <asp:Label ID="lblTitulo" runat="server" Text='<%# summaryComments(Eval("Titulo"), Eval("Fundamentacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")<0 %>'></asp:Label>
                               <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />

                                <asp:Label ID="lblChosseOptionRating" runat="server" Text='<%# OverallRating(Eval("Ponderacion")) %>' Visible='<%# Eval("Titulo").ToString().ToUpper().IndexOf("RATING")>=0 %>'></asp:Label>
                </ItemTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePerformanceReviewSummary" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="10" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            </asp:Panel>   
          

        <asp:Panel ID="PExecutiveManagementDevelopmentPlan" runat="server">
  <div style="font-family: Calibri !important;" runat="server">                   
 <table width="100%" border="0" cellspacing="0" cellpadding="0"  style="padding-left: 3px; padding-right: 3px;">
    <tr>
      <td class=""><span class="CalibriBlack12">
          <asp:Repeater ID="RepeaterPeriodo" runat="server" DataSourceID="ObjectDataSourceCabecera">
          <HeaderTemplate></HeaderTemplate>
          <ItemTemplate>
          <asp:Label runat="server" ID="LabelPeriodoDesde" Text='<%# (int.Parse(Eval("PeriodoDesde" , "{0:yyyy}")) + 1 )  + "/" + (int.Parse(Eval("PeriodoHasta" , "{0:yy}")) + 1) %>'></asp:Label>
         <%-- /
           <asp:Label runat="server" ID="Label26" Text='<%# Bind("PeriodoHasta" , "{0:yy}") %>'></asp:Label>--%>
          </ItemTemplate>
          <FooterTemplate></FooterTemplate>
          </asp:Repeater>
      Executive Management Succession &amp; Development Plan </span>
      <span class="CalibriBlack10">
       <i> (For International Use Only)</i>
      </span>
      </td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr>
      <td width="333" class="BORDEiz-ar"><span class="Calibri9">LAST NAME:<br/>
          <span class="CalibriNegrita12"> <asp:Label ID="lblApellidoEvaluado" runat="server" Text=""></asp:Label> &nbsp;</span></span></td>
      <td width="246" style="border-top: 1pt solid #000;" ><span class="Calibri9">FIRST NAME:<br/>
          <span class="CalibriNegrita12"><asp:Label ID="lblNombreEvaluado" runat="server" Text=""></asp:Label></span></span></td>
      <td width="172" class="BORDEde_ar"><span class="Calibri9">MI:<br/>
          <span class="X"> </span></span></td>
    </tr>
    <tr bgcolor="#DFDFDF">
      <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION A: CURRENT JOB</span></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr>
      <td width="333" class="BORDEiz"><span class="Calibri9" >TITLE:</span><br/><span class="CalibriNegrita10"> 
                <asp:Label ID="lblTitleA" runat="server" Text="Label"></asp:Label> </span></td>
      <td width="163"><span class="Calibri9">LEVEL:</span><br/>
            <span class="CalibriNegrita10"><asp:Label ID="lblEscalaSalarial" runat="server" Text="Label"></asp:Label> </span></td>
      <td width="255" class="BORDEde"><span class="Calibri9">TIME IN JOB:</span><br/>
            <span class="CalibriNegrita10">  <asp:Label ID="LabelTimeInJob" runat="server" Text=""></asp:Label></span></td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
  <tr bgcolor="#DFDFDF">
      <td class="BORDEiz-arGris" width="49%"><span class="Calibri95NegritaBlue">SECTION B: LAST PERFORMANCE RATINGS</span></td>
      <td width="51%" colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">EDUCATION</span></td>
    </tr>
    <tr>
      <td  class="BORDEiz" width="49%"><span class="Calibri8Italica">Enter last 3 years of performance ratings.</span></td>
      <td  class="BORDEiz" width="9%"><span class="Calibri9">DEGREE:</span></td>
      <td width="9%"><span class="Calibri9" >YEAR:</span></td>
      <td  class="BORDEde"><span class="Calibri9">SCHOOL: </span></td>
    </tr>
<!-- -->   
<tr> 
 <td class="BORDEiz">
        <asp:Repeater ID="RepeaterSeccionBPerformanceRatings" runat="server" DataSourceID="ObjectDataSourceLastRating">
            <HeaderTemplate>
            <table border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                  <td><span style="mso-no-proof:yes" class="Calibri85">
                  <b  style='mso-bidi-font-weight:normal'>
                  <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label></b>&nbsp;</span>
                 <span class="Calibri9"> <asp:Label ID="LabelCalificacion" runat="server" Text='<%# Eval("Calificacion") %>'></asp:Label></span> </td>                     
                </tr>                                              
            </ItemTemplate>
            <FooterTemplate>
            </table>
            </FooterTemplate>
        </asp:Repeater>
        </td>
         <td class="BORDEiz-de" colspan="3">
        <!---->

         <asp:Repeater ID="DataListEducation" runat="server"  >
         <HeaderTemplate>
          <table id="DataListEducation" border="0">
          </HeaderTemplate>
         <ItemTemplate>
         <tr>
                  <td  width="17%"><span class="Calibri10"><asp:Label ID="Label7" runat="server" Text='<%# DameResumeDegree(Eval("Degree")) %>'></asp:Label>&nbsp;</span></td>
                  <td  width="17%"><span class="Calibri10"  ><asp:Label ID="Label8" runat="server" Text='<%# Bind("Year") %>' ></asp:Label>&nbsp;</span></td>
                  <td ><span class="Calibri10" style="word-wrap: break-word;"><asp:Label ID="Label9" runat="server" Text='<%# DameSchool(Eval("School")) %>'></asp:Label></span></td>        
         </tr>
         </ItemTemplate>
         <FooterTemplate></table></FooterTemplate>
        </asp:Repeater>

        </td>
        </tr>
         
        




  

        
<!-- -->
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
  <tr bgcolor="#DFDFDF">
      <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION C: MOBILITY &amp;
  TRAVEL (Completed by Employee)</span></td>
 </tr>
 <tr>
 

         <asp:Repeater ID="RepeaterMobilityAndTravel" runat="server">
                <ItemTemplate>
                
                

                    <asp:Literal ID="lblTitulo" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "185" || Eval("ItemEvaluacionID").ToString()== "390" %>' Text='<%# "<td colspan=\"2\" class=\"BORDEiz\"><span class=\"Calibri8\">" + Eval("Titulo").ToString() +":" %>'>
                    </asp:Literal> 
                    <asp:Label ID="Label10" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "185" || Eval("ItemEvaluacionID").ToString()== "390" %>' Text='<%# setMobilidad(Eval("Ponderacion")) %>'></asp:Label>
                    
                    
                    <asp:Label ID="Label18" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "186" || Eval("ItemEvaluacionID").ToString()== "391" %>' Text='<%# "&nbsp;&nbsp;&nbsp;&nbsp" + Eval("Titulo").ToString() %>'>
                    Willingness to Travel: </asp:Label>
                    <asp:Literal ID="Label16" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "186" || Eval("ItemEvaluacionID").ToString()== "391" %>' Text='<%# (setMobilidadYN(Eval("Ponderacion")) + "</span></td>") %>'></asp:Literal>
                    
      <td width="50%" class="BORDEiz-de" id="tdModbiOne" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392" %>'><span class="Calibri8Negrita">Limitations to mobility/travel. The character
  limit should not exceed 45.</span>
  <br><span class="Calibri9"><asp:Label ID="Label11" runat="server" Text='<%# Bind("Fundamentacion") %>' Visible='<%# Eval("ItemEvaluacionID").ToString()== "187" || Eval("ItemEvaluacionID").ToString()== "392" %>'></asp:Label></span></td>
                                                      
                   
                </ItemTemplate>

            </asp:Repeater>


 <!---->


    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
  <tr bgcolor="#DFDFDF">
      <td class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION D: LANGUAGE PROFICIENCY (Completed by Employee)</span></td>
 </tr>
  <tr style='mso-yfti-irow:11;page-break-inside:avoid;height:13.0pt'>
  <td width="100%" class="BORDEiz-ar-deGris">

                 <asp:Repeater ID="RepeaterLanguage" runat="server"  >
                <HeaderTemplate>
                     <table width="100%" >
                    
                </HeaderTemplate>
                <ItemTemplate>

                        <tr>
                            <td width="2%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>' ><span lang=EN-US style='font-size:9.0pt;mso-bidi-font-size:10.0pt'><b>
                             <asp:Label runat="server" ID="LabelNumber" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>'></asp:Label>   
                             <asp:HiddenField ID="HiddenFieldNumber" Value='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' runat="server" />                    

                            </b></span></td>
                            <td width="35%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'>
                            <span style='font-size:9.0pt;letter-spacing:.1pt' class="Calibri9"><b style='mso-bidi-font-weight:normal'>
                                <asp:Label ID="Label22" runat="server" Text='<%# Eval("Idioma") %>'></asp:Label></b>
                                </span></td>
                            <td width="5%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9"><asp:label ID="LabelNative22" runat="server" Visible='<%# Eval("IsNative").ToString()=="True" %>' >Native:</asp:label></span></td>


                            <td  width="10%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9">
                            <%--<asp:CheckBox ID="CheckBoxNative" runat="server"  Text="Native:" Checked='<%# Eval("IsNative").ToString()=="1" %>' Visible='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()=="1" %>'  TextAlign="Left"/>--%>
                            <asp:Label runat="server" ID="LabelNative" Text='<%# Eval("IsNative").ToString()=="True"? "Yes": "" %>'></asp:Label>
                            </span></td>
                            
                            <td width="10%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9">
                            </span></td>
                            <td width="7%" style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'>
                            <span class="Calibri9">Proficiency: </span></td>
                            <td style='<%# (((RepeaterItem)Container).ItemIndex+1).ToString() =="4" ? "" : "border-bottom: 0.8pt solid #C0C0C0;"%>'><span class="Calibri9"> <asp:Label ID="LabelProfiency" runat="server" Text='<%# DameProficiency(Eval("Proficiency")) %>'></asp:Label> </span></td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>

</td>
 </tr> 
  
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr bgcolor="#DFDFDF">
      <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue">SECTION E: CAREER INTERESTS (Completed by Employee)</span></td>
    </tr>
    <tr>
      <td colspan="2" class="BORDEiz"><span class="Calibri8Italica">List jobs in order of greatest interest to you. Link for list of current job titles <a
  href="documents\JobCodes_Full_List.xls">click
  here</a></span></td>
      <td width="51%" class="BORDEiz-de"><span class="Calibri8NegritaMayuscula">ADDITIONAL  COMMENTS </span><span class="Calibri8"><i> <b>(IF NEEDED) (45
character limit)</b></i></span></td>
    </tr>
<!-- -->
       <asp:Repeater ID="RepeaterCareerInterests" runat="server">
        <ItemTemplate>
            <tr style="line-height: 20px;">
              <td  class="BORDEiz" style="padding-bottom: 3.5pt"><span class="Calibri9Negrita" >

                    <asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />              
              
              </span><span class="Calibri8">Code:</span> <span class="Calibri9Negrita">
              <asp:Label ID="Label13" runat="server" Text='<%# setCareerCode(Eval("Ponderacion")) %>'></asp:Label>
               &nbsp;</span></td>
              <td style="padding-bottom: 3.5pt"><span class="Calibri8">Description: </span><span class="Calibri9">
              <asp:Label ID="Label5" runat="server" Text='<%# setCareer(Eval("Ponderacion")) %>'></asp:Label> </span> </td>
              <td valign="top" class="BORDEiz-de" style="padding-bottom: 3.5pt"><span class="Calibri9"> <asp:Label ID="Label14" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label> </span></td>
            </tr>
                    
                 
                                      
        </ItemTemplate>
      </asp:Repeater>

<!-- -->
  </table>
  
  <asp:Panel runat="server" ID="PanelSectionF">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr bgcolor="#DFDFDF">
      <td colspan="3" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaRed">SECTION F: NEXT ASSIGNMENT (Completed by Manager)</span></td>
    </tr>
    <tr>
      <td  colspan="2" class="BORDEiz"><span class="Arial8">Prioritize 1-3 job(s) as logical next assignment(s). For a list of SCJ  job titles <a href="http://sharepoint.scj.com/sites/global/FormsLibrary/Memo/JobCodes_Full%20List.xls">click
  here</a> </span></td>
      <td width="51%" class="BORDEiz-de"><span class="Calibri8NegritaMayuscula"> &nbsp;ADDITIONAL  COMMENTS </span><span class="Arial8"><i> <b>(IF NEEDED) (45
character limit)</b></i></span></td>
    </tr>

<!---->
       <asp:Repeater ID="RepeaterNextAssigment" runat="server">
        <ItemTemplate>

    <tr style=" line-height: 20px;">
      <td  class="BORDEiz" style="padding-bottom: 3.5pt"><span class="Calibri9Negrita"><asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />     </span> <span class="Calibri8">Code:</span> 
                    <span class="Calibri9Negrita"><asp:Label ID="Label13" runat="server" Text='<%# setCareerCode(Eval("Ponderacion")) %>'></asp:Label> &nbsp;</span></td>
      <td style="padding-bottom: 3.5t"><span class="Calibri9">Description: </span><span class="Calibri9"><asp:Label ID="Label5" runat="server" Text='<%# setCareer(Eval("Ponderacion")) %>'></asp:Label> &nbsp;</span></td>
      <td  style="padding-bottom: 3.5pt" valign="top" class="BORDEiz-de"><span class="Calibri9">
      <asp:Label ID="Label14" runat="server" Text='<%# Bind("Fundamentacion") %>'></asp:Label> &nbsp;</span></td>
    </tr>
        </ItemTemplate>
    </asp:Repeater>
     
  </table>

  </asp:Panel>
  
  <asp:Panel runat="server" ID="PanelSectionG">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr bgcolor="#DFDFDF">
      <td colspan="2" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaRed">SECTION G: DEVELOPMENT SUMMARY (Completed by Manager)</span></td>
    </tr>
    <tr>
      <td colspan="2" class="BORDEiz-de"><span class="Calibri9Italica">1-3 Strengths/Development Needs from PMP form per the SCJ Skills List (Note: &quot;Other&quot; may be selected once). <a href="http://sharepoint.scj.com/sites/global/FormsLibrary/Memo/Skill%20List%20Definitions-master.xls" target="_blank">SCJ Skills List</a> </span><span class="Arial10Blue"></span></td>
    </tr>
    <tr style=" line-height: 20px;">
      <td class="BORDEiz-de" width="49%" ><span class="Calibri9Negrita">Strengths (Skill)</span></td>
      <td class="BORDEde" width="51%"><span class="Calibri9Negrita">Comments (Behavior) (<i>25 character limit</i>):</span></td>
    </tr>
    <!---->
          <asp:Repeater ID="Repeater1" runat="server" DataSourceID="ObjectDataSourceSTRENGTHS">

                <ItemTemplate>
                    <tr style="line-height: 20px;">
                        <td class="BORDEiz-de" style="padding-bottom: 3.5pt"><span class="Calibri9Negrita">
                           <asp:Label ID="Label6" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />                         
                        </span><span class="Calibri9">
                            <asp:Label ID="Label15" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>'></asp:Label> &nbsp; </span></td>
                        <td  class="BORDEde" valign="top" style="padding-bottom: 3.5pt"><span class="Calibri9">
                        <asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label> 
                         &nbsp;</span></td>
                    </tr>
                       
                </ItemTemplate>

            </asp:Repeater>
    <!---->  
    <tr style=" line-height: 20px;">
      <td class="BORDEiz-de" ><span class="Calibri9Negrita">Development Needs (Skill)</span></td>
      <td  class="BORDEde" ><span class="Calibri9Negrita">Comments (Behavior) (<i>25 character limit</i>):</span></td>
    </tr>

<!---->

         <asp:Repeater ID="Repeater2" runat="server" DataSourceID="ObjectDataSourceDevelopmentNeeds">
            <ItemTemplate>
                    <tr style=" line-height: 20px;" >
                        <td class="BORDEiz-de" style="padding-bottom: 3.5pt" ><span class="Calibri9Negrita">
                           <asp:Label ID="Label6" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />                         
                        </span><span class="Calibri9">
                            <asp:Label ID="Label15" runat="server" Text='<%# devuelveFortalezaNecesidad(Eval("Ponderacion")) %>'></asp:Label> &nbsp; </span></td>
                        <td  class="BORDEde" valign="top" style="padding-bottom: 3.5pt"><span class="Calibri9">
                        <asp:Label ID="LabelFundamentacion" runat="server" Text='<%# Eval("Fundamentacion") %>'></asp:Label> 
                         &nbsp;</span></td>
                    </tr>
                       
                </ItemTemplate>
                
            </asp:Repeater>

<!---->    
  </table>
  
  </asp:Panel>
  
  <asp:Panel runat="server" ID="PanelSectionH">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">
    <tr bgcolor="#DFDFDF">
      <td class="BORDEiz-ar-deGris"><span class="Calibri95NegritaBlue" style="font-size: 9.5pt"><b style='mso-bidi-font-weight:normal'><span style="mso-bidi-font-size:10.0pt;color:blue">SECTION H:&nbsp; DEVELOPMENT ACTIONS (Completed by Employee)
        <o:p></o:p>
      </span></b></span></td>
    </tr>
    <tr>
      <td class="BORDEiz-ar-de-abGris" height="30">
       
        
        <asp:Repeater ID="RepeaterSectionH" runat="server" DataSourceID="ObjectDataSourceDEVELOPMENTACTIONS">
        <ItemTemplate>
                        <p ><span class="Calibri9"><asp:Label runat="server" ID="LabelSectionH" Text='<%# Bind("Fundamentacion") %>' ></asp:Label>&nbsp;</span></p>
                   
                </ItemTemplate>

            </asp:Repeater>
      </td>
    </tr>
  </table>

  </asp:Panel>
 
  
  <asp:Panel runat="server" ID="PanelSectionI">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 3px; padding-right: 3px;">  
 <tr bgcolor="#DFDFDF" >
      <td colspan="27" class="BORDEiz-ar-deGris"><span class="Calibri95NegritaRed">SECTION I:&nbsp; POTENTIAL REPLACEMENTS (Completed
  by Manager)</span></td>
    </tr>
    <tr style='page-break-inside:avoid;'>
  <td style="background:#E5E5E5;border:none;border-left:solid windowtext 1.0pt;" colspan="8" valign="top">
  <span class="Calibri9Negrita" >&nbsp;&nbsp;NAME  (Priority Order) </span> </td>
  <td colspan="11" style='width:18.3%;border-top:none;border-left:
  solid windowtext 1.0pt; background:#E5E5E5;border-bottom:none;border-right:solid windowtext 1.0pt;'>
  <span><b><span lang=EN-US
  style='text-transform:uppercase;mso-ansi-language:EN-US'><span class="Calibri9Negrita" >readiness</span></span></b></span><span
  style='mso-bookmark:Casilla1'><span style='font-size:9.0pt'></span></span>  </td>

  <td colspan="8" style=' background:#E5E5E5;width:19.28%;border:none;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt'>
 <span style='mso-bookmark:Casilla1'><b><span lang=EN-US
  style='font-size:9.0pt;text-transform:uppercase;mso-ansi-language:EN-US'><span class="Calibri9Negrita">availability</span></span></b></span><span
  ><span style='font-size:9.0pt'></span></span>  </td>
 
 </tr> 
 

                      <asp:Repeater ID="RepeaterPotencialReplacements1" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements1">
                <HeaderTemplate>
                    <tr style='page-break-inside:avoid; line-height: 23px;'>
                </HeaderTemplate>
                <ItemTemplate>
                            
                              <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "194" || Eval("ItemEvaluacionID").ToString()== "399" %>'  width="99%" colspan="8" style='width:62.14%;border:none;border-left:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:12.0pt'>
                              <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b>
                              <span lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                  <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                  <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                  </span></b></p>  </td>
                              <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "195" || Eval("ItemEvaluacionID").ToString()== "400" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:
                              solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt">
                              <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                               &nbsp;</o:p></span></span></p>  </td>
                   
                              <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "196" || Eval("ItemEvaluacionID").ToString()== "401" %>' width="19%" colspan=8 style='width:19.28%;border:none;border-right:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt'>
                              <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                              <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              

                   
                </ItemTemplate>
                <FooterTemplate>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements1" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="34" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>

  <asp:Repeater ID="RepeaterPotencialReplacements2" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements2">
                   <HeaderTemplate>
                    <tr style='mso-yfti-irow:42;page-break-inside:avoid; line-height: 23px;'>
                </HeaderTemplate>
                <ItemTemplate>
                            
                              <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "197" || Eval("ItemEvaluacionID").ToString()== "402" %>'  width="99%" colspan=8 style='width:62.14%;border:none;border-left:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:12.0pt'>
                              <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b><span
                              lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                  <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                  <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                  </span></b></p>  </td>
                              <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "198" || Eval("ItemEvaluacionID").ToString()== "403" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:
                              solid windowtext 1.0pt;border-bottom:none;border-right:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt">
                              <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                               &nbsp;</o:p></span></span></p>  </td>
                   
                              <span id="span1" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "198"  || Eval("ItemEvaluacionID").ToString()== "403" %>' style="mso-bookmark:Casilla1"></span>
                              <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "199" || Eval("ItemEvaluacionID").ToString()== "404"  %>' width="19%" colspan=8 style='width:19.28%;border:none;border-right:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt'>
                              <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                              <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              
                   
                </ItemTemplate>
                <FooterTemplate>
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements2" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="35" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>



         <asp:Repeater ID="RepeaterPotencialReplacements3" runat="server" DataSourceID="ObjectDataSourcePotencialReplacements3">
                             <HeaderTemplate>
                    <tr style='mso-yfti-irow:42;page-break-inside:avoid; line-height: 23px;'>
                </HeaderTemplate>
                <ItemTemplate>
                            
                              <td id="primerTDID" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "200"  || Eval("ItemEvaluacionID").ToString()== "405" %>'  width="99%" colspan=8 style='width:62.14%;border:none;border-left:solid windowtext 1.0pt;  border-bottom:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:12.0pt'>
                              <p class="MsoNormal" style="margin-left:18.0pt;text-indent:-18.0pt"><b><span
                              lang="EN-US" style="font-size:9.0pt;mso-ansi-language:EN-US" class="Calibri9">
                                  <asp:Label ID="LabelNumeration" runat="server" Text='<%# setNumeration(Eval("ItemEvaluacionID")) %>'></asp:Label> &nbsp; 
                                  <asp:Label ID="Label17" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>
                                  </span></b></p>  </td>
                              <td id="proserTDID2" runat="server" Visible='<%# Eval("ItemEvaluacionID").ToString()== "201" || Eval("ItemEvaluacionID").ToString()== "406" %>'   width="18%" colspan="11" style="width:18.3%;border-top:none;border-left:
                              solid windowtext 1.0pt; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt">
                              <p class="MsoNormal"><span style="mso-bookmark:Casilla1"><span style="font-size:9.0pt" class="Calibri9"><o:p>
                                <asp:Label ID="Label19" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>
                               &nbsp;</o:p></span></span></p>  </td>
                   
                              <td id="tdSegundo2" runat="server"  Visible='<%# Eval("ItemEvaluacionID").ToString()== "202" || Eval("ItemEvaluacionID").ToString()== "407" %>' width="19%" colspan=8 style='width:19.28%;border:none;border-right:solid windowtext 1.0pt;  border-bottom:solid windowtext 1.0pt;
                              padding:0cm 5.4pt 0cm 5.4pt;height:8.0pt'>
                              <p class=MsoNormal><span style='mso-bookmark:Casilla1'><span style='font-size:9.0pt' class="Calibri9">
                                <o:p> 
                              <asp:Label ID="Label20" runat="server" Text='<%# SetYear(Eval("Ponderacion")) %>' ></asp:Label>                  
                                &nbsp;</o:p></span></span></p>  </td>
        
                              
                              
                   
                </ItemTemplate>
                <FooterTemplate>
                
                    </tr>
                </FooterTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourcePotencialReplacements3" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="36" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
           
  </table>  
  </asp:Panel>
   </div>
       
        </asp:Panel>
          
    <asp:Panel ID="PanelResume" runat="server">

<p align="center" style="text-transform:uppercase;"><strong> 
    <asp:Label ID="lblNombreC" runat="server" Text=""></asp:Label>&nbsp; 
    <asp:Label ID="lblApellidoC" runat="server" Text=""></asp:Label></strong></p>
<p>&nbsp;</p>
<p><strong>EXPERIENCE</strong></p>

<!---->
<asp:Repeater ID="DataListExperience" runat="server" 
                    DataSourceID="ObjectDataSourceExperience" >
            <%--onitemcommand="DataListExperience_ItemCommand" >--%>
                   <%-- onitemdatabound="DataListExperience_ItemDataBound">--%>
             
             <HeaderTemplate><table id="DataListExperience"></HeaderTemplate>
                     <ItemTemplate>
                     <tr id="tridexperiencia" runat="server" Visible='<%# (Eval("Company").ToString() != "") %>' ><td class="exp_block">
                     <table width="100%">

             
                     <tr>
                       <td colspan="2"><strong><asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Company") %>'></asp:Label></strong></td>
                     </tr>

                     <tr>
                         <td width="35%">
                         <strong><span class="texto10">
                             <asp:HiddenField runat="server" ID="HiddenFieldID" Value='<%#(((RepeaterItem)Container).ItemIndex+1) %>' />
                             <asp:Label ID="Label21" runat="server" Text='<%# Eval("FromDate").ToString() + " " %>' ></asp:Label>
                                          </span>
                            <span class="texto10">To <asp:Label ID="lblTodata" runat="server" Text='<%# " " + Eval("ToDate").ToString() %>'></asp:Label>
                             </span>
                             </strong>
                            </td>
                          <td>
                          <span class="texto10">
                          <strong>
                             <asp:Label ID="lblPosition" runat="server" Text='<%# Eval("Position") %>'></asp:Label></strong></span>
                           </td>
                    
                      </tr>
                      <tr>
                      <td colspan="2">
                         <span class="texto10"> <asp:Label ID="lblJobDescriotion" runat="server" Text='<%# Eval("Description") %>'></asp:Label>    
                         </span>
                     </td>
         
                     </tr>
                     </table>
                     </td>
                     </tr>
                     </ItemTemplate>
             <FooterTemplate></table></FooterTemplate>
            </asp:Repeater>

            <asp:ObjectDataSource ID="ObjectDataSourceExperience" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetExperienceByTramiteID" 
                    TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                            PropertyName="Value" Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                            PropertyName="Value" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
<!---->


                 <asp:Repeater ID="RepeaterResume" runat="server" DataSourceID="ObjectDataSourceResume">
                        <HeaderTemplate>
                            <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%" id="RepeaterResume">
                        </HeaderTemplate>
                        <ItemTemplate>
                        <tr>
                        <td>
                        <table cellspacing="1" cellpadding="3" style="border-width: 0px;" border="0" width="100%">
                        
                            <tr>
                                <td>
                                    <h4><asp:Label ID="lblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label></h4>
                                    <asp:HiddenField ID="HFID" runat="server" Value='<%# Eval("ItemEvaluacionID") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>     
                                   <span class="texto10"> <asp:Label ID="lblComentarios" runat="server" Text='<%# Eval("Fundamentacion") %>' ></asp:Label>   </span>             
                                </td>
                            </tr>

                            </table>
                        </td>
                        </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                 </asp:Repeater>
                 <asp:ObjectDataSource ID="ObjectDataSourceResume" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="13" Name="SeccionID" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                                PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                                Type="Int32" />
                            <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                                PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                </asp:ObjectDataSource>

          <H1 class="SaltoDePagina"> </H1> 
</asp:Panel>

   <asp:Panel runat="server" ID="PanelCareerPlan">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="600" class="borde_inferior3"><span class="titulos_helvetica" style="font-size:16.0pt;mso-bidi-font-size:10.0pt">Career Plan</span></td>
      <td width="136" class="borde_inferior3"  align="center"><span class="subtitulos_helvetica12negrita" style="font-size:12.0pt;mso-bidi-font-size:10.0pt">
      <asp:Label runat="server" ID="LabelPeriodoFiscalDescripcion"></asp:Label></span></td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="35" bgcolor="#000000" class="subtitulos_helvetica14">1 - Career Plan (Employee and Supervisor)<%--<img src="Img/FondoNegro5.jpg" alt="titulo" width="736" height="34" />--%></td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="48" class="texto11times">Name</td>
      <td width="367" class="bordeinferior1_x"><span id="EvaluadorNombre"><span id="EvaluadorNombre"><span id="EvaluadorNombre">
          <asp:Label ID="lblNombreEvaluadoX" runat="server" Text=""></asp:Label> </span></span>&nbsp;
          <asp:Label ID="lblApellidoEvaluadoX" runat="server" Text=""></asp:Label>  </span></td>
      <td width="56" class="texto11times"><div align="center">Title</div></td>
      <td width="289" class="bordeinferior1_x"><span id="Cargo"> 
          <asp:Label ID="lblCargoX" runat="server" Text=""></asp:Label></span></td>
      <td width="116"><div align="center"><span class="texto11times" style="text-align:right"><span lang="EN-US" xml:lang="EN-US">Job Level</span></span></div></td>
      <td width="339" class="bordeinferior1_x"><span id="JobLevel"><asp:Label ID="lblJobLevelH" runat="server" Text="Label"></asp:Label></span></td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="27"><p>&nbsp;</p></td>
      <td width="709" class="subtitulos_helvetica12">Long Term:</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="texto11times">3-5 Year &ldquo;End in Mind&rdquo;. List 1-3 types of jobs and organizational levels that represent the career objective:</td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
<asp:Repeater ID="RepeaterCareerPlanLongTerm" runat="server" 
                DataSourceID="ObjectDataSourceCareerPlanLongTerm" >

                <ItemTemplate>

                    <asp:Label ID="Label24" runat="server" Text='<%# buildLongTerm(Eval("ItemEvaluacionID") , Eval("Titulo"), Eval("Fundamentacion"),Eval("Ponderacion")) %>' ></asp:Label>
               
                </ItemTemplate>

            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanLongTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="17" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
<!---->

    
    <tr>
      <td colspan="3" valign="bottom" height="25" bgcolor="#E5E5E5" class="texto11timesitalica" >This section to be prepared following an additional management
  review of the employee&rsquo;s long term career objectives. When completed,
  Supervisor and Employee sign and date.</i></td>
    </tr>
    <tr>
      <td colspan="3" valign="bottom" height="50" bgcolor="#E5E5E5" class="texto11timesitalica">&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="250" height="15" bgcolor="#E5E5E5" class="bordesuperior1">Supervisor</td>
      <td width="100" bgcolor="#E5E5E5" class="bordesuperior1">Date</td>
      <td width="286" bgcolor="#E5E5E5" class="bordesuperior1">Employee</td>
      <td width="100" bgcolor="#E5E5E5" class="bordesuperior1">Date</td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="27"><p>&nbsp;</p></td>
      <td width="709" class="subtitulos_helvetica12">Near Term:</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="texto11times">What is (are) the most logical assignment(s) in the near-term (0-2) years out?</td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <asp:Repeater ID="RepeaterCareerPlanNearTerm" runat="server" DataSourceID="ObjectDataSourceCareerPlanNearTerm">               
                <ItemTemplate>

                <asp:Label ID="Label24" runat="server" Text='<%# buildNearTerm(Eval("ItemEvaluacionID") , Eval("Titulo"), Eval("Fundamentacion"),Eval("Ponderacion")) %>' ></asp:Label>

                </ItemTemplate>
            </asp:Repeater>
         <asp:ObjectDataSource ID="ObjectDataSourceCareerPlanNearTerm" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="18" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
  </table>
  <br/>
  </asp:Panel>  
  <asp:Panel runat="server" ID="PanelFeedback">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="35" bgcolor="#000000" class="subtitulos_helvetica14">2 - Feedback<%--<img src="Img/FondoNegro6.jpg" alt="titulo" width="736" height="34" />--%></td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="368" class="texto10times">List the top three rated Key Success in last year.</td>
      <td valign="top" width="29" class="texto10times">&nbsp;</td>
      <td valign="top" width="339" class="texto10times">List performance ratings for last  three years.</td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <td width="50%">&nbsp;</td>
      
      <td  width="50%"></td>
  </tr>
  <tr>

  <td>
          <asp:Repeater ID="RepeaterFeedback" runat="server" DataSourceID="ObjectDataSourceFeedback">
            <HeaderTemplate>
                <table  width="100%" id="RepeaterFeedback">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                   
                    <td class="bordeinferior1_x"><asp:label ID="labelComentario" runat="server" Text='<%# Bind("Fundamentacion") %>' MaxLength="70"></asp:label>
                     
                   

                            
                            </td>
                </tr>
                   
            </ItemTemplate>
            <FooterTemplate>
                </table></FooterTemplate>
        </asp:Repeater>
        <asp:ObjectDataSource ID="ObjectDataSourceFeedback" runat="server" OldValuesParameterFormatString="original_{0}"
                SelectMethod="GetItemEvaluacion" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                <SelectParameters>
                    <asp:Parameter DefaultValue="12" Name="SeccionID" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" DefaultValue="" Name="PeriodoID"
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                        PropertyName="Value" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
  
  </td>
  <td>
  <asp:Repeater ID="RepeaterPerformanceRatings" runat="server" 
            DataSourceID="ObjectDataSourceLastRating" >
                <HeaderTemplate>
                    <table width="100%">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td  class="bordeinferior1_x">
                      <asp:Label ID="LabelPeriodo" runat="server" Text='<%# Periodo(Eval("PeriodoID")) %>'></asp:Label>
                      <asp:Label ID="LabelCalificacion" runat="server" Text='<%# DefaultVal(Eval("Calificacion")) %>'></asp:Label> 
                      <asp:Label ID="Label12" runat="server" Text='<%# GetTextRating((((RepeaterItem)Container).ItemIndex+1)) %>'></asp:Label>
                      </td>
                    </tr>
                   
                </ItemTemplate>
                <FooterTemplate>
                    </table></FooterTemplate>
            </asp:Repeater>
              <asp:ObjectDataSource ID="ObjectDataSourceLastRating" runat="server" 
                OldValuesParameterFormatString="original_{0}" 
                SelectMethod="GetLastRatings" 
                TypeName="com.paginar.formularios.businesslogiclayer.FormulariosFYController">
                <SelectParameters>
                    <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" 
                        PropertyName="Value" Type="Int32" />
                    <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="periodoActual" 
                        PropertyName="Value" Type="Int32" />
                        <asp:Parameter DefaultValue="3" Type="Int32" Name="TipoPeriodoID" />
                </SelectParameters>
            </asp:ObjectDataSource>
  </td>
  
  </tr>
  </table>

  
  
  <br />
  <br />
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="texto10timesitalica">Please sign to verify that this sheet accurately reflects Supervisor/Employee input. It will be used in subsequent career objective discussions/MS&amp;D forums.</td>
    </tr>
  </table>
  <br/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="291" class="bordeinferior1_x">&nbsp;</td>
      <td width="80" class="bordeinferior1_x">&nbsp;</td>
      <td width="17">&nbsp;</td>
      <td width="268" class="bordeinferior1_x">&nbsp;</td>
      <td width="80" class="bordeinferior1_x">&nbsp;</td>
    </tr>
    <tr>
      <td class="texto10times">Supervisor</td>
      <td class="texto10times">Date</td>
      <td>&nbsp;</td>
      <td class="texto10times">Employee</td>
      <td class="texto10times">Date</td>
	</tr>
</table>	
    </asp:Panel>


   
      
    </form>

   </div>
</body>
</html>
