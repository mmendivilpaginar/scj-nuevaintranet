﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImpresionFY.aspx.cs" Inherits="com.paginar.johnson.Web.PMPFY.ImprimirFY" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <link rel="stylesheet" type="text/css" href="css/styles.css" />
  <link rel="stylesheet" type="text/css" href="css/print.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
          <asp:HiddenField ID="HiddenFieldPeriodoID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldTipoFormularioID" Value="" runat="server" />
         <asp:HiddenField ID="HiddenFieldlegajo" Value="" runat="server" />
         
         <asp:FormView ID="FVC" runat="server" DataSourceID="ObjectDataSourceCabecera" 
             BorderStyle="Dotted">
            <ItemTemplate>
               <div id="header">
                  <!--<div id="headerBG"></div>-->
                  <div id="header-inner">
                     
                     <div id="logo-title">
                        <div id="logo">
                           <img id="logo-image" alt="Logo Johnson " src="img/logoprint.jpg" /></div>
                        <div id="site-name">
                           <h1>Evaluación de Desempeño <span><asp:Literal ID="TipoFormularioLabel" runat="server" Text='<%# Bind("TipoFormulario") %>' /></span></h1>
                        </div>
                     </div>
                     
                     <div id="datos">
                        
                        <ul id="fechas">
                           <li>Período de Evaluación: <asp:Literal ID="Label1" runat="server" Text='<%# Bind("Periodo") %>' /></li>
                           <li>Fecha de Inicio del PMP: <asp:Literal ID="EvaluacionFechaLabel" runat="server" Text='<%# Bind("EvaluacionFecha", "{0:dd/MM/yyyy}") %>' /></li>
                        </ul>
                        
                        <!--datos de Evaluador y Evaluado-->
                        <table width="100%">
                           <tr>
                              <td valign="top">
                                 <div id="personas">
                                    <div id="personas-inner">
                                       <table width="100%">
                                          <tr>
                                             <td>
                                                <div class="evaluador">
                                                   <h2>Evaluador</h2>
                                                   <table>
                                                      <tr>
                                                         <td><strong>Nombre y Apellido: </strong><asp:Literal ID="EvaluadorNombreLabel" runat="server" Text='<%# Bind("EvaluadorNombre") %>' /></td>
                                                         <td><strong>Legajo: </strong><asp:Literal ID="EvaluadorLegajoLabel" runat="server" Text='<%# Bind("EvaluadorLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Área: </strong><asp:Literal ID="Literal1" runat="server" Text='<%# Bind("EvaluadorArea") %>' /></td>
                                                         <td><strong>Cargo: </strong><asp:Literal ID="Literal2" runat="server" Text='<%# Bind("EvaluadorCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                             
                                             <td>
                                                <div class="evaluado">
                                                   <h2>Evaluado</h2>
                                                   <table>
                                                      <tr>
                                                         <td><strong>Nombre y Apellido: </strong><asp:Literal ID="Label2" runat="server" Text='<%# Bind("EvaluadoNombre") %>' /></td>
                                                         <td><strong>Legajo: </strong><asp:Literal ID="EvaluadoLegajoLabel" runat="server" Text='<%# Bind("EvaluadoLegajo") %>' /></td>
                                                      </tr>
                                                      <tr>
                                                         <td><strong>Área: </strong><asp:Literal ID="EvaluadoSectorLabel" runat="server" Text='<%# Bind("EvaluadoArea") %>' /></td>
                                                         <td><strong>Cargo: </strong><asp:Literal ID="Literal3" runat="server" Text='<%# Bind("EvaluadoCargo") %>' /></td>
                                                      </tr>                                                      
                                                   </table>
                                                </div>                                             
                                             </td>
                                          </tr>
                                       </table>
                                    </div>
                                 </div>                              
                              </td>
                              <td valign="top">
                                 <div id="foto">
                                    <asp:Image ID="Image1" ToolTip='<%# Bind("EvaluadoNombre") %>' AlternateText='<%# Bind("EvaluadoNombre") %>' ImageUrl='<%# Bind("EvaluadoFoto") %>' runat="server" />
                                 </div>                              
                              </td>
                           </tr>
                        </table>

                        <div id="calificacion">
                           <h2> <span id="SpanCalificacion"><%--<asp:Literal ID="TextCalificacion" runat="server" Text='<%# Bind("Calificacion") %>' />--%></span></h2>
                        </div>
                     </div><!--/datos-->
                     
                     <div id="acciones">                        
                        <asp:Button ID="btmExportar" runat="server" Text="Exportar" onclick="btnExportar_Click" SkinID="SubmitExportar" class="hidebutton"/>
                        <input type="Button" name="Capture" value="Imprimir" onclick="Imprimir()" class="hidebutton" />
                        <input type="Button" value="Cerrar" onclick="window.close()" class="hidebutton" />
                     </div>
                     
                  </div><!--/header-inner-->
               </div><!--/header-->
               
               <!--INICIO - ModalPopup Evaluaciones anteriores-->
               <asp:Panel ID="PanelFA" CssClass="contentPopup" Style="display: none" runat="server">
                  <asp:Panel CssClass="titleModalPopup" ID="titleBarEvalAnt" runat="server">
                     <h2>Evaluaciones anteriores</h2>
                     <span id="btnOkay" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <ul>
                        <asp:Repeater ID="RFA" runat="server" DataSourceID="ObjectDataSourceFormulariosAnteriores">
                           <ItemTemplate>
                              <li>
                                 <asp:LinkButton Text='<%# Convert.ToString(Eval("Fecha","{0:dd/MM/yyyy}"))+" "+Convert.ToString(Eval("Calificacion")) %>'
                                    ID="LinkButtonFormu" runat="server">LinkButton</asp:LinkButton></li>
                           </ItemTemplate>
                           <AlternatingItemTemplate>
                           </AlternatingItemTemplate>
                           <FooterTemplate>
                              <asp:Label ID="lblEmpty" Text="El usuario no cuenta con evaluaciones anteriores" runat="server"
                                 Visible='<%# bool.Parse((((Repeater)FVC.FindControl("RFA")).Items.Count==0).ToString())%>'>
                              </asp:Label>
                           </FooterTemplate>
                        </asp:Repeater>
                     </ul>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceFormulariosAnteriores" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorialByLegajo" TypeName="com.paginar.formularios.businesslogiclayer.DashboardController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="Legajo" PropertyName="Value"
                           Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Evaluaciones anteriores-->
               <asp:Panel CssClass="contentPopup" ID="PanelH" Style="display: none" runat="server">
                  <asp:Panel ID="Panel1" CssClass="titleModalPopup" runat="server">
                     <h2>
                        Historial</h2>
                     <span id="btnOkayHistorial" class="closeModalPopup">Cerrar</span>
                  </asp:Panel>
                  <div class="contentModalPopup">
                     <asp:GridView ID="GridViewHistorial" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSourceHistorial">
                        <Columns>
                           <asp:BoundField DataField="Estado" HeaderText="Estado" ReadOnly="True" SortExpression="Estado" />
                           <asp:BoundField DataField="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha"
                              SortExpression="Fecha" />
                           <asp:BoundField DataField="Usuario" HeaderText="Usuario" ReadOnly="True" SortExpression="Usuario" />
                        </Columns>
                        <EmptyDataTemplate>
                           <asp:Label ID="Label3" runat="server" Text="El formulario actual no tiene registros en el historial."></asp:Label>
                        </EmptyDataTemplate>
                     </asp:GridView>
                  </div>
                  <asp:ObjectDataSource ID="ObjectDataSourceHistorial" runat="server" OldValuesParameterFormatString="original_{0}"
                     SelectMethod="GetHistorial" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
                     <SelectParameters>
                        <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                           Type="Int32" />
                        <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                           PropertyName="Value" Type="Int32" />
                     </SelectParameters>
                  </asp:ObjectDataSource>
               </asp:Panel>
               <!--FIN - ModalPopup Historial-->
            </ItemTemplate>
         </asp:FormView>
         
         <asp:ObjectDataSource ID="ObjectDataSourceCabecera" runat="server" OldValuesParameterFormatString="original_{0}"
            SelectMethod="GetCabecera" TypeName="com.paginar.formularios.businesslogiclayer.FormulariosController">
            <SelectParameters>
               <asp:ControlParameter ControlID="HiddenFieldPeriodoID" Name="PeriodoID" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldlegajo" Name="legajo" PropertyName="Value"
                  Type="Int32" />
               <asp:ControlParameter ControlID="HiddenFieldTipoFormularioID" Name="TipoFormularioID"
                  PropertyName="Value" Type="Int32" />
                <asp:Parameter Name="LegajoEvaluador" Type="Int32" />
            </SelectParameters>
         </asp:ObjectDataSource>
        

    </form>
</body>
</html>
