﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.johnson.BL;
using com.paginar.johnson.membership;
using com.paginar.johnson.DAL;
using com.paginar.johnson.Web.PMP;
using com.paginar.formularios.dataaccesslayer;
using com.paginar.johnson.Web.PMPFY.MasterPages;
using System.Threading;



namespace com.paginar.johnson.Web.PMPFY
{
    public partial class Form_PMPFY : PageBase
    {
        private FormulariosController _fc;
        public FormulariosController FC
        {
            get
            {
                _fc = (FormulariosController)this.Session["fc"];
                if (_fc == null)
                {
                    _fc = new FormulariosController();
                    this.Session["fc"] = _fc;
                }
                return _fc;
            }
            set
            {
                this.Session["fc"] = value;

            }
        }
        void omb_OkButtonPressed(object sender, EventArgs args)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "self.close();", true);
        }

        protected string incrementarUnAnio(object a)
        {
            int retorno;
            retorno = int.Parse(a.ToString()) + 1;
            return retorno.ToString();
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            omb.OkButtonPressed += new pmp_OKMessageBox.OkButtonPressedHandler(omb_OkButtonPressed);
            WUCGuardadoPrevio.GuardadoPrevio += new pmp_WebUserControlGuardadoPrevio.GuardadoPrevioHandler(WUCGuardadoPrevio_GuardadoPrevio);
            int? PasoActualID; 
            
            if (!Page.IsPostBack)
            {

                setearID();
               // ListBoxNecesidades.Attributes.Add("onChange", "FiltrarListboxCursos()");

                if (string.IsNullOrEmpty(HiddenFieldlegajo.Value) || string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value) || string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                    Response.Redirect("EvaluacionError.aspx");                

                
                FormulariosController f = new FormulariosController();
                FormulariosFYController fYc = new FormulariosFYController();
                

                if (!f.ExisteEvaluacion(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value)))
                    Response.Redirect("EvaluacionError.aspx");

                PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
                HiddenFieldPasoID.Value = PasoActualID==-1 ? "2" : PasoActualID.ToString();


                if (!puedeeditar())
                    Response.Redirect("~/UnauthorizedAccess.aspx");


                llenarDebilidadesCursos();
               


                //pop up evaluador
                if (PasoActualID == -1)
                {
                    fillObjetives();
                    PanelPopEvaluador_ModalPopupExtender.Show();
                    EnviarEvaluacion.Visible = false;
                    GuardarNuevoEvaluador.Visible = true;
                    WUCGuardadoPrevio.EnabledTimer = false;
                }

                else
                {
                    EnviarEvaluacion.Visible = true;
                    GuardarNuevoEvaluador.Visible = false;
                
                }

                 

                HtmlGenericControl BodyPmp = this.Master.FindControl("BodyPmp") as HtmlGenericControl;
               // BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.reload();");
                BodyPmp.Attributes.Add("onunload", "if (!window.opener.closed)window.opener.location.replace('/servicios/form_evaldesemp.aspx');");


                SetearSeccionesVisibles(PasoActualID);

               
                    


            }

        }

        protected void fillObjetives()
        {
            RepeaterIndividualObjetives1.DataBind();
            RepeaterIndividualObjetives2.DataBind();
            RepeaterIndividualObjetives3.DataBind();
            RepeaterIndividualObjetives4.DataBind();
            RepeaterIndividualObjetives5.DataBind();
        
        }


        protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("--");
            else
                return (val.ToString());

        }

        private void SetearSeccionesVisibles(int? PasoActualID)
        {
            if (PasoActualID == 2 || PasoActualID == -1)
            {
                SectionF_NextAssignment.Attributes.Add("Style", "display:none");
                SectionG_DevelopmentSummary.Attributes.Add("Style", "display:none");
                SectionI_PotencialReplacements.Attributes.Add("Style", "display:none");

                ButtonDevolverEvaluado.Visible = false;
                ButtonAprobarEvaluacion.Visible = false;
            }
            else
             ButtonEnviarEvaluador.Visible = false;
            if (int.Parse(HiddenFieldTipoFormularioID.Value) == 11)
            {
                RepeaterCareerPlanLongTerm.Visible = false;
                t8.Visible = false;
                t9.Visible = false;
                tab8x.Visible = false;
                //DropDownListImprimirSeccion.Items.Remove("CareerPlan");
            }
            else
            {
                ListItem L = new ListItem();
                L.Text = "Career Plan";
                L.Value = "CareerPlan";
                DropDownListImprimirSeccion.Items.Add(L);
            
            }

            if (int.Parse(HiddenFieldTipoFormularioID.Value) == 10)
            {
                RepeaterSeccionBPerformanceRatings.Visible = false;
                DataListEducation.Visible = false;
                RepeaterLanguage.Visible = false;
                DropDownListImprimirSeccion.Items.RemoveAt(2);
            }
        }

        private void llenarDebilidadesCursos()
        {

            FormulariosController f = new FormulariosController();
            FormulariosFYController fYc = new FormulariosFYController();

            //string Debilidades = string.Empty;
            //FormulariosDS.ItemEvaluacionDataTable DTI = new FormulariosDS.ItemEvaluacionDataTable();
            //DTI = f.GetItemEvaluacion(16, int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
            //foreach (FormulariosDS.ItemEvaluacionRow DR in DTI.Rows)
            //{
            //    Debilidades = Debilidades + DR["Ponderacion"].ToString() + ",";

            //}
            //HiddenFieldDebilidadesSeleccionadas.Value = Debilidades.Trim(',');

            //CursosSeleccionados
            FormulariosDS.CursosDataTable CDRCT = new FormulariosDS.CursosDataTable();
            CDRCT = f.GetCursosByTramiteID(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value));

            string CursosSeleccionados = string.Empty;
            foreach (FormulariosDS.CursosRow DR in CDRCT.Rows)
            {
                ListItem LI = new ListItem();
                LI.Text = DR["CursoDescripcion"].ToString();
                LI.Value = DR["CursoID"].ToString();
               // LI.Attributes.Add("Deb", DR["DebilidadID"].ToString());
                ListBoxCursosSeleccionados.Items.Add(LI);

                CursosSeleccionados = CursosSeleccionados + DR["CursoID"].ToString() + ",";
            }

            HiddenFieldCursosSeleccionados.Value = CursosSeleccionados.Trim(',');

            //Cursos disponibles
            FormulariosDS.CursosDataTable CDT = new FormulariosDS.CursosDataTable();
            CDT = f.GetCursosDisponiblesByTramiteID(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value));

            foreach (FormulariosDS.CursosRow DR in CDT.Rows)
            {
                ListItem LI = new ListItem();
                LI.Text = DR["CursoDescripcion"].ToString();
                LI.Value = DR["CursoID"].ToString();
               // LI.Attributes.Add("Deb", DR["DebilidadID"].ToString());
                ListBoxCursosDisponibles.Items.Add(LI);
            }

        }

        private void setearID()
        {
            HiddenFieldlegajo.Value = (!string.IsNullOrEmpty(Request.QueryString["Legajo"])) ? Request.QueryString["Legajo"] : string.Empty;
            HiddenFieldPeriodoID.Value = (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"])) ? Request.QueryString["PeriodoID"] : string.Empty;
            HiddenFieldTipoFormularioID.Value = (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"])) ? Request.QueryString["TipoFormularioID"] : string.Empty;
        }

        private bool puedeeditar()
        {
            int LegajoLog = 0;
            int? LegajoEvaluador;
            User Usuario = null;

            Usuarios usercontroller = new Usuarios();
            Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);

            LegajoLog = Usuario.GetLegajo();

            LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);

            int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));


            return ((PasoActualID == 2 || PasoActualID == -1) && LegajoLog == int.Parse(HiddenFieldlegajo.Value)) ||
                   (LegajoLog == LegajoEvaluador && (PasoActualID == 1 ));
        }


        public string Periodo(object PeriodoID)
        {
            string retorno = FC.GetPeriodoPMPMY(int.Parse(PeriodoID.ToString()));
            return retorno;

        }



       

        void WUCGuardadoPrevio_GuardadoPrevio()
        {
            EjecutarScript("$('.button').each(function () { if ($(this).html() == '')  $(this).remove();});");
            GuardadoPrevio();
        }



        private void EjecutarScript(string js)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this);
            if (sm.IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "IF100", js, true);
            else
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "VMSwapScript", js, true);


        }

        protected void RepeaterSuccessFactors_ItemDataBound(object sender, RepeaterItemEventArgs e)
         {
             if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
             {

                 CustomValidator CustomValidatorSuccessFactors = e.Item.FindControl("CustomValidatorSuccessFactors") as CustomValidator;
                 DropDownList DropDownListOptional = e.Item.FindControl("DropDownListOptional") as DropDownList;
                 DropDownList DropDownListRating = e.Item.FindControl("DropDownListRating") as DropDownList;
                 Label lblTitulo = e.Item.FindControl("lblTitulo") as Label;

                 if (DropDownListOptional.Visible == true)
                 {
                     CustomValidatorSuccessFactors.Attributes.Add("DropDownListRating", "#" + DropDownListRating.ClientID);
                     CustomValidatorSuccessFactors.Attributes.Add("DropDownListOptional", "#" + DropDownListOptional.ClientID);
                 }
                 else
                 {
                     CustomValidatorSuccessFactors.Attributes.Add("DropDownListRating", "#" + DropDownListRating.ClientID);
                 }

                 CustomValidatorSuccessFactors.ErrorMessage = "PERFORMANCE REVIEW: No completó el campo: " + lblTitulo.Text;
             }
        }



        
        protected void DataListEducation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorEducation = e.Item.FindControl("CustomValidatorEducation") as CustomValidator;
                DropDownList DropDownListDegree = e.Item.FindControl("DropDownListDegree") as DropDownList;
                TextBox TextBoxYear = e.Item.FindControl("TextBoxYear") as TextBox;
                TextBox TextBoxSchool = e.Item.FindControl("TextBoxSchool") as TextBox;
                HiddenField HiddenFieldDegree = e.Item.FindControl("HiddenFieldDegree") as HiddenField;
                HiddenField HiddenFieldYear = e.Item.FindControl("HiddenFieldYear") as HiddenField;
                HiddenField HiddenFieldID = e.Item.FindControl("HiddenFieldID") as HiddenField;

                if(HiddenFieldYear.Value.Length>4)
                   TextBoxYear.Text = HiddenFieldYear.Value.Substring(HiddenFieldYear.Value.Length - 4);
                else
                    TextBoxYear.Text = HiddenFieldYear.Value;


                CustomValidatorEducation.Attributes.Add("DropDownListDegree", "#" + DropDownListDegree.ClientID);
                CustomValidatorEducation.Attributes.Add("TextBoxYear", "#" + TextBoxYear.ClientID);
                CustomValidatorEducation.Attributes.Add("TextBoxSchool", "#" + TextBoxSchool.ClientID);
                CustomValidatorEducation.Attributes.Add("HiddenFieldID", "#" + HiddenFieldID.ClientID);

                


                if (DropDownListDegree.Items.FindByValue(HiddenFieldDegree.Value) != null)
                    DropDownListDegree.SelectedValue = HiddenFieldDegree.Value;
                else
                    DropDownListDegree.SelectedValue = "";
            }

        }


        protected void RepeaterLanguage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorLanguage = e.Item.FindControl("CustomValidatorLanguage") as CustomValidator;
                DropDownList DropDownListProficiency = e.Item.FindControl("DropDownListProficiency") as DropDownList;
                TextBox TextBoxIdioma = e.Item.FindControl("TextBoxIdioma") as TextBox;
                CheckBox CheckBoxNative = e.Item.FindControl("CheckBoxNative") as CheckBox;
                HiddenField HiddenFieldNumber = e.Item.FindControl("HiddenFieldNumber") as HiddenField;


                
                CustomValidatorLanguage.Attributes.Add("CheckBoxNative", "#" + CheckBoxNative.ClientID);
                CustomValidatorLanguage.Attributes.Add("DropDownListProficiency", "#" + DropDownListProficiency.ClientID);
                CustomValidatorLanguage.Attributes.Add("TextBoxIdioma", "#" + TextBoxIdioma.ClientID);
                CustomValidatorLanguage.Attributes.Add("HiddenFieldNumber", "#" + HiddenFieldNumber.ClientID);
                
            }

        }

        protected void RepeaterPerformanceReviewSummary_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList DropDownListRating = (DropDownList)e.Item.FindControl("DropDownListRating");
                if (DropDownListRating.Visible == true)
                    DropDownListRating.Attributes.Add("onChange", "SetLastRating('" + DropDownListRating.ClientID.ToString() + "')");
            }

        }

        protected void RepeaterDevelopmentNeeds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
          //  if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
          //  {
              //  DropDownList DropDownListRating = (DropDownList)e.Item.FindControl("DropDownListRating");
               // if (DropDownListRating.Visible == true)
                   // DropDownListRating.Attributes.Add("onChange", "LlenarListboxCursosDisponibles()");
                 //   DropDownListRating.Attributes.Add("onpropertychange", @"if ( event.propertyName == 'selectedIndex' ) LlenarListboxCursosDisponibles(this);");
                   // DropDownListRating.Attributes.Add("onclick", "selectList_cache=this.value;");

              //  CustomValidator CustomValidatorCursosSeleccionados = (CustomValidator)e.Item.FindControl("CustomValidatorCursosSeleccionados");
              //  CustomValidatorCursosSeleccionados.Attributes.Add("DropDownListRating", "#" + DropDownListRating.ClientID);
         //   }


                
        }


        protected void RepeaterMobilityAndTravel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HFID = (HiddenField)e.Item.FindControl("HFID");
                HiddenField HiddenFieldPonderacion = (HiddenField)e.Item.FindControl("HiddenFieldPonderacion");

                DropDownList DropDownListRating = (DropDownList)e.Item.FindControl("DropDownListRating");
                if (HFID.Value == "185" || HFID.Value == "390")
                {
                    DropDownListRating.Items.Add(new ListItem("Not Mobile", "1"));
                    DropDownListRating.Items.Add(new ListItem("Domestic Only", "2"));
                    DropDownListRating.Items.Add(new ListItem("International Only", "3"));
                    DropDownListRating.Items.Add(new ListItem("Domestic and International", "4"));
                    DropDownListRating.SelectedValue = HiddenFieldPonderacion.Value;

                }

                if (HFID.Value == "186" || HFID.Value == "391")
                {
                    DropDownListRating.Items.Add(new ListItem("Yes", "1"));
                    DropDownListRating.Items.Add(new ListItem("No", "2"));
                    DropDownListRating.SelectedValue = HiddenFieldPonderacion.Value;
                }

            }

        }

        protected void DataListExperience_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CustomValidator CustomValidatorExperience = e.Item.FindControl("CustomValidatorExperience") as CustomValidator;
                CustomValidator CustomValidatorJobDescription = e.Item.FindControl("CustomValidatorJobDescription") as CustomValidator;
                
                
                TextBox TextBoxToDate = e.Item.FindControl("TextBoxToDate") as TextBox;
                TextBox TextBoxFromDate = e.Item.FindControl("TextBoxFromDate") as TextBox;
                TextBox TextBoxCompany = e.Item.FindControl("TextBoxCompany") as TextBox;
                TextBox TextBoxPosition = e.Item.FindControl("TextBoxPosition") as TextBox;
                TextBox TextBoxDescription = e.Item.FindControl("TextBoxDescription") as TextBox;
                HiddenField HiddenFieldID = e.Item.FindControl("HiddenFieldID") as HiddenField;

                CustomValidatorExperience.Attributes.Add("TextBoxToDate", "#" + TextBoxToDate.ClientID);
                CustomValidatorExperience.Attributes.Add("TextBoxFromDate", "#" + TextBoxFromDate.ClientID);
                CustomValidatorExperience.Attributes.Add("TextBoxCompany", "#" + TextBoxCompany.ClientID);
                CustomValidatorExperience.Attributes.Add("TextBoxPosition", "#" + TextBoxPosition.ClientID);
                CustomValidatorExperience.Attributes.Add("TextBoxDescription", "#" + TextBoxDescription.ClientID);
                CustomValidatorExperience.Attributes.Add("HiddenFieldID", "#" + HiddenFieldID.ClientID);


                CustomValidatorJobDescription.Attributes.Add("TextBoxDescription", "#" + TextBoxDescription.ClientID);
                CustomValidatorJobDescription.Attributes.Add("HiddenFieldID", "#" + HiddenFieldID.ClientID);




            }
        }

        protected void RepeaterDEVELOPMENTACTIONS_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox TextBoxComentario = (TextBox)e.Item.FindControl("TextBoxComentario");
                TextBoxComentario.Attributes.Add("onkeyup", "SetDEVELOPMENTACTIONSinMSD(this.value)");
            }

        }

        protected void RepeaterCareerPlanLongTerm_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox TextBoxOther = (TextBox)e.Item.FindControl("TextBoxOther");
                DropDownList DropDownListRating = e.Item.FindControl("DropDownListRating") as DropDownList;
                CustomValidator CustomValidatorCareerPlanOther = e.Item.FindControl("CustomValidatorCareerPlanOther") as CustomValidator;


                CustomValidatorCareerPlanOther.Attributes.Add("DropDownListRating", "#" + DropDownListRating.ClientID);
                CustomValidatorCareerPlanOther.Attributes.Add("TextBoxOther", "#" + TextBoxOther.ClientID);

                DropDownListRating.Attributes.Add("onChange", "MostrarOtraAreaDeseada(this.value, '#" + TextBoxOther.ClientID + "')");

                if (DropDownListRating.SelectedItem.Text == "Other")
                    TextBoxOther.Attributes.Add("Style", "display: display;");
               else
                    TextBoxOther.Attributes.Add("Style", "display: none;");

                HiddenField HFID= (HiddenField)e.Item.FindControl("HFID");

                HiddenField HiddenFieldTitulo = (HiddenField)e.Item.FindControl("HiddenFieldTitulo");

                 


                TextBox TextBoxComentario = (TextBox)e.Item.FindControl("TextBoxComentario");

                if (TextBoxComentario.Visible)

                {
                    


                    if ( (HiddenFieldTitulo.Value.ToString().ToUpper().IndexOf("SUPERVISOR")>=0 && HiddenFieldPasoID.Value == "1")  ||
                         (HiddenFieldTitulo.Value.ToString().ToUpper().IndexOf("EMPLOYEE")>=0 && HiddenFieldPasoID.Value == "2") )
                        
                        
                        TextBoxComentario.Enabled = true;
                    else
                        TextBoxComentario.Enabled = false;

                }
            }

        }



        //seleccione evaluador

        #region SeleccioneEvaluador

        protected void DropDownEvaluadores_DataBound(object sender, EventArgs e)
        {
            int? PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));


            if (PasoActualID != -1 || sender==null)
            {
                int? LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);

                if (DropDownEvaluadores.Items.FindByValue(LegajoEvaluador.ToString()) != null)
                {

                    DropDownEvaluadores.SelectedValue = LegajoEvaluador.ToString();
                    lblEvaluadorAsignado.Text = "Su evaluador seleccionado es: " + DropDownEvaluadores.SelectedItem.Text;
                }
                else
                    lblEvaluadorAsignado.Text = "Su evaluador seleccionado ya no es válido, deberá seleccionar un evaluador habilitado";
            }
        }

        protected void GuardarNuevoEvaluador_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                
                FC.ActualizarEvaluador(DropDownEvaluadores.SelectedItem.Value, HiddenFieldlegajo.Value, HiddenFieldPeriodoID.Value, HiddenFieldTipoFormularioID.Value, 1);

                ObjectDataGetEvaluators.DataBind();
                DropDownEvaluadores.Items.Clear();
                DropDownEvaluadores.Items.Add(new ListItem("-Seleccione-", "-Seleccione-"));
                DropDownEvaluadores.DataBind();


                //llenarDebilidadesCursos();
                GuardarNuevoEvaluador.Visible = false;
                EnviarEvaluacion.Visible = true;

                (this.Master as IMasterPMPFY).setHiddenEvaluador(DropDownEvaluadores.SelectedItem.Value);

                (this.Master as IMasterPMPFY).bindFVC();
                //               

                if(sender!=null)
                  WUCGuardadoPrevio.EnabledTimer = true;

                DropDownEvaluadores_DataBound(null, null);

            }
        }

        #endregion



        #region AccionesFormulario
        protected void ButtonGSinEnviar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                EjecutarScript("GuardarSinEnviar()");
            }

        }

        private void GuardadoPrevio()
        {

            int legajo = 0;
            int PeriodoID = 0;
            int TipoFormularioID = 0;
            int LegajoEvaluador = 0;

            HiddenField HiddenFieldLegajoEvaluador = (HiddenField)this.Master.FindControl("HiddenFieldLegajoEvaluador");

            if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
            {
                legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
            {
                PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
            }
            if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
            {
                TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
            }


            FormulariosDS.RelEvaluadosEvaluadoresDataTable RelEvaluadosEvaluadoresDt = new FormulariosDS.RelEvaluadosEvaluadoresDataTable();
            FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByFormularioID(RelEvaluadosEvaluadoresDt, legajo, PeriodoID, TipoFormularioID);
            RelEvaluadosEvaluadoresDt.Select("PasoID=2");
            LegajoEvaluador = int.Parse(RelEvaluadosEvaluadoresDt.Rows[0]["LegajoEvaluador"].ToString());

            //Asociar evaluador si el mismo es null
            //if (string.IsNullOrEmpty(RelEvaluadosEvaluadoresDt.Rows[0]["LegajoEvaluador"].ToString()))
            //{
            //    FC.AsociarEvaluacion(legajo, PeriodoID, TipoFormularioID, 1, LegajoEvaluador);
            //    //Limpio Hiddden para q no lo tome despues en el envio y lo asociie nuevamente
            //    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
            //    NotificacionesController n = new NotificacionesController();



            //}
            
            FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

            try
            {

                GuardarDatosFormulario();

                FC.GrabarEvaluadoFY();
                
                FC.SaveHistorial(int.Parse(HiddenFieldPasoID.Value), PeriodoID, legajo, TipoFormularioID);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void ButtonEnviarEvaluador_Click(object sender, EventArgs e)
        {

            FormulariosDS.HistorialDataTable DTH = new FormulariosDS.HistorialDataTable();
            DTH=FC.GetHistorial(int.Parse(HiddenFieldPeriodoID.Value),int.Parse(HiddenFieldlegajo.Value),int.Parse(HiddenFieldTipoFormularioID.Value));

            if(DTH.Rows.Count>1)
            {
            EnviarEvaluacion_Click(null,null);
            }
            else
            {

            GuardarNuevoEvaluador.Visible = false;
            EnviarEvaluacion.Visible = true;
            PanelPopEvaluador_ModalPopupExtender.Show();
            WUCGuardadoPrevio.EnabledTimer = false;
            
            }
        }

        protected void EnviarEvaluacion_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                GuardarNuevoEvaluador_Click(null,null);
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;

                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

                UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                NotificacionesController n = new NotificacionesController();
                int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

                /////El evaluador esta deshabilitado? se muestra nuevamente el popup
                //ControllerUsuarios ControllerUsuarios1 = new ControllerUsuarios();
                //DSUsuarios.UsuarioDataTable Evaluador = new DSUsuarios.UsuarioDataTable();

                //int EstadoEvaluador = 0;

                //Evaluador = ControllerUsuarios1.GetUsuariosByLegajo(legajoevaluador);


                //foreach (DSUsuarios.UsuarioRow EvaluadorRow in Evaluador.Rows)
                //{
                //    EstadoEvaluador = EvaluadorRow.Estado;
                //}

                //if (EstadoEvaluador != 2)
                //{
                //    WUCGuardadoPrevio.EnabledTimer = false;
                //    ObjectDataGetEvaluators.DataBind();
                //    DropDownEvaluadores.Items.Clear();
                //    DropDownEvaluadores.Items.Add(new ListItem("-Seleccione-", ""));
                //    DropDownEvaluadores.DataBind();
                //    PanelPopEvaluador_ModalPopupExtender.Show();
                //    return;
                //}

                //////////


                try
                {
                    WUCGuardadoPrevio.EnabledTimer = false;

                    GuardarDatosFormulario();

                    FC.GrabarEvaluadoFY();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);
                    FC.SaveHistorial(1, PeriodoID, legajo, TipoFormularioID);



                    string mailEvaluador = UE.GetMail(legajoevaluador);
                    string envio;
                    string URLPMP;

                    URLPMP = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=FY&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];

                    if (!string.IsNullOrEmpty(mailEvaluador))
                        envio = n.envioEvaluadorFY(mailEvaluador, UE.GetUsuarioNombre(legajo), URLPMP);

                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");
                    EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));



                }
                catch (Exception exc)
                {

                    omb.ShowMessage("Ha ocurrido Un error. Intentar Nuevamente.");
                    // omb.ShowMessage(exc.Message);
                }

            }

        }

        protected void ButtonDevolverEvaluado_Click(object sender, EventArgs e)
        {

            //Thread.Sleep(1);
            if (Page.IsValid)
            {
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }

              
                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);
                
                try
                {
                    GuardarDatosFormulario();
                    FC.GrabarEvaluadorFY();                   
                    FC.GrabarEvaluador();
                    FC.SaveHistorial(2, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

                    string mailEvaluado = UE.GetMail(legajo);
                    string envio;
                    string urlPMPMY;
                    urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=FY&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];


                    if (!string.IsNullOrEmpty(mailEvaluado))
                        envio = n.devuelveevaluacionFY(mailEvaluado, UE.GetUsuarioNombre(legajoevaluador), urlPMPMY);

                    WUCGuardadoPrevio.EnabledTimer = false;
                   
                    omb.ShowMessage("La Evaluación fue enviada satisfactoriamente");

                    EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));
                    

                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        protected void ButtonAprobarEvaluacion_Click(object sender, EventArgs e)
        {
            //if (Page.IsValid)
            //{
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {
                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }


                FC = new FormulariosController(PeriodoID, legajo, TipoFormularioID);

                try
                {
                    

                    GuardarDatosFormulario();

                    FC.GrabarEvaluadorFY();
                    FC.SaveHistorial(4, PeriodoID, legajo, TipoFormularioID);

                    UsuariosEvaluacion UE = new UsuariosEvaluacion(Page.User.Identity.Name);
                    NotificacionesController n = new NotificacionesController();
                    int legajoevaluador = UE.GetLegajodeEvaluador(legajo, PeriodoID, TipoFormularioID, 1);

                    string mailEvaluado = UE.GetMail(legajo);
                    string envio;
                    string urlPMPMY;
                    urlPMPMY = "http://" + Request.ServerVariables["HTTP_HOST"] + "/servicios/form_evaldesemp.aspx?openPop=FYI&Legajo=" + Request.QueryString["Legajo"] + "&PeriodoID=" + Request.QueryString["PeriodoID"] + "&TipoFormularioID=" + Request.QueryString["TipoFormularioID"];

                    if (!string.IsNullOrEmpty(mailEvaluado))
                        envio = n.aproboevaluacionFY(mailEvaluado, UE.GetUsuarioNombre(legajoevaluador), urlPMPMY);

                    WUCGuardadoPrevio.EnabledTimer = false;
                    omb.ShowMessage("La Evaluación fue Aprobada");
                    EjecutarScript(string.Format("PreguntaImprimirEvaluacion({0},{1},{2});", legajo, PeriodoID, TipoFormularioID));

                }
                catch (Exception)
                {

                    throw;
                }

          //  }
        }

        protected void ButtonImprimir_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GuardadoPrevio();
                //if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                //{
                //    string legajost = string.Empty;
                //    legajost = HiddenFieldlegajo.Value;
                //    fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresion PMPMY", legajost.ToString());
                //}
                int legajo = 0;
                int PeriodoID = 0;
                int TipoFormularioID = 0;
                if (!string.IsNullOrEmpty(HiddenFieldlegajo.Value))
                {

                    legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldPeriodoID.Value))
                {
                    PeriodoID = Convert.ToInt32(HiddenFieldPeriodoID.Value);
                }
                if (!string.IsNullOrEmpty(HiddenFieldTipoFormularioID.Value))
                {
                    TipoFormularioID = Convert.ToInt32(HiddenFieldTipoFormularioID.Value);
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "IF100", string.Format("ImprimirEvaluacionFY({0},{1},{2});", legajo, PeriodoID, TipoFormularioID), true);

            }
        }

        protected void ButtonDoc_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region GuardarDatos

        private void GuardarDatosFormulario()
        {
            GuardarItems();
            GuardarCursos();
            GuardarItemsEducation(DataListEducation);
            GuardarItemsLanguage( RepeaterLanguage);
            GuardarItemsExperience(DataListExperience);
        }

        private void GuardarItemsLanguage(Repeater RptItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {

                 string Idioma= string.Empty;
                 bool IsNative=false;
                 string Proficiency = string.Empty;

                 TextBox TextBoxIdioma = item.FindControl("TextBoxIdioma") as TextBox;
                 CheckBox CheckBoxNative = item.FindControl("CheckBoxNative") as CheckBox;
                 DropDownList DropDownListProficiency= item.FindControl("DropDownListProficiency") as DropDownList;

                


                 if (!string.IsNullOrEmpty(TextBoxIdioma.Text) || !string.IsNullOrEmpty(DropDownListProficiency.SelectedValue.ToString()))
                {
                      Idioma = TextBoxIdioma.Text;
                      IsNative = CheckBoxNative.Checked;
                      Proficiency = DropDownListProficiency.SelectedValue;

                    FC.SaveLanguage(Idioma,IsNative,Proficiency);
                
                }
                 
                
            
            }
        }

        private void GuardarItems()
        {
            //1.objetivos
            GuardarItemsRepeater(RepeaterIndividualObjetives1);
            GuardarItemsRepeater(RepeaterIndividualObjetives2);
            GuardarItemsRepeater(RepeaterIndividualObjetives3);
            GuardarItemsRepeater(RepeaterIndividualObjetives4);
            GuardarItemsRepeater(RepeaterIndividualObjetives5);
            GuardarItemsRepeater(RepeaterIndividualObjetives6);
            GuardarItemsRepeater(RepeaterIndividualObjetives7);
            GuardarItemsRepeater(RepeaterIndividualObjetives8);
            GuardarItemsRepeater(RepeaterIndividualObjetives9);
            GuardarItemsRepeater(RepeaterIndividualObjetives10);

            //2.Success Factors for Effective Leadership
            GuardarItemsSuccessFactors(RepeaterSuccessFactors);
            //3.CURRENT STRENGTHS / DEVELOPMENT NEEDS

            GuardarItemsRepeater(RepeaterSTRENGTHS);
            GuardarItemsRepeater(RepeaterDevelopmentNeeds);

            //4. DEVELOPMENT ACTIONS FOR THE NEXT FISCAL YEAR
            GuardarItemsRepeater(RepeaterDEVELOPMENTACTIONS);

            //5.PERFORMANCE REVIEW - SUMMARY
            GuardarItemsRepeater(RepeaterPerformanceReviewSummary);
            //6.EXECUTIVE MANAGEMENT SUCCESSION & DEVELOPMENT PLAN 

            GuardarItemsRepeater(RepeaterMobilityAndTravel);
            GuardarItemsRepeater(RepeaterCareerInterests);
            GuardarItemsRepeater(RepeaterNextAssigment);
            GuardarItemsRepeater(RepeaterPotencialReplacements1);
            GuardarItemsRepeater(RepeaterPotencialReplacements2);
            GuardarItemsRepeater(RepeaterPotencialReplacements3);

           

            //7.RESUME
            GuardarItemsRepeater(RepeaterResume);

            //8.CAREER PLAN

            GuardarItemsRepeaterCareerPlan(RepeaterCareerPlanLongTerm);
            GuardarItemsRepeaterCareerPlan(RepeaterCareerPlanNearTerm);

            //9.FEEDBACK
            GuardarItemsRepeater(RepeaterFeedback);
        }

        private void GuardarItemsRepeaterCareerPlan(Repeater RptItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {

                int ItemEvaluacionID = 0;
                int Ponderacion = 0;
                string Fundamentacion = string.Empty;

                HiddenField HFID = item.FindControl("HFID") as HiddenField;
                ItemEvaluacionID = int.Parse(HFID.Value);



                DropDownList DropDownListRating = item.FindControl("DropDownListRating") as DropDownList;
                if (DropDownListRating.Visible == true)
                {

                    int.TryParse(DropDownListRating.SelectedValue, out Ponderacion);
                    TextBox TextBoxOther = item.FindControl("TextBoxOther") as TextBox;
                    Fundamentacion = TextBoxOther.Text;
                }
                else
                {

                    TextBox TextBoxComentario = item.FindControl("TextBoxComentario") as TextBox;
                    if (TextBoxComentario.Visible == true)
                        Fundamentacion = TextBoxComentario.Text;
                    
                }

                FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);

            }
            
        }


        private void GuardarItemsSuccessFactors(Repeater RptItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {

                int ItemEvaluacionID = 0;
                int Ponderacion = 0;
                string Fundamentacion = string.Empty;

                HiddenField HFID = item.FindControl("HFID") as HiddenField;
                ItemEvaluacionID = int.Parse(HFID.Value);



                DropDownList DropDownListRating = item.FindControl("DropDownListRating") as DropDownList;
                if (DropDownListRating.Visible == true)
                    int.TryParse(DropDownListRating.SelectedValue, out Ponderacion);


                DropDownList DropDownListOptional = item.FindControl("DropDownListOptional") as DropDownList;
                if (DropDownListOptional.Visible == true)
                    Fundamentacion = DropDownListOptional.SelectedValue;


                FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);

            }
        }

        private void GuardarItemsEducation( Repeater dtlItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in dtlItems.Items)
            {

                string Year = string.Empty;
                string Degree = string.Empty; 
                string School = string.Empty; 

                

                DropDownList DropDownListDegree = item.FindControl("DropDownListDegree") as DropDownList;
                Degree = DropDownListDegree.SelectedValue;

                TextBox TextBoxYear = item.FindControl("TextBoxYear") as TextBox;
                if(!string.IsNullOrEmpty(TextBoxYear.Text))
                    Year = TextBoxYear.Text.Trim();

                TextBox TextBoxSchool = item.FindControl("TextBoxSchool") as TextBox;
                School=TextBoxSchool.Text;
                FC.SaveEducation(Degree, Year, School);

            }
        }

        private void GuardarItemsExperience(Repeater dtlItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in dtlItems.Items)
            {

                string FromDate;
                string ToDate;
                string Company;
                string Position;
                string Description;



                TextBox TextBoxFromDate = item.FindControl("TextBoxFromDate") as TextBox;
                FromDate = TextBoxFromDate.Text;

                TextBox TextBoxToDate = item.FindControl("TextBoxToDate") as TextBox;
                ToDate = TextBoxToDate.Text;

                TextBox TextBoxCompany = item.FindControl("TextBoxCompany") as TextBox;
                Company = TextBoxCompany.Text;

                TextBox TextBoxPosition = item.FindControl("TextBoxPosition") as TextBox;
                Position = TextBoxPosition.Text;

                TextBox TextBoxDescription = item.FindControl("TextBoxDescription") as TextBox;
                Description = TextBoxDescription.Text;

                if(!string.IsNullOrEmpty(FromDate)||!string.IsNullOrEmpty(ToDate)||!string.IsNullOrEmpty(Company)||!string.IsNullOrEmpty(Position) || !string.IsNullOrEmpty(Description))
                    FC.SaveExperience(FromDate,ToDate,Company,Position,Description);

            }
        }



        private void GuardarItemsRepeater(Repeater RptItems)
        {
            foreach (System.Web.UI.WebControls.RepeaterItem item in RptItems.Items)
            {

                int ItemEvaluacionID = 0;
                int Ponderacion = 0;
                string Fundamentacion = string.Empty;

                HiddenField HFID = item.FindControl("HFID") as HiddenField;
                ItemEvaluacionID = int.Parse(HFID.Value);



                DropDownList DropDownListRating = item.FindControl("DropDownListRating") as DropDownList;
                if (DropDownListRating.Visible == true)
                    int.TryParse(DropDownListRating.SelectedValue, out Ponderacion);



                TextBox TextBoxComentario = item.FindControl("TextBoxComentario") as TextBox;
                if (TextBoxComentario.Visible == true)
                    Fundamentacion = TextBoxComentario.Text;
                FC.SaveItemEvaluacion(ItemEvaluacionID, Ponderacion, Fundamentacion);

            }
        }

        private void GuardarCursos()
        {
            int Legajo = int.Parse(HiddenFieldlegajo.Value);
            int PeriodoID = int.Parse(HiddenFieldPeriodoID.Value);
            int TipoFormularioID = int.Parse(HiddenFieldTipoFormularioID.Value);
            string CursosSeleccionados = HiddenFieldCursosSeleccionados.Value.ToString();

            if (!string.IsNullOrEmpty(CursosSeleccionados.Trim()))
            {

                string[] words = CursosSeleccionados.Split(',');


                foreach (string CursoSeleccionado in words)
                {
                    int CursoID = int.Parse(CursoSeleccionado);
                    FC.SaveCursos(CursoID);
                }

            }

        }

        #endregion

        protected void Imprimir_Click(object sender, EventArgs e)
        {
            //LogController fle = new LogController(); ;

            //int legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            //fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresión PMPFY", legajo.ToString());
        }

        protected void DropDownListProficiency_DataBound(object sender, EventArgs e)
        {
            if (int.Parse(Request.QueryString["PeriodoID"].ToString()) >= 19)
            {
                ((DropDownList)sender).Items.Clear();
                ((DropDownList)sender).Items.Add(new ListItem("Please Select",""));
                ((DropDownList)sender).Items.Add(new ListItem("Conversational","4"));
                ((DropDownList)sender).Items.Add(new ListItem("Business Fluent","5"));
            }
        }


        protected void DropDownListRating_DataBound(object sender, EventArgs e)
        {
            if (int.Parse(Request.QueryString["PeriodoID"].ToString()) >= 19)
            {
                ((DropDownList)sender).Items.Clear();
                ((DropDownList)sender).Items.Add(new ListItem("Please Select","0"));
                ((DropDownList)sender).Items.Add(new ListItem("Now","1"));
                ((DropDownList)sender).Items.Add(new ListItem("1 year","2"));
                ((DropDownList)sender).Items.Add(new ListItem("2 year","3"));
                ((DropDownList)sender).Items.Add(new ListItem("More than 2 years","6"));
                                 
            }
        }
        
















    }
}