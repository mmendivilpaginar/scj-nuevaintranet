﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.paginar.formularios.dataaccesslayer ;
using com.paginar.formularios.businesslogiclayer;
using com.paginar.johnson.DAL;
using com.paginar.johnson.BL;
using com.paginar.johnson.membership;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Globalization;

namespace com.paginar.johnson.Web.PMPFY
{
    public partial class Impresion : PageBase
    {
        protected FormulariosFYController FC = new FormulariosFYController();
        protected FormulariosPMPFYDS.frmPMPCalificacionDataTable TCalificaciones = new FormulariosPMPFYDS.frmPMPCalificacionDataTable();
        protected FormulariosPMPFYDS.frmPMPFortalezasTempDataTable Tfortaleza = new FormulariosPMPFYDS.frmPMPFortalezasTempDataTable();
        protected FormulariosController FCC = new FormulariosController();
        protected string NombreEvaluado;
        protected string ApellidoEvaluado;
        protected string NombreEvaluador;
        protected string ApellidoEvaluador;
        protected object PonderacionGuardada;
        protected string incrementarUnAnio(object a)
        {
            int retorno;
            retorno = int.Parse(a.ToString()) + 1;
            return retorno.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int? PasoActualID; 
            if (!string.IsNullOrEmpty(Request.QueryString["Legajo"]))
            {
                HiddenFieldlegajo.Value = Request.QueryString["Legajo"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["PeriodoID"]))
            {
                HiddenFieldPeriodoID.Value = Request.QueryString["PeriodoID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["TipoFormularioID"]))
            {
                HiddenFieldTipoFormularioID.Value = Request.QueryString["TipoFormularioID"];
            }
            PasoActualID = FormularioInfo.GetPasoActual(Convert.ToInt32(HiddenFieldlegajo.Value), Convert.ToInt32(HiddenFieldPeriodoID.Value), Convert.ToInt32(HiddenFieldTipoFormularioID.Value));
            HiddenFieldPasoID.Value = PasoActualID == -1 ? "2" : PasoActualID.ToString();
            TCalificaciones = FC.GetCalificaciones("I", "P"); 
            Tfortaleza = FC.GetFortalezasByTipoFormularioID(int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
            if (int.Parse(HiddenFieldPeriodoID.Value) >= 19)
                RepeaterCursos.Visible = false;
            FormulariosController f = new FormulariosController();
            FormulariosFYController fYc = new FormulariosFYController();


            FormulariosDS.CabeceraFormularioDataTable Cab = FCC.GetCabecera(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value), null);

            ControllerUsuarios CU = new ControllerUsuarios();
            DSUsuarios.UsuarioDataTable DTU = CU.GetUsuariosByLegajo( int.Parse(Cab.Rows[0].ItemArray[4].ToString()));
            
            if (HiddenFieldTipoFormularioID.Value == "11")
            {
                ((Label)FVC.FindControl("lblCodeForm")).Text = "SF000024"; //aqui existia una O SFO
                PanelCareerPlan.Visible = false;
                PanelFeedback.Visible = false;
            }
            else
                ((Label)FVC.FindControl("lblCodeForm")).Text = "SF000021";
            
            lblApellidoEvaluado.Text = DTU.Rows[0].ItemArray[2].ToString().ToUpper();
            lblNombreEvaluado.Text = DTU.Rows[0].ItemArray[3].ToString().ToUpper();

            lblNombreC.Text = DTU.Rows[0].ItemArray[2].ToString();
            lblApellidoC.Text = DTU.Rows[0].ItemArray[3].ToString();
            lblTitleA.Text = DTU.Rows[0].ItemArray[34].ToString().ToUpper();
            lblNombreEvaluadoX.Text = DTU.Rows[0].ItemArray[2].ToString();
            lblApellidoEvaluadoX.Text = DTU.Rows[0].ItemArray[3].ToString();
            lblCargoX.Text = Cab.Rows[0].ItemArray[10].ToString();

            string strTimeInJob = FCC.GetTimeInJobByID(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), int.Parse(HiddenFieldlegajo.Value));
            if (!string.IsNullOrEmpty(strTimeInJob))
            {
                DateTime TImeInJob = DateTime.Parse(strTimeInJob);
                LabelTimeInJob.Text = TImeInJob.ToString("MMMM", new CultureInfo("en-US")) + ", " + TImeInJob.ToString("yyyy");
            }

            string escalasalarial = FCC.DameJobGrade(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value));
                //CU.DameEscalaSalarial(int.Parse(DTU.Rows[0].ItemArray[1].ToString()));
            
            lblEscalaSalarial.Text = (escalasalarial!=null) ? escalasalarial.ToUpper() : string.Empty;
            lblJobLevelH.Text = escalasalarial;


            if (!f.ExisteEvaluacion(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value)))
                Response.Redirect("EvaluacionError.aspx");

            //lblDebilidadesyCursos.Text = llenarDebilidadesCursos();

            LabelPeriodoFiscalDescripcion.Text = DamePeriodoAnios(HiddenFieldPeriodoID.Value);
            SetearSeccionesVisibles(PasoActualID);
            if(ObjectUsuario!=null)
             LabelRegistro.Text = "<strong>Fecha de Exportación/Impresión:</strong> " + DateTime.Now + "<br/><strong>Usuario:</strong> " + ObjectUsuario.apellido + " " + ObjectUsuario.nombre;



            if (DameAnioDesdePeriodo(int.Parse(HiddenFieldPeriodoID.Value)) <= 2009)
            {
                RepeaterCursosSeleccionados2010_2011.Visible = false;
                RepeaterCursos.Visible = false;
            }
            else
            {
                //if(DameAnioDesdePeriodo(int.Parse(HiddenFieldPeriodoID.Value))==2010)
                //{
                //  RepeaterCursosSeleccionados2010_2011.Visible = true;
                //  RepeaterCursos.Visible = false;
                //}
                //else
                //{
                  RepeaterCursosSeleccionados2010_2011.Visible = false;
                  if (int.Parse(HiddenFieldPeriodoID.Value) >= 19)
                    RepeaterCursos.Visible = false;
                    else
                    RepeaterCursos.Visible = true;
                //}

            }



            string SeccionaImprimir = (!string.IsNullOrEmpty(Request.QueryString["Seccion"]) )? Request.QueryString["Seccion"].ToString() : string.Empty ;

           // FormView FormViewFVC = (FormView)this.Master.FindControl("FVC");
            switch (SeccionaImprimir) { 
            
            
                case "PrintAll":
                    if (HiddenFieldTipoFormularioID.Value == "10")
                        PExecutiveManagementDevelopmentPlan.Visible = false;
                    if (HiddenFieldTipoFormularioID.Value == "11")
                    {
                        PanelCareerPlan.Visible = false;
                        PanelFeedback.Visible = false;
                    }
                    break;
                case "Resume":
                   
                    FVC.Visible = false;
                    PPReview.Visible = false;
                    PLeaderImperatives.Visible = false;
                    PSDNeed.Visible = false;
                    PDAFTNextFiscalYear.Visible = false;
                    PPerformanceReviewSummary.Visible = false;
                    PExecutiveManagementDevelopmentPlan.Visible = false;
                    PanelResume.Visible = true;
                    PanelCareerPlan.Visible = false;
                    PanelFeedback.Visible = false;

                    break;
                case "PMP":
                    FVC.Visible = true;
                    PPReview.Visible = true;
                    PLeaderImperatives.Visible = true;
                    PSDNeed.Visible = true;
                    PDAFTNextFiscalYear.Visible = true;
                    PPerformanceReviewSummary.Visible = true;
                    PExecutiveManagementDevelopmentPlan.Visible = false;
                    PanelResume.Visible = false;
                    PanelCareerPlan.Visible = false;
                    PanelFeedback.Visible = false;
                    break;
                case "CareerPlan":

                    FVC.Visible = false;
                    PPReview.Visible = false;
                    PLeaderImperatives.Visible = false;
                    PSDNeed.Visible = false;
                    PDAFTNextFiscalYear.Visible = false;
                    PPerformanceReviewSummary.Visible = false;
                    PExecutiveManagementDevelopmentPlan.Visible = false;
                    PanelResume.Visible = false;
                    PanelCareerPlan.Visible = true;
                    PanelFeedback.Visible = true;
                    break;
                case "MSD":
                    FVC.Visible = false;
                    PPReview.Visible = false;
                    PLeaderImperatives.Visible = false;
                    PSDNeed.Visible = false;
                    PDAFTNextFiscalYear.Visible = false;
                    PPerformanceReviewSummary.Visible = false;
                    PExecutiveManagementDevelopmentPlan.Visible = true;
                    PanelResume.Visible = false;
                    PanelCareerPlan.Visible = false;
                    PanelFeedback.Visible = false;
                    Divbr1.Visible = false;
                    Divbr2.Visible = false;
                    //SaltoPaginaEncabezado.Visible = false;
                    SaltoPaginaResume.Visible = false;
                    break;
                default:

                    FVC.Visible = true;
                    PPReview.Visible = true;
                    PLeaderImperatives.Visible = true;
                    //PSDNeed.Visible = true;
                    //PDAFTNextFiscalYear.Visible = true;
                    PPerformanceReviewSummary.Visible = true;
                   
                    PanelResume.Visible = true;


                    if (HiddenFieldTipoFormularioID.Value == "10")
                    {
                        PanelCareerPlan.Visible = true;
                        PanelFeedback.Visible = true;
                        PExecutiveManagementDevelopmentPlan.Controls.Clear();
                        PExecutiveManagementDevelopmentPlan.Visible = false;
                    }
                    else
                        PExecutiveManagementDevelopmentPlan.Visible = true;
                    break;


            }


        }

 

        private void SetearSeccionesVisibles(int? PasoActualID)
        {

            int LegajoLog = 0;
            int? LegajoEvaluador;
            User Usuario = null;

            Usuarios usercontroller = new Usuarios();
            Usuario = usercontroller.GetUserByName(Page.User.Identity.Name);

            LegajoLog = Usuario.GetLegajo();

            LegajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldTipoFormularioID.Value), 1);


            if (LegajoLog != LegajoEvaluador)
            {
                PanelSectionF.Visible = false;
                PanelSectionG.Visible = false;
                PanelSectionI.Visible = false;

               
            }


            if (int.Parse(HiddenFieldTipoFormularioID.Value) == 10)
            {
                RepeaterSeccionBPerformanceRatings.Visible = false;
                DataListEducation.Visible = false;
                
                RepeaterLanguage.Visible = false;
                RepeaterMobilityAndTravel.Visible = false;
                RepeaterLanguage.Visible = false;
                RepeaterCareerInterests.Visible = false;
            }
            else
            {
                DataListEducation.DataSource = FCC.GetEducationByTramiteID(int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(HiddenFieldlegajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
                DataListEducation.DataBind();

                RepeaterLanguage.DataSource = FCC.GetLanguageByTramiteID(int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(HiddenFieldlegajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
                RepeaterLanguage.DataBind();

                RepeaterMobilityAndTravel.DataSource = FCC.GetItemEvaluacion(20, int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(HiddenFieldlegajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
                RepeaterMobilityAndTravel.DataBind();



                RepeaterCareerInterests.DataSource = FCC.GetItemEvaluacion(21, int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(HiddenFieldlegajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
                RepeaterCareerInterests.DataBind();

                RepeaterNextAssigment.DataSource = FCC.GetItemEvaluacion(22, int.Parse(HiddenFieldPeriodoID.Value.ToString()), int.Parse(HiddenFieldlegajo.Value.ToString()), int.Parse(HiddenFieldTipoFormularioID.Value.ToString()));
                RepeaterNextAssigment.DataBind();
            }
        }

        protected string devuelveFortalezaNecesidad(object fortaleza)
        {
            string Output = string.Empty;

            int TipoFormularioID = int.Parse(HiddenFieldTipoFormularioID.Value);
            int codigo;
            int.TryParse(fortaleza.ToString(), out codigo);

            if(codigo<=0)
               return Output;

            FormulariosFYController fYc = new FormulariosFYController();
            FormulariosPMPFYDS.frmPMPFortalezasTempDataTable DTFortalezas = new FormulariosPMPFYDS.frmPMPFortalezasTempDataTable();
            DTFortalezas = fYc.GetFortalezaByID(codigo, TipoFormularioID);
            if (DTFortalezas.Rows.Count > 0)
                Output = DTFortalezas[0].Descripcion;

            return Output;
        }




        protected string setValorPonderacion(object Valor)
        {
            string Output = string.Empty;

            if (int.Parse( Valor.ToString() ) == 0)
                return Output;

                foreach (FormulariosPMPFYDS.frmPMPCalificacionRow item in TCalificaciones.Rows)
                {
                    if (item.calificacion.ToString() == Valor.ToString())
                        Output += item.descripcion.ToString();                
                }


            return Output;
        }


        protected string setCalificacionSummary(object Valor)
        {
            string Output = string.Empty;

            if (int.Parse(Valor.ToString()) == 0)
                return Output;

            FormulariosFYController fYc = new FormulariosFYController();
            Output = fYc.GetDescripcionCalificacionByID("I", "R", short.Parse(Valor.ToString()));


            return Output;
        }

        protected string formatearTexto(string Titulo, string Descripcion)
        {
            string Output = string.Empty;

            Output += "<strong>" + Titulo + "</strong>  " + Descripcion;

            return Output;
        }

        
        protected string OverallRating(object Valor)
        {
            string Output = string.Empty;
                    Output += "<table class=\"pnet-table\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                    Output += "<tr>";
                    Output += "<td width=\"33%\" class=\"texto10negrita\" style=\"border-top:4px solid black\"><div align=\"center\">Performance on</br> Objectives Wt. = 60%</div></td>";
                    Output += "<td width=\"33%\" class=\"texto10negrita\" style=\"border-top:4px solid black\"><div align=\"center\">Performance on</br> Success Factors Wt. = 40%</div></td>";
                    Output += "<td width=\"33%\" class=\"texto10negrita\" style=\"border:4px solid red\"><div align=\"center\">Overall Rating:";
                    Output += "<span class=\"x\">&nbsp;";
                    Output += setCalificacionSummary(Valor);
                    Output += "</span> ";
                    Output += "</div></td>";
                    Output += "</tr>";
                    Output += "</table>";
                    Output += "<br/>";
            return Output;
        }


        public class Curso
        {

            public int CursoID;
            public string CursoDescripcion;
            public string DebilidadID;

            public Curso(int CursoID, string CursoDescripcion, string DebilidadID)
            {
                this.CursoID = CursoID;
                this.CursoDescripcion = CursoDescripcion;
                this.DebilidadID = DebilidadID;
            }

        }


        public string Periodo(object PeriodoID)
        {
            string retorno = FCC.GetPeriodoPMPMY(int.Parse(PeriodoID.ToString()));

            
            return retorno.Substring(3);

        }

        public string DameResumeDegree(object Degree)
        {
            string retorno= string.Empty;
            switch (Degree.ToString().ToLower())
            {

                case "school studies":
                    retorno = "School. St.";
                    break;
                case "graduated studies":
                    retorno = "Gt. St.";
                    break;
                case "post graduated studies":
                    retorno = "Post. Gr.";
                    break;

            
            }


            return retorno;

        }

        public string DameSchool(object school)
        {
            
            int tamanio = (school.ToString().Length<35) ? school.ToString().Length : 35;

            return school.ToString().Substring(0,tamanio);
        }

        protected int DameAnioDesdePeriodo(int PeriodoID)
        {

            int elPeriodo=0;
            FormulariosDS.PeriodoDataTable periodo = new FormulariosDS.PeriodoDataTable();
            periodo = FachadaDA.Singleton.Periodo.GetDataByID(PeriodoID);
            foreach (FormulariosDS.PeriodoRow item in periodo.Rows)
            {
                elPeriodo = ((DateTime)item.EvaluadoDesde).Year;
            }

            return elPeriodo;
        }

        public string DamePeriodoAnios(object PeriodoID)
        {
            string retorno = FCC.GetPeriodoPMPMY(int.Parse(PeriodoID.ToString())).Replace("FY","");

            return retorno;

        }

       


      protected string DefaultVal(object val)
        {
            if ((val == System.DBNull.Value) || (val == string.Empty))
                return ("--");
            else
                return (val.ToString());

        }

        protected string setMobilidad(object Valor)
        {
            string Output = string.Empty;
            switch (Valor.ToString())
            {
                case "1":
                    Output = "Not Mobile";
                    break;
                case "2":
                    Output = "Domestic Only";
                    break;
                case "3":
                    Output = "International Only";
                    break;
                case "4":
                    Output = "Domestic and International";
                    break;
                
            }

            return Output;
        }

        protected string setMobilidadYN(object Value)
        {
            string Output = string.Empty;

            switch (Value.ToString())
            {
                case "1":
                    Output = "Yes";
                    break;
                case "2":
                    Output = "No";
                    break;
                default:
                    break;
            }

            return Output;
        }

        protected string nivelIdioma(object Value)
        {
            string Output = string.Empty;
            switch (Value.ToString())
            {
                case "1":
                    Output = "High";
                    break;
                case "2":
                    Output = "Medium";
                    break;
                case "3":
                    Output = "Low";
                    break;

            }

            return Output;
        }

        protected string setCareer(object Valor)
        {
            int ID;
            int.TryParse(Valor.ToString(), out ID);
            FormulariosFYController FYC = new FormulariosFYController();
            return FYC.GetCareerDescripcion(ID);
        
        }

        protected string setCareerCode(object Valor)
        {
            int ID;
            int.TryParse(Valor.ToString(), out ID);
            FormulariosFYController FYC = new FormulariosFYController();
            return FYC.GetCareerCode(ID);

        }


        protected string GetTextRating(object valor) {

            string Output = string.Empty;
            if (string.IsNullOrEmpty(valor.ToString()))
                return Output;

            switch (valor.ToString())
            { 
            
                case "1":
                    Output = "(current year)";
                    break;
                case "2":
                    Output = "(one year ago)";
                    break;
                case "3":
                    Output = "(two years ago)";
                    break;
                default:
                    Output = "";
                    break;
            }
            return Output;
        }

        protected string SetYear(object Valor)
        {
            string Output = string.Empty;
            switch (Valor.ToString())
            {
                case "0":
                    Output += " ";
                    break;
                case "1":
                    Output += "Now";
                    break;
                case "2":
                    Output += "1 year";
                    break;
                case "3":
                    Output += "2 Years";
                    break;
                case "4":
                    Output += "3 years";
                    break;
                case "5":
                    Output += "4+ years";
                    break;
                case "6":
                    Output += "More than 2 years";
                    break;

            }
            return Output;
        }

        protected string setNumeration(object Valor)
        {
            string Output = string.Empty;
            switch (Valor.ToString())
            {
                case "194":
                    Output += "1.";

                    break;
                case "197":
                    Output += "2.";
                    break;
                case "200":
                    Output += "3.";
                    break;
                default:
                    Output += "";
                    break;
            }

            return Output;
        }

        protected string setArea(object Valor)
        {
            try
            {
                return FC.GetAreaDescByID(int.Parse(Valor.ToString()));
            }
            catch
            {
                return Valor.ToString();
            }
        }

        protected string DameProficiency(Object Proficiency)
        {
        
        
            string ret;


            switch (Proficiency.ToString())
            {
                case "1":
                    ret="High";
                    break;
                case "2":
                    ret="Medium";
                    break;
                case "3":
                     ret="Low";
                    break;
                case "4":
                    ret = "Conversational";
                    break;
                case "5":
                    ret = "Business Fluent";                    
                    break;
                default:
                    ret="";
                    break;

            
            
            }

            return ret;

        }


        protected string buildLongTerm(object Valor, object titulo, object Fundamentacion, object Ponderacion)
        {
            string Output = string.Empty;

            switch (Valor.ToString())
            {
                case "176":
                case "144":
                case "335":
                     if(Ponderacion.ToString()=="310")

                        PonderacionGuardada = Fundamentacion;
                    else
                        PonderacionGuardada = Ponderacion;
                    break;
                case "146":
                case "178":
                case "337":
                    
                    Output += "<tr><td width=\"220\" valign=\"top\"><strong>" + titulo.ToString() + "</strong></td>";
                    Output += "<td valign=\"top\"><span class=\"Perspectivas\" id=\"LargoPlazoEmpleado\">" + setArea(PonderacionGuardada);
                    Output += Fundamentacion.ToString() + "</span></td></tr>";
                    //Output += "<tr><td height=\"10\" colspan=\"2\" valign=\"top\" class=\"texto11times\"></td></tr>";
                    break;
                case "181":
                case "149":
                case "340":
                    Output += "<tr><td width=\"220\" valign=\"top\"><strong>"+ titulo.ToString() +"</strong></td>";
                    Output += "<td valign=\"top\"><span class=\"Perspectivas\" id=\"LargoPlazoCompania1\">"+ Fundamentacion.ToString() +"</span></td></tr>";
                   
                    break;



            }

            
            return Output;
        }

        protected string buildNearTerm(object Valor, object titulo, object Fundamentacion, object Ponderacion)
        {
            string Output = string.Empty;

            switch (Valor.ToString())
            {
                case "177":
                case "145":
                case "336":
                    if (Ponderacion.ToString() == "310")

                        PonderacionGuardada = Fundamentacion;
                    else
                        PonderacionGuardada = Ponderacion;
                    break;
                case "147":
                case "179":
                case "338":

                    Output += "<tr><td width=\"220\" valign=\"top\"><strong>" + titulo.ToString() + "</strong></td>";
                    Output += "<td valign=\"top\"><span class=\"Perspectivas\" id=\"LargoPlazoEmpleado\">" + setArea(PonderacionGuardada);
                    Output += Fundamentacion.ToString() + "</span></td></tr>";
                    //Output += "<tr><td height=\"10\" colspan=\"2\" valign=\"top\" class=\"texto11times\"></td></tr>";
                    break;
                case "180":
                case "148":
                case "339":
                    Output += "<tr><td width=\"220\" valign=\"top\"><strong>" + titulo.ToString() + "</strong></td>";
                    Output += "<td valign=\"top\"><span class=\"Perspectivas\" id=\"LargoPlazoCompania1\">" + Fundamentacion.ToString() + "</span></td></tr>";

                    break;



            }


            return Output;
        }


       
        protected void RepeaterIndividualObjetives_PreRender(object sender, EventArgs e)
        {

            if (((Repeater)sender).Items.Count == 0)
                ((Repeater)sender).Visible = false;
        }

       

        protected string summaryComments(object Titulo, object Comentario)
        {
            string supervisor = "Supervisor Comments";
            string titulo = Titulo.ToString();
            string Output = string.Empty;
            if (Titulo.ToString().IndexOf(supervisor) == 0)
            {
                titulo = Titulo.ToString().Substring(0, 18);
            }
                    Output += "<br/>";
                    Output += "<table class=\"pnet-table\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                    Output += "<tr>";
                    Output += "<td bgcolor=\"#000000\" width=\"205\" class=\"texto12negrita_enblanco\">" + titulo;// +"<img src=\"Img/FondoNegro3.jpg\" alt=\"titulo\" width=\"202\" height=\"21\" />";
                    Output += "</td><td width=\"32\" style=\"border:0\"></td>";
                    Output += "<td width=\"499\" style=\"border:0\"><span class=\"texto10italica\"><span style=\"font-size:10.0pt;font-family:Arial;mso-bidi-font-family:&quot;Times New Roman&quot;\">";
                    if (Titulo.ToString().IndexOf(supervisor) == 0)
                    Output += "(Be sure to include this individual&rsquo;s Key Accomplishments)";
                    Output += "</span></span></td></tr>";
                    //Output += "</table>";
                    //Output += "<table width=\"100%\" border=\"0\" class=\"pnet-table\"  cellspacing=\"0\" cellpadding=\"0\">";
                    Output += "<tr>";
                    Output += "<td colspan=\"3\" valign=\"top\" style=\"word-wrap:break-word;\">" + Comentario.ToString() + "&nbsp;</td>";
                    Output += "</tr>";
                    Output += "</table>";

            return Output;
        }



 

        protected void RepeaterCursosSeleccionados_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)

            {

                HiddenField HiddenFieldDebilidadID = e.Item.FindControl("HiddenFieldDebilidadID") as HiddenField;
                Literal LiteralCursosRelacionados = e.Item.FindControl("LiteralCursosRelacionados") as Literal;
                FormulariosController f = new FormulariosController();
                FormulariosDS.CursosDataTable CDRCT = new FormulariosDS.CursosDataTable();
                CDRCT = f.GetCursosByTramiteID(int.Parse(HiddenFieldPeriodoID.Value), int.Parse(HiddenFieldlegajo.Value), int.Parse(HiddenFieldTipoFormularioID.Value));

                string cursosRelacionados = string.Empty;

                foreach  (FormulariosDS.CursosRow  CursoRow in CDRCT.Rows)
                {
                    if (!CursoRow.IsDebilidadIDNull())
                    {
                       if(CursoRow.DebilidadID.IndexOf("[" + HiddenFieldDebilidadID.Value + "]") >= 0)
                        cursosRelacionados = cursosRelacionados + CursoRow.CursoDescripcion + "<br/>";
                    }
                }

                LiteralCursosRelacionados.Text = cursosRelacionados;
            
            }

        }



        public string ImageToBase64(MemoryStream ms,
  System.Drawing.Imaging.ImageFormat format)
        {

            byte[] imageBytes = ms.ToArray();


            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;

        }

        protected string GetUrl(string imagepath)
        {

            string[] splits = Request.Url.AbsoluteUri.Split('/');

            if (splits.Length >= 2)
            {

                string url = splits[0] + "//";

                for (int i = 2; i < splits.Length - 1; i++)
                {

                    url += splits[i];

                    url += "/";

                }

                return url + imagepath;

            }

            return imagepath;

        }

        public static string getHeaderMime()
        {
            StringWriter headerMime = new StringWriter();

            headerMime.WriteLine("From: \"Guardado por Windows Internet Explorer 8\"");
            headerMime.WriteLine("Subject: ");
            headerMime.WriteLine("Date: Thu, 19 Jan 2012 17:37:25 -0300");
            headerMime.WriteLine("MIME-Version: 1.0");
            headerMime.WriteLine("Content-Type: multipart/related;");
            headerMime.WriteLine("	type=\"text/html\";");
            headerMime.WriteLine("	boundary=\"----=_NextPart_000_0000_01CCD6D1.01BB9000\"");
            headerMime.WriteLine("X-MimeOLE: Produced By Microsoft MimeOLE V6.1.7600.16543");
            headerMime.WriteLine("");
            headerMime.WriteLine("This is a multi-part message in MIME format.");
            headerMime.WriteLine("");
            headerMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            headerMime.WriteLine("Content-Type: text/html;");
            headerMime.WriteLine("	charset=\"Windows-1252\"");
            headerMime.WriteLine("Content-Transfer-Encoding: quoted-printable");
            headerMime.WriteLine("Content-Location: http://scj/web/Word.html");
            headerMime.WriteLine("");
            headerMime.WriteLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">");
            headerMime.WriteLine("<HTML><HEAD>");
            headerMime.WriteLine("<META content=3D\"text/html; charset=3Dwindows-1252\" =");
            headerMime.WriteLine("http-equiv=3DContent-Type>");
            headerMime.WriteLine("<META name=3DGENERATOR content=3D\"MSHTML 8.00.7600.16722\"></HEAD>");
            headerMime.WriteLine("<BODY>");
            return headerMime.ToString(); ;
        }



        public string getFootMime()
        {


            string imguser= "R0lGODlhyADIAMQAAC4uLv0AU4WFhQEBAejo6E1NTXl5ebi4uNLS0v/I3szMzMHBwZqamqOjo2Rk" +
"ZNrb2vHx8f9qo/+01f/a7Pj4+P////z8/P7+/v/1+fWfwa2uro2Njc7X1P/h7f/o88fHxyH5BAAA" +
"AAAALAAAAADIAMgAAAX/YCWOZGmeaKqubOu+cCzPdG3feK7vfO//wKBwSCwaj8ikcslsOp/QqHRK" +
"rVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh8Tq/b7/i8fs/v+/+AgYKDhIWGh4iJiouMjY6PkJGS" +
"k5SVlpeYmZqbnJ2en6ChoqOkpaanqKmqq6ytrq+wsbKztLW2t7i5uru8vb6/wMHCw8TFxsfIycrL" +
"zM3Oz9DRshQOAA4WFwIGBg8A3gzSKAADAgAH4w4DCgPsBuEnBewFC+oiCOzkFiffF84HDOnigbtw" +
"j52AExfaPRvHjkO6cfQM6jMhcRmBBg4ObGiQIcIFDgs+FCSHwqCyC/EG/wCocCFCgJcT+q2TuAEf" +
"gAcV2AHQ9uECg23bNvQjUMCbgX7d8G2oQADotokLDBxsaoBChW0ELhD1BqBBBW3uKmjgtojhgAMT" +
"XqoNEKHCTHIa2DVgmBMfuwoMGTaAIG/cSnkM2B14q1OEAXYQPshNOOBDXbsPDg9gQEHlIsX4KnjI" +
"sDZCArdKBbAjyO4Bu6XsEPi1MK5AwQsRCyKoK6DgA8wiRA9AMFMAYwV1NWAGZ7PxREQpLYtoGaDD" +
"hX6YB2yoOYD0ANMkS8db6Vc2XgDeDcouWMGCbgQFD7JTkM13A5Nx2TlgZHblCAsS+omIPv3uTOzq" +
"XecXXgMUwJpSFRQEnP9B2KF311eppVcXcLrhI4A+DOFUlk0kfGAfS2/1NwBo15mU2nYEAmABNQwZ" +
"oGBdG4x3l3kRmrhbfABIdhCNA8y3SDqZiTDTNUKGdpdtNibFXYHdXENdg4zVVuOIV00ZIAK6oZbP" +
"Y9ctQtgBB9ilkwWSDbCAbhVERICNCKDY3V1qsrMAbTJSSV1IbGZJWwXpUPdhIvVZEJhNEFhgVwXY" +
"5VWXO6kNiKJOheVV2kwK4FaeWcWpEyakA3BjGZBzKnKgPEyNs8GKZh0gAgHVALDUBdvMORYBP4GD" +
"gAHgQFCUqyNs4I0DWVFFwFdhrVhNAQpA4FRWDThlwAJRIWABVeAsYgH/kPIZUICvdnnFxHMlWHAc" +
"CfqVAO5y2DQCQTVi4lMABO+kIO4H9NJbbrnx5qvvvvz2668YY4WFwk9ADbtENkVVNQo+B4xLAqaq" +
"igAtCRRAO2zFC2TF1AEU9INAxG4tYBUCC8BrrpgL6IfAB6HaM3GCLKsc0rgEZFwuycOS7LHICZb8" +
"BnYFpjAOAytbJRY7GowQUQEXbOoOmWayNI4IUDM6gMEkpAQcAxomZ98FDMEbJNj4VEvBgMsNOHXV" +
"dWHNRp8P7jNAAyvrl5KB+2UGZFiH9UTgfewEVgBLJchVQlzgDS0CQxpUFvc4lvLFDtYMqUaloZMF" +
"ja8ad43jrQmYjlBg/zwjRMfXOAdV6djfI5RpcuFUknBYAxaEOTiBBQY29eIjPphOOqnj7gDphuHz" +
"OhvY3b057w5s00+YDqTjrWIB9dhp8avvLjo5y9cFcvGqrsM0gfHsGrtZYaX2eAEPxU5b92gkFyTo" +
"u2Vt1+2KlcOOYnw3xnsJollduLYDAa1YpSbzEU1YxuE04hFIARoCWmkWd5P5QUiAbZhcPwInN7+V" +
"Z0pWoYc2VHKP/oXKLwBggD4O07ISjEROWuGUwcaBOQJoT3t8mowCDuMjb+BOdlFzgwK8oR8D5MgE" +
"1wLPCIa4lKuAhyAp9FADugEOQSnxAq1KYXkYoEQUrIsrs0lQUeZBtfRqVKAofLJPEu9TjQtMyxrl" +
"GV4FPPQhK4aRDm5cXh7JdS/9nItqE3Gjw8TVD0GmoI/3Eddx9phIdB1HkYAsJCId+a9KWlILHUiA" +
"JjfJyU568pOgDKUoR0nKUprylKhMpSo5iQH4ucAla4mlLGdJy1ra8pa4zKUud8nLXvqylp+xASx/" +
"ScxiGvOYyEzmLyVwSRP8sZnQjKY0p0nNalrzmtjMpja3yc1uevOb4AynOMdJznKa85zoTKc618nO" +
"drrznfCMpzznSc962vOe+MynPvfJz376858ADahAB0rQghr0oAhNqEIXytCGOvShEI2oRCd6zhAA" +
"ADs=";

            StringWriter FootMime = new StringWriter();

            FootMime.WriteLine("</BODY></HTML>");
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            FootMime.WriteLine("Content-Type: image/jpeg");
            FootMime.WriteLine("Content-Transfer-Encoding: base64");
            FootMime.WriteLine("Content-Location: http://Word/logoprint.jpg");
            FootMime.WriteLine("");
            FootMime.WriteLine("/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAZAAA/+4ADkFkb2JlAGTAAAAAAf/b");
            FootMime.WriteLine("AIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgIC");
            FootMime.WriteLine("AwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD");
            FootMime.WriteLine("AwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgANACqAwERAAIRAQMRAf/EAIMAAQACAwEBAQAAAAAAAAAA");
            FootMime.WriteLine("AAAHCAYJCgULAgEBAAAAAAAAAAAAAAAAAAAAABAAAAYCAQEGAgUIBA8AAAAAAgMEBQYHAQgACRES");
            FootMime.WriteLine("ExSoKRVpISIWFxgxIzM0Nlc4GWMkVDlBYkNlZqa2N2coaHjYCjoRAQAAAAAAAAAAAAAAAAAAAAD/");
            FootMime.WriteLine("2gAMAwEAAhEDEQA/AO/jgOA4DgOA4DgOA4HiyQ8CaOv6kxec1ATsroeN0Tk5UKG0BSE8wS8hPjsy");
            FootMime.WriteLine("ecjwHxAgx9IhBxj/AA8Dhq0jcm+q9o6EKpVfq7trdMybtlHOLXhUsp28bL5w/LK+m7mxSzZiO3H8");
            FootMime.WriteLine("GqcloCqdCTDUGGEw4vDZ45qvJhY1K4LvdLlm6XkuxRVg7GTWQvHVHMulUZJjbTm96IrRT3OnsdcR");
            FootMime.WriteLine("FWXMVE4JIa6IhtpSEAxK0aoIcnHFqjSzSxlkBqo6bf8Afa1x/wBxV8f7P2pwPoUcDS/sz1Qp7RHU");
            FootMime.WriteLine("s190XaauiD9E7l+6j4nO3F4eU0hZvvDlr9HFvkG5MHLaf8OIZwmFeJn6wx5wL6MY4G6DgOA4DgOA");
            FootMime.WriteLine("4DgOA4DgOA4DgOA4DgclG1fVJ2b2836jegGk1in0jX59sG1HJbbj6JAfOZOpY1akix5S1u60J5zL");
            FootMime.WriteLine("GIkjZ3A9sA1mJFrkFLg0SnADwElBjPWf0830iVPvljss5rt01VgDZHo9MIlWxLow20/RxAtQMrLY");
            FootMime.WriteLine("OwchVNKRxvF5WLgI1TypMW+USuBnnCmwsBZ6wIbOulNqfvVUEWhj3uS8UpO21RHEa+NNcsjR0k2a");
            FootMime.WriteLine("qMZbcWGPMK22DWkr4iBtTZ8se3KlLkY2l90pKuAUT5bIcsmksoUwfq3p5oiTELVkQtTaKUJEarJg");
            FootMime.WriteLine("UytTH4FcTsQmUiKEE0JB5qTAR5DnAsBznsz28DMgbU73WBqzce8LnvVs60zKK7MVtWiCBRiz5TG6");
            FootMime.WriteLine("3Emm8Vmsvc1oYczOyKMoUqFQwJSEqBOhAiwTk3BgB94PdCSrsmNkblb+9L+UySfvVeWldVEasJXq");
            FootMime.WriteLine("0IEWna5GxShVYk+Y105jZJGEyJrfT1SMS4sBQQJ06gzsLBgsIQcD2Ip1Ad4mTXHqNwZz2jtiSOdL");
            FootMime.WriteLine("SmqMwKxHKRrwWGwqMX2lr19C1ykpT8XSNMnYsYwqQiONIAMPaV4ffO8UPMXbk72a6oOnfeYdzr3s");
            FootMime.WriteLine("gzYRqdJ/MK/nsrWPkJ8tG7qfa/NixjY4mrinJofou3gCoGcDKghUYM5OYWPBOSgz3Y7bDZ22N89+");
            FootMime.WriteLine("66m29tzasI6fBerbrbD41b7zRsBl8uriwWiL1VXzmd9pIhFmsE/hQlLkY9uCgkJxocH5UYKHgOQy");
            FootMime.WriteLine("PcLenqQ0908tNIxOLsXx2dW9N7+McLlqy2YNL5jP6xq/FSmV+cVcdMSyRoO+JzsJzRrzUjiS5qcN");
            FootMime.WriteLine("JAVghCEfk8M1hGyO3Op3UR2m0rV7a3xecDb6J2ITtciuCbvkskzNJodqPLL0i8zja97c3dbFHtnf");
            FootMime.WriteLine("mPCcPkDyijSjMiMAIwIBACkKDbDfUvRpZuEXvhsx9pGfbuPa8FQ1RPXZZHz2lzqCQWh9o1BytacY");
            FootMime.WriteLine("etJXsIUuUphRiY0g3ORfk7Mh0X/zE9iv7fF/7gj+Yn+zaH+Ir+3/AJP2X/zb+g4G/fgOA4DgOA4D");
            FootMime.WriteLine("gOA4Hzg66kz501OrIhkVstbolLo7YWTJJhnwDxr11cTP40wrJc1EFl5OciXiuJh8XQACHGVhRpYf");
            FootMime.WriteLine("q9/6A3tdXbrDSqPU/IaCq+kbRgDhcKZGfB9j3V6jwq8l1ZEuTU9pp3R0pgzzIEEx+0zaEgGB+bSm");
            FootMime.WriteLine("tJSzsUFYU48EAWY1v66LbYGsEkve3tcbHgzNV0MWp5pbA3CLIqcm9toWhR8Br6tlrq5t0okMtsF8");
            FootMime.WriteLine("TlgC1Nza4CYS1PirjgoyRKxBz0dFvXaf7Y7xP03yA8DJDYBdMpnkvUELTmtDJbVgUzr6LJz1XjeI");
            FootMime.WriteLine("e5LJPNMuIEwjcnKUjYqz25CAecBmhfT/AOpPB9Ybc0qM0nn8hWS7YyvLRR2cwSSMK4oEEHjExiJ6");
            FootMime.WriteLine("JKZ5ryDg2Pf2iTKSFw1iUKcsBmDywix2BC8iTpt7cVnvj0u3TFSyKVQegaw1gZLYsWOARrYdGJHH");
            FootMime.WriteLine("J3L5JOW/LllWWYrSxcT3gvKgBfdPKCEwOMd7u4CuDr0zN6WGLdTuMH66zRe42jKIAqrQxlNZHdBO");
            FootMime.WriteLine("0KLY5HNFC2OLkTqaQpJKiosLTAGZKOJB2gNAA4Aywhkd+dNvd6X629Lwcf16mjg8U7E5/CLSiQMt");
            FootMime.WriteLine("IJbBnQd9yGVti16ZTHIJuWV8jyjCpMqTiPBgGMYOwSMwoJgS/uXrJ1GHXZfd9NYGpSncmsbkjNoR");
            FootMime.WriteLine("3WuaJWasXU6nCpfNmyQVXMGCVLY2pnrW9VNDi1bFlryoQZEM4eQHDTYLMNCCLl6PO+BvTp1QZkNZ");
            FootMime.WriteLine("qpLZVcWxsJMZZT7U+MS+XxOJXcjqFNHBhCJ7w1LFCFXVRqhehQHHqEwnoGRl4ESryWE+QrSnd/ZP");
            FootMime.WriteLine("fTaHd+wtXZbQDC9UNf2GCDyqQszw9yCczjVaT0NHIZGxJSUSx8VOC97EtyeJIjTEABgsRuTMgAaF");
            FootMime.WriteLine("d2Xpnb0LulBNKexrpNklpoN9YncgIG4jZkEidK7Q0DI4Gsf2ZKpdAFOYU0kkBBQiSx5PyAJg8AyE");
            FootMime.WriteLine("sXA2gfhJ2S/c/L//AJpfwk/qyX+JL9z/AOt/tf8A0f6L+k4HSzwHAcBwHAcBwHAcCgO6vTO1O3zS");
            FootMime.WriteLine("I1N2wxcinjQ3ZaWC2IG4lRqxGht8Qw8ttG4monNnkLWmUHGGEJXZC4EJhmmCICUI0wQg1cGf+v8A");
            FootMime.WriteLine("S2PQF3p+tOoDPG2mHR6C9lVVbNDQa8IY2Lg5VD+LtUekUuZY4ySsZynOROrahQqRhyMIu3vdoQyN");
            FootMime.WriteLine("d0AUVsOEO/FZvBd11xeBtyNqicDh0NiFOQqMNiX6o2iIxROtnEXhjKqKLKLMIakKQzIAZ/O97uCA");
            FootMime.WriteLine("G6TW3V+jdSK1Q1PQUDbIJEEp+V60tMNQtd5E9mJ06VVIZO+rzVDo/PawlKWER55gu4WAJRWCygAL");
            FootMime.WriteLine("CE/cBwHAcBwHAcBwHAcBwHAiS+7dYaBpK2Ltk/dEx1VX0rna5PkzBZjhiNsytyTtKYQs4xla8KyC");
            FootMime.WriteLine("0pAfyjOOCHH054HNv0Ptz7TkuyEppG8dmHzYRbf9DsF/RDL7YjvPU9WzlokMgFK6hbCnd5dzYm64");
            FootMime.WriteLine("ibyBasay/CKTltIOwBfaHAw6KdqXh3j2sGx7+wOjixvzHQtwPDI9s65S2O7O7tleyJa2ujW5IjSF");
            FootMime.WriteLine("je4t6wgBxB5IwGlGgCIIsCxjPA0e9DyWTaZszTbFqz3qdzR+faOmq2STXZywCZVouu+H2ZH0+HCo");
            FootMime.WriteLine("F70+OMnxYCFsbAlgULuwBSQp7D3sB7mMhLfTI6ntibc7NXNW1lukBHB5zFpNdGqaCMBQp5Iy1bEb");
            FootMime.WriteLine("nnVZOEYscJCg1WGdqmpI0upaZUWnUmNn9fAX5dUDwwrRMer3spXV4dQOrZATD3EqKyKzam0nKJja");
            FootMime.WriteLine("VMvdbqi1nVfXrXEnPGD8YlapM23a1PJwDcgDlK1qMjyHAvpCe4z1EtknTpPaubeq3OIiuW2L/j9d");
            FootMime.WriteLine("y9aCKJQR86NOOyU6rFSUgYcH+AhWZirAnBg0Is5wdgRnZ2i7OBf/APETZP8ANN/Cb5po+6D8AH4i");
            FootMime.WriteLine("fJfCivj33k/iK+7XzXxzv+P8I+y/1PKd3ueN+c7e36OBRh83C3wRb7n9PpOjjYH+QXgzXBFbr+w6");
            FootMime.WriteLine("IbCl0QOjjy5yhvPbTFXljp8xSJtLYk7oYX4al3EYAQMAyXngbobema2uKmtCw21sy9OMDrubTNvZ");
            FootMime.WriteLine("wlGnidlsXjTm+JWwJCcwk87K89CErAADCMXf7MZxnszwNHnSx1/s/ZSsqX6htv7tbQy22ZzM5VLn");
            FootMime.WriteLine("SumK0fIa/lxpjl0kiAqxdKuRofhuG8eGoSk4hOajTpFWSggT/mRjUBG969SPduNH7jbVV870yh1b");
            FootMime.WriteLine("0d2yS6wyah3mLmrbBtVK0TSMQeay0U3E5lLY+5jdpKQNoClKwl8Ew3JxJ2UQ/MhOO8vUeufVbqW6");
            FootMime.WriteLine("70iUuiqLVh9qmr57ey92Y0QnKMN1hXfNqWUzE2UKFJRzSwsbocwmnhCEXdAEzPZnv54H76WXUH2M");
            FootMime.WriteLine("3Cd97F1ukR5rZanaajndIsrdGyGlc0Qm449bM7i4X9SA0Zj6aoh7MxGBMMwHOe0Yv8pngYJ0nepX");
            FootMime.WriteLine("sJtLYErZrolkBsWr2PWcu6ZlZ0Sq6RVygpO0Ec1IZnCkpK8KlCiNyhaZBzxP+VafAewooQS8i8M/");
            FootMime.WriteLine("AAzDpSdT6zdyrzuyurePhCdvkMTUXrre0RfDYB5YqkS2jMIC6RWeDQrFAxy5sAFlPLKOCBWJGaJQ");
            FootMime.WriteLine("YHATQ90KrPnWH2Zidu7f0/KcQ9qVRjbcuuNXpOfFEZSCQV/C9sGylrZhC3IloiJDMmmFShsXFDxg");
            FootMime.WriteLine("tWQUFQqGARYis4C6PUV3O3L1KvNlglWxVosWN7UwJprzVYAIymGdCNp8TqJsLo3Thy8x2OUUVwd8");
            FootMime.WriteLine("Pd0vjBxkasvJPYFOQecANmf2O2S/fLEf4dPsd/u7SfxJfvl/Wv2R/wBHf0X+PwLF8BwHAr5tLrXB");
            FootMime.WriteLine("NuqQl9AWa6TBqgk4MYRSIyDvCRiflZEekDZJkSELitangktEe6NBGTweDnJpYcg7cYFntCGTenNq");
            FootMime.WriteLine("uhuegb5gMFIp6e67FyJLFDahQRiDNcqbpOyp484tljpEEcMPlqULMFQQWIRxJ+ALlPeMFkYcgC3N");
            FootMime.WriteLine("hQhmsyAzit5GJaCPWBD5NCH0baeWlcQM0rZVrC6CQKTSVJSdaFCvHkoYizAgM7M5CLGOzIQNX2oN");
            FootMime.WriteLine("ZVfqV+DOHPM7basDX00rUh6+Otw7ASR+eGv5r4oSSHDGBAneQfaVThMpChxkj6gg474MC4ER1z0x");
            FootMime.WriteLine("dR6dnuuVm1FBA1fNtbY2/RZmfoGji0bcbUbpJEUUNdT73Wt8YLXWg6mIEg1AFao0tQWvVHnhFgQ8");
            FootMime.WriteLine("d0PNd+lzq0/2XG7YekU2dJZE9r5tuOyiWP7aY3kWxP0lcJ35AYk+AYEbCvMVQxqiUOR+MFSk7wlA");
            FootMime.WriteLine("w57mA9hs6bevTVqfVumqVbY2ajqKwG+yYopNkjWOYGSFtsl9tNOB2ecRwCFY3Zk0hUBEUBGSPKXA");
            FootMime.WriteLine("Ad/AsZHkPQvrp+1pfd/x/ZdTbWyVSWpH6yaqgC60LbqurSXiANU0eJ8GPyAbQ0Hurq3uEjeMmK04");
            FootMime.WriteLine("1WEp+EyftK75QR8CcHHW2vHPZqO7YqT5Hi0YxT7vSTanKckoYqKGvUoBLVhqppy3CWGvWHUvGAH4");
            FootMime.WriteLine("VBLCV9XJec/W4E9nEkqCTU6gos8g8sZJ5BwAmknEmhyAwo0seBAMLMALOBBzjOM4z2Z4GsSpukxr");
            FootMime.WriteLine("hRF0NtuU5O9jK5YGqWnThNr7G7hXEa7Dkx+BYGvVV8paVS5UWDv58NONyymKDgIAFhAAsIQ/FmdI");
            FootMime.WriteLine("vVC1ruklzyJdciBvn02jtlWlRsestY06/wBsWDFzsHtEwsGvAtpxru8lG5MEPJK5MSYI88WS+8ec");
            FootMime.WriteLine("IYSZsx04tdNsZzOLAtoM3Pe5/ru1azPBDE/Nrc2pIEx3G13m1OrQnVMLiejmKKds5AgrBGmE+VDk");
            FootMime.WriteLine("rJGc5yPgZpRej1H67SC45BWhUpQmXfX9FVpKmxe7olTQ1xbXirTagrxPGEpLQkNbVIYidnzozjFO");
            FootMime.WriteLine("FCrGDAhLx2gyHux/UWtItqRnTBheJ021R91z3UOHZO+N4Z6XEJEkcG92wVIPgmUJTopQuh5WFAUW");
            FootMime.WriteLine("O4EfaEOBYwLARXWfTT1MpazqJt2noETVszoeCvleolcCRRiNFWkxv0ZRxhUsu3LdGyVtiyMkpHha");
            FootMime.WriteLine("ByPOJWCcTBnmjM7cBwGBWH0l9TrOyiPkxNh5dG3aiyNu258QyZqTO6KybXeWl/mrCnUGRs8rFfuL");
            FootMime.WriteLine("rH0BoUGSxKAZRl/1nPYLvBce3Nf4LdMmpGVzA5/KdKBtBPbcGCzOCZElOlSaPPkZLLfij0Cwbg0/");
            FootMime.WriteLine("DpAfnJJYyB5MwEXidmM4yE38D51Pvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB");
            FootMime.WriteLine("78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZH");
            FootMime.WriteLine("Ae/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvWRwHvx/Ny9ZHAe/H83L1kcB78fzcvW");
            FootMime.WriteLine("RwOiv/ny/wCrn/51P+Mn8eX/AJc/65cD/9k=");
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000");
            FootMime.WriteLine("Content-Type: image/gif");
            FootMime.WriteLine("Content-Transfer-Encoding: base64");
            FootMime.WriteLine("Content-Location: http://Word/user.gif");
            FootMime.WriteLine("");
            FootMime.WriteLine(imguser);
            FootMime.WriteLine("");
            FootMime.WriteLine("------=_NextPart_000_0000_01CCD6D1.01BB9000--");


            return FootMime.ToString();
        }




        /// <summary>
        /// Convierte un recurso de imagen en formato de stream.
        /// </summary>
        /// <param name="Image_Url">La url completa que lleva a la imagen</param>
        /// <returns></returns>
        private MemoryStream ConvertImage_FromUrlToStream(string Image_Url)
        {

            string fileName = Image_Url.Substring(Image_Url.LastIndexOf("/") + 1);
            string folderBaseImage = "/images/personas/";
            string FullFileNameRelative = folderBaseImage + fileName;
            string FullFileNameAbsolute = this.Server.MapPath(FullFileNameRelative);

            if (!File.Exists(FullFileNameAbsolute))
            {
                return new MemoryStream();
            }
            else
            {
                byte[] imageInBytesFormat = File.ReadAllBytes(FullFileNameAbsolute);

                using (MemoryStream imageInStreamFormat = new MemoryStream(imageInBytesFormat, 0, imageInBytesFormat.Length))
                {
                    imageInStreamFormat.Write(imageInBytesFormat, 0, imageInBytesFormat.Length);
                    return imageInStreamFormat;
                }
            }


        }

    



        protected void Button1_Click(object sender, EventArgs e)
        {

            //LogController fle = new LogController(); ;

            int legajo = Convert.ToInt32(HiddenFieldlegajo.Value);
            //fle.Log(this.ObjectUsuario.legajo.ToString(), "Exportacion PMPFY", legajo.ToString());
            LogController fle = new LogController(); ;


            fle.Log(this.ObjectUsuario.legajo.ToString(), "Exportación PMP (" + ((Label)FVC.FindControl("Label2")).Text + ") - FY " + LabelPeriodoFiscalDescripcion.Text + "  Evaluado: " + ((Label)FVC.FindControl("Label5")).Text, ((Label)FVC.FindControl("Label6")).Text);


            HtmlContainerControl divLogo = (HtmlContainerControl)FVC.FindControl("logo");


            System.Web.UI.WebControls.Image imgUser = (System.Web.UI.WebControls.Image)FVC.FindControl("Image1");


            divLogo.InnerHtml = "<img ID=\"imagen-logo\" src=\"http://Word/logoprint.jpg\">";
            Exportar.Visible = false;
            btnImprimir.Visible = false;
            Response.Clear();

            Response.Buffer = true;

            Response.AddHeader("content-disposition",

              "attachment;filename=PMP " + ((Label)FVC.FindControl("Label2")).Text + " Legajo " + HiddenFieldlegajo.Value.ToString() + " Fecha " + DateTime.Now.ToShortDateString() + ".doc");

            Response.Charset = "";

            Response.ContentType = "application/vnd.ms-word ";

            StringBuilder sb = new StringBuilder();

            StringWriter sw = new StringWriter(sb);

            HtmlTextWriter hw = new HtmlTextWriter(sw);


            FileInfo fi = new FileInfo(Server.MapPath("css/print/career.css"));
            FileInfo fi1 = new FileInfo(Server.MapPath("css/print/contributors.css"));
            FileInfo fi2 = new FileInfo(Server.MapPath("css/print/estilos.css"));
           // FileInfo fi3 = new FileInfo(Server.MapPath("css/print/managers.css"));



            System.Text.StringBuilder sbCSS = new System.Text.StringBuilder();
            StreamReader sr = fi.OpenText();
            while (sr.Peek() >= 0)
            {
                sbCSS.AppendLine(sr.ReadLine());
            }
            sr.Close();

            //StreamReader sr3 = fi3.OpenText();
            //while (sr3.Peek() >= 0)
            //{
            //    sbCSS.AppendLine(sr3.ReadLine());
            //}
            //sr3.Close();

            StreamReader sr1 = fi1.OpenText();
            while (sr1.Peek() >= 0)
            {
                sbCSS.AppendLine(sr1.ReadLine());
            }
            sr1.Close();

            StreamReader sr2 = fi2.OpenText();
            while (sr2.Peek() >= 0)
            {
                sbCSS.AppendLine(sr2.ReadLine());
            }
            sr2.Close();


            sbCSS.AppendLine("@page Section2 {margin:.5in .5in .5in .5in;mso-header-margin:.5in;mso-footer-margin:.5in;}");
            sbCSS.AppendLine("div.Section2 {page:Section2;}");

            export.RenderControl(hw);

            StringBuilder sb2 = new StringBuilder(sb.ToString().Replace("=", "=3D").Replace("FVC_", ""));


            System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<(input|INPUT)[^>]*?>");
            string HTML;
            HTML = regEx.Replace(sb2.ToString(), "");


            System.Text.RegularExpressions.Regex regExForm = new System.Text.RegularExpressions.Regex("<(form|FORM)[^>]*?>");
            HTML = regExForm.Replace(HTML, "");
            HTML = HTML.Replace("</FORM>", "");
            HTML = HTML.Replace("</form>", "");


            Response.Output.Write(getHeaderMime() + "<style type=3D\"text/css\">" + sbCSS.ToString().Replace("=", "=3D") + "</style>" + HTML + getFootMime());

            Response.Flush();

            Response.End();


        }

        protected void RepeaterIndividualObjetives3_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives3.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                   RepeaterIndividualObjetives3.Visible = true;


            }

        }

        protected void RepeaterIndividualObjetives4_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives4.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives4.Visible = true;


            }

        }

        protected void RepeaterIndividualObjetives5_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives5.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives5.Visible = true;

            }
        }

        protected void RepeaterIndividualObjetives6_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives6.Visible = false;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives6.Visible = true;
            }
        }

        protected void RepeaterIndividualObjetives7_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives7.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives7.Visible = true;
            }

        }

        protected void RepeaterIndividualObjetives8_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives8.Visible = false;


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives8.Visible = true;
            }
        }

        protected void RepeaterIndividualObjetives9_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives9.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives9.Visible = true;
            }

        }

        protected void RepeaterIndividualObjetives10_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
                RepeaterIndividualObjetives10.Visible = false;

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (!string.IsNullOrEmpty(((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString()) && ((System.Data.DataRowView)e.Item.DataItem).Row.ItemArray[3].ToString() != "")
                    RepeaterIndividualObjetives10.Visible = true;
            }
        }

        protected void Imprimir_Click(object sender, EventArgs e)
        {
            LogController fle = new LogController(); ;


            fle.Log(this.ObjectUsuario.legajo.ToString(), "Impresión PMP (" + ((Label)FVC.FindControl("Label2")).Text + ") - FY " + LabelPeriodoFiscalDescripcion.Text + "  Evaluado: " + ((Label)FVC.FindControl("Label5")).Text, ((Label)FVC.FindControl("Label6")).Text);


        }

  



    }
}
