﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Net.Mail;

namespace com.paginar.johnson.utiles
{
    public class Message
    {
        private string _fromAddress;
        private string _toAddress;
        private bool _isBodyHtml;
        private string _subject;
        private string _body;
        private string _smtpAddress;
        private int _port;
        private string _userName;
        private string _password;
        private bool _sslEnabled;
        private ArrayList _attachments;
        Settings Config = new Settings();

        public string FromAddress
        {
            get { return _fromAddress; }
            set { _fromAddress = value; }
        }
        public string ToAddress
        {
            get { return _toAddress; }
            set { _toAddress = value; }
        }
        public bool IsBodyHtml
        {
            get { return _isBodyHtml; }
            set { _isBodyHtml = value; }
        }
        public string subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
        public string body
        {
            get { return _body; }
            set { _body = value; }
        }
        public string SmtpAddress
        {
            get { return _smtpAddress; }
            set { _smtpAddress = value; }
        }
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public bool SslEnabled
        {
            get { return _sslEnabled; }
            set { _sslEnabled = value; }
        }

        #region "Constructors"
        public Message(string From, string To, string Subject, string Body)
        {
            UserName = Config.From;
            FromAddress = From;
            ToAddress = To;
	        IsBodyHtml = true;
            subject = Subject;
            body = Body;
            SmtpAddress = Config.Smtp;
            Port = Config.Port;
	        UserName = null;
	        Password = null;
	        SslEnabled = false;
            UserName = Config.From;
            Password = Config.Password;
        }

        public Message( string To, string Subject, string Body)
        {            
            ToAddress = To;
            IsBodyHtml = true;
            subject = Subject;
            body = Body;
            SmtpAddress = Config.Smtp;
            Port = Config.Port;
            UserName = null;
            Password = null;
            SslEnabled = false;
            UserName = Config.From;
            FromAddress = Config.From;
            Password = Config.Password;
        }
        #endregion

        #region "Methods"


        public void AddAttachment(string attachment)
        {
            _attachments.Add(attachment);

        }


        public void RemoveAttachment(string attachment)
        {
            _attachments.Remove(attachment);

        }

        //public bool Send(Message _emailMessage)
            public bool Send()
        {

            bool mailSent = false;

            try
            {
                System.Net.Mail.MailMessage Mail = new System.Net.Mail.MailMessage();

                var _with1 = Mail;
                _with1.To.Add(ToAddress);
                _with1.From = new MailAddress(FromAddress);
                _with1.Subject = subject;
                _with1.Body = body;
                _with1.IsBodyHtml = IsBodyHtml;

                if ((_attachments!=null) && (!(_attachments.Count == 0)))
                {
                    //int i = 0;

                    foreach (string objAttachment in _attachments)
                    {
                        //_with1.Attachments.Add( new a _attachments[i]);
                        _with1.Attachments.Add(new Attachment(objAttachment));
                        //i += 1;
                    }
                }


                SmtpClient SMTP = new SmtpClient(SmtpAddress);

                if (!((UserName == null) && (Password == null)))
                {
                    SMTP.Credentials = new System.Net.NetworkCredential(UserName, Password);

                }

                SMTP.Port = Port;
                SMTP.Host = SmtpAddress;
                SMTP.EnableSsl = SslEnabled;
                SMTP.Send(Mail);
                SMTP = null;
                Mail.Dispose();
                mailSent = true;

            }
            catch (Exception ex)
            {
                mailSent = false;
            }

            return mailSent;

        }
        #endregion



    }
}
