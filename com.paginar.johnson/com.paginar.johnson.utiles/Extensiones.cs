﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.paginar.johnson.utiles
{
    public static class Extensiones
    {
        public static string Truncate(this string text, int length, string ellipsis, bool keepFullWordAtEnd)
        {
            if (string.IsNullOrEmpty( text)) return string.Empty;

            if (text.Length < length) return text;

            text = text.Substring(0, length);

            if (keepFullWordAtEnd)
            {
                text = text.Substring(0, text.LastIndexOf(' '));
            }

            return text + ellipsis;
        }
        public static bool Validar(this DateTime fecha, string texto)
        {
            DateTime aux = DateTime.MinValue;
            return DateTime.TryParse(texto, out aux);

        }

    }
}
