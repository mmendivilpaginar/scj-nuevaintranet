﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    [DataObject]
    public class FormulariosController
    {
        FormulariosDS FDS = new FormulariosDS();
        
        public FormulariosController() { }
                
        public string GetCalificacionByPuntos(int PuntosCompetencias, int PuntosObjetivos,int TipoFormularioID)
        {
            return (string)FachadaDA.Singleton.FormularioPmpOperarios.GetCalificacionByPuntos(PuntosCompetencias, PuntosObjetivos,TipoFormularioID);

            //return FachadaDA.Singleton.FormularioPmpOperarios.GetCalificacionByPuntos(Puntos, TipoFormularioID);
        }

        public string GetTimeInJobByID(int PeriodoID, int TipoFormularioID, int Legajo)
        {
            return (string)FachadaDA.Singleton.fyCVExperienceTableAdapter.GetTimeInJobByID(PeriodoID, TipoFormularioID,Legajo);
        }
        

        public FormulariosController(int PeriodoID, int legajo, int TipoFormularioID)
        {
            FachadaDA.Singleton.FormularioPmpOperarios.FillByID(FDS.FormularioPmpOperarios, legajo, PeriodoID, TipoFormularioID);
            if (FDS.FormularioPmpOperarios.Rows.Count == 0)
            {
                FormulariosDS.FormularioPmpOperariosRow FRow = FDS.FormularioPmpOperarios.NewFormularioPmpOperariosRow();
                FRow.Legajo = legajo;
                FRow.TipoFormularioID = TipoFormularioID;
                FRow.PeriodoID = PeriodoID;
                FRow.FAlta = DateTime.Now;
                FDS.FormularioPmpOperarios.AddFormularioPmpOperariosRow(FRow);


                if (TipoFormularioID == 11 || TipoFormularioID == 10 || TipoFormularioID == 12)
                {
                    FachadaDA.Singleton.fyCVTrackingTableAdapter.FillByTramiteID(FDS.fyCVTracking,legajo,TipoFormularioID,PeriodoID);
                        if(FDS.fyCVTracking.Rows.Count==0)
                        {
                            FormulariosDS.fyCVTrackingRow fyCVTrackingRow = FDS.fyCVTracking.NewfyCVTrackingRow();
                            fyCVTrackingRow.CVTrackingID= Guid.NewGuid();
                            fyCVTrackingRow.Descripcion="Tracking desde pmp";
                            fyCVTrackingRow.Fecha= DateTime.Now;
                            fyCVTrackingRow.TrackingActivo = false;
                            fyCVTrackingRow.SetParentRow(FDS.FormularioPmpOperarios.Rows[0]);
                            FDS.fyCVTracking.AddfyCVTrackingRow(fyCVTrackingRow);


                        
                        }

                    FachadaDA.Singleton.fyCVHeaderTableAdapter.FillByLegajo(FDS.fyCVHeader,legajo);
                        if (FDS.fyCVHeader.Rows.Count == 0)
                        {
                            FormulariosDS.fyCVHeaderRow fyCVHeaderRow = FDS.fyCVHeader.NewfyCVHeaderRow();
                            fyCVHeaderRow.CreateDate = DateTime.Now;
                            fyCVHeaderRow.CVHeaderID = Guid.NewGuid();
                            fyCVHeaderRow.SetParentRow(FDS.fyCVTracking.Rows[0]);
                            fyCVHeaderRow.Legajo = legajo;
                            FDS.fyCVHeader.AddfyCVHeaderRow(fyCVHeaderRow);

                        }


                
                }
            }
            else
            {

                if (TipoFormularioID == 11 || TipoFormularioID == 10 || TipoFormularioID == 12)
                {
                    FachadaDA.Singleton.fyCVTrackingTableAdapter.FillByTramiteID(FDS.fyCVTracking, legajo, TipoFormularioID, PeriodoID);
                    if (FDS.fyCVTracking.Rows.Count == 0)
                    {
                        FormulariosDS.fyCVTrackingRow fyCVTrackingRow = FDS.fyCVTracking.NewfyCVTrackingRow();
                        fyCVTrackingRow.CVTrackingID = Guid.NewGuid();
                        fyCVTrackingRow.Descripcion = "Tracking desde pmp";
                        fyCVTrackingRow.Fecha = DateTime.Now;
                        fyCVTrackingRow.TrackingActivo = false;
                        fyCVTrackingRow.SetParentRow(FDS.FormularioPmpOperarios.Rows[0]);
                        FDS.fyCVTracking.AddfyCVTrackingRow(fyCVTrackingRow);


                    }

                    FachadaDA.Singleton.fyCVHeaderTableAdapter.FillByLegajo(FDS.fyCVHeader, legajo);
                    if (FDS.fyCVHeader.Rows.Count == 0)
                    {
                        FormulariosDS.fyCVHeaderRow fyCVHeaderRow = FDS.fyCVHeader.NewfyCVHeaderRow();
                        fyCVHeaderRow.CreateDate = DateTime.Now;
                        fyCVHeaderRow.CVHeaderID = Guid.NewGuid();
                        fyCVHeaderRow.SetParentRow(FDS.fyCVTracking.Rows[0]);
                        fyCVHeaderRow.Legajo = legajo;
                        FDS.fyCVHeader.AddfyCVHeaderRow(fyCVHeaderRow);

                    }
                    FachadaDA.Singleton.fyCVEducationTableAdapter.FillByID(FDS.fyCVEducation, (Guid)FDS.fyCVTracking.Rows[0]["cvTrackingid"], (Guid)FDS.fyCVHeader.Rows[0]["cvHeaderid"]);

                    foreach (FormulariosDS.fyCVEducationRow EducationRw in FDS.fyCVEducation)
                    {
                        EducationRw.Delete();
                    }

                    FachadaDA.Singleton.fyCVLanguageTableAdapter.FillByID(FDS.fyCVLanguage, (Guid)FDS.fyCVTracking.Rows[0]["cvTrackingid"], (Guid)FDS.fyCVHeader.Rows[0]["cvHeaderid"]);

                    foreach (FormulariosDS.fyCVLanguageRow LanguageRw in FDS.fyCVLanguage)
                    {
                        LanguageRw.Delete();
                    }

                    FachadaDA.Singleton.fyCVExperienceTableAdapter.FillByID(FDS.fyCVExperience, (Guid)FDS.fyCVTracking.Rows[0]["cvTrackingid"], (Guid)FDS.fyCVHeader.Rows[0]["cvHeaderid"]);

                    foreach (FormulariosDS.fyCVExperienceRow ExperienceRw in FDS.fyCVExperience)
                    {
                        ExperienceRw.Delete();
                    }

                    FachadaDA.Singleton.eval_RelFormularioPmpOperariosCursosTableAdapter.FillByID(FDS.eval_RelFormularioPmpOperariosCursos, PeriodoID, TipoFormularioID, legajo);
                    foreach (FormulariosDS.eval_RelFormularioPmpOperariosCursosRow dr in FDS.eval_RelFormularioPmpOperariosCursos.Rows)
                    {
                        dr.Delete();
                    }

                }

                FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.FillByID(FDS.RelFormularioPmpOperariosItemsEvaluacion, legajo, PeriodoID, TipoFormularioID);
                foreach (FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow dr in FDS.RelFormularioPmpOperariosItemsEvaluacion.Rows)
                {
                    dr.Delete();
                }
                FachadaDA.Singleton.RelFormularioPmpOperariosAreasDeOperaciones.FillByID(FDS.RelFormularioPmpOperariosAreasDeOperaciones, legajo, PeriodoID, TipoFormularioID);
                foreach (FormulariosDS.RelFormularioPmpOperariosAreasDeOperacionesRow dr in FDS.RelFormularioPmpOperariosAreasDeOperaciones.Rows)
                {
                    dr.Delete();
                }

                
            }
        }

        public string GetComentarioJefeTurno()
        {
            return (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).ComentarioJefeTurno.ToString();        
        }

        public void SetComentarioJefeTurno(string ComentarioJefeTurno)
        {
            (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).ComentarioJefeTurno = ComentarioJefeTurno;
        }

        public void SetComentarioEvaluado(string ComentarioEvaluado)
        {
            (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).ComentarioEvaluado = ComentarioEvaluado;
        }

        public void SetComentarioJefeDePlanta(string ComentarioJefeDePlanta)
        {
            (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).ComentarioJefeDePlanta = ComentarioJefeDePlanta;
        }

        public void SetDatosCalibracion(int UsuarioID)
        {
            (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).fcalibracion = DateTime.Now;
            (FDS.FormularioPmpOperarios.Rows[0] as FormulariosDS.FormularioPmpOperariosRow).usuariocalibracion = UsuarioID; 

        }

        public string DameJobGrade(int Legajo, int PeriodoID, int TipoFormularioID)
        {
            
           return FachadaDA.Singleton.Evaluados.GetJobGrade(Legajo, PeriodoID, TipoFormularioID);
        }

        public void SetLineaEvaluado(int Legajo,int PeriodoID, int TipoFormularioID, string Linea)
        {
            FormulariosDS.EvaluadosRow RowEval;// = new FormulariosDS.EvaluadosRow();
            FormulariosDS.EvaluadosDataTable DTEvaluados = new FormulariosDS.EvaluadosDataTable();
           //   FDS.Evaluados.FindByLegajoPeriodoIDTipoFormularioID(Legajo, PeriodoID, TipoFormularioID);
            FachadaDA.Singleton.Evaluados.FillByID(DTEvaluados, PeriodoID, TipoFormularioID, Legajo);
            if(DTEvaluados.Rows.Count>0)          
              {
                  RowEval = (FormulariosDS.EvaluadosRow)DTEvaluados.Rows[0];
                  RowEval.Linea = Linea;
                  FachadaDA.Singleton.Evaluados.Update(RowEval);        
              }
             
        }


        public void SaveHistorial(int PasoID, int PeriodoID, int legajo, int TipoFormularioID)
        {
            int? PasoActualIDAux = GetPasoIDActual(PeriodoID, legajo, TipoFormularioID);
            int PasoActualID = (PasoActualIDAux == null) ? -1 : ((int)PasoActualIDAux);
            
            if ((PasoActualID == -1) || (PasoActualID != PasoID))
            {
                FachadaDA.Singleton.Historial.FillByID(FDS.Historial, legajo, PeriodoID, TipoFormularioID);
                FormulariosDS.HistorialRow HRow = FDS.Historial.NewHistorialRow();
                HRow.PasoID = PasoID;
                HRow.Legajo = legajo;
                HRow.PeriodoID = PeriodoID;
                HRow.TipoFormularioID = TipoFormularioID;
                HRow.Fecha = DateTime.Now;
                FDS.Historial.AddHistorialRow(HRow);
                FachadaDA.Singleton.Historial.Update(FDS.Historial);
            }

        }

        public bool? ValidarPeriodoCarga(int PeriodoID,DateTime Fecha)
        {
            return FachadaDA.Singleton.Periodo.ISEnPeriodoDeCarga(PeriodoID, Fecha);
        }

        public int? GetPasoIDActual(int PeriodoID, int legajo, int TipoFormularioID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetPasoIDActual(legajo, PeriodoID, TipoFormularioID);
        }

       
        public bool SiPasoActualEs(int PasoID, int PeriodoID, int legajo, int TipoFormularioID)
        {
            int? PasoActualIDAux = GetPasoIDActual(PeriodoID, legajo, TipoFormularioID);
            int PasoActualID = (PasoActualIDAux == null) ? -1 : ((int)PasoActualIDAux);
            if ((PasoActualID == -1) || (PasoActualID != PasoID))
                return false;
            else return true;

        }
        public void SaveItemEvaluacion(int ItemEvaluacionID,int Ponderacion, string Fundamentacion)
        {
            FormulariosDS.RelFormularioPmpOperariosItemsEvaluacionRow RFI = FDS.RelFormularioPmpOperariosItemsEvaluacion.NewRelFormularioPmpOperariosItemsEvaluacionRow();
            RFI.SetParentRow(FDS.FormularioPmpOperarios.Rows[0]);
            RFI.ItemEvaluacionID = ItemEvaluacionID;
            RFI.Ponderacion = Ponderacion;
            RFI.Fundamentacion = Fundamentacion;
            FDS.RelFormularioPmpOperariosItemsEvaluacion.AddRelFormularioPmpOperariosItemsEvaluacionRow(RFI);
        }
        public void AsociarEvaluacion(int Legajo, int PeriodoID, int TipoFormularioID, int PasoID, int LegajoEvaluador)
        {
            FachadaDA.Singleton.Evaluados.FillByID(FDS.Evaluados, PeriodoID, TipoFormularioID, Legajo);
            FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByID(FDS.RelEvaluadosEvaluadores, Legajo, PeriodoID, TipoFormularioID, PasoID);
            foreach (FormulariosDS.RelEvaluadosEvaluadoresRow row in FDS.RelEvaluadosEvaluadores)
            {
                row.Delete();
            }

            FormulariosDS.RelEvaluadosEvaluadoresRow RowREE = FDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
            RowREE.Legajo = Legajo;
            RowREE.PeriodoID = PeriodoID;
            RowREE.TipoFormularioID = TipoFormularioID;
            RowREE.PasoID = PasoID;
            RowREE.LegajoEvaluador = LegajoEvaluador;
            FDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RowREE);
            FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(FDS.RelEvaluadosEvaluadores);
        }

        public void SaveAreaOperacion(int AreaDeOperacionID, string Observacion)
        {
            FormulariosDS.RelFormularioPmpOperariosAreasDeOperacionesRow RFA = FDS.RelFormularioPmpOperariosAreasDeOperaciones.NewRelFormularioPmpOperariosAreasDeOperacionesRow();
            RFA.SetParentRow(FDS.FormularioPmpOperarios.Rows[0]);
            RFA.AreaDeOperacionID = AreaDeOperacionID;
            
            RFA.Observacion = Observacion;
            FDS.RelFormularioPmpOperariosAreasDeOperaciones.AddRelFormularioPmpOperariosAreasDeOperacionesRow(RFA);
        }

        public void SaveCursos(int CursoID)
        {
            FormulariosDS.eval_RelFormularioPmpOperariosCursosRow RCT = FDS.eval_RelFormularioPmpOperariosCursos.Neweval_RelFormularioPmpOperariosCursosRow();
            RCT.SetParentRow(FDS.FormularioPmpOperarios.Rows[0]);
            RCT.CursoID = CursoID;
           
            FDS.eval_RelFormularioPmpOperariosCursos.Addeval_RelFormularioPmpOperariosCursosRow(RCT);
        }

        public void SaveEducation(string Degree, string year, string School)
        {
            FormulariosDS.fyCVEducationRow RCT = FDS.fyCVEducation.NewfyCVEducationRow();
            
            RCT.SetParentRow(FDS.fyCVHeader.Rows[0]);
            RCT.SetParentRow(FDS.fyCVTracking.Rows[0]);
            RCT.Degree = Degree;
            RCT.Year =  (string.IsNullOrEmpty(year) ? null : year);
            RCT.School = School;
            RCT.CreateDate = DateTime.Now;
            FDS.fyCVEducation.AddfyCVEducationRow(RCT);
        }

        public void SaveLanguage(string Idioma, bool IsNative, string Proficiency)
        {
            FormulariosDS.fyCVLanguageRow RCT = FDS.fyCVLanguage.NewfyCVLanguageRow();
            RCT.SetParentRow(FDS.fyCVHeader.Rows[0]);
            RCT.SetParentRow(FDS.fyCVTracking.Rows[0]);
            RCT.Idioma = Idioma;
            RCT.IsNative = IsNative;
            RCT.Proficiency = (string.IsNullOrEmpty(Proficiency) ? null : Proficiency); ;
            RCT.CreateDate = DateTime.Now;
            FDS.fyCVLanguage.AddfyCVLanguageRow(RCT);
        }

        public void SaveExperience(string FromDate, string ToDate, string Company, string Position, string Description)
        {
            FormulariosDS.fyCVExperienceRow RCT = FDS.fyCVExperience.NewfyCVExperienceRow();
            RCT.SetParentRow(FDS.fyCVHeader.Rows[0]);
            RCT.SetParentRow(FDS.fyCVTracking.Rows[0]);
            RCT.FromDate = FromDate;
            RCT.ToDate = ToDate;
            RCT.Company = Company;
            RCT.Position = Position;            
            RCT.Description = Description;
            RCT.CreateDate = DateTime.Now;
            FDS.fyCVExperience.AddfyCVExperienceRow(RCT);
        }


        public void GrabarEvaluador()
        {
            FachadaDA.Singleton.FormularioPmpOperarios.Update(FDS);
            FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.Update(FDS);
        }

        public void GrabarEvaluadorFY()
        {
            FachadaDA.Singleton.FormularioPmpOperarios.Update(FDS);
            FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.Update(FDS);
            FachadaDA.Singleton.eval_RelFormularioPmpOperariosCursosTableAdapter.Update(FDS);
        }

        public void GrabarEvaluadoFY()
        {
            FachadaDA.Singleton.FormularioPmpOperarios.Update(FDS);
            FachadaDA.Singleton.fyCVTrackingTableAdapter.Update(FDS);
            FachadaDA.Singleton.fyCVHeaderTableAdapter.Update(FDS);

            FachadaDA.Singleton.RelFormularioPmpOperariosItemsEvaluacion.Update(FDS);
            FachadaDA.Singleton.eval_RelFormularioPmpOperariosCursosTableAdapter.Update(FDS);
            FachadaDA.Singleton.fyCVEducationTableAdapter.Update(FDS);
            FachadaDA.Singleton.fyCVExperienceTableAdapter.Update(FDS);

            FachadaDA.Singleton.fyCVLanguageTableAdapter.Update(FDS);
            
        }
        public void GrabarEvaluado()
        {
            FachadaDA.Singleton.FormularioPmpOperarios.Update(FDS);
            FachadaDA.Singleton.RelFormularioPmpOperariosAreasDeOperaciones.Update(FDS);
        }


        public void GrabarJP()
        {
            FachadaDA.Singleton.FormularioPmpOperarios.Update(FDS);            
        }

        

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.CabeceraFormularioDataTable GetCabecera(int PeriodoID, int legajo, int TipoFormularioID, int? LegajoEvaluador)
        {
            int PasoID = 1; //Sirve para recuperar el evaluador en cada Paso, en este caso el evaluador es el encargado en el primer Paso
            return FachadaDA.Singleton.CabeceraFormulario.GetData(PasoID, legajo, PeriodoID, TipoFormularioID, LegajoEvaluador);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.ItemEvaluacionDataTable GetItemEvaluacion(int SeccionID,int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.ItemEvaluacion.GetDataByEvaluadoSeccionID(SeccionID, legajo,PeriodoID, TipoFormularioID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.AreasDeOperacionesDataTable GetAreasOperaciones( int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.AreasDeOperaciones.GetDataByFormulario(legajo, TipoFormularioID, PeriodoID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.CursosDataTable GetCursosByTramiteID(int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.CursosTableAdapter.GetDataByID(TipoFormularioID, legajo,PeriodoID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.CursosDataTable GetCursosDisponiblesByTramiteID(int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.CursosTableAdapter.GetCursosDisponiblesByTramiteID(TipoFormularioID, legajo, PeriodoID);

        }




        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.fyCVEducationGetByTramiteIDDataTable GetEducationByTramiteID(int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.fyCVEducationGetByTramiteIDTableAdapter.GetData(legajo, TipoFormularioID, PeriodoID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.fyCVExperienceGetByTramiteIDDataTable GetExperienceByTramiteID(int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.fyCVExperienceGetByTramiteIDTableAdapter.GetData(legajo, TipoFormularioID, PeriodoID);

        }

        public FormulariosDS.fyCVLanguageGetByTramiteIDDataTable GetLanguageByTramiteID(int? PeriodoID, int? legajo, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.fyCVLanguageGetByTramiteIDTableAdapter.GetData(legajo, TipoFormularioID, PeriodoID);

        }



        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.PeriodoDataTable GetPeriodo(int PeriodoID)
        {
            return FachadaDA.Singleton.Periodo.GetDataByID(PeriodoID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public string GetTipoFormulario(int TipoFormularioID)
        {
            string elTipo = string.Empty;
            FormulariosDS.TipoFormularioDataTable formulario = new FormulariosDS.TipoFormularioDataTable();
            formulario = FachadaDA.Singleton.TipoFormulario.GetDataByID(TipoFormularioID);
            foreach (FormulariosDS.TipoFormularioRow item in formulario.Rows)
            {
                elTipo = item.Descripcion;
            }
            return elTipo;
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public string GetPeriodoPMPMY(int PeriodoID)
        {
            string elPeriodo = string.Empty;
            FormulariosDS.PeriodoDataTable periodo = new FormulariosDS.PeriodoDataTable();
            periodo = FachadaDA.Singleton.Periodo.GetDataByID(PeriodoID);
            foreach (FormulariosDS.PeriodoRow item in periodo.Rows)
	            {
		            elPeriodo = item.Descripcion;
	            }

            return elPeriodo;

        }

        
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetFormulariosAnteriores(int PeriodoID, int legajo, int TipoFormularioID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetDataFormulariosAnteriores(legajo, TipoFormularioID, PeriodoID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.HistorialDataTable GetHistorial(int PeriodoID, int legajo, int TipoFormularioID)
        {
            return FachadaDA.Singleton.Historial.GetDataByID(legajo, PeriodoID, TipoFormularioID);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetFormulariosByLegajoTipoPeriodo(int legajo, int PeriodoID, string Modo)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetFormulariosByLegajoTipoPeriodo(legajo, PeriodoID, Modo);

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetHistoricoByLegajo(int legajo, int periodoActual, int? TipoPeriodoID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetHistoricoByLegajo(legajo, periodoActual, TipoPeriodoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetHistoricoActualByLegajo(int legajo, int periodoActual, int? TipoPeriodoID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetHistoricoActualByLegajo(legajo, TipoPeriodoID, periodoActual);
        }



        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public int? GetCantidadHistoricoEnPeriodo(int legajo, int periodo, int tipoformulario)
        {

            return FachadaDA.Singleton.Historial.GetCantHistorial(legajo, periodo, tipoformulario);

        }

        public void ActualizarEvaluador(object LegajoNuevoEvaluadro, object Legajo,object PeriodoID,object TipoFormularioID,object PasoID)
        {
            FachadaDA.Singleton.RelEvaluadosEvaluadores.UpdateEvaluador(int.Parse(LegajoNuevoEvaluadro.ToString()), int.Parse(Legajo.ToString()), int.Parse(PeriodoID.ToString()), int.Parse(TipoFormularioID.ToString()), int.Parse(PasoID.ToString()));                
        }

        public FormulariosDS.UbicacionEvaluadoDataTable GetEvaluadoByLegajoPeriodoID(int Legajo, int PeriodoID)
        {
            return FachadaDA.Singleton.UbicacionEvaluadoTableAdapter.GetUbicacionByLegajoPeriodoID(Legajo, PeriodoID);
        }

        public bool ExisteEvaluacion(int PeriodoID, int legajo, int TipoFormularioID)
        {
            FormulariosDS.EvaluadosDataTable EvaDT = new FormulariosDS.EvaluadosDataTable();
            EvaDT= FachadaDA.Singleton.Evaluados.GetDataByID(PeriodoID, TipoFormularioID, legajo);
            if (EvaDT.Rows.Count > 0)
                return true;
            else
                return false;
        }

        public int DameTipoPeriodoByPeriodoID(int PeriodoID)
        {
            FormulariosDS.eval_PeriodoDataTable PeriodoDT = new FormulariosDS.eval_PeriodoDataTable();
            PeriodoDT = FachadaDA.Singleton.eval_PeriodoTableAdapter.GetDataByPeriodoID(PeriodoID);
            if (PeriodoDT.Rows.Count > 0)
                return PeriodoDT[0].TipoPeriodoID;
            else
                return 0;
        }
    }
}
