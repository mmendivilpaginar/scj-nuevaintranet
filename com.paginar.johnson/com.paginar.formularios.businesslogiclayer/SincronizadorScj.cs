﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;
using System.Data;


namespace com.paginar.formularios.businesslogiclayer
{
    [DataObject]
    public class SincronizadorScj : Sincronizador
    {
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.PeriodoDataTable GetPeriodos()
        {
            return this.getPeriodos();
        }

        public int GetTipoPeriodoID(int TipoFormularioID)
        {
            FormulariosDS.TipoFormularioDataTable TipoFormularioDt = new FormulariosDS.TipoFormularioDataTable();
            FachadaDA.Singleton.TipoFormulario.FillByID(TipoFormularioDt, TipoFormularioID);
            if (TipoFormularioDt.Rows.Count > 0)
                return ((TipoFormularioDt.Rows[0]) as FormulariosDS.TipoFormularioRow).TipoPeriodoID;
            return 0;

        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.TipoFormularioDataTable GetTiposFormularios()
        {
            return this.getTiposFormularios();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.TipoFormularioDataTable GetTiposFormulariosByTipoPeriodoID(int TipoPeriodoID)
        {
            return this.getTiposFormulariosByTipoPeriodoID(TipoPeriodoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.vwUsuariosDataTable GetUsuarios()
        {
            return this.getUsuarios();
        }

        public DataView GetUsuarios(int Legajo)
        {
            return this.getUsuariosAuditores(Legajo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.PeriodoDataTable GetPeriodoByID(int PeriodoID)
        {
            return this.getPeriodoByID(PeriodoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public override DataTable GetUsuariosImportacion()
        {
            DataView dv = new DataView();
            dv = FormDS.Evaluados.DefaultView;           
            dv.Sort = "ApellidoNombre asc";
            return dv.ToTable();         
        }

        public override void BuscarUsuarios(int? PeriodoID, int? TipoFormularioID, string MODO, int? Cluster)
        {            
            FormDS.RelEvaluadosEvaluadores.Clear();
            FachadaDA.Singleton.Evaluados.FillUsuariosImportacion(FormDS.Evaluados, PeriodoID, TipoFormularioID, MODO, Cluster);
         
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public override FormulariosDS.AreasDataTable GetAreas()
        {
            return FachadaDA.Singleton.Areas.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public override FormulariosDS.CargoDataTable GetCargos()
        {
            return FachadaDA.Singleton.Cargo.GetData();
        }

        public override void DeleteUsuariosModuloImportacion(int legajo,int PeriodoID,int TipoFormularioID)
        {            
            FormulariosDS.EvaluadosRow EvalRow = FormDS.Evaluados.FindByLegajoPeriodoIDTipoFormularioID(legajo,PeriodoID,TipoFormularioID);
            if (!(EvalRow == null))
            {
                EvalRow.Delete();
            }
            FormulariosDS.RelEvaluadosEvaluadoresDataTable RelEvaluadosEvaluadoresDt = new FormulariosDS.RelEvaluadosEvaluadoresDataTable();
            FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByFormularioID(RelEvaluadosEvaluadoresDt, legajo, PeriodoID, TipoFormularioID);
            foreach (FormulariosDS.RelEvaluadosEvaluadoresRow item in RelEvaluadosEvaluadoresDt.Rows)
            {
                item.Delete();
            }
            FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(RelEvaluadosEvaluadoresDt);
            FachadaDA.Singleton.Evaluados.Update(EvalRow);
        }

        public override void InsertarUsuariosModuloImportacion(int ID, int PeriodoID, int TipoFormularioID)
        {            
            FormulariosDS.EvaluadosRow RowUMI = FormDS.Evaluados.NewEvaluadosRow();
            FormulariosDS.vwUsuariosRow RowvwU = VwEvaluadosDisponibles.FindByLegajo(ID);
            
            RowUMI.Legajo = RowvwU.Legajo;
            RowUMI.PeriodoID = PeriodoID;
            RowUMI.TipoFormularioID = TipoFormularioID;
            RowUMI.Apellido = RowvwU.Apellido;
            RowUMI.Nombre = RowvwU.Nombre;
            RowUMI.CargoID = RowvwU.IsCargoIDNull() ? -1 : RowvwU.CargoID;
            RowUMI.AreaID = RowvwU.IsAreaIDNull() ? -1 : RowvwU.AreaID;
            RowUMI.ApellidoNombre = RowvwU.Apellido + ", " + RowvwU.Nombre;
            RowUMI.EstadoID = "-1";
            RowUMI.Estado = "No Iniciado";
            RowUMI.LT = -1;
            RowUMI.JP = -1;
            //Nuevos Campos
            RowUMI.JobGrade = RowvwU.JobGrade;
            RowUMI.DireccionID = RowvwU.DireccionID;
            RowUMI.Direccion = RowvwU.Direccion;
            RowUMI.LegajoReal = RowvwU.legajoWD;
            //
            VwEvaluadosDisponibles.Rows.Remove(RowvwU);    
            FormDS.Evaluados.AddEvaluadosRow(RowUMI);
            FormulariosDS.UsuariosModuloImportacionDataTable DTUmi = new FormulariosDS.UsuariosModuloImportacionDataTable();
            FormDS.UsuariosModuloImportacion.DefaultView.Sort = "Evaluado";
            DTUmi.Load(FormDS.UsuariosModuloImportacion.DefaultView.ToTable().CreateDataReader());
            FormDS.UsuariosModuloImportacion.Clear();
            FormDS.UsuariosModuloImportacion.Load(DTUmi.DefaultView.ToTable().CreateDataReader());
        }

        public void GetVwUsuarios()
        {
            FachadaDA.Singleton.vwUsuarios.Fill(FormDS.vwUsuarios);
            
        }

        public void GetVwUsuariosDisponibles(int PeriodoID, int TipoFormularioID, string MODO, int Cluster)
        {
            FachadaDA.Singleton.vwUsuarios.FillUsuariosDisponibles(VwEvaluadosDisponibles, PeriodoID, Cluster);
            foreach (FormulariosDS.EvaluadosRow evaluado in FormDS.Evaluados.Rows)
            {
                if (evaluado.RowState != DataRowState.Deleted)
                {
                    FormulariosDS.vwUsuariosRow RowvwUsuario = VwEvaluadosDisponibles.FindByLegajo(evaluado.Legajo);
                    if (RowvwUsuario != null)
                        VwEvaluadosDisponibles.RemovevwUsuariosRow(RowvwUsuario);
                }
            }
           
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.vwUsuariosDataTable GetVwUsuariosDisponibles()
        {
            return VwEvaluadosDisponibles;
        }

        public static DataTable Difference(DataTable First, DataTable Second)
        {

            //Create Empty Table
            DataTable table = new DataTable();
            //Must use a Dataset to make use of a DataRelation object
            using (DataSet dsScr3 = new DataSet())
            {
                //Add tables
                dsScr3.Tables.AddRange(new DataTable[] { First.Copy(), Second.Copy() });
                //Get Columns for DataRelation
                DataColumn[] firstcolumns = new DataColumn[dsScr3.Tables[0].Columns.Count];
                for (int i = 0; i < firstcolumns.Length; i++)
                {
                    firstcolumns[i] = dsScr3.Tables[0].Columns[i];
                }
                DataColumn[] secondcolumns = new DataColumn[dsScr3.Tables[1].Columns.Count];
                for (int i = 0; i < secondcolumns.Length; i++)
                {
                    secondcolumns[i] = dsScr3.Tables[1].Columns[i];
                }

                //Create DataRelation

                DataRelation r = new DataRelation(string.Empty, firstcolumns, secondcolumns, false);
                dsScr3.Relations.Add(r);
                //Create columns for return table
                for (int i = 0; i < First.Columns.Count; i++)
                {
                    table.Columns.Add(First.Columns[i].ColumnName, First.Columns[i].DataType);
                }

                //If First Row not in Second, Add to return table.

                table.BeginLoadData();
                foreach (DataRow parentrow in dsScr3.Tables[0].Rows)
                {
                    DataRow[] childrows = parentrow.GetChildRows(r);
                    if (childrows == null || childrows.Length == 0)
                        table.LoadDataRow(parentrow.ItemArray, true);
                }
                table.EndLoadData();
            }
            return table;
        }

    }
}
