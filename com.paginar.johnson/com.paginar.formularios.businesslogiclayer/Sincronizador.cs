﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;
using System.Data;
using System.Collections;


namespace com.paginar.formularios.businesslogiclayer
{
    
    public abstract class Sincronizador
    {
        public FormulariosDS FormDS = new FormulariosDS();
        public FormulariosDS.vwUsuariosDataTable VwEvaluadosDisponibles = new FormulariosDS.vwUsuariosDataTable();       

        protected FormulariosDS.PeriodoDataTable getPeriodos()
        {
            return FachadaDA.Singleton.Periodo.GetData();
        }

        
        protected FormulariosDS.TipoFormularioDataTable getTiposFormularios()
        {
            return FachadaDA.Singleton.TipoFormulario.GetData();
        }

        protected FormulariosDS.TipoFormularioDataTable getTiposFormulariosByTipoPeriodoID(int TipoPeriodoID)
        {
            return FachadaDA.Singleton.TipoFormulario.GetDataByTipoPeriodoID(TipoPeriodoID);
        }

        private void SetRelEvaluadosEvaluadoresRow( ref FormulariosDS DsRelEvaluadosEvaluadores, int Legajo, int PeriodoID, int TipoFormularioID, int PasoID,int LegajoEvaluador)
        {
            FormulariosDS.RelEvaluadosEvaluadoresRow RowRelEvaluadosEvaluadores = DsRelEvaluadosEvaluadores.RelEvaluadosEvaluadores.FindByLegajoPeriodoIDTipoFormularioIDPasoID(Legajo, PeriodoID, TipoFormularioID, PasoID);
            if (RowRelEvaluadosEvaluadores != null)
            {
                RowRelEvaluadosEvaluadores.LegajoEvaluador = LegajoEvaluador;
            }
            else
            {
                RowRelEvaluadosEvaluadores = DsRelEvaluadosEvaluadores.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
                RowRelEvaluadosEvaluadores.Legajo = Legajo;
                RowRelEvaluadosEvaluadores.PeriodoID = PeriodoID;
                RowRelEvaluadosEvaluadores.TipoFormularioID = TipoFormularioID;
                RowRelEvaluadosEvaluadores.PasoID = PasoID;
                RowRelEvaluadosEvaluadores.LegajoEvaluador = LegajoEvaluador;
                DsRelEvaluadosEvaluadores.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RowRelEvaluadosEvaluadores);
            }

        }
        protected FormulariosDS.vwUsuariosDataTable getUsuarios()
        {

           

            return FormDS.vwUsuarios;
        }

        public DataView getUsuariosAuditores(int Legajo)
        {
            
           // FormDS.vwUsuarios
            DataView DVUsuarios = new DataView(FormDS.vwUsuarios, "Legajo<>"+ Legajo, "", DataViewRowState.CurrentRows);

            
            //IEnumerable <DataRow> query
            //      = from Usuarios in FormDS.vwUsuarios.AsEnumerable()
            //        where Usuarios.Field("Legajo") != Legajo
            //        select Usuarios;
            //DataView njview = query.AsDataView();

            return DVUsuarios;
        }


        protected FormulariosDS.PeriodoDataTable getPeriodoByID(int PeriodoID)
        {
            return FachadaDA.Singleton.Periodo.GetDataByID(PeriodoID);
        }
        public abstract DataTable GetUsuariosImportacion();
        public abstract FormulariosDS.AreasDataTable GetAreas();
        public abstract FormulariosDS.CargoDataTable GetCargos();
        public bool AgregarEvaluado(int Legajo,int PeriodoID, int TipoFormularioID, string Nombre, string Apellido,string Area,int AreaID, string Cargo, int CargoID, string Foto, string interno, int Lt, int Jp)
        {
            FormulariosDS.EvaluadosRow RowEval = FormDS.Evaluados.FindByLegajoPeriodoIDTipoFormularioID(Legajo,PeriodoID,TipoFormularioID);
            if (RowEval != null)
            {
                RowEval.Nombre = Nombre;
                RowEval.Apellido = Apellido;
                RowEval.Area = Area;
                RowEval.AreaID = AreaID;
                RowEval.Cargo = Cargo;
                RowEval.CargoID = CargoID;
                RowEval.Foto = Foto;
                RowEval.Interno = interno;       
                RowEval.LT = Lt;
                RowEval.JP = Jp;
            }
            else
            {
                FormulariosDS.EvaluadosRow RowNewEval = FormDS.Evaluados.NewEvaluadosRow();
                RowNewEval.Nombre = Nombre;
                RowNewEval.Apellido = Apellido;
                RowNewEval.Area = Area;
                RowNewEval.AreaID = AreaID;
                RowNewEval.Cargo = Cargo;
                RowNewEval.CargoID = CargoID;
                RowNewEval.Foto = Foto;
                RowNewEval.Interno = interno;
                RowEval.LT = Lt;
                RowEval.JP = Jp;
                FormDS.Evaluados.AddEvaluadosRow(RowEval);
            }

            FormDS.RelEvaluadosEvaluadores.Merge(FachadaDA.Singleton.RelEvaluadosEvaluadores.GetDataByFormularioID(Legajo, PeriodoID, TipoFormularioID));
            SetRelEvaluadosEvaluadoresRow(ref FormDS, Legajo, PeriodoID, TipoFormularioID, 1, Lt);
            SetRelEvaluadosEvaluadoresRow(ref  FormDS, Legajo, PeriodoID, TipoFormularioID, 2, Legajo);
            SetRelEvaluadosEvaluadoresRow(ref FormDS, Legajo, PeriodoID, TipoFormularioID, 3, Jp);
                     
            return true;
        }

        public bool ActualizarEvaluado(int Legajo, int PeriodoID, int TipoFormularioID, string Nombre, string Apellido, string Area, int AreaID, string Cargo, int CargoID, string Foto, string interno, int Lt, int? Jp, string JobGrade, string Direccion, int? DireccionID)
        {

            FormulariosDS.EvaluadosRow RowEval = FormDS.Evaluados.FindByLegajoPeriodoIDTipoFormularioID(Legajo, PeriodoID, TipoFormularioID);
            if (RowEval != null)
            {
                RowEval.Nombre = Nombre;
                RowEval.Apellido = Apellido;
                RowEval.Area = Area;
                RowEval.AreaID = AreaID;
                RowEval.Cargo = Cargo;
                RowEval.CargoID = CargoID;
                RowEval.Foto = Foto;
                RowEval.Interno = interno;
                RowEval.JobGrade = JobGrade;
                RowEval.Direccion = Direccion;
                if (DireccionID != null)
                    RowEval.DireccionID = (int)DireccionID;
                


                RowEval.LT = Lt;
                FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByFormularioID(FormDS.RelEvaluadosEvaluadores, Legajo, PeriodoID, TipoFormularioID);
                FormulariosDS.RelEvaluadosEvaluadoresRow RelEvaluadosEvaluadoresRLT = FormDS.RelEvaluadosEvaluadores.FindByLegajoPeriodoIDTipoFormularioIDPasoID(Legajo, PeriodoID, TipoFormularioID,1);
                if (RelEvaluadosEvaluadoresRLT == null)
                {
                    RelEvaluadosEvaluadoresRLT = FormDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
                    RelEvaluadosEvaluadoresRLT.Legajo = Legajo;
                    RelEvaluadosEvaluadoresRLT.TipoFormularioID = TipoFormularioID;
                    RelEvaluadosEvaluadoresRLT.PeriodoID = PeriodoID;
                    RelEvaluadosEvaluadoresRLT.PasoID = 1;
                    RelEvaluadosEvaluadoresRLT.LegajoEvaluador = -1;
                    FormDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RelEvaluadosEvaluadoresRLT);

                }


                FormulariosDS.RelEvaluadosEvaluadoresRow RelEvaluadosEvaluadoR = FormDS.RelEvaluadosEvaluadores.FindByLegajoPeriodoIDTipoFormularioIDPasoID(Legajo, PeriodoID, TipoFormularioID, 2);
                if (RelEvaluadosEvaluadoR == null)
                {
                    RelEvaluadosEvaluadoR = FormDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
                    RelEvaluadosEvaluadoR.Legajo = Legajo;
                    RelEvaluadosEvaluadoR.TipoFormularioID = TipoFormularioID;
                    RelEvaluadosEvaluadoR.PeriodoID = PeriodoID;
                    RelEvaluadosEvaluadoR.PasoID = 2;
                    RelEvaluadosEvaluadoR.LegajoEvaluador = -1;
                    FormDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RelEvaluadosEvaluadoR);
                }
                RelEvaluadosEvaluadoR.LegajoEvaluador = int.Parse(Legajo.ToString());
                FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(RelEvaluadosEvaluadoR);



                RelEvaluadosEvaluadoresRLT.LegajoEvaluador = Lt;
                FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(RelEvaluadosEvaluadoresRLT);

                if (Jp != null)
                {
                    RowEval.JP = int.Parse(Jp.ToString());
                    FormulariosDS.RelEvaluadosEvaluadoresRow RelEvaluadosEvaluadoresRJP = FormDS.RelEvaluadosEvaluadores.FindByLegajoPeriodoIDTipoFormularioIDPasoID(Legajo, PeriodoID, TipoFormularioID, 3);
                    if (RelEvaluadosEvaluadoresRJP == null)
                    {
                        RelEvaluadosEvaluadoresRJP = FormDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
                        RelEvaluadosEvaluadoresRJP.Legajo = Legajo;
                        RelEvaluadosEvaluadoresRJP.TipoFormularioID = TipoFormularioID;
                        RelEvaluadosEvaluadoresRJP.PeriodoID = PeriodoID;
                        RelEvaluadosEvaluadoresRJP.PasoID = 3;
                        RelEvaluadosEvaluadoresRJP.LegajoEvaluador = -1;
                        FormDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RelEvaluadosEvaluadoresRJP);
                    }
                    RelEvaluadosEvaluadoresRJP.LegajoEvaluador = int.Parse(Jp.ToString());
                    FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(RelEvaluadosEvaluadoresRJP);
                }

            }
            FachadaDA.Singleton.Evaluados.Update(RowEval);
            return true;
        }

        public void GrabarCambios()
        {            
            //FachadaDA.Singleton.Evaluados.Update(FormDS.Evaluados);
            //FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(FormDS.RelEvaluadosEvaluadores);
            
        }

        public void SincronizarCambios(int PeriodoID, int TipoFormularioID )
        {
            FachadaDA.Singleton.Evaluados.DeleteByPeriodo(PeriodoID, TipoFormularioID);
            
            for (int i = 0; i < FormDS.Evaluados.Rows.Count; i++)
			{
                if (FormDS.Evaluados.Rows[i].RowState == DataRowState.Deleted)
                {
                    FormDS.Evaluados.Rows.RemoveAt(i);
                }               
			}

            for (int i = 0; i < FormDS.Evaluados.Rows.Count; i++)
            {
                FormDS.Evaluados.Rows[i].AcceptChanges();
                FormDS.Evaluados.Rows[i].SetAdded();
            }

            for (int i = 0; i < FormDS.RelEvaluadosEvaluadores.Rows.Count; i++)
            {
                if (FormDS.RelEvaluadosEvaluadores.Rows[i].RowState == DataRowState.Deleted)
                {
                    FormDS.RelEvaluadosEvaluadores.Rows.RemoveAt(i);
                }
            }

            for (int i = 0; i < FormDS.RelEvaluadosEvaluadores.Rows.Count; i++)
            {
                FormDS.RelEvaluadosEvaluadores.Rows[i].AcceptChanges();
                FormDS.RelEvaluadosEvaluadores.Rows[i].SetAdded();
            }



            
            FachadaDA.Singleton.Evaluados.Update(FormDS.Evaluados);
            FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(FormDS.RelEvaluadosEvaluadores);
            
            
        }

        public void Sincronizar(int PeriodoID, int TipoFormularioID)
        {

        }

        public void AddPaso(int Legajo, int PeriodoID, int TipoFormularioID,int LegajoEvaluador, int PasoID)
        {
            FormulariosDS FDS = new FormulariosDS();
            FormulariosDS.RelEvaluadosEvaluadoresRow RowRelEvaluadosEvaluadores = FDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
            RowRelEvaluadosEvaluadores.Legajo = Legajo;
            RowRelEvaluadosEvaluadores.TipoFormularioID = TipoFormularioID;
            RowRelEvaluadosEvaluadores.PeriodoID = PeriodoID;
            RowRelEvaluadosEvaluadores.PasoID = PasoID;
            RowRelEvaluadosEvaluadores.LegajoEvaluador = LegajoEvaluador;
            FDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RowRelEvaluadosEvaluadores);
            FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(FDS.RelEvaluadosEvaluadores);
        }

        public abstract void BuscarUsuarios(int? PeriodoID, int? TipoFormularioID, string MODO, int? cluster);

        public abstract void DeleteUsuariosModuloImportacion(int legajo, int PeriodoID, int TipoFormularioID);

        public abstract void InsertarUsuariosModuloImportacion(int Legajo, int PeriodoID, int TipoFormularioID);

        
        
    }
}
