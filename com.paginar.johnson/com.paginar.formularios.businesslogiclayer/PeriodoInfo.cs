﻿using System;
using System.Collections.Generic;
using System.Text;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    static public class PeriodoInfo
    {
        static public int? GetPeriodoActualID(DateTime Fecha)
        {
            return (int?)FachadaDA.Singleton.Periodo.GetPeriodoActualID(Fecha);
            //return 0;
        }

        static public int? GetPeriodoActualByFechaAndType(DateTime Fecha, int Type)
        {
            return (int?)FachadaDA.Singleton.Periodo.GetPeriodoActualIDByDateAndFormTypeId(Fecha, Type);
                //.GetPeriodoActualIDByDateAndFormType(Fecha, Type);
        }
    }
}
