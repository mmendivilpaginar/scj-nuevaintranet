﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web.Security;
using System.Web;
using System.Net.Mail;

namespace com.paginar.formularios.businesslogiclayer
{
    public class NotificacionesController
    {

        Notificaciones m = new Notificaciones();


        bool MODO;
        string SMTP;
        string MailToTest;
        string MailRRHH;
        string smtp_host ;
        string port;
        string from_dir ;
        string pass ;
        string password;

       
        public void getConfiguracion()
        {
            SMTP = ConfigurationSettings.AppSettings["SMTP"];
            MODO = bool.Parse(ConfigurationSettings.AppSettings["SENDMAIL"].ToString());
            MailToTest = ConfigurationSettings.AppSettings["MAILTOTEST"].ToString();
            MailRRHH = ConfigurationSettings.AppSettings["MAILRRHH"].ToString();
            smtp_host = ConfigurationSettings.AppSettings["smtp"];
            port = ConfigurationSettings.AppSettings["port"];
            from_dir = ConfigurationSettings.AppSettings["from"];
            pass = ConfigurationSettings.AppSettings["pass"];
            password =  ConfigurationSettings.AppSettings["password"];
        }

        public bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
        public string inicioEvaluacion(string evaluador,string evaluado)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Operarios";

            if (!MODO)
                m.TO = MailToTest;
            else
            {
                //m.TO = MailRRHH;
                string[] Usuarios = Roles.GetUsersInRole("RRHH-PMPO");

                foreach (string UsuarioID in Usuarios)
                {
                    com.paginar.johnson.membership.Usuarios user = new com.paginar.johnson.membership.Usuarios();
                        //MembershipUser mu = user.GetUser(int.Parse(UsuarioID));
                    MembershipUser mu = user.GetUser(UsuarioID, false);


                    m.TO += ";" + mu.Email;
                }
            }
            

            

            m.SUBJECT = "PMP - status: nuevo colaborador añadido";
            m.BODY = String.Format("Estimado/a,<BR><BR>{0} Ha incorporado en su proceso de evaluaciones a un nuevo colaborador/a de su sector, {1}, para " +
                        "iniciar el correspondiente PMP. <BR><BR>"+                        
                        "Muchas gracias. " ,evaluador,evaluado);
            return m.send();
        }

        public string AprobadaPMPOEvaluado(string mailOperario, string nombreAuditor, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Operarios";

            if (mailOperario.Length > 0)
            {
                m.TO = mailOperario;
                m.SUBJECT = "PMP - status: evaluación aprobada";
                m.BODY = String.Format("Estimado/a,<br><br>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma finalizó correctamente, y se encuentra aprobada." +
                    "<br><br>Muchas gracias.<br><a href=\"{1}\">Acceder a la PMP</a><br>Recursos Humanos.", nombreAuditor, url);

                return m.send();
            }
            else
            {
                return "El operario no posee mail";
            }

        
        }

        public string AprobadaPMPOEvaluador(string nombreAuditor, string nombreEvaluado, string mailEvaluador, string url)
        {

            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "PMP - status: evaluación aprobada";

            m.TO = mailEvaluador;
            m.SUBJECT = "PMP - status: evaluación aprobada";
            m.BODY = String.Format("Estimado/a,<br>Le comunicamos que {0} realizó los comentarios en la evaluación de {1}, la misma finalizó correctamente, y se encuentra aprobada." +
                        "<br><br>Muchas gracias.<a href=\"{2}\">Acceder a la PMP</a><br>Recursos Humanos.", nombreAuditor, nombreEvaluado, url);

            return m.send();

        }

        public string inicioEvaluacionOperario(string mailOperario, string Evaluador, string url)
        { 
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Operarios";
            
            if(mailOperario.Length > 0)
            {
                m.TO = mailOperario;
                m.SUBJECT = "PMP - Evaluación de Desempeño";
                m.BODY = String.Format("Estimado/a,<br><br>Le comunicamos que {0} ha completado todos los comentarios de su evaluación. La misma está lista para que Usted agregue las acotaciones que desee. Aguardamos sus comentarios para continuar con el proceso." +
                            "<br><br>Muchas gracias.<br><a href=\"{1}\" target=\"_blank\">Acceder a la PMP</a><br>Recursos Humanos.", Evaluador, url);
                return m.send();
            }
            else
            {
                return "El operario no posee mail";
            }
        }        
        

        public string envioJefePlanta(string mailjefeplanta,string evaluado, string calificacion,string comentarios,string comentariosoperario, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Operarios";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailjefeplanta;

            m.FROM = from_dir;
            m.SUBJECT = "PMP - status: comentarios completados";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su PMP y la evaluación está " +
                        "lista para ser revisada. Aguardamos sus comentarios y validación en esta etapa final del proceso. <BR><BR>" +
                        "A continuación le compartimos una síntesis del PMP de {0}: <BR><BR>" +
                        "Evaluación del PMP: {1} " +
                        "<BR><BR>" +
                        "Comentarios del Líder de Turno: {2} " +
                        "<BR><BR>" +
                        "Comentarios del Operario: {3} " +
                        "<BR><BR>" +
                        "Muchas gracias. " +
                        "<br><a href=\"{4}\" target=\"_blank\">Acceder a la PMP</a><BR>" +
                        "Recursos Humanos.", evaluado, calificacion, (comentarios.Length == 0) ? "-" : comentarios, (comentariosoperario.Length == 0) ? "-" : comentariosoperario,url);

            return m.send();
        }

        public string devuelveJefeTurno(string mailevaluador,string jefeplanta,string evaluado, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Operarios";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailevaluador;

            m.SUBJECT = "PMP - status: PMP no validado ";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que el PMP de {0} no fue aprobado por el Jefe de Planta, {1}. Por favor, reinicie " +
                                    "el proceso: compruebe las observaciones sugeridas por el Jefe de Planta, evalúe nuevamente PMP y realice sus comentarios. <BR>" +
                                    "La instancia posterior requerirá que se vuelva a reunir con su colaborador, que luego él continúe con el circuito y realice los comentarios (opcional) antes de reenviar el formulario al Jefe de Planta. <BR><BR>" +
                                    "Muchas gracias." +
                                    "<br><a href=\"{2}\" target=\"_blank\">Acceder a la PMP</a><BR>" +
                                    "Recursos Humanos.", evaluado, jefeplanta,url);
            return m.send();
        }

        public string envioEvaluadorMY(string mailEvaluador, string evaluado, string logros, string OportunidadesMejora, string Comportamiento)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluador;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma está " +
                        "lista para ser revisada. Aguardamos sus comentarios y validación en esta etapa final del proceso." +                        
                        "<BR><BR>Muchas gracias. " +
                        "<BR>" +
                        "Recursos Humanos.", evaluado);

            return m.send();
        }

        public string envioEvaluadorMYV2(string mailEvaluador, string evaluado, string logros, string OportunidadesMejora, string Comportamiento, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluador;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma está " +
                        "lista para ser revisada. Aguardamos sus comentarios y validación en esta etapa final del proceso." +
                        "<BR><BR>Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" + 
                        "Recursos Humanos.", evaluado);

            return m.send();
        }



        public string envioUbicacionPMP(string mailDestinatario, string grid, string Tipo)
        {
            string TipoPMP = System.Web.HttpUtility.HtmlDecode(Tipo);
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Evaluaciones de desempeño";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailDestinatario;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño " + TipoPMP + " - Ubicacion";
            m.BODY = String.Format("Estimado/a,<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;Se adjuntan Datos Ubicacion " + Tipo + "<br />{0}",grid);

            return m.send();
        }


        public string aproboevaluacionMY(string mailEvaluado, string evaluador)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma finalizó " +
                        "correctamente, y  se encuentra aprobada. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "Recursos Humanos.", evaluador);

            return m.send();
        }

        public string aproboevaluacionMYV2(string mailEvaluado, string evaluador, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma finalizó " +
                        "correctamente, y  se encuentra aprobada. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" + 
                        "Recursos Humanos.", evaluador);

            return m.send();
        }

        public string devuelveevaluacionMY(string mailEvaluado, string evaluador)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} decidió devolver su evaluación. Por favor revise los datos ingresados " +
                        "y continúe con el proceso. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "Recursos Humanos.", evaluador);

            return m.send();
        }

        public string devuelveevaluacionMYV2(string mailEvaluado, string evaluador, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Mid year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño de Mitad de Año - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} decidió devolver su evaluación. Por favor revise los datos ingresados " +
                        "y continúe con el proceso. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" + 
                        "Recursos Humanos.", evaluador);

            return m.send();
        }



        public string envioEvaluadorFY(string mailEvaluador, string evaluado, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Full year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluador;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma está " +
                        "lista para ser revisada. Aguardamos sus comentarios y validación en esta etapa final del proceso." +
                        "<BR><BR>Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" +
                        "Recursos Humanos.", evaluado);

            return m.send();
        }


        public string aproboevaluacionFY(string mailEvaluado, string evaluador, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Full year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} realizó los comentarios en su evaluación, la misma finalizó " +
                        "correctamente, y  se encuentra aprobada. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" +
                        "Recursos Humanos.", evaluador);

            return m.send();
        }


        public string devuelveevaluacionFY(string mailEvaluado, string evaluador, string url)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Pmp-Full year";

            if (!MODO)
                m.TO = MailToTest;
            else
                m.TO = mailEvaluado;

            m.FROM = from_dir;
            m.SUBJECT = "Evaluación de desempeño - Status: datos completados por el evaluado";
            m.BODY = String.Format("Estimado/a,<BR><BR>Le comunicamos que {0} decidió devolver su evaluación. Por favor revise los datos ingresados " +
                        "y continúe con el proceso. <BR><BR>" +
                        "Muchas gracias. " +
                        "<BR>" +
                        "<a href=" + url + ">Acceder a la PMP</a> <br />" +
                        "Recursos Humanos.", evaluador);

            return m.send();
        }

        public string EnvioMailWorkshop(string to, string subject, string body)
        { 

            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Gestión de Talento";
            m.TO = to;
            m.FROM = from_dir;
            m.SUBJECT = subject;
            m.BODY = body;

            return m.send();
        
        }

        public string EnvioMailVoluntariado(string to, string subject, string body)
        {

            getConfiguracion();
            m.SMTP = SMTP;
            m.FROM = from_dir;
            m.DISPLAYNAME = "Voluntariado";
            m.TO = to;
            m.FROM = from_dir;
            m.SUBJECT = subject;
            m.BODY = body;

            return m.send();

        }


        public string EnvioMailNewsletter(string from,string to, string subject, AlternateView html )
        //public string EnvioMailNewsletter(string from, string to, string subject, string body)
        {
            getConfiguracion();
            m.SMTP = SMTP;
            m.Port = port;
            m.Pass = password;
            m.FROM = from;            
            m.TO = to;
            m.FROM = from;
            m.SUBJECT = subject;
            //m.BODY = body;

            return m.send("Comunicaciones",html);

        }
    }


}
