﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    [DataObject]
    public class DashboardController
    {
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.PeriodoDataTable GetPeriodos()
        {
            return FachadaDA.Singleton.Periodo.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.vwUsuariosDataTable GetUsuarios()
        {
            return FachadaDA.Singleton.vwUsuarios.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.PasosDataTable GetPasos()
        {
            return FachadaDA.Singleton.Pasos.GetData();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.PasosDataTable GetPasosTodos()
        {
            return FachadaDA.Singleton.Pasos.GetTodos();
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetFormulariosByEvaluadorEstado(int LegajoEvaluador,int PasoID,int PeriodoID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetDataByEvaluadorEstado(LegajoEvaluador, PasoID, PeriodoID);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetFormulariosByEvaluadorEstadoTodos(int LegajoEvaluador, int PasoID, int PeriodoID,int TipoFormularioID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetDDataByEvaluadorEstadoTodos(PasoID, LegajoEvaluador, PeriodoID, TipoFormularioID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.HistorialDataTable GetLastHistorialByPasoID(int Legajo,int PeriodoID,int TipoFormularioID, int PasoID)
        {
            return FachadaDA.Singleton.Historial.GetDataLastHistorialByPasoID(Legajo, PeriodoID, TipoFormularioID, PasoID);
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.EvaluadosDataTable GetDataOtrosEvaluadosByPaso(int PeriodoID,int TipoFormularioID,int legajo,int PasoID)
        {
            return FachadaDA.Singleton.Evaluados.GetDataOtrosEvaluadosByPaso(PeriodoID, TipoFormularioID, legajo, PasoID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.vwUsuariosDataTable GetDataAllOtrosEvaluadosByPaso(int PeriodoID, int TipoFormularioID, int legajo, int PasoID)
        {
            return FachadaDA.Singleton.vwUsuarios.GetDataOtrosEvaluadosByLegajo(PeriodoID, legajo, PasoID);
        }

        public bool VerificarTieneFormulario(int Legajo, int PeriodoID, int TipoFormularioID)
        {
            return (FachadaDA.Singleton.FormularioPmpOperarios.GetDataByID(Legajo, PeriodoID, TipoFormularioID).Rows.Count > 0);
        }
        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetFormulario(int Legajo, int PeriodoID, int TipoFormularioID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetDataByID(Legajo, PeriodoID, TipoFormularioID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetDataByJP(int PasoID, int LegajoJP, int LegajoJT, int PeriodoID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetDataByJP(PasoID, LegajoJP, LegajoJT, PeriodoID);
        }

        public void AsociarEvaluacion(int Legajo, int PeriodoID, int TipoFormularioID, int PasoID, int LegajoEvaluador)
        {
            FormulariosDS FDS = new FormulariosDS();
            FachadaDA.Singleton.Evaluados.FillByID(FDS.Evaluados, Legajo, PeriodoID, TipoFormularioID);
            if (FDS.Evaluados.Rows.Count == 0)
            {
                FachadaDA.Singleton.vwUsuarios.FillBylegajo(FDS.vwUsuarios, Legajo);
                FormulariosDS.EvaluadosRow ER = FDS.Evaluados.NewEvaluadosRow();
                foreach (FormulariosDS.vwUsuariosRow usuario in FDS.vwUsuarios.Rows)
                {
                    ER.Apellido = usuario.Apellido;
                    ER.Nombre = usuario.Nombre;
                    ER.Legajo = usuario.Legajo;
                    ER.PeriodoID = PeriodoID;
                    ER.TipoFormularioID = TipoFormularioID;
                    if (!usuario.IsSectorNull())
                    ER.Sector = usuario.Sector;
                    if (!usuario.IsCargoNull())
                    ER.Cargo = usuario.Cargo;
                    if (!usuario.IsAreaNull())
                        ER.Area = usuario.Area; 
                    if (!usuario.IsFotoNull())
                    ER.Foto = usuario.Foto;                

                }
                FDS.Evaluados.AddEvaluadosRow(ER);
                FachadaDA.Singleton.Evaluados.Update(FDS.Evaluados);
            }

            FachadaDA.Singleton.RelEvaluadosEvaluadores.FillByID(FDS.RelEvaluadosEvaluadores, Legajo, PeriodoID, TipoFormularioID, PasoID);
            foreach (FormulariosDS.RelEvaluadosEvaluadoresRow row in FDS.RelEvaluadosEvaluadores)
            {
                row.Delete();
            }

            FormulariosDS.RelEvaluadosEvaluadoresRow RowREE = FDS.RelEvaluadosEvaluadores.NewRelEvaluadosEvaluadoresRow();
            RowREE.Legajo = Legajo;
            RowREE.PeriodoID = PeriodoID;
            RowREE.TipoFormularioID = TipoFormularioID;
            RowREE.PasoID = PasoID;
            RowREE.LegajoEvaluador = LegajoEvaluador;
            FDS.RelEvaluadosEvaluadores.AddRelEvaluadosEvaluadoresRow(RowREE);
            FachadaDA.Singleton.RelEvaluadosEvaluadores.Update(FDS.RelEvaluadosEvaluadores);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.vwUsuariosDataTable GetDataByLideresByJefeDePlanta(int? LegajoJefePlanta, int? PeriodoID, int? TipoFormularioID)
        {
            return FachadaDA.Singleton.vwUsuarios.GetDataByLideresByJefeDePlanta(LegajoJefePlanta, PeriodoID, TipoFormularioID);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.HistorialDataTable GetHistorialByLegajo(int legajo)
        {
            return FachadaDA.Singleton.Historial.GetHistorial(legajo);
        }

        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public FormulariosDS.HistorialDataTable GetHistorialFYByLegajo(int legajo)
        {
            return FachadaDA.Singleton.Historial.GetHistorialFY(legajo);
        }


        [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public FormulariosDS.FormularioPmpOperariosDataTable GetEvaluacionesByEvaluado(int legajo)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetEvaluacionesByEvaluado(legajo);
        }

        public bool ExisteEvaluado(int Legajo, int PeriodoID, int TipoFormularioID)
        {
            FormulariosDS FDS = new FormulariosDS();
            FachadaDA.Singleton.Evaluados.FillByID(FDS.Evaluados, Legajo, PeriodoID, TipoFormularioID);
            return (FDS.Evaluados.Rows.Count > 0);
        }

        public bool CrearEvaluado(int Legajo, int PeriodoID, int TipoFormularioID)
        {
            FormulariosDS FDS = new FormulariosDS();
            FachadaDA.Singleton.vwUsuarios.FillBylegajo(FDS.vwUsuarios, Legajo);
            FormulariosDS.EvaluadosRow ER = FDS.Evaluados.NewEvaluadosRow();
            foreach (FormulariosDS.vwUsuariosRow usuario in FDS.vwUsuarios.Rows)
            {
                ER.Apellido = usuario.Apellido;
                ER.Nombre = usuario.Nombre;
                ER.Legajo = usuario.Legajo;
                ER.PeriodoID = PeriodoID;
                ER.TipoFormularioID = TipoFormularioID;
                if (!usuario.IsSectorNull())
                    ER.Sector = usuario.Sector;
                if (!usuario.IsCargoNull())
                    ER.Cargo = usuario.Cargo;
                if (!usuario.IsAreaNull())
                    ER.Area = usuario.Area;
                if (!usuario.IsFotoNull())
                    ER.Foto = usuario.Foto;

            }
            FDS.Evaluados.AddEvaluadosRow(ER);
            try
            {
                FachadaDA.Singleton.Evaluados.Update(FDS.Evaluados);
                return true;
            }
            catch
            {

                throw;
            }
            return false;
        }

    }
}
