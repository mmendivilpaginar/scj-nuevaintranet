﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    [DataObject]
    public class FormulariosFYController
    {
         FormulariosPMPFYDS FYDS = new FormulariosPMPFYDS();



         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCalificacionDataTable GetCalificaciones(string Idioma, string modulo)
         {
             return FachadaDA.Singleton.frmPMPCalificacionTableAdapter.GetDataByIdiomaModulo(Idioma, modulo);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCalificacionDataTable GetCalificacionesByID(string Idioma, string modulo, short Calificacion)
         {
             return FachadaDA.Singleton.frmPMPCalificacionTableAdapter.GetDataByID(Idioma, modulo,Calificacion);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPFortalezasTempDataTable GetFortalezasByTipoFormularioID(int TipoFormularioID)
         {
             return FachadaDA.Singleton.frmPMPFortalezasTempTableAdapter.GetDataByTipoFormularioID(TipoFormularioID);
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPAreasDataTable GetAreasActivas()
         {
             return FachadaDA.Singleton.frmPMPAreasTableAdapter.GetDataHabilitados();
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPAreasDataTable GetAreas()
         {
             return FachadaDA.Singleton.frmPMPAreasTableAdapter.GetData();
         }

         public String GetAreaDescByID(int AreaID)
         {
             string AreaDesc= string.Empty;
             FormulariosPMPFYDS.frmPMPAreasDataTable DTA = FachadaDA.Singleton.frmPMPAreasTableAdapter.GetAreaByID(AreaID);
             if(DTA.Rows.Count>0)
                 AreaDesc=DTA[0].DescAreas;
             return AreaDesc;
         }
         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCareerDataTable GetCareerActivas()
         {
             return FachadaDA.Singleton.frmPMPCareerTableAdapter.GetDataHabilitados();
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCareerDataTable GetCareerByIDGenerado(int IDGenerado)
         {
             return FachadaDA.Singleton.frmPMPCareerTableAdapter.GetDataByIDGenerado(IDGenerado);
         }

         public string GetCareerDescripcion(int IDGenerado)
         {
             string Descripcion= string.Empty;
             FormulariosPMPFYDS.frmPMPCareerDataTable DTCareer = new FormulariosPMPFYDS.frmPMPCareerDataTable();
             DTCareer = this.GetCareerByIDGenerado(IDGenerado);
             if (DTCareer.Rows.Count > 0)
                 Descripcion = DTCareer[0].Descripcion;
             return Descripcion;
         }

         public string GetCareerCode(int IDGenerado)
         {
             string Code = string.Empty;
             FormulariosPMPFYDS.frmPMPCareerDataTable DTCareer = new FormulariosPMPFYDS.frmPMPCareerDataTable();
             DTCareer = this.GetCareerByIDGenerado(IDGenerado);
             if (DTCareer.Rows.Count > 0)
                 Code = DTCareer[0].ID.Replace("_11", "");
             return Code;
         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCursosDataTable GetCursosByDebilidadID(int TipoFormularioID, int AreaID, string Debilidad)
         {
             return FachadaDA.Singleton.frmPMPCursosTableAdapter.GetDataByDebilidadID(TipoFormularioID, AreaID, Debilidad);

         }

         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
         public FormulariosPMPFYDS.frmPMPCursosDataTable GetCursosDisponiblesByDebilidad(int TipoFormularioID, int Legajo, int Periodoid, int AreaID, string Debilidad)
         {
             return FachadaDA.Singleton.frmPMPCursosTableAdapter.GetCursosDisponiblesByDebilidad(TipoFormularioID, Legajo, Periodoid, AreaID, Debilidad);

         }


         [DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
         public FormulariosPMPFYDS.LastRatingsDataTable GetLastRatings(int legajo, int periodoActual, int TipoPeriodoID)
         {
             return FachadaDA.Singleton.LastRatingsTableAdapter.GetLastRatings(legajo, TipoPeriodoID, periodoActual);
         }

         public FormulariosPMPFYDS.frmPMPFortalezasTempDataTable GetFortalezaByID(int codigo, int TipoFormularioID)
         {
             return FachadaDA.Singleton.frmPMPFortalezasTempTableAdapter.GetDataByID(codigo, TipoFormularioID);
         }


         public string GetDescripcionCalificacionByID(string Idioma, string modulo, int Calificacion)
         {

             string Output = string.Empty;
             if (int.Parse(Calificacion.ToString()) == 0)
                 return Output;

             

             FormulariosFYController fYc = new FormulariosFYController();
             FormulariosPMPFYDS.frmPMPCalificacionDataTable DTCalificacion = new FormulariosPMPFYDS.frmPMPCalificacionDataTable();
             DTCalificacion = fYc.GetCalificacionesByID(Idioma  ,modulo, short.Parse(Calificacion.ToString()));
             if (DTCalificacion.Rows.Count > 0)
                 Output = DTCalificacion[0].descripcion;


             return Output;
         }

    }
}
