﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mail;
using System.Web.Configuration;
using System.Net.Mail;


namespace com.paginar.formularios.businesslogiclayer
{
    public class Notificaciones
    {
        string _from;
        string _to;
        string _cc;
        string _subjet;
        string _body;        
        string _smtp;
        string _displayname;
        string _port;
        string _pass;

        public string DISPLAYNAME
        {
            get { return _displayname; }
            set { _displayname = value; }
        }
        public string FROM
        {
            get { return _from; }
            set { _from = value; }

        }
        public string TO { 
            get{ return _to;}
            set { _to = value; }
        }
        public string CC
        {
            get { return TO; }
            set { TO = value; }
        }
        public string SUBJECT
        {
            get { return _subjet; }
            set { _subjet = value; }
        }
        public string BODY
        {
            get { return _body; }
            set { _body = value; }
        }        

        public string SMTP
        {
            get { return _smtp; }
            set { _smtp = value; }
        }

        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        public string Pass
        {
              get { return _pass; }
            set { _pass = value; }
        }

        public string send()
        {
         
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = TO;
            //msg.From = DISPLAYNAME + "<"+ FROM+">";
            msg.From = FROM;
            msg.Subject = SUBJECT;            
            msg.Body = BODY;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            string enviado;
            try
            {                
                if (!string.IsNullOrEmpty(SMTP))
                    SmtpMail.SmtpServer = SMTP;
                System.Web.Mail.SmtpMail.Send(msg);                
                enviado = "true";                

            }
            catch (Exception ex)
            {
                enviado = ex.Message;
                //enviado = false;
            }

            return enviado;
        }


        public string send(string DisplayName)
        {

            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.To = TO;            
            msg.From = DisplayName + "<" + FROM + ">";
            msg.Subject = SUBJECT;
            msg.Body = BODY;
            msg.BodyFormat = System.Web.Mail.MailFormat.Html;
            string enviado;
            try
            {
                if (!string.IsNullOrEmpty(SMTP))
                    SmtpMail.SmtpServer = SMTP;
                System.Web.Mail.SmtpMail.Send(msg);
                enviado = "true";

            }
            catch (Exception ex)
            {
                enviado = ex.Message;
                //enviado = false;
            }

            return enviado;
        }

        public string send(string DisplayName, AlternateView htmlView)
        {

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.To.Add(TO);
            msg.From  = new MailAddress(FROM,DisplayName);  
            msg.Subject = SUBJECT;            
            msg.AlternateViews.Add(htmlView);

            string enviado;
            try
            {
     

                SmtpClient client = new SmtpClient(SMTP);
                client.Port = int.Parse(Port);

                if (!((FROM == null) && (Pass == null)))
                {
                    client.Credentials = new System.Net.NetworkCredential(FROM, Pass);

                }



                client.EnableSsl = false;
                client.Send(msg);
                client = null;
                msg.Dispose();
                enviado = "true";
                


            }
            catch (Exception ex)
            {
                enviado = ex.Message;
                //enviado = false;
            }

            return enviado;
        }

    }
}
