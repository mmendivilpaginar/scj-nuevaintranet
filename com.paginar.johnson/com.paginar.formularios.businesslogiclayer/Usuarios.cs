﻿using System;
using System.Collections.Generic;
using System.Text;
using com.paginar.formularios.dataaccesslayer;
namespace com.paginar.formularios.businesslogiclayer
{
    public class UsuariosEvaluacion
    {
        string LoginName;
        public int legajo;
        public UsuariosEvaluacion(string LoginName)
        { 
            this.LoginName = LoginName;
            this.legajo = this.GetLegajoUsuario(this.LoginName);
        }
        public Boolean EstaAutorizado(int PasoID, int legajoformulario, int TipoFormularioID, int PeriodoID)
        {
           return ((Boolean)FachadaDA.Singleton.RelEvaluadosEvaluadores.ISAutorizadoPaso(legajoformulario, PeriodoID, TipoFormularioID, PasoID, this.legajo));

        }
        private int GetLegajoUsuario(string LoginName)
        {
           return ((int) FachadaDA.Singleton.vwUsuarios.GetLegajoByLoginName(LoginName));
        }

        public string GetUsuarioNombre(int legajo)
        {
           FormulariosDS.vwUsuariosRow dr = (FormulariosDS.vwUsuariosRow)FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(legajo)[0];
           return dr.Nombre + " " + dr.Apellido;
        }

        public int GetLegajoJefe(int legajo)
        {
            FormulariosDS.vwUsuariosRow dr = (FormulariosDS.vwUsuariosRow)FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(legajo)[0];
            return dr.LegajoJefe;
        }

        public int GetLegajodeEvaluador(int legajo,int periodoid,int tipoformularioid,int PasoID)
        {
            FormulariosDS.RelEvaluadosEvaluadoresRow dr = (FormulariosDS.RelEvaluadosEvaluadoresRow)FachadaDA.Singleton.RelEvaluadosEvaluadores.GetDataEvaluadoEvaluador(legajo, periodoid, tipoformularioid,PasoID)[0];
            return dr.LegajoEvaluador;
        }

        public string GetMail(int legajo)
        {
            
            FormulariosDS.vwUsuariosRow dr       = (FormulariosDS.vwUsuariosRow)      FachadaDA.Singleton.vwUsuarios.GetDataBylegajo(legajo)[0];
            if (!dr.IsEmailNull())
                return dr.Email;
            else
                return  string.Empty;
        }

        public bool RealizaEvaluacionPeriodoActual(int legajo, int TipoFormularioID)
        {
            return (FachadaDA.Singleton.Evaluados.GetDataByLegajoTipoFormularioID(TipoFormularioID, legajo).Count>0);
        }
        
    }
}
