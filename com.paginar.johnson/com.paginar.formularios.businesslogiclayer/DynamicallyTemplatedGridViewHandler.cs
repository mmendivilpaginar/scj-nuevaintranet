using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Data.SqlClient;
/// <summary>
/// Article: Dynamically Templated GridView with Edit,Insert and Delete Options
/// Author: G. Mohyuddin
/// Brief Notes: This class implements ITemplate which is resposible to create template fields of 
/// the GridView dynamically and also to add buttons for Edit,Delete and Insert.
/// </summary>
public class DynamicallyTemplatedGridViewHandler : ITemplate
{
    #region data memebers
   
    ListItemType ItemType;
    string FieldName;
    string InfoType;
    string ToolTip;
    //Esto es para edición multiple
    bool esEditable;
    int rownumber;
    //Esto es para edición multiple
   #endregion

    #region constructor

    public DynamicallyTemplatedGridViewHandler(ListItemType item_type, string field_name, string info_type, string ToolTip_, bool Editable = false, int rownumberPar = 0)
    {
        ItemType = item_type;
        FieldName = field_name;
        InfoType = info_type;
        ToolTip=ToolTip_;
        esEditable = Editable;
        rownumber = rownumberPar;
    }

    #endregion

    #region Methods

    public void InstantiateIn(System.Web.UI.Control Container)
    {
        switch (ItemType)
        {
            case ListItemType.Header:
                Literal header_ltrl = new Literal();
                header_ltrl.Text = FieldName ;
                Container.Controls.Add(header_ltrl);
                break;
            case ListItemType.Item:
                switch (InfoType)
                {
                    case "Command":
                        if (esEditable)
                        {
                            Label field_lblv = new Label();
                            field_lblv.ID = "LabelValidacion";
                            field_lblv.Text = "Falta Ingresar Comentarios";
                            field_lblv.CssClass = "msg-error1";
                            field_lblv.Style.Add(HtmlTextWriterStyle.Display, "none");
                            Container.Controls.Add(field_lblv);                        
                        }
                        ImageButton edit_button = new ImageButton();
                        edit_button.ID = "edit_button";
                        edit_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                        edit_button.CommandName = "Edit";
                        edit_button.Click += new ImageClickEventHandler(edit_button_Click);
                        edit_button.CssClass = "IcnCalEdit";
                        edit_button.ToolTip = "Modificar";
                        edit_button.OnClientClick = "Preloader();";
                        Container.Controls.Add(edit_button);                       

                        ImageButton send_button = new ImageButton();
                        send_button.ID = "send_button";
                        send_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                        send_button.CommandName = "Enviar";
                        send_button.Click += new ImageClickEventHandler(edit_button_Click);
                        send_button.CssClass = "IcnCalEnviar";
                        send_button.ToolTip = "Enviar";
                        //send_button.OnClientClick = "PreloaderConfirm();";
                        send_button.Attributes["onclick"] = "return PreloaderConfirm();";
                        Container.Controls.Add(send_button);

                        ImageButton print_button = new ImageButton();
                        print_button.ID = "print_button";
                        print_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                        print_button.CommandName = "Imprimir";
                        print_button.Click += new ImageClickEventHandler(edit_button_Click);
                        print_button.CssClass = "IcnCalImprimir";
                        print_button.ToolTip = "Imprimir";
                        //print_button.OnClientClick = "ConfirmPrint();";
                        Container.Controls.Add(print_button);

                        break;

                    case "Comentario":

                        Label mostrar_comentario = new Label();
                        mostrar_comentario.ID = "mostrar_comentario";
                        mostrar_comentario.Visible = !esEditable;
                        Container.Controls.Add(mostrar_comentario);
                        if (esEditable)
                        {
                            TextBox editar_comentario = new TextBox();
                            editar_comentario.ID = "editar_comentario";
                            editar_comentario.MaxLength = 500;
                            Container.Controls.Add(editar_comentario);                        
                        }


                        break;

                    default:
                        Label field_lbl = new Label();
                        field_lbl.ID = "LabelItemEvaluacion"+FieldName;
                        field_lbl.Text = String.Empty; //we will bind it later through 'OnDataBinding' event
                       //// field_lbl.ToolTip = ToolTip;
                        field_lbl.DataBinding += new EventHandler(OnDataBinding);
                        field_lbl.Visible = !esEditable;
                        Container.Controls.Add(field_lbl);

                        HiddenField field_hdf = new HiddenField();
                        field_hdf.ID = "HiddenFieldItemEvaluacion" + FieldName;
                        field_hdf.Value = String.Empty;
                        field_hdf.DataBinding += new EventHandler(OnDataBinding);
                        Container.Controls.Add(field_hdf);
                        if (esEditable)
                        {
                            CustomValidator field_CV = new CustomValidator();
                            field_CV.ClientValidationFunction = "ValidarTextBoxFundamentacion";
                            field_CV.ID = "CustomValidatorTextBoxFundamentacion" + FieldName;
                            field_CV.ValidationGroup = "Grabar";
                            //field_CV.Text = "Debe Completar la Fundamentación";
                            field_CV.Display = ValidatorDisplay.Static;
                            //field_CV.ErrorMessage = "Debe Completar la Fundamentación";
                            Container.Controls.Add(field_CV);


                            //TextBox field_txtbox = new TextBox();
                            DropDownList field_dropDownList = new DropDownList();
                            ListItem Li0 = new ListItem();

                            Li0.Value = "";
                            Li0.Text = "No Corresponde";
                            field_dropDownList.Items.Add(Li0);

                            ListItem Li1 = new ListItem();
                            Li1.Value = "0";
                            Li1.Text = "Seleccione una opcion";
                            field_dropDownList.Items.Add(Li1);


                            ListItem Li2 = new ListItem();
                            Li2.Value = "1";
                            Li2.Text = "No Satisfactorio";
                            field_dropDownList.Items.Add(Li2);

                            ListItem Li3 = new ListItem();
                            Li3.Value = "2";
                            Li3.Text = "Regularmente Cumple / Necesita Desarrollarse";
                            field_dropDownList.Items.Add(Li3);

                            ListItem Li4 = new ListItem();
                            Li4.Value = "3";
                            Li4.Text = "Bueno";
                            field_dropDownList.Items.Add(Li4);

                            ListItem Li5 = new ListItem();
                            Li5.Value = "4";
                            Li5.Text = "Muy Bueno";
                            field_dropDownList.Items.Add(Li5);

                            int FieldNameInt;
                            int.TryParse(FieldName, out FieldNameInt);

                            field_dropDownList.ID = "DropDownListItemEvaluacion" + FieldName;

                            field_dropDownList.DataBinding += new EventHandler(OnDataBinding);
                            Container.Controls.Add(field_dropDownList);

                            /*
                            HiddenField field_lbl = new HiddenField();
                            field_lbl.ID = "HiddenFieldItemEvaluacion" + FieldName;
                            field_lbl.Value = String.Empty;
                            field_lbl.DataBinding += new EventHandler(OnDataBinding);
                            Container.Controls.Add(field_lbl);
                            */


                            Literal myLnk = new Literal();
                            myLnk.ID = "LiteralItemEvaluacion" + FieldName;
                            Container.Controls.Add(myLnk);                        
                        }
                      
                        
                        break;

                }
                break;
            case ListItemType.EditItem:
                if (InfoType == "Command")
                {
                    Label field_lbl = new Label();
                    field_lbl.ID = "LabelValidacion";
                    field_lbl.Text = "Falta Ingresar Comentarios";
                    field_lbl.CssClass = "msg-error1";
                    field_lbl.Style.Add(HtmlTextWriterStyle.Display, "none");                    
                    Container.Controls.Add(field_lbl);

                    ImageButton update_button = new ImageButton();
                    update_button.ID = "update_button";
                    update_button.CommandName = "Update";
                    //update_button.ImageUrl = "~/images/calibracion/CalibracionGuardar.png";
                    update_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                    update_button.ToolTip = "Actualizar";
                    update_button.ValidationGroup = "Grabar";
                    update_button.CssClass = "IconosEdit IcnCalGrab";
                    update_button.OnClientClick = "Preloader();";
                    //update_button.OnClientClick = "return confirm('Are you sure to update the record?')";
                    Container.Controls.Add(update_button);

                    ImageButton print_button = new ImageButton();
                    print_button.ID = "print_button";
                    print_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                    print_button.CommandName = "Imprimir";
                    print_button.Click += new ImageClickEventHandler(edit_button_Click);
                    print_button.CssClass = "IcnCalImprimir";
                    print_button.ToolTip = "Imprimir";
                    print_button.OnClientClick = "ConfirmPrint();";
                    Container.Controls.Add(print_button);
                   

                    ImageButton cancel_button = new ImageButton();
                    //cancel_button.ImageUrl = "~/images/calibracion/CalibracionCancel.png";
                    cancel_button.ImageUrl = "~/images/calibracion/Calibracion1x1.png";
                    cancel_button.ID = "cancel_button";
                    cancel_button.CommandName = "Cancel";
                    cancel_button.ToolTip = "Cancelar";
                    cancel_button.CssClass = "IconosEdit IcnCalCancel";
                    cancel_button.OnClientClick = "Preloader();";
                    Container.Controls.Add(cancel_button);

                }
                else if (InfoType == "Comentario")
                {
                    TextBox editar_comentario = new TextBox();
                    editar_comentario.ID = "editar_comentario";
                    editar_comentario.MaxLength = 500;
                    Container.Controls.Add(editar_comentario);
                
                }
                else// for other 'non-command' i.e. the key and non key fields, bind textboxes with corresponding field values
                {

                    CustomValidator field_CV = new CustomValidator();
                    field_CV.ClientValidationFunction = "ValidarTextBoxFundamentacion";
                    field_CV.ID = "CustomValidatorTextBoxFundamentacion" + FieldName;
                    field_CV.ValidationGroup = "Grabar";
                    //field_CV.Text = "Debe Completar la Fundamentación";
                    field_CV.Display = ValidatorDisplay.Static;
                    //field_CV.ErrorMessage = "Debe Completar la Fundamentación";
                    Container.Controls.Add(field_CV);


                    //TextBox field_txtbox = new TextBox();
                    DropDownList field_dropDownList = new DropDownList();
                    ListItem Li0 = new ListItem();

                    Li0.Value = "";
                    Li0.Text = "No Corresponde";
                    field_dropDownList.Items.Add(Li0);

                    ListItem Li1 = new ListItem();
                    Li1.Value = "0";
                    Li1.Text = "Seleccione una opcion";
                    field_dropDownList.Items.Add(Li1);


                    ListItem Li2 = new ListItem();
                    Li2.Value = "1";
                    Li2.Text = "No Satisfactorio";
                    field_dropDownList.Items.Add(Li2);

                    ListItem Li3 = new ListItem();
                    Li3.Value = "2";
                    Li3.Text = "Regularmente Cumple / Necesita Desarrollarse";
                    field_dropDownList.Items.Add(Li3);

                    ListItem Li4 = new ListItem();
                    Li4.Value = "3";
                    Li4.Text = "Bueno";
                    field_dropDownList.Items.Add(Li4);

                    ListItem Li5 = new ListItem();
                    Li5.Value = "4";
                    Li5.Text = "Muy Bueno";
                    field_dropDownList.Items.Add(Li5);

                    int FieldNameInt;
                    int.TryParse(FieldName, out FieldNameInt);

                    field_dropDownList.ID = "DropDownListItemEvaluacion" + FieldName;
                    
                    //field_dropDownList.ToolTip = ToolTip;
                   // field_dropDownList.SelectedIndex = 0;
                    // if Inert is intended no need to bind it with text..keep them empty
                    //if ((int)new Page().Session["InsertFlag"] == 0)
                    field_dropDownList.DataBinding += new EventHandler(OnDataBinding);
                    Container.Controls.Add(field_dropDownList);

                   // TextBox field_txtbox = new TextBox();
                   // field_txtbox.ID = FieldName;
                   // field_txtbox.Text = String.Empty;
                   // // if Inert is intended no need to bind it with text..keep them empty
                   ////if ((int)new Page().Session["InsertFlag"] == 0)
                   //     field_txtbox.DataBinding += new EventHandler(OnDataBinding);
                   // Container.Controls.Add(field_txtbox);


                    //Label field_lbl = new Label();
                    //field_lbl.ID = "LabelItemEvaluacion" + FieldName;
                    //field_lbl.Text = String.Empty;
                    //field_lbl.DataBinding += new EventHandler(OnDataBinding);
                    //Container.Controls.Add(field_lbl);

                    HiddenField field_lbl = new HiddenField();
                    field_lbl.ID = "HiddenFieldItemEvaluacion" + FieldName;
                    field_lbl.Value = String.Empty;
                    field_lbl.DataBinding += new EventHandler(OnDataBinding);
                    Container.Controls.Add(field_lbl);                    

                    

                    

                    


                    Literal myLnk = new Literal();
                    myLnk.ID = "LiteralItemEvaluacion" + FieldName;
                    Container.Controls.Add(myLnk);
                }
                break;

        }

    }

    #endregion

    #region Event Handlers

    //just sets the insert flag ON so that we ll be able to decide in OnRowUpdating event whether to insert or update
    protected void insert_button_Click(Object sender, EventArgs e)
    {
        new Page().Session["InsertFlag"] = 1; 
    }
    //just sets the insert flag OFF so that we ll be able to decide in OnRowUpdating event whether to insert or update 
    protected void edit_button_Click(Object sender, EventArgs e)
    {
        //new Page().Session["InsertFlag"] = 0;
    }

    private void OnDataBinding(object sender, EventArgs e)
    {
        
        object bound_value_obj = null;
        
        Control ctrl = (Control)sender;
        IDataItemContainer data_item_container = (IDataItemContainer)ctrl.NamingContainer;
        bound_value_obj = DataBinder.Eval(data_item_container.DataItem, FieldName);
        
        switch (ItemType)
        {
            case ListItemType.Item:
                if (sender.GetType().ToString() == "System.Web.UI.WebControls.Label")
                {
                    Label field_ltrl = (Label)sender;
                    field_ltrl.Text = GetCalificacion(bound_value_obj.ToString());
                }
                else
                {
                    if (sender.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
                    {
                        DropDownList field_txtbox = (DropDownList)sender;
                        field_txtbox.SelectedValue = bound_value_obj.ToString();
                    }
                    else
                    {
                        bound_value_obj = DataBinder.Eval(data_item_container.DataItem, "Fundamentacion" + FieldName);
                        HiddenField field_ltrl1 = (HiddenField)sender;
                        field_ltrl1.Value = bound_value_obj.ToString();
                    }
                }
                break;
            case ListItemType.EditItem:
                //TextBox field_txtbox = (TextBox)sender;
                //field_txtbox.Text = bound_value_obj.ToString();
                if (sender.GetType().ToString() == "System.Web.UI.WebControls.DropDownList")
                {
                    DropDownList field_txtbox = (DropDownList)sender;
                    field_txtbox.SelectedValue = bound_value_obj.ToString();
                }
                else
                {
                    bound_value_obj = DataBinder.Eval(data_item_container.DataItem, "Fundamentacion" + FieldName);
                    HiddenField field_ltrl1 = (HiddenField)sender;
                    field_ltrl1.Value = bound_value_obj.ToString();
                }
               
               break;
        }


    }

    private string GetCalificacion(string calificacion)
    {

        switch (calificacion)
        {
            case "":
                calificacion = "";
                break;
            case "0":
                calificacion = "";
                break;
            case "1":
                calificacion = "No Satisfactorio";
                break;
            case "2":
                calificacion = "Regularmente Cumple / Necesita Desarrollarse";
                break;
            case "3":
                calificacion = "Bueno";
                break;
            case "4":
                calificacion = "Muy Bueno";
                break;
        }
        return calificacion;
    }

    #endregion


}
