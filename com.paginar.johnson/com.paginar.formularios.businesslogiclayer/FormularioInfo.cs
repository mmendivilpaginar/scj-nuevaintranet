﻿using System;
using System.Collections.Generic;
using System.Text;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    static public class FormularioInfo
    {
        static public int? GetPasoActual(int PeriodoID, int legajo, int TipoFormularioID)
        {
            int? aux = (string.IsNullOrEmpty(FachadaDA.Singleton.Historial.GetPasoByFormularioID(PeriodoID, legajo, TipoFormularioID))) ? null : (int?)Convert.ToInt32(FachadaDA.Singleton.Historial.GetPasoByFormularioID(PeriodoID, legajo, TipoFormularioID));
            return aux;
        }

        static public void SaveHistorial(int PasoID, int PeriodoID, int legajo, int TipoFormularioID)
        {
            FormulariosDS FDS = new FormulariosDS();
            int? PasoActualIDAux = GetPasoIDActual(PeriodoID, legajo, TipoFormularioID);
            int PasoActualID = (PasoActualIDAux == null) ? -1 : ((int)PasoActualIDAux);

            if ((PasoActualID == -1) || (PasoActualID != PasoID))
            {
                FachadaDA.Singleton.Historial.FillByID(FDS.Historial, legajo, PeriodoID, TipoFormularioID);
                FormulariosDS.HistorialRow HRow = FDS.Historial.NewHistorialRow();
                HRow.PasoID = PasoID;
                HRow.Legajo = legajo;
                HRow.PeriodoID = PeriodoID;
                HRow.TipoFormularioID = TipoFormularioID;
                HRow.Fecha = DateTime.Now;
                FDS.Historial.AddHistorialRow(HRow);
                FachadaDA.Singleton.Historial.Update(FDS.Historial);
            }

        }

        static public int? GetPasoIDActual(int PeriodoID, int legajo, int TipoFormularioID)
        {
            return FachadaDA.Singleton.FormularioPmpOperarios.GetPasoIDActual(legajo, PeriodoID, TipoFormularioID);
        }

        static public int GetCantHistorial(int PeriodoID, int legajo, int TipoFormularioID)
        {
            int? cantidad = 0;
            cantidad = FachadaDA.Singleton.Historial.GetCantHistorial(legajo, PeriodoID, TipoFormularioID);
            return (cantidad == null) ? 0 : ((int)cantidad);
        }

        static public bool EstuvoEnPaso(int legajo, int PeriodoID, int TipoFormularioID, int PasoID)
        {
            int? cantidad = 0;
            cantidad = FachadaDA.Singleton.Historial.EstuvoEnPaso(legajo, PeriodoID, TipoFormularioID,PasoID);

            return (cantidad > 0);
        }

        static public bool PuedeEditar(int legajoUsuario, int legajoEvaluado, int PeriodoID, int TipoFormularioID)
        { 
            
            int? legajoEvaluador = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(legajoEvaluado,PeriodoID,TipoFormularioID,1);
            int? legajoAuditor = FachadaDA.Singleton.RelEvaluadosEvaluadores.GetEvaluadorEnPaso(legajoEvaluado, PeriodoID, TipoFormularioID,3);

            if (GetPasoIDActual(PeriodoID, legajoEvaluado, TipoFormularioID) == 2 && legajoUsuario == legajoEvaluado)
                return true;

            if (GetPasoIDActual(PeriodoID, legajoEvaluado, TipoFormularioID) == 1 && legajoUsuario == legajoEvaluador)
                return true;

            if (GetPasoIDActual(PeriodoID, legajoEvaluado, TipoFormularioID) == 3 && legajoUsuario == legajoAuditor)
                return true;

            return false;
        
        }


        static public string DameURLForm(int TipoFormularioID, int PeriodoID, int PasoID)
        {
            string UrlForm= string.Empty;
            FormulariosDS.RelPasosTipoFormulariosDataTable RelPasosTipoFormulariosDt = new FormulariosDS.RelPasosTipoFormulariosDataTable();
            FachadaDA.Singleton.RelPasosTipoFormularios.FillByID(RelPasosTipoFormulariosDt, PasoID, PeriodoID, TipoFormularioID);

            if (RelPasosTipoFormulariosDt.Rows.Count > 0)
            {
                FormulariosDS.RelPasosTipoFormulariosRow RelPasosTipoFormulariosRow = RelPasosTipoFormulariosDt.Rows[0] as FormulariosDS.RelPasosTipoFormulariosRow;
                UrlForm = RelPasosTipoFormulariosRow.FormAspx;
            }

           return UrlForm;
        
        
        }
        
    }
}
