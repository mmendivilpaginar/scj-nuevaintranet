﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using com.paginar.formularios.dataaccesslayer;

namespace com.paginar.formularios.businesslogiclayer
{
    [DataObject]
    public class LogController
    {
        FormularioLogExportDS FLE = new FormularioLogExportDS();
        public LogController() { }
        public void Log(string usuario, string accion, string usuarioPMP)
        {
            FachadaDA.Singleton.log_DownloadDevTableAdapter.LogAction(usuario, accion,usuarioPMP);
        }
    }
}
